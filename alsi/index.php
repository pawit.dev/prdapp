<?php
session_start();
// include Yii bootstrap file
require_once(dirname(__FILE__).'/framework/yii.php');
$config=dirname(__FILE__).'/protected/config/main.php';

// include custom class
require_once dirname(__FILE__).'/protected/external/util/ConfigUtil.php';

// create a Web application instance and run
Yii::createWebApplication($config)->run();
