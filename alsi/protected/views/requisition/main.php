<?php 

$specifications = MSpecification::model()->findAll();
$typeOfTests = MTypeOfTest::model()->findAll();
$typeOfTestSubs = MTypeOfTestSub::model()->findAll();
$customers = MCustomer::model()->findAll();
$customerContractPerson = MCustomerContractPerson::model()->findAll();

?>




<div class="row-fluid">
	<div class="span12">
		<div class="tabbable tabbable-custom boxless">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1" data-toggle="tab">Analysis
						Requistion Form</a>
				</li>
				<li><a class="advance_form_with_chosen_element" href="#tab_2"
					data-toggle="tab">Sample Infomation Form</a>
				</li>
				<li><a class="advance_form_with_chosen_element" href="#tab_3"
					data-toggle="tab">Chain of Custody</a>
				</li>
			</ul>
			<div class="tab-content">

				<div class="tab-pane active" id="tab_1">
					<div class="portlet box green">
						<div class="portlet-title">
							<h4>
								<i class="icon-reorder"></i>ANALYSIS REQUISITION FORM
							</h4>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a> <a
									href="#portlet-config" data-toggle="modal" class="config"></a>
								<a href="javascript:;" class="reload"></a> <a
									href="javascript:;" class="remove"></a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="form-horizontal">
								<h3 class="form-section">Person Info</h3>
								<div class="row-fluid">
									<div class="span6 ">
										<label class="control-label" for="ContractPerson">Contract
											Person:</label>
										<div class="controls">
											<select class="span6 chosen"  id="contract_person_id" name="JobInfo[contract_person_id]">
												<option value="">--Select--</option>
												<?php foreach($customerContractPerson as $val) {?>
												<option value="<?php echo $val->ID?>">
													<?php echo $val->name?>
												</option>
												<?php }?>
											</select>
										</div>
									</div>
									<!--/span-->
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="CompanyName">Company Name:</label>
											<div class="controls">
												<input type="text" id="CompanyName" class="m-wrap span6"
													placeholder=""> <a href="#myModal1" role="button"
													class="btn btn-primary" data-toggle="modal">...</a>
											</div>
										</div>
										<div id="myModal1" class="modal hide fade" tabindex="-1"
											role="dialog" aria-labelledby="myModalLabel1"
											aria-hidden="true">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 id="myModalLabel1">Search Company</h3>
											</div>
											<div class="modal-body">
											<select class="span8"  id="customer_id" name="JobInfo[customer_id]">
												<option value="">--Select--</option>
												<?php foreach($customers as $val) {?>
												<option value="<?php echo $val->ID?>">
													<?php echo $val->company_name?>
												</option>
												<?php }?>
											</select>
											</div>
											<div class="modal-footer">
												<button class="btn yellow" data-dismiss="modal"
													aria-hidden="true">Select</button>
												<!-- 														<button class="btn yellow">OK</button> -->
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
								<!--/row-->
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="Department">Department:</label>
											<div class="controls">
												<input type="text" id="Department" class="m-wrap span6"
													placeholder="">
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="TelNumber">Tel. Number:</label>
											<div class="controls">
												<input type="text" id="TelNumber" class="m-wrap span6"
													placeholder="">
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="Email">Email:</label>
											<div class="controls">
												<div class="input-prepend">
													<span class="add-on">@</span><input type="text" id="Email"
														class="m-wrap" placeholder="Email Address">
												</div>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="Fax">Fax Number:</label>
											<div class="controls">
												<input type="text" id="Fax" class="m-wrap span6"
													placeholder="">
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
								<div class="row-fluid">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label" for="Address">Address:</label>
											<div class="controls">
												<textarea rows="4" cols="1" id="Address"
													class="m-wrap span6"></textarea>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
								<div class="row-fluid">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label" for="CustomerRefNo">Customer
												Ref. No.:</label>
											<div class="controls">
												<input type="text" id="customer_ref_no" name="JobInfo[customer_ref_no]" class="m-wrap span6"
													placeholder="">
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
								<div class="row-fluid">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label" for="CompanyNametoStateInReport">Company
												Name to State in Report:</label>
											<div class="controls">
												<input type="text" id="company_name_to_state_in_report" name="JobInfo[company_name_to_state_in_report]" 
													class="m-wrap span6" placeholder="">
											</div>
										</div>
									</div>
									<!--/span-->
								</div>

								<!--/row-->
								<h3 class="form-section">Job Information</h3>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="JobNumber">*Job Number:</label>
											<div class="controls">
												<input type="text" id="job_number" name="JobInfo[job_number]" class="m-wrap span6"
													placeholder="">
											</div>
										</div>
									</div>
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="DateOfReceive">Date of
												Receive:</label>
											<div class="controls">

												<div class="input-prepend">
													<span class="add-on"><i class="icon-calendar"></i> </span>
													<input class="m-wrap m-ctrl-medium date-picker" size="16"
														type="text" id="date_of_receive" name="JobInfo[date_of_receive]"
														value="<?php echo date('Y-m-d')?>"
														placeholder="yyyy-MM-dd" />
												</div>


											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label" for="JobNumber">S'pore Ref No:</label>
											<div class="controls">
												<input type="text" id="s_pore_ref_no" name="JobInfo[s_pore_ref_no]" class="m-wrap span6"
													placeholder="">
											</div>
										</div>
									</div>
								</div>
								<h3 class="form-section">Sample Infomation:</h3>
								<div class="row-fluid">
									<div class="span12 ">

										<div class="portlet-body">
											<div class="clearfix">
												<div class="btn-group">
													<button id="btnAddSampleInfo" class="btn green">
														Add New <i class="icon-plus"></i>
													</button>
												</div>
											</div>
											<table class="table table-striped table-hover table-bordered"
												id="gridSampleInfo">
												<thead>
													<tr>
														<th>S/N</th>
														<th>Sample Description (Part description, Part no. etc.)</th>
														<th>model</th>
														<th>Surface Area</th>
														<th>Remarks</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
													<?//php for( $i=0;$i<5;$i++){?>
<!-- 													<tr class=""> -->
<!-- 														<td>alex</td> -->
<!-- 														<td>Alex Nilson</td> -->
<!-- 														<td>1234</td> -->
<!-- 														<td class="center">power user</td> -->
<!-- 														<td class="center">power user</td> -->
<!-- 														<td><a class="edit" href="javascript:;">Edit</a> -->
<!-- 														</td> -->
<!-- 														<td><a class="delete" href="javascript:;">Delete</a> -->
<!-- 														</td> -->
<!-- 													</tr> -->
													<?php //}?>
												</tbody>
											</table>
										</div>

									</div>
								</div>
								<h3 class="form-section">Sample Requisition:</h3>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Sample Disposition:</label>
											<div class="controls">
												<label class="radio"> <input type="radio"
													name="JobInfo[sample_diposition]" value="0" /> Discard
												</label> <label class="radio"> <input type="radio"
													name="JobInfo[sample_diposition]" value="1" checked /> Return all
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Specification:</label>
											<div class="controls">
											<select class="span8 chosen"  id="specification_id" name="JobInfo[specification_id]">
												<option value="">--Select--</option>
												<?php foreach($specifications as $val) {?>
												<option value="<?php echo $val->ID?>">
													<?php echo $val->name?>
												</option>
												<?php }?>
											</select>
												<input class="span8"
													size="16" type="text" id="specification_other"
													name="JobInfo[specification_other]" value="" placeholder="Other" />
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Type Of Test:</label>
											<div class="controls">
											<select class="span8 chosen"  id="type_of_test_id" name="JobInfo[type_of_test_id]">
												<option value="">--Select--</option>
												<?php foreach($typeOfTests as $val) {?>
												<option value="<?php echo $val->ID?>">
													<?php echo $val->name?>
												</option>
												<?php }?>
											</select>
												<input class="span8"
													size="16" type="text" id="type_of_test_other"
													name="JobInfo[type_of_test_other]" value="" placeholder="Other" />

											</div>
										</div>
									</div>
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Sub Type Of Test:</label>
											<div class="controls">
											<select class="span8 chosen"  id="sub_type_of_test_id" name="JobInfo[sub_type_of_test_id]">
												<option value="">--Select--</option>
												<?php foreach($typeOfTestSubs as $val) {?>
												<option value="<?php echo $val->ID?>">
													<?php echo $val->name?>
												</option>
												<?php }?>
											</select>
												<input class="span8"
													size="16" type="text" id="type_of_test_other"
													name="JobInfo[type_of_test_other]" value="" placeholder="Other" />
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">No. of Report:</label>
											<div class="controls">
											<select class="span4 chosen"  id="no_of_report" name="JobInfo[no_of_report]">
												<option value="">--Select--</option>
												<?php for( $i=1;$i<=5;$i++){?>
												<option value="<?php echo $i?>">
													<?php echo $i?>
												</option>
												<?php }?>
											</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">**UNCERTAINTY:</label>
											<div class="controls">
												<label class="radio"> <input type="radio" id="uncertainty"
													name="JobInfo[uncertainty]" value="Y" /> Y
												</label> <label class="radio"> <input type="radio" id="uncertainty"
													name="JobInfo[uncertainty]" value="N" checked /> N
												</label>
											</div>
										</div>
									</div>
								</div>

								<h3 class="form-section">Internal Use Only</h3>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Custody seal infact?</label>
											<div class="controls">
												<label class="radio"> <input type="radio" id="custody_seal_intact_status" name="JobInfo[custody_seal_intact_status]"
													value="3" /> Express
												</label> <label class="radio"> <input type="radio" id="custody_seal_intact_status" 
													name="JobInfo[custody_seal_intact_status]" value="5" /> Urgent
												</label> <label class="radio"> <input type="radio" id="custody_seal_intact_status" 
													name="JobInfo[custody_seal_intact_status]" value="8" checked /> Normal
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Custody seal infact?</label>
											<div class="controls">
												<label class="radio"> <input type="radio" id="custody_seal_intact_status"
													name="JobInfo[custody_seal_intact_status]" value="Y" /> Yes
												</label> <label class="radio"> <input type="radio" id="custody_seal_intact_status"
													name="JobInfo[custody_seal_intact_status]" value="N" checked /> No
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Number of sample received?</label>
											<div class="controls">
												<label class="radio"> <input type="radio" id="number_of_sample_recived_status"
													name="JobInfo[number_of_sample_recived_status]" value="Y" /> Yes
												</label> <label class="radio"> <input type="radio" id="number_of_sample_recived_status"
													name="JobInfo[number_of_sample_recived_status]" value="N" checked />
													No
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Operator / Work load?</label>
											<div class="controls">
												<label class="radio"> <input type="radio" id="operator_workload_status"
													name="JobInfo[operator_workload_status]" value="Y" /> Yes
												</label> <label class="radio"> <input type="radio" id="operator_workload_status"
													name="JobInfo[operator_workload_status]" value="N" checked /> No
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Instrument / Test method?</label>
											<div class="controls">
												<label class="radio"> <input type="radio" id="instrument_test_method_status"
													name="JobInfo[instrument_test_method_status]" value="Y" /> Yes
												</label> <label class="radio"> <input type="radio" id="instrument_test_method_status"
													name="JobInfo[instrument_test_method_status]" value="N" checked />
													No
												</label>
											</div>
										</div>
									</div>
								</div>
								
								<!--/row-->
								<div class="form-actions">
									<button type="submit" class="btn blue">
										<i class="icon-ok"></i> Save
									</button>
									<button type="button" class="btn">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
				<div class="tab-pane " id="tab_2"></div>
				<div class="tab-pane " id="tab_3"></div>
			</div>
		</div>
	</div>
</div>

<!-- INCLUDE SCRIPT -->
<script
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.8.3.min.js"></script>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.uniform.min.js"></script>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/chosen.jquery.min.js"></script>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dataTables.js"></script>
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl; ?>/js/DT_bootstrap.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
		$('.date-picker').datepicker();

		App.setPage("requisition_form");  // set current page
		App.init(); // init the rest of plugins and elements
	});
</script>
<!-- END INCLUDE SCRIPT -->
