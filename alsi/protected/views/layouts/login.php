<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title>Metronic Admin Dashboard Template</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/metro.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_responsive.css" rel="stylesheet" />
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_default.css" rel="stylesheet" id="style_color" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/uniform.default.css" />
  <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
  <!-- BEGIN LOGO -->
  <div class="logo">

  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
		<?php echo $content; ?>
    <!-- END REGISTRATION FORM -->
  </div>
  <!-- END LOGIN -->
  <!-- BEGIN COPYRIGHT -->
  <div class="copyright">
    ALS TESTING SERVICES (THAILAND) CO., LTD. Part of the ALS Laboratory Group A Campbell Brothers Limited Company.
  </div>
  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>  
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.uniform.min.js"></script> 
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.blockui.js"></script>
  <script type="<?php echo Yii::app()->request->baseUrl; ?>/text/javascript" src="/js/jquery.validate.min.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
  <script>
    jQuery(document).ready(function() {     
//       App.initLogin();
    });
  </script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>