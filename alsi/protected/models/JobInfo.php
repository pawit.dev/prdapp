<?php

class JobInfo extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public function __construct($scenario='insert') {
		parent::__construct($scenario);
	}

	public static function model($className=__CLASS__)
	{

		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'job_info';
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'm_customer' => array(self::BELONGS_TO, 'MCustomer', 'company_id'),
				'm_Specification' => array(self::BELONGS_TO, 'MSpecification', 'specification_id'),
				'm_typeOfTest' => array(self::BELONGS_TO, 'MTypeOfTest', 'type_of_test_id'),
				'm_typeOfTestSub' => array(self::BELONGS_TO, 'MTypeOfTestSub', 'sub_type_of_test_id'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				'contract_person',
				'customer_id',
				'specification_id',
				'type_of_test_id',
				'sub_type_of_test_id',
				'customer_ref_no',
				'customer_po_ref',
				's_pore_ref_no',
				'company_name_to_state_in_report',
				'job_number',
				'invoice',
				'date_of_receive',
				'sample_diposition',
				'type_of_test_other',
				'sub_type_of_test_other',
				'no_of_report',
				'uncertainty',
				'work_type',
				'custody_seal_intact_status',
				'number_of_sample_recived_status',
				'operator_workload_status',
				'instrument_test_method_status',
				'create_by',
				'update_by',
				'create_date',
				'update_date',
				'duedate',
				'job_type','safe');
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	}

	/**
	 * @param Post the post that this comment belongs to. If null, the method
	 * will query for the post.
	 * @return string the permalink URL for this comment
	 */
	public function getUrl($post=null)
	{
		if($post===null)
			$post=$this->post;
		return $post->url.'#c'.$this->id;
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		return true;
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		return new CActiveDataProvider(get_class($this), array(
				'criteria' => $criteria,
				'sort' => array(
						'defaultOrder' => 't.create_date desc',
				),
				'pagination' => array(
						'pageSize' => ConfigUtil::getDefaultPageSize()
				),
		));
	}
}