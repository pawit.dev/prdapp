﻿using DOH.DC.Dao;
using DOH.DC.Model;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DOH.DC.MVC.Service.Controllers
{
    public class ServiceController : Controller
    {

        public ActionResult GetWimByLane(string _lane,string _stationId)
        {

            LiveViewDao dao = new LiveViewDao();
            List<WimModel> data = dao.getWimByLane(new DataFilters { lane = _lane, stationID = _stationId });


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult AsyncAmphurs(string SCHOOL_ADDR_PROVINCE)
        //{
        //    DelayResponse();
        //    var amphurs = repository.GetAmphur(SCHOOL_ADDR_PROVINCE).ToList().Select(a => new SelectListItem()
        //    {
        //        Text = a.AMPHUR_NAME,
        //        Value = a.AMPHUR_ID.ToString(),
        //    });

        //    return Json(amphurs);
        //}
    }
}
