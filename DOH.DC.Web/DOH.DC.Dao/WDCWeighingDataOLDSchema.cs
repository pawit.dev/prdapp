﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DOH.DC.Utils;

namespace DOH.DC.Dao
{
    public class WDCWeighingDataOLDSchema
    {
        public static string dbConnectionString = GlobalConfiguration.ConnectionString;

        public static List<DataTemplate> DataList = new List<DataTemplate>();
        public static DataTemplate Weighing = new DataTemplate();
        public static DataTemplate Weighing_LastInsert = new DataTemplate();
        public static DataTemplate Weighing_Today = new DataTemplate();
        public static DataTemplate Weighing_Today_CurrentUser = new DataTemplate();

        public static int GetTodayLastSeqNumber()
        {
            int ret = 0;
            Weighing_LastInsert.Refresh();
            if (Weighing_LastInsert.DataTable.Rows.Count > 0)
            {
                ret = (int)Weighing_LastInsert.DataTable.Rows[0]["SeqNumber"];
            }
            return ret;
        }

        public static long GetTodayLastENFID()
        {
            long ret = 0;
            Weighing_LastInsert.Refresh();
            if (Weighing_LastInsert.DataTable.Rows.Count > 0)
            {
                ret = (long)Weighing_LastInsert.DataTable.Rows[0]["ENFID"];
            }
            return ret;
        }

        public static DataTable GetWeighingRecord_By_ENFID(long ENFID)
        {
            DataTable table = new DataTable();
            try
            {
                DataTemplate WeighingByENFID = new DataTemplate();
                string conn = dbConnectionString;
                // table WeighingByTransID
                WeighingByENFID.dbConnString = conn;
                WeighingByENFID.TableName = "Weighing";
                WeighingByENFID.PKColumnName = "ENFID";
                WeighingByENFID.SQLCommandText = "SELECT * FROM Weighing WHERE ENFID=" + ENFID;
                WeighingByENFID.LoadDataFromDB();

                table = WeighingByENFID.DataTable;
            }
            catch { }

            return table;
        }

        public static DataTable GetWeighingView_By_ENFID(long ENFID)
        {
            DataTable table = new DataTable();
            try
            {
                DataTemplate WeighingByENFID = new DataTemplate();
                string conn = dbConnectionString;

                string sql = StaticSQLText.SqlMappedOldSchema;

                // table WeighingByENFID
                WeighingByENFID.dbConnString = conn;
                WeighingByENFID.TableName = "WeighingView";
                WeighingByENFID.PKColumnName = "";
                WeighingByENFID.SQLCommandText = sql + "WHERE dbo.Weighing.ENFID=" + ENFID;
                WeighingByENFID.LoadDataFromDB();

                table = WeighingByENFID.DataTable;
            }
            catch { }

            return table;
        }

        public static DataTable GetWeighingRecord_By_DateRange(DateTime begin, DateTime end, string stationID)
        {
            DataTable table = new DataTable();
            try
            {
                DataTemplate weighing = new DataTemplate();
                string conn = dbConnectionString;

                string sql = StaticSQLText.SqlMappedOldSchema;

                string stationFilter = string.Empty;
                if (stationID != "0")
                { 
                    stationFilter = " AND dbo.Weighing.StationID = " + stationID;
                }

                weighing.dbConnString = conn;
                weighing.TableName = "Weighing";
                weighing.PKColumnName = "";
                weighing.SQLCommandText = sql + "WHERE dbo.Weighing.TimeStamp >= '" + begin.ToString("s") + "'" +
                                                " AND dbo.Weighing.TimeStamp <= '" + end.ToString("s") + "'" +
                                                stationFilter;
                weighing.LoadDataFromDB();

                table = weighing.DataTable;
            }
            catch { }

            return table;
        }

        public static DataTable GetWeighingRecord_By_StartDate(DateTime begin, string stationID)
        {
            DataTable table = new DataTable();
            try
            {
                DataTemplate weighing = new DataTemplate();
                string conn = dbConnectionString;

                string sql = StaticSQLText.SqlMappedOldSchema;

                string stationFilter = string.Empty;
                if (stationID != "0")
                {
                    stationFilter = " AND dbo.Weighing.StationID = " + stationID;
                }

                weighing.dbConnString = conn;
                weighing.TableName = "Weighing";
                weighing.PKColumnName = "";
                weighing.SQLCommandText = sql + " WHERE dbo.Weighing.TimeStamp >= '" + begin.ToString("s") + "'" +
                                                stationFilter;

                weighing.LoadDataFromDB();

                table = weighing.DataTable;
            }
            catch { }

            return table;
        }

        public static DataTable GetWeighingRecord_By_EndDate(DateTime end, string stationID)
        {
            DataTable table = new DataTable();
            try
            {
                DataTemplate weighing = new DataTemplate();
                string conn = dbConnectionString;

                string stationFilter = string.Empty;
                if (stationID != "0")
                {
                    stationFilter = " AND dbo.Weighing.StationID = " + stationID;
                }

                string sql = StaticSQLText.SqlMappedOldSchema;

                weighing.dbConnString = conn;
                weighing.TableName = "Weighing";
                weighing.PKColumnName = "";
                weighing.SQLCommandText = sql + " WHERE dbo.Weighing.TimeStamp <= '" + end.ToString("s") + "'" +
                                                stationFilter;

                weighing.LoadDataFromDB();

                table = weighing.DataTable;
            }
            catch { }

            return table;
        }

        public static DataTable GetWeighingView_By_Variables(DateTime begin, DateTime end, string isWeightOver, string userID, string stationID)
        {
            DataTable table = new DataTable();
            try
            {
                DataTemplate weighing = new DataTemplate();
                string conn = dbConnectionString;

                string stationFilter = string.Empty;
                if (stationID != "0")
                {
                    stationFilter = " AND dbo.Weighing.StationID = " + stationID;
                }

                string userIDfilter = string.Empty;
                int uid;
                if (Int32.TryParse(userID, out uid))
                {
                    userIDfilter = " AND dbo.Weighing.UserID = " + userID;
                }
               
                string sql = StaticSQLText.SqlMappedOldSchema;

                weighing.dbConnString = conn;
                weighing.TableName = "WeighingView";
                weighing.PKColumnName = "";
                weighing.SQLCommandText = sql + " WHERE dbo.Weighing.TimeStamp >= '" + begin.ToString() + "'" +
                                          " AND dbo.Weighing.TimeStamp <= '" + end.ToString() + "'" +
                                          " AND dbo.Weighing.IsOverWeight LIKE '" + isWeightOver + "'" +
                                          userIDfilter +
                                          stationFilter;

                weighing.LoadDataFromDB();

                table = weighing.DataTable;
            }
            catch(Exception ex) {}

            return table;
        }

        //public static DataTable GetWeighingView_By_Today_CurrentUser()
        //{
        //    DataTable table = new DataTable();
        //    try
        //    {
        //        DataTemplate weighing = new DataTemplate();
        //        string conn = DBConn;

        //        string sql = SQLText.SqlMappedOldSchema;

        //        DateTime today = DateTime.Today;
        //        weighing.dbConnString = conn;
        //        weighing.TableName = "WeighingView";
        //        weighing.PKColumnName = "ENFID";
        //        weighing.SQLCommandText = sql + " WHERE TimeStamp >= '" + today.ToString("s") + "'" +
        //                                                " AND TimeStamp < '" + today.AddDays(1).ToString("s") + "'" +
        //                                                " AND dbo.Weighing.UserID = " + WDCLogin.UserID.ToString() +
        //                                                " AND dbo.Weighing.StationID = " + AppParameters.StationID +
        //                                                " ORDER BY ENFID DESC";

        //        weighing.LoadDataFromDB();

        //        table = weighing.DataTable;
        //    }
        //    catch { }

        //    return table;
        //}

        public static DataTable GetWeighingView_By_Date_PeriodID(DateTime date, int shiftID, string stationID)
        {
            DataTable table = new DataTable();
            try
            {
                DataTemplate weighing = new DataTemplate();
                string conn = dbConnectionString;

                string stationFilter = string.Empty;
                if (stationID != "0")
                {
                    stationFilter = " AND dbo.Weighing.StationID = " + stationID;
                }

                string sql = StaticSQLText.SqlMappedOldSchema;

                DateTime d1 = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
                DateTime d2 = d1.AddDays(1);
                weighing.dbConnString = conn;
                weighing.TableName = "WeighingView";
                weighing.PKColumnName = "";

                weighing.SQLCommandText = sql + " WHERE dbo.Weighing.TimeStamp >= '" + d1.ToString() + "'" +
                                                        " AND dbo.Weighing.TimeStamp < '" + d2.ToString() + "'" +
                                                        " AND dbo.Users.ShiftID = " + shiftID.ToString() +
                                                        stationFilter +
                                                        " ORDER BY dbo.Weighing.TimeStamp";

                weighing.LoadDataFromDB();

                table = weighing.DataTable;
            }
            catch { }

            return table;
        }

        //public static bool LoadData()
        //{
        //    bool ret = false;
        //    try
        //    {
        //        string conn = dbConnectionString;

        //        // table Weighing
        //        Weighing.dbConnString = conn;
        //        Weighing.TableName = "Weighing";
        //        Weighing.PKColumnName = "ENFID";
        //        Weighing.SQLCommandText = "SELECT * FROM Weighing";
        //        Weighing.LoadDataFromDB();
        //        DataList.Add(Weighing);


        //        // table Weighing_LastInsert
        //        Weighing_LastInsert.dbConnString = conn;
        //        Weighing_LastInsert.TableName = "Weighing_LastInsert";
        //        Weighing_LastInsert.PKColumnName = "ENFID";
        //        Weighing_LastInsert.SQLCommandText = "SELECT TOP 1 ENFID, SeqNumber FROM Weighing WHERE " +
        //                                             " CONVERT(varchar(10),TimeStamp, 101) = " +
        //                                             " CONVERT(varchar(10), getdate(), 101) " +
        //                                             " AND StationID = " + AppParameters.StationID +
        //                                             " ORDER BY ENFID DESC";
        //        Weighing_LastInsert.LoadDataFromDB();
        //        DataList.Add(Weighing_LastInsert);

        //        // table Weighing_Today
        //        DateTime today = DateTime.Today;
        //        Weighing_Today.dbConnString = conn;
        //        Weighing_Today.TableName = "Weighing_Today";
        //        Weighing_Today.PKColumnName = "ENFID";
        //        Weighing_Today.SQLCommandText = "SELECT * FROM Weighing_View_1 WHERE TimeStamp >= '" + today.ToString("s") + "'" +
        //                                                " AND TimeStamp < '" + today.AddDays(1).ToString("s") + "'" +
        //                                                " AND StationID = " + AppParameters.StationID +
        //                                                " ORDER BY ENFID DESC";
        //        Weighing_Today.LoadDataFromDB();
        //        DataList.Add(Weighing_Today);

        //        ret = true;

        //    }
        //    catch (Exception ex)
        //    {
        //        Messaging.LogAndShow("WDCWeighingOLDSchema", "ERROR_LoadData: " + ex.Message);
        //    }
        //    return ret;
        //}

        
    }
}
