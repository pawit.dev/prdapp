﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DOH.DC.Model;
using System.Data.SqlClient;
using System.Data;
using DOH.DC.Utils;
using System.IO;

namespace DOH.DC.Dao
{
    public class WeighingDao
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(WeighingDao));

        public static List<View_STATIC> select(DataFilters df)
        {
            List<View_STATIC> lists = new List<View_STATIC>();
            try
            {
                using (SqlConnection con = new SqlConnection(GlobalConfiguration.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand(SQLText.GetSQLText_STATIC(df), con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    int run_no = 1;
                    while (dr.Read())
                    {
                        View_STATIC model = new View_STATIC
                        {

                            ENFID = Convert.ToInt32(dr["ENFID"].ToString()),
                            StationID = Convert.ToInt16(dr["StationID"].ToString()),
                            TimeStamp = Convert.ToDateTime(dr["TimeStamp"].ToString()),
                            SeqNumber = Convert.ToInt16(dr["SeqNumber"].ToString()),
                            WIMID = Convert.ToInt32(dr["WIMID"].ToString()),
                            VehicleClassID = Convert.ToInt16(dr["VehicleClassID"].ToString()),
                            LPR_Number = dr["LPR_Number"].ToString(),
                            LPR_ProvinceID = Convert.ToInt16(dr["LPR_ProvinceID"].ToString()),
                            LPR_ImageName = dr["LPR_ImageName"].ToString(),
                            VehicleImageName = dr["VehicleImageName"].ToString(),
                            Material = dr["Material"].ToString(),
                            DepartedFrom = dr["DepartedFrom"].ToString(),
                            Destination = dr["Destination"].ToString(),
                            Notes = dr["Notes"].ToString(),
                            //ComplyMethod = dr["ComplyMethod"].ToString(),
                            //SaveMethod = dr["SaveMethod"].ToString(),
                            AG1_Weight_Max = Convert.ToInt32(dr["AG1_Weight_Max"].ToString()),
                            AG2_Weight_Max = Convert.ToInt32(dr["AG2_Weight_Max"].ToString()),
                            AG3_Weight_Max = Convert.ToInt32(dr["AG3_Weight_Max"].ToString()),
                            GVW_Weight_Max = Convert.ToInt32(dr["GVW_Weight_Max"].ToString()),
                            AG1_Weight_Measured = Convert.ToInt32(dr["AG1_Weight_Measured"].ToString()),
                            AG2_Weight_Measured = Convert.ToInt32(dr["AG2_Weight_Measured"].ToString()),
                            AG3_Weight_Measured = Convert.ToInt32(dr["AG3_Weight_Measured"].ToString()),
                            GVW_Weight_Measured = Convert.ToInt32(dr["GVW_Weight_Measured"].ToString()),
                            AG1_Weight_Over = Convert.ToInt32(dr["AG1_Weight_Over"].ToString()),
                            AG2_Weight_Over = Convert.ToInt32(dr["AG2_Weight_Over"].ToString()),
                            AG3_Weight_Over = Convert.ToInt32(dr["AG3_Weight_Over"].ToString()),
                            GVW_Weight_Over = Convert.ToInt32(dr["GVW_Weight_Over"].ToString()),
                            GVW_Weight_WIM = Convert.ToInt32(dr["GVW_Weight_WIM"].ToString()),
                            IsOverWeight = dr["IsOverWeight"].ToString(),
                            IsBGOpened = dr["IsBGOpened"].ToString(),
                            UserID = Convert.ToInt16(dr["UserID"].ToString()),
                            StationName = dr["StationName"].ToString(),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            LoginID = dr["LoginID"].ToString(),
                            ProvinceName = dr["ProvinceName"].ToString()

                        };
                        run_no++;
                        lists.Add(model);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
            return lists;
        }

        public static View_STATIC getById(String id)
        {
            List<View_STATIC> lists = new List<View_STATIC>();
            try
            {
                using (SqlConnection con = new SqlConnection(GlobalConfiguration.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[View_STATIC] Where ENFID=" + id, con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    int run_no = 1;
                    while (dr.Read())
                    {
                        View_STATIC model = new View_STATIC
                        {
                            Running = run_no,

                            ENFID = Convert.ToInt32(dr["ENFID"].ToString()),

                            StationID = Convert.ToInt32(dr["StationID"].ToString()),
                            TimeStamp = Convert.ToDateTime(dr["TimeStamp"].ToString()),
                            SeqNumber = Convert.ToInt32(dr["SeqNumber"].ToString()),
                            WIMID = Convert.ToInt64(dr["WIMID"].ToString()),
                            VehicleClassID = Convert.ToInt32(dr["VehicleClassID"].ToString()),
                            LPR_Number = dr["LPR_Number"].ToString(),
                            LPR_ProvinceID = Convert.ToInt32(dr["LPR_ProvinceID"].ToString()),
                            LPR_ImageName = dr["LPR_ImageName"].ToString(),
                            VehicleImageName = dr["VehicleImageName"].ToString(),
                            Material = dr["Material"].ToString(),
                            DepartedFrom = dr["DepartedFrom"].ToString(),
                            Destination = dr["Destination"].ToString(),
                            Notes = dr["Notes"].ToString(),
                            ComplyMethod = Convert.ToByte(dr["ComplyMethod"].ToString()),
                            SaveMethod = Convert.ToByte(dr["SaveMethod"].ToString()),
                            AG1_Weight_Max = Convert.ToInt32(dr["AG1_Weight_Max"].ToString()),
                            AG2_Weight_Max = Convert.ToInt32(dr["AG2_Weight_Max"].ToString()),
                            AG3_Weight_Max = Convert.ToInt32(dr["AG3_Weight_Max"].ToString()),
                            GVW_Weight_Max = Convert.ToInt32(dr["GVW_Weight_Max"].ToString()),
                            AG1_Weight_Measured = Convert.ToInt32(dr["AG1_Weight_Measured"].ToString()),
                            AG2_Weight_Measured = Convert.ToInt32(dr["AG2_Weight_Measured"].ToString()),
                            AG3_Weight_Measured = Convert.ToInt32(dr["AG3_Weight_Measured"].ToString()),
                            GVW_Weight_Measured = Convert.ToInt32(dr["GVW_Weight_Measured"].ToString()),
                            AG1_Weight_Over = Convert.ToInt32(dr["AG1_Weight_Over"].ToString()),
                            AG2_Weight_Over = Convert.ToInt32(dr["AG2_Weight_Over"].ToString()),
                            AG3_Weight_Over = Convert.ToInt32(dr["AG3_Weight_Over"].ToString()),
                            GVW_Weight_Over = Convert.ToInt32(dr["GVW_Weight_Over"].ToString()),
                            GVW_Weight_WIM = Convert.ToInt32(dr["GVW_Weight_WIM"].ToString()),
                            IsOverWeight = dr["IsOverWeight"].ToString(),
                            IsBGOpened = dr["IsBGOpened"].ToString(),
                            UserID = Convert.ToInt32(dr["UserID"].ToString()),
                            StationName = dr["StationName"].ToString(),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            LoginID = dr["LoginID"].ToString(),
                            ProvinceName = dr["ProvinceName"].ToString()
                        };
                        run_no++;
                        lists.Add(model);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
            return (lists.Count > 0) ? lists[0] : null;
        }
    }
}
