﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DOH.DC.Utils;

namespace DOH.DC.Dao
{
    public class WDCSiteParam
    {

        public static List<DataTemplate> DataList = new List<DataTemplate>();
     
        public static DataTemplate UsersView = new DataTemplate();
        public static DataTemplate UsersShiftView = new DataTemplate();
        public static DataTemplate Users = new DataTemplate();
        public static DataTemplate VehClass = new DataTemplate();
        public static DataTemplate Provinces = new DataTemplate();
        public static DataTemplate Shift = new DataTemplate();
        public static DataTemplate UserGroup = new DataTemplate();


        public static bool LoadData()
        {
            bool ret = false;
            try
            {
                string conn = GlobalConfiguration.ConnectionString;
                int stationID = GlobalConfiguration.StationID;
                
                //// table VehicleClass
                //VehClass.dbConnString = conn;
                //VehClass.TableName = "VehicleClass";
                //VehClass.PKColumnName = "VehicleClassID";
                //VehClass.SQLCommandText = "SELECT * FROM VehicleClass ORDER BY VehicleClassID";
                //VehClass.LoadDataFromDB();
                //DataList.Add(VehClass);

                //// table Province
                //Provinces.dbConnString = conn;
                //Provinces.TableName = "Provinces";
                //Provinces.PKColumnName = "ProvinceID";
                //Provinces.SQLCommandText = "SELECT * " +
                //                          "FROM Provinces ORDER BY ProvinceID";
                //Provinces.LoadDataFromDB();
                //DataList.Add(Provinces);

                //// table Shift
                //Shift.dbConnString = conn;
                //Shift.TableName = "Shift";
                //Shift.PKColumnName = "";
                //Shift.SQLCommandText = "SELECT * " +
                //                          "FROM Shift ORDER BY ShiftID";
                //Shift.LoadDataFromDB();
                //DataList.Add(Shift);

                //// table UserGroup
                //UserGroup.dbConnString = conn;
                //UserGroup.TableName = "UserGroup";
                //UserGroup.PKColumnName = "";
                //UserGroup.SQLCommandText = "SELECT * " +
                //                          "FROM UserGroup ORDER BY UserGroupID";
                //UserGroup.LoadDataFromDB();
                //DataList.Add(UserGroup);

                //// table Users
                //Users.dbConnString = conn;
                //Users.TableName = "Users";
                //Users.PKColumnName = "UserID";
                //Users.SQLCommandText = "SELECT * FROM Users WHERE Active = 1 AND StationID = " + stationID.ToString();
                //Users.LoadDataFromDB();
                //DataList.Add(Users);

                //// table UserShiftView
                //UsersShiftView.dbConnString = conn;
                //UsersShiftView.TableName = "Users";
                //UsersShiftView.PKColumnName = "";
                //UsersShiftView.SQLCommandText = "SELECT DISTINCT dbo.Users.ShiftID, dbo.Shift.ShiftDescription FROM Users " + 
                //                                //" WHERE UserTypeID = 4 " +       // Only Operator                                       
                //                                //" AND Active = 1 ORDER BY ShiftID";
                //                                "INNER JOIN dbo.Shift ON dbo.Shift.ShiftID = dbo.Users.ShiftID " +
                //                                "WHERE Active = 1 AND dbo.Users.StationID = " + stationID.ToString() + " ORDER BY dbo.Users.ShiftID";
                //UsersShiftView.LoadDataFromDB();
                //DataList.Add(UsersShiftView);

                // table UserView
                UsersView.dbConnString = conn;
                UsersView.TableName = "Users";
                UsersView.PKColumnName = "UserID";
                UsersView.SQLCommandText = "SELECT dbo.Users.UserID, dbo.Users.FirstName, dbo.Users.LastName, dbo.Users.Title, dbo.Users.StationID, " +
                                           "dbo.Users.ShiftID, dbo.Shift.ShiftDescription, dbo.Users.LoginID, dbo.Users.Password, " +
                                           "dbo.Users.UserGroupID, dbo.UserGroup.UserGroupDescription, " +
                                           "(dbo.Users.FirstName + ' ' + dbo.Users.LastName) AS FullName, dbo.Users.Active " +
                                           "FROM Users " +
                                           "INNER JOIN dbo.UserGroup ON dbo.UserGroup.UserGroupID = dbo.Users.UserGroupID  " +
                                           "INNER JOIN dbo.Shift ON dbo.Shift.ShiftID = dbo.Users.ShiftID  " +
                                           "WHERE dbo.Users.Active = 1 AND dbo.Users.StationID = " + stationID.ToString() + " ORDER BY dbo.Users.UserID ";

                if (UsersView.LoadDataFromDB() == 0)
                {
                    DataList.Add(UsersView);
                    ret = true;
                }
                else
                {
                    ret = false;
                }
               
            }
            catch (Exception ex)
            {
                //Messaging.LogAndShow("WDCSiteParam", "ERROR_LoadData: " + ex.Message);
            }
            return ret;
        }



        public static DataTable GetTable_UserView(string stationID)
        {
            string sql = "SELECT dbo.Users.UserID, dbo.Users.FirstName, dbo.Users.LastName, dbo.Users.Title, dbo.Users.StationID, " +
                                "dbo.Users.ShiftID, dbo.Shift.ShiftDescription, dbo.Users.LoginID, dbo.Users.Password, " +
                                "dbo.Users.UserGroupID, dbo.UserGroup.UserGroupDescription, " +
                                "ISNULL(dbo.Users.FirstName + ' ' + dbo.Users.LastName,'') AS FullName, dbo.Users.Active " +
                                "FROM Users " +
                                "INNER JOIN dbo.UserGroup ON dbo.UserGroup.UserGroupID = dbo.Users.UserGroupID  " +
                                "INNER JOIN dbo.Shift ON dbo.Shift.ShiftID = dbo.Users.ShiftID  " +
                                "WHERE dbo.Users.Active = 1 AND dbo.Users.StationID = " + stationID + " " +
                                "AND dbo.Users.UserGroupID > 1 " +
                                "ORDER BY dbo.Users.UserID ";
            return Util.GetDataTable(sql);      
        }

        public static DataTable GetTable_ShiftView()
        {
            string sql = "SELECT dbo.Shift.ShiftID, dbo.Shift.ShiftDescription FROM dbo.Shift " +
                " WHERE dbo.Shift.ShiftID > 0 " +
                " ORDER BY dbo.Shift.ShiftID";
            return Util.GetDataTable(sql);
        }

        public static string GetProvinceName(string provinceID)
        {
            string output = string.Empty;
            try
            {
                foreach (DataRow dr in Provinces.DataTable.Rows)
                {
                    if (dr["ProvinceID"].ToString() == provinceID)
                    {
                        output = (string)dr["ProvinceName"];
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //Messaging.LogAndShow("WDCSiteParam", "ERROR_GetProvinceName: " + ex.Message);
            }
            return output;
        }

        public static int GetProvinceID(string provinceName)
        {
            int output = 0;
            try
            {
                foreach (DataRow dr in Provinces.DataTable.Rows)
                {
                    if (dr["ProvinceName"].ToString() == provinceName)
                    {
                        output = (int)dr["ProvinceID"];
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //Messaging.LogAndShow("WDCSiteParam", "ERROR_GetProvinceID: " + ex.Message);
            }
            return output;
        }

        public static byte GetVehicleClassID(byte wimClassID)
        {
            byte output = 0;
            try
            {
                foreach (DataRow dr in VehClass.DataTable.Rows)
                {
                    if ((byte)((int)dr["WIMClassID"]) == wimClassID)
                    {
                        output = (byte)((int)dr["VehicleClassID"]);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                //Messaging.LogAndShow("WDCSiteParam", "ERROR_GetVehicleClassID: " + ex.Message);
            }
            return output;
        }

    }
}
