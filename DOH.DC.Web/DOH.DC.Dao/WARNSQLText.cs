﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOH.DC.Dao
{
    public class WARNSQLText
    {
        public static string GetQuery_REP_WARN_101(DateTime dateFrom, DateTime dateTo, int stationID)
        {            
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            string sql = string.Empty;

            sql += "    SELECT dbo.WarningVehicles.Type AS WarningType, COUNT(dbo.WarningVehicles.Type) AS VehCount";           
            sql += "    FROM dbo.WarningVehicles ";
            sql += "    WHERE ";
            sql += "    TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "    AND TimeStamp <= '" + dateTo.ToString("s") + "'"; 
            sql += filter_stationID;
            sql += "    GROUP BY dbo.WarningVehicles.Type ";
            sql += "    ORDER BY dbo.WarningVehicles.Type ";

            return sql;
        }

        public static string GetQuery_REP_WARN_102(DateTime dateFrom, DateTime dateTo, int stationID, string datepart)
        {
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            string sql = string.Empty;

            sql += "    SELECT DATEPART(hh, TimeStamp) AS Duration, Type AS WarningType, COUNT(Type) AS VehCount";
            sql += "    FROM dbo.WarningVehicles ";
            sql += "    WHERE ";
            sql += "    TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "    AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += "    GROUP BY DATEPART(hh, TimeStamp), Type ";
            sql += "    ORDER BY DATEPART(hh, TimeStamp), Type ";

            sql = sql.Replace("hh", datepart);

            return sql;
        }

        public static string GetQuery_REP_WARN_103(DateTime dateFrom, DateTime dateTo, int stationID, int warningType)
        {
            string filter_stationID = string.Empty;
            if (stationID > 0)
            {
                filter_stationID = " AND StationID = " + stationID;
            }

            string filter_warningType = string.Empty;
            if (warningType > 0)
            {
                filter_warningType = " AND Type = " + warningType.ToString();
            }

            string sql = string.Empty;


            sql += "    SELECT Type, RefID, StationID, TimeStamp, RefNumber, ImageName, ImageName2";
            sql += "    FROM dbo.WarningVehicles ";
            sql += "    WHERE ";
            sql += "    TimeStamp >= '" + dateFrom.ToString("s") + "'";
            sql += "    AND TimeStamp <= '" + dateTo.ToString("s") + "'";
            sql += filter_stationID;
            sql += filter_warningType;
            sql += "    ORDER BY TimeStamp ";
            return sql;
        }

        
    }
}
