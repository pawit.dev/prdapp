﻿using DOH.DC.Model;
using DOH.DC.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace DOH.DC.Dao
{
    public class LiveViewDao
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(LiveViewDao));
        public  List<WimModel> getWimByLane(DataFilters df)
        {
            logger.Debug(String.Format("Station ID {0} , Lane {1}", df.stationID, df.lane));

            List<WimModel> listWim = new List<WimModel>();
            try
            {
                String stationID = df.stationID;// (String)HttpContext.Current.Session["stationID"];
                String sql = "Select Top 10 WIMID,TimeStamp,GVW,MaxGVW,StationName,StatusCode,Image01Name,VehicleNumber,Lane,VehicleClass,SortDecision from View_WIM Where Lane='" + df.lane + "'" +
                    ((!String.IsNullOrEmpty(stationID)) ? " and StationID='" + stationID + "'" : "") +
                    " order by TimeStamp desc";
                logger.Debug(sql);
                StringBuilder div = new StringBuilder();
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DohConStr"].ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql.ToString(), connection))
                    {
                        command.Notification = null;

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                        int running = 1;


                        while (dr.Read())
                        {
                            String imgPath = "";
                            if (!String.IsNullOrEmpty(Path.GetExtension(dr["Image01Name"].ToString())))
                            {
                                string[] imgArgs = dr["Image01Name"].ToString().Split('-');
                                String ipAddr = (String)HttpContext.Current.Session["RequestIP"];

                                if (ipAddr.Substring(0, 12).Equals("192.168.100."))
                                {
                                    imgPath = String.Format(GlobalConfiguration.imageWebPathLocal, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper(), dr["Image01Name"].ToString());
                                }
                                else
                                {
                                    imgPath = String.Format(GlobalConfiguration.ImageWebPath, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), imgArgs[0].ToUpper(), dr["Image01Name"].ToString());
                                }
                            }
                            else
                            {
                                imgPath = "http://" + HttpContext.Current.Request.Url.Authority + "/dc/images/inf.jpg";
                            }
                            WimModel wim = new WimModel();
                            wim.wimId = (DBNull.Value == dr["WIMID"]) ? 0 : Convert.ToInt32(dr["WIMID"]);
                            wim.StationName = (DBNull.Value == dr["StationName"]) ? String.Empty : dr["StationName"].ToString();
                            wim.TimeStamp = (DBNull.Value == dr["TimeStamp"]) ? DateTime.MinValue : Convert.ToDateTime(dr["TimeStamp"]);
                            wim.VehicleNumber = (DBNull.Value == dr["VehicleNumber"]) ? 0 : Convert.ToInt32(dr["VehicleNumber"]);
                            wim.VehicleClass = (DBNull.Value == dr["VehicleClass"]) ? 0 : Convert.ToInt32(dr["VehicleClass"]);
                            wim.SortDecision = (DBNull.Value == dr["SortDecision"]) ? String.Empty : dr["SortDecision"].ToString();
                            wim.Lane = (DBNull.Value == dr["Lane"]) ? 0 : Convert.ToInt32(dr["Lane"]);
                            wim.MaxGvw = (DBNull.Value == dr["MaxGvw"]) ? String.Empty : Function.customNumberFormat(dr["MaxGvw"].ToString());
                            wim.Gvw = (DBNull.Value == dr["Gvw"]) ? String.Empty : Function.customNumberFormat(dr["Gvw"].ToString());
                            wim.imgpath = imgPath;
                            listWim.Add(wim);

                            running++;
                        }

                        dr.Close();
                    }
                }
            }
            catch (Exception ex) {
                logger.Error(ex.Message);
            }
            return listWim;
        }

    }
}
