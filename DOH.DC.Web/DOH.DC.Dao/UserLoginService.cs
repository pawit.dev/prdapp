﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DOH.DC.Model;
using DOH.DC.Dao;



namespace DOH.VWS.Biz
{
    public class UserLoginService
    {
        public bool authen(string username, string password)
        {
            bool result = false;
            UserLoginDao dao = new UserLoginDao();
            UserLogin userLoginBean = dao.getUserLoginByUsernameAndPassword(username, password);
            if (userLoginBean != null)
            {
                

               
                HttpContext.Current.Session["isLogin"] = "true";
                HttpContext.Current.Session["userLoginBean"] = userLoginBean;
                result = true;
            }
            return result;
        }

        public void logout()
        {
            HttpContext.Current.Session.Remove("isLogin");
            HttpContext.Current.Session.Remove("userLoginBean");
        }

        //public bool editProfile(int id, string password, string email, string fullName)
        //{
        //    UserLoginBAO bao = new UserLoginBAO();
        //    UserLoginBean bean = bao.updateByPk(id, password, email, fullName);
        //    if (bean != null)
        //    {
        //        HttpContext.Current.Session["userLoginBean"] = bean;
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //}
    }
}