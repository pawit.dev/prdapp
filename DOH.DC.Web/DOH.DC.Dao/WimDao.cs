﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DOH.DC.Model;
using System.Data.SqlClient;
using System.Data;
using DOH.DC.Utils;
using System.IO;

namespace DOH.DC.Dao
{
    public class WimDao
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(WimDao));


        public static View_WIM getById(String id)
        {
            List<View_WIM> lists = new List<View_WIM>();
            try
            {
                using (SqlConnection con = new SqlConnection(GlobalConfiguration.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(SQLText.GetSQLText_ViewWim(id), con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    int run_no = 1;
                    while (dr.Read())
                    {
                        View_WIM model = new View_WIM
                        {
                            Running = run_no,
                            //logo = logo,
                            StationName = dr["StationName"].ToString(),
                            WIMID = Convert.ToInt32(dr["WIMID"]),
                            StationID = Convert.ToInt16(dr["StationID"]),
                            TimeStamp = Convert.ToDateTime(dr["TimeStamp"]),
                            VehicleNumber = Convert.ToInt32(dr["VehicleNumber"]),
                            Lane = Convert.ToInt16(dr["Lane"]),
                            Error = Convert.ToInt16(dr["Error"]),
                            StatusCode = Convert.ToInt16(dr["StatusCode"]),

                            GVW = Convert.ToInt32(dr["GVW"]),

                            MaxGVW = Convert.ToInt32(dr["MaxGVW"]),
                            ESAL = Convert.ToDouble(dr["ESAL"]),
                            Speed = Convert.ToInt16(dr["Speed"]),
                            AxleCount = Convert.ToInt16(dr["AxleCount"]),

                            Axle01Seperation = Convert.ToInt16(dr["Axle01Seperation"]),
                            Axle01Weight = Convert.ToInt16(dr["Axle01Weight"]),
                            Axle01Max = Convert.ToInt16(dr["Axle01Max"]),
                            Axle01Group = Convert.ToInt16(dr["Axle01Group"]),
                            Axle01TireCode = Convert.ToInt16(dr["Axle01TireCode"]),
                            Axle02Seperation = Convert.ToInt16(dr["Axle02Seperation"]),
                            Axle02Weight = Convert.ToInt16(dr["Axle02Weight"]),
                            Axle02Max = Convert.ToInt16(dr["Axle02Max"]),
                            Axle02Group = Convert.ToInt16(dr["Axle02Group"]),
                            Axle02TireCode = Convert.ToInt16(dr["Axle02TireCode"]),
                            Axle03Seperation = Convert.ToInt16(dr["Axle03Seperation"]),
                            Axle03Weight = Convert.ToInt16(dr["Axle03Weight"]),
                            Axle03Max = Convert.ToInt16(dr["Axle03Max"]),
                            Axle03Group = Convert.ToInt16(dr["Axle03Group"]),
                            Axle03TireCode = Convert.ToInt16(dr["Axle03TireCode"]),
                            Axle04Seperation = Convert.ToInt16(dr["Axle04Seperation"]),
                            Axle04Weight = Convert.ToInt16(dr["Axle04Weight"]),
                            Axle04Max = Convert.ToInt16(dr["Axle04Max"]),
                            Axle04Group = Convert.ToInt16(dr["Axle04Group"]),
                            Axle04TireCode = Convert.ToInt16(dr["Axle04TireCode"]),
                            Axle05Seperation = Convert.ToInt16(dr["Axle05Seperation"]),
                            Axle05Weight = Convert.ToInt16(dr["Axle05Weight"]),
                            Axle05Max = Convert.ToInt16(dr["Axle05Max"]),
                            Axle05Group = Convert.ToInt16(dr["Axle05Group"]),
                            Axle05TireCode = Convert.ToInt16(dr["Axle05TireCode"]),
                            Axle06Seperation = Convert.ToInt16(dr["Axle06Seperation"]),
                            Axle06Weight = Convert.ToInt16(dr["Axle06Weight"]),
                            Axle06Max = Convert.ToInt16(dr["Axle06Max"]),
                            Axle06Group = Convert.ToInt16(dr["Axle06Group"]),
                            Axle06TireCode = Convert.ToInt16(dr["Axle06TireCode"]),
                            Axle07Seperation = Convert.ToInt16(dr["Axle07Seperation"]),
                            Axle07Weight = Convert.ToInt16(dr["Axle07Weight"]),
                            Axle07Max = Convert.ToInt16(dr["Axle07Max"]),
                            Axle07Group = Convert.ToInt16(dr["Axle07Group"]),
                            Axle07TireCode = Convert.ToInt16(dr["Axle07TireCode"]),
                            Axle08Seperation = Convert.ToInt16(dr["Axle08Seperation"]),
                            Axle08Weight = Convert.ToInt16(dr["Axle08Weight"]),
                            Axle08Max = Convert.ToInt16(dr["Axle08Max"]),
                            Axle08Group = Convert.ToInt16(dr["Axle08Group"]),
                            Axle08TireCode = Convert.ToInt16(dr["Axle08TireCode"]),
                            Axle09Seperation = Convert.ToInt16(dr["Axle09Seperation"]),
                            Axle09Weight = Convert.ToInt16(dr["Axle09Weight"]),
                            Axle09Max = Convert.ToInt16(dr["Axle09Max"]),
                            Axle09Group = Convert.ToInt16(dr["Axle09Group"]),
                            Axle09TireCode = Convert.ToInt16(dr["Axle09TireCode"]),
                            Axle10Seperation = Convert.ToInt16(dr["Axle10Seperation"]),
                            Axle10Weight = Convert.ToInt16(dr["Axle10Weight"]),
                            Axle10Max = Convert.ToInt16(dr["Axle10Max"]),
                            Axle10Group = Convert.ToInt16(dr["Axle10Group"]),
                            Axle10TireCode = Convert.ToInt16(dr["Axle10TireCode"]),
                            Axle11Seperation = Convert.ToInt16(dr["Axle11Seperation"]),
                            Axle11Weight = Convert.ToInt16(dr["Axle11Weight"]),
                            Axle11Max = Convert.ToInt16(dr["Axle11Max"]),
                            Axle11Group = Convert.ToInt16(dr["Axle11Group"]),
                            Axle11TireCode = Convert.ToInt16(dr["Axle11TireCode"]),
                            Axle12Seperation = Convert.ToInt16(dr["Axle12Seperation"]),
                            Axle12Weight = Convert.ToInt16(dr["Axle12Weight"]),
                            Axle12Max = Convert.ToInt16(dr["Axle12Max"]),
                            Axle12Group = Convert.ToInt16(dr["Axle12Group"]),
                            Axle12TireCode = Convert.ToInt16(dr["Axle12TireCode"]),
                            Axle13Seperation = Convert.ToInt16(dr["Axle13Seperation"]),
                            Axle13Weight = Convert.ToInt16(dr["Axle13Weight"]),
                            Axle13Max = Convert.ToInt16(dr["Axle13Max"]),
                            Axle13Group = Convert.ToInt16(dr["Axle13Group"]),
                            Axle13TireCode = Convert.ToInt16(dr["Axle13TireCode"]),
                            Axle14Seperation = Convert.ToInt16(dr["Axle14Seperation"]),
                            Axle14Weight = Convert.ToInt16(dr["Axle14Weight"]),
                            Axle14Max = Convert.ToInt16(dr["Axle14Max"]),
                            Axle14Group = Convert.ToInt16(dr["Axle14Group"]),
                            Axle14TireCode = Convert.ToInt16(dr["Axle14TireCode"]),
                            Length = Convert.ToInt16(dr["Length"]),
                            FrontOverHang = Convert.ToInt16(dr["FrontOverHang"]),
                            RearOverHang = Convert.ToInt16(dr["RearOverHang"]),
                            VehicleType = Convert.ToInt16(dr["VehicleType"]),
                            VehicleClass = Convert.ToInt16(dr["VehicleClass"]),
                            RecordType = Convert.ToInt16(dr["RecordType"]),
                            ImageCount = Convert.ToInt16(dr["ImageCount"]),
                            Image01Name = Convert.ToString(dr["Image01Name"]),
                            Image02Name = Convert.ToString(dr["Image02Name"]),
                            Image03Name = Convert.ToString(dr["Image03Name"]),
                            Image04Name = Convert.ToString(dr["Image04Name"]),
                            Image05Name = Convert.ToString(dr["Image05Name"]),
                            Image06Name = Convert.ToString(dr["Image06Name"]),
                            Image07Name = Convert.ToString(dr["Image07Name"]),
                            Image08Name = Convert.ToString(dr["Image08Name"]),
                            Image09Name = Convert.ToString(dr["Image09Name"]),
                            Image10Name = Convert.ToString(dr["Image10Name"]),
                            SortDecision = Convert.ToInt16(dr["SortDecision"]),
                            LicensePlateNumber = Convert.ToString(dr["LicensePlateNumber"]),
                            LicensePlateProvinceID = Convert.ToInt16(dr["LicensePlateProvinceID"]),
                            LicensePlateImageName = Convert.ToString(dr["LicensePlateImageName"]),
                            LicensePlateConfidence = Convert.ToInt16(dr["LicensePlateConfidence"]),

                        };
                        run_no++;
                        lists.Add(model);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
            return (lists.Count > 0) ? lists[0] : null;
        }
        public static List<View_WIM> select(DataFilters df)
        {
            List<View_WIM> lists = new List<View_WIM>();
            try
            {
                using (SqlConnection con = new SqlConnection(GlobalConfiguration.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand(SQLText.GetSQLText_WIM(df), con);
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    int run_no = 1;
                    while (dr.Read())
                    {
                        View_WIM model = new View_WIM
                        {
                            Running = run_no,
                            //logo = logo,
                            StationName = dr["StationName"].ToString(),
                            WIMID = Convert.ToInt32(dr["WIMID"]),
                            StationID = Convert.ToInt16(dr["StationID"]),
                            TimeStamp = Convert.ToDateTime(dr["TimeStamp"]),
                            VehicleNumber = Convert.ToInt16(dr["VehicleNumber"]),
                            Lane = Convert.ToInt16(dr["Lane"]),
                            Error = Convert.ToInt16(dr["Error"]),
                            StatusCode = Convert.ToInt16(dr["StatusCode"]),

                            GVW = Convert.ToInt32(dr["GVW"]),

                            MaxGVW = Convert.ToInt32(dr["MaxGVW"]),
                            ESAL = Convert.ToDouble(dr["ESAL"]),
                            Speed = Convert.ToInt16(dr["Speed"]),
                            AxleCount = Convert.ToInt16(dr["AxleCount"]),

                            Axle01Seperation = Convert.ToInt16(dr["Axle01Seperation"]),
                            Axle01Weight = Convert.ToInt16(dr["Axle01Weight"]),
                            Axle01Max = Convert.ToInt16(dr["Axle01Max"]),
                            Axle01Group = Convert.ToInt16(dr["Axle01Group"]),
                            Axle01TireCode = Convert.ToInt16(dr["Axle01TireCode"]),
                            Axle02Seperation = Convert.ToInt16(dr["Axle02Seperation"]),
                            Axle02Weight = Convert.ToInt16(dr["Axle02Weight"]),
                            Axle02Max = Convert.ToInt16(dr["Axle02Max"]),
                            Axle02Group = Convert.ToInt16(dr["Axle02Group"]),
                            Axle02TireCode = Convert.ToInt16(dr["Axle02TireCode"]),
                            Axle03Seperation = Convert.ToInt16(dr["Axle03Seperation"]),
                            Axle03Weight = Convert.ToInt16(dr["Axle03Weight"]),
                            Axle03Max = Convert.ToInt16(dr["Axle03Max"]),
                            Axle03Group = Convert.ToInt16(dr["Axle03Group"]),
                            Axle03TireCode = Convert.ToInt16(dr["Axle03TireCode"]),
                            Axle04Seperation = Convert.ToInt16(dr["Axle04Seperation"]),
                            Axle04Weight = Convert.ToInt16(dr["Axle04Weight"]),
                            Axle04Max = Convert.ToInt16(dr["Axle04Max"]),
                            Axle04Group = Convert.ToInt16(dr["Axle04Group"]),
                            Axle04TireCode = Convert.ToInt16(dr["Axle04TireCode"]),
                            Axle05Seperation = Convert.ToInt16(dr["Axle05Seperation"]),
                            Axle05Weight = Convert.ToInt16(dr["Axle05Weight"]),
                            Axle05Max = Convert.ToInt16(dr["Axle05Max"]),
                            Axle05Group = Convert.ToInt16(dr["Axle05Group"]),
                            Axle05TireCode = Convert.ToInt16(dr["Axle05TireCode"]),
                            Axle06Seperation = Convert.ToInt16(dr["Axle06Seperation"]),
                            Axle06Weight = Convert.ToInt16(dr["Axle06Weight"]),
                            Axle06Max = Convert.ToInt16(dr["Axle06Max"]),
                            Axle06Group = Convert.ToInt16(dr["Axle06Group"]),
                            Axle06TireCode = Convert.ToInt16(dr["Axle06TireCode"]),
                            Axle07Seperation = Convert.ToInt16(dr["Axle07Seperation"]),
                            Axle07Weight = Convert.ToInt16(dr["Axle07Weight"]),
                            Axle07Max = Convert.ToInt16(dr["Axle07Max"]),
                            Axle07Group = Convert.ToInt16(dr["Axle07Group"]),
                            Axle07TireCode = Convert.ToInt16(dr["Axle07TireCode"]),
                            Axle08Seperation = Convert.ToInt16(dr["Axle08Seperation"]),
                            Axle08Weight = Convert.ToInt16(dr["Axle08Weight"]),
                            Axle08Max = Convert.ToInt16(dr["Axle08Max"]),
                            Axle08Group = Convert.ToInt16(dr["Axle08Group"]),
                            Axle08TireCode = Convert.ToInt16(dr["Axle08TireCode"]),
                            Axle09Seperation = Convert.ToInt16(dr["Axle09Seperation"]),
                            Axle09Weight = Convert.ToInt16(dr["Axle09Weight"]),
                            Axle09Max = Convert.ToInt16(dr["Axle09Max"]),
                            Axle09Group = Convert.ToInt16(dr["Axle09Group"]),
                            Axle09TireCode = Convert.ToInt16(dr["Axle09TireCode"]),
                            Axle10Seperation = Convert.ToInt16(dr["Axle10Seperation"]),
                            Axle10Weight = Convert.ToInt16(dr["Axle10Weight"]),
                            Axle10Max = Convert.ToInt16(dr["Axle10Max"]),
                            Axle10Group = Convert.ToInt16(dr["Axle10Group"]),
                            Axle10TireCode = Convert.ToInt16(dr["Axle10TireCode"]),
                            Axle11Seperation = Convert.ToInt16(dr["Axle11Seperation"]),
                            Axle11Weight = Convert.ToInt16(dr["Axle11Weight"]),
                            Axle11Max = Convert.ToInt16(dr["Axle11Max"]),
                            Axle11Group = Convert.ToInt16(dr["Axle11Group"]),
                            Axle11TireCode = Convert.ToInt16(dr["Axle11TireCode"]),
                            Axle12Seperation = Convert.ToInt16(dr["Axle12Seperation"]),
                            Axle12Weight = Convert.ToInt16(dr["Axle12Weight"]),
                            Axle12Max = Convert.ToInt16(dr["Axle12Max"]),
                            Axle12Group = Convert.ToInt16(dr["Axle12Group"]),
                            Axle12TireCode = Convert.ToInt16(dr["Axle12TireCode"]),
                            Axle13Seperation = Convert.ToInt16(dr["Axle13Seperation"]),
                            Axle13Weight = Convert.ToInt16(dr["Axle13Weight"]),
                            Axle13Max = Convert.ToInt16(dr["Axle13Max"]),
                            Axle13Group = Convert.ToInt16(dr["Axle13Group"]),
                            Axle13TireCode = Convert.ToInt16(dr["Axle13TireCode"]),
                            Axle14Seperation = Convert.ToInt16(dr["Axle14Seperation"]),
                            Axle14Weight = Convert.ToInt16(dr["Axle14Weight"]),
                            Axle14Max = Convert.ToInt16(dr["Axle14Max"]),
                            Axle14Group = Convert.ToInt16(dr["Axle14Group"]),
                            Axle14TireCode = Convert.ToInt16(dr["Axle14TireCode"]),
                            Length = Convert.ToInt16(dr["Length"]),
                            FrontOverHang = Convert.ToInt16(dr["FrontOverHang"]),
                            RearOverHang = Convert.ToInt16(dr["RearOverHang"]),
                            VehicleType = Convert.ToInt16(dr["VehicleType"]),
                            VehicleClass = Convert.ToInt16(dr["VehicleClass"]),
                            RecordType = Convert.ToInt16(dr["RecordType"]),
                            ImageCount = Convert.ToInt16(dr["ImageCount"]),
                            Image01Name = Convert.ToString(dr["Image01Name"]),
                            Image02Name = Convert.ToString(dr["Image02Name"]),
                            Image03Name = Convert.ToString(dr["Image03Name"]),
                            Image04Name = Convert.ToString(dr["Image04Name"]),
                            Image05Name = Convert.ToString(dr["Image05Name"]),
                            Image06Name = Convert.ToString(dr["Image06Name"]),
                            Image07Name = Convert.ToString(dr["Image07Name"]),
                            Image08Name = Convert.ToString(dr["Image08Name"]),
                            Image09Name = Convert.ToString(dr["Image09Name"]),
                            Image10Name = Convert.ToString(dr["Image10Name"]),
                            SortDecision = Convert.ToInt16(dr["SortDecision"]),
                            LicensePlateNumber = Convert.ToString(dr["LicensePlateNumber"]),
                            LicensePlateProvinceID = Convert.ToInt16(dr["LicensePlateProvinceID"]),
                            LicensePlateImageName = Convert.ToString(dr["LicensePlateImageName"]),
                            LicensePlateConfidence = Convert.ToInt16(dr["LicensePlateConfidence"]),

                        };
                        run_no++;
                        lists.Add(model);
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
            return lists;
        }
    }
}
