﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOH.DC.Model
{
    public class Weighing
    {

        public String fldTransID { get; set; }
        public String fldRefID { get; set; }
        public DateTime fldTransDateTime { get; set; }
        public int fldTruckClass { get; set; }
        public String fldTruckDescription { get; set; }
        public String fldLimitedWeightGroup1 { get; set; }
        public String fldLimitedWeightGroup2 { get; set; }
        public String fldLimitedWeightGroup3 { get; set; }
        public double fldLimitedGVW { get; set; }
        public double fldWeightGVW { get; set; }
        public String fldComplyMethod { get; set; }
        public String fldLicenceNumber { get; set; }
        public String fldProvince { get; set; }
        public String fldMaterial { get; set; }
        public String fldDepartedFrom { get; set; }
        public String fldDestination { get; set; }
        public String fldNotes { get; set; }
        public String fldWeightGroup1 { get; set; }
        public String fldWeightGroup2 { get; set; }
        public String fldWeightGroup3 { get; set; }
        public int fldIsOverWeight { get; set; }
        public String fldWeightOver1 { get; set; }
        public String fldWeightOver2 { get; set; }
        public String fldWeightOver3 { get; set; }
        public String fldWeightOverGVW { get; set; }
        public String fldOperatorName { get; set; }
        public String fldTeamName { get; set; }
        public String fldPeriodID { get; set; }
        public String fldSaveMethod { get; set; }
        public String fldTruckImage { get; set; }
        public String fldClassImage { get; set; }
    }
}
