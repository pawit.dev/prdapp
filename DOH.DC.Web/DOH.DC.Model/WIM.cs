//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DOH.DC.Biz
{
    using System;
    using System.Collections.Generic;
    
    public partial class WIM
    {
        public long WIMID { get; set; }
        public Nullable<byte> StationID { get; set; }
        public Nullable<System.DateTime> TimeStamp { get; set; }
        public Nullable<int> VehicleNumber { get; set; }
        public Nullable<byte> Lane { get; set; }
        public Nullable<byte> Error { get; set; }
        public Nullable<int> StatusCode { get; set; }
        public Nullable<int> GVW { get; set; }
        public Nullable<int> MaxGVW { get; set; }
        public Nullable<double> ESAL { get; set; }
        public Nullable<byte> Speed { get; set; }
        public Nullable<byte> AxleCount { get; set; }
        public Nullable<short> Axle01Seperation { get; set; }
        public Nullable<short> Axle01Weight { get; set; }
        public Nullable<short> Axle01Max { get; set; }
        public Nullable<byte> Axle01Group { get; set; }
        public Nullable<byte> Axle01TireCode { get; set; }
        public Nullable<short> Axle02Seperation { get; set; }
        public Nullable<short> Axle02Weight { get; set; }
        public Nullable<short> Axle02Max { get; set; }
        public Nullable<byte> Axle02Group { get; set; }
        public Nullable<byte> Axle02TireCode { get; set; }
        public Nullable<short> Axle03Seperation { get; set; }
        public Nullable<short> Axle03Weight { get; set; }
        public Nullable<short> Axle03Max { get; set; }
        public Nullable<byte> Axle03Group { get; set; }
        public Nullable<byte> Axle03TireCode { get; set; }
        public Nullable<short> Axle04Seperation { get; set; }
        public Nullable<short> Axle04Weight { get; set; }
        public Nullable<short> Axle04Max { get; set; }
        public Nullable<byte> Axle04Group { get; set; }
        public Nullable<byte> Axle04TireCode { get; set; }
        public Nullable<short> Axle05Seperation { get; set; }
        public Nullable<short> Axle05Weight { get; set; }
        public Nullable<short> Axle05Max { get; set; }
        public Nullable<byte> Axle05Group { get; set; }
        public Nullable<byte> Axle05TireCode { get; set; }
        public Nullable<short> Axle06Seperation { get; set; }
        public Nullable<short> Axle06Weight { get; set; }
        public Nullable<short> Axle06Max { get; set; }
        public Nullable<byte> Axle06Group { get; set; }
        public Nullable<byte> Axle06TireCode { get; set; }
        public Nullable<short> Axle07Seperation { get; set; }
        public Nullable<short> Axle07Weight { get; set; }
        public Nullable<short> Axle07Max { get; set; }
        public Nullable<byte> Axle07Group { get; set; }
        public Nullable<byte> Axle07TireCode { get; set; }
        public Nullable<short> Axle08Seperation { get; set; }
        public Nullable<short> Axle08Weight { get; set; }
        public Nullable<short> Axle08Max { get; set; }
        public Nullable<byte> Axle08Group { get; set; }
        public Nullable<byte> Axle08TireCode { get; set; }
        public Nullable<short> Axle09Seperation { get; set; }
        public Nullable<short> Axle09Weight { get; set; }
        public Nullable<short> Axle09Max { get; set; }
        public Nullable<byte> Axle09Group { get; set; }
        public Nullable<byte> Axle09TireCode { get; set; }
        public Nullable<short> Axle10Seperation { get; set; }
        public Nullable<short> Axle10Weight { get; set; }
        public Nullable<short> Axle10Max { get; set; }
        public Nullable<byte> Axle10Group { get; set; }
        public Nullable<byte> Axle10TireCode { get; set; }
        public Nullable<short> Axle11Seperation { get; set; }
        public Nullable<short> Axle11Weight { get; set; }
        public Nullable<short> Axle11Max { get; set; }
        public Nullable<byte> Axle11Group { get; set; }
        public Nullable<byte> Axle11TireCode { get; set; }
        public Nullable<short> Axle12Seperation { get; set; }
        public Nullable<short> Axle12Weight { get; set; }
        public Nullable<short> Axle12Max { get; set; }
        public Nullable<byte> Axle12Group { get; set; }
        public Nullable<byte> Axle12TireCode { get; set; }
        public Nullable<short> Axle13Seperation { get; set; }
        public Nullable<short> Axle13Weight { get; set; }
        public Nullable<short> Axle13Max { get; set; }
        public Nullable<byte> Axle13Group { get; set; }
        public Nullable<byte> Axle13TireCode { get; set; }
        public Nullable<short> Axle14Seperation { get; set; }
        public Nullable<short> Axle14Weight { get; set; }
        public Nullable<short> Axle14Max { get; set; }
        public Nullable<byte> Axle14Group { get; set; }
        public Nullable<byte> Axle14TireCode { get; set; }
        public Nullable<short> Length { get; set; }
        public Nullable<short> FrontOverHang { get; set; }
        public Nullable<short> RearOverHang { get; set; }
        public Nullable<byte> VehicleType { get; set; }
        public Nullable<byte> VehicleClass { get; set; }
        public Nullable<byte> RecordType { get; set; }
        public Nullable<byte> ImageCount { get; set; }
        public string Image01Name { get; set; }
        public string Image02Name { get; set; }
        public string Image03Name { get; set; }
        public string Image04Name { get; set; }
        public string Image05Name { get; set; }
        public string Image06Name { get; set; }
        public string Image07Name { get; set; }
        public string Image08Name { get; set; }
        public string Image09Name { get; set; }
        public string Image10Name { get; set; }
        public Nullable<byte> SortDecision { get; set; }
        public string LicensePlateNumber { get; set; }
        public Nullable<byte> LicensePlateProvinceID { get; set; }
        public string LicensePlateImageName { get; set; }
        public Nullable<byte> LicensePlateConfidence { get; set; }
    }
}
