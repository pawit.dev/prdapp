﻿

setInterval(getWimLane1, 2000);
setInterval(getWimLane2, 2000);
//setInterval(getStatic, 2000);

function getWimLane1() {
    $.ajax({
        type: "POST",
        url: "LiveAjax.aspx/getWimLane1Live",
//        data: JSON.stringify({ name: $('#hStationID').val() }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnWimLane1Success,
        failure: function (response) {
            alert(response.d);
        }
    });
}

function getWimLane2() {
    $.ajax({
        type: "POST",
        url: "LiveAjax.aspx/getWimLane2Live",
        //        data: '{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnWimLane2Success,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function getStatic() {
    $.ajax({
        type: "POST",
        url: "LiveAjax.aspx/getStaticLive",
        //        data: '{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnStaticSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}

function OnWimLane1Success(response) {
    $("#tbReport1").html(response.d);
}
function OnWimLane2Success(response) {
    $("#tbReport2").html(response.d);
}
function OnStaticSuccess(response) {
    $("#tbReport3").html(response.d);
}




