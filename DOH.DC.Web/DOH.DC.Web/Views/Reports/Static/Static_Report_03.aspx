﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Static_Report_03.aspx.cs" Inherits="DOH.DC.Web.Views.Reports.Static.Static_Report_03" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="well">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#divSearch" data-toggle="tab">
                <asp:Label ID="lbTitle" runat="server" Text=""></asp:Label></a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane active in" id="divSearch">
                <table class="table-search">
                    <tr>
                        <td>
                            <label>
                                สถานี</label>
                        </td>
                        <td width="300px" style="width: 150px">
                            <asp:DropDownList ID="ddlStation" runat="server" CssClass="input-xlarge" DataTextField="StationName"
                                DataValueField="StationID" AppendDataBoundItems="true" TabIndex="1" AutoPostBack="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbFrom" runat="server" Text="วัน"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="dtDataDate" runat="server" AutoPostBack="True"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="dtDataDate">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="ผลัด"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="comboBox_Shift" runat="server" CssClass="input-xlarge" DataTextField="ShiftDescription"
                                DataValueField="ShiftID" AppendDataBoundItems="true" TabIndex="1" AutoPostBack="True" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="label_status" runat="server" Text=""></asp:Label>
                <div class="submit">
                    <asp:LinkButton ID="btnOK" runat="server" CssClass="btn" TabIndex="10" OnClick="btnOK_Click"><i class="icon-search"></i> OK</asp:LinkButton>
                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn" Text="Clear" TabIndex="11"
                        OnClick="btnCancel_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
