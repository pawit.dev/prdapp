﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Static_Report_01.aspx.cs" Inherits="DOH.DC.Web.Views.Reports.Static.Static_Report_01" %>

<%@ Register Src="~/UserControls/DateTimeSelection.ascx" TagPrefix="asp" TagName="DateTimeSelection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:DateTimeSelection runat="server" ID="dtpDateTimeSelection" CssStyle="input-mlarge"
                FormatType="Date" />
            <asp:Label ID="label_status" runat="server" Text=""></asp:Label>
            <div class="submit">
                <asp:LinkButton ID="btnOK" runat="server" CssClass="btn" TabIndex="10" OnClick="btnOK_Click"><i class="icon-search"></i> OK</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn" Text="Clear" TabIndex="11"
                    OnClick="btnCancel_Click" />
            </div>
            <br />
            <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                CssClass="table" ShowHeaderWhenEmpty="True" DataKeyNames="fldTransID" OnRowCommand="gvResult_RowCommand"
                OnPageIndexChanging="gvResult_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnInfo" runat="server" ToolTip="Info" CommandName="View" CommandArgument='<%#  Eval("fldTransID")%>'><i class="icon-zoom-in"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No." ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="วันเวลา" DataField="fldTransDateTime" DataFormatString="{0:dd-MM-yyyy}"
                        ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText=" ประเภท" DataField="fldTruckClass" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField HeaderText=" นน" DataField="fldLimitedGVW" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField HeaderText=" นน.วัด" DataField="fldWeightGVW" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField HeaderText=" ทะเบียน" DataField="fldLicenceNumber" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField HeaderText=" จังหวัด" DataField="fldProvince" ItemStyle-HorizontalAlign="Left" />
                </Columns>
                <PagerTemplate>
                    <div class="pagination">
                        <ul>
                            <li>
                                <asp:LinkButton ID="btnPrev" runat="server" CssClass="btnArrowBack" CommandName="Page"
                                    CommandArgument="Prev" CausesValidation="false" Text="Prev" ToolTip="Previous Page" />
                            </li>
                            <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                            <li>
                                <asp:LinkButton ID="btnNext" runat="server" CssClass="btnArrowNext" CommandName="Page"
                                    CommandArgument="Next" CausesValidation="false" Text="Next" ToolTip="Next Page" />
                            </li>
                        </ul>
                    </div>
                </PagerTemplate>
                <EmptyDataTemplate>
                    <div class="data-not-found">
                        <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
