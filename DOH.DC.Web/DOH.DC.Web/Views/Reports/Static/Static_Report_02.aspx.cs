﻿using System;
using System.Web.UI;
using System.Data;
using DOH.DC.Dao;
using CrystalDecisions.CrystalReports.Engine;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Reports.Static
{
    public partial class Static_Report_02 : System.Web.UI.Page
    {

        DataTable weighingRecord = new DataTable();
        // Report Parameters
        string param_StationName = string.Empty;
        string param_UserFullName = string.Empty;
        string param_WeightType = string.Empty;
        string param_TimeBegin = string.Empty;
        string param_TimeEnd = string.Empty;
        string param_FilterTitle = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/wim/Login.aspx");

            if (!Page.IsPostBack)
            {
                initial();
            }
        }
        private void initial()
        {
            dtpDateTimeSelection.setTitle("STATIC SCALE - รายงานรถเข้าด่านชั่งตามตัวกรอง");
        }
        protected void btnOK_Click(object sender, EventArgs e)
        {

            dtpDateTimeSelection.Summarize();

            param_StationName = dtpDateTimeSelection.Param_StationName;
            param_UserFullName = dtpDateTimeSelection.Param_UserFullName;
            param_FilterTitle = dtpDateTimeSelection.Param_FilterTitle;
            param_WeightType = dtpDateTimeSelection.Param_WeightType;
            param_TimeBegin = dtpDateTimeSelection.Param_TimeBegin;
            param_TimeEnd = dtpDateTimeSelection.Param_TimeEnd;

            try
            {
                weighingRecord = WDCWeighingDataOLDSchema.GetWeighingView_By_Variables(dtpDateTimeSelection.startDate,
                    dtpDateTimeSelection.endDate, dtpDateTimeSelection.IsWeightOver, dtpDateTimeSelection.UserID, dtpDateTimeSelection.StationID);

            }
            catch (Exception)
            {
                // MessageBox.Show("ERROR Loading data for Datalist report\r\n\r\n" + ex.Message);
            }

            if (weighingRecord.Rows.Count == 0)
            {
                //isToClose = false;
                label_status.Text = "ไม่พบข้อมูล";

            }
            else
            {
                try
                {

                    ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("~/ReportObjects/Static/RPT_DataList_Filtered.rpt"));

                    int n1 = 0;
                    int n2 = 0;
                    foreach (DataRow r in weighingRecord.Rows)
                    {
                        // Overrided
                        r["fldWeightGroup1"] = r["fldGVW_Weight_WIM"];

                        if (r["fldIsOverWeight"].ToString().StartsWith("Y"))
                        {
                            n1 = n1 + 1;
                        }
                        else
                        {
                            n2 = n2 + 1;
                        }
                    }

                    report.SetDataSource(weighingRecord);
                    report.SetParameterValue("SiteName", dtpDateTimeSelection.Param_StationName);
                    report.SetParameterValue("UserName", param_UserFullName);
                    report.SetParameterValue("WeightType", param_WeightType);
                    report.SetParameterValue("TimeBegin", param_TimeBegin);
                    report.SetParameterValue("TimeEnd", param_TimeEnd);
                    report.SetParameterValue("TruckCount", n1 + n2);
                    report.SetParameterValue("OverWeightTruck", n1);
                    report.SetParameterValue("NotOverWeightTruck", n2);
                    report.SetParameterValue("FilteredTime", param_FilterTitle);

                    Session["RptDoc"] = report;
                    Response.Redirect("ReportStaticViewer.aspx");
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            dtpDateTimeSelection.clear();
        }

    }
}