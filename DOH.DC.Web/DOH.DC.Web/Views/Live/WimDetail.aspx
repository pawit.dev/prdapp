﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="WimDetail.aspx.cs"
    Inherits="DOH.DC.Web.Views.Live.WimDetail" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head id="Head1" runat="server">
    <title>- กรมทางหลวง -</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, chrome=1" />
    <link rel="Stylesheet" href="/dc/Styles/Input.css" />
    <link rel="Stylesheet" href="/dc/Styles/Main.css" />
    <link rel="Stylesheet" href="/dc/Scripts/jquery-ui-1.10.3/themes/base/jquery-ui.css" />
    <link rel="Stylesheet" href="/dc/Styles/jquery.dropdown.css" />
    <link rel="stylesheet" href="/dc/Styles/colorbox.css" />
    <script type="text/javascript" src="/dc/Scripts/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="/dc/Scripts/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
    <script type="text/javascript" src="/dc/Scripts/jquery.dropdown.js"></script>
    <script type="text/javascript" src="/dc/Scripts/jquery.colorbox.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".pic1").colorbox({ rel: 'pic1' });
        });
    </script>
    <style type="text/css">
        <!-- CSS goes in the document HEAD or added to your external stylesheet -->
       table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #666666;
            background-color: #dedede;
        }

        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #666666;
            background-color: #ffffff;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <fieldset>
            <%if (model != null)
              { %>
            <table>
                <tr>
                    <%
              if (model.GVW > model.MaxGVW)
              { 
                    %>
                    <td bgcolor="#FF0000">&nbsp;
                    </td>
                    <%
              }
              else
              {                    
                    %>
                    <td bgcolor="#42D756">&nbsp;
                    </td>
                    <%} %>
                    <td width="15%" style="padding: 10px;">
                        <%--    <a href="<%= model.Image01Name %>" class="pic1" title="Img Front">--%>
                        <img alt="" border="0" src="<%= model.Image01Name%>" width="200" />

                        <%--     </a>--%>
                        <br />
                        <br />
                        <%--     <a href="<%= model.Image02Name %>" class="pic1" title="Img Front">--%>
                        <img alt="" border="0" src="<%= model.Image02Name%>" width="200" />

                        <%--         </a>--%>
                    </td>
                    <td valign="top">
                        <table width="100%" class="tbl1">
                            <tr>
                                <th class="style9">&nbsp;
                                </th>
                                <th class="style3">&nbsp;
                                </th>
                                <th class="style9">&nbsp;
                                </th>
                                <th class="style11" style="text-align: left">&nbsp;
                                </th>
                            </tr>
                            <tr>
                                <td class="style7">
                                    <strong>WIMID</strong>
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_wimid" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="style9">
                                    <strong>Wim Gvw </strong>
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_wimGvw" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    <strong style="text-align: center">Station</strong>
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_Station" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="style9">
                                    <strong>Max Gvw</strong>
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_MaxGvw" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    <strong style="text-align: center">Date</strong>
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_Date" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="style8">Length
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_Length" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    <strong style="text-align: center">Time</strong>
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_Time" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="style9">Front Over
                                </td>
                                <td class="style3">
                                    <asp:Label ID="l_FrontOver" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8">
                                    <strong style="text-align: center">Vehicle Number</strong>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_VehicleNumber" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="style9">Rear Over
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_RearOver" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8">
                                    <strong style="text-align: center">Lane</strong>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_lane" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="ui-priority-primary">ESAL
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_esal" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8">
                                    <strong style="text-align: center">Vehicle Class</strong>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_VehicleClass" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="ui-priority-primary">Error
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_error" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8">
                                    <strong style="text-align: center">Axle Count</strong>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_AxleCount" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="ui-priority-primary">Sort Decision
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_sortDecision" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8">
                                    <strong style="text-align: center">Speed</strong>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_Speed" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="ui-priority-primary">Status
                                </td>
                                <td class="style4">
                                    <asp:Label ID="l_Status" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8">&nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                <td class="style10">&nbsp;
                                </td>
                                <td class="number">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="style2">
                                    <img alt="" border="0" src="<%= model.classImg%>" width="190" />
                                </td>
                                <td class="style2">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <%--                            <td colspan="3">
                                &nbsp;&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/pdf.png"
                                    OnClick="ImageButton1_Click" Style="width: 16px" Visible="False" />
                            </td>--%>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="gridtable">
                                        <tr>
                                            <th>Axle</th>
                                            <th>1</th>
                                            <th>2</th>
                                            <th>3</th>
                                            <th>4</th>
                                            <th>5</th>
                                            <th>6</th>
                                            <th>7</th>
                                        </tr>
                                        <tr>
                                            <td>Seperation</td>
                                            <td>
                                                <asp:Label ID="lbSep1" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbSep2" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbSep3" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbSep4" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbSep5" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbSep6" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbSep7" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Group</td>
                                            <td>
                                                <asp:Label ID="lbGroup1" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbGroup2" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbGroup3" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbGroup4" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbGroup5" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbGroup6" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbGroup7" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Weight</td>
                                            <td>
                                                <asp:Label ID="lbWeight1" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbWeight2" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbWeight3" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbWeight4" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbWeight5" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbWeight6" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbWeight7" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}
              else
              { %>Not found data.
        <%} %>
        </fieldset>
    </form>
</body>
</html>
