﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrameLive.aspx.cs" Inherits="DOH.DC.Web.Views.Live.FrameLive" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link rel="stylesheet" href="../../Styles/colorbox.css" />
    <script type="text/javascript" src="../../Scripts/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Scripts/LiveWeighing.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.colorbox.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".pic1").colorbox({ rel: 'pic1' });
        });
    </script>
</head>
<body >
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    Station :
                    <asp:Label ID="lbStation" runat="server" Text="-"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    LANE: WIM 1
                </td>
                <td>
                    LANE: WIM 2
                </td>
            </tr>
            <tr>
                <td>
                    <div id="tbReport1" style="width:530px; height: 768px; overflow-y: scroll;">
                    </div>
                </td>
                <td>
                    <div id="tbReport2" style="width: 530px; height: 768px; overflow-y: scroll;">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
