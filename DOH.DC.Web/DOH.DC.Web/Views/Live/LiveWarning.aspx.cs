﻿using System;

namespace DOH.DC.Web.Views.Live
{
    public partial class LiveWarning : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.myIframe.Attributes["src"] = "FrameWarningLive.aspx";
        }
    }
}