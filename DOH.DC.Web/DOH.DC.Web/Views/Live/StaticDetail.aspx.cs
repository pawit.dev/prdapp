﻿using System;
using System.Web;
using System.Web.UI;
using DOH.DC.Dao;
using DOH.DC.Model;
using System.IO;
using DOH.DC.Utils;

namespace DOH.DC.Web.Views.Live
{
    public partial class StaticDetail : System.Web.UI.Page
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(StaticDetail));

        protected View_STATIC model
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                String id = Request.QueryString["ENFID"];

                model = WeighingDao.getById(id);
                if (model != null)
                {
                    String imgPath = "";
                    if (!String.IsNullOrEmpty(Path.GetExtension(model.LPR_ImageName)))
                    {
                        string[] imgArgs = model.LPR_ImageName.ToString().Split('-');
                        String ipAddr = (String)HttpContext.Current.Session["RequestIP"];


                        if (ipAddr.Substring(0, 12).Equals("192.168.100."))
                        {
                            imgPath = String.Format(GlobalConfiguration.imageWebPathLocal, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), "STATIC", model.LPR_ImageName);
                        }
                        else
                        {
                            imgPath = String.Format(GlobalConfiguration.ImageWebPath, imgArgs[1], imgArgs[2].Substring(0, 4), imgArgs[2].Substring(4, 2), imgArgs[2].Substring(6, 2), "STATIC", model.LPR_ImageName);
                        }
                        
                    }
                    else
                    {
                        imgPath = "http://" + HttpContext.Current.Request.Url.Authority + "/dc/images/inf.jpg";
                    }

                    model.LPR_ImageName = imgPath;
                    //model.LicensePlateImageName = model.Image01Name;
                    model.classImg = "http://" + HttpContext.Current.Request.Url.Authority + "/dc/images/vclass/class" + model.VehicleClassID + ".jpg";

                    l_enfid.Text = model.ENFID + "";
                    l_Station.Text = model.StationName;
                    l_Date.Text = model.TimeStamp.ToShortDateString() + "";
                    l_Time.Text = model.TimeStamp.ToShortTimeString() + "";
                    l_SeqNumber.Text = model.SeqNumber + "";
                    l_User.Text = model.LoginID + "";
                    l_VehicleClass.Text = model.VehicleClassID + "";
                    l_LpNumber.Text = model.LPR_Number + "";
                    l_Province.Text = model.ProvinceName + "";
                    l_ComplyMethod.Text = model.ComplyMethod + "";


                    l_wimid.Text = model.WIMID + "";
                    l_WimGvw.Text = model.GVW_Weight_WIM + "";
                    l_StaticGvw.Text = model.GVW_Weight_Measured + "";
                    l_MaxGvw.Text = model.GVW_Weight_Max + "";
                    l_Status.Text = "";
                    l_Barrier.Text = model.IsBGOpened + "";
                    l_good.Text = "";
                    l_Departed.Text = model.DepartedFrom + "";
                    l_Destination.Text = model.Destination + "";
                    l_Status.Text = model.Notes + "";
                }
                else
                {
                    logger.Debug("WIMID IS NULL.");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}