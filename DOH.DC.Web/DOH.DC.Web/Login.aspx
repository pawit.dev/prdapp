﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DOH.DC.Web.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" type="text/css" href="/wim/Styles/login.css" media="screen, projection" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>- กรมทางหลวง -</title>
    <script language="javascript" type="text/javascript">
// <![CDATA[


// ]]>
    </script>
</head>
<body>
    <div id="top-wrapper">
        <div class="main-panel inner">
            <div id="main-logo">
                <img alt="" src="images/logo.png" />
            </div>
        </div>
    </div>
    <div id="main-wrapper">
        <div id="main" class="inner">
            <!-- Content -->
            <form action="" runat="server">
            <div id="wrongpassword" runat="server" visible="false" style="color: Red; font-size: 13px;
                position: absolute; top: 269px; left: 392px">
                Invalid Username Or Password
            </div>
            <input type="text" name="username" id="txtUsername" tabindex="0" />&nbsp;
            <input type="checkbox" id="chkRemember" />
            <input type="submit" name="submit" value="Login" id="btnLogin" />
            <input type="password" name="password" id="txtPassword" tabindex="1" /></form>
            <!-- End Content -->
        </div>
    </div>
    <div id="bottom-wrapper">
        <div class="inner" id="bottom-footer">
            © 2010 Department Of Highways.</div>
    </div>
</body>
</html>
