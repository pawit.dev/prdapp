﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimeSelection_2.ascx.cs"
    Inherits="DOH.DC.Web.UserControl.DateTimeSelection_2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="well">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#divSearch" data-toggle="tab">
            <asp:Label ID="lbTitle" runat="server" Text=""></asp:Label></a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane active in" id="divSearch">
            <table class="table-search">
                <tr>
                    <td>
                        <label>
                            สถานี</label>
                    </td>
                    <td width="300px" style="width: 150px">
                        <asp:DropDownList ID="ddlStation" runat="server" CssClass="input-xlarge" DataTextField="StationName"
                            DataValueField="StationID" AppendDataBoundItems="true" TabIndex="1" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlStation_SelectedIndexChanged" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbFrom0" runat="server" Text="ช่วงเวลา"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rbPeriod" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rbPeriod_SelectedIndexChanged"
                            RepeatDirection="Horizontal" Width="529px">
                            <asp:ListItem Selected="True" Value="0">วัน</asp:ListItem>
                            <asp:ListItem Value="1">สัปดาห์</asp:ListItem>
                            <asp:ListItem Value="2">เดือน</asp:ListItem>
                            <asp:ListItem Value="3">ปี</asp:ListItem>
                            <asp:ListItem Value="4" Selected="True">เลือกเอง</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbFrom" runat="server" Text="วัน"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="dtFrom" runat="server" AutoPostBack="True" 
                            ontextchanged="dtFrom_TextChanged"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="dtFrom">
                        </asp:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbTo" runat="server" Text="ถึง"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="dtTo" runat="server" AutoPostBack="True" 
                            ontextchanged="dtTo_TextChanged"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="dtTo">
                        </asp:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:LinkButton ID="llbPrev10" runat="server" OnClick="llbPrev10_Click">&lt;&lt; 10</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="llbPrevWeek" runat="server" OnClick="llbPrevWeek_Click">ก่อนหน้า</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="llbNextWeek" runat="server" OnClick="llbNextWeek_Click">ถัดไป</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="llbNext10" runat="server" OnClick="llbNext10_Click">10 &gt;&gt;</asp:LinkButton>
                    </td>
                </tr>

            </table>
        </div>
    </div>
</div>
