﻿
using DOH.DC.Dao;
using DOH.DC.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;

namespace DOH.DC.Service
{


    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        [WebMethod(CacheDuration = 60)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetWimByLane(string _lane,string _station_id)
        {


            LiveViewDao dao = new LiveViewDao();
            List<WimModel> wims = dao.getWimByLane(new DataFilters { lane = _lane,stationID = _station_id });

            return JsonConvert.SerializeObject(wims);
        }


        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
    }
}