﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;


namespace DOH.DC.Utils
{
    public class Function
    {
        private const String NUMBER_FORMAT_1 = "{0:#,##}";

        public static String customNumberFormat(String _value)
        {

            if (_value == null) return "";
            if (_value == "null") return "";
            if (_value.Equals(String.Empty)) return "";
            return String.Format(CultureInfo.InvariantCulture,
                        NUMBER_FORMAT_1, Convert.ToDouble(_value));
        }

        public static DataTable GetDataTable(string SQLText)
        {
            DataTable table = new DataTable();
            try
            {
                using (SqlConnection connection = new SqlConnection(GlobalConfiguration.ConnectionString))
                {
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(SQLText, connection))
                    {
                        adapter.Fill(table);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(""+ex.Message);
                //Messaging.LogAndShow("GetDataTable", "Exception_GetDataTable: " +
                //    "\r\n\r\n" + ex.Message +
                //    //  "\r\n\r\n" + AppParameters.ConnectionString +
                //    "\r\n\r\n" + SQLText);
            }
            GC.Collect();
            return table;
        }

        //public static bool ExecuteSQLCommand(string SQLText, out object output)
        //{
        //    bool success = true;
        //    try
        //    {
        //        using (SqlConnection connection = new SqlConnection(AppParameters.ConnectionString))
        //        {
        //            connection.Open();
        //            using (SqlCommand command = new SqlCommand(SQLText, connection))
        //            {
        //                output = command.ExecuteScalar();
        //                success = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        success = false;
        //        output = ex;
        //    }

        //    return success;
        //}

        //public static string GetImageFolder(string imageName)
        //{
        //    string imageFolder = string.Empty;
        //    string appname = string.Empty;

        //    if (imageName.StartsWith("rsd"))
        //    {
        //        appname = "RSD";
        //    }
        //    else if (imageName.StartsWith("lpr"))
        //    {
        //        appname = "STATIC";
        //    }
        //    else if (imageName.StartsWith("wim"))
        //    {
        //        appname = "WIM";
        //    }

        //    if (imageName.EndsWith("jpg"))
        //    {
        //        if (imageName.Length > 15)
        //        {
        //            string siteID = imageName.Substring(4, 2);
        //            string y = imageName.Substring(7, 4);
        //            string m = imageName.Substring(11, 2);
        //            string d = imageName.Substring(13, 2);

        //            string subFolder = siteID + @"\" + y + @"\" + m + @"\" + d;
        //            imageFolder = AppParameters.DataLocation + @"\IMAGES\" + subFolder + @"\" + appname;
        //        }
        //    }

        //    return imageFolder;
        //}

        //public static string GetStatusCodeText(VRStatusCode status)
        //{
        //    string text = string.Empty;
        //    foreach (VRStatusCode val in Enum.GetValues(typeof(VRStatusCode)))
        //    {
        //        if (val != VRStatusCode.StatusClear)
        //        {
        //            if ((val & status) == val)
        //            {
        //                text += (text.Length > 0) ? "\r\n" : "";
        //                text += val.ToString();
        //            }
        //        }
        //    }
        //    return text;
        //}

        //public static string GetAxleGroupText(byte groupType)
        //{
        //    if (groupType == 1)
        //    {
        //        return "single";
        //    }
        //    else if (groupType == 2)
        //    {
        //        return "tandem";
        //    }
        //    else if (groupType == 3)
        //    {
        //        return "tridem";
        //    }
        //    else if (groupType == 4)
        //    {
        //        return "quadrem";
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}

        //public static string GetLaneName(byte lane)
        //{
        //    if (lane == 0)
        //    {
        //        return "WIM"; // lane 1
        //    }
        //    else if (lane == 1)
        //    {
        //        return "Report"; // lane 2
        //    }
        //    else if (lane == 2)
        //    {
        //        return "Bypass"; // lane 3
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}

        //public static string GetLaneSequence(byte lane)
        //{
        //    return (lane + 1).ToString();
        //}

        //public static Color GetSortDecisionColorCode(VRSortDecisionCode sortDecision)
        //{
        //    Color green = Color.LawnGreen;
        //    Color red = Color.Crimson;
        //    Color gray = Color.Silver;
        //    Color orange = Color.Orange;

        //    if (sortDecision == VRSortDecisionCode.Bypass)
        //    {
        //        return green;
        //    }
        //    else if (sortDecision == VRSortDecisionCode.CredentialReport)
        //    {
        //        return red;
        //    }
        //    else if (sortDecision == VRSortDecisionCode.Off)
        //    {
        //        return gray;
        //    }
        //    else if (sortDecision == VRSortDecisionCode.Report)
        //    {
        //        return red;
        //    }
        //    else if (sortDecision == VRSortDecisionCode.Sorting)
        //    {
        //        return orange;
        //    }
        //    else
        //    {
        //        return gray;
        //    }
        //}

        //public static Color GetIsOverWeightColorCode(string status)
        //{
        //    Color green = Color.LawnGreen;
        //    Color red = Color.Crimson;
        //    Color gray = Color.Silver;

        //    if (status.ToUpper() == "Y")
        //    {
        //        return red;
        //    }
        //    else if (status.ToUpper() == "N")
        //    {
        //        return green;
        //    }
        //    else
        //    {
        //        return gray;
        //    }

        //}

        //public static Color GetWarningTypeColorCode(int Type)
        //{
        //    Color avoiding = Color.Yellow;
        //    Color runningscale = Color.Orange;
        //    Color overweight = Color.Crimson;
        //    Color bgopened = Color.BlueViolet;

        //    if (Type == 1)
        //    {
        //        return avoiding;
        //    }
        //    else if (Type == 2)
        //    {
        //        return runningscale;
        //    }
        //    else if (Type == 3)
        //    {
        //        return overweight;
        //    }
        //    else if (Type == 4)
        //    {
        //        return bgopened;
        //    }
        //    else
        //    {
        //        return Color.Gray;
        //    }

        //}

        //public static string GetStationName(int stationID)
        //{
        //    string name = string.Empty;
        //    DataTable table = GetDataTable("SELECT * FROM Station");
        //    if (table.Rows.Count > 0)
        //    {
        //        foreach (DataRow r in table.Rows)
        //        {
        //            if (stationID == (int)r["StationID"])
        //            {
        //                name = (string)r["StationName"];
        //                break;
        //            }
        //        }
        //    }
        //    return name;
        //}

        //public static string GetDateTimeString(DateTime date, DateTime time)
        //{
        //    int y = date.Year;
        //    int M = date.Month;
        //    int d = date.Day;
        //    int h = time.Hour;
        //    int m = time.Minute;
        //    return new DateTime(y, M, d, h, m, 0).ToString("s");
        //}

        public static byte[] getImageByte(String path)
        {

            FileStream fiStream = new FileStream(path, FileMode.Open);
            BinaryReader binReader = new BinaryReader(fiStream);

            byte[] pic = { };

            pic = binReader.ReadBytes((int)fiStream.Length);
            return pic;
        }

        public static String getReportPrefix(String reportType)
        {
            String reportFormat = "";
            switch (reportType)
            {
                case "2"://Bar Chart
                    reportFormat = "_Chart_Bar";
                    break;
                case "3"://Line Chart
                    reportFormat = "_Chart_Line";
                    break;
                default:
                    break;
            }
            return reportFormat;
        }

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    
    
    }
}
