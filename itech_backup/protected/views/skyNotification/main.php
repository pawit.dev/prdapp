<div class="module-head">SKY Notification</div>
<div>
	<?php 
	if(UserLoginUtil::hasPermission(array("FULL_ADMIN"))){
	}
	?>

	<div class="search-box">
		<?php 
		$form = $this->beginWidget('CActiveForm', array(
				'id' => 'equipment-type-form',
				'method'=>'get',
				'action'=>'',
				'enableAjaxValidation' => false,
		));
		?>
		<input type="text" name="search_text"
			value="<?php echo $_GET['search_text']?>">
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Key',
						'value'=>'$data->request_key',
				),
				array(
						'header'=>'Create Date',
						'value'=>'$data->create_date',
				),
				array(
						'header'=>'Status',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'value'=>' $data->status',
				),
				array(
						'header'=>'Action',
						'class'=>'CButtonColumn',
						'template'=>'{active}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'active' => array
								(
										'imageUrl'=>Yii::app()->request->baseUrl.'/images/active.png',
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN"))',
										'url'=>'Yii::app()->createUrl("SkyNotification/loadData", array("id"=>$data->id))',
								)
						),
				),
		),
));
?>
