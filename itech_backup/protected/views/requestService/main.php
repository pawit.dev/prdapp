﻿
<script type="text/javascript">
$(function(){
	$('#month_filter').val('<?php echo date('m') *1?>');
	$('#day_filter').val('<?php echo date('d') *1?>');
	//filter();	
});
function filter(){
	var data = '';
	if($('#year_filter').val() != '') {
		data = 'year_filter='+$('#year_filter').val();
		if($('#month_filter').html() == '<option value="">- All Month -</option>') {
			$('#month_filter').html('');
			$('#month_filter').append('<option value="">- All Month -</option>');
			$('#month_filter').append('<option value="1">January</option>');
			$('#month_filter').append('<option value="2">February</option>');
			$('#month_filter').append('<option value="3">March</option>');
			$('#month_filter').append('<option value="4">April</option>');
			$('#month_filter').append('<option value="5">May</option>');
			$('#month_filter').append('<option value="6">June</option>');
			$('#month_filter').append('<option value="7">July</option>');
			$('#month_filter').append('<option value="8">August</option>');
			$('#month_filter').append('<option value="9">September</option>');
			$('#month_filter').append('<option value="10">October</option>');
			$('#month_filter').append('<option value="11">November</option>');
			$('#month_filter').append('<option value="12">December</option>');
		}
	} else {
		$('#month_filter').html('<option value="">- All Month -</option>');
		$('#day_filter').html('<option value="">- All Day -</option>');
		data = 'year_filter=';
	}
	/*
	var dayValue = '';
	if($('#day_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'day_filter='+$('#day_filter').val();
		dayValue = $('#day_filter').val();
	}
	
	if($('#month_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'month_filter='+$('#month_filter').val();
		$('#day_filter').html('<option value="">- All Day -</option>');

		var endDayOfMonth = 31;
		if($('#month_filter').val() == '4' || $('#month_filter').val() == '6' || $('#month_filter').val() == '9' || $('#month_filter').val() == '11') {
			endDayOfMonth = 30;
		}
		if($('#month_filter').val() == '2') {
			var year = parseInt($('#year_filter').val());
			if(year % 4 == 0) {
				endDayOfMonth = 29;
			} else {
				endDayOfMonth = 28;
			}
		}
		for(var i = 1; i <= endDayOfMonth; i++) {
			$('#day_filter').append('<option value="' + i + '">' + i + '</option>');
		}		
	} else {
		$('#day_filter').html('<option value="">- All Day -</option>');
		if(data != ''){
			data = data + '&';
		}
		data = data + 'month_filter=';
	}

	if(dayValue != '') {
		if(data != ''){
			data = data + '&';
		}
		$("#day_filter option[value='" + dayValue + "']").attr("selected", "selected");
	} else {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'day_filter=';
	}
*/
	if($('#request_type_filter').val() != '') {
		if(data != ''){
			data = data + '&';
		}
		data = data + 'request_type_filter='+$('#request_type_filter').val();			
	}
	$('#my-model-grid').yiiGridView('update', {url : '<?php echo Yii::app()->createUrl('RequestService/Index')?>/ajax/my-model-grid', data: data});
	$('#my-model-grid2').yiiGridView('update', {url : '<?php echo Yii::app()->createUrl('RequestService/Index')?>/ajax/my-model-grid2', data: data});
	}
</script>
<div>
<?php 
$requestTypes = RequestServiceType::model()->findAll();
$requestStatuses = Status::model()->findAll(array('condition'=>"t.status_group_id='REQUEST_ISERVICE_STATUS'"));
?>
<div class="filter">
		<b>Filter</b><select name="request_type_filter" id="request_type_filter"
			onchange="filter()"><option value="">- All Type -</option>
			<?php 
			foreach($requestTypes as $requestType) {
				?>
			<option value="<?php echo $requestType->id?>">
				<?php echo $requestType->name?>
			</option>
			<?php }?>
		</select> <select name="year_filter" id="year_filter"
			onchange="filter()">
			<option value="">All Year</option>
			<?php 
			for($i = date("Y"); $i < (date("Y") + 5); $i++) {
			?>
			<option value="<?php echo $i?>" <?php echo $i == date("Y") ? 'selected="selected"' : ''?>>
				<?php echo $i?>
			</option>
			<?php }?>
		</select> <select name="month_filter" id="month_filter"
			onchange="filter()"><option value="">- All Month -</option>
			<option value="1">January</option>
			<option value="2">February</option>
			<option value="3">March</option>
			<option value="4">April</option>
			<option value="5">May</option>
			<option value="6">June</option>
			<option value="7">July</option>
			<option value="8">August</option>
			<option value="9">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
			</select>
	</div>
	<div class="clear"></div>
</div>
<br>
<span class="module-head">On Going Request</span>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid2',
		'dataProvider' => $data2->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Service Type',
						'value'=>'RequestUtil::getAllRequestServiceTypeName($data->id)',
				),
				array(
						'header'=>'Requested Date',
						'value'=>'DateTimeUtil::getDateFormat($data->create_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(
						'header'=>'Due Date',
						'value'=>'DateTimeUtil::getDateFormat($data->due_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(
						'header'=>'Status',
						'value'=>'$data->status->name;',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'template'=>'{file} {view} {update} {delete}',
						'htmlOptions'=>array('width'=>'13%', 'align'=>'center'),
						'buttons'=>array
						(
								'view' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_SERVICE", "VIEW_ALL_REQUEST_SERVICE"))',
								),
								'update' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_SERVICE"))',
								),
								'delete' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "DELETE_REQUEST_SERVICE"))',
								),
								'file' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_SERVICE")) && RequestUtil::hasRequestServiceFile("$data->id")',
										'imageUrl' => Yii::app()->request->baseUrl.'/images/attach.jpg',
										'url' => 'Yii::app()->request->baseUrl."/".RequestUtil::getRequestServiceFilePath($data->id)',
								),
						),
				),
		),
));
?>
<br>
<span class="module-head">Completed Request</span>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Service Type',
						'value'=>'RequestUtil::getAllRequestServiceTypeName($data->id)',
				),
				array(
						'header'=>'Requested Date',
						'value'=>'DateTimeUtil::getDateFormat($data->create_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(
						'header'=>'Due Date',
						'value'=>'DateTimeUtil::getDateFormat($data->due_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(
						'header'=>'Status',
						'value'=>'$data->status->name;',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'template'=>'{file} {view} {update} {delete}',
						'htmlOptions'=>array('width'=>'13%', 'align'=>'center'),
						'buttons'=>array
						(
								'view' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_SERVICE", "VIEW_ALL_REQUEST_SERVICE"))',
								),
								'update' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_SERVICE"))',
								),
								'delete' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "DELETE_REQUEST_SERVICE"))',
								),
								'file' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "VIEW_REQUEST_SERVICE")) && RequestUtil::hasRequestServiceFile("$data->id")',
										'imageUrl' => Yii::app()->request->baseUrl.'/images/attach.jpg',
										'url' => 'Yii::app()->request->baseUrl."/".RequestUtil::getRequestServiceFilePath($data->id)',
								),
						),
				),
		),
));
?>
