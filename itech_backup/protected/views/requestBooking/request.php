<div class="module-head">
	<span>Request</span>
</div>
<?php 
if(isset($_SESSION['OPERATION_RESULT'])) {
	$result = $_SESSION['OPERATION_RESULT'];
	echo '<div class="'.$result['class'].'">'.$result['message'].'</div>';
	unset($_SESSION['OPERATION_RESULT']);
}
?>
<div>
	<link rel="stylesheet"
		href="<?php echo Yii::app()->request->baseUrl;?>/css/bootstrap.css">
	<!-- Generic page styles -->
	<link rel="stylesheet"
		href="<?php echo Yii::app()->request->baseUrl;?>/jQuery-File-Upload-8.2.1/css/style.css">
	<!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
	<link rel="stylesheet"
		href="<?php echo Yii::app()->request->baseUrl;?>/jQuery-File-Upload-8.2.1/css/jquery.fileupload-ui.css">
	<script type="text/javascript"
		src="<?php echo Yii::app()->request->baseUrl;?>/js/ajax.js"></script>
	<script type="text/javascript"
		src="<?php echo Yii::app()->request->baseUrl;?>/js/util.js"></script>
	<script type="text/javascript"
		src="<?php echo Yii::app()->request->baseUrl;?>/jQuery-File-Upload-8.2.1/js/vendor/jquery.ui.widget.js"></script>
	<script type="text/javascript"
		src="<?php echo Yii::app()->request->baseUrl;?>/jQuery-File-Upload-8.2.1/js/jquery.iframe-transport.js"></script>
	<script type="text/javascript"
		src="<?php echo Yii::app()->request->baseUrl;?>/jQuery-File-Upload-8.2.1/js/jquery.fileupload.js"></script>

	<script type="text/javascript">
		function loadAjaxUpload() {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var time = '<?php echo time()?>';
		    var url2 = '<?php echo Yii::app()->request->baseUrl;?>/upload/?folder=' + time;
		    $('#fileupload').fileupload({
		        url: url2,
		        dataType: 'json',
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		                $('<p/>').text(file.name).appendTo('#files');
		                $('#f_path').val('upload/files/' + time + '/' + file.name);
		            });
		            $('#fileupload').hide();		            
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .bar').css(
		                'width',
		                progress + '%'
		            );
		        }
		    });
		}
		
	$(function(){
		$('#users-form').submit(function(){
			return (validateForm() && confirm('Do you want to bokking ?'));
			});
	});
	function validateForm() {
		var requestType = $('#request-type');
		
		if(requestType.val() == '1') {			
			if(!validateRoom()) return false;
			if(!validateEquipmentInRoom()) return false;
			if(!validateCourseName()) return false;
		} else if (requestType.val() == '2') {
			if(!validateRoom()) return false;
			if(!validateEquipmentInRoom()) return false;
			if(!validateCourseName()) return false;
		} else if (requestType.val() == '3') {
			if(!validateRoom()) return false;
			if(!validateEquipmentInRoom()) return false;
			if(!validateEventType()) return false;
			if(!validateRoomType()) return false;
			if(!validatePresentType()) return false;
		}
		return true;
	}
	function validateRoom(){
		var roomId = $('#room_id');
		if(roomId.val() == '') {
			alert('Please select room.');
			roomId.focus();
			return false;
		}
		return true;
	}

	function validateEquipmentInRoom(){
		var objs = document.getElementsByName('equipments[]');
		if(typeof(objs) != "undefined" && objs.length > 0) {
			var hasChecked = false;
			for (var i = 0; i < objs.length; i++) {
				if (objs[i].checked) {
					hasChecked = true;
					break;
				}
			}
			if(!hasChecked) {
				alert('Please select equipment.');
				return false;
			}
		}
		return true;
	}
	

	function validateCourseName() {
		var courseName = $('#course_name');
		if(courseName.val() == '') {
			alert('Please enter your course name.');
			courseName.focus();
			return false;
		}
		return true;
	}
	function validateEventType() {
		var obj = $('#event_type');
		if(obj.val() == '') {
			alert('Please select event type.');
			obj.focus();
			return false;
		}
		return true;
	}
	function validateRoomType() {
		var obj = $('#room_type');
		if(obj.val() == '') {
			alert('Please select room type.');
			obj.focus();
			return false;
		}
		return true;
	}
	function validatePresentType() {
		var objs = document.getElementsByName('present_type[]');
		var hasChecked = false;
		for (var i = 0; i < objs.length; i++) {
			if (objs[i].checked) {
				hasChecked = true;
				break;
			}
		}
		if(!hasChecked) {
			alert('Please select present type.');
			return false;
		}
		return true;
	}
	
	
	
	function resetForm() {
		//alert('asd');
		document.getElementById('request-type').value = '';
		$('#classroom-request-booking-form-area').html('');
	}
	function loadSpinner() {
		<?php 
		$equipmentTypes = EquipmentType::model()->findAll();
		foreach ($equipmentTypes as $equipmentType){
			$equipments = Equipment::model()->findAll(array('condition'=>"(room_id = '7') and equipment_type_id = '".$equipmentType->id."'"));
			$requestBorrows = RequestBorrow::model()->findAll(array('condition'=>"status_code != 'REQUEST_BORROW_COMPLETED' AND status_code != 'REQUEST_BORROW_CANCELLED'"));
			$used = 0;
			foreach($requestBorrows as $requestBorrow) {
				$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model()->findAll(array('condition'=>"request_borrow_id = '".$requestBorrow->id."' and equipment_type_id='".$equipmentType->id."'"));
				if($requestBorrowEquipmentTypes != null && count($requestBorrowEquipmentTypes) > 0 ) {
					$requestBorrowEquipmentType = $requestBorrowEquipmentTypes[0];
					$used = $used + $requestBorrowEquipmentType->quantity;
				}
			}
			$max = count($equipments) - $used;
			echo '$("#EquipmentType'.$equipmentType->id.'").spinner({min: 0, max: '.($max * 1).', stop: function(event, ui) {$(this).change();}    }); $("#maxvalue-'.$equipmentType->id.'").html("Available : '.$max.'");';
		}?>
	}
	function qtyChange(element){
		var checkboxId = 'C' + element.id;
		if(parseInt(element.value) > 0) {
			document.getElementById(checkboxId).checked = true;
		} else {
			document.getElementById(checkboxId).checked = false;
		}
	}



	
	<?php
			if(isset($_SESSION['request_room'])){
				$afterScript = "ajaxUpdateArea(\'".Yii::app()->createUrl("AjaxRequest/EquipmentInRoom/room")."/\' + $(\'#room_id\').val(), \'equipment-area\');";
			} else {
				$afterScript = '';
			}
			?>
			
			function checkRequestBookingDate() {
				
				if($('#datepicker').val() != '' && $('#time-end').val() != '' && $('#time-start').val()  != '') {  
					ajaxUpdateArea(
				'<?php echo Yii::app()->createUrl("AjaxRequest/AvailableClassroomBookingPeriod/RequestType")?>/'
						+ $('#request-type').val() 
						+ '/Date/'
						+ $('#datepicker').val()
						+ '/Start/'
					+ $('#time-start').val() + '/End/' + $('#time-end').val(),
					'available-request-room', '<?php echo $afterScript ?>' + 'checkRequestBookingSemesterDate(); ' + 'checkRequestBookingActivityDate();');
				}
			}
			function checkRequestBookingSemesterDate() {
				if($('#request-type').val() == '1') {
					if($('#datepicker').val() != '' && $('#time-end').val() != '' && $('#time-start').val()  != '') {  
						ajaxUpdateArea('<?php echo Yii::app()->createUrl("AjaxRequest/AvailableClassroomBookingPeriod/RequestType")?>'
								 + '/' + $('#request-type').val()
								 + '/Date/'	+ $('#datepicker').val()
								 +'/Start/' + $('#time-start').val() 
								 + '/End/' + $('#time-end').val(), 'available-request-room', '<?php echo $afterScript ?>');
					}
				}
				if($('#request-type').val() == '2') {
					if($('#day-in-week').val() != '' && $('#semester').val() != '' && $('#time-end').val() != '' && $('#time-start').val()  != '') {  
						ajaxUpdateArea('<?php echo Yii::app()->createUrl("AjaxRequest/AvailableClassroomBookingPeriod/RequestType")?>' + '/' + $('#request-type').val() + '/Day/' + $('#day-in-week').val() + '/Semester/'+ $('#semester').val() +'/Start/' + $('#time-start').val() + '/End/' + $('#time-end').val(), 'available-request-room', '<?php echo $afterScript ?>');
					}
				}
			}
			function checkRequestBookingActivityDate() {
				if($('#request-type').val() == '3') {
					if($('#datepicker').val() != '' && $('#time-end').val() != '' && $('#time-start').val()  != '') {  
						ajaxUpdateArea('<?php echo Yii::app()->createUrl("AjaxRequest/AvailableClassroomBookingPeriod/RequestType/3/Date")?>/' + $('#datepicker').val() + '/Start/' + $('#time-start').val() + '/End/' + $('#time-end').val(), 'available-request-room', 'loadSpinner(); loadAjaxUpload();')
					}
				}
			}
						
</script>
	<?php 
	$requestTypes = RequestBookingType::model()->findAll();
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'users-form',
			'htmlOptions'=>array('enctype' => 'multipart/form-data', 'name'=>'form1')

	));

	?>
	<div style="color: darkred">
		<?php 
		if(isset($_SESSION['error'])) {
	echo $_SESSION['error'].'<br />';
	unset($_SESSION['error']);
}
//echo $_SESSION['request_type'];
?>
	</div>
	<div>
		<table class="simple-form">
			<tr>
				<td class="column-left" width="20%">Request Type</td>
				<td class="column-right"><select name="request-type"
					id="request-type"
					onchange="ajaxUpdateArea('<?php echo Yii::app()->createUrl("AjaxRequest/ClassroomRequestBooking/RequestType")?>/' + this.value, 'classroom-request-booking-form-area', 'loadDatePicker(\'datepicker\'); checkRequestBookingDate()')">
						<option value="">-- Select --</option>
						<?php foreach($requestTypes as $requestType) {?>
						<option value="<?php echo $requestType->id?>"
						<?php echo $_SESSION['request_type'] == $requestType->id ? 'selected="selected"' : '' ?>>
							<?php echo $requestType->name?>
						</option>
						<?php }?>
				</select>
				</td>
			</tr>
		</table>
	</div>
	<div id="classroom-request-booking-form-area"></div>
	<?php $this->endWidget(); ?>
</div>
<?php if($_SESSION['request_type'] != '') {	
	?>
<script type="text/javascript">
	ajaxUpdateArea('<?php echo Yii::app()->createUrl("AjaxRequest/ClassroomRequestBooking/RequestType")?>/' + $('#request-type').val() + '&tmp=<?php echo time()?>', 'classroom-request-booking-form-area', 'loadDatePicker(\'datepicker\'); checkRequestBookingDate()')
</script>
<?php 
unset($_SESSION['request_type'] );
}?>
