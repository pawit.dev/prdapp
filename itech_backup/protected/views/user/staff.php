<div class="module-head">Staff</div>
<div>

</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'name'=>'username',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(
						'header' => 'Title',
						'name'=>'user_information_personal_title_search',
						'value'=>'$data->user_information->personal_title',
						'htmlOptions'=>array('width'=>'10%'),
				),
				array(
						'header' => 'First Name',
						'name'=>'user_information_first_name_search',
						'value'=>'$data->user_information->first_name',
						'htmlOptions'=>array('width'=>'15%'),
				),
				array(
						'header' => 'Last Name',
						'name'=>'user_information_last_name_search',
						'value'=>'$data->user_information->last_name',
						'htmlOptions'=>array('width'=>'15%'),
				),
				array(
						'header' => 'Email',
						'name'=>'user_information_email_search',
						'value'=>'$data->email',
				),
				array(
						'header' => 'Status',
						'name'=>'status_name_search',
						'value'=>'$data->user_status->name',
						'htmlOptions'=>array('width'=>'8%', 'align'=>'center'),
				),
				array(
						'class'=>'CButtonColumn',
						'header'=>'',
						'template'=>'{Assign Rooms}',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
						'buttons'=>array
						(
								'Assign Rooms' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN"))',
										'url'=>'Yii::app()->createUrl("User/AssignRoom/id/".$data->id)',
								),
						),
				),
		),
));
?>
