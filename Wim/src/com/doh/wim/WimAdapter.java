package com.doh.wim;

import java.util.ArrayList;

import com.doh.wim.model.Wim;
import com.doh.wim.utils.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WimAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<Wim> wimList;
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public WimAdapter(Activity a, ArrayList<Wim> _wimList) {
		activity = a;
		wimList = _wimList;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount() {
		return wimList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.fragment_station_detail, null);

		TextView txtStation = (TextView) vi.findViewById(R.id.txtStation);

		TextView txtVehicleNum = (TextView) vi.findViewById(R.id.txtVehicleNum);
		TextView txtLane = (TextView) vi.findViewById(R.id.txtLane);
		TextView txtClass = (TextView) vi.findViewById(R.id.txtClass);

		TextView txtSort = (TextView) vi.findViewById(R.id.txtSort);
		TextView txtMax = (TextView) vi.findViewById(R.id.txtMax);
		TextView txtGvw = (TextView) vi.findViewById(R.id.txtGvw);

		TextView txtDate = (TextView) vi.findViewById(R.id.txtDate);

		ImageView imageView = (ImageView) vi.findViewById(R.id.list_image); 
		ImageView imageView1 = (ImageView) vi.findViewById(R.id.list_image2); 
		
		 Wim wim = wimList.get(position);
		 txtStation.setText(wim.getStationName());
		 
		 txtVehicleNum.setText(wim.getVehicleNumber());
		 txtLane.setText(wim.getLane());
		 txtClass.setText(wim.getVehicleClass());
		 
		 txtSort.setText(wim.getSortDecision());
		 txtMax.setText(wim.getMaxGVW());
		 txtGvw.setText(wim.getGVW());
		 
		 txtDate.setText(wim.getTimeStamp());
		
		 imageLoader.DisplayImage(wim.getImage01Name(),imageView);
		 imageLoader.DisplayImage(wim.getImage02Name(),imageView1);
		 
		return vi;
	}
}