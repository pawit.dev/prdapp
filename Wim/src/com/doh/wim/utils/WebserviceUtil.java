package com.doh.wim.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.*;

import org.json.JSONObject;
import org.json.JSONArray;

import com.doh.wim.model.Station;


/**
 * Created by pawit on 10/18/14 AD.
 */

public class WebserviceUtil {

    public static final String URL_STATION = "http://61.19.97.185/wim_service/WimService/GetStation";
    public static final String URL_WIMDATA = "http://61.19.97.185/wim_service/WimService/GetWimData?top=5";

    //public static ArrayList<Station> STATIONS = new ArrayList<Station>();
    //public static Map<String, Station> ITEM_MAP = new HashMap<String, Station>();


    private String jsonData;
    private JSONObject jsonObj;
    private JSONArray jsonArr;

    public WebserviceUtil(){

    }

    /*
    public boolean connectInputStream(){
        String result = getHttpData(path);
//		System.out.println("result = " + result);

        if(result != "" || !result.equalsIgnoreCase("")){
            jsonData = result;
            return true;
        }else{
            jsonData = "";
            return false;
        }
    }
*/

    public void setJsonData(String jsonData){
        this.jsonData = jsonData;
    }

    public boolean feedLoginData(){
        boolean result = false;
        try {
            jsonObj = new JSONObject(jsonData);
            if(jsonObj.length() > 0){
                result = jsonObj.getBoolean("result");
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("##### ERROR feedLogShareInfo ######");
            return false;
        }
        return result;
    }

    public ArrayList<Station> getStation(){
    	ArrayList<Station> stationList = new ArrayList<Station>();
    	
        try {

            jsonData = getHttpData(URL_STATION);
/*
            if (jsonData == null || jsonData.equals(null)
                    || jsonData.equalsIgnoreCase("null") || jsonData.equalsIgnoreCase("[]")
                    || jsonData.equals("") || jsonData.length() == 0) {
                return STATIONS;
            }
*/
            jsonArr = new JSONArray(jsonData);
            for(int i = 0; i < jsonArr.length(); i++){
                Station item = new Station();

                JSONObject data = jsonArr.getJSONObject(i);
                Iterator keys = data.keys();

                while(keys.hasNext()) {
                    String key = (String) keys.next();

                    if(key.equalsIgnoreCase("StationID")){
                        item.setStationId(data.getInt(key));
                    }else if(key.equalsIgnoreCase("StationName")){
                        item.setStationName(data.getString(key));
                    }
                }
                stationList.add(item);
            }
        } catch (Exception e) {
            // TODO: handle exception
        	stationList = null;
            System.out.println("##### ERROR feedStation ######");
        }

        return stationList;
    }

    //////////////////////////////////////////////////////////////

    private static String getHttpData(String url) {
        System.out.println("URL Connected ===>>> " + url);
        String result = null;
        int timeOutMS = 1000*10;

        try {
            URL myURL = new URL(url);
            URLConnection ucon = myURL.openConnection();
            ucon.setConnectTimeout(timeOutMS);
            InputStream is = ucon.getInputStream();
            result = convertStreamToString(is);
            is.close();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            result = "";
            return result;
        }

        return result;
    }

    private static String convertStreamToString(InputStream is) {

        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"));
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        }

        return sb.toString();
    }





}
