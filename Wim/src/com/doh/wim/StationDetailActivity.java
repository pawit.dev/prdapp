package com.doh.wim;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.doh.wim.dao.DatabaseHandler;
import com.doh.wim.model.Wim;
import com.doh.wim.utils.WebserviceUtil;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * An activity representing a single Station detail screen. This activity is
 * only used on handset devices. On tablet-size devices, item details are
 * presented side-by-side with a list of items in a {@link StationListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link StationDetailFragment}.
 */
public class StationDetailActivity extends Activity {

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static ArrayList<Wim> wimList = new ArrayList<Wim>();
	ListView list;
	WimAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_station_detail);

		
		
		//Begin load data.
		new WimTask(this).execute();
		
		
		for (int i = 0; i < 1; i++) {
			
			Wim wim = new Wim();
			wim.setWIMID(i);
			wim.setStationName("Banna");

			wim.setVehicleNumber("6000123");
			wim.setVehicleClass("1");
			wim.setLane("1");

			wim.setSortDecision("-");
			wim.setGVW("10,000");
			wim.setMaxGVW("20,000");
			wim.setTimeStamp("2014-10-22 15:21");

			wim.setImage01Name("http://61.19.97.185/weighing_data_img/04/2014/10/22/WIM/wim-04-20141022-141743-7957-0.jpg");
			wim.setImage02Name("http://61.19.97.185/wim/images/inf.jpg");
			wimList.add(wim);

		}

		list = (ListView) findViewById(R.id.list);

		// Getting adapter by passing xml data ArrayList
		adapter = new WimAdapter(this, wimList);
		list.setAdapter(adapter);

		// Click event for single list row
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Log.d("ITEM SELECT", "Selected:" + id);
			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpTo(this, new Intent(this,
					StationListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	

	public class WimTask extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		private ProgressDialog Dialog = null;
		private Context context;

		String data = "";
		String serverText = "";
		
		public WimTask(Context _context){
			context = _context;
			Dialog = new ProgressDialog(_context);
		}
		
		protected void onPreExecute() {
			// NOTE: You can call UI Element here.

			// Start Progress Dialog (Message)

			Dialog.setMessage("Please wait..");
			Dialog.show();

			try {
				// Set Request parameter
				data += "&" + URLEncoder.encode("data", "UTF-8") + "="
						+ serverText;

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(WebserviceUtil.URL_WIMDATA);

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(data);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "");
				}

				// Append Server Response To Content String
				Content = sb.toString();
			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
					Log.d("",""+ex.getMessage());
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (Error != null) {
				Log.d("Error", Error);
			} else {
				Log.d("Content", Content);
				/****************** Start Parse Response JSON Data *************/
				JSONArray jsonArr;

				try {
					DatabaseHandler db = new DatabaseHandler(context);
					
					jsonArr = new JSONArray(Content);
					for (int i = 0; i < jsonArr.length(); i++) {
						Wim item = new Wim();

						JSONObject data = jsonArr.getJSONObject(i);
						@SuppressWarnings("rawtypes")
						Iterator keys = data.keys();

						while (keys.hasNext()) {
							String key = (String) keys.next();

							if (key.equalsIgnoreCase("wimid")) { item.setWIMID(data.getInt(key));} 
							else if (key.equalsIgnoreCase("imgpath")) { item.setImage01Name(data.getString(key));}
							else if (key.equalsIgnoreCase("imgPath2")) { item.setImage02Name(data.getString(key));}
							//else if (key.equalsIgnoreCase("RefID")) { item.se(data.getString(key));}
							else if (key.equalsIgnoreCase("StationName")) { item.setStationName(data.getString(key));}
							else if (key.equalsIgnoreCase("StationId")) { item.setStationID(data.getInt(key));}
							else if (key.equalsIgnoreCase("TimeStamp")) { item.setTimeStamp(data.getString(key));}
							else if (key.equalsIgnoreCase("VehicleNumber")) { item.setVehicleNumber(data.getString(key));}
							//else if (key.equalsIgnoreCase("Lane")) { item.setLane(data.getString(key));}
							else if (key.equalsIgnoreCase("LaneId")) { item.setLane(data.getString(key));}
							else if (key.equalsIgnoreCase("VehicleClass")) { item.setVehicleClass(data.getString(key));}
							else if (key.equalsIgnoreCase("SortDecision")) { item.setSortDecision(data.getString(key));}
							else if (key.equalsIgnoreCase("MaxGvw")) { item.setMaxGVW(data.getString(key));}
							else if (key.equalsIgnoreCase("Gvw")) { item.setGVW(data.getString(key));}
							else if (key.equalsIgnoreCase("LicensePlateNumber")) { item.setLicensePlateNumber(data.getString(key));}
							else if (key.equalsIgnoreCase("ProvinceName")) { item.setProvinceName(data.getString(key));}
							else if (key.equalsIgnoreCase("statusCode")) { item.setStatusCode(data.getString(key));}
							else if (key.equalsIgnoreCase("errorCode")) { item.setError(data.getString(key));}
							else if (key.equalsIgnoreCase("StatusColor")) { item.setError(data.getString(key));}
							else if (key.equalsIgnoreCase("Speed")) { item.setSpeed(data.getString(key));}
							else if (key.equalsIgnoreCase("ESAL")) { item.setESAL(data.getString(key));}
							else if (key.equalsIgnoreCase("AxleCount")) { item.setAxleCount(data.getString(key));}
							else if (key.equalsIgnoreCase("Length")) { item.setLength(data.getString(key));}
							else if (key.equalsIgnoreCase("FrontOverHang")) { item.setFrontOverHang(data.getString(key));}
							else if (key.equalsIgnoreCase("RearOverHang")) { item.setRearOverHang(data.getString(key));}
							
							else if (key.equalsIgnoreCase("SeperationCol_1")) { item.setAxle01Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_2")) { item.setAxle02Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_3")) { item.setAxle03Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_4")) { item.setAxle04Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_5")) { item.setAxle05Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_6")) { item.setAxle06Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_7")) { item.setAxle07Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_8")) { item.setAxle08Seperation(data.getString(key));}
							else if (key.equalsIgnoreCase("SeperationCol_9")) { item.setAxle09Seperation(data.getString(key));}
							
							else if (key.equalsIgnoreCase("GroupCol_1")) { item.setAxle01Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_2")) { item.setAxle02Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_3")) { item.setAxle03Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_4")) { item.setAxle04Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_5")) { item.setAxle05Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_6")) { item.setAxle06Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_7")) { item.setAxle07Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_8")) { item.setAxle08Group(data.getString(key));}
							else if (key.equalsIgnoreCase("GroupCol_9")) { item.setAxle09Group(data.getString(key));}
							
							else if (key.equalsIgnoreCase("WeightCol_1")) { item.setAxle01Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_2")) { item.setAxle02Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_3")) { item.setAxle03Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_4")) { item.setAxle04Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_5")) { item.setAxle05Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_6")) { item.setAxle06Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_7")) { item.setAxle07Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_8")) { item.setAxle08Weight(data.getString(key));}
							else if (key.equalsIgnoreCase("WeightCol_9")) { item.setAxle09Weight(data.getString(key));}
						 }
						try {
							int size = db.getWimCount();
							Wim wim = db.getWim(item.getWIMID());
							if (wim == null) {
								db.addWim(item);
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}



				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
