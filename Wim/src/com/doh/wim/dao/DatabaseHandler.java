package com.doh.wim.dao;

import java.util.ArrayList;

import com.doh.wim.model.Station;
import com.doh.wim.model.Wim;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
	// All Static variables
		// Database Version
		private static final int DATABASE_VERSION = 1;

		// Database Name
		private static final String DATABASE_NAME = "wimdb";

		private static final String TABLE_STATON = "STATON";
		private static final String TABLE_WIM = "WIM";
		
		
		String CREATE_STATON_TABLE = "CREATE TABLE " + TABLE_STATON + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_STATION_NAME + " TEXT"
				+ ")";
		
		String DROP_STATON_TABLE = "DROP TABLE IF EXISTS " + TABLE_STATON;

		String CREATE_WIM_TABLE = "CREATE TABLE " + TABLE_WIM + "("
				+ KEY_WIMID + " INTEGER PRIMARY KEY," + KEY_STATIONID
				+ " TEXT," + KEY_TIMESTAMP + " TEXT," + KEY_VEHICLENUMBER
				+ " TEXT," + KEY_LANE + " TEXT," + KEY_ERROR + " TEXT,"
				+ KEY_STATUSCODE + " TEXT," + KEY_GVW + " TEXT," + KEY_MAXGVW
				+ " TEXT," + KEY_ESAL + " TEXT," + KEY_SPEED + " TEXT,"
				+ KEY_AXLECOUNT + " TEXT," + KEY_AXLE01SEPERATION + " TEXT,"
				+ KEY_AXLE01WEIGHT + " TEXT," + KEY_AXLE01MAX + " TEXT,"
				+ KEY_AXLE01GROUP + " TEXT," + KEY_AXLE01TIRECODE + " TEXT,"
				+ KEY_AXLE02SEPERATION + " TEXT," + KEY_AXLE02WEIGHT + " TEXT,"
				+ KEY_AXLE02MAX + " TEXT," + KEY_AXLE02GROUP + " TEXT,"
				+ KEY_AXLE02TIRECODE + " TEXT," + KEY_AXLE03SEPERATION
				+ " TEXT," + KEY_AXLE03WEIGHT + " TEXT," + KEY_AXLE03MAX
				+ " TEXT," + KEY_AXLE03GROUP + " TEXT," + KEY_AXLE03TIRECODE
				+ " TEXT," + KEY_AXLE04SEPERATION + " TEXT," + KEY_AXLE04WEIGHT
				+ " TEXT," + KEY_AXLE04MAX + " TEXT," + KEY_AXLE04GROUP
				+ " TEXT," + KEY_AXLE04TIRECODE + " TEXT,"
				+ KEY_AXLE05SEPERATION + " TEXT," + KEY_AXLE05WEIGHT + " TEXT,"
				+ KEY_AXLE05MAX + " TEXT," + KEY_AXLE05GROUP + " TEXT,"
				+ KEY_AXLE05TIRECODE + " TEXT," + KEY_AXLE06SEPERATION
				+ " TEXT," + KEY_AXLE06WEIGHT + " TEXT," + KEY_AXLE06MAX
				+ " TEXT," + KEY_AXLE06GROUP + " TEXT," + KEY_AXLE06TIRECODE
				+ " TEXT," + KEY_AXLE07SEPERATION + " TEXT," + KEY_AXLE07WEIGHT
				+ " TEXT," + KEY_AXLE07MAX + " TEXT," + KEY_AXLE07GROUP
				+ " TEXT," + KEY_AXLE07TIRECODE + " TEXT,"
				+ KEY_AXLE08SEPERATION + " TEXT," + KEY_AXLE08WEIGHT + " TEXT,"
				+ KEY_AXLE08MAX + " TEXT," + KEY_AXLE08GROUP + " TEXT,"
				+ KEY_AXLE08TIRECODE + " TEXT," + KEY_AXLE09SEPERATION
				+ " TEXT," + KEY_AXLE09WEIGHT + " TEXT," + KEY_AXLE09MAX
				+ " TEXT," + KEY_AXLE09GROUP + " TEXT," + KEY_AXLE09TIRECODE
				+ " TEXT," + KEY_AXLE10SEPERATION + " TEXT," + KEY_AXLE10WEIGHT
				+ " TEXT," + KEY_AXLE10MAX + " TEXT," + KEY_AXLE10GROUP
				+ " TEXT," + KEY_AXLE10TIRECODE + " TEXT,"
				+ KEY_AXLE11SEPERATION + " TEXT," + KEY_AXLE11WEIGHT + " TEXT,"
				+ KEY_AXLE11MAX + " TEXT," + KEY_AXLE11GROUP + " TEXT,"
				+ KEY_AXLE11TIRECODE + " TEXT," + KEY_AXLE12SEPERATION
				+ " TEXT," + KEY_AXLE12WEIGHT + " TEXT," + KEY_AXLE12MAX
				+ " TEXT," + KEY_AXLE12GROUP + " TEXT," + KEY_AXLE12TIRECODE
				+ " TEXT," + KEY_AXLE13SEPERATION + " TEXT," + KEY_AXLE13WEIGHT
				+ " TEXT," + KEY_AXLE13MAX + " TEXT," + KEY_AXLE13GROUP
				+ " TEXT," + KEY_AXLE13TIRECODE + " TEXT,"
				+ KEY_AXLE14SEPERATION + " TEXT," + KEY_AXLE14WEIGHT + " TEXT,"
				+ KEY_AXLE14MAX + " TEXT," + KEY_AXLE14GROUP + " TEXT,"
				+ KEY_AXLE14TIRECODE + " TEXT," + KEY_LENGTH + " TEXT,"
				+ KEY_FRONTOVERHANG + " TEXT," + KEY_REAROVERHANG + " TEXT,"
				+ KEY_VEHICLETYPE + " TEXT," + KEY_VEHICLECLASS + " TEXT,"
				+ KEY_RECORDTYPE + " TEXT," + KEY_IMAGECOUNT + " TEXT,"
				+ KEY_IMAGE01NAME + " TEXT," + KEY_IMAGE02NAME + " TEXT,"
				+ KEY_IMAGE03NAME + " TEXT," + KEY_IMAGE04NAME + " TEXT,"
				+ KEY_IMAGE05NAME + " TEXT," + KEY_IMAGE06NAME + " TEXT,"
				+ KEY_IMAGE07NAME + " TEXT," + KEY_IMAGE08NAME + " TEXT,"
				+ KEY_IMAGE09NAME + " TEXT," + KEY_IMAGE10NAME + " TEXT,"
				+ KEY_SORTDECISION + " TEXT," + KEY_LICENSEPLATENUMBER
				+ " TEXT," + KEY_LICENSEPLATEPROVINCEID + " TEXT,"
				+ KEY_LICENSEPLATEIMAGENAME + " TEXT,"
				+ KEY_LICENSEPLATECONFIDENCE + " TEXT," + KEY_STATUSCOLOR
				+ " TEXT," + KEY_PROVINCENAME + " TEXT," + KEY_STATIONNAME
				+ " TEXT" + ")";

		String DROP_WIM_TABLE = "DROP TABLE IF EXISTS " + TABLE_WIM;
		
		// STATON Table Columns names
		private static final String KEY_ID = "stationId";
		private static final String KEY_STATION_NAME = "stationName";

		// WIM Table Columns names
		private static final String KEY_WIMID = "WIMID";
		private static final String KEY_STATIONID = "StationID";
		private static final String KEY_TIMESTAMP = "TimeStamp";
		private static final String KEY_VEHICLENUMBER = "VehicleNumber";
		private static final String KEY_LANE = "Lane";
		private static final String KEY_ERROR = "Error";
		private static final String KEY_STATUSCODE = "StatusCode";
		private static final String KEY_GVW = "GVW";
		private static final String KEY_MAXGVW = "MaxGVW";
		private static final String KEY_ESAL = "ESAL";
		private static final String KEY_SPEED = "Speed";
		private static final String KEY_AXLECOUNT = "AxleCount";
		private static final String KEY_AXLE01SEPERATION = "Axle01Seperation";
		private static final String KEY_AXLE01WEIGHT = "Axle01Weight";
		private static final String KEY_AXLE01MAX = "Axle01Max";
		private static final String KEY_AXLE01GROUP = "Axle01Group";
		private static final String KEY_AXLE01TIRECODE = "Axle01TireCode";
		private static final String KEY_AXLE02SEPERATION = "Axle02Seperation";
		private static final String KEY_AXLE02WEIGHT = "Axle02Weight";
		private static final String KEY_AXLE02MAX = "Axle02Max";
		private static final String KEY_AXLE02GROUP = "Axle02Group";
		private static final String KEY_AXLE02TIRECODE = "Axle02TireCode";
		private static final String KEY_AXLE03SEPERATION = "Axle03Seperation";
		private static final String KEY_AXLE03WEIGHT = "Axle03Weight";
		private static final String KEY_AXLE03MAX = "Axle03Max";
		private static final String KEY_AXLE03GROUP = "Axle03Group";
		private static final String KEY_AXLE03TIRECODE = "Axle03TireCode";
		private static final String KEY_AXLE04SEPERATION = "Axle04Seperation";
		private static final String KEY_AXLE04WEIGHT = "Axle04Weight";
		private static final String KEY_AXLE04MAX = "Axle04Max";
		private static final String KEY_AXLE04GROUP = "Axle04Group";
		private static final String KEY_AXLE04TIRECODE = "Axle04TireCode";
		private static final String KEY_AXLE05SEPERATION = "Axle05Seperation";
		private static final String KEY_AXLE05WEIGHT = "Axle05Weight";
		private static final String KEY_AXLE05MAX = "Axle05Max";
		private static final String KEY_AXLE05GROUP = "Axle05Group";
		private static final String KEY_AXLE05TIRECODE = "Axle05TireCode";
		private static final String KEY_AXLE06SEPERATION = "Axle06Seperation";
		private static final String KEY_AXLE06WEIGHT = "Axle06Weight";
		private static final String KEY_AXLE06MAX = "Axle06Max";
		private static final String KEY_AXLE06GROUP = "Axle06Group";
		private static final String KEY_AXLE06TIRECODE = "Axle06TireCode";
		private static final String KEY_AXLE07SEPERATION = "Axle07Seperation";
		private static final String KEY_AXLE07WEIGHT = "Axle07Weight";
		private static final String KEY_AXLE07MAX = "Axle07Max";
		private static final String KEY_AXLE07GROUP = "Axle07Group";
		private static final String KEY_AXLE07TIRECODE = "Axle07TireCode";
		private static final String KEY_AXLE08SEPERATION = "Axle08Seperation";
		private static final String KEY_AXLE08WEIGHT = "Axle08Weight";
		private static final String KEY_AXLE08MAX = "Axle08Max";
		private static final String KEY_AXLE08GROUP = "Axle08Group";
		private static final String KEY_AXLE08TIRECODE = "Axle08TireCode";
		private static final String KEY_AXLE09SEPERATION = "Axle09Seperation";
		private static final String KEY_AXLE09WEIGHT = "Axle09Weight";
		private static final String KEY_AXLE09MAX = "Axle09Max";
		private static final String KEY_AXLE09GROUP = "Axle09Group";
		private static final String KEY_AXLE09TIRECODE = "Axle09TireCode";
		private static final String KEY_AXLE10SEPERATION = "Axle10Seperation";
		private static final String KEY_AXLE10WEIGHT = "Axle10Weight";
		private static final String KEY_AXLE10MAX = "Axle10Max";
		private static final String KEY_AXLE10GROUP = "Axle10Group";
		private static final String KEY_AXLE10TIRECODE = "Axle10TireCode";
		private static final String KEY_AXLE11SEPERATION = "Axle11Seperation";
		private static final String KEY_AXLE11WEIGHT = "Axle11Weight";
		private static final String KEY_AXLE11MAX = "Axle11Max";
		private static final String KEY_AXLE11GROUP = "Axle11Group";
		private static final String KEY_AXLE11TIRECODE = "Axle11TireCode";
		private static final String KEY_AXLE12SEPERATION = "Axle12Seperation";
		private static final String KEY_AXLE12WEIGHT = "Axle12Weight";
		private static final String KEY_AXLE12MAX = "Axle12Max";
		private static final String KEY_AXLE12GROUP = "Axle12Group";
		private static final String KEY_AXLE12TIRECODE = "Axle12TireCode";
		private static final String KEY_AXLE13SEPERATION = "Axle13Seperation";
		private static final String KEY_AXLE13WEIGHT = "Axle13Weight";
		private static final String KEY_AXLE13MAX = "Axle13Max";
		private static final String KEY_AXLE13GROUP = "Axle13Group";
		private static final String KEY_AXLE13TIRECODE = "Axle13TireCode";
		private static final String KEY_AXLE14SEPERATION = "Axle14Seperation";
		private static final String KEY_AXLE14WEIGHT = "Axle14Weight";
		private static final String KEY_AXLE14MAX = "Axle14Max";
		private static final String KEY_AXLE14GROUP = "Axle14Group";
		private static final String KEY_AXLE14TIRECODE = "Axle14TireCode";
		private static final String KEY_LENGTH = "Length";
		private static final String KEY_FRONTOVERHANG = "FrontOverHang";
		private static final String KEY_REAROVERHANG = "RearOverHang";
		private static final String KEY_VEHICLETYPE = "VehicleType";
		private static final String KEY_VEHICLECLASS = "VehicleClass";
		private static final String KEY_RECORDTYPE = "RecordType";
		private static final String KEY_IMAGECOUNT = "ImageCount";
		private static final String KEY_IMAGE01NAME = "Image01Name";
		private static final String KEY_IMAGE02NAME = "Image02Name";
		private static final String KEY_IMAGE03NAME = "Image03Name";
		private static final String KEY_IMAGE04NAME = "Image04Name";
		private static final String KEY_IMAGE05NAME = "Image05Name";
		private static final String KEY_IMAGE06NAME = "Image06Name";
		private static final String KEY_IMAGE07NAME = "Image07Name";
		private static final String KEY_IMAGE08NAME = "Image08Name";
		private static final String KEY_IMAGE09NAME = "Image09Name";
		private static final String KEY_IMAGE10NAME = "Image10Name";
		private static final String KEY_SORTDECISION = "SortDecision";
		private static final String KEY_LICENSEPLATENUMBER = "LicensePlateNumber";
		private static final String KEY_LICENSEPLATEPROVINCEID = "LicensePlateProvinceID";
		private static final String KEY_LICENSEPLATEIMAGENAME = "LicensePlateImageName";
		private static final String KEY_LICENSEPLATECONFIDENCE = "LicensePlateConfidence";
		private static final String KEY_STATUSCOLOR = "StatusColor";
		private static final String KEY_PROVINCENAME = "ProvinceName";
		private static final String KEY_STATIONNAME = "StationName";

		
		public DatabaseHandler(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		// Creating Tables
		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(CREATE_STATON_TABLE);
			db.execSQL(CREATE_WIM_TABLE);
		}

		// Upgrading database
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Drop older table if existed
			db.execSQL(DROP_STATON_TABLE);
			db.execSQL(DROP_WIM_TABLE);
			// Create tables again
			onCreate(db);
		}
		
		

		/**
		 * STATON All CRUD(Create, Read, Update, Delete) Operations
		 */

		// Adding new Station
		public void addStation(Station station) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_ID, station.getStationId()); // Station Name
			values.put(KEY_STATION_NAME, station.getStationName()); // Station Name

			// Inserting Row
			db.insert(TABLE_STATON, null, values);
			db.close(); // Closing database connection
		}

		// Getting single Station
		public Station getStation(int id) {
			SQLiteDatabase db = this.getReadableDatabase();
			Station Station = null;
			Cursor cursor = db.query(TABLE_STATON, new String[] { KEY_ID, KEY_ID,
					KEY_STATION_NAME }, KEY_ID + "=?",
					new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null) {
				cursor.moveToFirst();
				if (cursor.getCount() > 0) {
					Station = new Station(Integer.parseInt(cursor.getString(0)),
							cursor.getString(1));
				}
			}
			// return Station
			return Station;
		}

		// Getting All STATON
		public ArrayList<Station> getAllStation() {
			ArrayList<Station> StationList = new ArrayList<Station>();
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_STATON;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Station station = new Station();
					station.setStationId(Integer.parseInt(cursor.getString(0)));
					station.setStationName(cursor.getString(1));
					// Adding Station to list
					StationList.add(station);

				} while (cursor.moveToNext());
			}

			// return Station list
			return StationList;
		}

		// Updating single Station
		public int updateStation(Station station) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_ID, station.getStationId());
			values.put(KEY_STATION_NAME, station.getStationName());

			// updating row
			return db.update(TABLE_STATON, values, KEY_ID + " = ?",
					new String[] { String.valueOf(station.getStationId()) });
		}

		// Deleting single Station
		public void deleteStation(Station station) {
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_STATON, KEY_ID + " = ?",
					new String[] { String.valueOf(station.getStationId()) });
			db.close();
		}

		// Getting STATON Count
		public int getStationCount() {
			String countQuery = "SELECT  * FROM " + TABLE_STATON;
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(countQuery, null);
			cursor.close();

			// return count
			return cursor.getCount();
		}

		/**
		 * WIM All CRUD(Create, Read, Update, Delete) Operations
		 */
		
		// Adding new Wim
		public void addWim(Wim wim) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_WIMID, wim.getWIMID());
			values.put(KEY_STATIONID, wim.getStationID());
			values.put(KEY_TIMESTAMP, wim.getTimeStamp());
			values.put(KEY_VEHICLENUMBER, wim.getVehicleNumber());
			values.put(KEY_LANE, wim.getLane());
			values.put(KEY_ERROR, wim.getError());
			values.put(KEY_STATUSCODE, wim.getStatusCode());
			values.put(KEY_GVW, wim.getGVW());
			values.put(KEY_MAXGVW, wim.getMaxGVW());
			values.put(KEY_ESAL, wim.getESAL());
			values.put(KEY_SPEED, wim.getSpeed());
			values.put(KEY_AXLECOUNT, wim.getAxleCount());
			values.put(KEY_AXLE01SEPERATION, wim.getAxle01Seperation());
			values.put(KEY_AXLE01WEIGHT, wim.getAxle01Weight());
			values.put(KEY_AXLE01MAX, wim.getAxle01Max());
			values.put(KEY_AXLE01GROUP, wim.getAxle01Group());
			values.put(KEY_AXLE01TIRECODE, wim.getAxle01TireCode());
			values.put(KEY_AXLE02SEPERATION, wim.getAxle02Seperation());
			values.put(KEY_AXLE02WEIGHT, wim.getAxle02Weight());
			values.put(KEY_AXLE02MAX, wim.getAxle02Max());
			values.put(KEY_AXLE02GROUP, wim.getAxle02Group());
			values.put(KEY_AXLE02TIRECODE, wim.getAxle02TireCode());
			values.put(KEY_AXLE03SEPERATION, wim.getAxle03Seperation());
			values.put(KEY_AXLE03WEIGHT, wim.getAxle03Weight());
			values.put(KEY_AXLE03MAX, wim.getAxle03Max());
			values.put(KEY_AXLE03GROUP, wim.getAxle03Group());
			values.put(KEY_AXLE03TIRECODE, wim.getAxle03TireCode());
			values.put(KEY_AXLE04SEPERATION, wim.getAxle04Seperation());
			values.put(KEY_AXLE04WEIGHT, wim.getAxle04Weight());
			values.put(KEY_AXLE04MAX, wim.getAxle04Max());
			values.put(KEY_AXLE04GROUP, wim.getAxle04Group());
			values.put(KEY_AXLE04TIRECODE, wim.getAxle04TireCode());
			values.put(KEY_AXLE05SEPERATION, wim.getAxle05Seperation());
			values.put(KEY_AXLE05WEIGHT, wim.getAxle05Weight());
			values.put(KEY_AXLE05MAX, wim.getAxle05Max());
			values.put(KEY_AXLE05GROUP, wim.getAxle05Group());
			values.put(KEY_AXLE05TIRECODE, wim.getAxle05TireCode());
			values.put(KEY_AXLE06SEPERATION, wim.getAxle06Seperation());
			values.put(KEY_AXLE06WEIGHT, wim.getAxle06Weight());
			values.put(KEY_AXLE06MAX, wim.getAxle06Max());
			values.put(KEY_AXLE06GROUP, wim.getAxle06Group());
			values.put(KEY_AXLE06TIRECODE, wim.getAxle06TireCode());
			values.put(KEY_AXLE07SEPERATION, wim.getAxle07Seperation());
			values.put(KEY_AXLE07WEIGHT, wim.getAxle07Weight());
			values.put(KEY_AXLE07MAX, wim.getAxle07Max());
			values.put(KEY_AXLE07GROUP, wim.getAxle07Group());
			values.put(KEY_AXLE07TIRECODE, wim.getAxle07TireCode());
			values.put(KEY_AXLE08SEPERATION, wim.getAxle08Seperation());
			values.put(KEY_AXLE08WEIGHT, wim.getAxle08Weight());
			values.put(KEY_AXLE08MAX, wim.getAxle08Max());
			values.put(KEY_AXLE08GROUP, wim.getAxle08Group());
			values.put(KEY_AXLE08TIRECODE, wim.getAxle08TireCode());
			values.put(KEY_AXLE09SEPERATION, wim.getAxle09Seperation());
			values.put(KEY_AXLE09WEIGHT, wim.getAxle09Weight());
			values.put(KEY_AXLE09MAX, wim.getAxle09Max());
			values.put(KEY_AXLE09GROUP, wim.getAxle09Group());
			values.put(KEY_AXLE09TIRECODE, wim.getAxle09TireCode());
			values.put(KEY_AXLE10SEPERATION, wim.getAxle10Seperation());
			values.put(KEY_AXLE10WEIGHT, wim.getAxle10Weight());
			values.put(KEY_AXLE10MAX, wim.getAxle10Max());
			values.put(KEY_AXLE10GROUP, wim.getAxle10Group());
			values.put(KEY_AXLE10TIRECODE, wim.getAxle10TireCode());
			values.put(KEY_AXLE11SEPERATION, wim.getAxle11Seperation());
			values.put(KEY_AXLE11WEIGHT, wim.getAxle11Weight());
			values.put(KEY_AXLE11MAX, wim.getAxle11Max());
			values.put(KEY_AXLE11GROUP, wim.getAxle11Group());
			values.put(KEY_AXLE11TIRECODE, wim.getAxle11TireCode());
			values.put(KEY_AXLE12SEPERATION, wim.getAxle12Seperation());
			values.put(KEY_AXLE12WEIGHT, wim.getAxle12Weight());
			values.put(KEY_AXLE12MAX, wim.getAxle12Max());
			values.put(KEY_AXLE12GROUP, wim.getAxle12Group());
			values.put(KEY_AXLE12TIRECODE, wim.getAxle12TireCode());
			values.put(KEY_AXLE13SEPERATION, wim.getAxle13Seperation());
			values.put(KEY_AXLE13WEIGHT, wim.getAxle13Weight());
			values.put(KEY_AXLE13MAX, wim.getAxle13Max());
			values.put(KEY_AXLE13GROUP, wim.getAxle13Group());
			values.put(KEY_AXLE13TIRECODE, wim.getAxle13TireCode());
			values.put(KEY_AXLE14SEPERATION, wim.getAxle14Seperation());
			values.put(KEY_AXLE14WEIGHT, wim.getAxle14Weight());
			values.put(KEY_AXLE14MAX, wim.getAxle14Max());
			values.put(KEY_AXLE14GROUP, wim.getAxle14Group());
			values.put(KEY_AXLE14TIRECODE, wim.getAxle14TireCode());
			values.put(KEY_LENGTH, wim.getLength());
			values.put(KEY_FRONTOVERHANG, wim.getFrontOverHang());
			values.put(KEY_REAROVERHANG, wim.getRearOverHang());
			values.put(KEY_VEHICLETYPE, wim.getVehicleType());
			values.put(KEY_VEHICLECLASS, wim.getVehicleClass());
			values.put(KEY_RECORDTYPE, wim.getRecordType());
			values.put(KEY_IMAGECOUNT, wim.getImageCount());
			values.put(KEY_IMAGE01NAME, wim.getImage01Name());
			values.put(KEY_IMAGE02NAME, wim.getImage02Name());
			values.put(KEY_IMAGE03NAME, wim.getImage03Name());
			values.put(KEY_IMAGE04NAME, wim.getImage04Name());
			values.put(KEY_IMAGE05NAME, wim.getImage05Name());
			values.put(KEY_IMAGE06NAME, wim.getImage06Name());
			values.put(KEY_IMAGE07NAME, wim.getImage07Name());
			values.put(KEY_IMAGE08NAME, wim.getImage08Name());
			values.put(KEY_IMAGE09NAME, wim.getImage09Name());
			values.put(KEY_IMAGE10NAME, wim.getImage10Name());
			values.put(KEY_SORTDECISION, wim.getSortDecision());
			values.put(KEY_LICENSEPLATENUMBER, wim.getLicensePlateNumber());
			values.put(KEY_LICENSEPLATEPROVINCEID, wim.getLicensePlateProvinceID());
			values.put(KEY_LICENSEPLATEIMAGENAME, wim.getLicensePlateImageName());
			values.put(KEY_LICENSEPLATECONFIDENCE, wim.getLicensePlateConfidence());
			values.put(KEY_STATUSCOLOR, wim.getStatusColor());
			values.put(KEY_PROVINCENAME, wim.getProvinceName());
			values.put(KEY_STATIONNAME, wim.getStationName());

			// Inserting Row
			db.insert(TABLE_WIM, null, values);
			db.close(); // Closing database connection
		}

		// Getting single Wim
		public Wim getWim(int id) {
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.query(TABLE_WIM, new String[] { KEY_WIMID,
					KEY_STATIONID, KEY_TIMESTAMP, KEY_VEHICLENUMBER, KEY_LANE,
					KEY_ERROR, KEY_STATUSCODE, KEY_GVW, KEY_MAXGVW, KEY_ESAL,
					KEY_SPEED, KEY_AXLECOUNT, KEY_AXLE01SEPERATION,
					KEY_AXLE01WEIGHT, KEY_AXLE01MAX, KEY_AXLE01GROUP,
					KEY_AXLE01TIRECODE, KEY_AXLE02SEPERATION, KEY_AXLE02WEIGHT,
					KEY_AXLE02MAX, KEY_AXLE02GROUP, KEY_AXLE02TIRECODE,
					KEY_AXLE03SEPERATION, KEY_AXLE03WEIGHT, KEY_AXLE03MAX,
					KEY_AXLE03GROUP, KEY_AXLE03TIRECODE, KEY_AXLE04SEPERATION,
					KEY_AXLE04WEIGHT, KEY_AXLE04MAX, KEY_AXLE04GROUP,
					KEY_AXLE04TIRECODE, KEY_AXLE05SEPERATION, KEY_AXLE05WEIGHT,
					KEY_AXLE05MAX, KEY_AXLE05GROUP, KEY_AXLE05TIRECODE,
					KEY_AXLE06SEPERATION, KEY_AXLE06WEIGHT, KEY_AXLE06MAX,
					KEY_AXLE06GROUP, KEY_AXLE06TIRECODE, KEY_AXLE07SEPERATION,
					KEY_AXLE07WEIGHT, KEY_AXLE07MAX, KEY_AXLE07GROUP,
					KEY_AXLE07TIRECODE, KEY_AXLE08SEPERATION, KEY_AXLE08WEIGHT,
					KEY_AXLE08MAX, KEY_AXLE08GROUP, KEY_AXLE08TIRECODE,
					KEY_AXLE09SEPERATION, KEY_AXLE09WEIGHT, KEY_AXLE09MAX,
					KEY_AXLE09GROUP, KEY_AXLE09TIRECODE, KEY_AXLE10SEPERATION,
					KEY_AXLE10WEIGHT, KEY_AXLE10MAX, KEY_AXLE10GROUP,
					KEY_AXLE10TIRECODE, KEY_AXLE11SEPERATION, KEY_AXLE11WEIGHT,
					KEY_AXLE11MAX, KEY_AXLE11GROUP, KEY_AXLE11TIRECODE,
					KEY_AXLE12SEPERATION, KEY_AXLE12WEIGHT, KEY_AXLE12MAX,
					KEY_AXLE12GROUP, KEY_AXLE12TIRECODE, KEY_AXLE13SEPERATION,
					KEY_AXLE13WEIGHT, KEY_AXLE13MAX, KEY_AXLE13GROUP,
					KEY_AXLE13TIRECODE, KEY_AXLE14SEPERATION, KEY_AXLE14WEIGHT,
					KEY_AXLE14MAX, KEY_AXLE14GROUP, KEY_AXLE14TIRECODE, KEY_LENGTH,
					KEY_FRONTOVERHANG, KEY_REAROVERHANG, KEY_VEHICLETYPE,
					KEY_VEHICLECLASS, KEY_RECORDTYPE, KEY_IMAGECOUNT,
					KEY_IMAGE01NAME, KEY_IMAGE02NAME, KEY_IMAGE03NAME,
					KEY_IMAGE04NAME, KEY_IMAGE05NAME, KEY_IMAGE06NAME,
					KEY_IMAGE07NAME, KEY_IMAGE08NAME, KEY_IMAGE09NAME,
					KEY_IMAGE10NAME, KEY_SORTDECISION, KEY_LICENSEPLATENUMBER,
					KEY_LICENSEPLATEPROVINCEID, KEY_LICENSEPLATEIMAGENAME,
					KEY_LICENSEPLATECONFIDENCE, KEY_STATUSCOLOR, KEY_PROVINCENAME,
					KEY_STATIONNAME,

			}, KEY_WIMID + "=?", new String[] { String.valueOf(id) }, null, null,
					null, null);

			Wim wim = null;

			if (cursor != null) {
				cursor.moveToFirst();
				if (cursor.getCount() > 0) {
					wim = new Wim();
					wim.setStationID(cursor.getInt(0));
					wim.setTimeStamp(cursor.getString(1));
					wim.setVehicleNumber(cursor.getString(2));
					wim.setLane(cursor.getString(3));
					wim.setError(cursor.getString(4));
					wim.setStatusCode(cursor.getString(5));
					wim.setGVW(cursor.getString(6));
					wim.setMaxGVW(cursor.getString(7));
					wim.setESAL(cursor.getString(8));
					wim.setSpeed(cursor.getString(9));
					wim.setAxleCount(cursor.getString(10));
					wim.setAxle01Seperation(cursor.getString(11));
					wim.setAxle01Weight(cursor.getString(12));
					wim.setAxle01Max(cursor.getString(13));
					wim.setAxle01Group(cursor.getString(14));
					wim.setAxle01TireCode(cursor.getString(15));
					wim.setAxle02Seperation(cursor.getString(16));
					wim.setAxle02Weight(cursor.getString(17));
					wim.setAxle02Max(cursor.getString(18));
					wim.setAxle02Group(cursor.getString(19));
					wim.setAxle02TireCode(cursor.getString(20));
					wim.setAxle03Seperation(cursor.getString(21));
					wim.setAxle03Weight(cursor.getString(22));
					wim.setAxle03Max(cursor.getString(23));
					wim.setAxle03Group(cursor.getString(24));
					wim.setAxle03TireCode(cursor.getString(25));
					wim.setAxle04Seperation(cursor.getString(26));
					wim.setAxle04Weight(cursor.getString(27));
					wim.setAxle04Max(cursor.getString(28));
					wim.setAxle04Group(cursor.getString(29));
					wim.setAxle04TireCode(cursor.getString(30));
					wim.setAxle05Seperation(cursor.getString(31));
					wim.setAxle05Weight(cursor.getString(32));
					wim.setAxle05Max(cursor.getString(33));
					wim.setAxle05Group(cursor.getString(34));
					wim.setAxle05TireCode(cursor.getString(35));
					wim.setAxle06Seperation(cursor.getString(36));
					wim.setAxle06Weight(cursor.getString(37));
					wim.setAxle06Max(cursor.getString(38));
					wim.setAxle06Group(cursor.getString(39));
					wim.setAxle06TireCode(cursor.getString(40));
					wim.setAxle07Seperation(cursor.getString(41));
					wim.setAxle07Weight(cursor.getString(42));
					wim.setAxle07Max(cursor.getString(43));
					wim.setAxle07Group(cursor.getString(44));
					wim.setAxle07TireCode(cursor.getString(45));
					wim.setAxle08Seperation(cursor.getString(46));
					wim.setAxle08Weight(cursor.getString(47));
					wim.setAxle08Max(cursor.getString(48));
					wim.setAxle08Group(cursor.getString(49));
					wim.setAxle08TireCode(cursor.getString(50));
					wim.setAxle09Seperation(cursor.getString(51));
					wim.setAxle09Weight(cursor.getString(52));
					wim.setAxle09Max(cursor.getString(53));
					wim.setAxle09Group(cursor.getString(54));
					wim.setAxle09TireCode(cursor.getString(55));
					wim.setAxle10Seperation(cursor.getString(56));
					wim.setAxle10Weight(cursor.getString(57));
					wim.setAxle10Max(cursor.getString(58));
					wim.setAxle10Group(cursor.getString(59));
					wim.setAxle10TireCode(cursor.getString(60));
					wim.setAxle11Seperation(cursor.getString(61));
					wim.setAxle11Weight(cursor.getString(62));
					wim.setAxle11Max(cursor.getString(63));
					wim.setAxle11Group(cursor.getString(64));
					wim.setAxle11TireCode(cursor.getString(65));
					wim.setAxle12Seperation(cursor.getString(66));
					wim.setAxle12Weight(cursor.getString(67));
					wim.setAxle12Max(cursor.getString(68));
					wim.setAxle12Group(cursor.getString(69));
					wim.setAxle12TireCode(cursor.getString(70));
					wim.setAxle13Seperation(cursor.getString(71));
					wim.setAxle13Weight(cursor.getString(72));
					wim.setAxle13Max(cursor.getString(73));
					wim.setAxle13Group(cursor.getString(74));
					wim.setAxle13TireCode(cursor.getString(75));
					wim.setAxle14Seperation(cursor.getString(76));
					wim.setAxle14Weight(cursor.getString(77));
					wim.setAxle14Max(cursor.getString(78));
					wim.setAxle14Group(cursor.getString(79));
					wim.setAxle14TireCode(cursor.getString(80));
					wim.setLength(cursor.getString(81));
					wim.setFrontOverHang(cursor.getString(82));
					wim.setRearOverHang(cursor.getString(83));
					wim.setVehicleType(cursor.getString(84));
					wim.setVehicleClass(cursor.getString(85));
					wim.setRecordType(cursor.getString(86));
					wim.setImageCount(cursor.getString(87));
					wim.setImage01Name(cursor.getString(88));
					wim.setImage02Name(cursor.getString(89));
					wim.setImage03Name(cursor.getString(90));
					wim.setImage04Name(cursor.getString(91));
					wim.setImage05Name(cursor.getString(92));
					wim.setImage06Name(cursor.getString(93));
					wim.setImage07Name(cursor.getString(94));
					wim.setImage08Name(cursor.getString(95));
					wim.setImage09Name(cursor.getString(96));
					wim.setImage10Name(cursor.getString(97));
					wim.setSortDecision(cursor.getString(98));
					wim.setLicensePlateNumber(cursor.getString(99));
					wim.setLicensePlateProvinceID(cursor.getString(100));
					wim.setLicensePlateImageName(cursor.getString(101));
					wim.setLicensePlateConfidence(cursor.getString(102));
					wim.setStatusColor(cursor.getString(103));
					wim.setProvinceName(cursor.getString(104));
					wim.setStationName(cursor.getString(105));
				}
			}
			return wim;
		}

		// Getting All STATON
		public ArrayList<Wim> getAllWim() {
			ArrayList<Wim> wimList = new ArrayList<Wim>();
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_WIM;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Wim wim = new Wim();
					wim.setStationID(cursor.getInt(0));
					wim.setTimeStamp(cursor.getString(1));
					wim.setVehicleNumber(cursor.getString(2));
					wim.setLane(cursor.getString(3));
					wim.setError(cursor.getString(4));
					wim.setStatusCode(cursor.getString(5));
					wim.setGVW(cursor.getString(6));
					wim.setMaxGVW(cursor.getString(7));
					wim.setESAL(cursor.getString(8));
					wim.setSpeed(cursor.getString(9));
					wim.setAxleCount(cursor.getString(10));
					wim.setAxle01Seperation(cursor.getString(11));
					wim.setAxle01Weight(cursor.getString(12));
					wim.setAxle01Max(cursor.getString(13));
					wim.setAxle01Group(cursor.getString(14));
					wim.setAxle01TireCode(cursor.getString(15));
					wim.setAxle02Seperation(cursor.getString(16));
					wim.setAxle02Weight(cursor.getString(17));
					wim.setAxle02Max(cursor.getString(18));
					wim.setAxle02Group(cursor.getString(19));
					wim.setAxle02TireCode(cursor.getString(20));
					wim.setAxle03Seperation(cursor.getString(21));
					wim.setAxle03Weight(cursor.getString(22));
					wim.setAxle03Max(cursor.getString(23));
					wim.setAxle03Group(cursor.getString(24));
					wim.setAxle03TireCode(cursor.getString(25));
					wim.setAxle04Seperation(cursor.getString(26));
					wim.setAxle04Weight(cursor.getString(27));
					wim.setAxle04Max(cursor.getString(28));
					wim.setAxle04Group(cursor.getString(29));
					wim.setAxle04TireCode(cursor.getString(30));
					wim.setAxle05Seperation(cursor.getString(31));
					wim.setAxle05Weight(cursor.getString(32));
					wim.setAxle05Max(cursor.getString(33));
					wim.setAxle05Group(cursor.getString(34));
					wim.setAxle05TireCode(cursor.getString(35));
					wim.setAxle06Seperation(cursor.getString(36));
					wim.setAxle06Weight(cursor.getString(37));
					wim.setAxle06Max(cursor.getString(38));
					wim.setAxle06Group(cursor.getString(39));
					wim.setAxle06TireCode(cursor.getString(40));
					wim.setAxle07Seperation(cursor.getString(41));
					wim.setAxle07Weight(cursor.getString(42));
					wim.setAxle07Max(cursor.getString(43));
					wim.setAxle07Group(cursor.getString(44));
					wim.setAxle07TireCode(cursor.getString(45));
					wim.setAxle08Seperation(cursor.getString(46));
					wim.setAxle08Weight(cursor.getString(47));
					wim.setAxle08Max(cursor.getString(48));
					wim.setAxle08Group(cursor.getString(49));
					wim.setAxle08TireCode(cursor.getString(50));
					wim.setAxle09Seperation(cursor.getString(51));
					wim.setAxle09Weight(cursor.getString(52));
					wim.setAxle09Max(cursor.getString(53));
					wim.setAxle09Group(cursor.getString(54));
					wim.setAxle09TireCode(cursor.getString(55));
					wim.setAxle10Seperation(cursor.getString(56));
					wim.setAxle10Weight(cursor.getString(57));
					wim.setAxle10Max(cursor.getString(58));
					wim.setAxle10Group(cursor.getString(59));
					wim.setAxle10TireCode(cursor.getString(60));
					wim.setAxle11Seperation(cursor.getString(61));
					wim.setAxle11Weight(cursor.getString(62));
					wim.setAxle11Max(cursor.getString(63));
					wim.setAxle11Group(cursor.getString(64));
					wim.setAxle11TireCode(cursor.getString(65));
					wim.setAxle12Seperation(cursor.getString(66));
					wim.setAxle12Weight(cursor.getString(67));
					wim.setAxle12Max(cursor.getString(68));
					wim.setAxle12Group(cursor.getString(69));
					wim.setAxle12TireCode(cursor.getString(70));
					wim.setAxle13Seperation(cursor.getString(71));
					wim.setAxle13Weight(cursor.getString(72));
					wim.setAxle13Max(cursor.getString(73));
					wim.setAxle13Group(cursor.getString(74));
					wim.setAxle13TireCode(cursor.getString(75));
					wim.setAxle14Seperation(cursor.getString(76));
					wim.setAxle14Weight(cursor.getString(77));
					wim.setAxle14Max(cursor.getString(78));
					wim.setAxle14Group(cursor.getString(79));
					wim.setAxle14TireCode(cursor.getString(80));
					wim.setLength(cursor.getString(81));
					wim.setFrontOverHang(cursor.getString(82));
					wim.setRearOverHang(cursor.getString(83));
					wim.setVehicleType(cursor.getString(84));
					wim.setVehicleClass(cursor.getString(85));
					wim.setRecordType(cursor.getString(86));
					wim.setImageCount(cursor.getString(87));
					wim.setImage01Name(cursor.getString(88));
					wim.setImage02Name(cursor.getString(89));
					wim.setImage03Name(cursor.getString(90));
					wim.setImage04Name(cursor.getString(91));
					wim.setImage05Name(cursor.getString(92));
					wim.setImage06Name(cursor.getString(93));
					wim.setImage07Name(cursor.getString(94));
					wim.setImage08Name(cursor.getString(95));
					wim.setImage09Name(cursor.getString(96));
					wim.setImage10Name(cursor.getString(97));
					wim.setSortDecision(cursor.getString(98));
					wim.setLicensePlateNumber(cursor.getString(99));
					wim.setLicensePlateProvinceID(cursor.getString(100));
					wim.setLicensePlateImageName(cursor.getString(101));
					wim.setLicensePlateConfidence(cursor.getString(102));
					wim.setStatusColor(cursor.getString(103));
					wim.setProvinceName(cursor.getString(104));
					wim.setStationName(cursor.getString(105));

					// Adding Wim to list
					wimList.add(wim);
				} while (cursor.moveToNext());
			}

			// return Wim list
			return wimList;
		}

		// Updating single Wim
		public int updateWim(Wim wim) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_WIMID, wim.getWIMID());
			values.put(KEY_STATIONID, wim.getStationID());
			values.put(KEY_TIMESTAMP, wim.getTimeStamp());
			values.put(KEY_VEHICLENUMBER, wim.getVehicleNumber());
			values.put(KEY_LANE, wim.getLane());
			values.put(KEY_ERROR, wim.getError());
			values.put(KEY_STATUSCODE, wim.getStatusCode());
			values.put(KEY_GVW, wim.getGVW());
			values.put(KEY_MAXGVW, wim.getMaxGVW());
			values.put(KEY_ESAL, wim.getESAL());
			values.put(KEY_SPEED, wim.getSpeed());
			values.put(KEY_AXLECOUNT, wim.getAxleCount());
			values.put(KEY_AXLE01SEPERATION, wim.getAxle01Seperation());
			values.put(KEY_AXLE01WEIGHT, wim.getAxle01Weight());
			values.put(KEY_AXLE01MAX, wim.getAxle01Max());
			values.put(KEY_AXLE01GROUP, wim.getAxle01Group());
			values.put(KEY_AXLE01TIRECODE, wim.getAxle01TireCode());
			values.put(KEY_AXLE02SEPERATION, wim.getAxle02Seperation());
			values.put(KEY_AXLE02WEIGHT, wim.getAxle02Weight());
			values.put(KEY_AXLE02MAX, wim.getAxle02Max());
			values.put(KEY_AXLE02GROUP, wim.getAxle02Group());
			values.put(KEY_AXLE02TIRECODE, wim.getAxle02TireCode());
			values.put(KEY_AXLE03SEPERATION, wim.getAxle03Seperation());
			values.put(KEY_AXLE03WEIGHT, wim.getAxle03Weight());
			values.put(KEY_AXLE03MAX, wim.getAxle03Max());
			values.put(KEY_AXLE03GROUP, wim.getAxle03Group());
			values.put(KEY_AXLE03TIRECODE, wim.getAxle03TireCode());
			values.put(KEY_AXLE04SEPERATION, wim.getAxle04Seperation());
			values.put(KEY_AXLE04WEIGHT, wim.getAxle04Weight());
			values.put(KEY_AXLE04MAX, wim.getAxle04Max());
			values.put(KEY_AXLE04GROUP, wim.getAxle04Group());
			values.put(KEY_AXLE04TIRECODE, wim.getAxle04TireCode());
			values.put(KEY_AXLE05SEPERATION, wim.getAxle05Seperation());
			values.put(KEY_AXLE05WEIGHT, wim.getAxle05Weight());
			values.put(KEY_AXLE05MAX, wim.getAxle05Max());
			values.put(KEY_AXLE05GROUP, wim.getAxle05Group());
			values.put(KEY_AXLE05TIRECODE, wim.getAxle05TireCode());
			values.put(KEY_AXLE06SEPERATION, wim.getAxle06Seperation());
			values.put(KEY_AXLE06WEIGHT, wim.getAxle06Weight());
			values.put(KEY_AXLE06MAX, wim.getAxle06Max());
			values.put(KEY_AXLE06GROUP, wim.getAxle06Group());
			values.put(KEY_AXLE06TIRECODE, wim.getAxle06TireCode());
			values.put(KEY_AXLE07SEPERATION, wim.getAxle07Seperation());
			values.put(KEY_AXLE07WEIGHT, wim.getAxle07Weight());
			values.put(KEY_AXLE07MAX, wim.getAxle07Max());
			values.put(KEY_AXLE07GROUP, wim.getAxle07Group());
			values.put(KEY_AXLE07TIRECODE, wim.getAxle07TireCode());
			values.put(KEY_AXLE08SEPERATION, wim.getAxle08Seperation());
			values.put(KEY_AXLE08WEIGHT, wim.getAxle08Weight());
			values.put(KEY_AXLE08MAX, wim.getAxle08Max());
			values.put(KEY_AXLE08GROUP, wim.getAxle08Group());
			values.put(KEY_AXLE08TIRECODE, wim.getAxle08TireCode());
			values.put(KEY_AXLE09SEPERATION, wim.getAxle09Seperation());
			values.put(KEY_AXLE09WEIGHT, wim.getAxle09Weight());
			values.put(KEY_AXLE09MAX, wim.getAxle09Max());
			values.put(KEY_AXLE09GROUP, wim.getAxle09Group());
			values.put(KEY_AXLE09TIRECODE, wim.getAxle09TireCode());
			values.put(KEY_AXLE10SEPERATION, wim.getAxle10Seperation());
			values.put(KEY_AXLE10WEIGHT, wim.getAxle10Weight());
			values.put(KEY_AXLE10MAX, wim.getAxle10Max());
			values.put(KEY_AXLE10GROUP, wim.getAxle10Group());
			values.put(KEY_AXLE10TIRECODE, wim.getAxle10TireCode());
			values.put(KEY_AXLE11SEPERATION, wim.getAxle11Seperation());
			values.put(KEY_AXLE11WEIGHT, wim.getAxle11Weight());
			values.put(KEY_AXLE11MAX, wim.getAxle11Max());
			values.put(KEY_AXLE11GROUP, wim.getAxle11Group());
			values.put(KEY_AXLE11TIRECODE, wim.getAxle11TireCode());
			values.put(KEY_AXLE12SEPERATION, wim.getAxle12Seperation());
			values.put(KEY_AXLE12WEIGHT, wim.getAxle12Weight());
			values.put(KEY_AXLE12MAX, wim.getAxle12Max());
			values.put(KEY_AXLE12GROUP, wim.getAxle12Group());
			values.put(KEY_AXLE12TIRECODE, wim.getAxle12TireCode());
			values.put(KEY_AXLE13SEPERATION, wim.getAxle13Seperation());
			values.put(KEY_AXLE13WEIGHT, wim.getAxle13Weight());
			values.put(KEY_AXLE13MAX, wim.getAxle13Max());
			values.put(KEY_AXLE13GROUP, wim.getAxle13Group());
			values.put(KEY_AXLE13TIRECODE, wim.getAxle13TireCode());
			values.put(KEY_AXLE14SEPERATION, wim.getAxle14Seperation());
			values.put(KEY_AXLE14WEIGHT, wim.getAxle14Weight());
			values.put(KEY_AXLE14MAX, wim.getAxle14Max());
			values.put(KEY_AXLE14GROUP, wim.getAxle14Group());
			values.put(KEY_AXLE14TIRECODE, wim.getAxle14TireCode());
			values.put(KEY_LENGTH, wim.getLength());
			values.put(KEY_FRONTOVERHANG, wim.getFrontOverHang());
			values.put(KEY_REAROVERHANG, wim.getRearOverHang());
			values.put(KEY_VEHICLETYPE, wim.getVehicleType());
			values.put(KEY_VEHICLECLASS, wim.getVehicleClass());
			values.put(KEY_RECORDTYPE, wim.getRecordType());
			values.put(KEY_IMAGECOUNT, wim.getImageCount());
			values.put(KEY_IMAGE01NAME, wim.getImage01Name());
			values.put(KEY_IMAGE02NAME, wim.getImage02Name());
			values.put(KEY_IMAGE03NAME, wim.getImage03Name());
			values.put(KEY_IMAGE04NAME, wim.getImage04Name());
			values.put(KEY_IMAGE05NAME, wim.getImage05Name());
			values.put(KEY_IMAGE06NAME, wim.getImage06Name());
			values.put(KEY_IMAGE07NAME, wim.getImage07Name());
			values.put(KEY_IMAGE08NAME, wim.getImage08Name());
			values.put(KEY_IMAGE09NAME, wim.getImage09Name());
			values.put(KEY_IMAGE10NAME, wim.getImage10Name());
			values.put(KEY_SORTDECISION, wim.getSortDecision());
			values.put(KEY_LICENSEPLATENUMBER, wim.getLicensePlateNumber());
			values.put(KEY_LICENSEPLATEPROVINCEID, wim.getLicensePlateProvinceID());
			values.put(KEY_LICENSEPLATEIMAGENAME, wim.getLicensePlateImageName());
			values.put(KEY_LICENSEPLATECONFIDENCE, wim.getLicensePlateConfidence());
			values.put(KEY_STATUSCOLOR, wim.getStatusColor());
			values.put(KEY_PROVINCENAME, wim.getProvinceName());
			values.put(KEY_STATIONNAME, wim.getStationName());

			// updating row
			return db.update(TABLE_WIM, values, KEY_WIMID + " = ?",
					new String[] { String.valueOf(wim.getWIMID()) });
		}

		// Deleting single Wim
		public void deleteWim(Wim Wim) {
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_WIM, KEY_WIMID + " = ?",
					new String[] { String.valueOf(Wim.getWIMID()) });
			db.close();
		}

		// Getting STATON Count
		public int getWimCount() {
			
			String countQuery = "SELECT  * FROM " + TABLE_WIM;
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(countQuery, null);
			cursor.close();

			// return count
			return cursor.getCount();
		}
		
}
