﻿using System;
using System.Collections.Generic;

using System.Data;
using ICMS.DAL;
using ICMS.MODEL;
using ICMS.DAO;
using MySql.Data.MySqlClient;

namespace KUAssetWeb.DAL
{
    class PlayTimeDAO
    {
        //Get Data
        public static List<PlayTime> select(String _criteria)
        {
            String sql = "select * from tbl_playtime "+_criteria;
            List<PlayTime> lists = new List<PlayTime>();
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            int running = 1;
            while (dr.Read())
            {
                PlayTime playTime = new PlayTime();
                if (DBNull.Value != dr["ID"])
                    playTime.Id = Convert.ToInt16(dr["ID"]);
                if (DBNull.Value != dr["com_no"])
                    playTime.Com_no = Convert.ToString(dr["com_no"]);
                if (DBNull.Value != dr["start_time"])
                    playTime.Start_time = Convert.ToDateTime(dr["start_time"]);
                if (DBNull.Value != dr["end_time"])
                    playTime.End_time = Convert.ToDateTime(dr["end_time"]);
                if (DBNull.Value != dr["create_date"])
                    playTime.Create_date = Convert.ToDateTime(dr["create_date"]);
                if (DBNull.Value != dr["Play_type"])
                    playTime.Play_type = Convert.ToString(dr["Play_type"]);
                if (DBNull.Value != dr["alert_time"])
                    playTime.Alert_time = Convert.ToDateTime(dr["alert_time"]);
                if (DBNull.Value != dr["amount"])
                    playTime.Amount = Convert.ToDouble(dr["amount"]);
                lists.Add(playTime);
                running++;
            }
            dr.Close();
            return lists;
        }

        public static PlayTime selectById(PlayTime _playTime)
        {
            PlayTime playTime = new PlayTime();
            String sql = String.Format("select * from tbl_playtime where id={0}", _playTime.Id);
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;

            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                if (DBNull.Value != dr["com_no"])
                    playTime.Com_no = Convert.ToString(dr["com_no"]);
                if (DBNull.Value != dr["start_time"])
                    playTime.Start_time = Convert.ToDateTime(dr["start_time"]);
                if (DBNull.Value != dr["end_time"])
                    playTime.End_time = Convert.ToDateTime(dr["end_time"]);
                if (DBNull.Value != dr["create_date"])
                    playTime.Create_date = Convert.ToDateTime(dr["create_date"]);
                if (DBNull.Value != dr["Play_type"])
                    playTime.Play_type = Convert.ToString(dr["Play_type"]);
                if (DBNull.Value != dr["alert_time"])
                    playTime.Alert_time = Convert.ToDateTime(dr["alert_time"]);
                if (DBNull.Value != dr["amount"])
                    playTime.Amount = Convert.ToDouble(dr["amount"]);
            }
            dr.Close();
            return playTime;
        }
        //Insert,Delete,Update
        public static int insert(PlayTime playTime)
        {
            String sql = "";
            //if (playTime.Alert_time.Year == 1)
            //{
            //    sql = String.Format("insert into tbl_playtime(com_no,start_time,create_date,play_type,amount,keyuser) values('{0}','{1}','{2}','{3}',{4},'{5}')", playTime.Com_no, playTime.Start_time, playTime.Create_date, playTime.Play_type, playTime.Amount,playTime.Keyuser);
            //}
            //else {
            //    sql = String.Format("insert into tbl_playtime(com_no,start_time,create_date,play_type,alert_time,amount,keyuser) values('{0}','{1}','{2}','{3}','{4}',{5},'{6}')", playTime.Com_no, playTime.Start_time, playTime.Create_date, playTime.Play_type, playTime.Alert_time, playTime.Amount,playTime.Keyuser);
            //}
            if (playTime.Alert_time.Year == 1)
            {
                sql = String.Format("insert into tbl_playtime(com_no,start_time,create_date,play_type,amount) values('{0}','{1}','{2}','{3}',{4})", playTime.Com_no, playTime.Start_time, playTime.Create_date, playTime.Play_type, playTime.Amount);
            }
            else
            {
                sql = String.Format("insert into tbl_playtime(com_no,start_time,create_date,play_type,alert_time,amount) values('{0}','{1}','{2}','{3}','{4}',{5})", playTime.Com_no, playTime.Start_time, playTime.Create_date, playTime.Play_type, playTime.Alert_time, playTime.Amount);
            }
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);

            cmd.CommandType = CommandType.Text;

            cmd.Connection.Open();
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            if (i > 0)
            {
                try
                {
                    Console.WriteLine("Post data result:" + DBUtil.postData("http://prdapp.net/cinetapp/cinet_service.php?action=insert&sql=" + sql));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return i;
        }
        
        public static int delete(PlayTime playTime)
        {
            String sql = "delete from tbl_playtime where id={0}";
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(String.Format(sql, playTime.Id), connection);
            cmd.CommandType = CommandType.Text;
            //cmd.Parameters.Add(new OleDbParameter("id", id));
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            if (i > 0)
            {
                try
                {
                    Console.WriteLine("Post data result:" + DBUtil.postData("http://prdapp.net/cinetapp/cinet_service.php?action=delete&sql=" + sql));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return i;
        }

        public static int update(PlayTime playTime)
        {
            String sql = String.Format("update tbl_playtime set start_time='{0}',end_time='{1}',play_type='{2}',alert_time='{3}',amount={4} where com_no='{5}' and Format (create_date, 'yyyymmdd') = Format('{6}','yyyymmdd') and id={7}", playTime.Start_time, playTime.End_time, playTime.Play_type, playTime.Alert_time, playTime.Amount,playTime.Com_no,playTime.Create_date,playTime.Id);
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            int i = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            if (i > 0)
            {
                try
                {
                    String pSql = sql = String.Format("insert into tbl_playtime(id,com_no,start_time,create_date,play_type,amount) values({0},'{1}','{2}',NOW(),{4},{5})",playTime.Id, playTime.Com_no, playTime.Start_time, playTime.Create_date, playTime.Play_type, playTime.Amount);
                    Console.WriteLine("Post data result:" + DBUtil.postData("http://prdapp.net/cinetapp/cinet_service.php?action=update&sql=" + pSql));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return i;
        }

        public static bool isPlaying(PlayTime playTime)
        {
            PlayTime pt = new PlayTime();
            String sql = String.Format("select com_no,start_time,end_time from tbl_playtime where Format (create_date, 'yyyymmdd') = Format('{0}','yyyymmdd') and com_no='{1}'", playTime.Create_date,playTime.Com_no);
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                if (DBNull.Value == dr["com_no"])
                    playTime.Com_no = Convert.ToString(dr["com_no"]);
                if (DBNull.Value == dr["start_time"])
                    playTime.Start_time = Convert.ToDateTime(dr["start_time"]);
                if (DBNull.Value == dr["end_time"])
                {
                    return true;
                }
            }
            dr.Close();
            return false;
        }

        public static PlayTime excuteSQL(String sql)
        {
            PlayTime playTime = null;
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                playTime = new PlayTime();
                if (DBNull.Value != dr["id"])
                    playTime.Id = Convert.ToInt16(dr["id"]);
                if (DBNull.Value != dr["com_no"])
                    playTime.Com_no = Convert.ToString(dr["com_no"]);
                if (DBNull.Value != dr["start_time"])
                    playTime.Start_time = Convert.ToDateTime(dr["start_time"]);
                if (DBNull.Value != dr["end_time"])
                    playTime.End_time = Convert.ToDateTime(dr["end_time"]);
                if (DBNull.Value != dr["create_date"])
                    playTime.Create_date = Convert.ToDateTime(dr["create_date"]);
                if (DBNull.Value != dr["Play_type"])
                    playTime.Play_type = Convert.ToString(dr["Play_type"]);
                //if (DBNull.Value != dr["alert_time"])
                //    playTime.Alert_time = Convert.ToDateTime(dr["alert_time"]);
                //if (DBNull.Value != dr["amount"])
                //    playTime.Amount = Convert.ToDouble(dr["amount"]);
            }
            dr.Close();
            return playTime;
        }

        public static double getIncome(DateTime _date)
        {
            double amount = 0;
            String sql = String.Format("select sum(amount) as amount from tbl_playtime where format(create_date,'yyyymmdd')=format('{0}','yyyymmdd')",_date);
            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                if (DBNull.Value != dr["amount"])
                    amount = Convert.ToDouble(dr["amount"]);
            }
            return amount;
        }

        public static List<PlayTime> getAlert()
        {
            List<PlayTime> lists = new List<PlayTime>();
            String criteria = "Format (create_date, 'yyyymmdd') = Format('" + DateTime.Now + "','yyyymmdd')";

            MySqlConnection connection = new MySqlConnection(Connection.ConnectionString());
            connection.Open();
            //Create Command
            MySqlCommand cmd = new MySqlCommand("select * from tbl_playtime where play_type ='1' and end_time is null and " + criteria, connection);

            cmd.CommandType = CommandType.Text;
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            int running = 1;
            while (dr.Read())
            {
                PlayTime playTime = new PlayTime();
                if (DBNull.Value != dr["com_no"])
                    playTime.Com_no = Convert.ToString(dr["com_no"]);
                if (DBNull.Value != dr["start_time"])
                    playTime.Start_time = Convert.ToDateTime(dr["start_time"]);
                if (DBNull.Value != dr["end_time"])
                    playTime.End_time = Convert.ToDateTime(dr["end_time"]);
                if (DBNull.Value != dr["create_date"])
                    playTime.Create_date = Convert.ToDateTime(dr["create_date"]);
                if (DBNull.Value != dr["Play_type"])
                    playTime.Play_type = Convert.ToString(dr["Play_type"]);
                if (DBNull.Value != dr["alert_time"])
                    playTime.Alert_time = Convert.ToDateTime(dr["alert_time"]);
                if (DBNull.Value != dr["amount"])
                    playTime.Amount = Convert.ToDouble(dr["amount"]);
                lists.Add(playTime);
                running++;
            }
            dr.Close();
            return lists;
        }    
    }
}
