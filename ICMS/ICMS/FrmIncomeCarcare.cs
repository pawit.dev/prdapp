﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KUAssetWeb.DAL;
using ICMS.MODEL;

namespace ICMS
{
    public partial class FrmIncomeCarcare : Form
    {
        private int totalCost = 0;
        private int incomeID = 0;
        private int cartype = 0;
        private Income income;
        public FrmIncomeCarcare()
        {
            InitializeComponent();
        }
        public FrmIncomeCarcare(int _incomeID)
        {
            InitializeComponent();
            this.incomeID = _incomeID;
        }
        private void FrmCheckInCom_Load(object sender, EventArgs e)
        {
            List<IncomeType> lists = IncomeTypeDAO.select("");
            if (lists.Count > 0)
            {
                comboBox1.DataSource = lists;
            }

            if (this.incomeID != 0)
            {
                this.income = IncomeDAO.selectById(new Income { id = this.incomeID });
                if (this.income != null)
                {
                    //udpate 
                    comboBox1.SelectedValue = this.income.Income_type; //IncomeTypeDAO.selectById(new IncomeType{Id= this.income.Income_type}).Incometype_name;
                    //TXT_COUNT.Text = this.income.Income_count + "";
                    switch(this.income.income_type_car)
                    {
                        case 1: radioButton1.Checked = true;
                            break;
                        case 2: radioButton2.Checked = true;
                            break;
                        case 3: radioButton3.Checked = true;
                            break;
                        case 4: radioButton4.Checked = true;
                            break;
                        case 5: radioButton5.Checked = true;
                            break;
                        case 6: radioButton6.Checked = true;
                            break;
                        case 7: radioButton7.Checked = true;
                            break;
                        default:
                            radioButton1.Checked = false;
                            radioButton2.Checked = false;
                            radioButton3.Checked = false;
                            radioButton4.Checked = false;
                            radioButton5.Checked = false;
                            radioButton6.Checked = false;
                            radioButton7.Checked = false;
                            break;
                    }
                    
                    TXT_AMOUNT.Text = this.income.Income_amount + "";
                    TXT_REMARK.Text = this.income.Income_remark;
                    CMD_SAVE.Text = "แก้ไข";
                    CMD_DELETE.Enabled = true;
                    comboBox1.Enabled = true;
                }
                else
                {
                    MessageBox.Show("ไม่พบข้อมูลที่ต้องการแก้ไข (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                //normal insert
                CMD_SAVE.Text = "บันทึก";
                CMD_DELETE.Enabled = false;
                comboBox1.Enabled = true;
                radioButton1.Checked = true;
            }
        }

        private void CMD_SAVE_Click(object sender, EventArgs e)
        {
            Button bnt = (Button)sender;
            int result = 0;
            switch (bnt.Name)
            {
                case "CMD_SAVE":
                    if (TXT_REMARK.Text.Equals("") )
                    {
                        MessageBox.Show("ยังไม่ได้ป้อนเลขทะเบียน", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        TXT_REMARK.Focus();
                    }
                    else if (TXT_AMOUNT.Text.Equals("") || TXT_AMOUNT.Text.Equals("0"))
                    {
                        MessageBox.Show("จำนวนเงินต้องมีค่ามากกว่า 0", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        TXT_AMOUNT.Focus();
                    }
                    else
                    {
                        Income _income = new Income();
                        _income.id = this.incomeID;
                        _income.Income_type = Convert.ToInt16(comboBox1.SelectedValue);
                        _income.Income_count = 1;
                        _income.Income_amount = (_income.Income_count * Convert.ToInt16(TXT_AMOUNT.Text));
                        _income.Income_remark = TXT_REMARK.Text;
                        _income.Income_date = DateTime.Now;
                        _income.income_type_car = cartype;
                        _income.Keyuser = "";

                        switch (bnt.Text)
                        {
                            case "บันทึก":
                                result = IncomeDAO.insert(_income);
                                if (result > 0)
                                {
                                    MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("เกิดข้อผิดพลาดในการบันทึกข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                break;
                            case "แก้ไข":
                                result = IncomeDAO.update(_income);
                                if (result > 0)
                                {
                                    MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("เกิดข้อผิดพลาดในการแก้ไขข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                break;
                        }
                        Close();
                    }
                    break;
                case "CMD_DELETE":
                    result = IncomeDAO.delete(new Income { id = this.incomeID });
                    if (result > 0)
                    {
                        MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดในการลบข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    Close();
                    break;
                case "CMD_CANCEL":
                    Close();
                    break;

            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            /*
                    1)4ประตู			220/300
                    2)ตอนครึ่ง			200/280
                    3)ตอนเดียว			180/260
                    4)เก๋งไซส์ใหย่			220
                    5)เก๋งกลาง			200
                    6)เก๋งเล็ก			180
                    7)มอไซด์พิเศษ 		80/100            
             */
            RadioButton rb = (RadioButton)sender;
            switch (rb.Name)
            {
                case "radioButton1":
                    totalCost = (checkBox1.Checked) ? 300 : 220;
                    break;
                case "radioButton2":
                    totalCost = (checkBox1.Checked) ? 280 : 200;
                    break;
                case "radioButton3":
                    totalCost = (checkBox1.Checked) ? 260 : 180;
                    break;
                case "radioButton4":
                    totalCost = (checkBox1.Checked) ? 220 : 220;
                    break;
                case "radioButton5":
                    totalCost = (checkBox1.Checked) ? 200 : 200;
                    break;
                case "radioButton6":
                    totalCost = (checkBox1.Checked) ? 180 : 180;
                    break;
                case "radioButton7":
                    totalCost = (checkBox1.Checked) ? 100 : 80;
                    break;
                default:
                    totalCost = (checkBox1.Checked) ? 0 : 0;
                    break;
            }
            TXT_AMOUNT.Text = totalCost + "";
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton1.Checked)
            {
                totalCost = (checkBox1.Checked) ? 300 : 220;
            }
            else if (radioButton2.Checked)
            {
                totalCost = (checkBox1.Checked) ? 280 : 200;
            }
            else if (radioButton3.Checked)
            {
                totalCost = (checkBox1.Checked) ? 260 : 180;
            }
            else if (radioButton4.Checked)
            {
                totalCost = (checkBox1.Checked) ? 220 : 220;
            }
            else if (radioButton5.Checked)
            {
                totalCost = (checkBox1.Checked) ? 200 : 200;
            }
            else if (radioButton6.Checked)
            {
                totalCost = (checkBox1.Checked) ? 180 : 180;
            }
            else if (radioButton6.Checked)
            {
                totalCost = (checkBox1.Checked) ? 100 : 80;
            }
            else
            {

            }
            TXT_AMOUNT.Text = totalCost + "";
        }

    }
}
