﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KUAssetWeb.DAL;
using ICMS.MODEL;

namespace ICMS
{
    public partial class FrmCheckInCom : Form
    {
        private PlayTime pt = null;
        private int comindex = 0;
        private int pk_comid = 0;
        private double total_amount = 0;        
        public FrmCheckInCom()
        {
            InitializeComponent();
        }

        public FrmCheckInCom(int _comindex,int _pk_comid)
        {
            InitializeComponent();
            this.comindex = _comindex;
            this.pk_comid = _pk_comid;

        }

        private void FrmCheckInCom_Load(object sender, EventArgs e)
        {
            this.Text = String.Format(this.Text, this.comindex.ToString("00"));
            LB_Header.Text = this.Text;
            String sql = String.Format("select id,com_no,start_time,end_time,create_date,play_type from tbl_playtime where Format (create_date, 'yyyymmdd') = Format('{0}','yyyymmdd') and com_no='{1}' and id={2} and end_time is null", DateTime.Now, this.comindex, this.pk_comid);
            this.pt = PlayTimeDAO.excuteSQL(sql);
            tabControl1.Focus();

            //กำหนดหน้าที่จะเปิดตอนทำรายการใช้เครื่อง ซึ่งถ้ามีการใช้เครื่องนี้อยู่แล้วให้แสดงหน้า "คิดเงิน"
            if (this.pt == null)
            {
               
                tabControl1.SelectedIndex = 0;                
            }
            else
            {
                tabControl1.SelectedIndex = 1;
            }
        }

        private void CMD_SAVE_Click(object sender, EventArgs e)
        {
            Button bnt = (Button)sender;

            switch (bnt.Name)
            {
                case "CMD_SAVE":
                    switch (tabControl1.SelectedIndex)
                    {
                        case 0://ใช้เครื่อง
                            //ตรวจสอบว่ายังเล่นอยู่หรือไม่
                            if (PlayTimeDAO.isPlaying(new PlayTime { Create_date = DateTime.Now, Com_no = this.comindex + "" }))
                            {
                                MessageBox.Show("เครื่องนี้ยังเล่นอยู่ (ต้องทำรายการชำระเงินก่อนที่จะเล่นต่อ)",Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Stop);
                            }
                            else
                            {

                                string play_type = "1";
                                int hr = 0;
                                if (radioButton1.Checked)
                                {
                                    hr = 15;
                                    play_type = "1";
                                }
                                else if (radioButton2.Checked)
                                {
                                    hr = 30;
                                    play_type = "1";
                                }
                                else if (radioButton3.Checked)
                                {
                                    hr = 60;
                                    play_type = "1";
                                }
                                else if (radioButton4.Checked)
                                {
                                    hr = 120;
                                    play_type = "1";
                                }
                                else if (radioButton5.Checked)
                                {
                                    hr = 180;
                                    play_type = "1";
                                }
                                else if (radioButton6.Checked)
                                {
                                    hr = 240;
                                    play_type = "1";
                                }
                                else if (radioButton7.Checked)
                                {
                                    hr = Convert.ToInt16(TXT_MINUTE.Text);
                                    play_type = "1";
                                }
                                else
                                {
                                    play_type = "2";//เล่นไปเรื่อย ๆ (เก็บเงินทีหลัง)
                                }
                                PlayTime pt = new PlayTime
                                {
                                    Com_no = this.comindex + "",
                                    Start_time = DateTime.Now,
                                    Create_date = DateTime.Now,
                                    Play_type = play_type,
                                    Keyuser = "0000",
                                    Amount = 0
                                };
                                if (play_type.Equals("1"))
                                {
                                    pt.Alert_time = DateTime.Now.AddMinutes(hr);
                                }

                                int result = PlayTimeDAO.insert(pt);
                                if (result > 0)
                                {
                                    MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("เกิดความผิดพลาดในการบันทึกข้อมูล (ติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                Close();
                            }
                            break;
                        case 1://คิดเงิน

                            if (this.pt != null)
                            {
                                this.pt.End_time = Convert.ToDateTime(DateTime.Now);
                                this.pt.Amount = Math.Floor( Convert.ToDouble(TXT_AMOUNT.Text));
                                
                                int result = PlayTimeDAO.update(this.pt);
                                if (result > 0)
                                {
                                    MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("เกิดความผิดพลาดในการบันทึกข้อมูล (ติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                Close();
                            }
                            break;
                    }

                    break;
                case "CMD_CANCEL":
                    Close();
                    break;
            }
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
            {
                if (pt != null)
                {

                    L_SUM_HEAD.Text = String.Format(L_SUM_HEAD.Text, pt.Start_time.ToShortTimeString(), DateTime.Now.ToShortTimeString());
                    int end_time = (DateTime.Now.Hour * 60) + (DateTime.Now.Minute);
                    int start_time = (pt.Start_time.Hour * 60) + (pt.Start_time.Minute);
                    int total_minute = end_time - start_time;
                    total_amount = total_minute * Constants.PLAY_REATE_PER_MINUTE;
                    int total_h = total_minute / 60;
                    int total_m = total_minute % 60;
                    
                    L_TOTAL_HR.Text = String.Format(L_TOTAL_HR.Text, ((total_h == 0) ? total_m + "" : (total_h + " ชั่วโมง " + total_m)));
                    TXT_AMOUNT.Text = Math.Floor(total_amount) + "";

                    TXT_AMOUNT.Visible = true;
                    L_TOTAL_AMOUNT.Visible = true;
                    label2.Visible = true;
                }
                else {
                    L_SUM_HEAD.Text = "ยังไม่มีการใช้งานเครื่อง";
                    L_TOTAL_HR.Text = "";
                    TXT_AMOUNT.Visible = false;
                    L_TOTAL_AMOUNT.Visible = false;
                    label2.Visible = false;
                    
                }
            }
        }

    }
}
