﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICMS
{
    class Constants
    {
        public const int COM_TOTAL = 14;
        public const int ALERT_TIME = 1;//1 minute
        public const double PLAY_REATE_PER_MINUTE = 0.33;

        //table
        public const String SQL_TBL_INCOME = 
                    "CREATE TABLE [tbl_income] (" +
                        "[ID] AUTOINCREMENT," +
                        "[income_type] LONG," +
                        "[income_count] LONG," +
                        "[income_amount] LONG," +
                        "[income_remark] TEXT(255) WITH COMPRESSION," +
                        "[income_date] DATETIME," +
                        "[keyuser] TEXT(255) WITH COMPRESSION," +
                        "CONSTRAINT [PrimaryKey] PRIMARY KEY ([ID]));";
        public const String SQL_TBL_INCOMETYPE = 
                        "CREATE TABLE [tbl_incometype] ("+
                        "	[ID] AUTOINCREMENT,"+
                        "	[incometype_name] TEXT(255) WITH COMPRESSION,"+
                        "	[keyuser] TEXT(255) WITH COMPRESSION,"+
                        "	CONSTRAINT [PrimaryKey] PRIMARY KEY ([ID])" +
                        ");";
        public const String SQL_TBL_PLAYTIME = 
                        "CREATE TABLE [tbl_playtime] ("+
	                        "[ID] AUTOINCREMENT,"+
	                        "[com_no] TEXT(255) WITH COMPRESSION,"+
	                        "[start_time] DATETIME,"+
	                        "[end_time] DATETIME DEFAULT Null,"+
	                        "[play_type] TEXT(255) WITH COMPRESSION,"+
	                        "[create_date] DATETIME,"+
	                        "[alert_time] DATETIME,"+
	                        "[amount] LONG,"+
	                        "[keyuser] TEXT(255) WITH COMPRESSION,"+
	                        "CONSTRAINT [PrimaryKey] PRIMARY KEY ([ID])"+
                        ");";
    }
}
