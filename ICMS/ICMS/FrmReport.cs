﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KUAssetWeb.DAL;
using ICMS.MODEL;
using ICMS.DAO;

namespace ICMS
{
    public partial class FrmReport : Form
    {
        public FrmReport()
        {
            InitializeComponent();
        }

        private void FrmCheckInCom_Load(object sender, EventArgs e)
        {
            refreshData();
        }

        private void refreshData()
        {
            List<Report01> lists = ReportDAO.report01();
            if (lists.Count > 0)
            {
                Report01 _tmp = new Report01();
                _tmp.Name = "                              รวมเป็นเงิน";
                foreach (Report01 rpt01 in lists)
                {
                    _tmp.Amount += rpt01.Amount;
                }
                lists.Add(_tmp);
                dataGridView1.DataSource = lists;
            }

        }

    }
}
