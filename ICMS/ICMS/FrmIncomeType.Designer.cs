﻿namespace ICMS
{
    partial class FrmIncomeType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CMD_SAVE = new System.Windows.Forms.Button();
            this.CMD_CANCEL = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.LB_Header = new System.Windows.Forms.Label();
            this.TXT_NAME = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TXT_ID = new System.Windows.Forms.TextBox();
            this.CMD_DELETE = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incometypenameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keyuserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incomeTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playTimeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incomeTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playTimeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // CMD_SAVE
            // 
            this.CMD_SAVE.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CMD_SAVE.Image = global::ICMS.Properties.Resources.disk_blue_ok;
            this.CMD_SAVE.Location = new System.Drawing.Point(172, 143);
            this.CMD_SAVE.Name = "CMD_SAVE";
            this.CMD_SAVE.Size = new System.Drawing.Size(106, 46);
            this.CMD_SAVE.TabIndex = 0;
            this.CMD_SAVE.Text = "บันทึก";
            this.CMD_SAVE.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CMD_SAVE.UseVisualStyleBackColor = true;
            this.CMD_SAVE.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // CMD_CANCEL
            // 
            this.CMD_CANCEL.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CMD_CANCEL.Image = global::ICMS.Properties.Resources.redo;
            this.CMD_CANCEL.Location = new System.Drawing.Point(396, 143);
            this.CMD_CANCEL.Name = "CMD_CANCEL";
            this.CMD_CANCEL.Size = new System.Drawing.Size(106, 46);
            this.CMD_CANCEL.TabIndex = 1;
            this.CMD_CANCEL.Text = "ยกเลิก";
            this.CMD_CANCEL.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CMD_CANCEL.UseVisualStyleBackColor = true;
            this.CMD_CANCEL.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::ICMS.Properties.Resources.folder_document;
            this.pictureBox1.Location = new System.Drawing.Point(10, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(39, 36);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(-2, -2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(792, 50);
            this.pictureBox2.TabIndex = 119;
            this.pictureBox2.TabStop = false;
            // 
            // LB_Header
            // 
            this.LB_Header.AutoSize = true;
            this.LB_Header.BackColor = System.Drawing.Color.White;
            this.LB_Header.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LB_Header.ForeColor = System.Drawing.Color.Green;
            this.LB_Header.Location = new System.Drawing.Point(69, 9);
            this.LB_Header.Name = "LB_Header";
            this.LB_Header.Size = new System.Drawing.Size(168, 25);
            this.LB_Header.TabIndex = 120;
            this.LB_Header.Text = "ประเภทการชำระ";
            // 
            // TXT_NAME
            // 
            this.TXT_NAME.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_NAME.Location = new System.Drawing.Point(105, 47);
            this.TXT_NAME.Name = "TXT_NAME";
            this.TXT_NAME.Size = new System.Drawing.Size(187, 22);
            this.TXT_NAME.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(77, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "รหัส";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TXT_ID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TXT_NAME);
            this.groupBox1.Location = new System.Drawing.Point(12, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(490, 83);
            this.groupBox1.TabIndex = 121;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ข้อมูลบันทึก";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(77, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 14);
            this.label1.TabIndex = 20;
            this.label1.Text = "ชื่อ";
            // 
            // TXT_ID
            // 
            this.TXT_ID.Enabled = false;
            this.TXT_ID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_ID.Location = new System.Drawing.Point(105, 19);
            this.TXT_ID.Name = "TXT_ID";
            this.TXT_ID.Size = new System.Drawing.Size(58, 22);
            this.TXT_ID.TabIndex = 19;
            this.TXT_ID.Text = "0";
            this.TXT_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CMD_DELETE
            // 
            this.CMD_DELETE.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CMD_DELETE.Image = global::ICMS.Properties.Resources.error;
            this.CMD_DELETE.Location = new System.Drawing.Point(284, 143);
            this.CMD_DELETE.Name = "CMD_DELETE";
            this.CMD_DELETE.Size = new System.Drawing.Size(106, 46);
            this.CMD_DELETE.TabIndex = 122;
            this.CMD_DELETE.Text = "ลบ";
            this.CMD_DELETE.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CMD_DELETE.UseVisualStyleBackColor = true;
            this.CMD_DELETE.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.incometypenameDataGridViewTextBoxColumn,
            this.keyuserDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.incomeTypeBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 17);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(484, 222);
            this.dataGridView1.TabIndex = 123;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox2.Location = new System.Drawing.Point(12, 195);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(490, 242);
            this.groupBox2.TabIndex = 124;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ประเภทการชำระ";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "รหัส";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Width = 50;
            // 
            // incometypenameDataGridViewTextBoxColumn
            // 
            this.incometypenameDataGridViewTextBoxColumn.DataPropertyName = "Incometype_name";
            this.incometypenameDataGridViewTextBoxColumn.HeaderText = "ชื่อประเภท";
            this.incometypenameDataGridViewTextBoxColumn.Name = "incometypenameDataGridViewTextBoxColumn";
            this.incometypenameDataGridViewTextBoxColumn.Width = 200;
            // 
            // keyuserDataGridViewTextBoxColumn
            // 
            this.keyuserDataGridViewTextBoxColumn.DataPropertyName = "Keyuser";
            this.keyuserDataGridViewTextBoxColumn.HeaderText = "Keyuser";
            this.keyuserDataGridViewTextBoxColumn.Name = "keyuserDataGridViewTextBoxColumn";
            this.keyuserDataGridViewTextBoxColumn.Visible = false;
            // 
            // incomeTypeBindingSource
            // 
            this.incomeTypeBindingSource.DataSource = typeof(ICMS.MODEL.IncomeType);
            // 
            // playTimeBindingSource
            // 
            this.playTimeBindingSource.DataSource = typeof(ICMS.MODEL.PlayTime);
            // 
            // FrmIncomeType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 449);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.CMD_DELETE);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.LB_Header);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.CMD_CANCEL);
            this.Controls.Add(this.CMD_SAVE);
            this.Name = "FrmIncomeType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ประเภทการชำระ";
            this.Load += new System.EventHandler(this.FrmCheckInCom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incomeTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playTimeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CMD_CANCEL;
        private System.Windows.Forms.Button CMD_SAVE;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label LB_Header;
        private System.Windows.Forms.BindingSource incomeTypeBindingSource;
        private System.Windows.Forms.BindingSource playTimeBindingSource;
        private System.Windows.Forms.TextBox TXT_NAME;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button CMD_DELETE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXT_ID;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn incometypenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn keyuserDataGridViewTextBoxColumn;
    }
}