﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KUAssetWeb.DAL;
using ICMS.MODEL;

namespace ICMS
{
    public partial class FrmIncome : Form
    {
        private int incomeID = 0;
        private Income income;
        public FrmIncome()
        {
            InitializeComponent();
        }
        public FrmIncome(int _incomeID)
        {
            InitializeComponent();
            this.incomeID = _incomeID;
        }
        private void FrmCheckInCom_Load(object sender, EventArgs e)
        {
            List<IncomeType> lists = IncomeTypeDAO.select("");
            if (lists.Count > 0)
            {
                comboBox1.DataSource = lists;
            }

            if (this.incomeID != 0)
            {
                this.income = IncomeDAO.selectById(new Income { id = this.incomeID });
                if (this.income != null)
                {
                    //udpate 
                    comboBox1.SelectedValue = this.income.Income_type; //IncomeTypeDAO.selectById(new IncomeType{Id= this.income.Income_type}).Incometype_name;
                    TXT_COUNT.Text = this.income.Income_count + "";
                    TXT_AMOUNT.Text = this.income.Income_amount + "";
                    TXT_REMARK.Text = this.income.Income_remark;
                    CMD_SAVE.Text = "แก้ไข";
                    CMD_DELETE.Enabled = true;
                    comboBox1.Enabled = true;
                }
                else
                {
                    MessageBox.Show("ไม่พบข้อมูลที่ต้องการแก้ไข (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                //normal insert
                CMD_SAVE.Text = "บันทึก";
                CMD_DELETE.Enabled = false;
                comboBox1.Enabled = true;
            }
        }

        private void CMD_SAVE_Click(object sender, EventArgs e)
        {
            Button bnt = (Button)sender;
            int result = 0;
            switch (bnt.Name)
            {
                case "CMD_SAVE":
                    if (TXT_COUNT.Text.Equals("") || TXT_COUNT.Text.Equals("0"))
                    {
                        MessageBox.Show("จำนวนต้องมีค่ามากกว่า 0", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        TXT_COUNT.Focus();
                    }
                    else if (TXT_AMOUNT.Text.Equals("") || TXT_AMOUNT.Text.Equals("0"))
                    {
                        MessageBox.Show("จำนวนเงินต้องมีค่ามากกว่า 0", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        TXT_AMOUNT.Focus();
                    }
                    else
                    {
                        Income _income = new Income();
                        _income.id = this.incomeID;
                        _income.Income_type = Convert.ToInt16(comboBox1.SelectedValue);
                        _income.Income_count = Convert.ToInt16(TXT_COUNT.Text);
                        _income.Income_amount = (_income.Income_count * Convert.ToInt16(TXT_AMOUNT.Text));
                        _income.Income_remark = TXT_REMARK.Text;
                        _income.Income_date = DateTime.Now;
                        _income.Keyuser = "";

                        switch (bnt.Text)
                        {
                            case "บันทึก":
                                result = IncomeDAO.insert(_income);
                                if (result > 0)
                                {
                                    MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("เกิดข้อผิดพลาดในการบันทึกข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                break;
                            case "แก้ไข":
                                result = IncomeDAO.update(_income);
                                if (result > 0)
                                {
                                    MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("เกิดข้อผิดพลาดในการแก้ไขข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                break;
                        }
                        Close();
                    }
                    break;
                case "CMD_DELETE":
                    result = IncomeDAO.delete(new Income { id = this.incomeID });
                    if (result > 0)
                    {
                        MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("เกิดข้อผิดพลาดในการลบข้อมูล (กรุณาติดต่อผู้พัฒนาระบบ)", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    Close();
                    break;
                case "CMD_CANCEL":
                    Close();
                    break;

            }
        }


    }
}
