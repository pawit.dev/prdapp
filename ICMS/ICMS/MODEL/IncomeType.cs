﻿using System;

namespace ICMS.MODEL
{
    class IncomeType : Default
    {

        private String incometype_name;

        public String Incometype_name
        {
            get { return incometype_name; }
            set { incometype_name = value; }
        }
    }
}
