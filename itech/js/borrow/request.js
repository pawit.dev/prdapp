var listArea;
var noItem;
$(function() {
	listArea = $('#equipmentList');
	noItem = $('<div><i>- no item found -</i></div>');
	listArea.append(noItem);
	$('#add_equipment').click(function() {
		addEquipment()
	});
});

function addEquipment() {
	var eq = $('#equipment_type').val();
	var eqText = $('#equipment_type option:selected').text();
	var qt = $('#EquipmentTypeQty').val();
	if (eq == '' || qt == '' || qt < 1) {
		alert("Please select equipment and quantity must be more than 0.");
	} else {
		if (appendItem(eq, qt, eqText)) {
			noItem.hide();
			$('#equipment_type').val('');
			$('#EquipmentTypeQty').val('');
		}
	}
}

function appendItem(eq, qt, eqText) {
	var exItem = $('#eq-' + eq);
	if (typeof (exItem.val()) === "undefined") {
		var newItem = $('<div class="eq-detail" id="item-'
				+ eq
				+ '"><div class="item-detail-left">'
				+ eqText
				+ '</div><div id="qt-'
				+ eq
				+ '" class="item-detail-right">'
				+ qt
				+ '</div><div class="item-detail-manage"><a href="javascript:deleteEq(\''
				+ eq + '\')"> </a></div><input type="hidden" name="eqs[' + eq
				+ ']" id="eq-' + eq + '" value="' + qt
				+ '"><div class="clear"></div></div>');
		listArea.append(newItem);
		exItem = $('#' + eq)
	} else {
		var newQty = (parseInt(exItem.val()) + parseInt(qt));
		var exQty = $('#qt-' + eq);
		exItem.val(newQty);
		exQty.html(newQty);
	}
	return true;
}

function deleteEq(eq) {
	if (confirm('Confirm remove?')) {
		var item = $('#item-' + eq);
		if (typeof (item.val()) !== "undefined") {
			item.remove();
		}
	}
}
