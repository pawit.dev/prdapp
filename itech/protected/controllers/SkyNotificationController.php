<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class SkyNotificationController extends CController
{
	public $layout='management';
	private $_model;

	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex()
	{
		// Permission
		if(!UserLoginUtil::hasPermission(array("FULL_ADMIN"))){
			throw new CHttpException(404,Yii::t('yii','The system is unable to find the requested',
					array('{action}'=>$actionID==''?$this->defaultAction:$actionID)));
		}

		$model = new SkyNotification();

		// Set Search Text
		if(isset($_GET['search_text'])){
			$model->search_text = addslashes($_GET['search_text']);
		}

		$this->render('main', array(
				'data' => $model,
		));
	}


	public function actionLoadData()
	{

		$id = addslashes($_GET['id']);

		$str = file_get_contents('https://sky.muic.mahidol.ac.th/api/externals/reservations?auth_token=437d2b611411d9942123a848d1ee11ecb6a70e&data%5bstart_datetime%5d=1404708905&data%5bend_datetime%5d=1406696105');
		$json = json_decode($str, true);

		mysql_connect(ConfigUtil::getHostName(), ConfigUtil::getUsername(), ConfigUtil::getPassword());
		mysql_select_db(ConfigUtil::getDbName());

		//Delete old data
		$sql = "delete from ".ConfigUtil::getDbName().".request_booking where request_sky_noti_id=".$id;
		if ($result = mysql_query($sql)) {
				
		}else {
			print mysql_error();
		}

		foreach($json['data'] as $item) {

			/* 			echo 'user_loin_id: ' . $item['sender']['username'] . '<br />';
			 echo 'request_booking_type_id: ' . '1' . '<br />';
			echo 'room_id: ' . $item['room']['room_number'] . '<br />';
			echo 'semester_id: ' . 'NULL' . '<br />';
			echo 'request_date: ' . $item['start_datetime'] . '<br />';
			echo 'request_day_in_week: ' . 'NULL' . '<br />';
			echo 'period_start: ' . $item['start_datetime'] . '<br />';//เน�เธ� mapping เธ�เธฑเธ� table
			echo 'period_end: ' . $item['end_datetime'] . '<br />';
			echo 'description: ' . $item['remark'] . '<br />';
			echo 'status_code: ' . 'REQUEST_APPROVE' . '<br />';
			echo 'create_date: ' . $item['created_at'] . '<br />';
			echo 'course_name: ' . '' . '<br />';
			echo ' --------------------------------------------------  '. '<br />'; */

			$epoch_start_datetime =  $item['start_datetime'];
			$dt_start_date = new DateTime("@$epoch_start_datetime");  // convert UNIX timestamp to PHP DateTime
			$epoch_end_datetime =  $item['end_datetime'];
			$dt_end_date = new DateTime("@$epoch_end_datetime");  // convert UNIX timestamp to PHP DateTime

			$period_start = CommonUtil::getPeriodId($dt_start_date->format('H'),$dt_start_date->format('i'));
			$period_end = CommonUtil::getPeriodId($dt_end_date->format('H'),$dt_end_date->format('i'));
			$request_date = $dt_start_date->format('Y-m-d');
			// 			echo '>>>'.$request_date.'<br>';

			$sql = "
					INSERT INTO ".ConfigUtil::getDbName().".request_booking
							(
							`request_sky_id`,
							`request_sky_noti_id`,
							`user_login_id`,
							`request_booking_type_id`,
							`room_id`,
							`request_date`,
							`period_start`,
							`period_end`,
							`description`,
							`status_code`,
							`create_date`,
							`course_name`)
							VALUES
							(". $item['id'].",
									".$id.",
											".$period_start.",
													".$period_end.",
															". $item['room']['room_number']. ",
																	'".$request_date."',
																			1,
																			1,
																			'".$item['remark']."',
																					'REQUEST_SKYDATA_WAIT_APPROVE',
																					NOW(),
																					'')";
			if ($result = mysql_query($sql)) {
					
			}else {
				//print mysql_error();
			}

		}
		$model = new RequestBooking();
		$model->status_filter ='REQUEST_SKYDATA_WAIT_APPROVE';
		$this->render('listReservation', array(
				'data' => $model,
		));

	}
	public function actionApproved()
	{

		$id = addslashes($_GET['id']);

		mysql_connect(ConfigUtil::getHostName(), ConfigUtil::getUsername(), ConfigUtil::getPassword());
		mysql_select_db(ConfigUtil::getDbName());

		//Delete old data
		$sql = "update ".ConfigUtil::getDbName().".request_booking set status_code='REQUEST_APPROVE' where request_sky_noti_id=".$id;
// 		echo $sql.'<br>';
		$result = mysql_query($sql);
		if($result)
		{
			$sql = "update ".ConfigUtil::getDbName().".tb_sky_notification set status=1 where id=".$id;
// 			echo $sql.'<br>';
			$result = mysql_query($sql);
		}

		$model = new SkyNotification();
		$this->render('main', array(
				'data' => $model,
		));

	}
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id'])) {
				$id = addslashes($_GET['id']);
				$this->_model=SkyNotification::model()->findbyPk($id);
			}
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}


}
