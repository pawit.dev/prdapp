<?php
class ConfigUtil {
	private static $siteName = 'http://ed.muic.mahidol.ac.th/itech';
	private static $PushSiteName = 'http://www.prdapp.net/itechservice/';
	// private static $siteName = 'http://localhost:88/itech';
	private static $defaultPageSize = 15;
	private static $returnLatePricePerDay = 500;
	public static function getDbName() {
		$str = Yii::app ()->db->connectionString;
		list ( $host, $db ) = explode ( ';', $str );
		list ( $xx, $dbName ) = explode ( '=', $db );
		return $dbName;
	}
	public static function getHostName() {
		$str = Yii::app ()->db->connectionString;
		list ( $host, $db ) = explode ( ';', $str );
		list ( $xx, $hostName ) = explode ( '=', $host );
		return $hostName;
	}
	public static function getUsername() {
		return Yii::app ()->db->username;
	}
	public static function getReturnLatePricePerDay() {
		return ConfigUtil::getConfig("BORROW_RETURN_LATE_PRICE_PER_DAY");
	}
	public static function getPassword() {
		return Yii::app ()->db->password;
	}
	public static function getSiteName() {
		return self::$siteName;
	}
	public static function getPushSiteName() {
		return self::$PushSiteName;
	}
	public static function getDefaultPageSize() {
		return self::$defaultPageSize;
	}
	public static function getConfig($configName) {
		$myfile = fopen ( "config.property", "r" ) or die ( "Unable to open file!" );
		// Output one character until end-of-file
		while ( ! feof ( $myfile ) ) {
			$line = fgets ( $myfile );
			if (strpos ( $line, $configName ) !== false) {
				list ( $name, $val ) = explode ( '=', $line );
				$val = trim ( $val );
				echo $val;
				break;
			}
		}
		fclose ( $myfile );
	}
	public static function saveConfig($configName, $value) {
		$myfile = fopen ( "config.property", "r" ) or die ( "Unable to open file!" );
		// Output one character until end-of-file
		$str = "";
		$hasConfig = false;
		while ( ! feof ( $myfile ) ) {
			$line = fgets ( $myfile );
			if (strpos ( $line, $configName ) !== false) {
				list ( $name, $val ) = explode ( '=', $line );
				$val = trim ( $val );
				$name = trim ( $name );
				$line = $name . ' = ' . $value;
				$hasConfig = true;
			}
			$str = $str . $line;
		}
		if (! $hasConfig) {
			$str = $str . $configName . ' = ' . $value . PHP_EOL;
		}
		fclose ( $myfile );
		
		$wFile = fopen ( "config.property", "w" ) or die ( "Unable to open file!" );
		fwrite ( $wFile, $str );
		fclose ( $wFile );
	}
}
?>