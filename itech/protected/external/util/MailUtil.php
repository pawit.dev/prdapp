<?php
class MailUtil {
	public static function sendMail($emailAddress, $subject, $content) {
		$mail = Yii::app ()->Smtpmail;
		$mail->IsSMTP ();
		$mail->SetFrom ( 'chayanon.poo@mahidol.ac.th', 'Technology Education Section' );
		$mail->Subject = $subject;
		$mail->MsgHTML ( $content );
		$mail->ClearAddresses ();
		$mail->AddAddress ( $emailAddress, "" );
		if ($mail->Send ()) {
			return true;
		} else {
			return false;
		}
		// $subject ="test";
		// $content = "test";
		// $key = "7Frc6kSp788qPNz2jKlq8";
		// $summary = md5($emailAddress.$key);
		// $url = 'http://www.prdapp.net/MUICService/mail.php?to='.urlencode($emailAddress).'&subject='.urlencode($subject).'&content='.urlencode($content).'&check_sum='.urlencode($summary);
		// echo $url.'<br>';
		// $curl = curl_init();
		// // Set some options - we are passing in a useragent too here
		// curl_setopt_array($curl, array(
		// CURLOPT_RETURNTRANSFER => 1,
		// CURLOPT_URL => $url,
		// CURLOPT_USERAGENT => 'Codular Sample cURL Request',
		// ));
		// $result = curl_exec($curl);
		// curl_close($curl);
		return true;
	}
	public static function getApproveMailContent($key, $requestBorrow) {
		$link = '<a href="' . ConfigUtil::getSiteName () . '/index.php/RequestBorrowNew/ApproveExternal/key/' . $key . '">>>Click Here to Approve this request.<<</a>';
		$dlink = '<a href="' . ConfigUtil::getSiteName () . '/index.php/RequestBorrowNew/DisapproveExternal/key/' . $key . '">>>Click Here to Disapprove this request.<<</a>';
		
		$content = '<table>' . '<tr><td><img alt=""	src="https://ed.muic.mahidol.ac.th/itech/images/logo.png"></td></tr><tr>' . '<td>Hi approver.<br> "' . $requestBorrow->user_login->user_information->first_name . '" has request to borrow equipment.<br>' . 'Here is the request detail<br> <br>' . '<table class="simple-form"><tr>' . '<td class="column-left" width="150">Request Date</td>' . '<td class="column-right">' . DateTimeUtil::getDateFormat ( $requestBorrow->from_date, "dd MM yyyy" ) . ' - ' . DateTimeUtil::getDateFormat ( $requestBorrow->thru_date, "dd MM yyyy" ) . '</td></tr><tr><td class="column-left" valign="top">Location Type</td><td class="column-right">' . ($requestBorrow->location == 'WHITHIN_MUIC' ? 'Within MUIC' : 'Without MUIC') . '</td></tr><tr><td class="column-left">Type of event</td><td class="column-right">' . $requestBorrow->event_type->name . '</td></tr><tr><td class="column-left">Important Notes</td>' . '<td class="column-right">' . $requestBorrow->description . '</td></tr><tr>' . '<td class="column-left">Status</td><td class="column-right">' . $requestBorrow->status->name . '</td>	</tr><tr><td colspan="2"><fieldset><legend>Equipment List</legend><table>';
		$returnPriceList = array ();
		$brokenList = array ();
		$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model ()->findAll ( array (
				'condition' => "request_borrow_id = '" . $requestBorrow->id . "'" 
		) );
		if (count ( $requestBorrowEquipmentTypes ) > 0) {
			foreach ( $requestBorrowEquipmentTypes as $requestBorrowEquipmentType ) {
				
				$content = $content . '<tr><th width="200" align="left">' . $requestBorrowEquipmentType->equipment_type->name . '</th><th width="100">' . $requestBorrowEquipmentType->quantity . '</th></tr>';
				$criteria = new CDbCriteria ();
				$criteria->condition = "request_borrow_equipment_type_id = '" . $requestBorrowEquipmentType->id . "'";
				$requestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model ()->findAll ( $criteria );
				if (isset ( $requestBorrowEquipmentTypeItems ) && count ( $requestBorrowEquipmentTypeItems ) > 0) {
					foreach ( $requestBorrowEquipmentTypeItems as $requestBorrowEquipmentTypeItem ) {
						if ($requestBorrowEquipmentTypeItem->return_price > 0) {
							$returnPriceList [count ( $returnPriceList )] = $requestBorrowEquipmentTypeItem;
						}
						if ($requestBorrowEquipmentTypeItem->broken_price > 0) {
							$brokenList [count ( $brokenList )] = $requestBorrowEquipmentTypeItem;
						}
						$content = $content . '<tr><td>' . $requestBorrowEquipmentTypeItem->equipment->barcode . '</td><td>' . ($requestBorrowEquipmentTypeItem->return_date != '' ? ' ( Returned ) ' : '') . '</td></tr>';
					}
				}
			}
		} else {
			$content = $content . '<tr><td colspan="2"><i>- no item found -</i></td></tr>';
		}
		$content = $content . '</table></fieldset></td></tr>';
		if (count ( $returnPriceList ) > 0) {
			$content = $content . '<tr><td colspan="2"><fieldset><legend>Return Late List</legend>' . '<div class="simple-grid"><table class="items"><tr>' . '<th width="200">Equipment</th>	<th width="100">Price</th></tr>';
			$counter = 1;
			$sum = 0;
			foreach ( $returnPriceList as $returnPrice ) {
				$sum = $sum + $returnPrice->return_price;
				$content = $content . '<tr><td align="left">' . $returnPrice->equipment->barcode . '</td><td align="right">' . number_format ( $returnPrice->return_price ) . '</td></tr>';
			}
			$content = $content . '<tr><td align="right">Total</td><td align="right">' . number_format ( $sum ) . '</td></tr></table></div></fieldset></td></tr>';
		}
		if (count ( $brokenList ) > 0) {
			$content = $content . '<tr><td colspan="2"><fieldset><legend>Broken List</legend><div class="simple-grid">' . '<table class="items"><tr><th width="200">Equipment</th><th width="100">Price</th></tr>';
			
			$counter = 1;
			$sum = 0;
			foreach ( $brokenList as $broken ) {
				$sum = $sum + $broken->broken_price;
				$content = $content . '<tr><td align="left">' . $broken->equipment->barcode . '</td><td align="right">' . number_format ( $broken->broken_price ) . '</td></tr>	';
			}
			$content = $content . '<tr><td align="right">Total</td><td align="right">' . number_format ( $sum ) . '</td></tr></table></div></fieldset></td></tr>';
		}
		$content = $content . '</table></td></tr><tr>	<td>';
		$content = $content . $link;
		$content = $content . '<br><br>Or<br><br></td></tr><tr><td>';
		$content = $content . $dlink;
		$content = $content . '</td></tr></table><br><br>Best Regards.';
		return $content;
	}
	public static function getBorrowDetailMailContent($requestBorrow) {
		$content = '<table>' . '<tr><td><img alt=""	src="https://ed.muic.mahidol.ac.th/itech/images/logo.png"></td></tr><tr>' . '<td>Hi "' . $requestBorrow->user_login->user_information->first_name . '" <br> You are request to borrow equipment.<br>' . 'Here is the request detail<br> <br>' . '<table class="simple-form"><tr>' . '<td class="column-left" width="150">Request Date</td>' . '<td class="column-right">' . DateTimeUtil::getDateFormat ( $requestBorrow->from_date, "dd MM yyyy" ) . ' - ' . DateTimeUtil::getDateFormat ( $requestBorrow->thru_date, "dd MM yyyy" ) . '</td></tr><tr><td class="column-left" valign="top">Location Type</td><td class="column-right">' . ($requestBorrow->location == 'WHITHIN_MUIC' ? 'Within MUIC' : 'Without MUIC') . '</td></tr><tr><td class="column-left">Type of event</td><td class="column-right">' . $requestBorrow->event_type->name . '</td></tr><tr><td class="column-left">Important Notes</td>' . '<td class="column-right">' . $requestBorrow->description . '</td></tr><tr>' . '<td class="column-left">Status</td><td class="column-right">' . $requestBorrow->status->name . '</td>	</tr><tr><td colspan="2"><fieldset><legend>Equipment List</legend><table>';
		$returnPriceList = array ();
		$brokenList = array ();
		$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model ()->findAll ( array (
				'condition' => "request_borrow_id = '" . $requestBorrow->id . "'" 
		) );
		if (count ( $requestBorrowEquipmentTypes ) > 0) {
			foreach ( $requestBorrowEquipmentTypes as $requestBorrowEquipmentType ) {
				
				$content = $content . '<tr><th width="200" align="left">' . $requestBorrowEquipmentType->equipment_type->name . '</th><th width="100">' . $requestBorrowEquipmentType->quantity . '</th></tr>';
				$criteria = new CDbCriteria ();
				$criteria->condition = "request_borrow_equipment_type_id = '" . $requestBorrowEquipmentType->id . "'";
				$requestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model ()->findAll ( $criteria );
				if (isset ( $requestBorrowEquipmentTypeItems ) && count ( $requestBorrowEquipmentTypeItems ) > 0) {
					foreach ( $requestBorrowEquipmentTypeItems as $requestBorrowEquipmentTypeItem ) {
						if ($requestBorrowEquipmentTypeItem->return_price > 0) {
							$returnPriceList [count ( $returnPriceList )] = $requestBorrowEquipmentTypeItem;
						}
						if ($requestBorrowEquipmentTypeItem->broken_price > 0) {
							$brokenList [count ( $brokenList )] = $requestBorrowEquipmentTypeItem;
						}
						$content = $content . '<tr><td>' . $requestBorrowEquipmentTypeItem->equipment->barcode . '</td><td>' . ($requestBorrowEquipmentTypeItem->return_date != '' ? ' ( Returned ) ' : '') . '</td></tr>';
					}
				}
			}
		} else {
			$content = $content . '<tr><td colspan="2"><i>- no item found -</i></td></tr>';
		}
		$content = $content . '</table></fieldset></td></tr>';
		if (count ( $returnPriceList ) > 0) {
			$content = $content . '<tr><td colspan="2"><fieldset><legend>Return Late List</legend>' . '<div class="simple-grid"><table class="items"><tr>' . '<th width="200">Equipment</th>	<th width="100">Price</th></tr>';
			$counter = 1;
			$sum = 0;
			foreach ( $returnPriceList as $returnPrice ) {
				$sum = $sum + $returnPrice->return_price;
				$content = $content . '<tr><td align="left">' . $returnPrice->equipment->barcode . '</td><td align="right">' . number_format ( $returnPrice->return_price ) . '</td></tr>';
			}
			$content = $content . '<tr><td align="right">Total</td><td align="right">' . number_format ( $sum ) . '</td></tr></table></div></fieldset></td></tr>';
		}
		if (count ( $brokenList ) > 0) {
			$content = $content . '<tr><td colspan="2"><fieldset><legend>Broken List</legend><div class="simple-grid">' . '<table class="items"><tr><th width="200">Equipment</th><th width="100">Price</th></tr>';
			
			$counter = 1;
			$sum = 0;
			foreach ( $brokenList as $broken ) {
				$sum = $sum + $broken->broken_price;
				$content = $content . '<tr><td align="left">' . $broken->equipment->barcode . '</td><td align="right">' . number_format ( $broken->broken_price ) . '</td></tr>	';
			}
			$content = $content . '<tr><td align="right">Total</td><td align="right">' . number_format ( $sum ) . '</td></tr></table></div></fieldset></td></tr>';
		}
		$content = $content . '</table></td></tr></table><br>Best Regards.';
		return $content;
	}
	
	public static function getBorrowStatusChangeMailContent($requestBorrow) {
		$content = '<table>' . '<tr><td><img alt=""	src="https://ed.muic.mahidol.ac.th/itech/images/logo.png"></td></tr><tr>' . '<td>Hi "' . $requestBorrow->user_login->user_information->first_name . '" <br> You are request to borrow equipment.<br> And now your request status has been changed.<br>' . 'Here is the request detail<br> <br>' . '<table class="simple-form"><tr>' . '<td class="column-left" width="150">Request Date</td>' . '<td class="column-right">' . DateTimeUtil::getDateFormat ( $requestBorrow->from_date, "dd MM yyyy" ) . ' - ' . DateTimeUtil::getDateFormat ( $requestBorrow->thru_date, "dd MM yyyy" ) . '</td></tr><tr><td class="column-left" valign="top">Location Type</td><td class="column-right">' . ($requestBorrow->location == 'WHITHIN_MUIC' ? 'Within MUIC' : 'Without MUIC') . '</td></tr><tr><td class="column-left">Type of event</td><td class="column-right">' . $requestBorrow->event_type->name . '</td></tr><tr><td class="column-left">Important Notes</td>' . '<td class="column-right">' . $requestBorrow->description . '</td></tr><tr>' . '<td class="column-left">Status</td><td class="column-right">' . $requestBorrow->status->name . '</td>	</tr><tr><td colspan="2"><fieldset><legend>Equipment List</legend><table>';
		$returnPriceList = array ();
		$brokenList = array ();
		$requestBorrowEquipmentTypes = RequestBorrowEquipmentType::model ()->findAll ( array (
				'condition' => "request_borrow_id = '" . $requestBorrow->id . "'"
		) );
		if (count ( $requestBorrowEquipmentTypes ) > 0) {
			foreach ( $requestBorrowEquipmentTypes as $requestBorrowEquipmentType ) {
	
				$content = $content . '<tr><th width="200" align="left">' . $requestBorrowEquipmentType->equipment_type->name . '</th><th width="100">' . $requestBorrowEquipmentType->quantity . '</th></tr>';
				$criteria = new CDbCriteria ();
				$criteria->condition = "request_borrow_equipment_type_id = '" . $requestBorrowEquipmentType->id . "'";
				$requestBorrowEquipmentTypeItems = RequestBorrowEquipmentTypeItem::model ()->findAll ( $criteria );
				if (isset ( $requestBorrowEquipmentTypeItems ) && count ( $requestBorrowEquipmentTypeItems ) > 0) {
					foreach ( $requestBorrowEquipmentTypeItems as $requestBorrowEquipmentTypeItem ) {
						if ($requestBorrowEquipmentTypeItem->return_price > 0) {
							$returnPriceList [count ( $returnPriceList )] = $requestBorrowEquipmentTypeItem;
						}
						if ($requestBorrowEquipmentTypeItem->broken_price > 0) {
							$brokenList [count ( $brokenList )] = $requestBorrowEquipmentTypeItem;
						}
						$content = $content . '<tr><td>' . $requestBorrowEquipmentTypeItem->equipment->barcode . '</td><td>' . ($requestBorrowEquipmentTypeItem->return_date != '' ? ' ( Returned ) ' : '') . '</td></tr>';
					}
				}
			}
		} else {
			$content = $content . '<tr><td colspan="2"><i>- no item found -</i></td></tr>';
		}
		$content = $content . '</table></fieldset></td></tr>';
		if (count ( $returnPriceList ) > 0) {
			$content = $content . '<tr><td colspan="2"><fieldset><legend>Return Late List</legend>' . '<div class="simple-grid"><table class="items"><tr>' . '<th width="200">Equipment</th>	<th width="100">Price</th></tr>';
			$counter = 1;
			$sum = 0;
			foreach ( $returnPriceList as $returnPrice ) {
				$sum = $sum + $returnPrice->return_price;
				$content = $content . '<tr><td align="left">' . $returnPrice->equipment->barcode . '</td><td align="right">' . number_format ( $returnPrice->return_price ) . '</td></tr>';
			}
			$content = $content . '<tr><td align="right">Total</td><td align="right">' . number_format ( $sum ) . '</td></tr></table></div></fieldset></td></tr>';
		}
		if (count ( $brokenList ) > 0) {
			$content = $content . '<tr><td colspan="2"><fieldset><legend>Broken List</legend><div class="simple-grid">' . '<table class="items"><tr><th width="200">Equipment</th><th width="100">Price</th></tr>';
				
			$counter = 1;
			$sum = 0;
			foreach ( $brokenList as $broken ) {
				$sum = $sum + $broken->broken_price;
				$content = $content . '<tr><td align="left">' . $broken->equipment->barcode . '</td><td align="right">' . number_format ( $broken->broken_price ) . '</td></tr>	';
			}
			$content = $content . '<tr><td align="right">Total</td><td align="right">' . number_format ( $sum ) . '</td></tr></table></div></fieldset></td></tr>';
		}
		$content = $content . '</table></td></tr></table><br>Best Regards.';
		return $content;
	}
}
?>
