<span class="module-head">Approve</span>


<div class="simple-grid">
	<table class="items">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="15%">Service Type</th>
				<th width="20%">Requested Date</th>
				<th width="13%">Due Date</th>
				<th width="10%">Status</th>
				<th width="10%">Action</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			$counter = 1;
			$dataProvider = $data->search();


			foreach ($dataProvider->data as $request) {
		?>
			<tr class="line-<?php echo $counter%2 == 0 ? '1' : '2'?>">
			<td class="center"><?php echo GridUtil::getDataIndex($dataProvider, $counter++)?></td>
			
				<td class="center"><?php echo RequestUtil::getAllRequestServiceTypeName($request->id); ?>
				<td class="center"><?php echo DateTimeUtil::getDateFormat($request->create_date, "dd MM yyyy"); ?>
				<td class="center"><?php echo DateTimeUtil::getDateFormat($request->due_date, "dd MM yyyy"); ?>
				<td class="center"><?php echo $request->status->name; ?></td>

				<td class="center">
				<?php if( UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_SERVICE",)) ){?>
				
					<a title="Approve" href="<?php echo Yii::app()->CreateUrl('RequestService/ApproveRequest/id/'.$request->id); ?>">Approve</a>|
					<a title="DisApprove" href="<?php echo Yii::app()->CreateUrl('RequestService/DisapproveRequest/id/'.$request->id); ?>">Approve</a>
					
					<?php }?>
					

				</td>
				
			</tr>
				
		<?php }?>
		</tbody>
	</table>
				<div class="paging">
				<?php GridUtil::RenderPageButton($this, $dataProvider); ?>
			</div>
</div>



<?php
/*
$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'my-model-grid',
		'dataProvider' => $data->search(),
		'ajaxUpdate'=>true,
		'columns' => array(
				array(
						'header'=>'#',
						'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
						'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
				),
				array(
						'header'=>'Service Type',
						'value'=>'RequestUtil::getAllRequestServiceTypeName($data->id)',
				),
				array(
						'header'=>'Requested Date',
						'value'=>'DateTimeUtil::getDateFormat($data->create_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'15%', 'align'=>'center'),
				),
				array(
						'header'=>'Due Date',
						'value'=>'DateTimeUtil::getDateFormat($data->due_date, "dd MM yyyy");',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(
						'header'=>'Status',
						'value'=>'$data->status->name;',
						'htmlOptions'=>array('width'=>'25%', 'align'=>'center'),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'header'=>'Approve',
						'template'=>'{Approve}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'Approve' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_SERVICE"))',
										'url' => 'Yii::app()->createUrl("/RequestService/ApproveRequest/")."/id/".$data->id',
								),
						),
				),
				array(            // display a column with "view", "update" and "delete" buttons
						'class'=>'CButtonColumn',
						'header'=>'Disapprove',
						'template'=>'{Disapprove}',
						'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
						'buttons'=>array
						(
								'Disapprove' => array
								(
										'visible'=>'UserLoginUtil::hasPermission(array("FULL_ADMIN", "UPDATE_REQUEST_SERVICE"))',
										'url' => 'Yii::app()->createUrl("/RequestService/DisapproveRequest/")."/id/".$data->id',
								),
						),
				),
		),
));
*/
?> <br>