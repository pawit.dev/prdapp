<span class="module-head">Approve</span>
<div class="simple-grid">
	<table class="items">
		<thead>
			<tr>
				<th width="5%">#</th>
				<th width="15%">User</th>
				<th width="14%">Booking Type</th>
				<th width="13%">Request Date</th>
				<th width="10%">Use Date</th>
				<th width="10%">Use Time</th>
				<th width="10%">Room</th>
				<th width="10%">Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			$counter = 1;
			foreach ($data->searchAllBooking()->data as $request) {
		?>
			<tr class="line-<?php echo $counter%2 == 0 ? '1' : '2'?>">
				<td class="center"><?php echo $counter++?></td>
				<td class="center"><?php echo $request->user_login->user_information->first_name?>
				<td class="center"><?php echo $request->request_booking_type->name?>
				<td class="center"><?php echo DateTimeUtil::getDateFormat($data->create_date, "dd MM yyyy");?>
				<td class="center"><?php echo $request->request_date == null ? $request->day_in_week->name : DateTimeUtil::getDateFormat($request->request_date, "dd MM yyyy");?>
				<td class="center"><?php echo DateTimeUtil::getTimeFormat($request->period_s->start_hour, $request->period_s->start_min)." - ".DateTimeUtil::getTimeFormat($request->period_e->end_hour, $request->period_e->end_min); ?>
				<td class="center"><?php echo $request->room->room_code?></td>
				<td class="center"><?php echo $request->status->name?></td>
				<td>
				
					<a title="Approve" href="<?php echo Yii::app()->CreateUrl('RequestBooking/ApproveRequest/id/'.$request->id)?>">Approve</a> | 
					<a title="DisApprove" href="<?php echo Yii::app()->CreateUrl('RequestBooking/DisapproveRequest/id/'.$request->id)?>">DisApprove</a>
				</td>
				
			</tr>
				
		<?php }?>
		</tbody>
	</table>
</div>

<br>

