<?php

class RequestBorrow extends CActiveRecord
{
	public $view_all_request = false;
	public $year_filter;
	public $month_filter;
	public $day_filter;
	public $status_filter;
	public $show_all_active;
	public $completed_request;
	public $approve_filter;
	public $status_for_approve;
	public $status_not_finish;
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public function __construct($scenario='insert') {
		parent::__construct($scenario);
		$this->year_filter = date("Y") * 1;
		$this->month_filter = date("m") * 1;
		$this->day_filter = date("d") * 1;
	}
	public static function model($className=__CLASS__)
	{

		return parent::model($className);
	}

	public function clearDateFilter()
	{
		$this->year_filter = '';
		$this->month_filter = '';
		$this->day_filter = '';
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'request_borrow';
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'approver' => array(self::BELONGS_TO, 'UserLogin', 'approve_by'),
				'user_login' => array(self::BELONGS_TO, 'UserLogin', 'user_login_id'),
				'event_type' => array(self::BELONGS_TO, 'EventType', 'event_type_id'),
				'status' => array(self::BELONGS_TO, 'Status', 'status_code'),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('event_type_id, location, approve_by, chef_email, description, from_date, thru_date', 'safe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
				'description' => 'Description',
		);
	}

	/**
	 * @param Post the post that this comment belongs to. If null, the method
	 * will query for the post.
	 * @return string the permalink URL for this comment
	 */
	public function getUrl($post=null)
	{
		if($post===null)
			$post=$this->post;
		return $post->url.'#c'.$this->id;
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		/*
		 if(parent::beforeSave())
		 {
		if($this->isNewRecord)
			$this->create_time=time();
		return true;
		}
		else
			return false;
		*/
		return true;
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		if(!$this->view_all_request) {
			if(isset($this->approve_filter) && $this->approve_filter) {
				
				$criteria->addCondition("t.approve_by = '".UserLoginUtil::getUserLoginId()."'");
				/*
				 * old coding by X
				$criteria->addCondition("(t.user_login_id in (select id from user_login where parent = '".UserLoginUtil::getUserLoginId()."') and t.approve_by = '')".
						" or (t.approve_by in (select id from user_login where parent = '".UserLoginUtil::getUserLoginId()."') and t.approve_by != '')");
				*/
			} else {
				if(!UserLoginUtil::areUserRole (array (UserRoles::ADMIN,UserRoles::STAFF_AV))){
					//echo "XXXXXX:".UserLoginUtil::getUserLoginId();
					$criteria->addCondition("t.user_login_id = '".UserLoginUtil::getUserLoginId()."'");
				}
			}
		}
		
		if(isset($this->status_for_approve) && $this->status_for_approve) {
			$criteria->addCondition("status_code in ('R_B_NEW_WAIT_APPROVE_1','R_B_NEW_WAIT_APPROVE_2','R_B_NEW_WAIT_APPROVE_3') ");
		}
		if(isset($this->status_not_finish) && $this->status_not_finish) {
			$criteria->addCondition("status_code not in ('R_B_NEW_RETURNED','R_B_NEW_CANCELLED') ");
		}
		if(isset($this->status_filter)){
			if($this->status_filter =='WAIT_APPROVE') {
				$criteria->addCondition("status_code='R_B_NEW_WAIT_APPROVE_1' or status_code='R_B_NEW_WAIT_APPROVE_2' or status_code='R_B_NEW_WAIT_APPROVE_3'");
			} else {
				$criteria->addCondition("status_code='".$this->status_filter."'");
			}
		}

		
		
		// 			if($this->completed_request){
		// 				$criteria->addCondition("status_code IN ('REQUEST_BORROW_CANCELLED', 'REQUEST_BORROW_COMPLETED')");
		// 			} else {
		// 				$criteria->addCondition("status_code NOT IN ('REQUEST_BORROW_CANCELLED', 'REQUEST_BORROW_COMPLETED')");
		// 			}
		// 		}


		return new CActiveDataProvider(get_class($this), array(
				'criteria' => $criteria,
				'sort' => array(
						'defaultOrder' => 't.thru_date',
				),
				'pagination' => array(
						'pageSize' => ConfigUtil::getDefaultPageSize()
				),
		));
	}
}