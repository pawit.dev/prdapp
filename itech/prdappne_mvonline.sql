-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jan 04, 2014 at 06:56 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `prdappne_mvonline`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `department`
-- 

CREATE TABLE `department` (
  `id` int(11) NOT NULL auto_increment,
  `department_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `department_code` (`department_code`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `department`
-- 

INSERT INTO `department` VALUES (1, '10', 'การเงิน และ การบัญชี', '');
INSERT INTO `department` VALUES (2, '-', '-', '-');

-- --------------------------------------------------------

-- 
-- Table structure for table `enumeration`
-- 

CREATE TABLE `enumeration` (
  `id` int(11) NOT NULL auto_increment,
  `enumeration_type_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `enumeration_type_id` (`enumeration_type_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `enumeration`
-- 

INSERT INTO `enumeration` VALUES (1, 'DAY_IN_WEEK', 'Sunday', '');
INSERT INTO `enumeration` VALUES (2, 'DAY_IN_WEEK', 'Monday', '');
INSERT INTO `enumeration` VALUES (3, 'DAY_IN_WEEK', 'Tuesday', '');
INSERT INTO `enumeration` VALUES (4, 'DAY_IN_WEEK', 'Wednesday', '');
INSERT INTO `enumeration` VALUES (5, 'DAY_IN_WEEK', 'Thursday', '');
INSERT INTO `enumeration` VALUES (6, 'DAY_IN_WEEK', 'Friday', '');
INSERT INTO `enumeration` VALUES (7, 'DAY_IN_WEEK', 'Saturday', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `enumeration_type`
-- 

CREATE TABLE `enumeration_type` (
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `enumeration_type`
-- 

INSERT INTO `enumeration_type` VALUES ('DAY_IN_WEEK', 'Day in week', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `equipment`
-- 

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL auto_increment,
  `equipment_type_id` int(11) NOT NULL,
  `room_id` int(11) default NULL,
  `equipment_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `mac_address` varchar(255) NOT NULL,
  `client_user` varchar(40) NOT NULL,
  `client_pass` varchar(40) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `equipment_code` (`equipment_code`),
  KEY `equipment_type_id` (`equipment_type_id`),
  KEY `room_id` (`room_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=450 ;

-- 
-- Dumping data for table `equipment`
-- 

INSERT INTO `equipment` VALUES (1, 4, 1302, '1', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (2, 5, 1302, '2', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (3, 6, 1302, '3', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (4, 8, 1302, '4', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (5, 9, 1302, '5', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (6, 4, 1303, '6', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (7, 5, 1303, '7', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (8, 6, 1303, '8', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (9, 8, 1303, '9', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (10, 9, 1303, '10', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (11, 4, 1304, '11', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (12, 5, 1304, '12', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (13, 6, 1304, '13', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (14, 8, 1304, '14', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (15, 9, 1304, '15', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (16, 4, 1305, '16', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (17, 5, 1305, '17', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (18, 6, 1305, '18', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (19, 8, 1305, '19', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (20, 9, 1305, '20', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (21, 4, 1306, '21', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (22, 5, 1306, '22', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (23, 6, 1306, '23', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (24, 8, 1306, '24', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (25, 9, 1306, '25', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (26, 4, 1307, '26', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (27, 5, 1307, '27', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (28, 6, 1307, '28', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (29, 8, 1307, '29', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (30, 9, 1307, '30', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (31, 4, 1308, '31', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (32, 5, 1308, '32', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (33, 6, 1308, '33', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (34, 8, 1308, '34', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (35, 9, 1308, '35', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (36, 4, 1314, '36', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (37, 5, 1314, '37', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (38, 6, 1314, '38', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (39, 8, 1314, '39', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (40, 9, 1314, '40', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (41, 4, 1315, '41', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (42, 5, 1315, '42', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (43, 6, 1315, '43', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (44, 8, 1315, '44', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (45, 9, 1315, '45', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (46, 4, 1402, '46', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (47, 5, 1402, '47', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (48, 6, 1402, '48', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (49, 8, 1402, '49', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (50, 9, 1402, '50', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (51, 4, 1403, '51', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (52, 5, 1403, '52', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (53, 6, 1403, '53', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (54, 8, 1403, '54', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (55, 9, 1403, '55', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (56, 4, 1404, '56', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (57, 5, 1404, '57', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (58, 6, 1404, '58', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (59, 8, 1404, '59', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (60, 9, 1404, '60', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (61, 4, 1405, '61', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (62, 5, 1405, '62', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (63, 6, 1405, '63', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (64, 8, 1405, '64', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (65, 9, 1405, '65', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (66, 4, 1406, '66', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (67, 5, 1406, '67', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (68, 6, 1406, '68', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (69, 8, 1406, '69', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (70, 9, 1406, '70', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (71, 4, 1407, '71', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (72, 5, 1407, '72', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (73, 6, 1407, '73', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (74, 8, 1407, '74', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (75, 9, 1407, '75', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (76, 4, 1408, '76', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (77, 5, 1408, '77', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (78, 6, 1408, '78', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (79, 8, 1408, '79', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (80, 9, 1408, '80', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (81, 4, 1417, '81', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (82, 5, 1417, '82', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (83, 6, 1417, '83', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (84, 8, 1417, '84', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (85, 9, 1417, '85', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (86, 4, 1418, '86', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (87, 5, 1418, '87', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (88, 6, 1418, '88', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (89, 8, 1418, '89', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (90, 9, 1418, '90', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (91, 4, 1419, '91', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (92, 5, 1419, '92', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (93, 6, 1419, '93', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (94, 8, 1419, '94', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (95, 9, 1419, '95', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (96, 4, 1502, '96', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (97, 5, 1502, '97', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (98, 6, 1502, '98', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (99, 8, 1502, '99', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (100, 9, 1502, '100', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (101, 4, 1503, '101', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (102, 5, 1503, '102', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (103, 6, 1503, '103', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (104, 8, 1503, '104', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (105, 9, 1503, '105', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (106, 4, 1504, '106', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (107, 5, 1504, '107', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (108, 6, 1504, '108', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (109, 8, 1504, '109', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (110, 9, 1504, '110', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (111, 4, 2302, '111', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (112, 5, 2302, '112', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (113, 6, 2302, '113', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (114, 8, 2302, '114', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (115, 9, 2302, '115', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (116, 4, 2303, '116', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (117, 5, 2303, '117', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (118, 6, 2303, '118', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (119, 8, 2303, '119', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (120, 9, 2303, '120', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (121, 4, 2306, '121', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (122, 5, 2306, '122', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (123, 6, 2306, '123', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (124, 8, 2306, '124', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (125, 9, 2306, '125', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (126, 4, 2307, '126', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (127, 5, 2307, '127', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (128, 6, 2307, '128', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (129, 8, 2307, '129', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (130, 9, 2307, '130', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (131, 4, 2308, '131', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (132, 5, 2308, '132', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (133, 6, 2308, '133', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (134, 8, 2308, '134', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (135, 9, 2308, '135', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (136, 4, 3302, '136', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (137, 5, 3302, '137', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (138, 6, 3302, '138', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (139, 8, 3302, '139', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (140, 9, 3302, '140', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (141, 4, 3303, '141', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (142, 5, 3303, '142', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (143, 6, 3303, '143', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (144, 8, 3303, '144', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (145, 9, 3303, '145', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (146, 4, 3304, '146', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (147, 5, 3304, '147', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (148, 6, 3304, '148', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (149, 8, 3304, '149', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (150, 9, 3304, '150', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (151, 4, 3305, '151', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (152, 5, 3305, '152', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (153, 6, 3305, '153', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (154, 8, 3305, '154', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (155, 9, 3305, '155', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (156, 4, 3306, '156', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (157, 5, 3306, '157', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (158, 6, 3306, '158', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (159, 8, 3306, '159', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (160, 9, 3306, '160', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (161, 4, 3315, '161', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (162, 5, 3315, '162', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (163, 6, 3315, '163', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (164, 8, 3315, '164', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (165, 9, 3315, '165', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (166, 4, 3316, '166', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (167, 5, 3316, '167', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (168, 6, 3316, '168', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (169, 8, 3316, '169', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (170, 9, 3316, '170', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (171, 4, 3317, '171', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (172, 5, 3317, '172', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (173, 6, 3317, '173', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (174, 8, 3317, '174', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (175, 9, 3317, '175', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (176, 4, 3407, '176', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (177, 5, 3407, '177', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (178, 6, 3407, '178', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (179, 8, 3407, '179', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (180, 9, 3407, '180', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (181, 4, 3408, '181', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (182, 5, 3408, '182', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (183, 6, 3408, '183', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (184, 8, 3408, '184', 'COM', 'COM', '10.27.3.146', '5C:F9:DD:DD:31:25', 'AV_MUIC', '1234', 'A');
INSERT INTO `equipment` VALUES (185, 9, 3408, '185', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (186, 4, 3409, '186', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (187, 5, 3409, '187', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (188, 6, 3409, '188', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (189, 8, 3409, '189', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (190, 9, 3409, '190', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (191, 4, 3410, '191', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (192, 5, 3410, '192', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (193, 6, 3410, '193', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (194, 8, 3410, '194', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (195, 9, 3410, '195', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (196, 4, 3411, '196', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (197, 5, 3411, '197', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (198, 6, 3411, '198', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (199, 8, 3411, '199', 'COM', 'COM', '10.27.3.149', '5C:F9:DD:DC:FF:94', '', '', 'A');
INSERT INTO `equipment` VALUES (200, 9, 3411, '200', 'LCD', 'LCD', '127.0.0.1', '', '', '', 'A');
INSERT INTO `equipment` VALUES (201, 4, 3412, '201', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (202, 5, 3412, '202', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (203, 6, 3412, '203', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (204, 8, 3412, '204', 'COM', 'COM', '192.168.1.42', '5C:F9:DD:DD:2B:6D', 'AV_MUIC', 'av_muic', 'A');
INSERT INTO `equipment` VALUES (205, 9, 3412, '205', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (206, 4, 3420, '206', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (207, 5, 3420, '207', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (208, 6, 3420, '208', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (209, 8, 3420, '209', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (210, 9, 3420, '210', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (211, 4, 3421, '211', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (212, 5, 3421, '212', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (213, 6, 3421, '213', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (214, 8, 3421, '214', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (215, 9, 3421, '215', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (216, 4, 3422, '216', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (217, 5, 3422, '217', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (218, 6, 3422, '218', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (219, 8, 3422, '219', 'COM', 'COM', '10.27.7.90', '5C:F9:DD:DD:32:D4', 'AV_MUIC', '1234', 'A');
INSERT INTO `equipment` VALUES (220, 9, 3422, '220', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (221, 4, 5207, '221', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (222, 5, 5207, '222', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (223, 6, 5207, '223', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (224, 8, 5207, '224', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (225, 9, 5207, '225', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (226, 4, 5208, '226', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (227, 5, 5208, '227', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (228, 6, 5208, '228', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (229, 8, 5208, '229', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (230, 9, 5208, '230', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (231, 4, 5209, '231', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (232, 5, 5209, '232', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (233, 6, 5209, '233', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (234, 8, 5209, '234', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (235, 9, 5209, '235', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (236, 4, 5210, '236', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (237, 5, 5210, '237', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (238, 6, 5210, '238', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (239, 8, 5210, '239', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (240, 9, 5210, '240', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (241, 4, 5211, '241', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (242, 5, 5211, '242', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (243, 6, 5211, '243', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (244, 8, 5211, '244', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (245, 9, 5211, '245', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (246, 4, 5212, '246', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (247, 5, 5212, '247', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (248, 6, 5212, '248', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (249, 8, 5212, '249', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (250, 9, 5212, '250', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (251, 4, 5301, '251', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (252, 5, 5301, '252', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (253, 6, 5301, '253', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (254, 8, 5301, '254', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (255, 9, 5301, '255', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (256, 4, 5307, '256', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (257, 5, 5307, '257', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (258, 6, 5307, '258', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (259, 8, 5307, '259', 'COM', 'COM', '192.168.1.42', '88:53:95:28:B5:6B', 'hui', 'password', 'A');
INSERT INTO `equipment` VALUES (260, 9, 5307, '260', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (261, 4, 5308, '261', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (262, 5, 5308, '262', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (263, 6, 5308, '263', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (264, 8, 5308, '264', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (265, 9, 5308, '265', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (266, 4, 5309, '266', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (267, 5, 5309, '267', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (268, 6, 5309, '268', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (269, 8, 5309, '269', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (270, 9, 5309, '270', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (271, 4, 5310, '271', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (272, 5, 5310, '272', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (273, 6, 5310, '273', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (274, 8, 5310, '274', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (275, 9, 5310, '275', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (276, 4, 5311, '276', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (277, 5, 5311, '277', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (278, 6, 5311, '278', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (279, 8, 5311, '279', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (280, 9, 5311, '280', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (281, 4, 5312, '281', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (282, 5, 5312, '282', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (283, 6, 5312, '283', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (284, 8, 5312, '284', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (285, 9, 5312, '285', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (286, 4, 5313, '286', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (287, 5, 5313, '287', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (288, 6, 5313, '288', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (289, 8, 5313, '289', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (290, 9, 5313, '290', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (291, 8, 1309, '291', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (292, 9, 1309, '292', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (293, 9, 1312, '293', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (294, 9, 1506, '294', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (295, 8, 3307, '295', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (296, 9, 3307, '296', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (297, 6, 3414, '297', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (298, 8, 3414, '298', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (299, 9, 3414, '299', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (300, 4, 3415, '300', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (301, 5, 3415, '301', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (302, 6, 3415, '302', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (303, 8, 3415, '303', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (304, 9, 3415, '304', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (305, 9, 3508, '305', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (306, 8, 1108, '306', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (307, 9, 1108, '307', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (308, 9, 1210, '308', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (309, 5, 1210, '309', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (310, 4, 1210, '310', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (311, 4, 1210, '311', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (312, 4, 1210, '312', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (313, 6, 1214, '313', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (314, 8, 1214, '314', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (315, 9, 1214, '315', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (316, 9, 1318, '316', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (317, 9, 1318, '317', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (318, 9, 1318, '318', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (319, 5, 1318, '319', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (320, 6, 1318, '320', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (321, 4, 1318, '321', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (322, 4, 1318, '322', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (323, 4, 1318, '323', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (324, 4, 1318, '324', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (325, 4, 1318, '325', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (326, 4, 1318, '326', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (327, 3, 1318, '327', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (328, 3, 1318, '328', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (329, 3, 1318, '329', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (330, 8, 2207, '330', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (331, 9, 2207, '331', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (332, 6, 2207, '332', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (333, 9, 7, '333', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (334, 9, 7, '334', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (335, 9, 7, '335', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (336, 9, 7, '336', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (337, 9, 7, '337', 'LCD', 'LCD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (338, 8, 7, '338', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (339, 8, 7, '339', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (340, 8, 7, '340', 'COM', 'COM', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (341, 7, 7, '341', 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (342, 7, 7, '342', 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (343, 7, 7, '343', 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (344, 7, 7, '344', 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (345, 7, 7, '345', 'COM(NOTEBOOK)', 'COM(NOTEBOOK)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (346, 6, 7, '346', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (347, 6, 7, '347', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (348, 6, 7, '348', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (349, 6, 7, '349', 'Visualizer', 'Visualizer', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (350, 5, 7, '350', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (351, 5, 7, '351', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (352, 5, 7, '352', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (353, 5, 7, '353', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (354, 5, 7, '354', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (355, 5, 7, '355', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (356, 5, 7, '356', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (357, 5, 7, '357', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (358, 5, 7, '358', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (359, 5, 7, '359', 'DVD', 'DVD', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (360, 3, 7, '360', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (361, 3, 7, '361', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (362, 3, 7, '362', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (363, 3, 7, '363', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (364, 1, 7, '364', 'Portable Sound System', 'Portable Sound System', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (365, 1, 7, '365', 'Portable Sound System', 'Portable Sound System', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (366, 10, 7, '366', 'LED TV', 'LED TV', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (367, 10, 7, '367', 'LED TV', 'LED TV', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (368, 4, 7, '368', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (369, 4, 7, '369', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (370, 4, 7, '370', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (371, 4, 7, '371', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (372, 4, 7, '372', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (373, 4, 7, '373', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (374, 4, 7, '374', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (375, 4, 7, '375', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (376, 4, 7, '376', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (377, 4, 7, '377', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (378, 4, 7, '378', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (379, 4, 7, '379', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (380, 4, 7, '380', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (381, 4, 7, '381', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (382, 4, 7, '382', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (383, 4, 7, '383', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (384, 4, 7, '384', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (385, 4, 7, '385', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (386, 4, 7, '386', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (387, 4, 7, '387', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (388, 4, 7, '388', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (389, 4, 7, '389', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (390, 4, 7, '390', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (391, 4, 7, '391', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (392, 4, 7, '392', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (393, 4, 7, '393', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (394, 4, 7, '394', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (395, 4, 7, '395', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (396, 4, 7, '396', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (397, 4, 7, '397', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (398, 4, 7, '398', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (399, 4, 7, '399', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (400, 4, 7, '400', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (401, 4, 7, '401', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (402, 4, 7, '402', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (403, 4, 7, '403', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (404, 4, 7, '404', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (405, 4, 7, '405', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (406, 4, 7, '406', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (407, 4, 7, '407', 'Sound Mic (Cable)', 'Sound Mic (Cable)', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (408, 2, 7, '408', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (409, 2, 7, '409', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (410, 2, 7, '410', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (411, 2, 7, '411', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (412, 2, 7, '412', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (413, 2, 7, '413', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (414, 2, 7, '414', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (415, 2, 7, '415', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (416, 2, 7, '416', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (417, 2, 7, '417', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (418, 2, 7, '418', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (419, 2, 7, '419', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (420, 2, 7, '420', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (421, 2, 7, '421', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (422, 2, 7, '422', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (423, 2, 7, '423', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (424, 2, 7, '424', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (425, 2, 7, '425', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (426, 2, 7, '426', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (427, 2, 7, '427', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (428, 2, 7, '428', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (429, 2, 7, '429', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (430, 2, 7, '430', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (431, 2, 7, '431', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (432, 2, 7, '432', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (433, 2, 7, '433', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (434, 2, 7, '434', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (435, 2, 7, '435', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (436, 2, 7, '436', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (437, 2, 7, '437', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (438, 2, 7, '438', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (439, 2, 7, '439', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (440, 2, 7, '440', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (441, 2, 7, '441', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (442, 2, 7, '442', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (443, 2, 7, '443', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (444, 2, 7, '444', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (445, 2, 7, '445', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (446, 2, 7, '446', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (447, 2, 7, '447', 'Remote Presenter', 'Remote Presenter', '', '', '', '', 'A');
INSERT INTO `equipment` VALUES (448, 1, 1, '1357', '1', '1', '10.27.3.155', '3C-D9-2B-62-35-AD', '1', '1', 'A');
INSERT INTO `equipment` VALUES (449, 2, 1308, '1358', '1', '1', '47/585', '3C-D9-2B-62-35-AD', 'mv_online', 'mv_online', 'D');

-- --------------------------------------------------------

-- 
-- Table structure for table `equipment_cracked_log`
-- 

CREATE TABLE `equipment_cracked_log` (
  `id` int(11) NOT NULL auto_increment,
  `equipment_id` int(11) NOT NULL,
  `cracked_date` date NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `equipment_cracked_log`
-- 

INSERT INTO `equipment_cracked_log` VALUES (1, 98, '2013-06-03', 'S');
INSERT INTO `equipment_cracked_log` VALUES (2, 99, '2013-06-03', 'S');
INSERT INTO `equipment_cracked_log` VALUES (3, 99, '2013-06-03', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `equipment_type`
-- 

CREATE TABLE `equipment_type` (
  `id` int(11) NOT NULL auto_increment,
  `equipment_type_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `equipment_type_code` (`equipment_type_code`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `equipment_type`
-- 

INSERT INTO `equipment_type` VALUES (1, '1001', 'Portable Sound System', 'Portable Sound System');
INSERT INTO `equipment_type` VALUES (2, '1002', 'Remote Presenter', 'Remote Presenter');
INSERT INTO `equipment_type` VALUES (3, '1003', 'Sounc Mic(Wireless)', 'Sounc Mic(Wireless)');
INSERT INTO `equipment_type` VALUES (4, '1004', 'Sound Mic (Cable)', 'Sound Mic (Cable)');
INSERT INTO `equipment_type` VALUES (5, '1005', 'DVD', 'DVD');
INSERT INTO `equipment_type` VALUES (6, '1006', 'Visualizer', 'Visualizer');
INSERT INTO `equipment_type` VALUES (7, '1007', 'COM(NOTEBOOK)', 'COM(NOTEBOOK)');
INSERT INTO `equipment_type` VALUES (8, '1008', 'COM', 'COM');
INSERT INTO `equipment_type` VALUES (9, '1009', 'LCD', 'LCD');
INSERT INTO `equipment_type` VALUES (10, '1010', 'LED TV', 'LED TV');

-- --------------------------------------------------------

-- 
-- Table structure for table `event_type`
-- 

CREATE TABLE `event_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `event_type`
-- 

INSERT INTO `event_type` VALUES (1, 'Meeting', '');
INSERT INTO `event_type` VALUES (2, 'Seminar', '');
INSERT INTO `event_type` VALUES (3, 'Teaching', '');
INSERT INTO `event_type` VALUES (4, 'Visitting', '');
INSERT INTO `event_type` VALUES (5, 'Exhibition', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `gallery`
-- 

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `gallery`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `gcm_users`
-- 

CREATE TABLE `gcm_users` (
  `id` int(11) NOT NULL auto_increment,
  `gcm_regid` text,
  `name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `gcm_users`
-- 

INSERT INTO `gcm_users` VALUES (1, 'APA91bHdvYqoXx-Nh3Rhy2h6xg4OJNLheJgTBU1-C7mem0uB3vbZj3ofMKssqliFczHpeU4kFBT2m5U70ZtcMFe9KESqDERifvo3', 'pawit', 'pawitvaap@gmail.com', '2013-07-19 15:27:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `link`
-- 

CREATE TABLE `link` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `link`
-- 

INSERT INTO `link` VALUES (1, 'Google', 'Searching web.', 'http://www.google.com');

-- --------------------------------------------------------

-- 
-- Table structure for table `news`
-- 

CREATE TABLE `news` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `pic` varchar(255) default NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `news`
-- 

INSERT INTO `news` VALUES (7, '2nd inter cup', 'MUIC played host to the 2nd International Universities Cup, also known as the Inter Cup, from January 28 to February 4, 2013. The College’s football team placed third, after competing with teams from Thamassat University', 'MUIC played host to the 2nd International Universities Cup, also known as the Inter Cup, from January 28 to February 4, 2013. The College’s football team placed third, after competing with teams from Thamassat University ', 'upload/news/7/news2.gif', '1', '2013-04-28 20:08:01');
INSERT INTO `news` VALUES (9, 'Art Exhibition', 'The MUIC Art Club is currently sponsoring an exhibition on the ground floor of Building 1. The exhibition, which runs until February 13, consists of two features: artwork by members of the club and paintings from the general student  test. ', 'The MUIC Art Club is currently sponsoring an exhibition on the ground floor of Building 1. The exhibition, which runs until February 13, consists of two features: artwork by members of the club and paintings from the general student  test. ', 'upload/news/9/news1.gif', '1', '2013-04-28 20:09:41');

-- --------------------------------------------------------

-- 
-- Table structure for table `period`
-- 

CREATE TABLE `period` (
  `id` int(11) NOT NULL auto_increment,
  `period_group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start_hour` int(11) NOT NULL,
  `start_min` int(11) NOT NULL,
  `end_hour` int(11) NOT NULL,
  `end_min` int(11) NOT NULL,
  `status_code` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status_code` (`status_code`),
  KEY `preriod_group_id` (`period_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=129 ;

-- 
-- Dumping data for table `period`
-- 

INSERT INTO `period` VALUES (1, 1, '08.00 - 09.50', '', 8, 0, 9, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (2, 1, '10.00 - 11.50', '', 10, 0, 11, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (3, 1, '12.00 - 13.50', '', 12, 0, 13, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (4, 1, '14.00 - 15.50', '', 14, 0, 15, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (5, 1, '16.00 - 17.50', '', 16, 0, 17, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (6, 1, '18.00 - 19.50', '', 18, 0, 19, 50, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (101, 2, '08.00 - 08.30', '', 8, 0, 8, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (102, 2, '08.30 - 09.00', '', 8, 30, 9, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (103, 2, '09.00 - 09.30', '', 9, 0, 9, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (104, 2, '09.30 - 10.00', '', 9, 30, 10, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (105, 2, '10.00 - 10.30', '', 10, 0, 10, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (106, 2, '10.30 - 11.00', '', 10, 30, 11, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (107, 2, '11.00 - 11.30', '', 11, 0, 11, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (108, 2, '11.30 - 12.00', '', 11, 30, 12, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (109, 2, '12.00 - 12.30', '', 12, 0, 12, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (110, 2, '12.30 - 13.00', '', 12, 30, 13, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (111, 2, '13.00 - 13.30', '', 13, 0, 13, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (112, 2, '13.30 - 14.00', '', 13, 30, 14, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (113, 2, '14.00 - 14.30', '', 14, 0, 14, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (114, 2, '14.30 - 15.00', '', 14, 30, 15, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (115, 2, '15.00 - 15.30', '', 15, 0, 15, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (116, 2, '15.30 - 16.00', '', 15, 30, 16, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (117, 2, '16.00 - 16.30', '', 16, 0, 16, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (118, 2, '16.30 - 17.00', '', 16, 30, 17, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (119, 2, '17.00 - 17.30', '', 17, 0, 17, 30, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (120, 2, '17.30 - 18.00', '', 17, 30, 18, 0, 'PERIOD_ACTIVE');
INSERT INTO `period` VALUES (121, 2, '18.00 - 18.30', '', 18, 0, 18, 30, 'PERIOD_INACTIVE');
INSERT INTO `period` VALUES (122, 2, '18.30 - 19.00', '', 18, 30, 19, 0, 'PERIOD_INACTIVE');
INSERT INTO `period` VALUES (123, 2, '19.00 - 19.30', '', 19, 0, 19, 30, 'PERIOD_INACTIVE');
INSERT INTO `period` VALUES (124, 2, '19.30 - 20.00', '', 19, 30, 20, 0, 'PERIOD_INACTIVE');
INSERT INTO `period` VALUES (125, 2, '20.00 - 20.30', '', 20, 0, 20, 30, 'PERIOD_INACTIVE');
INSERT INTO `period` VALUES (126, 2, '20.30 - 21.00', '', 20, 30, 21, 0, 'PERIOD_INACTIVE');
INSERT INTO `period` VALUES (127, 2, '21.00 - 21.30', '', 21, 0, 21, 30, 'PERIOD_INACTIVE');
INSERT INTO `period` VALUES (128, 2, '21.30 - 22.00', '', 21, 30, 22, 0, 'PERIOD_INACTIVE');

-- --------------------------------------------------------

-- 
-- Table structure for table `period_group`
-- 

CREATE TABLE `period_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status_code` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `period_group`
-- 

INSERT INTO `period_group` VALUES (1, 'Default Period', 'Default period is make each period for 30 minutes. Start from 8.00 - 22.00', 'PERIOD_GROUP_ACTIVE');
INSERT INTO `period_group` VALUES (2, 'Meeting Period', '', 'PERIOD_GROUP_ACTIVE');

-- --------------------------------------------------------

-- 
-- Table structure for table `permission`
-- 

CREATE TABLE `permission` (
  `permission_code` varchar(255) NOT NULL,
  `permission_group_id` int(11) default NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`permission_code`),
  UNIQUE KEY `name` (`name`),
  KEY `permission_group_id` (`permission_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `permission`
-- 

INSERT INTO `permission` VALUES ('CHECK_STATUS_REQUEST_BOOKING', 3, 'Check Status Booking', '');
INSERT INTO `permission` VALUES ('CHECK_STATUS_REQUEST_BORROW', 4, 'Check Status Borrow', '');
INSERT INTO `permission` VALUES ('CONFIRM_USER', 1, 'Confirm User', '');
INSERT INTO `permission` VALUES ('CREATE_DEPARTMENT', 8, 'Create Department', '');
INSERT INTO `permission` VALUES ('CREATE_EQUIPMENT', 7, 'Create Equipment', '');
INSERT INTO `permission` VALUES ('CREATE_EQUIPMENT_TYPE', 6, 'Create Equipment Type', '');
INSERT INTO `permission` VALUES ('CREATE_GALLERY', 16, 'Create Gallery', '');
INSERT INTO `permission` VALUES ('CREATE_LINK', 17, 'Create Link', '');
INSERT INTO `permission` VALUES ('CREATE_NEWS', 15, 'Create News', '');
INSERT INTO `permission` VALUES ('CREATE_POSITION', 10, 'Create Position', '');
INSERT INTO `permission` VALUES ('CREATE_PRESEN_TYPE', 11, 'Create Present Type', '');
INSERT INTO `permission` VALUES ('CREATE_REQUEST_BOOKING', 3, 'Create Request Booking', '');
INSERT INTO `permission` VALUES ('CREATE_REQUEST_BORROW', 4, 'Create Request Borrow', '');
INSERT INTO `permission` VALUES ('CREATE_REQUEST_SERVICE', 5, 'Create Request Service', '');
INSERT INTO `permission` VALUES ('CREATE_ROLE', 2, 'Create Role', '');
INSERT INTO `permission` VALUES ('CREATE_ROOM', 9, 'Create Room', '');
INSERT INTO `permission` VALUES ('CREATE_SEMESTER', 14, 'Create Semester', '');
INSERT INTO `permission` VALUES ('CREATE_SERVICE_TYPE', 12, 'Create Service Type', '');
INSERT INTO `permission` VALUES ('CREATE_SERVICE_TYPE_ITEM', 13, 'Create Service Type Item', '');
INSERT INTO `permission` VALUES ('CREATE_SOCIAL_MEDIA', 18, 'Create Social Media', '');
INSERT INTO `permission` VALUES ('CREATE_USER', 1, 'Create User', 'Can create user login and user information for user.');
INSERT INTO `permission` VALUES ('DELETE_DEPARTMENT', 8, 'Delete Department', '');
INSERT INTO `permission` VALUES ('DELETE_EQUIPMENT', 7, 'Delete Equipment', '');
INSERT INTO `permission` VALUES ('DELETE_EQUIPMENT_TYPE', 6, 'Delete Equipment Type', '');
INSERT INTO `permission` VALUES ('DELETE_GALLERY', 16, 'Delete Gallery', '');
INSERT INTO `permission` VALUES ('DELETE_LINK', 17, 'Delete Link', '');
INSERT INTO `permission` VALUES ('DELETE_NEWS', 15, 'Delete News', '');
INSERT INTO `permission` VALUES ('DELETE_POSITION', 10, 'Delete Position', '');
INSERT INTO `permission` VALUES ('DELETE_PRESENT_TYPE', 11, 'Delete Present Type', '');
INSERT INTO `permission` VALUES ('DELETE_REQUEST_BOOKING', 3, 'Delete Request Booking', '');
INSERT INTO `permission` VALUES ('DELETE_REQUEST_BORROW', 4, 'Delete Request Borrow', '');
INSERT INTO `permission` VALUES ('DELETE_REQUEST_SERVICE', 5, 'Delete Request Service', '');
INSERT INTO `permission` VALUES ('DELETE_ROLE', 2, 'Delete Role', '');
INSERT INTO `permission` VALUES ('DELETE_ROOM', 9, 'Delete Room', '');
INSERT INTO `permission` VALUES ('DELETE_SEMESTER', 14, 'Delete Semester', '');
INSERT INTO `permission` VALUES ('DELETE_SERVICE_TYPE', 12, 'Delete Service Type', '');
INSERT INTO `permission` VALUES ('DELETE_SERVICE_TYPE_ITEM', 13, 'Delete Service Type Item', '');
INSERT INTO `permission` VALUES ('DELETE_SOCIAL_MEDIA', 18, 'Delete Social Media', '');
INSERT INTO `permission` VALUES ('DELETE_USER', 1, 'Delete User', '');
INSERT INTO `permission` VALUES ('EDIT_REQUEST_BOOKING', 3, 'Edit', '');
INSERT INTO `permission` VALUES ('FULL_ADMIN', NULL, 'Full Admin', '"Full Admin" can access all module.');
INSERT INTO `permission` VALUES ('UPDATE_DEPARTMENT', 8, 'Update Department', '');
INSERT INTO `permission` VALUES ('UPDATE_EQUIPMENT', 7, 'Update Equipment', '');
INSERT INTO `permission` VALUES ('UPDATE_EQUIPMENT_TYPE', 6, 'Update Equipment Type', '');
INSERT INTO `permission` VALUES ('UPDATE_GALLERY', 16, 'Update Gallery', '');
INSERT INTO `permission` VALUES ('UPDATE_LINK', 17, 'Update Link', '');
INSERT INTO `permission` VALUES ('UPDATE_NEWS', 15, 'Update News', '');
INSERT INTO `permission` VALUES ('UPDATE_POSITION', 10, 'Update Position', '');
INSERT INTO `permission` VALUES ('UPDATE_PRESENT_TYPE', 11, 'Update Present Type', '');
INSERT INTO `permission` VALUES ('UPDATE_REQUEST_BORROW', 4, 'Update Request Borrow', '');
INSERT INTO `permission` VALUES ('UPDATE_REQUEST_SERVICE', 5, 'Update Request Service', '');
INSERT INTO `permission` VALUES ('UPDATE_ROLE', 2, 'Update Role', '');
INSERT INTO `permission` VALUES ('UPDATE_ROOM', 9, 'Update Room', '');
INSERT INTO `permission` VALUES ('UPDATE_SEMESTER', 14, 'Update Semester', '');
INSERT INTO `permission` VALUES ('UPDATE_SERVICE_TYPE', 12, 'Update Service Type', '');
INSERT INTO `permission` VALUES ('UPDATE_SERVICE_TYPE)ITEM', 13, 'Update Service Type Item', '');
INSERT INTO `permission` VALUES ('UPDATE_SOCIAL_MEDIA', 18, 'Update Social Media', '');
INSERT INTO `permission` VALUES ('UPDATE_USER', 1, 'Update User', '');
INSERT INTO `permission` VALUES ('VIEW_ALL_REQUEST_BOOKING', 3, 'View All Request Booking', '');
INSERT INTO `permission` VALUES ('VIEW_ALL_REQUEST_BORROW', 4, 'View All Request Borrow', '');
INSERT INTO `permission` VALUES ('VIEW_ALL_REQUEST_SERVICE', 5, 'View All Request Service', '');
INSERT INTO `permission` VALUES ('VIEW_DEPARTMENT', 8, 'View Department', '');
INSERT INTO `permission` VALUES ('VIEW_EQUIPMENT', 7, 'View Equipment', '');
INSERT INTO `permission` VALUES ('VIEW_EQUIPMENT_TYPE', 6, 'View Equipment Type', '');
INSERT INTO `permission` VALUES ('VIEW_GALLERY', 16, 'View Gallery', '');
INSERT INTO `permission` VALUES ('VIEW_LINK', 17, 'View Link', '');
INSERT INTO `permission` VALUES ('VIEW_NEWS', 15, 'View News', '');
INSERT INTO `permission` VALUES ('VIEW_POSITION', 10, 'View Position', '');
INSERT INTO `permission` VALUES ('VIEW_PRESENT_TYPE', 11, 'View Present Type', '');
INSERT INTO `permission` VALUES ('VIEW_REQUEST_BOOKING', 3, 'View Request Booking', '');
INSERT INTO `permission` VALUES ('VIEW_REQUEST_BORROW', 4, 'View Request Borrow', '');
INSERT INTO `permission` VALUES ('VIEW_REQUEST_SERVICE', 5, 'View Request Service', '');
INSERT INTO `permission` VALUES ('VIEW_ROLE', 2, 'View Role', '');
INSERT INTO `permission` VALUES ('VIEW_ROOM', 9, 'View Room', '');
INSERT INTO `permission` VALUES ('VIEW_SEMESTER', 14, 'View Semester', '');
INSERT INTO `permission` VALUES ('VIEW_SERVICE_TYPE', 12, 'View Service Type', '');
INSERT INTO `permission` VALUES ('VIEW_SERVICE_TYPE_ITEM', 13, 'View Service Type Item', '');
INSERT INTO `permission` VALUES ('VIEW_SOCIAL_MEDIA', 18, 'View Social Media', '');
INSERT INTO `permission` VALUES ('VIEW_USER', 1, 'View User', 'View user information');

-- --------------------------------------------------------

-- 
-- Table structure for table `permission_group`
-- 

CREATE TABLE `permission_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- 
-- Dumping data for table `permission_group`
-- 

INSERT INTO `permission_group` VALUES (1, 'User', '');
INSERT INTO `permission_group` VALUES (2, 'Role', '');
INSERT INTO `permission_group` VALUES (3, 'Request Booking', '');
INSERT INTO `permission_group` VALUES (4, 'Request Borrow', '');
INSERT INTO `permission_group` VALUES (5, 'Request Service', '');
INSERT INTO `permission_group` VALUES (6, 'Equipment Type', '');
INSERT INTO `permission_group` VALUES (7, 'Equipment', '');
INSERT INTO `permission_group` VALUES (8, 'Department', '');
INSERT INTO `permission_group` VALUES (9, 'Room', '');
INSERT INTO `permission_group` VALUES (10, 'Position', '');
INSERT INTO `permission_group` VALUES (11, 'Present Type', '');
INSERT INTO `permission_group` VALUES (12, 'Service Type', '');
INSERT INTO `permission_group` VALUES (13, 'Service Type Item', '');
INSERT INTO `permission_group` VALUES (14, 'Semester', '');
INSERT INTO `permission_group` VALUES (15, 'ED Tech News', '');
INSERT INTO `permission_group` VALUES (16, 'Gallery', '');
INSERT INTO `permission_group` VALUES (17, 'Link', '');
INSERT INTO `permission_group` VALUES (18, 'Social Media', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `position`
-- 

CREATE TABLE `position` (
  `id` int(11) NOT NULL auto_increment,
  `position_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `position_code` (`position_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `position`
-- 

INSERT INTO `position` VALUES (1, 'fvcxv', 'sdfsdf', 'dsfsd');
INSERT INTO `position` VALUES (2, '', 'afds', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `present_type`
-- 

CREATE TABLE `present_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `present_type`
-- 

INSERT INTO `present_type` VALUES (1, 'Powerpoint presentation', '');
INSERT INTO `present_type` VALUES (2, 'VDO presentation', '');
INSERT INTO `present_type` VALUES (3, 'MUIC presentation', '');
INSERT INTO `present_type` VALUES (4, 'Paper or Written on paper', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_booking`
-- 

CREATE TABLE `request_booking` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `request_booking_type_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `semester_id` int(11) default NULL,
  `request_date` date default NULL,
  `request_day_in_week` int(11) default NULL,
  `period_start` int(11) NOT NULL,
  `period_end` int(11) NOT NULL,
  `description` text NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `course_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_login_id` (`user_login_id`),
  KEY `room_id` (`room_id`),
  KEY `period_start` (`period_start`),
  KEY `period_end` (`period_end`),
  KEY `status_code` (`status_code`),
  KEY `semester_id` (`semester_id`),
  KEY `request_booking_type_id` (`request_booking_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

-- 
-- Dumping data for table `request_booking`
-- 

INSERT INTO `request_booking` VALUES (23, 1, 3, 4, NULL, '2013-11-12', NULL, 110, 112, '', 'REQUEST_WAIT_APPROVE', '2013-11-12 10:20:03', '');
INSERT INTO `request_booking` VALUES (24, 1, 2, 1305, NULL, '0000-00-00', NULL, 4, 4, '', 'REQUEST_WAIT_APPROVE', '2013-11-20 08:36:39', 'CE111');
INSERT INTO `request_booking` VALUES (25, 1, 2, 1407, NULL, '0000-00-00', NULL, 3, 3, '', 'REQUEST_WAIT_APPROVE', '2013-11-22 16:19:17', 'CE111');
INSERT INTO `request_booking` VALUES (26, 1, 2, 1405, NULL, '0000-00-00', 1, 2, 2, '', 'REQUEST_WAIT_APPROVE', '2013-11-22 21:26:17', 'CE111');
INSERT INTO `request_booking` VALUES (27, 1, 2, 1408, NULL, '0000-00-00', NULL, 3, 3, '', 'REQUEST_WAIT_APPROVE', '2013-11-22 21:37:36', 'Monday1200-1350');
INSERT INTO `request_booking` VALUES (28, 1, 2, 1307, 3, NULL, 5, 3, 3, '', 'REQUEST_WAIT_APPROVE', '2013-11-22 21:56:15', 'Th12_1350');
INSERT INTO `request_booking` VALUES (29, 4, 3, 1210, NULL, '2013-11-29', NULL, 105, 107, '', 'REQUEST_CANCEL', '2013-11-28 07:48:59', '');
INSERT INTO `request_booking` VALUES (30, 4, 3, 2207, NULL, '2013-11-29', NULL, 112, 114, '', 'REQUEST_APPROVE', '2013-11-28 07:50:35', '');
INSERT INTO `request_booking` VALUES (31, 4, 3, 4, NULL, '2013-11-30', NULL, 109, 116, '', 'REQUEST_APPROVE', '2013-11-28 07:57:32', '');
INSERT INTO `request_booking` VALUES (32, 4, 2, 1406, 3, NULL, 5, 3, 3, '', 'REQUEST_CANCEL', '2013-11-28 09:41:33', 'ce311 sem R1406');
INSERT INTO `request_booking` VALUES (33, 4, 2, 1406, 3, NULL, 7, 4, 4, '', 'REQUEST_APPROVE', '2013-11-28 09:44:24', 'R1406 dvd');
INSERT INTO `request_booking` VALUES (34, 4, 2, 1504, 3, NULL, 1, 6, 6, '', 'REQUEST_APPROVE', '2013-11-28 09:48:57', 'xxxx');
INSERT INTO `request_booking` VALUES (35, 2, 1, 1302, NULL, '2014-01-03', NULL, 5, 5, '', 'REQUEST_APPROVE', '2014-01-03 13:54:55', '20140103_1302_DVD');
INSERT INTO `request_booking` VALUES (36, 1, 1, 1404, NULL, '2014-01-07', NULL, 2, 2, '', 'REQUEST_APPROVE', '2014-01-03 14:27:54', '20140107_1000_1150_Vis');
INSERT INTO `request_booking` VALUES (37, 6, 1, 1304, NULL, '2014-01-06', NULL, 1, 1, '', 'REQUEST_APPROVE', '2014-01-04 15:27:52', '20140601_DVD_1304');
INSERT INTO `request_booking` VALUES (38, 6, 2, 1302, 4, NULL, 2, 4, 4, '', 'REQUEST_APPROVE', '2014-01-06 07:35:36', '20140601_DVD_1304');
INSERT INTO `request_booking` VALUES (39, 6, 3, 1210, NULL, '2014-01-21', NULL, 101, 104, '', 'REQUEST_APPROVE', '2014-01-13 07:46:15', '');
INSERT INTO `request_booking` VALUES (40, 6, 3, 2, NULL, '2014-01-23', NULL, 104, 109, '', 'REQUEST_CANCEL', '2014-01-21 07:52:07', '');
INSERT INTO `request_booking` VALUES (41, 6, 3, 2, NULL, '2014-01-09', NULL, 101, 106, '', 'REQUEST_WAIT_APPROVE', '2014-01-04 18:20:12', '');
INSERT INTO `request_booking` VALUES (42, 6, 3, 2, NULL, '2014-01-27', NULL, 116, 119, '', 'REQUEST_CANCEL', '2014-01-04 18:46:56', '');
INSERT INTO `request_booking` VALUES (43, 6, 3, 4, NULL, '2014-01-27', NULL, 113, 116, '', 'REQUEST_APPROVE', '2014-01-04 18:48:32', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_booking_activity_detail`
-- 

CREATE TABLE `request_booking_activity_detail` (
  `id` int(11) NOT NULL auto_increment,
  `event_type_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `room_type_id` (`room_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

-- 
-- Dumping data for table `request_booking_activity_detail`
-- 

INSERT INTO `request_booking_activity_detail` VALUES (23, 1, 1, 'xxx', '');
INSERT INTO `request_booking_activity_detail` VALUES (29, 2, 2, 'File.docx PPT R1210', 'upload/files/1385599586/File.docx');
INSERT INTO `request_booking_activity_detail` VALUES (30, 3, 2, 'R2207 Visualizer', 'upload/files/1385599804/File.tmp');
INSERT INTO `request_booking_activity_detail` VALUES (31, 3, 2, 'SPF Vdo present', 'upload/files/1385600223/File.zip');
INSERT INTO `request_booking_activity_detail` VALUES (39, 1, 1, 'test activity', 'upload/files/1389573467/File.jpg');
INSERT INTO `request_booking_activity_detail` VALUES (40, 1, 1, 't', 'upload/files/1390265489/File.js');
INSERT INTO `request_booking_activity_detail` VALUES (41, 1, 1, 't', 'upload/files/1388834353/File.md');
INSERT INTO `request_booking_activity_detail` VALUES (42, 1, 1, '', 'upload/files/1388835999/File.js');
INSERT INTO `request_booking_activity_detail` VALUES (43, 4, 2, '1', 'upload/files/1388836086/File.md');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_booking_activity_present_type`
-- 

CREATE TABLE `request_booking_activity_present_type` (
  `id` int(11) NOT NULL auto_increment,
  `request_booking_activity_id` int(11) NOT NULL,
  `present_type_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_booking_activity_id` (`request_booking_activity_id`),
  KEY `present_type_id` (`present_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- 
-- Dumping data for table `request_booking_activity_present_type`
-- 

INSERT INTO `request_booking_activity_present_type` VALUES (6, 23, 3);
INSERT INTO `request_booking_activity_present_type` VALUES (7, 29, 1);
INSERT INTO `request_booking_activity_present_type` VALUES (8, 30, 3);
INSERT INTO `request_booking_activity_present_type` VALUES (9, 30, 4);
INSERT INTO `request_booking_activity_present_type` VALUES (10, 31, 2);
INSERT INTO `request_booking_activity_present_type` VALUES (11, 39, 1);
INSERT INTO `request_booking_activity_present_type` VALUES (12, 40, 1);
INSERT INTO `request_booking_activity_present_type` VALUES (13, 41, 3);
INSERT INTO `request_booking_activity_present_type` VALUES (14, 42, 1);
INSERT INTO `request_booking_activity_present_type` VALUES (15, 43, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `request_booking_alert_log`
-- 

CREATE TABLE `request_booking_alert_log` (
  `id` int(11) NOT NULL auto_increment,
  `request_booking_id` int(11) NOT NULL,
  `alert_date` date NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_booking_id` (`request_booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `request_booking_alert_log`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `request_booking_equipment_type`
-- 

CREATE TABLE `request_booking_equipment_type` (
  `id` int(11) NOT NULL auto_increment,
  `request_booking_id` int(11) NOT NULL,
  `equipment_type_id` int(11) NOT NULL,
  `quantity` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `request_booking_id` (`request_booking_id`),
  KEY `equipment_type_id` (`equipment_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

-- 
-- Dumping data for table `request_booking_equipment_type`
-- 

INSERT INTO `request_booking_equipment_type` VALUES (38, 23, 7, 1);
INSERT INTO `request_booking_equipment_type` VALUES (39, 24, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (40, 24, 6, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (41, 25, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (42, 26, 6, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (43, 27, 4, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (44, 27, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (45, 28, 4, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (46, 28, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (47, 29, 4, 1);
INSERT INTO `request_booking_equipment_type` VALUES (48, 30, 6, 1);
INSERT INTO `request_booking_equipment_type` VALUES (49, 31, 4, 1);
INSERT INTO `request_booking_equipment_type` VALUES (50, 32, 4, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (51, 33, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (52, 34, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (53, 35, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (54, 36, 6, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (55, 37, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (56, 38, 5, NULL);
INSERT INTO `request_booking_equipment_type` VALUES (57, 39, 2, 1);
INSERT INTO `request_booking_equipment_type` VALUES (58, 40, 2, 1);
INSERT INTO `request_booking_equipment_type` VALUES (59, 41, 7, 1);
INSERT INTO `request_booking_equipment_type` VALUES (60, 42, 4, 1);
INSERT INTO `request_booking_equipment_type` VALUES (61, 43, 4, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `request_booking_type`
-- 

CREATE TABLE `request_booking_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `request_booking_type`
-- 

INSERT INTO `request_booking_type` VALUES (1, 'Daily', '');
INSERT INTO `request_booking_type` VALUES (2, 'Semester', '');
INSERT INTO `request_booking_type` VALUES (3, 'Activity / Meeting', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_borrow`
-- 

CREATE TABLE `request_borrow` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `approve_by` varchar(255) NOT NULL,
  `chef_email` varchar(255) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `from_date` date NOT NULL,
  `thru_date` date NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `user_login_id` (`user_login_id`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

-- 
-- Dumping data for table `request_borrow`
-- 

INSERT INTO `request_borrow` VALUES (24, 1, 'WHITHIN_MUIC', '', 'pawit1357@hotmail.com', 1, 'nn', '2013-11-13', '2013-11-15', 'REQUEST_BORROW_APPROVED', '2013-11-12 10:33:33');
INSERT INTO `request_borrow` VALUES (25, 1, 'WITHOUT_MUIC', 'pawit1357@hotmail.com', 'pawit1357@hotmail.com', 1, 'test', '2013-11-22', '2013-11-23', 'REQUEST_BORROW_WAIT_FOR_APPROVE', '2013-11-22 21:25:33');
INSERT INTO `request_borrow` VALUES (26, 1, 'WITHOUT_MUIC', 'pawit1357@hotmail.com', 'pawit1357@hotmail.com', 1, 'ttt', '2013-11-27', '2013-11-29', 'REQUEST_BORROW_COMPLETED', '2013-11-27 21:03:37');
INSERT INTO `request_borrow` VALUES (27, 4, 'WHITHIN_MUIC', '', 'pawit13571@hotmail.com', 1, 'test', '2013-11-29', '2013-11-30', 'REQUEST_BORROW_COMPLETED', '2013-11-28 12:32:49');
INSERT INTO `request_borrow` VALUES (28, 4, 'WITHOUT_MUIC', 'pawit1357@hotmail.com', 'pawit13571@hotmail.com', 2, 'test', '2013-12-04', '2013-12-05', 'REQUEST_BORROW_APPROVED', '2013-11-28 12:35:32');
INSERT INTO `request_borrow` VALUES (29, 1, 'WHITHIN_MUIC', '', 'pawit13571@hotmail.com', 1, 'test', '2013-12-03', '2013-12-04', 'REQUEST_BORROW_APPROVED', '2013-12-03 10:55:46');
INSERT INTO `request_borrow` VALUES (30, 6, 'WHITHIN_MUIC', '', 'pawitvaap@gmail.com', 1, 'ยืม Remote presenter', '2014-01-23', '2014-01-24', 'REQUEST_BORROW_COMPLETED', '2014-01-23 08:15:43');
INSERT INTO `request_borrow` VALUES (31, 6, 'WITHOUT_MUIC', 'pawit1357@hotmail.com', 'pawitvaap@gmail.com', 1, 'ยืมไปภายนอก', '2014-01-26', '2014-01-29', 'REQUEST_BORROW_APPROVED', '2014-01-23 08:22:38');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_borrow_alert_log`
-- 

CREATE TABLE `request_borrow_alert_log` (
  `id` int(11) NOT NULL auto_increment,
  `request_borrow_id` int(11) NOT NULL,
  `alert_date` date NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_borrow_id` (`request_borrow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `request_borrow_alert_log`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `request_borrow_approve_link`
-- 

CREATE TABLE `request_borrow_approve_link` (
  `id` int(11) NOT NULL auto_increment,
  `request_key` varchar(255) NOT NULL,
  `request_borrow_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `approve_type` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `request_borrow_approve_link`
-- 

INSERT INTO `request_borrow_approve_link` VALUES (1, '71f6278d140af599e06ad9bf1ba03cb05dd9db5e033da9c6fb5ba83c7a7ebea99597353e41e6957b5e7aa79214fcb256ae0eb3eed39d2bcef4622b2499a05fe6a87ff679a2f3e71d9181a67b7542122cf033ab37c30201f73f142449d037028d', 25, '2013-11-22 21:25:37', 'ACTIVE', 'APPROVE_1');
INSERT INTO `request_borrow_approve_link` VALUES (2, 'ca46c1b9512a7a8315fa3c5a946e8265cf004fdc76fa1a4f25f62e0eb5261ca3a2cc63e065705fe938a4dda49092966f333222170ab9edca4785c39f55221fe77e7e69ea3384874304911625ac34321ca49e9411d64ff53eccfdd09ad10a15b3', 26, '2013-11-27 21:03:46', 'ACTIVE', 'APPROVE_1');
INSERT INTO `request_borrow_approve_link` VALUES (3, '7501e5d4da87ac39d782741cd794002d6da9003b743b65f4c0ccd295cc484e5755b37c5c270e5d84c793e486d798c01d013d407166ec4fa56eb1e1f8cbe183b92bd7f907b7f5b6bbd91822c0c7b835f6e56b06c51e1049195d7b26d043c478a0', 28, '2013-11-28 12:35:35', 'ACTIVE', 'APPROVE_1');
INSERT INTO `request_borrow_approve_link` VALUES (4, '86e8f7ab32cfd12577bc2619bc635690c1e39d912d21c91dce811d6da9929ae810c272d06794d3e5785d5e7c5356e9ffdea9ddb25cbf2352cf4dec30222a02a5bad5f33780c42f2588878a9d07405083d5cfead94f5350c12c322b5b664544c1', 31, '2014-01-23 08:22:42', 'ACTIVE', 'APPROVE_1');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_borrow_equipment_type`
-- 

CREATE TABLE `request_borrow_equipment_type` (
  `id` int(11) NOT NULL auto_increment,
  `request_borrow_id` int(11) NOT NULL,
  `equipment_type_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_borrow_id` (`request_borrow_id`),
  KEY `equipment_type_id` (`equipment_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

-- 
-- Dumping data for table `request_borrow_equipment_type`
-- 

INSERT INTO `request_borrow_equipment_type` VALUES (42, 24, 2, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (43, 25, 1, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (44, 26, 2, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (45, 27, 5, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (46, 28, 2, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (47, 28, 10, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (48, 29, 4, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (49, 30, 2, 1);
INSERT INTO `request_borrow_equipment_type` VALUES (50, 31, 5, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `request_service`
-- 

CREATE TABLE `request_service` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `time_period` int(11) NOT NULL,
  `status_code` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_login_id` (`user_login_id`),
  KEY `time_period` (`time_period`),
  KEY `status_code` (`status_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `request_service`
-- 

INSERT INTO `request_service` VALUES (1, 1, 'test', 'upload/files/1385561102/File.JPG', 1, '2013-11-28', 112, 'REQUEST_APPROVE', '2013-11-27 21:05:16');
INSERT INTO `request_service` VALUES (2, 4, 'graphic-file formats', 'upload/files/1385617301/File.zip', 1, '2013-11-29', 117, 'REQUEST_APPROVE', '2013-11-28 12:42:06');
INSERT INTO `request_service` VALUES (3, 1, 'test', 'upload/files/1386043012/File.xls', 1, '2013-12-12', 112, 'REQUEST_APPROVE', '2013-12-03 10:57:15');
INSERT INTO `request_service` VALUES (4, 6, 'Printing A4', 'upload/files/1390440947/File.js', 1, '2014-01-24', 110, 'REQUEST_APPROVE', '2014-01-23 08:36:11');
INSERT INTO `request_service` VALUES (5, 6, '1', 'upload/files/1390441353/File.md', 1, '2014-01-25', 112, 'REQUEST_ISERVICE_COMPLETED', '2014-01-23 08:42:47');
INSERT INTO `request_service` VALUES (6, 6, '1', 'upload/files/1390441544/File.js', 1, '2014-01-24', 112, 'REQUEST_ISERVICE_COMPLETED', '2014-01-23 08:45:57');
INSERT INTO `request_service` VALUES (7, 6, 'vdo-rec-sound', 'upload/files/1390441910/File.js', 1, '2014-01-10', 112, 'REQUEST_ISERVICE_COMPLETED', '2014-01-23 08:52:08');
INSERT INTO `request_service` VALUES (8, 6, 'ccc', 'upload/files/1390443152/File.md', 1, '2014-01-30', 112, 'REQUEST_ISERVICE_COMPLETED', '2014-01-23 09:12:46');
INSERT INTO `request_service` VALUES (9, 6, 'g-fileformat', 'upload/files/1390443423/File.md', 1, '2014-01-20', 110, 'REQUEST_ISERVICE_CANCEL', '2014-01-23 09:17:21');
INSERT INTO `request_service` VALUES (10, 6, 'g-book', 'upload/files/1390443511/File.js', 1, '2014-01-24', 112, 'REQUEST_ISERVICE_COMPLETED', '2014-01-23 09:18:48');
INSERT INTO `request_service` VALUES (11, 6, 'dsfas', 'upload/files/1390444455/File.js', 1, '2014-01-23', 110, 'REQUEST_ISERVICE_WAIT_APPROVE', '2014-01-23 09:34:28');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_service_detail`
-- 

CREATE TABLE `request_service_detail` (
  `id` int(11) NOT NULL auto_increment,
  `request_service_id` int(11) NOT NULL,
  `request_service_type_detail_id` int(11) default NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_service_id` (`request_service_id`),
  KEY `request_service_type_detail_id` (`request_service_type_detail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `request_service_detail`
-- 

INSERT INTO `request_service_detail` VALUES (1, 1, 2, '');
INSERT INTO `request_service_detail` VALUES (2, 2, 5, '');
INSERT INTO `request_service_detail` VALUES (3, 3, 4, '');
INSERT INTO `request_service_detail` VALUES (4, 4, 1, '');
INSERT INTO `request_service_detail` VALUES (5, 5, 9, '');
INSERT INTO `request_service_detail` VALUES (6, 6, 8, '');
INSERT INTO `request_service_detail` VALUES (7, 7, 9, '');
INSERT INTO `request_service_detail` VALUES (8, 8, 13, '');
INSERT INTO `request_service_detail` VALUES (9, 9, 5, '');
INSERT INTO `request_service_detail` VALUES (10, 10, 6, '');
INSERT INTO `request_service_detail` VALUES (11, 11, 2, '');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_service_type`
-- 

CREATE TABLE `request_service_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `request_service_type`
-- 

INSERT INTO `request_service_type` VALUES (1, 'Printing', '');
INSERT INTO `request_service_type` VALUES (2, 'Graphic', '');
INSERT INTO `request_service_type` VALUES (3, 'Sticker', '');
INSERT INTO `request_service_type` VALUES (4, 'VDO Records', '');
INSERT INTO `request_service_type` VALUES (5, 'Sound Records', '');
INSERT INTO `request_service_type` VALUES (6, 'Copy', '');
INSERT INTO `request_service_type` VALUES (7, 'VDO Record', '');
INSERT INTO `request_service_type` VALUES (8, 'Sound Record', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `request_service_type_detail`
-- 

CREATE TABLE `request_service_type_detail` (
  `id` int(11) NOT NULL auto_increment,
  `request_service_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `request_service_type_id` (`request_service_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- 
-- Dumping data for table `request_service_type_detail`
-- 

INSERT INTO `request_service_type_detail` VALUES (1, 1, 'A4', '');
INSERT INTO `request_service_type_detail` VALUES (2, 1, 'A3', '');
INSERT INTO `request_service_type_detail` VALUES (3, 1, 'A2', '');
INSERT INTO `request_service_type_detail` VALUES (4, 2, 'Poster / Paper', '');
INSERT INTO `request_service_type_detail` VALUES (5, 2, 'File formats', '');
INSERT INTO `request_service_type_detail` VALUES (6, 2, 'Books', '');
INSERT INTO `request_service_type_detail` VALUES (7, 3, 'Lable', '');
INSERT INTO `request_service_type_detail` VALUES (8, 3, 'Backdrop', '');
INSERT INTO `request_service_type_detail` VALUES (9, 4, 'Sound', '');
INSERT INTO `request_service_type_detail` VALUES (10, 4, 'VDO', '');
INSERT INTO `request_service_type_detail` VALUES (11, 5, 'E-book', '');
INSERT INTO `request_service_type_detail` VALUES (12, 5, 'Presentation', '');
INSERT INTO `request_service_type_detail` VALUES (13, 6, 'CD', '');
INSERT INTO `request_service_type_detail` VALUES (14, 6, 'DVD', '');
INSERT INTO `request_service_type_detail` VALUES (15, 7, 'Class Room', '');
INSERT INTO `request_service_type_detail` VALUES (16, 7, 'Activity', '');
INSERT INTO `request_service_type_detail` VALUES (17, 8, 'Activity', '');
INSERT INTO `request_service_type_detail` VALUES (18, 8, 'Media Presentation', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `role`
-- 

CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `role`
-- 

INSERT INTO `role` VALUES (1, 'Admin', 'Admin can access all data', 'HIDDEN');
INSERT INTO `role` VALUES (2, 'Staff', 'Staff is person who check and manage about user and booking.', '');
INSERT INTO `role` VALUES (3, 'User', 'User can request book for equipment. ', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `role_permission`
-- 

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL auto_increment,
  `role_id` int(11) NOT NULL,
  `permission_code` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `role_id` (`role_id`,`permission_code`),
  KEY `permission_id` (`permission_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;

-- 
-- Dumping data for table `role_permission`
-- 

INSERT INTO `role_permission` VALUES (1, 1, 'FULL_ADMIN');
INSERT INTO `role_permission` VALUES (100, 2, 'CHECK_STATUS_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (103, 2, 'CHECK_STATUS_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (45, 2, 'CREATE_DEPARTMENT');
INSERT INTO `role_permission` VALUES (46, 2, 'CREATE_EQUIPMENT');
INSERT INTO `role_permission` VALUES (47, 2, 'CREATE_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (48, 2, 'CREATE_POSITION');
INSERT INTO `role_permission` VALUES (49, 2, 'CREATE_PRESEN_TYPE');
INSERT INTO `role_permission` VALUES (98, 2, 'CREATE_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (50, 2, 'CREATE_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (51, 2, 'CREATE_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (52, 2, 'CREATE_ROOM');
INSERT INTO `role_permission` VALUES (53, 2, 'CREATE_SEMESTER');
INSERT INTO `role_permission` VALUES (54, 2, 'CREATE_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (55, 2, 'CREATE_SERVICE_TYPE_ITEM');
INSERT INTO `role_permission` VALUES (34, 2, 'CREATE_USER');
INSERT INTO `role_permission` VALUES (56, 2, 'DELETE_DEPARTMENT');
INSERT INTO `role_permission` VALUES (57, 2, 'DELETE_EQUIPMENT');
INSERT INTO `role_permission` VALUES (58, 2, 'DELETE_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (59, 2, 'DELETE_POSITION');
INSERT INTO `role_permission` VALUES (60, 2, 'DELETE_PRESENT_TYPE');
INSERT INTO `role_permission` VALUES (61, 2, 'DELETE_ROOM');
INSERT INTO `role_permission` VALUES (62, 2, 'DELETE_SEMESTER');
INSERT INTO `role_permission` VALUES (63, 2, 'DELETE_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (64, 2, 'DELETE_SERVICE_TYPE_ITEM');
INSERT INTO `role_permission` VALUES (65, 2, 'EDIT_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (66, 2, 'UPDATE_DEPARTMENT');
INSERT INTO `role_permission` VALUES (67, 2, 'UPDATE_EQUIPMENT');
INSERT INTO `role_permission` VALUES (68, 2, 'UPDATE_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (69, 2, 'UPDATE_POSITION');
INSERT INTO `role_permission` VALUES (70, 2, 'UPDATE_PRESENT_TYPE');
INSERT INTO `role_permission` VALUES (71, 2, 'UPDATE_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (72, 2, 'UPDATE_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (73, 2, 'UPDATE_ROOM');
INSERT INTO `role_permission` VALUES (74, 2, 'UPDATE_SEMESTER');
INSERT INTO `role_permission` VALUES (75, 2, 'UPDATE_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (76, 2, 'UPDATE_SERVICE_TYPE)ITEM');
INSERT INTO `role_permission` VALUES (32, 2, 'UPDATE_USER');
INSERT INTO `role_permission` VALUES (77, 2, 'VIEW_ALL_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (78, 2, 'VIEW_ALL_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (79, 2, 'VIEW_ALL_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (80, 2, 'VIEW_DEPARTMENT');
INSERT INTO `role_permission` VALUES (81, 2, 'VIEW_EQUIPMENT');
INSERT INTO `role_permission` VALUES (82, 2, 'VIEW_EQUIPMENT_TYPE');
INSERT INTO `role_permission` VALUES (83, 2, 'VIEW_POSITION');
INSERT INTO `role_permission` VALUES (84, 2, 'VIEW_PRESENT_TYPE');
INSERT INTO `role_permission` VALUES (85, 2, 'VIEW_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (86, 2, 'VIEW_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (87, 2, 'VIEW_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (88, 2, 'VIEW_ROOM');
INSERT INTO `role_permission` VALUES (89, 2, 'VIEW_SEMESTER');
INSERT INTO `role_permission` VALUES (90, 2, 'VIEW_SERVICE_TYPE');
INSERT INTO `role_permission` VALUES (91, 2, 'VIEW_SERVICE_TYPE_ITEM');
INSERT INTO `role_permission` VALUES (35, 2, 'VIEW_USER');
INSERT INTO `role_permission` VALUES (101, 3, 'CHECK_STATUS_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (102, 3, 'CHECK_STATUS_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (99, 3, 'CREATE_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (93, 3, 'CREATE_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (94, 3, 'CREATE_REQUEST_SERVICE');
INSERT INTO `role_permission` VALUES (95, 3, 'VIEW_REQUEST_BOOKING');
INSERT INTO `role_permission` VALUES (96, 3, 'VIEW_REQUEST_BORROW');
INSERT INTO `role_permission` VALUES (97, 3, 'VIEW_REQUEST_SERVICE');

-- --------------------------------------------------------

-- 
-- Table structure for table `room`
-- 

CREATE TABLE `room` (
  `id` int(11) NOT NULL auto_increment,
  `room_group_id` int(11) NOT NULL,
  `room_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `room_code` (`room_code`),
  KEY `room_type_id` (`room_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5314 ;

-- 
-- Dumping data for table `room`
-- 

INSERT INTO `room` VALUES (1, 3, '1', 'Ground Floor', 'Ground Floor');
INSERT INTO `room` VALUES (2, 2, '2', 'SPF(Taweewattana I)', 'SPF(Taweewattana I)');
INSERT INTO `room` VALUES (3, 2, '3', 'SPF(Taweewattana II)', 'SPF(Taweewattana II)');
INSERT INTO `room` VALUES (4, 2, '4', 'SPF(Narapirom)', 'SPF(Narapirom)');
INSERT INTO `room` VALUES (5, 2, '5', 'SPF(Mahaswasdee I)', 'SPF(Mahaswasdee I)');
INSERT INTO `room` VALUES (6, 2, '6', 'SPF(Mahaswasdee II)', 'SPF(Mahaswasdee II)');
INSERT INTO `room` VALUES (7, 4, '7', 'Ed Teach. Office', 'Ed Teach. Office');
INSERT INTO `room` VALUES (1108, 1, '1108', '1108 (Lab Food.)', '1108 (Lab Food.)');
INSERT INTO `room` VALUES (1210, 2, '1210', '1210 (Seminar)', '1210 (Seminar)');
INSERT INTO `room` VALUES (1214, 2, '1214', '1214(Dean)', '1214(Dean)');
INSERT INTO `room` VALUES (1302, 1, '1302', '1302', '1302');
INSERT INTO `room` VALUES (1303, 1, '1303', '1303', '1303');
INSERT INTO `room` VALUES (1304, 1, '1304', '1304', '1304');
INSERT INTO `room` VALUES (1305, 1, '1305', '1305', '1305');
INSERT INTO `room` VALUES (1306, 1, '1306', '1306', '1306');
INSERT INTO `room` VALUES (1307, 1, '1307', '1307', '1307');
INSERT INTO `room` VALUES (1308, 1, '1308', '1308', '1308');
INSERT INTO `room` VALUES (1309, 1, '1309', '1309 (Math Clinic)', '1309 (Math Clinic)');
INSERT INTO `room` VALUES (1312, 1, '1312', '1312 (FFA)', '1312 (FFA)');
INSERT INTO `room` VALUES (1314, 1, '1314', '1314', '1314');
INSERT INTO `room` VALUES (1315, 1, '1315', '1315', '1315');
INSERT INTO `room` VALUES (1318, 2, '1318', '1318 (Audi)', '1318 (Audi)');
INSERT INTO `room` VALUES (1402, 1, '1402', '1402', '1402');
INSERT INTO `room` VALUES (1403, 1, '1403', '1403', '1403');
INSERT INTO `room` VALUES (1404, 1, '1404', '1404', '1404');
INSERT INTO `room` VALUES (1405, 1, '1405', '1405', '1405');
INSERT INTO `room` VALUES (1406, 1, '1406', '1406', '1406');
INSERT INTO `room` VALUES (1407, 1, '1407', '1407', '1407');
INSERT INTO `room` VALUES (1408, 1, '1408', '1408', '1408');
INSERT INTO `room` VALUES (1417, 1, '1417', '1417', '1417');
INSERT INTO `room` VALUES (1418, 1, '1418', '1418', '1418');
INSERT INTO `room` VALUES (1419, 1, '1419', '1419', '1419');
INSERT INTO `room` VALUES (1502, 1, '1502', '1502', '1502');
INSERT INTO `room` VALUES (1503, 1, '1503', '1503', '1503');
INSERT INTO `room` VALUES (1504, 1, '1504', '1504', '1504');
INSERT INTO `room` VALUES (1506, 1, '1506', '1506 (Lab Sci.)', '1506 (Lab Sci.)');
INSERT INTO `room` VALUES (2207, 2, '2207', '2207 (BBA)', '2207 (BBA)');
INSERT INTO `room` VALUES (2302, 1, '2302', '2302', '2302');
INSERT INTO `room` VALUES (2303, 1, '2303', '2303', '2303');
INSERT INTO `room` VALUES (2306, 1, '2306', '2306', '2306');
INSERT INTO `room` VALUES (2307, 1, '2307', '2307', '2307');
INSERT INTO `room` VALUES (2308, 1, '2308', '2308', '2308');
INSERT INTO `room` VALUES (3302, 1, '3302', '3302', '3302');
INSERT INTO `room` VALUES (3303, 1, '3303', '3303', '3303');
INSERT INTO `room` VALUES (3304, 1, '3304', '3304', '3304');
INSERT INTO `room` VALUES (3305, 1, '3305', '3305', '3305');
INSERT INTO `room` VALUES (3306, 1, '3306', '3306', '3306');
INSERT INTO `room` VALUES (3307, 1, '3307', '3307 (FFA)', '3307 (FFA)');
INSERT INTO `room` VALUES (3315, 1, '3315', '3315', '3315');
INSERT INTO `room` VALUES (3316, 1, '3316', '3316', '3316');
INSERT INTO `room` VALUES (3317, 1, '3317', '3317', '3317');
INSERT INTO `room` VALUES (3407, 1, '3407', '3407', '3407');
INSERT INTO `room` VALUES (3408, 1, '3408', '3408', '3408');
INSERT INTO `room` VALUES (3409, 1, '3409', '3409', '3409');
INSERT INTO `room` VALUES (3410, 1, '3410', '3410', '3410');
INSERT INTO `room` VALUES (3411, 1, '3411', '3411', '3411');
INSERT INTO `room` VALUES (3412, 1, '3412', '3412', '3412');
INSERT INTO `room` VALUES (3414, 2, '3414', '3414 (Meeting)', '3414 (Meeting)');
INSERT INTO `room` VALUES (3415, 2, '3415', '3415 (Meeting)', '3415 (Meeting)');
INSERT INTO `room` VALUES (3420, 1, '3420', '3420', '3420');
INSERT INTO `room` VALUES (3421, 1, '3421', '3421', '3421');
INSERT INTO `room` VALUES (3422, 1, '3422', '3422', '3422');
INSERT INTO `room` VALUES (3508, 1, '3508', '3508 (Lab Sci.)', '3508 (Lab Sci.)');
INSERT INTO `room` VALUES (5207, 1, '5207', '5207', '5207');
INSERT INTO `room` VALUES (5208, 1, '5208', '5208', '5208');
INSERT INTO `room` VALUES (5209, 1, '5209', '5209', '5209');
INSERT INTO `room` VALUES (5210, 1, '5210', '5210', '5210');
INSERT INTO `room` VALUES (5211, 1, '5211', '5211', '5211');
INSERT INTO `room` VALUES (5212, 1, '5212', '5212', '5212');
INSERT INTO `room` VALUES (5301, 1, '5301', '5301', '5301');
INSERT INTO `room` VALUES (5307, 1, '5307', '5307', '5307');
INSERT INTO `room` VALUES (5308, 1, '5308', '5308', '5308');
INSERT INTO `room` VALUES (5309, 1, '5309', '5309', '5309');
INSERT INTO `room` VALUES (5310, 1, '5310', '5310', '5310');
INSERT INTO `room` VALUES (5311, 1, '5311', '5311', '5311');
INSERT INTO `room` VALUES (5312, 1, '5312', '5312', '5312');
INSERT INTO `room` VALUES (5313, 1, '5313', '5313', '5313');

-- --------------------------------------------------------

-- 
-- Table structure for table `room_equipment`
-- 

CREATE TABLE `room_equipment` (
  `id` int(11) NOT NULL auto_increment,
  `room_id` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `room_id` (`room_id`),
  KEY `equipment_id` (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `room_equipment`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `room_group`
-- 

CREATE TABLE `room_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `room_group`
-- 

INSERT INTO `room_group` VALUES (1, 'Class Room', '');
INSERT INTO `room_group` VALUES (2, 'Meeting Room', '');
INSERT INTO `room_group` VALUES (3, 'Activity Floor', 'Activity Floor');
INSERT INTO `room_group` VALUES (4, 'Ed Tech. Office', 'Ed Tech. Office');

-- --------------------------------------------------------

-- 
-- Table structure for table `room_staff`
-- 

CREATE TABLE `room_staff` (
  `id` int(11) NOT NULL auto_increment,
  `room_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `room_id` (`room_id`,`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

-- 
-- Dumping data for table `room_staff`
-- 

INSERT INTO `room_staff` VALUES (28, 1, 2);
INSERT INTO `room_staff` VALUES (12, 1, 13);
INSERT INTO `room_staff` VALUES (13, 3, 13);
INSERT INTO `room_staff` VALUES (14, 4, 13);
INSERT INTO `room_staff` VALUES (15, 6, 13);
INSERT INTO `room_staff` VALUES (16, 1108, 13);
INSERT INTO `room_staff` VALUES (17, 1214, 13);
INSERT INTO `room_staff` VALUES (29, 1302, 2);
INSERT INTO `room_staff` VALUES (18, 1304, 13);
INSERT INTO `room_staff` VALUES (19, 1307, 13);
INSERT INTO `room_staff` VALUES (20, 1309, 13);
INSERT INTO `room_staff` VALUES (21, 1312, 13);
INSERT INTO `room_staff` VALUES (22, 1315, 13);
INSERT INTO `room_staff` VALUES (23, 1318, 13);
INSERT INTO `room_staff` VALUES (24, 1404, 13);

-- --------------------------------------------------------

-- 
-- Table structure for table `room_type`
-- 

CREATE TABLE `room_type` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `room_type`
-- 

INSERT INTO `room_type` VALUES (1, 'Classroom', '');
INSERT INTO `room_type` VALUES (2, 'Conference', '');
INSERT INTO `room_type` VALUES (3, 'Without MUIC', '');
INSERT INTO `room_type` VALUES (4, 'Other', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `semester`
-- 

CREATE TABLE `semester` (
  `id` int(11) NOT NULL auto_increment,
  `academic_year` int(11) NOT NULL,
  `semester_number` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `semester`
-- 

INSERT INTO `semester` VALUES (1, 2555, 2, '2 / 2555', '', '2012-11-05', '2013-03-15', 'ACTIVE');
INSERT INTO `semester` VALUES (2, 2555, 3, '3 / 2555', '', '2013-03-18', '2013-05-17', 'ACTIVE');
INSERT INTO `semester` VALUES (3, 2556, 1, '1 / 2556', '', '2013-05-20', '2013-12-28', 'ACTIVE');
INSERT INTO `semester` VALUES (4, 2557, 1, '1/2557', '1/2557', '2014-01-01', '2014-03-29', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `social_media`
-- 

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `social_media`
-- 

INSERT INTO `social_media` VALUES (1, 'facebook', 'dfdfadsf', 'http://www.facebook.com');

-- --------------------------------------------------------

-- 
-- Table structure for table `status`
-- 

CREATE TABLE `status` (
  `status_code` varchar(255) NOT NULL,
  `status_group_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` varchar(1) NOT NULL,
  PRIMARY KEY  (`status_code`),
  KEY `status_group_id` (`status_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `status`
-- 

INSERT INTO `status` VALUES ('ACTIVE', 'USER_LOGIN_STATUS', 'Active', '', 'Y');
INSERT INTO `status` VALUES ('INACTIVE', 'USER_LOGIN_STATUS', 'Inactive', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_ACTIVE', 'PERIOD_STATUS', 'Active', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_GROUP_ACTIVE', 'PERIOD_GROUP_STATUS', 'Active', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_GROUP_INACTIVE', 'PERIOD_GROUP_STATUS', 'Inactive', '', 'Y');
INSERT INTO `status` VALUES ('PERIOD_INACTIVE', 'PERIOD_STATUS', 'Inactive', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_APPROVE', 'REQUEST_STATUS', 'Approve', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_APPROVED', 'REQUEST_BORROW_STATUS', 'Approved', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_APPROVE_1', 'REQUEST_BORROW_STATUS', 'Approve1', '', 'N');
INSERT INTO `status` VALUES ('REQUEST_BORROW_CANCELLED', 'REQUEST_BORROW_STATUS', 'Cancelled', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_COMPLETED', 'REQUEST_BORROW_STATUS', 'Returned', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_BORROW_WAIT_FOR_APPROVE', 'REQUEST_BORROW_STATUS', 'Wait For Approve', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_CANCEL', 'REQUEST_STATUS', 'Cancel', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_ISERVICE_APPROVE', 'REQUEST_ISERVICE_STATUS', 'Approved', ' ', 'Y');
INSERT INTO `status` VALUES ('REQUEST_ISERVICE_CANCEL', 'REQUEST_ISERVICE_STATUS', 'Cancel', ' ', 'Y');
INSERT INTO `status` VALUES ('REQUEST_ISERVICE_COMPLETED', 'REQUEST_ISERVICE_STATUS', 'Completed', ' ', 'Y');
INSERT INTO `status` VALUES ('REQUEST_ISERVICE_PROCESSING', 'REQUEST_ISERVICE_STATUS', 'Processing', '', 'Y');
INSERT INTO `status` VALUES ('REQUEST_ISERVICE_WAIT_APPROVE', 'REQUEST_ISERVICE_STATUS', 'Wait for approve', ' ', 'Y');
INSERT INTO `status` VALUES ('REQUEST_WAIT_APPROVE', 'REQUEST_STATUS', 'Wait for approve', '', 'Y');

-- --------------------------------------------------------

-- 
-- Table structure for table `status_group`
-- 

CREATE TABLE `status_group` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` varchar(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `status_group`
-- 

INSERT INTO `status_group` VALUES ('PERIOD_GROUP_STATUS', 'Period Group Status', '', 'Y');
INSERT INTO `status_group` VALUES ('PERIOD_STATUS', 'Period Status', '', 'Y');
INSERT INTO `status_group` VALUES ('REQUEST_BORROW_STATUS', 'Request Borrow Status', '', 'Y');
INSERT INTO `status_group` VALUES ('REQUEST_ISERVICE_STATUS', 'Request IService Status', '', 'Y');
INSERT INTO `status_group` VALUES ('REQUEST_STATUS', 'Request Status', '', 'Y');
INSERT INTO `status_group` VALUES ('USER_LOGIN_STATUS', 'User Login Status', '', 'Y');

-- --------------------------------------------------------

-- 
-- Table structure for table `tb_email_forapprove`
-- 

CREATE TABLE `tb_email_forapprove` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status_code` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `tb_email_forapprove`
-- 

INSERT INTO `tb_email_forapprove` VALUES (0, 'pawit1357@hotmail.com', 'REQUEST_BORROW_APPROVE_1');

-- --------------------------------------------------------

-- 
-- Table structure for table `user_forget_password_request`
-- 

CREATE TABLE `user_forget_password_request` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `request_date` datetime NOT NULL,
  `status` varchar(5) default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_login_id` (`user_login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `user_forget_password_request`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user_information`
-- 

CREATE TABLE `user_information` (
  `id` int(11) NOT NULL auto_increment COMMENT 'รหัส',
  `personal_card_id` varchar(13) NOT NULL COMMENT 'เลขประจำตัวประชาชน',
  `personal_title` varchar(255) NOT NULL COMMENT 'คำนำหน้า',
  `first_name` varchar(255) NOT NULL COMMENT 'ชื่อ',
  `last_name` varchar(255) NOT NULL COMMENT 'นามสกุล',
  `gender` varchar(5) NOT NULL COMMENT 'เพศ',
  `birth_date` date NOT NULL COMMENT 'วันเกิด',
  `address1` varchar(255) NOT NULL COMMENT 'ที่อยู่ 1',
  `address2` varchar(255) NOT NULL COMMENT 'ที่อยู่ 2',
  `sub_district` varchar(255) NOT NULL COMMENT 'ตำบล',
  `district` varchar(255) NOT NULL COMMENT 'อำเภอ',
  `province` varchar(255) NOT NULL COMMENT 'จังหวัด',
  `postal_code` varchar(5) NOT NULL COMMENT 'รหัสไปรษณีย์',
  `phone` varchar(50) NOT NULL COMMENT 'โทรศัพท์',
  `mobile` varchar(50) NOT NULL COMMENT 'โทรศัพท์มือถือ',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `personal_card_id` (`personal_card_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=183 ;

-- 
-- Dumping data for table `user_information`
-- 

INSERT INTO `user_information` VALUES (1, '1', '', 'admin', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (2, '2', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (3, '3', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (4, '4', '', 'pawit', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (5, '5', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (6, '6', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (12, '12', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (13, '13', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (15, '15', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (16, '16', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (17, '17', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (18, '18', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (19, '19', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (21, '21', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (23, '23', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (24, '24', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (25, '25', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (28, '28', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (29, '29', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (30, '30', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (31, '31', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (32, '32', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (35, '35', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (36, '36', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (40, '40', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (43, '43', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (44, '44', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (45, '45', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (47, '47', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (51, '51', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (53, '53', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (54, '54', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (55, '55', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (56, '56', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (58, '58', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (59, '59', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (61, '61', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (62, '62', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (63, '63', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (64, '64', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (65, '65', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (70, '70', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (72, '72', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (75, '75', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (76, '76', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (77, '77', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (78, '78', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (80, '80', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (81, '81', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (82, '82', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (84, '84', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (85, '85', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (87, '87', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (89, '89', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (90, '90', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (91, '91', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (92, '92', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (93, '93', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (94, '94', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (95, '95', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (97, '97', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (100, '100', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (102, '102', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (112, '112', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (117, '117', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (119, '119', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (121, '121', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (124, '124', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (129, '129', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (153, '153', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (154, '154', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (155, '155', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (156, '156', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (157, '157', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (158, '158', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (159, '159', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (167, '167', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (172, '172', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (177, '177', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (181, '181', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');
INSERT INTO `user_information` VALUES (182, '182', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `user_login`
-- 

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL auto_increment COMMENT 'รหัส',
  `role_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL COMMENT 'ชื่อผู้ใช้สำหรับ login',
  `password` varchar(100) NOT NULL COMMENT 'รหัสผ่าน',
  `status` varchar(255) NOT NULL COMMENT 'สถานะ',
  `create_by` int(11) default NULL COMMENT 'สร้างโดย',
  `latest_login` datetime NOT NULL COMMENT 'เข้าสู่ระบบครั้งล่าสุด',
  `department_id` int(11) default NULL,
  `email` varchar(255) NOT NULL,
  `token_id` varchar(100) NOT NULL,
  `phone_type` varchar(1) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `role_id` (`role_id`),
  KEY `department_id` (`department_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=183 ;

-- 
-- Dumping data for table `user_login`
-- 

INSERT INTO `user_login` VALUES (1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ACTIVE', NULL, '2014-01-04 15:22:42', NULL, 'pawitvaap@gmail.com', '800f29c324f7b9df1650dfd87e59689eae9ba2c4732ce0d34c319fee4f491b7a', '1');
INSERT INTO `user_login` VALUES (2, 2, 'staff01', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '2014-01-04 15:22:52', NULL, 'pawit13571@hotmail.com', 'APA91bEX4AQm0LIFsNFLutMAfftfepp7PsIOxvIVe5RHrN7jwij3yjBao9EMGix8jk01dTWWct_UmvGRtDpJuTyBNdBwfwJSTCAI', '2');
INSERT INTO `user_login` VALUES (3, 3, '4883830', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '2013-11-12 10:09:40', 2, 'thammachart@muic.ac.th', '', '');
INSERT INTO `user_login` VALUES (4, 3, 'staff02', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '2013-11-28 19:45:18', 2, 'pawit13357@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (5, 3, 'anukulat', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '2013-07-01 17:19:14', 2, 'anukul@thaicom.com', '', '');
INSERT INTO `user_login` VALUES (6, 3, 'ananda', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '2014-01-04 15:27:17', 2, 'pawit1357@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (12, 3, 'louiself', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'Loopyinthailand@Yahoo.co.uk.', '', '');
INSERT INTO `user_login` VALUES (13, 3, 'carriecv', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'carrie_villeneuve@yahoo.com', '', '');
INSERT INTO `user_login` VALUES (15, 3, 'marinees', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'marinees@mfa.go.th', '', '');
INSERT INTO `user_login` VALUES (16, 3, 'markmr', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'markread@bigfoot.com', '', '');
INSERT INTO `user_login` VALUES (17, 3, 'matthewmp', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'mcopeland@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (18, 3, 'chaivatna', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'chaivatptc@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (19, 3, 'chaiwatcm', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'tecmn@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (21, 3, 'mhayes', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '2013-09-26 17:11:31', 2, 'mhayes@asiaaccess.net.th', '', '');
INSERT INTO `user_login` VALUES (23, 3, 'nardtida', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scntr@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (24, 3, 'chinda', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'sccac@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (25, 3, 'nathabholnk', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'nathk@rangsit.rsu.ac.th', '', '');
INSERT INTO `user_login` VALUES (28, 3, 'nopadolnc', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scnck@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (29, 3, 'oranuch', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'oranuch@muic.ac.th', '', '');
INSERT INTO `user_login` VALUES (30, 3, 'orapanoy', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'orapan@tu.ac.th', '', '');
INSERT INTO `user_login` VALUES (31, 3, 'orawan', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '2013-07-01 17:10:09', 2, 'icorawan@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (32, 3, 'orieok', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'orie_k@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (35, 3, 'pannee', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scppt@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (36, 3, 'pansiri', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'pansiri@swu.ac.th', '', '');
INSERT INTO `user_login` VALUES (40, 3, 'pleanphitpj', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scpdr@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (43, 3, 'prapasripu', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'hrds1@loxinfo.co.th', '', '');
INSERT INTO `user_login` VALUES (44, 3, 'prapeeps', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scpr@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (45, 3, 'prateeppw', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'prateepw@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (47, 3, 'prayadpp', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'prayad@muic.ac.th', '', '');
INSERT INTO `user_login` VALUES (51, 3, 'ruchira', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'ruchi55@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (53, 3, 'santisw', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scsvt@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (54, 3, 'sittiwatsl', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scsls@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (55, 3, 'grsbr', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'grsbr@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (56, 3, 'sophonk', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'spkong46@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (58, 3, 'stefanss', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'stefan.schramm@lycos.com', '', '');
INSERT INTO `user_login` VALUES (59, 3, 'stitayass', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scssr@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (61, 3, 'sujindast', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'testn@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (62, 3, 'sumalee', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scstp@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (63, 3, 'sumaleesu', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'sumalee_unsurangsie@yahoo.com', '', '');
INSERT INTO `user_login` VALUES (64, 3, 'sunanta', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'sunantachutinan@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (65, 3, 'chuanpitcd', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'chuanpitDe@yahoo.com', '', '');
INSERT INTO `user_login` VALUES (70, 3, 'decha', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'egdwl@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (72, 3, 'laornual', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'egdlo@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (75, 3, 'tanyatb', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'tjbroccoli@yahoo.com', '', '');
INSERT INTO `user_login` VALUES (76, 3, 'jade', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'jade@biotec.or.th', '', '');
INSERT INTO `user_login` VALUES (77, 3, 'sctsw', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'sctsw@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (78, 3, 'jidapajj', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'giujuan_c@yahoo.com', '', '');
INSERT INTO `user_login` VALUES (80, 3, 'tom', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'frtmm@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (81, 3, 'jittipanjc', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scjcv@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (82, 3, 'toryosp', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'toryosp@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (84, 3, 'john', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'btk1@loxinfo.co.th', '', '');
INSERT INTO `user_login` VALUES (85, 3, '218824', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'udomt@mut.ac.th', '', '');
INSERT INTO `user_login` VALUES (87, 3, 'johnjm', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'j-mcnulty@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (89, 3, 'johnjw', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'johnwren@anet.net.th', '', '');
INSERT INTO `user_login` VALUES (90, 3, 'vipava', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'a_vipa@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (91, 3, 'vishnuvs', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'vishnu_somboonpeti@yahoo.co.uk', '', '');
INSERT INTO `user_login` VALUES (92, 3, 'visithvc', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'nuvca@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (93, 3, 'vongsvatvk', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'grvpt@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (94, 3, 'kittipongkt', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'xja2@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (95, 3, 'karakotkm', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'karakottuk@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (97, 3, 'loganboydls', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'loganswanson@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (100, 3, 'wiratwj', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'wirat@src.ku.ac.th', '', '');
INSERT INTO `user_login` VALUES (102, 3, 'yingyot', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'icyingyot@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (112, 3, 'michael', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'michael@muic.ac.th', '', '');
INSERT INTO `user_login` VALUES (117, 3, 'gregorygv', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'icgregory@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (119, 3, 'barrybj', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'barry@muic.ac.th', '', '');
INSERT INTO `user_login` VALUES (121, 3, 'phillips', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'frbjp@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (124, 3, 'veera23', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'icveera@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (129, 3, 'poraminpb', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'icporamin@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (153, 3, 'sckcy', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'sckcy@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (154, 3, 'krukay', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'krukay@yahoo.com', '', '');
INSERT INTO `user_login` VALUES (155, 3, 'korakotm', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'korakottuk@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (156, 3, 'anusorn', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'sornpohm@ksc.th.com', '', '');
INSERT INTO `user_login` VALUES (157, 2, '0673', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'chayanon_meme@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (158, 2, 'kanoon', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'yubol@muic.ac.th', '', '');
INSERT INTO `user_login` VALUES (159, 3, 'mikemj', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'mljohns27@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (167, 3, 'junya005', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'junya005@hotmail.com', '', '');
INSERT INTO `user_login` VALUES (172, 3, 'peejay', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'peejaygarcia@yahoo.com', '', '');
INSERT INTO `user_login` VALUES (177, 3, 'vacharobon', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'scvtk@mahidol.ac.th', '', '');
INSERT INTO `user_login` VALUES (181, 3, 'somchatsv', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'somchat@muic.ac.th', '', '');
INSERT INTO `user_login` VALUES (182, 3, 'saowaluck', '81dc9bdb52d04dc20036dbd8313ed055', 'ACTIVE', 1, '0000-00-00 00:00:00', 2, 'jj-promprasert@yahoo.com', '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `using_log`
-- 

CREATE TABLE `using_log` (
  `id` int(11) NOT NULL auto_increment,
  `user_login_id` int(11) default NULL,
  `controller_id` varchar(255) NOT NULL,
  `action_id` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3450 ;

-- 
-- Dumping data for table `using_log`
-- 

INSERT INTO `using_log` VALUES (5, 1, 'Solution', '', '2013-06-10 01:57:35');
INSERT INTO `using_log` VALUES (6, 1, 'Report', '', '2013-06-10 01:57:39');
INSERT INTO `using_log` VALUES (7, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-10 01:57:53');
INSERT INTO `using_log` VALUES (8, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 01:57:55');
INSERT INTO `using_log` VALUES (9, 1, 'RequestService', '', '2013-06-10 02:00:38');
INSERT INTO `using_log` VALUES (10, 1, 'RequestService', 'Request', '2013-06-10 02:00:40');
INSERT INTO `using_log` VALUES (11, 1, 'RequestService', '', '2013-06-10 02:00:57');
INSERT INTO `using_log` VALUES (12, 1, 'requestService', 'update', '2013-06-10 02:01:00');
INSERT INTO `using_log` VALUES (13, 1, 'RequestBorrow', '', '2013-06-10 02:02:09');
INSERT INTO `using_log` VALUES (14, 1, 'RequestBorrow', 'Request', '2013-06-10 02:02:12');
INSERT INTO `using_log` VALUES (15, 1, 'RequestBorrow', '', '2013-06-10 02:02:17');
INSERT INTO `using_log` VALUES (16, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:19');
INSERT INTO `using_log` VALUES (17, 1, 'RequestBorrow', '', '2013-06-10 02:02:24');
INSERT INTO `using_log` VALUES (18, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:25');
INSERT INTO `using_log` VALUES (19, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:27');
INSERT INTO `using_log` VALUES (20, 1, 'RequestBorrow', 'Index', '2013-06-10 02:02:29');
INSERT INTO `using_log` VALUES (21, 1, 'RequestBorrow', 'Request', '2013-06-10 02:02:41');
INSERT INTO `using_log` VALUES (22, 1, 'RequestBorrow', 'View', '2013-06-10 02:02:56');
INSERT INTO `using_log` VALUES (23, 1, 'RequestBorrow', 'Update', '2013-06-10 02:02:59');
INSERT INTO `using_log` VALUES (24, 1, 'Report', '', '2013-06-10 02:05:27');
INSERT INTO `using_log` VALUES (25, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:05:29');
INSERT INTO `using_log` VALUES (26, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:05:42');
INSERT INTO `using_log` VALUES (27, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:14');
INSERT INTO `using_log` VALUES (28, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:19');
INSERT INTO `using_log` VALUES (29, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:30');
INSERT INTO `using_log` VALUES (30, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 02:06:35');
INSERT INTO `using_log` VALUES (31, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-10 02:06:39');
INSERT INTO `using_log` VALUES (32, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-10 02:07:13');
INSERT INTO `using_log` VALUES (33, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-10 02:07:18');
INSERT INTO `using_log` VALUES (34, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-10 02:07:20');
INSERT INTO `using_log` VALUES (35, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-10 02:07:22');
INSERT INTO `using_log` VALUES (36, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:24');
INSERT INTO `using_log` VALUES (37, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-10 02:07:27');
INSERT INTO `using_log` VALUES (38, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:28');
INSERT INTO `using_log` VALUES (39, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:33');
INSERT INTO `using_log` VALUES (40, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-10 02:07:37');
INSERT INTO `using_log` VALUES (41, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-10 02:08:42');
INSERT INTO `using_log` VALUES (42, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-10 02:08:44');
INSERT INTO `using_log` VALUES (43, 1, 'RequestBooking', '', '2013-06-10 02:16:45');
INSERT INTO `using_log` VALUES (44, 1, 'RequestBooking', '', '2013-06-10 02:16:45');
INSERT INTO `using_log` VALUES (45, 1, 'RequestBooking', 'Request', '2013-06-10 02:16:47');
INSERT INTO `using_log` VALUES (46, 1, 'RequestBooking', 'View', '2013-06-10 02:16:57');
INSERT INTO `using_log` VALUES (47, 1, 'RequestBooking', 'update', '2013-06-10 02:23:02');
INSERT INTO `using_log` VALUES (48, 1, 'RequestBorrow', '', '2013-06-10 02:23:03');
INSERT INTO `using_log` VALUES (49, 1, 'RequestBorrow', 'CheckStatus', '2013-06-10 02:23:05');
INSERT INTO `using_log` VALUES (50, 1, 'RequestBorrow', '', '2013-06-10 02:23:07');
INSERT INTO `using_log` VALUES (51, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:09');
INSERT INTO `using_log` VALUES (52, 1, 'RequestBorrow', 'Request', '2013-06-10 02:23:11');
INSERT INTO `using_log` VALUES (53, 1, 'RequestBorrow', '', '2013-06-10 02:23:28');
INSERT INTO `using_log` VALUES (54, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:31');
INSERT INTO `using_log` VALUES (55, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:33');
INSERT INTO `using_log` VALUES (56, 1, 'RequestBorrow', '', '2013-06-10 02:23:34');
INSERT INTO `using_log` VALUES (57, 1, 'RequestBorrow', 'Index', '2013-06-10 02:23:36');
INSERT INTO `using_log` VALUES (58, 1, 'RequestBorrow', '', '2013-06-10 02:24:54');
INSERT INTO `using_log` VALUES (59, 1, 'RequestBorrow', 'Index', '2013-06-10 02:24:56');
INSERT INTO `using_log` VALUES (60, 1, 'requestBorrow', 'update', '2013-06-10 02:25:16');
INSERT INTO `using_log` VALUES (61, 1, 'requestBorrow', 'update', '2013-06-10 02:25:26');
INSERT INTO `using_log` VALUES (62, 1, 'requestBorrow', 'update', '2013-06-10 02:25:31');
INSERT INTO `using_log` VALUES (63, 1, 'requestBorrow', 'update', '2013-06-10 02:25:40');
INSERT INTO `using_log` VALUES (64, 1, 'RequestBorrow', '', '2013-06-10 02:25:50');
INSERT INTO `using_log` VALUES (65, 1, 'RequestBorrow', 'Index', '2013-06-10 02:25:52');
INSERT INTO `using_log` VALUES (66, 1, 'requestBorrow', 'update', '2013-06-10 02:25:54');
INSERT INTO `using_log` VALUES (67, 1, 'RequestBorrow', 'Request', '2013-06-10 02:25:57');
INSERT INTO `using_log` VALUES (68, NULL, 'management', 'login', '2013-06-10 14:40:58');
INSERT INTO `using_log` VALUES (69, NULL, '', '', '2013-06-10 14:40:59');
INSERT INTO `using_log` VALUES (70, 1, 'RequestBooking', '', '2013-06-10 14:41:01');
INSERT INTO `using_log` VALUES (71, 1, 'Report', '', '2013-06-10 14:41:04');
INSERT INTO `using_log` VALUES (72, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:41:06');
INSERT INTO `using_log` VALUES (73, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:41:20');
INSERT INTO `using_log` VALUES (74, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:43:21');
INSERT INTO `using_log` VALUES (75, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:43:37');
INSERT INTO `using_log` VALUES (76, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:43:48');
INSERT INTO `using_log` VALUES (77, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:11');
INSERT INTO `using_log` VALUES (78, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:20');
INSERT INTO `using_log` VALUES (79, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:41');
INSERT INTO `using_log` VALUES (80, 1, 'Report', 'UserStatisticReport', '2013-06-10 14:44:54');
INSERT INTO `using_log` VALUES (81, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:00:40');
INSERT INTO `using_log` VALUES (82, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:01:02');
INSERT INTO `using_log` VALUES (83, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:01:24');
INSERT INTO `using_log` VALUES (84, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:01:59');
INSERT INTO `using_log` VALUES (85, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:02:12');
INSERT INTO `using_log` VALUES (86, 1, 'Report', 'UserStatisticReport', '2013-06-10 15:04:25');
INSERT INTO `using_log` VALUES (87, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:13:35');
INSERT INTO `using_log` VALUES (88, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:19:06');
INSERT INTO `using_log` VALUES (89, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:20:56');
INSERT INTO `using_log` VALUES (90, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:27:07');
INSERT INTO `using_log` VALUES (91, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:27:24');
INSERT INTO `using_log` VALUES (92, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:27:33');
INSERT INTO `using_log` VALUES (93, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:29:15');
INSERT INTO `using_log` VALUES (94, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:29:22');
INSERT INTO `using_log` VALUES (95, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:29:37');
INSERT INTO `using_log` VALUES (96, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:30:17');
INSERT INTO `using_log` VALUES (97, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:31:32');
INSERT INTO `using_log` VALUES (98, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:09');
INSERT INTO `using_log` VALUES (99, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:36');
INSERT INTO `using_log` VALUES (100, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:40');
INSERT INTO `using_log` VALUES (101, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:32:44');
INSERT INTO `using_log` VALUES (102, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:33:33');
INSERT INTO `using_log` VALUES (103, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:01');
INSERT INTO `using_log` VALUES (104, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-10 16:35:03');
INSERT INTO `using_log` VALUES (105, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:05');
INSERT INTO `using_log` VALUES (106, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:28');
INSERT INTO `using_log` VALUES (107, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:32');
INSERT INTO `using_log` VALUES (108, 1, 'Solution', '', '2013-06-10 16:35:39');
INSERT INTO `using_log` VALUES (109, 1, 'RequestService', '', '2013-06-10 16:35:41');
INSERT INTO `using_log` VALUES (110, 1, 'RequestBorrow', '', '2013-06-10 16:35:43');
INSERT INTO `using_log` VALUES (111, 1, 'RequestBooking', '', '2013-06-10 16:35:43');
INSERT INTO `using_log` VALUES (112, 1, 'Report', '', '2013-06-10 16:35:45');
INSERT INTO `using_log` VALUES (113, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:35:46');
INSERT INTO `using_log` VALUES (114, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:53:16');
INSERT INTO `using_log` VALUES (115, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:53:22');
INSERT INTO `using_log` VALUES (116, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:59:27');
INSERT INTO `using_log` VALUES (117, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:59:31');
INSERT INTO `using_log` VALUES (118, 1, 'Report', 'UserStatisticReport', '2013-06-10 16:59:52');
INSERT INTO `using_log` VALUES (119, NULL, 'management', 'login', '2013-06-10 17:34:46');
INSERT INTO `using_log` VALUES (120, 1, 'RequestBooking', '', '2013-06-10 17:34:48');
INSERT INTO `using_log` VALUES (121, 1, 'Report', '', '2013-06-10 17:34:50');
INSERT INTO `using_log` VALUES (122, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:34:52');
INSERT INTO `using_log` VALUES (123, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:39:20');
INSERT INTO `using_log` VALUES (124, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:39:25');
INSERT INTO `using_log` VALUES (125, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:39:43');
INSERT INTO `using_log` VALUES (126, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:40:30');
INSERT INTO `using_log` VALUES (127, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:40:40');
INSERT INTO `using_log` VALUES (128, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:41:00');
INSERT INTO `using_log` VALUES (129, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:41:09');
INSERT INTO `using_log` VALUES (130, NULL, 'management', 'login', '2013-06-10 17:41:58');
INSERT INTO `using_log` VALUES (131, 1, 'RequestBooking', '', '2013-06-10 17:42:00');
INSERT INTO `using_log` VALUES (132, 1, 'Report', '', '2013-06-10 17:42:04');
INSERT INTO `using_log` VALUES (133, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:42:05');
INSERT INTO `using_log` VALUES (134, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:42:09');
INSERT INTO `using_log` VALUES (135, 1, 'Report', 'UserStatisticReport', '2013-06-10 17:42:56');
INSERT INTO `using_log` VALUES (136, NULL, 'management', 'login', '2013-06-10 23:49:36');
INSERT INTO `using_log` VALUES (137, 1, 'RequestBooking', '', '2013-06-10 23:49:37');
INSERT INTO `using_log` VALUES (138, 1, 'RequestBooking', 'Request', '2013-06-10 23:49:39');
INSERT INTO `using_log` VALUES (139, 1, 'RequestBooking', 'Request', '2013-06-11 00:25:03');
INSERT INTO `using_log` VALUES (140, 1, 'RequestBooking', 'Request', '2013-06-11 00:25:54');
INSERT INTO `using_log` VALUES (141, 1, 'RequestBooking', 'Request', '2013-06-11 00:26:54');
INSERT INTO `using_log` VALUES (142, 1, 'RequestBooking', 'Request', '2013-06-11 00:28:07');
INSERT INTO `using_log` VALUES (143, 1, 'RequestBooking', 'Request', '2013-06-11 00:29:41');
INSERT INTO `using_log` VALUES (144, 1, 'RequestBooking', 'Request', '2013-06-11 00:30:52');
INSERT INTO `using_log` VALUES (145, 1, 'RequestBooking', 'Request', '2013-06-11 00:32:03');
INSERT INTO `using_log` VALUES (146, 1, 'RequestBooking', 'Request', '2013-06-11 00:36:28');
INSERT INTO `using_log` VALUES (147, 1, 'RequestBooking', 'Request', '2013-06-11 00:39:27');
INSERT INTO `using_log` VALUES (148, 1, 'RequestBooking', 'Request', '2013-06-11 00:41:03');
INSERT INTO `using_log` VALUES (149, 1, 'RequestBooking', 'Request', '2013-06-11 00:41:13');
INSERT INTO `using_log` VALUES (150, 1, 'RequestBooking', 'Request', '2013-06-11 00:52:51');
INSERT INTO `using_log` VALUES (151, 1, 'RequestBooking', 'Request', '2013-06-11 00:53:03');
INSERT INTO `using_log` VALUES (152, 1, 'RequestBooking', 'Request', '2013-06-11 00:54:56');
INSERT INTO `using_log` VALUES (153, 1, 'RequestBooking', 'Request', '2013-06-11 00:55:22');
INSERT INTO `using_log` VALUES (154, 1, 'RequestBooking', 'Request', '2013-06-11 00:56:23');
INSERT INTO `using_log` VALUES (155, 1, 'RequestBooking', 'Request', '2013-06-11 00:56:37');
INSERT INTO `using_log` VALUES (156, 1, 'RequestBooking', 'Request', '2013-06-11 00:56:59');
INSERT INTO `using_log` VALUES (157, 1, 'RequestBooking', 'Request', '2013-06-11 01:00:02');
INSERT INTO `using_log` VALUES (158, 1, 'RequestBooking', 'Request', '2013-06-11 01:00:16');
INSERT INTO `using_log` VALUES (159, 1, 'RequestBooking', 'Request', '2013-06-11 01:00:26');
INSERT INTO `using_log` VALUES (160, 1, 'RequestBooking', 'Request', '2013-06-11 01:01:26');
INSERT INTO `using_log` VALUES (161, 1, 'RequestBooking', 'Request', '2013-06-11 01:01:50');
INSERT INTO `using_log` VALUES (162, 1, 'RequestBooking', 'Request', '2013-06-11 01:04:04');
INSERT INTO `using_log` VALUES (163, 1, 'RequestBooking', 'Request', '2013-06-11 01:04:52');
INSERT INTO `using_log` VALUES (164, 1, 'RequestBooking', 'Request', '2013-06-11 01:06:24');
INSERT INTO `using_log` VALUES (165, 1, 'RequestBooking', 'Request', '2013-06-11 01:06:46');
INSERT INTO `using_log` VALUES (166, 1, 'RequestBooking', 'Request', '2013-06-11 01:09:23');
INSERT INTO `using_log` VALUES (167, 1, 'RequestBooking', 'Request', '2013-06-11 01:10:41');
INSERT INTO `using_log` VALUES (168, 1, 'RequestBooking', 'Request', '2013-06-11 01:13:12');
INSERT INTO `using_log` VALUES (169, 1, 'RequestBooking', 'Request', '2013-06-11 01:13:33');
INSERT INTO `using_log` VALUES (170, 1, 'RequestBooking', 'Request', '2013-06-11 01:15:17');
INSERT INTO `using_log` VALUES (171, 1, 'RequestBooking', 'Request', '2013-06-11 01:16:35');
INSERT INTO `using_log` VALUES (172, 1, 'RequestBooking', 'Request', '2013-06-11 01:16:53');
INSERT INTO `using_log` VALUES (173, 1, 'RequestBooking', 'Request', '2013-06-11 01:20:12');
INSERT INTO `using_log` VALUES (174, 1, 'RequestBooking', 'Request', '2013-06-11 01:20:36');
INSERT INTO `using_log` VALUES (175, 1, 'RequestBooking', 'Request', '2013-06-11 01:25:04');
INSERT INTO `using_log` VALUES (176, 1, 'RequestBooking', 'Request', '2013-06-11 01:25:40');
INSERT INTO `using_log` VALUES (177, 1, 'RequestBooking', 'View', '2013-06-11 01:26:13');
INSERT INTO `using_log` VALUES (178, 1, 'RequestBooking', 'View', '2013-06-11 01:27:20');
INSERT INTO `using_log` VALUES (179, 1, 'RequestBooking', 'Request', '2013-06-11 01:27:21');
INSERT INTO `using_log` VALUES (180, 1, 'RequestBooking', 'Request', '2013-06-11 01:27:22');
INSERT INTO `using_log` VALUES (181, 1, 'RequestBooking', 'Request', '2013-06-11 01:27:52');
INSERT INTO `using_log` VALUES (182, 1, 'RequestBooking', 'View', '2013-06-11 01:28:16');
INSERT INTO `using_log` VALUES (183, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:04');
INSERT INTO `using_log` VALUES (184, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:05');
INSERT INTO `using_log` VALUES (185, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:17');
INSERT INTO `using_log` VALUES (186, 1, 'RequestBooking', 'View', '2013-06-11 01:29:39');
INSERT INTO `using_log` VALUES (187, 1, 'RequestBooking', 'Request', '2013-06-11 01:29:47');
INSERT INTO `using_log` VALUES (188, 1, 'RequestBooking', 'View', '2013-06-11 01:30:04');
INSERT INTO `using_log` VALUES (189, 1, 'RequestService', '', '2013-06-11 01:30:30');
INSERT INTO `using_log` VALUES (190, 1, 'RequestService', 'Request', '2013-06-11 01:30:31');
INSERT INTO `using_log` VALUES (191, 1, 'Report', '', '2013-06-11 01:32:33');
INSERT INTO `using_log` VALUES (192, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:32:34');
INSERT INTO `using_log` VALUES (193, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:32:45');
INSERT INTO `using_log` VALUES (194, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:33:30');
INSERT INTO `using_log` VALUES (195, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:33:34');
INSERT INTO `using_log` VALUES (196, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:33:36');
INSERT INTO `using_log` VALUES (197, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:33:38');
INSERT INTO `using_log` VALUES (198, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:33:39');
INSERT INTO `using_log` VALUES (199, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:33:41');
INSERT INTO `using_log` VALUES (200, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:33:46');
INSERT INTO `using_log` VALUES (201, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:33:54');
INSERT INTO `using_log` VALUES (202, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:33:56');
INSERT INTO `using_log` VALUES (203, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:34:05');
INSERT INTO `using_log` VALUES (204, 1, 'RequestService', '', '2013-06-11 01:36:26');
INSERT INTO `using_log` VALUES (205, 1, 'RequestService', 'Request', '2013-06-11 01:36:27');
INSERT INTO `using_log` VALUES (206, 1, 'RequestService', 'Request', '2013-06-11 01:37:33');
INSERT INTO `using_log` VALUES (207, 1, 'RequestService', '', '2013-06-11 01:37:46');
INSERT INTO `using_log` VALUES (208, 1, 'requestService', 'view', '2013-06-11 01:37:53');
INSERT INTO `using_log` VALUES (209, 1, 'requestService', 'view', '2013-06-11 01:37:59');
INSERT INTO `using_log` VALUES (210, 1, 'RequestService', 'Request', '2013-06-11 01:38:01');
INSERT INTO `using_log` VALUES (211, 1, 'RequestService', '', '2013-06-11 01:38:12');
INSERT INTO `using_log` VALUES (212, 1, 'requestService', 'view', '2013-06-11 01:38:15');
INSERT INTO `using_log` VALUES (213, 1, 'Report', '', '2013-06-11 01:38:20');
INSERT INTO `using_log` VALUES (214, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:38:22');
INSERT INTO `using_log` VALUES (215, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:38:43');
INSERT INTO `using_log` VALUES (216, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:39:12');
INSERT INTO `using_log` VALUES (217, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:39:21');
INSERT INTO `using_log` VALUES (218, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:39:31');
INSERT INTO `using_log` VALUES (219, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:39:43');
INSERT INTO `using_log` VALUES (220, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:39:53');
INSERT INTO `using_log` VALUES (221, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:39:55');
INSERT INTO `using_log` VALUES (222, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:39:57');
INSERT INTO `using_log` VALUES (223, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:39:58');
INSERT INTO `using_log` VALUES (224, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:40:21');
INSERT INTO `using_log` VALUES (225, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:44:30');
INSERT INTO `using_log` VALUES (226, 1, 'RequestBooking', '', '2013-06-11 01:44:50');
INSERT INTO `using_log` VALUES (227, 1, 'RequestBooking', 'Request', '2013-06-11 01:44:53');
INSERT INTO `using_log` VALUES (228, 1, 'RequestBooking', 'View', '2013-06-11 01:45:03');
INSERT INTO `using_log` VALUES (229, 1, 'Report', '', '2013-06-11 01:45:04');
INSERT INTO `using_log` VALUES (230, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:45:06');
INSERT INTO `using_log` VALUES (231, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:45:22');
INSERT INTO `using_log` VALUES (232, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:46:19');
INSERT INTO `using_log` VALUES (233, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:46:56');
INSERT INTO `using_log` VALUES (234, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:47:01');
INSERT INTO `using_log` VALUES (235, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:48:00');
INSERT INTO `using_log` VALUES (236, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:48:17');
INSERT INTO `using_log` VALUES (237, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:48:20');
INSERT INTO `using_log` VALUES (238, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:49:11');
INSERT INTO `using_log` VALUES (239, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:49:15');
INSERT INTO `using_log` VALUES (240, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:50:00');
INSERT INTO `using_log` VALUES (241, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:50:10');
INSERT INTO `using_log` VALUES (242, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:50:53');
INSERT INTO `using_log` VALUES (243, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:51:01');
INSERT INTO `using_log` VALUES (244, 1, 'Report', 'UserStatisticReport', '2013-06-11 01:51:04');
INSERT INTO `using_log` VALUES (245, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:51:05');
INSERT INTO `using_log` VALUES (246, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-06-11 01:51:50');
INSERT INTO `using_log` VALUES (247, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-06-11 01:51:58');
INSERT INTO `using_log` VALUES (248, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-11 01:52:00');
INSERT INTO `using_log` VALUES (249, 1, 'Report', 'RequestBorrowStatisticReport', '2013-06-11 01:52:03');
INSERT INTO `using_log` VALUES (250, 1, 'Report', 'RequestBorrowUsingStatisticReport', '2013-06-11 01:52:05');
INSERT INTO `using_log` VALUES (251, 1, 'Report', 'RequestServiceStatisticReport', '2013-06-11 01:52:10');
INSERT INTO `using_log` VALUES (252, 1, 'Report', 'RequestServiceUsingStatisticReport', '2013-06-11 01:52:12');
INSERT INTO `using_log` VALUES (253, 1, '', '', '2013-06-11 01:52:17');
INSERT INTO `using_log` VALUES (254, NULL, 'management', 'login', '2013-06-11 10:55:12');
INSERT INTO `using_log` VALUES (255, 1, 'RequestBooking', '', '2013-06-11 10:59:07');
INSERT INTO `using_log` VALUES (256, 1, 'RequestBooking', '', '2013-06-11 10:59:43');
INSERT INTO `using_log` VALUES (257, 1, 'RequestBooking', 'Request', '2013-06-11 10:59:44');
INSERT INTO `using_log` VALUES (258, 1, 'RequestBooking', 'View', '2013-06-11 11:00:04');
INSERT INTO `using_log` VALUES (259, 1, 'RequestBooking', 'Request', '2013-06-11 11:00:10');
INSERT INTO `using_log` VALUES (260, 1, 'RequestBooking', 'Request', '2013-06-11 11:00:34');
INSERT INTO `using_log` VALUES (261, 1, 'RequestBooking', 'Request', '2013-06-11 11:05:27');
INSERT INTO `using_log` VALUES (262, 1, 'RequestBooking', 'View', '2013-06-11 11:05:52');
INSERT INTO `using_log` VALUES (263, 1, 'RequestBooking', 'Request', '2013-06-11 11:05:53');
INSERT INTO `using_log` VALUES (264, 1, 'RequestBooking', 'Request', '2013-06-11 11:18:13');
INSERT INTO `using_log` VALUES (265, 1, 'RequestBooking', 'Request', '2013-06-11 11:18:53');
INSERT INTO `using_log` VALUES (266, 1, 'RequestBooking', 'Request', '2013-06-11 11:20:23');
INSERT INTO `using_log` VALUES (267, 1, 'RequestBooking', 'Request', '2013-06-11 11:20:36');
INSERT INTO `using_log` VALUES (268, 1, 'RequestBooking', 'Request', '2013-06-11 11:20:47');
INSERT INTO `using_log` VALUES (269, 1, 'RequestBooking', 'Request', '2013-06-11 11:29:45');
INSERT INTO `using_log` VALUES (270, 1, 'RequestBooking', 'Request', '2013-06-11 11:29:49');
INSERT INTO `using_log` VALUES (271, 1, 'RequestBooking', 'Request', '2013-06-11 11:30:10');
INSERT INTO `using_log` VALUES (272, 1, 'RequestBooking', 'Request', '2013-06-11 11:30:46');
INSERT INTO `using_log` VALUES (273, 1, 'RequestBooking', 'Request', '2013-06-11 11:31:09');
INSERT INTO `using_log` VALUES (274, 1, 'RequestBooking', 'Request', '2013-06-11 11:39:06');
INSERT INTO `using_log` VALUES (275, 1, 'RequestBooking', 'Request', '2013-06-11 11:39:32');
INSERT INTO `using_log` VALUES (276, 1, 'RequestBooking', 'Request', '2013-06-11 11:40:19');
INSERT INTO `using_log` VALUES (277, 1, 'RequestBooking', 'Request', '2013-06-11 11:41:40');
INSERT INTO `using_log` VALUES (278, 1, 'RequestBooking', 'Request', '2013-06-11 11:43:18');
INSERT INTO `using_log` VALUES (279, 1, 'RequestBooking', 'Request', '2013-06-11 11:43:24');
INSERT INTO `using_log` VALUES (280, 1, 'RequestBooking', 'Request', '2013-06-11 11:53:32');
INSERT INTO `using_log` VALUES (281, 1, 'RequestBooking', 'Request', '2013-06-11 11:55:15');
INSERT INTO `using_log` VALUES (282, 1, 'RequestBooking', 'Request', '2013-06-11 11:57:30');
INSERT INTO `using_log` VALUES (283, 1, 'RequestBooking', 'Request', '2013-06-11 11:58:02');
INSERT INTO `using_log` VALUES (284, 1, 'RequestBooking', 'Request', '2013-06-11 12:00:51');
INSERT INTO `using_log` VALUES (285, 1, 'RequestBorrow', '', '2013-06-11 12:42:57');
INSERT INTO `using_log` VALUES (286, 1, 'RequestBorrow', 'Request', '2013-06-11 12:42:59');
INSERT INTO `using_log` VALUES (287, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:47');
INSERT INTO `using_log` VALUES (288, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:51');
INSERT INTO `using_log` VALUES (289, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:52');
INSERT INTO `using_log` VALUES (290, 1, 'RequestBorrow', 'Request', '2013-06-11 12:50:56');
INSERT INTO `using_log` VALUES (291, 1, 'RequestBorrow', 'Request', '2013-06-11 12:51:01');
INSERT INTO `using_log` VALUES (292, 1, 'RequestBorrow', 'Request', '2013-06-11 12:52:08');
INSERT INTO `using_log` VALUES (293, 1, 'RequestBorrow', 'Request', '2013-06-11 12:52:09');
INSERT INTO `using_log` VALUES (294, 1, 'RequestBorrow', 'Request', '2013-06-11 12:52:53');
INSERT INTO `using_log` VALUES (295, 1, 'RequestBorrow', 'Request', '2013-06-11 12:53:48');
INSERT INTO `using_log` VALUES (296, 1, 'RequestBorrow', 'View', '2013-06-11 12:54:32');
INSERT INTO `using_log` VALUES (297, 1, 'RequestService', '', '2013-06-11 12:55:19');
INSERT INTO `using_log` VALUES (298, 1, 'RequestService', 'Request', '2013-06-11 12:55:21');
INSERT INTO `using_log` VALUES (299, NULL, 'management', 'login', '2013-06-13 10:48:09');
INSERT INTO `using_log` VALUES (300, 1, 'RequestBooking', '', '2013-06-13 10:48:26');
INSERT INTO `using_log` VALUES (301, 1, 'Solution', '', '2013-06-13 10:48:30');
INSERT INTO `using_log` VALUES (302, 1, 'RequestService', '', '2013-06-13 10:48:31');
INSERT INTO `using_log` VALUES (303, 1, 'RequestService', 'Request', '2013-06-13 10:48:33');
INSERT INTO `using_log` VALUES (304, 1, 'Solution', '', '2013-06-13 10:48:37');
INSERT INTO `using_log` VALUES (305, 1, 'RequestService', '', '2013-06-13 10:48:42');
INSERT INTO `using_log` VALUES (306, 1, 'RequestBorrow', '', '2013-06-13 10:48:45');
INSERT INTO `using_log` VALUES (307, 1, 'RequestBorrow', 'Request', '2013-06-13 10:48:46');
INSERT INTO `using_log` VALUES (308, 1, 'RequestService', '', '2013-06-13 10:48:51');
INSERT INTO `using_log` VALUES (309, 1, 'RequestService', 'Request', '2013-06-13 10:48:53');
INSERT INTO `using_log` VALUES (310, 1, 'RequestService', '', '2013-06-13 10:50:03');
INSERT INTO `using_log` VALUES (311, 1, 'requestService', 'view', '2013-06-13 10:50:05');
INSERT INTO `using_log` VALUES (312, 1, 'RequestService', '', '2013-06-13 10:50:11');
INSERT INTO `using_log` VALUES (313, 1, 'RequestService', 'Request', '2013-06-13 10:50:12');
INSERT INTO `using_log` VALUES (314, 1, 'RequestService', '', '2013-06-13 11:03:18');
INSERT INTO `using_log` VALUES (315, 1, 'RequestService', 'Request', '2013-06-13 11:04:04');
INSERT INTO `using_log` VALUES (316, 1, 'RequestService', 'Request', '2013-06-13 11:04:05');
INSERT INTO `using_log` VALUES (317, 1, 'RequestService', 'View', '2013-06-13 11:04:17');
INSERT INTO `using_log` VALUES (318, 1, 'RequestService', '', '2013-06-13 11:04:23');
INSERT INTO `using_log` VALUES (319, 1, 'RequestService', 'Request', '2013-06-13 11:09:05');
INSERT INTO `using_log` VALUES (320, 1, 'RequestService', 'Request', '2013-06-13 11:14:02');
INSERT INTO `using_log` VALUES (321, 1, 'RequestService', 'View', '2013-06-13 11:14:46');
INSERT INTO `using_log` VALUES (322, NULL, 'management', 'login', '2013-06-13 20:35:52');
INSERT INTO `using_log` VALUES (323, 1, 'RequestBooking', '', '2013-06-13 20:35:55');
INSERT INTO `using_log` VALUES (324, 1, 'RequestBorrow', '', '2013-06-13 20:36:06');
INSERT INTO `using_log` VALUES (325, 1, 'RequestBorrow', 'Request', '2013-06-13 20:36:07');
INSERT INTO `using_log` VALUES (326, 1, 'RequestBorrow', 'View', '2013-06-13 20:37:12');
INSERT INTO `using_log` VALUES (327, NULL, 'management', 'login', '2013-06-14 08:32:55');
INSERT INTO `using_log` VALUES (328, NULL, 'management', 'login', '2013-06-15 11:12:00');
INSERT INTO `using_log` VALUES (329, 1, 'RequestBooking', '', '2013-06-15 11:12:03');
INSERT INTO `using_log` VALUES (330, 1, 'RequestBooking', 'Request', '2013-06-15 11:12:05');
INSERT INTO `using_log` VALUES (331, 1, 'RequestBooking', 'Request', '2013-06-15 11:12:33');
INSERT INTO `using_log` VALUES (332, 1, 'RequestBooking', 'Request', '2013-06-15 11:13:08');
INSERT INTO `using_log` VALUES (333, 1, 'RequestBooking', 'Request', '2013-06-15 11:13:30');
INSERT INTO `using_log` VALUES (334, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:15:49');
INSERT INTO `using_log` VALUES (335, 1, 'RequestBorrow', '', '2013-06-15 11:16:02');
INSERT INTO `using_log` VALUES (336, 1, 'requestBorrow', 'view', '2013-06-15 11:16:09');
INSERT INTO `using_log` VALUES (337, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:18:37');
INSERT INTO `using_log` VALUES (338, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:19:31');
INSERT INTO `using_log` VALUES (339, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:26:07');
INSERT INTO `using_log` VALUES (340, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:26:16');
INSERT INTO `using_log` VALUES (341, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:07');
INSERT INTO `using_log` VALUES (342, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:12');
INSERT INTO `using_log` VALUES (343, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:13');
INSERT INTO `using_log` VALUES (344, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:25');
INSERT INTO `using_log` VALUES (345, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:27:33');
INSERT INTO `using_log` VALUES (346, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:28:16');
INSERT INTO `using_log` VALUES (347, NULL, 'management', 'login', '2013-06-15 11:28:26');
INSERT INTO `using_log` VALUES (348, NULL, 'RequestBorrow', 'Approve', '2013-06-15 11:28:27');
INSERT INTO `using_log` VALUES (349, 1, 'RequestBooking', '', '2013-06-15 11:28:32');
INSERT INTO `using_log` VALUES (350, 1, 'RequestBorrow', 'Approve', '2013-06-15 11:28:33');
INSERT INTO `using_log` VALUES (351, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 11:34:05');
INSERT INTO `using_log` VALUES (352, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 11:34:22');
INSERT INTO `using_log` VALUES (353, 1, 'RequestBorrow', '', '2013-06-15 11:34:31');
INSERT INTO `using_log` VALUES (354, 1, 'RequestBorrow', '', '2013-06-15 11:34:33');
INSERT INTO `using_log` VALUES (355, 1, 'RequestBorrow', 'Request', '2013-06-15 12:12:04');
INSERT INTO `using_log` VALUES (356, 1, 'RequestBorrow', 'Request', '2013-06-15 12:13:43');
INSERT INTO `using_log` VALUES (357, 1, 'RequestBorrow', 'Request', '2013-06-15 12:14:08');
INSERT INTO `using_log` VALUES (358, 1, 'RequestBorrow', 'View', '2013-06-15 12:14:32');
INSERT INTO `using_log` VALUES (359, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:15:17');
INSERT INTO `using_log` VALUES (360, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:15:30');
INSERT INTO `using_log` VALUES (361, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:15:50');
INSERT INTO `using_log` VALUES (362, 1, 'RequestBorrow', 'Request', '2013-06-15 12:16:22');
INSERT INTO `using_log` VALUES (363, 1, 'RequestBorrow', 'Request', '2013-06-15 12:22:06');
INSERT INTO `using_log` VALUES (364, 1, 'RequestBorrow', 'Request', '2013-06-15 12:22:24');
INSERT INTO `using_log` VALUES (365, 1, 'RequestBorrow', 'Request', '2013-06-15 12:23:51');
INSERT INTO `using_log` VALUES (366, 1, 'RequestBorrow', 'Request', '2013-06-15 12:25:48');
INSERT INTO `using_log` VALUES (367, 1, 'RequestBorrow', 'Request', '2013-06-15 12:26:47');
INSERT INTO `using_log` VALUES (368, 1, 'RequestBorrow', 'View', '2013-06-15 12:27:19');
INSERT INTO `using_log` VALUES (369, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:27:41');
INSERT INTO `using_log` VALUES (370, 1, 'RequestBorrow', 'View', '2013-06-15 12:28:43');
INSERT INTO `using_log` VALUES (371, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:29:00');
INSERT INTO `using_log` VALUES (372, 1, 'RequestBorrow', 'View', '2013-06-15 12:29:02');
INSERT INTO `using_log` VALUES (373, 1, 'RequestBorrow', 'View', '2013-06-15 12:29:03');
INSERT INTO `using_log` VALUES (374, 1, 'RequestBorrow', 'Request', '2013-06-15 12:31:02');
INSERT INTO `using_log` VALUES (375, 1, 'RequestBorrow', 'View', '2013-06-15 12:31:32');
INSERT INTO `using_log` VALUES (376, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:31:52');
INSERT INTO `using_log` VALUES (377, 1, 'RequestBorrow', 'View', '2013-06-15 12:32:00');
INSERT INTO `using_log` VALUES (378, 1, 'RequestBorrow', 'View', '2013-06-15 12:32:19');
INSERT INTO `using_log` VALUES (379, 1, 'RequestBorrow', 'FinalApprove', '2013-06-15 12:35:24');
INSERT INTO `using_log` VALUES (380, 1, 'RequestBorrow', 'View', '2013-06-15 12:35:27');
INSERT INTO `using_log` VALUES (381, 1, 'RequestBorrow', 'Request', '2013-06-15 12:36:26');
INSERT INTO `using_log` VALUES (382, 1, 'RequestBorrow', 'View', '2013-06-15 12:36:49');
INSERT INTO `using_log` VALUES (383, 1, 'RequestBorrow', 'Approve', '2013-06-15 12:37:03');
INSERT INTO `using_log` VALUES (384, 1, 'RequestBorrow', 'View', '2013-06-15 12:37:05');
INSERT INTO `using_log` VALUES (385, 1, 'RequestBorrow', 'Disapprove', '2013-06-15 12:37:22');
INSERT INTO `using_log` VALUES (386, 1, 'RequestBorrow', 'View', '2013-06-15 12:37:27');
INSERT INTO `using_log` VALUES (387, 1, 'RequestBooking', '', '2013-06-15 12:41:21');
INSERT INTO `using_log` VALUES (388, 1, 'RequestBooking', '', '2013-06-15 12:50:03');
INSERT INTO `using_log` VALUES (389, 1, 'RequestBooking', 'Request', '2013-06-15 12:50:05');
INSERT INTO `using_log` VALUES (390, 1, 'RequestBooking', 'View', '2013-06-15 12:50:23');
INSERT INTO `using_log` VALUES (391, 1, 'RequestBooking', '', '2013-06-15 12:50:31');
INSERT INTO `using_log` VALUES (392, 1, 'RequestBooking', 'Request', '2013-06-15 12:51:17');
INSERT INTO `using_log` VALUES (393, NULL, 'management', 'login', '2013-06-15 12:51:34');
INSERT INTO `using_log` VALUES (394, 1, 'RequestBooking', '', '2013-06-15 12:51:35');
INSERT INTO `using_log` VALUES (395, 1, 'RequestBooking', 'Request', '2013-06-15 12:51:38');
INSERT INTO `using_log` VALUES (396, 1, 'RequestBooking', 'View', '2013-06-15 12:51:47');
INSERT INTO `using_log` VALUES (397, 1, 'RequestBooking', 'Request', '2013-06-15 12:52:25');
INSERT INTO `using_log` VALUES (398, 1, 'RequestBooking', 'Request', '2013-06-15 12:52:51');
INSERT INTO `using_log` VALUES (399, 1, 'RequestBooking', 'View', '2013-06-15 12:53:12');
INSERT INTO `using_log` VALUES (400, 1, 'RequestBooking', '', '2013-06-15 12:53:26');
INSERT INTO `using_log` VALUES (401, 1, 'RequestBooking', '', '2013-06-15 12:53:29');
INSERT INTO `using_log` VALUES (402, 1, 'RequestBooking', '', '2013-06-15 12:54:34');
INSERT INTO `using_log` VALUES (403, 1, 'RequestBooking', 'Index', '2013-06-15 12:54:37');
INSERT INTO `using_log` VALUES (404, 1, 'RequestBooking', 'Index', '2013-06-15 12:54:39');
INSERT INTO `using_log` VALUES (405, 1, 'RequestBooking', 'Index', '2013-06-15 12:54:42');
INSERT INTO `using_log` VALUES (406, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:26');
INSERT INTO `using_log` VALUES (407, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:33');
INSERT INTO `using_log` VALUES (408, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:37');
INSERT INTO `using_log` VALUES (409, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:44');
INSERT INTO `using_log` VALUES (410, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:45');
INSERT INTO `using_log` VALUES (411, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:47');
INSERT INTO `using_log` VALUES (412, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:48');
INSERT INTO `using_log` VALUES (413, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:49');
INSERT INTO `using_log` VALUES (414, 1, 'RequestBooking', 'Index', '2013-06-15 12:55:54');
INSERT INTO `using_log` VALUES (415, 1, 'RequestBooking', '', '2013-06-15 12:56:19');
INSERT INTO `using_log` VALUES (416, 1, 'RequestBooking', 'CheckStatus', '2013-06-15 12:56:21');
INSERT INTO `using_log` VALUES (417, 1, 'RequestBooking', '', '2013-06-15 12:56:22');
INSERT INTO `using_log` VALUES (418, 1, 'RequestBooking', 'Request', '2013-06-15 12:56:26');
INSERT INTO `using_log` VALUES (419, 1, 'RequestBooking', '', '2013-06-15 12:56:27');
INSERT INTO `using_log` VALUES (420, 1, 'RequestBooking', 'Index', '2013-06-15 12:56:30');
INSERT INTO `using_log` VALUES (421, 1, 'requestBooking', 'view', '2013-06-15 12:56:32');
INSERT INTO `using_log` VALUES (422, 1, 'RequestBooking', '', '2013-06-15 12:56:34');
INSERT INTO `using_log` VALUES (423, 1, 'RequestBooking', 'Index', '2013-06-15 12:56:36');
INSERT INTO `using_log` VALUES (424, 1, 'RequestBooking', 'Index', '2013-06-15 12:56:38');
INSERT INTO `using_log` VALUES (425, 1, 'requestBooking', 'view', '2013-06-15 12:56:47');
INSERT INTO `using_log` VALUES (426, 1, 'RequestBooking', 'update', '2013-06-15 12:56:49');
INSERT INTO `using_log` VALUES (427, 1, 'RequestBooking', 'update', '2013-06-15 12:56:56');
INSERT INTO `using_log` VALUES (428, 1, 'RequestBooking', 'update', '2013-06-15 12:57:04');
INSERT INTO `using_log` VALUES (429, 1, 'RequestBooking', '', '2013-06-15 12:57:52');
INSERT INTO `using_log` VALUES (430, 1, 'RequestBooking', 'Index', '2013-06-15 12:57:55');
INSERT INTO `using_log` VALUES (431, 1, 'RequestBooking', 'Index', '2013-06-15 12:57:57');
INSERT INTO `using_log` VALUES (432, 1, 'RequestBooking', 'Index', '2013-06-15 12:58:01');
INSERT INTO `using_log` VALUES (433, 1, 'RequestBooking', 'Index', '2013-06-15 12:58:03');
INSERT INTO `using_log` VALUES (434, 1, 'RequestBooking', 'Index', '2013-06-15 12:58:05');
INSERT INTO `using_log` VALUES (435, 1, 'RequestBooking', '', '2013-06-15 12:58:53');
INSERT INTO `using_log` VALUES (436, 1, 'RequestBooking', '', '2013-06-15 12:59:15');
INSERT INTO `using_log` VALUES (437, 1, 'RequestBooking', '', '2013-06-15 12:59:29');
INSERT INTO `using_log` VALUES (438, 1, 'RequestBooking', '', '2013-06-15 12:59:42');
INSERT INTO `using_log` VALUES (439, 1, 'RequestBooking', '', '2013-06-15 13:00:00');
INSERT INTO `using_log` VALUES (440, 1, 'RequestBooking', 'Index', '2013-06-15 13:00:04');
INSERT INTO `using_log` VALUES (441, 1, 'RequestBooking', '', '2013-06-15 13:00:13');
INSERT INTO `using_log` VALUES (442, 1, 'RequestBooking', '', '2013-06-15 13:01:07');
INSERT INTO `using_log` VALUES (443, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:11');
INSERT INTO `using_log` VALUES (444, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:13');
INSERT INTO `using_log` VALUES (445, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:16');
INSERT INTO `using_log` VALUES (446, 1, 'RequestBooking', '', '2013-06-15 13:01:29');
INSERT INTO `using_log` VALUES (447, 1, 'RequestBooking', '', '2013-06-15 13:01:31');
INSERT INTO `using_log` VALUES (448, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:33');
INSERT INTO `using_log` VALUES (449, 1, 'RequestBooking', 'Index', '2013-06-15 13:01:45');
INSERT INTO `using_log` VALUES (450, 1, 'RequestBooking', '', '2013-06-15 13:02:04');
INSERT INTO `using_log` VALUES (451, 1, 'RequestBooking', '', '2013-06-15 13:02:06');
INSERT INTO `using_log` VALUES (452, 1, 'RequestBooking', '', '2013-06-15 13:02:08');
INSERT INTO `using_log` VALUES (453, 1, 'RequestBooking', '', '2013-06-15 13:03:12');
INSERT INTO `using_log` VALUES (454, 1, 'RequestBooking', '', '2013-06-15 13:03:22');
INSERT INTO `using_log` VALUES (455, 1, 'RequestBooking', '', '2013-06-15 13:03:58');
INSERT INTO `using_log` VALUES (456, 1, 'RequestBooking', '', '2013-06-15 13:04:09');
INSERT INTO `using_log` VALUES (457, 1, 'RequestBooking', '', '2013-06-15 13:04:10');
INSERT INTO `using_log` VALUES (458, 1, 'RequestBooking', '', '2013-06-15 13:04:18');
INSERT INTO `using_log` VALUES (459, 1, 'RequestBooking', 'Index', '2013-06-15 13:04:27');
INSERT INTO `using_log` VALUES (460, 1, 'RequestBooking', 'Index', '2013-06-15 13:04:31');
INSERT INTO `using_log` VALUES (461, 1, 'RequestBooking', 'Index', '2013-06-15 13:04:33');
INSERT INTO `using_log` VALUES (462, 1, 'RequestBooking', '', '2013-06-15 13:06:20');
INSERT INTO `using_log` VALUES (463, 1, 'RequestBooking', '', '2013-06-15 13:06:22');
INSERT INTO `using_log` VALUES (464, 1, 'RequestBooking', '', '2013-06-15 13:06:23');
INSERT INTO `using_log` VALUES (465, 1, 'RequestBooking', '', '2013-06-15 13:06:23');
INSERT INTO `using_log` VALUES (466, 1, 'RequestBooking', '', '2013-06-15 13:06:23');
INSERT INTO `using_log` VALUES (467, 1, 'RequestBooking', '', '2013-06-15 13:06:36');
INSERT INTO `using_log` VALUES (468, 1, 'RequestBooking', '', '2013-06-15 13:17:17');
INSERT INTO `using_log` VALUES (469, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:20');
INSERT INTO `using_log` VALUES (470, 1, 'RequestBooking', '', '2013-06-15 13:17:39');
INSERT INTO `using_log` VALUES (471, 1, 'RequestBooking', '', '2013-06-15 13:17:41');
INSERT INTO `using_log` VALUES (472, 1, 'RequestBooking', '', '2013-06-15 13:17:49');
INSERT INTO `using_log` VALUES (473, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:57');
INSERT INTO `using_log` VALUES (474, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:58');
INSERT INTO `using_log` VALUES (475, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:17:59');
INSERT INTO `using_log` VALUES (476, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:00');
INSERT INTO `using_log` VALUES (477, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:00');
INSERT INTO `using_log` VALUES (478, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:00');
INSERT INTO `using_log` VALUES (479, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:18:01');
INSERT INTO `using_log` VALUES (480, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:18');
INSERT INTO `using_log` VALUES (481, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:25');
INSERT INTO `using_log` VALUES (482, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:58');
INSERT INTO `using_log` VALUES (483, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:59');
INSERT INTO `using_log` VALUES (484, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:51:58');
INSERT INTO `using_log` VALUES (485, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:56:17');
INSERT INTO `using_log` VALUES (486, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:56:30');
INSERT INTO `using_log` VALUES (487, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:56:31');
INSERT INTO `using_log` VALUES (488, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:14:29');
INSERT INTO `using_log` VALUES (489, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:15:29');
INSERT INTO `using_log` VALUES (490, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:16:33');
INSERT INTO `using_log` VALUES (491, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:16:37');
INSERT INTO `using_log` VALUES (492, 1, 'management', 'CheckAlertToTurnItemBack', '2013-06-15 14:16:38');
INSERT INTO `using_log` VALUES (493, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:19:16');
INSERT INTO `using_log` VALUES (494, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:19:59');
INSERT INTO `using_log` VALUES (495, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:19:59');
INSERT INTO `using_log` VALUES (496, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 14:20:01');
INSERT INTO `using_log` VALUES (497, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:27');
INSERT INTO `using_log` VALUES (498, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:36');
INSERT INTO `using_log` VALUES (499, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:50:46');
INSERT INTO `using_log` VALUES (500, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:51:01');
INSERT INTO `using_log` VALUES (501, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:53:18');
INSERT INTO `using_log` VALUES (502, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:54:29');
INSERT INTO `using_log` VALUES (503, 1, 'management', 'checkalerttosocialmedia', '2013-06-15 13:55:29');
INSERT INTO `using_log` VALUES (504, NULL, '', '', '2013-06-16 10:42:23');
INSERT INTO `using_log` VALUES (505, NULL, 'management', 'login', '2013-06-16 10:42:50');
INSERT INTO `using_log` VALUES (506, NULL, 'management', 'login', '2013-06-16 10:42:55');
INSERT INTO `using_log` VALUES (507, 1, 'RequestBooking', '', '2013-06-16 10:43:03');
INSERT INTO `using_log` VALUES (508, 1, 'Admin', '', '2013-06-16 10:43:06');
INSERT INTO `using_log` VALUES (509, 1, 'Report', '', '2013-06-16 10:43:07');
INSERT INTO `using_log` VALUES (510, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-16 10:43:14');
INSERT INTO `using_log` VALUES (511, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-16 10:43:26');
INSERT INTO `using_log` VALUES (512, 1, 'Report', 'RequestBookingStatisticReport', '2013-06-16 10:43:29');
INSERT INTO `using_log` VALUES (513, 1, 'Admin', '', '2013-06-16 10:43:53');
INSERT INTO `using_log` VALUES (514, 1, 'Room', '', '2013-06-16 10:43:57');
INSERT INTO `using_log` VALUES (515, 1, 'EquipmentType', '', '2013-06-16 10:44:00');
INSERT INTO `using_log` VALUES (516, 1, 'equipmentType', 'update', '2013-06-16 10:44:06');
INSERT INTO `using_log` VALUES (517, 1, 'EquipmentType', '', '2013-06-16 10:44:18');
INSERT INTO `using_log` VALUES (518, 1, 'equipmentType', 'update', '2013-06-16 10:44:20');
INSERT INTO `using_log` VALUES (519, 1, 'EquipmentType', '', '2013-06-16 10:44:30');
INSERT INTO `using_log` VALUES (520, 1, 'equipmentType', 'update', '2013-06-16 10:44:32');
INSERT INTO `using_log` VALUES (521, 1, 'EquipmentType', '', '2013-06-16 10:44:45');
INSERT INTO `using_log` VALUES (522, 1, 'equipmentType', 'update', '2013-06-16 10:44:47');
INSERT INTO `using_log` VALUES (523, 1, 'EquipmentType', '', '2013-06-16 10:44:59');
INSERT INTO `using_log` VALUES (524, 1, 'equipmentType', 'update', '2013-06-16 10:45:08');
INSERT INTO `using_log` VALUES (525, 1, 'EquipmentType', '', '2013-06-16 10:45:21');
INSERT INTO `using_log` VALUES (526, 1, 'equipmentType', 'update', '2013-06-16 10:45:23');
INSERT INTO `using_log` VALUES (527, 1, 'EquipmentType', '', '2013-06-16 10:45:38');
INSERT INTO `using_log` VALUES (528, 1, 'equipmentType', 'update', '2013-06-16 10:45:41');
INSERT INTO `using_log` VALUES (529, 1, 'EquipmentType', '', '2013-06-16 10:45:46');
INSERT INTO `using_log` VALUES (530, 1, 'EquipmentType', '', '2013-06-16 10:45:47');
INSERT INTO `using_log` VALUES (531, 1, 'equipmentType', 'update', '2013-06-16 10:45:49');
INSERT INTO `using_log` VALUES (532, 1, 'EquipmentType', '', '2013-06-16 10:45:57');
INSERT INTO `using_log` VALUES (533, 1, 'equipmentType', 'update', '2013-06-16 10:46:02');
INSERT INTO `using_log` VALUES (534, 1, 'EquipmentType', '', '2013-06-16 10:46:14');
INSERT INTO `using_log` VALUES (535, 1, 'EquipmentType', 'create', '2013-06-16 10:46:16');
INSERT INTO `using_log` VALUES (536, 1, 'EquipmentType', '', '2013-06-16 10:46:20');
INSERT INTO `using_log` VALUES (537, 1, 'EquipmentType', 'create', '2013-06-16 10:46:28');
INSERT INTO `using_log` VALUES (538, 1, 'EquipmentType', '', '2013-06-16 10:46:53');
INSERT INTO `using_log` VALUES (539, 1, 'Equipment', '', '2013-06-16 10:47:02');
INSERT INTO `using_log` VALUES (540, 1, 'Equipment', '', '2013-06-16 11:40:44');
INSERT INTO `using_log` VALUES (541, 1, 'Room', '', '2013-06-16 11:40:46');
INSERT INTO `using_log` VALUES (542, 1, 'Equipment', '', '2013-06-16 11:41:14');
INSERT INTO `using_log` VALUES (543, NULL, '', '', '2013-06-16 12:50:19');
INSERT INTO `using_log` VALUES (544, 1, 'RequestBooking', '', '2013-06-16 12:50:23');
INSERT INTO `using_log` VALUES (545, 1, 'Admin', '', '2013-06-16 12:50:30');
INSERT INTO `using_log` VALUES (546, 1, 'Equipment', '', '2013-06-16 12:50:33');
INSERT INTO `using_log` VALUES (547, 1, 'Solution', '', '2013-06-16 12:50:39');
INSERT INTO `using_log` VALUES (548, 1, 'Solution', 'Index', '2013-06-16 12:50:43');
INSERT INTO `using_log` VALUES (549, 1, 'Solution', 'Index', '2013-06-16 12:50:54');
INSERT INTO `using_log` VALUES (550, 1, 'RequestBooking', '', '2013-06-16 12:51:07');
INSERT INTO `using_log` VALUES (551, 1, 'RequestBooking', 'Request', '2013-06-16 12:51:08');
INSERT INTO `using_log` VALUES (552, 1, 'RequestBooking', 'Request', '2013-06-16 12:51:14');
INSERT INTO `using_log` VALUES (553, 1, 'RequestBooking', '', '2013-06-16 12:51:20');
INSERT INTO `using_log` VALUES (554, 1, 'RequestBooking', 'Request', '2013-06-16 12:51:23');
INSERT INTO `using_log` VALUES (555, NULL, '', '', '2013-06-16 22:22:35');
INSERT INTO `using_log` VALUES (556, 2, 'RequestBooking', '', '2013-06-16 22:22:40');
INSERT INTO `using_log` VALUES (557, 2, 'RequestBooking', 'CheckStatus', '2013-06-16 22:22:47');
INSERT INTO `using_log` VALUES (558, 2, 'RequestBooking', '', '2013-06-16 22:22:49');
INSERT INTO `using_log` VALUES (559, 2, 'RequestBooking', 'Index', '2013-06-16 22:22:50');
INSERT INTO `using_log` VALUES (560, 2, 'RequestBooking', 'Request', '2013-06-16 22:22:55');
INSERT INTO `using_log` VALUES (561, 2, 'RequestBooking', 'View', '2013-06-16 22:23:57');
INSERT INTO `using_log` VALUES (562, 2, 'RequestBooking', '', '2013-06-16 22:23:59');
INSERT INTO `using_log` VALUES (563, 2, 'RequestBooking', 'Request', '2013-06-16 22:24:04');
INSERT INTO `using_log` VALUES (564, 2, 'RequestBooking', 'View', '2013-06-16 22:24:42');
INSERT INTO `using_log` VALUES (565, 2, 'RequestBooking', '', '2013-06-16 22:27:07');
INSERT INTO `using_log` VALUES (566, 2, 'RequestBooking', 'Request', '2013-06-16 22:27:09');
INSERT INTO `using_log` VALUES (567, 2, 'RequestBorrow', '', '2013-06-16 22:27:28');
INSERT INTO `using_log` VALUES (568, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:27:30');
INSERT INTO `using_log` VALUES (569, 2, 'RequestBooking', '', '2013-06-16 22:27:38');
INSERT INTO `using_log` VALUES (570, 2, 'RequestBooking', 'Request', '2013-06-16 22:27:39');
INSERT INTO `using_log` VALUES (571, 2, 'RequestBorrow', '', '2013-06-16 22:28:24');
INSERT INTO `using_log` VALUES (572, 2, 'RequestBorrow', 'Request', '2013-06-16 22:28:26');
INSERT INTO `using_log` VALUES (573, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:28:40');
INSERT INTO `using_log` VALUES (574, 2, 'RequestBorrow', 'Request', '2013-06-16 22:29:56');
INSERT INTO `using_log` VALUES (575, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:30:01');
INSERT INTO `using_log` VALUES (576, 2, 'RequestBorrow', 'Request', '2013-06-16 22:30:05');
INSERT INTO `using_log` VALUES (577, 2, 'RequestService', '', '2013-06-16 22:30:07');
INSERT INTO `using_log` VALUES (578, 2, 'RequestService', 'Request', '2013-06-16 22:30:08');
INSERT INTO `using_log` VALUES (579, 2, 'RequestBorrow', '', '2013-06-16 22:30:10');
INSERT INTO `using_log` VALUES (580, 2, 'RequestBorrow', 'Request', '2013-06-16 22:30:11');
INSERT INTO `using_log` VALUES (581, 2, 'RequestBorrow', 'Request', '2013-06-16 22:30:40');
INSERT INTO `using_log` VALUES (582, 2, 'RequestBorrow', 'Request', '2013-06-16 22:31:06');
INSERT INTO `using_log` VALUES (583, 2, 'RequestBorrow', '', '2013-06-16 22:31:45');
INSERT INTO `using_log` VALUES (584, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:31:47');
INSERT INTO `using_log` VALUES (585, 2, 'RequestBorrow', 'Request', '2013-06-16 22:31:49');
INSERT INTO `using_log` VALUES (586, 2, 'RequestBooking', '', '2013-06-16 22:33:51');
INSERT INTO `using_log` VALUES (587, 2, 'RequestBooking', 'Request', '2013-06-16 22:33:53');
INSERT INTO `using_log` VALUES (588, 2, 'RequestBooking', 'Request', '2013-06-16 22:34:37');
INSERT INTO `using_log` VALUES (589, 2, 'RequestBooking', 'Request', '2013-06-16 22:35:02');
INSERT INTO `using_log` VALUES (590, 2, 'RequestBooking', 'View', '2013-06-16 22:35:30');
INSERT INTO `using_log` VALUES (591, 2, 'RequestBooking', '', '2013-06-16 22:35:32');
INSERT INTO `using_log` VALUES (592, 2, 'RequestBooking', 'Index', '2013-06-16 22:35:34');
INSERT INTO `using_log` VALUES (593, 2, 'RequestBooking', 'Index', '2013-06-16 22:35:38');
INSERT INTO `using_log` VALUES (594, 2, 'RequestBooking', 'Index', '2013-06-16 22:35:42');
INSERT INTO `using_log` VALUES (595, 2, 'RequestService', '', '2013-06-16 22:35:46');
INSERT INTO `using_log` VALUES (596, 2, 'RequestService', 'Request', '2013-06-16 22:35:48');
INSERT INTO `using_log` VALUES (597, 2, 'RequestService', 'View', '2013-06-16 22:36:01');
INSERT INTO `using_log` VALUES (598, 2, 'RequestService', '', '2013-06-16 22:36:03');
INSERT INTO `using_log` VALUES (599, 2, 'RequestService', 'Request', '2013-06-16 22:36:12');
INSERT INTO `using_log` VALUES (600, 2, 'Admin', '', '2013-06-16 22:36:15');
INSERT INTO `using_log` VALUES (601, 2, 'ServiceTypeItem', '', '2013-06-16 22:36:17');
INSERT INTO `using_log` VALUES (602, 2, 'EquipmentType', '', '2013-06-16 22:36:18');
INSERT INTO `using_log` VALUES (603, 2, 'Equipment', '', '2013-06-16 22:36:19');
INSERT INTO `using_log` VALUES (604, 2, 'Department', '', '2013-06-16 22:36:20');
INSERT INTO `using_log` VALUES (605, 2, 'Room', '', '2013-06-16 22:36:22');
INSERT INTO `using_log` VALUES (606, 2, 'Semester', '', '2013-06-16 22:36:23');
INSERT INTO `using_log` VALUES (607, 2, 'User', '', '2013-06-16 22:36:24');
INSERT INTO `using_log` VALUES (608, 2, 'user', 'update', '2013-06-16 22:37:43');
INSERT INTO `using_log` VALUES (609, 2, 'User', '', '2013-06-16 22:37:50');
INSERT INTO `using_log` VALUES (610, 2, 'RequestBorrow', '', '2013-06-16 22:38:29');
INSERT INTO `using_log` VALUES (611, 2, 'RequestBooking', '', '2013-06-16 22:38:31');
INSERT INTO `using_log` VALUES (612, 2, 'RequestBooking', 'Request', '2013-06-16 22:38:32');
INSERT INTO `using_log` VALUES (613, 2, 'RequestBorrow', '', '2013-06-16 22:45:50');
INSERT INTO `using_log` VALUES (614, 2, 'RequestBooking', '', '2013-06-16 22:45:52');
INSERT INTO `using_log` VALUES (615, 2, 'RequestBooking', 'CheckStatus', '2013-06-16 22:45:54');
INSERT INTO `using_log` VALUES (616, 2, 'RequestBooking', 'Request', '2013-06-16 22:45:56');
INSERT INTO `using_log` VALUES (617, 2, 'RequestBorrow', '', '2013-06-16 22:46:08');
INSERT INTO `using_log` VALUES (618, 2, 'RequestBorrow', 'Request', '2013-06-16 22:46:10');
INSERT INTO `using_log` VALUES (619, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:46:11');
INSERT INTO `using_log` VALUES (620, 2, 'RequestBorrow', 'request', '2013-06-16 22:46:48');
INSERT INTO `using_log` VALUES (621, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:46:58');
INSERT INTO `using_log` VALUES (622, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:02');
INSERT INTO `using_log` VALUES (623, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:08');
INSERT INTO `using_log` VALUES (624, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:12');
INSERT INTO `using_log` VALUES (625, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:14');
INSERT INTO `using_log` VALUES (626, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:16');
INSERT INTO `using_log` VALUES (627, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:18');
INSERT INTO `using_log` VALUES (628, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:20');
INSERT INTO `using_log` VALUES (629, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:22');
INSERT INTO `using_log` VALUES (630, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:27');
INSERT INTO `using_log` VALUES (631, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:29');
INSERT INTO `using_log` VALUES (632, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:31');
INSERT INTO `using_log` VALUES (633, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:33');
INSERT INTO `using_log` VALUES (634, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:41');
INSERT INTO `using_log` VALUES (635, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:43');
INSERT INTO `using_log` VALUES (636, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:46');
INSERT INTO `using_log` VALUES (637, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:48');
INSERT INTO `using_log` VALUES (638, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:52');
INSERT INTO `using_log` VALUES (639, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:47:54');
INSERT INTO `using_log` VALUES (640, 2, 'RequestBorrow', 'request', '2013-06-16 22:47:59');
INSERT INTO `using_log` VALUES (641, 2, 'RequestBorrow', 'CheckStatus', '2013-06-16 22:48:01');
INSERT INTO `using_log` VALUES (642, 2, 'RequestBorrow', 'request', '2013-06-16 22:48:02');
INSERT INTO `using_log` VALUES (643, 2, 'RequestBorrow', 'Request', '2013-06-16 22:48:05');
INSERT INTO `using_log` VALUES (644, 2, 'RequestService', '', '2013-06-16 22:48:11');
INSERT INTO `using_log` VALUES (645, 2, 'Admin', '', '2013-06-16 22:48:13');
INSERT INTO `using_log` VALUES (646, 2, 'User', '', '2013-06-16 22:59:42');
INSERT INTO `using_log` VALUES (647, 2, 'Room', '', '2013-06-16 23:04:22');
INSERT INTO `using_log` VALUES (648, 2, 'User', '', '2013-06-16 23:04:26');
INSERT INTO `using_log` VALUES (649, 2, 'user', 'update', '2013-06-16 23:04:28');
INSERT INTO `using_log` VALUES (650, 2, 'User', '', '2013-06-16 23:04:30');
INSERT INTO `using_log` VALUES (651, 2, 'user', 'view', '2013-06-16 23:04:34');
INSERT INTO `using_log` VALUES (652, 2, 'user', '', '2013-06-16 23:04:36');
INSERT INTO `using_log` VALUES (653, 2, 'Room', '', '2013-06-16 23:04:39');
INSERT INTO `using_log` VALUES (654, 2, 'Semester', '', '2013-06-16 23:04:47');
INSERT INTO `using_log` VALUES (655, 2, 'Department', '', '2013-06-16 23:04:50');
INSERT INTO `using_log` VALUES (656, 2, 'department', 'view', '2013-06-16 23:04:54');
INSERT INTO `using_log` VALUES (657, 2, 'Department', '', '2013-06-16 23:04:55');
INSERT INTO `using_log` VALUES (658, NULL, 'management', 'login', '2013-06-16 23:05:08');
INSERT INTO `using_log` VALUES (659, 1, 'RequestBooking', '', '2013-06-16 23:05:16');
INSERT INTO `using_log` VALUES (660, 1, 'Admin', '', '2013-06-16 23:05:18');
INSERT INTO `using_log` VALUES (661, 1, 'User', '', '2013-06-16 23:05:21');
INSERT INTO `using_log` VALUES (662, 1, 'user', 'delete', '2013-06-16 23:05:24');
INSERT INTO `using_log` VALUES (663, 1, 'User', '', '2013-06-16 23:05:24');
INSERT INTO `using_log` VALUES (664, 1, 'user', 'delete', '2013-06-16 23:05:27');
INSERT INTO `using_log` VALUES (665, 1, 'User', '', '2013-06-16 23:05:27');
INSERT INTO `using_log` VALUES (666, 1, 'User', '', '2013-06-16 23:06:24');
INSERT INTO `using_log` VALUES (667, 1, 'user', 'delete', '2013-06-16 23:06:28');
INSERT INTO `using_log` VALUES (668, 1, 'User', '', '2013-06-16 23:06:28');
INSERT INTO `using_log` VALUES (669, 1, 'user', 'delete', '2013-06-16 23:06:31');
INSERT INTO `using_log` VALUES (670, 1, 'User', '', '2013-06-16 23:06:31');
INSERT INTO `using_log` VALUES (671, 1, 'user', 'delete', '2013-06-16 23:06:33');
INSERT INTO `using_log` VALUES (672, 1, 'User', '', '2013-06-16 23:06:33');
INSERT INTO `using_log` VALUES (673, 1, 'user', 'delete', '2013-06-16 23:06:36');
INSERT INTO `using_log` VALUES (674, 1, 'User', '', '2013-06-16 23:06:36');
INSERT INTO `using_log` VALUES (675, 1, 'user', 'delete', '2013-06-16 23:06:38');
INSERT INTO `using_log` VALUES (676, 1, 'User', '', '2013-06-16 23:06:38');
INSERT INTO `using_log` VALUES (677, 1, 'user', 'delete', '2013-06-16 23:06:40');
INSERT INTO `using_log` VALUES (678, 1, 'User', '', '2013-06-16 23:06:41');
INSERT INTO `using_log` VALUES (679, 1, 'user', 'delete', '2013-06-16 23:06:42');
INSERT INTO `using_log` VALUES (680, 1, 'User', '', '2013-06-16 23:06:43');
INSERT INTO `using_log` VALUES (681, 1, 'ConfirmUser', '', '2013-06-16 23:06:45');
INSERT INTO `using_log` VALUES (682, 1, 'admin', '', '2013-06-16 23:06:46');
INSERT INTO `using_log` VALUES (683, 1, 'ConfirmUser', '', '2013-06-16 23:06:48');
INSERT INTO `using_log` VALUES (684, 1, 'User', '', '2013-06-16 23:06:49');
INSERT INTO `using_log` VALUES (685, 1, 'User', 'Staff', '2013-06-16 23:06:51');
INSERT INTO `using_log` VALUES (686, 1, 'User', '', '2013-06-16 23:06:55');
INSERT INTO `using_log` VALUES (687, 1, 'Role', '', '2013-06-16 23:06:59');
INSERT INTO `using_log` VALUES (688, 1, 'User', 'Staff', '2013-06-16 23:07:01');
INSERT INTO `using_log` VALUES (689, 1, 'User', '', '2013-06-16 23:07:11');
INSERT INTO `using_log` VALUES (690, 1, 'user', 'create', '2013-06-16 23:07:17');
INSERT INTO `using_log` VALUES (691, 1, 'user', 'create', '2013-06-16 23:07:38');
INSERT INTO `using_log` VALUES (692, 1, 'user', 'create', '2013-06-16 23:08:00');
INSERT INTO `using_log` VALUES (693, 1, 'RequestBooking', '', '2013-06-16 23:08:11');
INSERT INTO `using_log` VALUES (694, 1, 'Admin', '', '2013-06-16 23:08:13');
INSERT INTO `using_log` VALUES (695, 1, 'User', '', '2013-06-16 23:08:14');
INSERT INTO `using_log` VALUES (696, 1, 'user', 'delete', '2013-06-16 23:08:26');
INSERT INTO `using_log` VALUES (697, 1, 'User', '', '2013-06-16 23:08:26');
INSERT INTO `using_log` VALUES (698, 1, 'user', 'update', '2013-06-16 23:08:28');
INSERT INTO `using_log` VALUES (699, 1, 'user', 'view', '2013-06-16 23:08:44');
INSERT INTO `using_log` VALUES (700, 1, 'user', '', '2013-06-16 23:08:46');
INSERT INTO `using_log` VALUES (701, 1, 'user', 'update', '2013-06-16 23:08:55');
INSERT INTO `using_log` VALUES (702, 1, 'user', 'view', '2013-06-16 23:09:08');
INSERT INTO `using_log` VALUES (703, 1, 'user', '', '2013-06-16 23:09:09');
INSERT INTO `using_log` VALUES (704, NULL, 'management', 'login', '2013-06-16 23:09:14');
INSERT INTO `using_log` VALUES (705, NULL, 'Management', 'Register', '2013-06-16 23:09:15');
INSERT INTO `using_log` VALUES (706, NULL, 'Management', 'Register', '2013-06-16 23:09:41');
INSERT INTO `using_log` VALUES (707, 1, 'RequestBooking', '', '2013-06-16 23:09:52');
INSERT INTO `using_log` VALUES (708, 1, 'Admin', '', '2013-06-16 23:09:54');
INSERT INTO `using_log` VALUES (709, 1, 'User', '', '2013-06-16 23:09:56');
INSERT INTO `using_log` VALUES (710, 1, 'RequestBorrow', '', '2013-06-16 23:09:57');
INSERT INTO `using_log` VALUES (711, 1, 'RequestBorrow', 'Request', '2013-06-16 23:09:58');
INSERT INTO `using_log` VALUES (712, 1, 'RequestBorrow', 'Request', '2013-06-16 23:10:33');
INSERT INTO `using_log` VALUES (713, 1, 'RequestBorrow', 'Request', '2013-06-16 23:11:00');
INSERT INTO `using_log` VALUES (714, 1, 'RequestService', '', '2013-06-16 23:11:11');
INSERT INTO `using_log` VALUES (715, 1, 'RequestBorrow', '', '2013-06-16 23:11:15');
INSERT INTO `using_log` VALUES (716, 1, 'Solution', '', '2013-06-16 23:12:11');
INSERT INTO `using_log` VALUES (717, 1, 'Solution', 'Index', '2013-06-16 23:12:14');
INSERT INTO `using_log` VALUES (718, 1, 'Solution', 'Index', '2013-06-16 23:12:15');
INSERT INTO `using_log` VALUES (719, 1, 'Solution', 'Index', '2013-06-16 23:12:22');
INSERT INTO `using_log` VALUES (720, 1, 'Solution', 'Index', '2013-06-16 23:12:23');
INSERT INTO `using_log` VALUES (721, 1, 'RequestService', '', '2013-06-16 23:12:25');
INSERT INTO `using_log` VALUES (722, 1, 'RequestBooking', '', '2013-06-16 23:12:26');
INSERT INTO `using_log` VALUES (723, 1, 'RequestBooking', 'Request', '2013-06-16 23:12:29');
INSERT INTO `using_log` VALUES (724, 1, 'RequestBooking', 'CheckStatus', '2013-06-16 23:12:31');
INSERT INTO `using_log` VALUES (725, 1, 'RequestBooking', '', '2013-06-16 23:12:32');
INSERT INTO `using_log` VALUES (726, 1, 'RequestBooking', 'Index', '2013-06-16 23:12:34');
INSERT INTO `using_log` VALUES (727, 1, 'RequestBooking', 'Request', '2013-06-16 23:12:38');
INSERT INTO `using_log` VALUES (728, 1, 'RequestBooking', 'Request', '2013-06-16 23:12:57');
INSERT INTO `using_log` VALUES (729, 1, 'RequestBooking', 'View', '2013-06-16 23:13:26');
INSERT INTO `using_log` VALUES (730, 1, 'RequestBooking', '', '2013-06-16 23:13:28');
INSERT INTO `using_log` VALUES (731, 1, 'RequestBorrow', '', '2013-06-16 23:13:29');
INSERT INTO `using_log` VALUES (732, 1, 'RequestBorrow', 'Request', '2013-06-16 23:13:30');
INSERT INTO `using_log` VALUES (733, 1, 'RequestBorrow', 'Request', '2013-06-16 23:13:49');
INSERT INTO `using_log` VALUES (734, 1, 'RequestBorrow', 'Request', '2013-06-16 23:21:41');
INSERT INTO `using_log` VALUES (735, 1, 'RequestBorrow', 'Request', '2013-06-16 23:22:01');
INSERT INTO `using_log` VALUES (736, 1, 'RequestBorrow', 'View', '2013-06-16 23:25:56');
INSERT INTO `using_log` VALUES (737, 1, 'RequestBorrow', '', '2013-06-16 23:26:04');
INSERT INTO `using_log` VALUES (738, 1, 'RequestBorrow', 'Request', '2013-06-16 23:26:06');
INSERT INTO `using_log` VALUES (739, 1, 'RequestBorrow', 'View', '2013-06-16 23:26:36');
INSERT INTO `using_log` VALUES (740, 1, 'RequestBorrow', '', '2013-06-16 23:26:38');
INSERT INTO `using_log` VALUES (741, 1, 'RequestBorrow', '', '2013-06-16 23:26:52');
INSERT INTO `using_log` VALUES (742, 1, 'RequestBorrow', 'Index', '2013-06-16 23:26:56');
INSERT INTO `using_log` VALUES (743, 1, 'RequestBorrow', 'CheckStatus', '2013-06-16 23:27:02');
INSERT INTO `using_log` VALUES (744, NULL, 'management', 'login', '2013-06-16 23:46:43');
INSERT INTO `using_log` VALUES (745, 3, 'RequestBooking', '', '2013-06-16 23:46:49');
INSERT INTO `using_log` VALUES (746, NULL, '', '', '2013-06-29 12:08:37');
INSERT INTO `using_log` VALUES (747, 1, 'RequestBooking', '', '2013-06-29 12:08:40');
INSERT INTO `using_log` VALUES (748, 1, 'Report', '', '2013-06-29 12:08:42');
INSERT INTO `using_log` VALUES (749, 1, 'Report', 'UsingSheet', '2013-06-29 12:08:44');
INSERT INTO `using_log` VALUES (750, 1, 'Report', 'UsingSheet', '2013-06-29 12:08:48');
INSERT INTO `using_log` VALUES (751, 1, 'WebService', 'ShowRequest', '2013-06-29 12:37:03');
INSERT INTO `using_log` VALUES (752, 1, 'WebService', 'ShowRequest', '2013-06-29 12:37:48');
INSERT INTO `using_log` VALUES (753, 1, 'WebService', 'ShowRequest', '2013-06-29 12:39:07');
INSERT INTO `using_log` VALUES (754, 1, 'WebService', 'ShowRequest', '2013-06-29 12:41:35');
INSERT INTO `using_log` VALUES (755, 1, '', '', '2013-06-29 13:36:52');
INSERT INTO `using_log` VALUES (756, 1, 'WebService', 'SendNotification', '2013-06-29 12:46:34');
INSERT INTO `using_log` VALUES (757, 1, 'WebService', 'SendNotification', '2013-06-29 12:46:51');
INSERT INTO `using_log` VALUES (758, 1, 'WebService', 'SendNotification', '2013-06-29 12:56:01');
INSERT INTO `using_log` VALUES (759, 1, 'WebService', 'SendNotification', '2013-06-29 12:56:04');
INSERT INTO `using_log` VALUES (760, 1, 'WebService', 'SendNotification', '2013-06-29 12:56:06');
INSERT INTO `using_log` VALUES (761, 1, 'WebService', 'SendNotification', '2013-06-29 12:56:17');
INSERT INTO `using_log` VALUES (762, 1, 'WebService', 'ShowRequest', '2013-06-29 14:15:41');
INSERT INTO `using_log` VALUES (763, 1, 'WebService', 'GetRequestType', '2013-06-29 14:18:08');
INSERT INTO `using_log` VALUES (764, NULL, '', '', '2013-07-01 14:50:03');
INSERT INTO `using_log` VALUES (765, 1, 'RequestBooking', '', '2013-07-01 14:50:08');
INSERT INTO `using_log` VALUES (766, 1, 'Solution', '', '2013-07-01 14:50:10');
INSERT INTO `using_log` VALUES (767, 1, 'Solution', '', '2013-07-01 14:50:18');
INSERT INTO `using_log` VALUES (768, 1, 'Admin', '', '2013-07-01 14:52:56');
INSERT INTO `using_log` VALUES (769, 1, 'Link', '', '2013-07-01 14:58:29');
INSERT INTO `using_log` VALUES (770, 1, 'News', '', '2013-07-01 14:58:31');
INSERT INTO `using_log` VALUES (771, 1, 'Link', '', '2013-07-01 14:58:34');
INSERT INTO `using_log` VALUES (772, 1, 'link', 'create', '2013-07-01 14:58:35');
INSERT INTO `using_log` VALUES (773, 1, 'Report', '', '2013-07-01 14:58:39');
INSERT INTO `using_log` VALUES (774, 1, 'RequestBorrow', '', '2013-07-01 14:58:40');
INSERT INTO `using_log` VALUES (775, 1, 'RequestBooking', '', '2013-07-01 14:58:42');
INSERT INTO `using_log` VALUES (776, 1, 'RequestBooking', 'Index', '2013-07-01 14:58:50');
INSERT INTO `using_log` VALUES (777, 1, 'RequestBorrow', '', '2013-07-01 14:58:58');
INSERT INTO `using_log` VALUES (778, 1, 'RequestService', '', '2013-07-01 14:59:00');
INSERT INTO `using_log` VALUES (779, 1, 'Solution', '', '2013-07-01 14:59:01');
INSERT INTO `using_log` VALUES (780, 1, 'Report', '', '2013-07-01 14:59:02');
INSERT INTO `using_log` VALUES (781, 1, 'Solution', '', '2013-07-01 14:59:03');
INSERT INTO `using_log` VALUES (782, 1, 'Report', '', '2013-07-01 14:59:04');
INSERT INTO `using_log` VALUES (783, 1, 'Report', 'UsingSheet', '2013-07-01 14:59:41');
INSERT INTO `using_log` VALUES (784, 1, 'Report', 'UsingSheet', '2013-07-01 14:59:45');
INSERT INTO `using_log` VALUES (785, 1, 'RequestBooking', '', '2013-07-01 14:59:50');
INSERT INTO `using_log` VALUES (786, 1, 'RequestBooking', 'Request', '2013-07-01 14:59:51');
INSERT INTO `using_log` VALUES (787, 1, 'RequestBorrow', '', '2013-07-01 14:59:56');
INSERT INTO `using_log` VALUES (788, 1, 'RequestBooking', '', '2013-07-01 14:59:57');
INSERT INTO `using_log` VALUES (789, 1, '', '', '2013-07-01 15:08:38');
INSERT INTO `using_log` VALUES (790, 1, 'RequestBooking', '', '2013-07-01 15:08:41');
INSERT INTO `using_log` VALUES (791, 1, 'Admin', '', '2013-07-01 15:08:43');
INSERT INTO `using_log` VALUES (792, 1, 'Solution', '', '2013-07-01 15:08:45');
INSERT INTO `using_log` VALUES (793, 1, 'Solution', 'Index', '2013-07-01 15:08:52');
INSERT INTO `using_log` VALUES (794, 1, 'Solution', 'Index', '2013-07-01 15:08:54');
INSERT INTO `using_log` VALUES (795, 1, 'Solution', '', '2013-07-01 15:12:33');
INSERT INTO `using_log` VALUES (796, 1, 'Solution', 'Index', '2013-07-01 15:12:37');
INSERT INTO `using_log` VALUES (797, 1, 'Solution', 'Index', '2013-07-01 15:12:39');
INSERT INTO `using_log` VALUES (798, 1, 'Solution', 'Index', '2013-07-01 15:12:41');
INSERT INTO `using_log` VALUES (799, 1, 'Solution', '', '2013-07-01 15:13:33');
INSERT INTO `using_log` VALUES (800, 1, 'Solution', 'Index', '2013-07-01 15:13:35');
INSERT INTO `using_log` VALUES (801, 1, 'Solution', 'Index', '2013-07-01 15:13:36');
INSERT INTO `using_log` VALUES (802, 1, 'Solution', 'Index', '2013-07-01 15:13:39');
INSERT INTO `using_log` VALUES (803, 1, 'Solution', 'Index', '2013-07-01 15:13:42');
INSERT INTO `using_log` VALUES (804, 1, 'RequestBooking', '', '2013-07-01 15:13:56');
INSERT INTO `using_log` VALUES (805, 1, 'RequestBooking', 'Request', '2013-07-01 15:14:28');
INSERT INTO `using_log` VALUES (806, 1, 'RequestBooking', 'Request', '2013-07-01 15:14:48');
INSERT INTO `using_log` VALUES (807, 1, 'RequestBooking', 'Request', '2013-07-01 15:15:00');
INSERT INTO `using_log` VALUES (808, 1, 'RequestBooking', 'Request', '2013-07-01 15:15:04');
INSERT INTO `using_log` VALUES (809, 1, 'RequestBooking', '', '2013-07-01 15:36:27');
INSERT INTO `using_log` VALUES (810, 1, 'RequestBooking', 'Request', '2013-07-01 15:36:29');
INSERT INTO `using_log` VALUES (811, 1, 'RequestBooking', 'Request', '2013-07-01 15:47:32');
INSERT INTO `using_log` VALUES (812, 1, 'RequestBorrow', '', '2013-07-01 15:47:49');
INSERT INTO `using_log` VALUES (813, 1, 'RequestBorrow', 'Request', '2013-07-01 15:47:53');
INSERT INTO `using_log` VALUES (814, 1, 'RequestBooking', '', '2013-07-01 15:48:07');
INSERT INTO `using_log` VALUES (815, 1, 'RequestBooking', 'Request', '2013-07-01 15:48:09');
INSERT INTO `using_log` VALUES (816, 1, 'Admin', '', '2013-07-01 15:48:25');
INSERT INTO `using_log` VALUES (817, 1, 'Report', '', '2013-07-01 15:48:28');
INSERT INTO `using_log` VALUES (818, 1, 'Solution', '', '2013-07-01 15:48:29');
INSERT INTO `using_log` VALUES (819, 1, 'Solution', '', '2013-07-01 15:48:33');
INSERT INTO `using_log` VALUES (820, 1, 'Solution', '', '2013-07-01 15:48:37');
INSERT INTO `using_log` VALUES (821, 1, 'Solution', '', '2013-07-01 15:50:37');
INSERT INTO `using_log` VALUES (822, 1, 'Admin', '', '2013-07-01 16:24:37');
INSERT INTO `using_log` VALUES (823, 1, 'Equipment', '', '2013-07-01 16:24:39');
INSERT INTO `using_log` VALUES (824, 1, 'equipment', 'create', '2013-07-01 16:24:41');
INSERT INTO `using_log` VALUES (825, 1, 'Equipment', '', '2013-07-01 16:24:52');
INSERT INTO `using_log` VALUES (826, 1, 'equipment', 'create', '2013-07-01 16:24:58');
INSERT INTO `using_log` VALUES (827, 1, 'Equipment', '', '2013-07-01 16:25:01');
INSERT INTO `using_log` VALUES (828, 1, 'equipment', 'create', '2013-07-01 16:25:05');
INSERT INTO `using_log` VALUES (829, 1, 'Equipment', '', '2013-07-01 16:25:28');
INSERT INTO `using_log` VALUES (830, 1, 'equipment', '', '2013-07-01 16:25:42');
INSERT INTO `using_log` VALUES (831, 1, 'equipment', '', '2013-07-01 16:25:47');
INSERT INTO `using_log` VALUES (832, 1, 'equipment', 'index', '2013-07-01 16:25:52');
INSERT INTO `using_log` VALUES (833, 1, 'equipment', 'index', '2013-07-01 16:25:53');
INSERT INTO `using_log` VALUES (834, 1, 'equipment', 'index', '2013-07-01 16:25:54');
INSERT INTO `using_log` VALUES (835, 1, 'equipment', 'index', '2013-07-01 16:25:54');
INSERT INTO `using_log` VALUES (836, 1, 'equipment', 'index', '2013-07-01 16:25:55');
INSERT INTO `using_log` VALUES (837, 1, 'equipment', 'index', '2013-07-01 16:25:56');
INSERT INTO `using_log` VALUES (838, 1, 'equipment', 'index', '2013-07-01 16:25:56');
INSERT INTO `using_log` VALUES (839, 1, 'equipment', 'index', '2013-07-01 16:25:57');
INSERT INTO `using_log` VALUES (840, 1, 'equipment', 'index', '2013-07-01 16:25:58');
INSERT INTO `using_log` VALUES (841, 1, 'equipment', 'index', '2013-07-01 16:25:58');
INSERT INTO `using_log` VALUES (842, 1, 'equipment', 'index', '2013-07-01 16:26:00');
INSERT INTO `using_log` VALUES (843, 1, 'equipment', 'index', '2013-07-01 16:26:02');
INSERT INTO `using_log` VALUES (844, 1, 'equipment', 'index', '2013-07-01 16:26:02');
INSERT INTO `using_log` VALUES (845, 1, 'equipment', 'index', '2013-07-01 16:26:03');
INSERT INTO `using_log` VALUES (846, 1, 'equipment', 'index', '2013-07-01 16:26:05');
INSERT INTO `using_log` VALUES (847, 1, 'equipment', 'index', '2013-07-01 16:26:07');
INSERT INTO `using_log` VALUES (848, 1, 'equipment', 'index', '2013-07-01 16:26:08');
INSERT INTO `using_log` VALUES (849, 1, 'equipment', 'index', '2013-07-01 16:26:09');
INSERT INTO `using_log` VALUES (850, 1, 'equipment', 'index', '2013-07-01 16:26:13');
INSERT INTO `using_log` VALUES (851, 1, 'equipment', 'index', '2013-07-01 16:26:13');
INSERT INTO `using_log` VALUES (852, 1, 'equipment', '', '2013-07-01 16:26:18');
INSERT INTO `using_log` VALUES (853, 1, 'equipment', '', '2013-07-01 16:26:30');
INSERT INTO `using_log` VALUES (854, 1, 'equipment', '', '2013-07-01 16:26:34');
INSERT INTO `using_log` VALUES (855, 1, 'equipment', '', '2013-07-01 16:26:35');
INSERT INTO `using_log` VALUES (856, 1, 'Equipment', '', '2013-07-01 16:26:35');
INSERT INTO `using_log` VALUES (857, 1, 'equipment', 'create', '2013-07-01 16:26:36');
INSERT INTO `using_log` VALUES (858, 1, 'Equipment', '', '2013-07-01 16:26:51');
INSERT INTO `using_log` VALUES (859, 1, 'equipment', '', '2013-07-01 16:26:58');
INSERT INTO `using_log` VALUES (860, 1, 'equipment', '', '2013-07-01 16:27:05');
INSERT INTO `using_log` VALUES (861, 1, 'equipment', '', '2013-07-01 16:27:16');
INSERT INTO `using_log` VALUES (862, 1, 'equipment', 'index', '2013-07-01 16:27:23');
INSERT INTO `using_log` VALUES (863, 1, 'equipment', 'index', '2013-07-01 16:27:24');
INSERT INTO `using_log` VALUES (864, 1, 'equipment', 'index', '2013-07-01 16:27:25');
INSERT INTO `using_log` VALUES (865, 1, 'equipment', 'index', '2013-07-01 16:27:25');
INSERT INTO `using_log` VALUES (866, 1, 'equipment', 'index', '2013-07-01 16:27:26');
INSERT INTO `using_log` VALUES (867, 1, 'equipment', 'index', '2013-07-01 16:27:26');
INSERT INTO `using_log` VALUES (868, 1, 'equipment', 'index', '2013-07-01 16:27:26');
INSERT INTO `using_log` VALUES (869, 1, 'equipment', 'index', '2013-07-01 16:27:27');
INSERT INTO `using_log` VALUES (870, 1, 'equipment', 'index', '2013-07-01 16:27:27');
INSERT INTO `using_log` VALUES (871, 1, 'equipment', 'index', '2013-07-01 16:27:28');
INSERT INTO `using_log` VALUES (872, 1, 'equipment', 'index', '2013-07-01 16:27:30');
INSERT INTO `using_log` VALUES (873, 1, 'equipment', 'index', '2013-07-01 16:27:31');
INSERT INTO `using_log` VALUES (874, 1, 'equipment', 'index', '2013-07-01 16:27:32');
INSERT INTO `using_log` VALUES (875, 1, 'equipment', 'index', '2013-07-01 16:27:32');
INSERT INTO `using_log` VALUES (876, 1, 'equipment', 'index', '2013-07-01 16:27:33');
INSERT INTO `using_log` VALUES (877, 1, 'equipment', 'index', '2013-07-01 16:27:33');
INSERT INTO `using_log` VALUES (878, 1, 'equipment', 'update', '2013-07-01 16:27:33');
INSERT INTO `using_log` VALUES (879, 1, 'Equipment', '', '2013-07-01 16:27:35');
INSERT INTO `using_log` VALUES (880, 1, 'equipment', 'index', '2013-07-01 16:27:37');
INSERT INTO `using_log` VALUES (881, 1, 'equipment', 'index', '2013-07-01 16:27:39');
INSERT INTO `using_log` VALUES (882, 1, 'equipment', 'index', '2013-07-01 16:27:40');
INSERT INTO `using_log` VALUES (883, 1, 'equipment', 'index', '2013-07-01 16:27:41');
INSERT INTO `using_log` VALUES (884, 1, 'equipment', 'index', '2013-07-01 16:27:43');
INSERT INTO `using_log` VALUES (885, 1, 'equipment', 'index', '2013-07-01 16:27:48');
INSERT INTO `using_log` VALUES (886, 1, 'equipment', 'index', '2013-07-01 16:27:49');
INSERT INTO `using_log` VALUES (887, 1, 'equipment', 'index', '2013-07-01 16:27:52');
INSERT INTO `using_log` VALUES (888, 1, 'equipment', 'index', '2013-07-01 16:27:54');
INSERT INTO `using_log` VALUES (889, 1, 'equipment', 'index', '2013-07-01 16:27:55');
INSERT INTO `using_log` VALUES (890, 1, 'equipment', 'index', '2013-07-01 16:27:58');
INSERT INTO `using_log` VALUES (891, 1, 'equipment', 'index', '2013-07-01 16:28:00');
INSERT INTO `using_log` VALUES (892, 1, 'equipment', 'index', '2013-07-01 16:28:01');
INSERT INTO `using_log` VALUES (893, 1, 'equipment', 'update', '2013-07-01 16:28:05');
INSERT INTO `using_log` VALUES (894, 1, 'Equipment', '', '2013-07-01 16:28:08');
INSERT INTO `using_log` VALUES (895, 1, 'equipment', 'index', '2013-07-01 16:28:11');
INSERT INTO `using_log` VALUES (896, 1, 'equipment', 'index', '2013-07-01 16:28:12');
INSERT INTO `using_log` VALUES (897, 1, 'equipment', 'update', '2013-07-01 16:28:55');
INSERT INTO `using_log` VALUES (898, 1, 'Equipment', '', '2013-07-01 16:31:47');
INSERT INTO `using_log` VALUES (899, 1, 'Equipment', '', '2013-07-01 16:31:49');
INSERT INTO `using_log` VALUES (900, 1, 'equipment', 'index', '2013-07-01 16:31:53');
INSERT INTO `using_log` VALUES (901, 1, 'equipment', 'index', '2013-07-01 16:31:54');
INSERT INTO `using_log` VALUES (902, 1, 'equipment', 'view', '2013-07-01 16:31:57');
INSERT INTO `using_log` VALUES (903, 1, 'equipment', 'update', '2013-07-01 16:32:00');
INSERT INTO `using_log` VALUES (904, 1, 'Equipment', '', '2013-07-01 16:32:03');
INSERT INTO `using_log` VALUES (905, 1, 'equipment', 'index', '2013-07-01 16:32:05');
INSERT INTO `using_log` VALUES (906, 1, 'equipment', 'index', '2013-07-01 16:32:06');
INSERT INTO `using_log` VALUES (907, 1, 'equipment', 'view', '2013-07-01 16:32:08');
INSERT INTO `using_log` VALUES (908, 1, 'equipment', '', '2013-07-01 16:32:10');
INSERT INTO `using_log` VALUES (909, 1, 'equipment', 'index', '2013-07-01 16:32:12');
INSERT INTO `using_log` VALUES (910, 1, 'equipment', 'index', '2013-07-01 16:32:13');
INSERT INTO `using_log` VALUES (911, 1, 'equipment', 'view', '2013-07-01 16:32:15');
INSERT INTO `using_log` VALUES (912, 1, 'equipment', '', '2013-07-01 16:32:18');
INSERT INTO `using_log` VALUES (913, 1, 'equipment', 'index', '2013-07-01 16:32:30');
INSERT INTO `using_log` VALUES (914, 1, 'equipment', 'index', '2013-07-01 16:32:31');
INSERT INTO `using_log` VALUES (915, 1, 'equipment', 'delete', '2013-07-01 16:32:34');
INSERT INTO `using_log` VALUES (916, 1, 'equipment', 'index', '2013-07-01 16:32:34');
INSERT INTO `using_log` VALUES (917, 1, 'equipment', '', '2013-07-01 16:32:44');
INSERT INTO `using_log` VALUES (918, 1, 'equipment', 'index', '2013-07-01 16:32:50');
INSERT INTO `using_log` VALUES (919, 1, 'Solution', '', '2013-07-01 16:32:57');
INSERT INTO `using_log` VALUES (920, 1, 'Solution', '', '2013-07-01 16:33:00');
INSERT INTO `using_log` VALUES (921, 1, 'Admin', '', '2013-07-01 16:33:08');
INSERT INTO `using_log` VALUES (922, 1, 'Equipment', '', '2013-07-01 16:33:13');
INSERT INTO `using_log` VALUES (923, 1, 'equipment', 'index', '2013-07-01 16:33:16');
INSERT INTO `using_log` VALUES (924, 1, 'equipment', 'index', '2013-07-01 16:33:20');
INSERT INTO `using_log` VALUES (925, 1, 'equipment', 'index', '2013-07-01 16:33:22');
INSERT INTO `using_log` VALUES (926, 1, 'equipment', 'index', '2013-07-01 16:33:24');
INSERT INTO `using_log` VALUES (927, 1, 'equipment', 'index', '2013-07-01 16:33:26');
INSERT INTO `using_log` VALUES (928, 1, 'equipment', 'index', '2013-07-01 16:33:32');
INSERT INTO `using_log` VALUES (929, 1, 'equipment', 'update', '2013-07-01 16:33:43');
INSERT INTO `using_log` VALUES (930, 1, 'Equipment', '', '2013-07-01 16:34:17');
INSERT INTO `using_log` VALUES (931, 1, 'Solution', '', '2013-07-01 16:34:21');
INSERT INTO `using_log` VALUES (932, 1, 'Solution', '', '2013-07-01 16:34:25');
INSERT INTO `using_log` VALUES (933, 1, 'Solution', '', '2013-07-01 16:39:31');
INSERT INTO `using_log` VALUES (934, 1, 'Admin', '', '2013-07-01 16:39:35');
INSERT INTO `using_log` VALUES (935, 1, 'Equipment', '', '2013-07-01 16:39:37');
INSERT INTO `using_log` VALUES (936, 1, 'equipment', 'index', '2013-07-01 16:39:40');
INSERT INTO `using_log` VALUES (937, 1, 'equipment', 'update', '2013-07-01 16:39:46');
INSERT INTO `using_log` VALUES (938, 1, 'Equipment', '', '2013-07-01 17:01:27');
INSERT INTO `using_log` VALUES (939, 1, 'Solution', '', '2013-07-01 17:01:29');
INSERT INTO `using_log` VALUES (940, 1, 'Solution', '', '2013-07-01 17:02:08');
INSERT INTO `using_log` VALUES (941, 1, 'Solution', '', '2013-07-01 17:02:12');
INSERT INTO `using_log` VALUES (942, 1, 'Solution', '', '2013-07-01 17:06:26');
INSERT INTO `using_log` VALUES (943, 1, 'RequestBooking', '', '2013-07-01 17:08:34');
INSERT INTO `using_log` VALUES (944, 1, 'RequestBooking', 'Request', '2013-07-01 17:08:36');
INSERT INTO `using_log` VALUES (945, 1, 'RequestBooking', 'View', '2013-07-01 17:09:09');
INSERT INTO `using_log` VALUES (946, 1, 'RequestBooking', '', '2013-07-01 17:09:11');
INSERT INTO `using_log` VALUES (947, NULL, 'management', 'login', '2013-07-01 17:09:19');
INSERT INTO `using_log` VALUES (948, NULL, 'management', 'login', '2013-07-01 17:09:25');
INSERT INTO `using_log` VALUES (949, NULL, 'management', 'login', '2013-07-01 17:09:30');
INSERT INTO `using_log` VALUES (950, NULL, 'management', 'login', '2013-07-01 17:09:35');
INSERT INTO `using_log` VALUES (951, 31, 'RequestBooking', '', '2013-07-01 17:10:09');
INSERT INTO `using_log` VALUES (952, 31, 'RequestBooking', 'Request', '2013-07-01 17:10:12');
INSERT INTO `using_log` VALUES (953, 31, 'RequestBooking', 'View', '2013-07-01 17:10:35');
INSERT INTO `using_log` VALUES (954, 31, 'RequestBooking', '', '2013-07-01 17:10:38');
INSERT INTO `using_log` VALUES (955, 31, 'RequestBooking', 'Request', '2013-07-01 17:10:54');
INSERT INTO `using_log` VALUES (956, 31, 'RequestBooking', '', '2013-07-01 17:10:55');
INSERT INTO `using_log` VALUES (957, NULL, 'management', 'login', '2013-07-01 17:10:59');
INSERT INTO `using_log` VALUES (958, 1, 'RequestBooking', '', '2013-07-01 17:11:05');
INSERT INTO `using_log` VALUES (959, 1, '', '', '2013-07-01 17:13:02');
INSERT INTO `using_log` VALUES (960, 1, 'RequestBooking', '', '2013-07-01 17:13:03');
INSERT INTO `using_log` VALUES (961, 1, 'RequestBooking', 'Request', '2013-07-01 17:13:04');
INSERT INTO `using_log` VALUES (962, 1, 'RequestBooking', '', '2013-07-01 17:13:06');
INSERT INTO `using_log` VALUES (963, NULL, 'management', 'login', '2013-07-01 17:14:09');
INSERT INTO `using_log` VALUES (964, 5, 'RequestBooking', '', '2013-07-01 17:14:13');
INSERT INTO `using_log` VALUES (965, 5, 'RequestBooking', 'Request', '2013-07-01 17:14:18');
INSERT INTO `using_log` VALUES (966, 5, 'Management', 'EditProfile', '2013-07-01 17:14:22');
INSERT INTO `using_log` VALUES (967, 5, 'Management', 'EditProfile', '2013-07-01 17:14:34');
INSERT INTO `using_log` VALUES (968, 5, 'Management', 'EditProfile', '2013-07-01 17:14:51');
INSERT INTO `using_log` VALUES (969, 5, 'Management', 'EditProfile', '2013-07-01 17:18:09');
INSERT INTO `using_log` VALUES (970, 5, 'Management', 'EditProfile', '2013-07-01 17:18:12');
INSERT INTO `using_log` VALUES (971, 5, 'Management', 'EditProfile', '2013-07-01 17:18:28');
INSERT INTO `using_log` VALUES (972, NULL, 'management', 'login', '2013-07-01 17:18:33');
INSERT INTO `using_log` VALUES (973, 1, 'RequestBooking', '', '2013-07-01 17:18:39');
INSERT INTO `using_log` VALUES (974, NULL, 'management', 'login', '2013-07-01 17:18:46');
INSERT INTO `using_log` VALUES (975, NULL, 'management', 'login', '2013-07-01 17:18:51');
INSERT INTO `using_log` VALUES (976, NULL, 'management', 'login', '2013-07-01 17:18:57');
INSERT INTO `using_log` VALUES (977, 5, 'RequestBooking', '', '2013-07-01 17:19:15');
INSERT INTO `using_log` VALUES (978, 5, 'RequestBooking', 'Request', '2013-07-01 17:19:17');
INSERT INTO `using_log` VALUES (979, 5, 'RequestBooking', 'Request', '2013-07-01 17:19:30');
INSERT INTO `using_log` VALUES (980, 5, 'RequestBooking', 'View', '2013-07-01 17:19:50');
INSERT INTO `using_log` VALUES (981, 5, 'RequestBooking', '', '2013-07-01 17:19:51');
INSERT INTO `using_log` VALUES (982, NULL, 'management', 'login', '2013-07-01 17:19:53');
INSERT INTO `using_log` VALUES (983, 1, 'RequestBooking', '', '2013-07-01 17:19:59');
INSERT INTO `using_log` VALUES (984, 1, 'RequestBooking', 'Request', '2013-07-01 17:25:49');
INSERT INTO `using_log` VALUES (985, 1, 'RequestBooking', '', '2013-07-01 17:25:51');
INSERT INTO `using_log` VALUES (986, 1, 'requestBooking', 'update', '2013-07-01 17:26:02');
INSERT INTO `using_log` VALUES (987, 1, 'RequestBooking', '', '2013-07-01 17:26:06');
INSERT INTO `using_log` VALUES (988, NULL, 'webservice', '', '2013-07-02 10:16:52');
INSERT INTO `using_log` VALUES (989, NULL, 'webservice', 'ShowRequest', '2013-07-02 10:17:00');
INSERT INTO `using_log` VALUES (990, NULL, '', '', '2013-07-02 11:00:02');
INSERT INTO `using_log` VALUES (991, 1, 'RequestBooking', '', '2013-07-02 11:00:06');
INSERT INTO `using_log` VALUES (992, 1, 'RequestBooking', 'Request', '2013-07-02 11:00:08');
INSERT INTO `using_log` VALUES (993, 1, 'RequestBooking', 'Request', '2013-07-02 11:00:18');
INSERT INTO `using_log` VALUES (994, 1, 'RequestBooking', 'View', '2013-07-02 11:00:38');
INSERT INTO `using_log` VALUES (995, 1, 'RequestBooking', '', '2013-07-02 11:00:40');
INSERT INTO `using_log` VALUES (996, 1, 'WebService', 'ShowRequest', '2013-07-02 11:01:58');
INSERT INTO `using_log` VALUES (997, 1, '', '', '2013-07-02 12:32:47');
INSERT INTO `using_log` VALUES (998, 1, 'Management', 'EditProfile', '2013-07-02 12:32:50');
INSERT INTO `using_log` VALUES (999, 1, 'Solution', '', '2013-07-02 12:37:35');
INSERT INTO `using_log` VALUES (1000, 1, 'Solution', '', '2013-07-02 12:38:48');
INSERT INTO `using_log` VALUES (1001, 1, 'Solution', '', '2013-07-02 12:38:50');
INSERT INTO `using_log` VALUES (1002, 1, 'Solution', '', '2013-07-02 12:39:13');
INSERT INTO `using_log` VALUES (1003, 1, 'Solution', '', '2013-07-02 12:49:31');
INSERT INTO `using_log` VALUES (1004, 1, 'Solution', '', '2013-07-02 12:49:46');
INSERT INTO `using_log` VALUES (1005, 1, 'Solution', '', '2013-07-02 12:53:20');
INSERT INTO `using_log` VALUES (1006, 1, 'Solution', '', '2013-07-02 12:53:40');
INSERT INTO `using_log` VALUES (1007, 1, 'solution', 'index', '2013-07-02 12:53:57');
INSERT INTO `using_log` VALUES (1008, 1, 'solution', 'index', '2013-07-02 12:53:59');
INSERT INTO `using_log` VALUES (1009, 1, 'solution', 'index', '2013-07-02 12:54:01');
INSERT INTO `using_log` VALUES (1010, 1, 'solution', 'index', '2013-07-02 12:54:05');
INSERT INTO `using_log` VALUES (1011, 1, 'solution', 'index', '2013-07-02 12:54:14');
INSERT INTO `using_log` VALUES (1012, 1, 'Solution', '', '2013-07-02 12:54:18');
INSERT INTO `using_log` VALUES (1013, 1, 'Solution', '', '2013-07-02 13:00:29');
INSERT INTO `using_log` VALUES (1014, 1, 'Solution', '', '2013-07-02 13:00:58');
INSERT INTO `using_log` VALUES (1015, 1, 'Solution', '', '2013-07-02 13:01:40');
INSERT INTO `using_log` VALUES (1016, NULL, '', '', '2013-07-02 13:38:49');
INSERT INTO `using_log` VALUES (1017, 1, 'RequestBooking', '', '2013-07-02 13:39:06');
INSERT INTO `using_log` VALUES (1018, 1, 'Solution', '', '2013-07-02 13:39:30');
INSERT INTO `using_log` VALUES (1019, 1, 'Solution', '', '2013-07-02 13:40:04');
INSERT INTO `using_log` VALUES (1020, 1, 'Solution', '', '2013-07-02 13:42:01');
INSERT INTO `using_log` VALUES (1021, 1, 'Solution', '', '2013-07-02 13:42:14');
INSERT INTO `using_log` VALUES (1022, 1, 'Solution', '', '2013-07-02 13:42:24');
INSERT INTO `using_log` VALUES (1023, 1, 'Solution', '', '2013-07-02 13:42:45');
INSERT INTO `using_log` VALUES (1024, 1, 'Solution', '', '2013-07-02 13:49:25');
INSERT INTO `using_log` VALUES (1025, 1, 'Solution', '', '2013-07-02 13:50:18');
INSERT INTO `using_log` VALUES (1026, 1, 'Solution', '', '2013-07-02 13:52:35');
INSERT INTO `using_log` VALUES (1027, 1, 'Solution', '', '2013-07-02 13:52:51');
INSERT INTO `using_log` VALUES (1028, 1, 'Solution', '', '2013-07-02 13:53:31');
INSERT INTO `using_log` VALUES (1029, 1, 'Solution', '', '2013-07-02 13:54:08');
INSERT INTO `using_log` VALUES (1030, 1, 'Solution', '', '2013-07-02 13:54:31');
INSERT INTO `using_log` VALUES (1031, 1, 'Solution', '', '2013-07-02 13:55:58');
INSERT INTO `using_log` VALUES (1032, 1, 'Solution', '', '2013-07-02 13:56:39');
INSERT INTO `using_log` VALUES (1033, 1, 'Solution', '', '2013-07-02 13:57:07');
INSERT INTO `using_log` VALUES (1034, 1, 'Solution', '', '2013-07-02 13:57:41');
INSERT INTO `using_log` VALUES (1035, 1, 'Solution', '', '2013-07-02 13:58:13');
INSERT INTO `using_log` VALUES (1036, 1, 'Solution', '', '2013-07-02 13:59:05');
INSERT INTO `using_log` VALUES (1037, 1, 'Solution', '', '2013-07-02 14:00:15');
INSERT INTO `using_log` VALUES (1038, 1, 'Management', 'Ping', '2013-07-02 14:00:17');
INSERT INTO `using_log` VALUES (1039, 1, 'Solution', '', '2013-07-02 14:01:20');
INSERT INTO `using_log` VALUES (1040, 1, 'Management', 'Ping', '2013-07-02 14:01:21');
INSERT INTO `using_log` VALUES (1041, 1, 'Solution', '', '2013-07-02 14:01:59');
INSERT INTO `using_log` VALUES (1042, 1, 'Management', 'Ping', '2013-07-02 14:02:00');
INSERT INTO `using_log` VALUES (1043, 1, 'Management', 'Ping', '2013-07-02 14:02:05');
INSERT INTO `using_log` VALUES (1044, 1, 'Solution', '', '2013-07-02 14:03:30');
INSERT INTO `using_log` VALUES (1045, 1, 'Management', 'Ping', '2013-07-02 14:03:31');
INSERT INTO `using_log` VALUES (1046, 1, 'Solution', '', '2013-07-02 14:03:56');
INSERT INTO `using_log` VALUES (1047, 1, 'Management', 'Ping', '2013-07-02 14:03:59');
INSERT INTO `using_log` VALUES (1048, 1, 'Solution', '', '2013-07-02 14:04:27');
INSERT INTO `using_log` VALUES (1049, 1, 'Management', 'Ping', '2013-07-02 14:04:29');
INSERT INTO `using_log` VALUES (1050, 1, 'Management', 'Ping', '2013-07-02 14:04:57');
INSERT INTO `using_log` VALUES (1051, 1, 'Management', 'Ping', '2013-07-02 14:05:30');
INSERT INTO `using_log` VALUES (1052, 1, 'Management', 'Ping', '2013-07-02 14:05:37');
INSERT INTO `using_log` VALUES (1053, 1, 'Management', 'Ping', '2013-07-02 14:05:51');
INSERT INTO `using_log` VALUES (1054, 1, 'Solution', '', '2013-07-02 14:06:10');
INSERT INTO `using_log` VALUES (1055, 1, 'Solution', '', '2013-07-02 14:06:42');
INSERT INTO `using_log` VALUES (1056, 1, 'Solution', '', '2013-07-02 14:11:23');
INSERT INTO `using_log` VALUES (1057, 1, 'Solution', '', '2013-07-02 14:11:39');
INSERT INTO `using_log` VALUES (1058, 1, 'Management', 'Ping', '2013-07-02 14:11:41');
INSERT INTO `using_log` VALUES (1059, 1, 'Solution', '', '2013-07-02 14:12:24');
INSERT INTO `using_log` VALUES (1060, 1, 'Management', 'Ping', '2013-07-02 14:12:25');
INSERT INTO `using_log` VALUES (1061, 1, 'Solution', '', '2013-07-02 14:18:45');
INSERT INTO `using_log` VALUES (1062, 1, 'Management', 'Ping', '2013-07-02 14:18:46');
INSERT INTO `using_log` VALUES (1063, 1, 'Solution', '', '2013-07-02 14:20:04');
INSERT INTO `using_log` VALUES (1064, 1, 'Management', 'Ping', '2013-07-02 14:20:06');
INSERT INTO `using_log` VALUES (1065, 1, 'Solution', '', '2013-07-02 14:20:24');
INSERT INTO `using_log` VALUES (1066, 1, 'Solution', '', '2013-07-02 14:22:20');
INSERT INTO `using_log` VALUES (1067, 1, 'Admin', '', '2013-07-02 14:22:53');
INSERT INTO `using_log` VALUES (1068, 1, 'Equipment', '', '2013-07-02 14:22:54');
INSERT INTO `using_log` VALUES (1069, 1, 'equipment', '', '2013-07-02 14:23:00');
INSERT INTO `using_log` VALUES (1070, 1, 'equipment', 'index', '2013-07-02 14:23:07');
INSERT INTO `using_log` VALUES (1071, 1, 'equipment', 'index', '2013-07-02 14:23:10');
INSERT INTO `using_log` VALUES (1072, 1, 'equipment', 'index', '2013-07-02 14:23:12');
INSERT INTO `using_log` VALUES (1073, 1, 'equipment', 'index', '2013-07-02 14:23:14');
INSERT INTO `using_log` VALUES (1074, 1, 'equipment', 'index', '2013-07-02 14:23:17');
INSERT INTO `using_log` VALUES (1075, 1, 'equipment', 'index', '2013-07-02 14:23:19');
INSERT INTO `using_log` VALUES (1076, 1, 'equipment', 'index', '2013-07-02 14:23:20');
INSERT INTO `using_log` VALUES (1077, 1, 'equipment', 'index', '2013-07-02 14:23:23');
INSERT INTO `using_log` VALUES (1078, 1, 'equipment', 'index', '2013-07-02 14:23:25');
INSERT INTO `using_log` VALUES (1079, 1, 'equipment', 'update', '2013-07-02 14:23:30');
INSERT INTO `using_log` VALUES (1080, 1, 'Equipment', '', '2013-07-02 14:23:38');
INSERT INTO `using_log` VALUES (1081, 1, 'Solution', '', '2013-07-02 14:23:40');
INSERT INTO `using_log` VALUES (1082, 1, 'Solution', '', '2013-07-02 14:23:43');
INSERT INTO `using_log` VALUES (1083, 1, 'Solution', '', '2013-07-02 14:24:21');
INSERT INTO `using_log` VALUES (1084, 1, 'Solution', '', '2013-07-02 14:25:04');
INSERT INTO `using_log` VALUES (1085, 1, 'Solution', '', '2013-07-02 14:25:23');
INSERT INTO `using_log` VALUES (1086, 1, 'Management', 'Ping', '2013-07-02 14:25:25');
INSERT INTO `using_log` VALUES (1087, 1, 'Solution', '', '2013-07-02 14:25:43');
INSERT INTO `using_log` VALUES (1088, 1, 'Management', 'Ping', '2013-07-02 14:25:45');
INSERT INTO `using_log` VALUES (1089, 1, 'Solution', '', '2013-07-02 14:25:57');
INSERT INTO `using_log` VALUES (1090, 1, 'Management', 'Ping', '2013-07-02 14:25:59');
INSERT INTO `using_log` VALUES (1091, 1, 'Solution', '', '2013-07-02 14:26:32');
INSERT INTO `using_log` VALUES (1092, 1, 'Management', 'Ping', '2013-07-02 14:26:53');
INSERT INTO `using_log` VALUES (1093, 1, 'Solution', '', '2013-07-02 14:27:50');
INSERT INTO `using_log` VALUES (1094, 1, 'Solution', '', '2013-07-02 14:28:13');
INSERT INTO `using_log` VALUES (1095, NULL, '', '', '2013-07-03 17:10:17');
INSERT INTO `using_log` VALUES (1096, 1, 'RequestBooking', '', '2013-07-03 17:10:21');
INSERT INTO `using_log` VALUES (1097, 1, 'Admin', '', '2013-07-03 17:10:25');
INSERT INTO `using_log` VALUES (1098, 1, 'Equipment', '', '2013-07-03 17:10:27');
INSERT INTO `using_log` VALUES (1099, 1, 'Equipment', '', '2013-07-03 17:10:30');
INSERT INTO `using_log` VALUES (1100, 1, 'Solution', '', '2013-07-03 17:10:40');
INSERT INTO `using_log` VALUES (1101, 1, 'Admin', '', '2013-07-03 17:10:45');
INSERT INTO `using_log` VALUES (1102, 1, 'RequestBooking', '', '2013-07-03 17:10:48');
INSERT INTO `using_log` VALUES (1103, 1, 'RequestBooking', 'CheckStatus', '2013-07-03 17:10:53');
INSERT INTO `using_log` VALUES (1104, 1, 'RequestBorrow', '', '2013-07-03 17:10:56');
INSERT INTO `using_log` VALUES (1105, 1, 'RequestBooking', '', '2013-07-03 17:41:33');
INSERT INTO `using_log` VALUES (1106, 1, 'RequestBorrow', '', '2013-07-03 17:41:35');
INSERT INTO `using_log` VALUES (1107, 1, 'RequestService', '', '2013-07-03 17:41:36');
INSERT INTO `using_log` VALUES (1108, 1, 'Solution', '', '2013-07-03 17:41:37');
INSERT INTO `using_log` VALUES (1109, 1, 'Solution', '', '2013-07-03 17:41:42');
INSERT INTO `using_log` VALUES (1110, 1, 'Report', '', '2013-07-03 17:41:46');
INSERT INTO `using_log` VALUES (1111, 1, 'Report', 'RequestBookingStatisticReport', '2013-07-03 17:41:48');
INSERT INTO `using_log` VALUES (1112, 1, 'Admin', '', '2013-07-03 17:41:50');
INSERT INTO `using_log` VALUES (1113, 1, 'Role', '', '2013-07-03 17:41:52');
INSERT INTO `using_log` VALUES (1114, 1, 'Role', '', '2013-07-03 17:41:55');
INSERT INTO `using_log` VALUES (1115, NULL, '', '', '2013-07-05 21:26:31');
INSERT INTO `using_log` VALUES (1116, 1, 'RequestBooking', '', '2013-07-05 21:26:40');
INSERT INTO `using_log` VALUES (1117, 1, 'RequestBooking', 'Request', '2013-07-05 21:26:55');
INSERT INTO `using_log` VALUES (1118, 1, 'RequestBooking', '', '2013-07-05 21:26:59');
INSERT INTO `using_log` VALUES (1119, 1, 'RequestBooking', 'Request', '2013-07-05 21:27:02');
INSERT INTO `using_log` VALUES (1120, 1, 'RequestBooking', 'View', '2013-07-05 21:27:32');
INSERT INTO `using_log` VALUES (1121, 1, 'RequestBooking', '', '2013-07-05 21:27:34');
INSERT INTO `using_log` VALUES (1122, 1, 'RequestBooking', 'Index', '2013-07-05 21:27:42');
INSERT INTO `using_log` VALUES (1123, NULL, 'management', 'login', '2013-07-05 21:27:51');
INSERT INTO `using_log` VALUES (1124, NULL, 'management', 'login', '2013-07-05 21:28:01');
INSERT INTO `using_log` VALUES (1125, 2, 'RequestBooking', '', '2013-07-05 21:28:10');
INSERT INTO `using_log` VALUES (1126, 2, 'RequestBooking', 'Request', '2013-07-05 21:28:13');
INSERT INTO `using_log` VALUES (1127, 2, 'RequestBooking', 'View', '2013-07-05 21:28:31');
INSERT INTO `using_log` VALUES (1128, 2, 'RequestBooking', '', '2013-07-05 21:28:33');
INSERT INTO `using_log` VALUES (1129, 2, 'RequestBooking', 'Index', '2013-07-05 21:28:36');
INSERT INTO `using_log` VALUES (1130, 2, 'Admin', '', '2013-07-05 21:30:52');
INSERT INTO `using_log` VALUES (1131, 2, 'RequestBooking', '', '2013-07-05 21:31:00');
INSERT INTO `using_log` VALUES (1132, NULL, 'management', 'login', '2013-07-05 21:31:16');
INSERT INTO `using_log` VALUES (1133, NULL, 'management', 'login', '2013-07-05 21:31:23');
INSERT INTO `using_log` VALUES (1134, NULL, 'management', 'login', '2013-07-05 21:31:31');
INSERT INTO `using_log` VALUES (1135, 21, 'RequestBooking', '', '2013-07-05 21:32:37');
INSERT INTO `using_log` VALUES (1136, 21, 'RequestBooking', 'Request', '2013-07-05 21:32:40');
INSERT INTO `using_log` VALUES (1137, 21, 'RequestBooking', 'Request', '2013-07-05 21:32:52');
INSERT INTO `using_log` VALUES (1138, 21, 'RequestBooking', 'Request', '2013-07-05 21:33:05');
INSERT INTO `using_log` VALUES (1139, 21, 'RequestBooking', 'View', '2013-07-05 21:33:30');
INSERT INTO `using_log` VALUES (1140, 21, 'RequestBooking', '', '2013-07-05 21:33:32');
INSERT INTO `using_log` VALUES (1141, 21, 'RequestBooking', 'Index', '2013-07-05 21:33:35');
INSERT INTO `using_log` VALUES (1142, 21, 'Management', 'EditProfile', '2013-07-05 21:33:48');
INSERT INTO `using_log` VALUES (1143, 21, 'Management', 'EditProfile', '2013-07-05 21:33:54');
INSERT INTO `using_log` VALUES (1144, NULL, 'management', 'login', '2013-07-05 21:35:21');
INSERT INTO `using_log` VALUES (1145, NULL, 'management', 'login', '2013-07-05 21:35:28');
INSERT INTO `using_log` VALUES (1146, 1, 'RequestBooking', '', '2013-07-05 21:35:35');
INSERT INTO `using_log` VALUES (1147, 1, 'RequestBooking', 'Index', '2013-07-05 21:35:48');
INSERT INTO `using_log` VALUES (1148, 1, 'RequestBooking', '', '2013-07-05 21:38:41');
INSERT INTO `using_log` VALUES (1149, 1, 'RequestBooking', 'Index', '2013-07-05 21:38:44');
INSERT INTO `using_log` VALUES (1150, 1, '', '', '2013-07-05 22:02:39');
INSERT INTO `using_log` VALUES (1151, 1, 'RequestBooking', '', '2013-07-05 22:02:42');
INSERT INTO `using_log` VALUES (1152, 1, 'RequestBooking', 'Index', '2013-07-05 22:02:45');
INSERT INTO `using_log` VALUES (1153, 1, 'RequestBooking', 'Index', '2013-07-05 22:02:49');
INSERT INTO `using_log` VALUES (1154, 1, 'RequestBooking', '', '2013-07-05 22:02:53');
INSERT INTO `using_log` VALUES (1155, 1, 'RequestBooking', 'Index', '2013-07-05 22:02:56');
INSERT INTO `using_log` VALUES (1156, 1, 'RequestBooking', 'Index', '2013-07-05 22:03:01');
INSERT INTO `using_log` VALUES (1157, 1, 'RequestBooking', '', '2013-07-05 22:05:15');
INSERT INTO `using_log` VALUES (1158, 1, 'RequestBooking', 'Index', '2013-07-05 22:05:18');
INSERT INTO `using_log` VALUES (1159, 1, 'RequestBooking', 'Index', '2013-07-05 22:05:20');
INSERT INTO `using_log` VALUES (1160, 1, 'RequestBooking', 'Index', '2013-07-05 22:05:24');
INSERT INTO `using_log` VALUES (1161, 1, 'RequestBooking', '', '2013-07-05 22:12:09');
INSERT INTO `using_log` VALUES (1162, 1, 'RequestBooking', 'Index', '2013-07-05 22:12:15');
INSERT INTO `using_log` VALUES (1163, 1, 'RequestBooking', 'Index', '2013-07-05 22:12:18');
INSERT INTO `using_log` VALUES (1164, 1, 'RequestBooking', '', '2013-07-05 22:12:45');
INSERT INTO `using_log` VALUES (1165, 1, 'RequestBooking', 'Index', '2013-07-05 22:12:48');
INSERT INTO `using_log` VALUES (1166, 1, 'RequestBooking', 'Index', '2013-07-05 22:12:49');
INSERT INTO `using_log` VALUES (1167, 1, 'RequestBooking', 'Index', '2013-07-05 22:12:51');
INSERT INTO `using_log` VALUES (1168, 1, 'RequestBooking', 'Index', '2013-07-05 22:12:53');
INSERT INTO `using_log` VALUES (1169, 1, 'RequestBooking', '', '2013-07-05 22:14:01');
INSERT INTO `using_log` VALUES (1170, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:04');
INSERT INTO `using_log` VALUES (1171, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:06');
INSERT INTO `using_log` VALUES (1172, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:08');
INSERT INTO `using_log` VALUES (1173, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:09');
INSERT INTO `using_log` VALUES (1174, 1, 'RequestBooking', '', '2013-07-05 22:14:37');
INSERT INTO `using_log` VALUES (1175, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:40');
INSERT INTO `using_log` VALUES (1176, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:41');
INSERT INTO `using_log` VALUES (1177, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:43');
INSERT INTO `using_log` VALUES (1178, 1, 'RequestBooking', 'Index', '2013-07-05 22:14:45');
INSERT INTO `using_log` VALUES (1179, 1, 'RequestBooking', '', '2013-07-05 22:15:35');
INSERT INTO `using_log` VALUES (1180, 1, 'RequestBooking', 'Index', '2013-07-05 22:15:38');
INSERT INTO `using_log` VALUES (1181, 1, 'RequestBooking', 'Index', '2013-07-05 22:15:39');
INSERT INTO `using_log` VALUES (1182, 1, 'RequestBooking', '', '2013-07-05 22:15:48');
INSERT INTO `using_log` VALUES (1183, 1, 'RequestBooking', '', '2013-07-05 22:15:50');
INSERT INTO `using_log` VALUES (1184, 1, 'RequestBooking', '', '2013-07-05 22:15:51');
INSERT INTO `using_log` VALUES (1185, 1, 'RequestBooking', 'Index', '2013-07-05 22:15:53');
INSERT INTO `using_log` VALUES (1186, 1, 'RequestBooking', '', '2013-07-05 22:20:05');
INSERT INTO `using_log` VALUES (1187, 1, 'RequestBooking', 'Index', '2013-07-05 22:20:08');
INSERT INTO `using_log` VALUES (1188, 1, 'RequestBooking', '', '2013-07-05 22:25:09');
INSERT INTO `using_log` VALUES (1189, 1, 'RequestBooking', 'Index', '2013-07-05 22:25:12');
INSERT INTO `using_log` VALUES (1190, 1, 'RequestBooking', '', '2013-07-05 22:25:26');
INSERT INTO `using_log` VALUES (1191, 1, 'RequestBooking', 'Index', '2013-07-05 22:25:28');
INSERT INTO `using_log` VALUES (1192, 1, 'RequestBooking', '', '2013-07-05 22:35:35');
INSERT INTO `using_log` VALUES (1193, 1, 'RequestBooking', 'Index', '2013-07-05 22:35:43');
INSERT INTO `using_log` VALUES (1194, 1, 'RequestBooking', '', '2013-07-06 22:35:59');
INSERT INTO `using_log` VALUES (1195, 1, 'RequestBooking', 'Index', '2013-07-06 22:36:08');
INSERT INTO `using_log` VALUES (1196, 1, 'RequestBooking', 'Index', '2013-07-06 22:36:10');
INSERT INTO `using_log` VALUES (1197, 1, 'RequestBooking', 'Index', '2013-07-06 22:36:11');
INSERT INTO `using_log` VALUES (1198, 1, 'RequestBooking', 'Index', '2013-07-06 22:36:17');
INSERT INTO `using_log` VALUES (1199, 1, 'RequestBooking', 'Index', '2013-07-06 22:36:19');
INSERT INTO `using_log` VALUES (1200, 1, 'RequestBooking', 'Index', '2013-07-06 22:36:21');
INSERT INTO `using_log` VALUES (1201, 1, 'RequestBooking', '', '2013-07-05 22:36:43');
INSERT INTO `using_log` VALUES (1202, 1, 'RequestBooking', 'Index', '2013-07-05 22:36:45');
INSERT INTO `using_log` VALUES (1203, 1, 'RequestBooking', '', '2013-07-05 22:44:07');
INSERT INTO `using_log` VALUES (1204, 1, 'RequestBooking', '', '2013-07-05 22:46:06');
INSERT INTO `using_log` VALUES (1205, 1, 'RequestBooking', '', '2013-07-05 22:46:08');
INSERT INTO `using_log` VALUES (1206, 1, 'RequestBooking', '', '2013-07-05 22:46:38');
INSERT INTO `using_log` VALUES (1207, 1, 'RequestBooking', '', '2013-07-05 22:48:02');
INSERT INTO `using_log` VALUES (1208, 1, 'RequestBooking', '', '2013-07-05 22:49:58');
INSERT INTO `using_log` VALUES (1209, 1, 'RequestBooking', '', '2013-07-05 22:51:07');
INSERT INTO `using_log` VALUES (1210, 1, 'RequestBooking', '', '2013-07-05 22:51:43');
INSERT INTO `using_log` VALUES (1211, 1, 'RequestBooking', 'Index', '2013-07-05 22:51:49');
INSERT INTO `using_log` VALUES (1212, 1, 'RequestBooking', 'Index', '2013-07-05 22:51:50');
INSERT INTO `using_log` VALUES (1213, 1, 'RequestBooking', 'Index', '2013-07-05 22:51:53');
INSERT INTO `using_log` VALUES (1214, 1, 'RequestBooking', 'Index', '2013-07-05 22:51:55');
INSERT INTO `using_log` VALUES (1215, 1, 'RequestBooking', 'Index', '2013-07-05 22:51:56');
INSERT INTO `using_log` VALUES (1216, 1, 'RequestBooking', 'Index', '2013-07-05 22:51:58');
INSERT INTO `using_log` VALUES (1217, 1, 'RequestBooking', 'Index', '2013-07-05 22:51:59');
INSERT INTO `using_log` VALUES (1218, 1, 'RequestBooking', '', '2013-07-05 22:52:49');
INSERT INTO `using_log` VALUES (1219, 1, 'RequestBooking', '', '2013-07-05 22:53:13');
INSERT INTO `using_log` VALUES (1220, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:14');
INSERT INTO `using_log` VALUES (1221, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:15');
INSERT INTO `using_log` VALUES (1222, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:16');
INSERT INTO `using_log` VALUES (1223, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:17');
INSERT INTO `using_log` VALUES (1224, 1, 'RequestBooking', '', '2013-07-05 22:53:47');
INSERT INTO `using_log` VALUES (1225, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:49');
INSERT INTO `using_log` VALUES (1226, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:50');
INSERT INTO `using_log` VALUES (1227, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:51');
INSERT INTO `using_log` VALUES (1228, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:52');
INSERT INTO `using_log` VALUES (1229, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:54');
INSERT INTO `using_log` VALUES (1230, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:56');
INSERT INTO `using_log` VALUES (1231, 1, 'RequestBooking', 'Index', '2013-07-05 22:53:57');
INSERT INTO `using_log` VALUES (1232, 1, 'RequestBooking', 'Index', '2013-07-05 22:54:00');
INSERT INTO `using_log` VALUES (1233, 1, 'RequestBooking', 'Index', '2013-07-05 22:54:03');
INSERT INTO `using_log` VALUES (1234, 1, 'RequestBooking', 'Index', '2013-07-05 22:54:04');
INSERT INTO `using_log` VALUES (1235, 1, 'RequestBooking', '', '2013-07-05 22:55:06');
INSERT INTO `using_log` VALUES (1236, 1, 'RequestBooking', 'Index', '2013-07-05 22:55:08');
INSERT INTO `using_log` VALUES (1237, 1, 'RequestBooking', 'Index', '2013-07-05 22:55:10');
INSERT INTO `using_log` VALUES (1238, 1, 'RequestBooking', '', '2013-07-05 22:55:46');
INSERT INTO `using_log` VALUES (1239, 1, 'RequestBooking', '', '2013-07-05 22:55:48');
INSERT INTO `using_log` VALUES (1240, 1, 'RequestBooking', 'Index', '2013-07-05 22:55:49');
INSERT INTO `using_log` VALUES (1241, 1, 'RequestBooking', 'Index', '2013-07-05 22:55:51');
INSERT INTO `using_log` VALUES (1242, 1, 'RequestBooking', 'Index', '2013-07-05 22:55:53');
INSERT INTO `using_log` VALUES (1243, 1, 'RequestBooking', '', '2013-07-05 23:02:34');
INSERT INTO `using_log` VALUES (1244, 1, 'RequestBooking', 'Index', '2013-07-05 23:02:38');
INSERT INTO `using_log` VALUES (1245, 1, 'RequestBooking', '', '2013-07-05 23:03:32');
INSERT INTO `using_log` VALUES (1246, 1, 'RequestBooking', 'Index', '2013-07-05 23:03:37');
INSERT INTO `using_log` VALUES (1247, 1, 'RequestBooking', '', '2013-07-05 23:05:27');
INSERT INTO `using_log` VALUES (1248, 1, 'RequestBooking', 'Index', '2013-07-05 23:05:31');
INSERT INTO `using_log` VALUES (1249, 1, 'RequestBooking', 'Index', '2013-07-05 23:05:33');
INSERT INTO `using_log` VALUES (1250, 1, 'RequestBooking', '', '2013-07-05 23:06:01');
INSERT INTO `using_log` VALUES (1251, 1, 'RequestBooking', 'Index', '2013-07-05 23:06:06');
INSERT INTO `using_log` VALUES (1252, 1, 'RequestBooking', '', '2013-07-05 23:55:07');
INSERT INTO `using_log` VALUES (1253, 1, 'RequestBooking', 'Index', '2013-07-05 23:55:11');
INSERT INTO `using_log` VALUES (1254, 1, 'RequestBooking', '', '2013-07-05 23:56:07');
INSERT INTO `using_log` VALUES (1255, 1, 'RequestBooking', 'Index', '2013-07-05 23:56:10');
INSERT INTO `using_log` VALUES (1256, 1, 'RequestBooking', '', '2013-07-05 23:56:41');
INSERT INTO `using_log` VALUES (1257, 1, 'RequestBooking', 'Index', '2013-07-05 23:56:47');
INSERT INTO `using_log` VALUES (1258, 1, 'RequestBooking', '', '2013-07-05 23:57:26');
INSERT INTO `using_log` VALUES (1259, 1, 'RequestBooking', 'Index', '2013-07-05 23:57:28');
INSERT INTO `using_log` VALUES (1260, 1, 'RequestBooking', '', '2013-07-05 23:57:45');
INSERT INTO `using_log` VALUES (1261, 1, 'RequestBooking', 'Index', '2013-07-05 23:57:48');
INSERT INTO `using_log` VALUES (1262, 1, 'RequestBooking', '', '2013-07-05 23:58:11');
INSERT INTO `using_log` VALUES (1263, 1, 'RequestBooking', 'Index', '2013-07-05 23:58:14');
INSERT INTO `using_log` VALUES (1264, 1, 'RequestBooking', '', '2013-07-05 23:59:08');
INSERT INTO `using_log` VALUES (1265, 1, 'RequestBooking', 'Index', '2013-07-05 23:59:11');
INSERT INTO `using_log` VALUES (1266, 1, 'RequestBooking', '', '2013-07-06 00:02:25');
INSERT INTO `using_log` VALUES (1267, 1, 'RequestBooking', '', '2013-07-06 00:15:10');
INSERT INTO `using_log` VALUES (1268, 1, 'RequestBooking', '', '2013-07-06 00:17:41');
INSERT INTO `using_log` VALUES (1269, 1, 'RequestBooking', 'Index', '2013-07-06 00:17:51');
INSERT INTO `using_log` VALUES (1270, 1, 'RequestBooking', '', '2013-07-06 00:18:49');
INSERT INTO `using_log` VALUES (1271, 1, 'RequestBooking', 'Index', '2013-07-06 00:18:55');
INSERT INTO `using_log` VALUES (1272, 1, 'RequestBooking', '', '2013-07-06 00:36:32');
INSERT INTO `using_log` VALUES (1273, 1, 'RequestBooking', 'Index', '2013-07-06 00:36:37');
INSERT INTO `using_log` VALUES (1274, 1, 'RequestBooking', 'Index', '2013-07-06 00:36:40');
INSERT INTO `using_log` VALUES (1275, 1, 'RequestBooking', 'Index', '2013-07-06 00:36:41');
INSERT INTO `using_log` VALUES (1276, 1, 'RequestBooking', 'Index', '2013-07-06 00:36:44');
INSERT INTO `using_log` VALUES (1277, 1, 'RequestBooking', 'Index', '2013-07-06 00:36:46');
INSERT INTO `using_log` VALUES (1278, 1, 'RequestBooking', '', '2013-07-06 00:45:05');
INSERT INTO `using_log` VALUES (1279, 1, 'WebService', 'ShowRequest', '2013-07-07 00:52:02');
INSERT INTO `using_log` VALUES (1280, 1, 'WebService', 'ShowRequest', '2013-07-06 00:52:38');
INSERT INTO `using_log` VALUES (1281, 1, 'WebService', 'ShowRequest', '2013-07-06 00:52:42');
INSERT INTO `using_log` VALUES (1282, NULL, '', '', '2013-07-06 11:19:25');
INSERT INTO `using_log` VALUES (1283, 1, 'RequestBooking', '', '2013-07-06 11:19:30');
INSERT INTO `using_log` VALUES (1284, 1, 'RequestBooking', 'Index', '2013-07-06 11:19:40');
INSERT INTO `using_log` VALUES (1285, 1, 'RequestBooking', 'Index', '2013-07-06 11:19:43');
INSERT INTO `using_log` VALUES (1286, 1, 'requestBooking', 'update', '2013-07-06 11:22:33');
INSERT INTO `using_log` VALUES (1287, 1, 'RequestBooking', '', '2013-07-06 11:22:34');
INSERT INTO `using_log` VALUES (1288, 1, '', '', '2013-07-06 12:12:41');
INSERT INTO `using_log` VALUES (1289, 1, 'RequestBooking', '', '2013-07-06 12:12:45');
INSERT INTO `using_log` VALUES (1290, 1, 'Admin', '', '2013-07-06 12:12:47');
INSERT INTO `using_log` VALUES (1291, 1, 'Equipment', '', '2013-07-06 12:12:49');
INSERT INTO `using_log` VALUES (1292, 1, 'Equipment', '', '2013-07-06 12:12:53');
INSERT INTO `using_log` VALUES (1293, 1, 'equipment', 'update', '2013-07-06 12:13:20');
INSERT INTO `using_log` VALUES (1294, 1, 'Equipment', '', '2013-07-06 12:13:27');
INSERT INTO `using_log` VALUES (1295, 1, 'Solution', '', '2013-07-06 12:13:28');
INSERT INTO `using_log` VALUES (1296, 1, 'Solution', '', '2013-07-06 12:13:32');
INSERT INTO `using_log` VALUES (1297, 1, 'Management', 'Ping', '2013-07-06 12:13:34');
INSERT INTO `using_log` VALUES (1298, 1, 'Management', 'Ping', '2013-07-06 12:13:49');
INSERT INTO `using_log` VALUES (1299, 1, 'Management', 'Ping', '2013-07-06 12:13:51');
INSERT INTO `using_log` VALUES (1300, 1, 'Solution', '', '2013-07-06 12:18:54');
INSERT INTO `using_log` VALUES (1301, 1, 'Management', 'Ping', '2013-07-06 12:18:56');
INSERT INTO `using_log` VALUES (1302, 1, 'Management', 'Ping', '2013-07-06 12:19:05');
INSERT INTO `using_log` VALUES (1303, 1, 'Solution', '', '2013-07-06 12:22:46');
INSERT INTO `using_log` VALUES (1304, 1, 'Management', 'Ping', '2013-07-06 12:22:50');
INSERT INTO `using_log` VALUES (1305, 1, 'Solution', '', '2013-07-06 12:23:30');
INSERT INTO `using_log` VALUES (1306, 1, 'Management', 'Ping', '2013-07-06 12:23:35');
INSERT INTO `using_log` VALUES (1307, 1, 'Solution', '', '2013-07-06 12:23:58');
INSERT INTO `using_log` VALUES (1308, 1, 'Management', 'Ping', '2013-07-06 12:24:03');
INSERT INTO `using_log` VALUES (1309, 1, '', '', '2013-07-06 12:31:06');
INSERT INTO `using_log` VALUES (1310, 1, 'RequestBooking', '', '2013-07-06 12:31:09');
INSERT INTO `using_log` VALUES (1311, 1, 'Solution', '', '2013-07-06 12:31:12');
INSERT INTO `using_log` VALUES (1312, 1, 'Solution', '', '2013-07-06 12:31:16');
INSERT INTO `using_log` VALUES (1313, 1, 'Management', 'Ping', '2013-07-06 12:31:20');
INSERT INTO `using_log` VALUES (1314, 1, 'Solution', '', '2013-07-06 12:33:55');
INSERT INTO `using_log` VALUES (1315, 1, 'Management', 'Ping', '2013-07-06 12:34:00');
INSERT INTO `using_log` VALUES (1316, 1, 'Solution', '', '2013-07-06 12:34:21');
INSERT INTO `using_log` VALUES (1317, 1, 'Solution', '', '2013-07-06 12:34:47');
INSERT INTO `using_log` VALUES (1318, 1, 'Management', 'Ping', '2013-07-06 12:34:51');
INSERT INTO `using_log` VALUES (1319, 1, 'Solution', '', '2013-07-06 12:37:08');
INSERT INTO `using_log` VALUES (1320, 1, 'Management', 'Ping', '2013-07-06 12:37:10');
INSERT INTO `using_log` VALUES (1321, 1, 'Solution', '', '2013-07-06 12:37:27');
INSERT INTO `using_log` VALUES (1322, 1, 'Management', 'Ping', '2013-07-06 12:37:31');
INSERT INTO `using_log` VALUES (1323, 1, 'Solution', '', '2013-07-06 12:37:52');
INSERT INTO `using_log` VALUES (1324, 1, 'Solution', '', '2013-07-06 12:37:53');
INSERT INTO `using_log` VALUES (1325, 1, 'Management', 'Ping', '2013-07-06 12:37:57');
INSERT INTO `using_log` VALUES (1326, 1, 'Solution', '', '2013-07-06 12:38:31');
INSERT INTO `using_log` VALUES (1327, 1, 'Management', 'Ping', '2013-07-06 12:38:35');
INSERT INTO `using_log` VALUES (1328, 1, 'Solution', '', '2013-07-06 12:38:43');
INSERT INTO `using_log` VALUES (1329, 1, 'Management', 'Ping', '2013-07-06 12:38:43');
INSERT INTO `using_log` VALUES (1330, 1, 'Solution', '', '2013-07-06 12:41:33');
INSERT INTO `using_log` VALUES (1331, 1, 'Solution', '', '2013-07-06 12:42:27');
INSERT INTO `using_log` VALUES (1332, 1, 'Solution', '', '2013-07-06 12:42:28');
INSERT INTO `using_log` VALUES (1333, 1, 'Solution', '', '2013-07-06 12:42:49');
INSERT INTO `using_log` VALUES (1334, 1, 'Solution', '', '2013-07-06 12:43:08');
INSERT INTO `using_log` VALUES (1335, 1, 'Management', 'Ping', '2013-07-06 12:43:10');
INSERT INTO `using_log` VALUES (1336, 1, 'Solution', '', '2013-07-06 12:44:08');
INSERT INTO `using_log` VALUES (1337, 1, 'Solution', '', '2013-07-06 12:44:47');
INSERT INTO `using_log` VALUES (1338, 1, 'Management', 'Ping', '2013-07-06 12:44:52');
INSERT INTO `using_log` VALUES (1339, 1, 'Solution', '', '2013-07-06 12:45:09');
INSERT INTO `using_log` VALUES (1340, 1, 'Solution', '', '2013-07-06 12:45:10');
INSERT INTO `using_log` VALUES (1341, 1, 'Management', 'Ping', '2013-07-06 12:45:14');
INSERT INTO `using_log` VALUES (1342, 1, 'Solution', '', '2013-07-06 12:45:36');
INSERT INTO `using_log` VALUES (1343, 1, 'Management', 'Ping', '2013-07-06 12:45:40');
INSERT INTO `using_log` VALUES (1344, 1, 'Solution', '', '2013-07-06 12:46:49');
INSERT INTO `using_log` VALUES (1345, 1, 'Management', 'Ping', '2013-07-06 12:46:53');
INSERT INTO `using_log` VALUES (1346, 1, 'WebService', 'Ping', '2013-07-06 13:54:06');
INSERT INTO `using_log` VALUES (1347, 1, 'WebService', 'Ping', '2013-07-06 13:54:44');
INSERT INTO `using_log` VALUES (1348, 1, 'Solution', '', '2013-07-06 13:55:16');
INSERT INTO `using_log` VALUES (1349, 1, 'WebService', 'Ping', '2013-07-06 13:55:21');
INSERT INTO `using_log` VALUES (1350, 1, 'Solution', '', '2013-07-06 13:55:57');
INSERT INTO `using_log` VALUES (1351, 1, 'WebService', 'Ping', '2013-07-06 13:56:02');
INSERT INTO `using_log` VALUES (1352, 1, 'Solution', '', '2013-07-06 14:08:56');
INSERT INTO `using_log` VALUES (1353, 1, 'Management', 'Ping', '2013-07-06 14:09:00');
INSERT INTO `using_log` VALUES (1354, 1, 'Solution', '', '2013-07-06 14:09:08');
INSERT INTO `using_log` VALUES (1355, 1, 'Management', 'Ping', '2013-07-06 14:09:11');
INSERT INTO `using_log` VALUES (1356, 1, 'Solution', '', '2013-07-06 14:09:35');
INSERT INTO `using_log` VALUES (1357, 1, 'Management', 'Ping', '2013-07-06 14:09:39');
INSERT INTO `using_log` VALUES (1358, 1, 'Solution', '', '2013-07-06 14:10:06');
INSERT INTO `using_log` VALUES (1359, 1, 'Management', 'Ping', '2013-07-06 14:10:10');
INSERT INTO `using_log` VALUES (1360, 1, 'Solution', '', '2013-07-06 14:10:33');
INSERT INTO `using_log` VALUES (1361, 1, 'Management', 'Ping', '2013-07-06 14:10:37');
INSERT INTO `using_log` VALUES (1362, 1, 'Solution', '', '2013-07-06 14:10:58');
INSERT INTO `using_log` VALUES (1363, 1, 'Management', 'Ping', '2013-07-06 14:11:03');
INSERT INTO `using_log` VALUES (1364, 1, 'Solution', '', '2013-07-06 14:11:16');
INSERT INTO `using_log` VALUES (1365, 1, 'Management', 'Ping', '2013-07-06 14:11:20');
INSERT INTO `using_log` VALUES (1366, 1, 'Solution', '', '2013-07-06 14:11:53');
INSERT INTO `using_log` VALUES (1367, 1, 'Management', 'Ping', '2013-07-06 14:11:57');
INSERT INTO `using_log` VALUES (1368, 1, 'Management', 'Ping', '2013-07-06 14:12:19');
INSERT INTO `using_log` VALUES (1369, 1, 'Solution', '', '2013-07-06 14:13:39');
INSERT INTO `using_log` VALUES (1370, 1, 'Management', 'Ping', '2013-07-06 14:13:41');
INSERT INTO `using_log` VALUES (1371, 1, 'Solution', '', '2013-07-06 14:14:01');
INSERT INTO `using_log` VALUES (1372, 1, 'Management', 'Ping', '2013-07-06 14:14:03');
INSERT INTO `using_log` VALUES (1373, 1, 'Management', 'Ping', '2013-07-06 14:14:08');
INSERT INTO `using_log` VALUES (1374, 1, 'Solution', '', '2013-07-06 14:14:08');
INSERT INTO `using_log` VALUES (1375, 1, 'Solution', '', '2013-07-06 14:14:24');
INSERT INTO `using_log` VALUES (1376, 1, 'Management', 'Ping', '2013-07-06 14:14:26');
INSERT INTO `using_log` VALUES (1377, 1, 'Management', 'Ping', '2013-07-06 14:14:30');
INSERT INTO `using_log` VALUES (1378, 1, 'Solution', '', '2013-07-06 14:15:23');
INSERT INTO `using_log` VALUES (1379, 1, 'Management', 'Ping', '2013-07-06 14:15:24');
INSERT INTO `using_log` VALUES (1380, 1, 'Management', 'Ping', '2013-07-06 14:15:29');
INSERT INTO `using_log` VALUES (1381, 1, 'Solution', '', '2013-07-06 14:15:37');
INSERT INTO `using_log` VALUES (1382, 1, 'Management', 'Ping', '2013-07-06 14:15:41');
INSERT INTO `using_log` VALUES (1383, 1, 'Solution', '', '2013-07-06 14:16:49');
INSERT INTO `using_log` VALUES (1384, 1, 'Management', 'Ping', '2013-07-06 14:16:50');
INSERT INTO `using_log` VALUES (1385, 1, 'Solution', '', '2013-07-06 14:17:01');
INSERT INTO `using_log` VALUES (1386, 1, 'Management', 'Ping', '2013-07-06 14:17:02');
INSERT INTO `using_log` VALUES (1387, 1, 'Solution', '', '2013-07-06 14:17:30');
INSERT INTO `using_log` VALUES (1388, 1, 'Solution', '', '2013-07-06 14:17:31');
INSERT INTO `using_log` VALUES (1389, 1, 'Management', 'Ping', '2013-07-06 14:17:32');
INSERT INTO `using_log` VALUES (1390, 1, 'Solution', '', '2013-07-06 14:17:48');
INSERT INTO `using_log` VALUES (1391, 1, 'Solution', '', '2013-07-06 14:17:49');
INSERT INTO `using_log` VALUES (1392, 1, 'Management', 'Ping', '2013-07-06 14:17:50');
INSERT INTO `using_log` VALUES (1393, 1, 'Solution', '', '2013-07-06 14:24:01');
INSERT INTO `using_log` VALUES (1394, 1, 'Management', 'Ping', '2013-07-06 14:24:02');
INSERT INTO `using_log` VALUES (1395, 1, 'Solution', '', '2013-07-06 14:26:06');
INSERT INTO `using_log` VALUES (1396, 1, 'Management', 'Ping', '2013-07-06 14:26:07');
INSERT INTO `using_log` VALUES (1397, 1, 'Management', 'Ping', '2013-07-06 14:26:12');
INSERT INTO `using_log` VALUES (1398, 1, 'Solution', '', '2013-07-06 14:26:14');
INSERT INTO `using_log` VALUES (1399, 1, 'Solution', '', '2013-07-06 14:26:14');
INSERT INTO `using_log` VALUES (1400, 1, 'Solution', '', '2013-07-06 14:26:15');
INSERT INTO `using_log` VALUES (1401, 1, 'Solution', '', '2013-07-06 14:26:15');
INSERT INTO `using_log` VALUES (1402, 1, 'Solution', '', '2013-07-06 14:26:15');
INSERT INTO `using_log` VALUES (1403, 1, 'Solution', '', '2013-07-06 14:26:15');
INSERT INTO `using_log` VALUES (1404, 1, 'Solution', '', '2013-07-06 14:26:16');
INSERT INTO `using_log` VALUES (1405, 1, 'Solution', '', '2013-07-06 14:26:16');
INSERT INTO `using_log` VALUES (1406, 1, 'Solution', '', '2013-07-06 14:26:16');
INSERT INTO `using_log` VALUES (1407, 1, 'Solution', '', '2013-07-06 14:26:16');
INSERT INTO `using_log` VALUES (1408, 1, 'Management', 'Ping', '2013-07-06 14:26:19');
INSERT INTO `using_log` VALUES (1409, 1, 'Solution', '', '2013-07-06 14:26:44');
INSERT INTO `using_log` VALUES (1410, 1, 'Management', 'Ping', '2013-07-06 14:26:45');
INSERT INTO `using_log` VALUES (1411, 1, 'Solution', '', '2013-07-06 14:27:06');
INSERT INTO `using_log` VALUES (1412, 1, 'Management', 'Ping', '2013-07-06 14:27:08');
INSERT INTO `using_log` VALUES (1413, 1, 'Solution', '', '2013-07-06 14:27:43');
INSERT INTO `using_log` VALUES (1414, 1, 'Management', 'Ping', '2013-07-06 14:27:45');
INSERT INTO `using_log` VALUES (1415, 1, 'Solution', '', '2013-07-06 14:28:20');
INSERT INTO `using_log` VALUES (1416, 1, 'Management', 'Ping', '2013-07-06 14:28:22');
INSERT INTO `using_log` VALUES (1417, 1, 'Solution', '', '2013-07-06 14:30:08');
INSERT INTO `using_log` VALUES (1418, 1, 'Management', 'Ping', '2013-07-06 14:30:09');
INSERT INTO `using_log` VALUES (1419, 1, 'Solution', '', '2013-07-06 14:30:28');
INSERT INTO `using_log` VALUES (1420, 1, 'Management', 'Ping', '2013-07-06 14:30:29');
INSERT INTO `using_log` VALUES (1421, 1, 'Solution', '', '2013-07-06 14:30:43');
INSERT INTO `using_log` VALUES (1422, 1, 'Management', 'Ping', '2013-07-06 14:31:03');
INSERT INTO `using_log` VALUES (1423, 1, 'Solution', '', '2013-07-06 14:31:24');
INSERT INTO `using_log` VALUES (1424, 1, 'Solution', '', '2013-07-06 14:31:25');
INSERT INTO `using_log` VALUES (1425, 1, 'Management', 'Ping', '2013-07-06 14:31:27');
INSERT INTO `using_log` VALUES (1426, 1, 'Solution', '', '2013-07-06 14:32:26');
INSERT INTO `using_log` VALUES (1427, 1, 'Management', 'Ping', '2013-07-06 14:32:39');
INSERT INTO `using_log` VALUES (1428, 1, 'Solution', '', '2013-07-06 14:33:02');
INSERT INTO `using_log` VALUES (1429, 1, 'Management', 'Ping', '2013-07-06 14:33:17');
INSERT INTO `using_log` VALUES (1430, 1, 'Solution', '', '2013-07-06 14:34:39');
INSERT INTO `using_log` VALUES (1431, 1, 'Solution', '', '2013-07-06 14:34:40');
INSERT INTO `using_log` VALUES (1432, 1, 'Solution', '', '2013-07-06 14:34:40');
INSERT INTO `using_log` VALUES (1433, 1, 'Solution', '', '2013-07-06 14:34:41');
INSERT INTO `using_log` VALUES (1434, 1, 'Solution', '', '2013-07-06 14:34:42');
INSERT INTO `using_log` VALUES (1435, 1, 'Management', 'Ping', '2013-07-06 14:34:58');
INSERT INTO `using_log` VALUES (1436, 1, 'Solution', '', '2013-07-06 14:36:41');
INSERT INTO `using_log` VALUES (1437, 1, 'Solution', '', '2013-07-06 14:36:42');
INSERT INTO `using_log` VALUES (1438, 1, 'Management', 'Ping', '2013-07-06 14:36:47');
INSERT INTO `using_log` VALUES (1439, 1, 'Solution', '', '2013-07-06 14:37:39');
INSERT INTO `using_log` VALUES (1440, 1, 'Management', 'Ping', '2013-07-06 14:37:44');
INSERT INTO `using_log` VALUES (1441, 1, 'Solution', '', '2013-07-06 14:39:18');
INSERT INTO `using_log` VALUES (1442, 1, 'Solution', '', '2013-07-06 14:39:19');
INSERT INTO `using_log` VALUES (1443, 1, 'Management', 'Ping', '2013-07-06 14:39:23');
INSERT INTO `using_log` VALUES (1444, 1, 'Solution', '', '2013-07-06 14:40:41');
INSERT INTO `using_log` VALUES (1445, 1, 'Management', 'Ping', '2013-07-06 14:40:46');
INSERT INTO `using_log` VALUES (1446, 1, 'Management', 'Ping', '2013-07-06 14:41:33');
INSERT INTO `using_log` VALUES (1447, 1, 'Solution', '', '2013-07-06 14:42:26');
INSERT INTO `using_log` VALUES (1448, 1, 'Management', 'Ping', '2013-07-06 14:42:31');
INSERT INTO `using_log` VALUES (1449, 1, 'Solution', '', '2013-07-06 14:46:45');
INSERT INTO `using_log` VALUES (1450, 1, 'Solution', '', '2013-07-06 14:46:46');
INSERT INTO `using_log` VALUES (1451, 1, 'Management', 'Ping', '2013-07-06 14:46:50');
INSERT INTO `using_log` VALUES (1452, 1, 'Solution', '', '2013-07-06 14:49:16');
INSERT INTO `using_log` VALUES (1453, 1, 'Management', 'Ping', '2013-07-06 14:49:17');
INSERT INTO `using_log` VALUES (1454, 1, 'Solution', '', '2013-07-06 14:50:30');
INSERT INTO `using_log` VALUES (1455, 1, 'Management', 'Ping', '2013-07-06 14:50:50');
INSERT INTO `using_log` VALUES (1456, 1, 'Solution', '', '2013-07-06 14:53:11');
INSERT INTO `using_log` VALUES (1457, 1, 'Management', 'Ping', '2013-07-06 14:53:16');
INSERT INTO `using_log` VALUES (1458, 1, 'Management', 'Ping', '2013-07-06 14:54:34');
INSERT INTO `using_log` VALUES (1459, 1, 'Management', 'Ping', '2013-07-06 14:55:24');
INSERT INTO `using_log` VALUES (1460, 1, 'Solution', '', '2013-07-06 14:58:17');
INSERT INTO `using_log` VALUES (1461, 1, 'Management', 'Ping', '2013-07-06 14:58:19');
INSERT INTO `using_log` VALUES (1462, 1, 'Solution', '', '2013-07-06 14:58:38');
INSERT INTO `using_log` VALUES (1463, 1, 'Management', 'Ping', '2013-07-06 14:58:56');
INSERT INTO `using_log` VALUES (1464, 1, 'Solution', '', '2013-07-06 15:00:15');
INSERT INTO `using_log` VALUES (1465, 1, 'Solution', '', '2013-07-06 15:01:17');
INSERT INTO `using_log` VALUES (1466, 1, 'Solution', '', '2013-07-06 15:01:18');
INSERT INTO `using_log` VALUES (1467, 1, 'Solution', '', '2013-07-06 15:06:28');
INSERT INTO `using_log` VALUES (1468, 1, 'Solution', '', '2013-07-06 15:07:35');
INSERT INTO `using_log` VALUES (1469, 1, 'Solution', '', '2013-07-06 15:07:52');
INSERT INTO `using_log` VALUES (1470, 1, 'Solution', '', '2013-07-06 15:07:53');
INSERT INTO `using_log` VALUES (1471, 1, 'Solution', '', '2013-07-06 15:07:55');
INSERT INTO `using_log` VALUES (1472, 1, 'Solution', '', '2013-07-06 15:08:17');
INSERT INTO `using_log` VALUES (1473, 1, 'Solution', '', '2013-07-06 15:08:19');
INSERT INTO `using_log` VALUES (1474, 1, 'Solution', '', '2013-07-06 15:09:28');
INSERT INTO `using_log` VALUES (1475, 1, 'Solution', '', '2013-07-06 15:09:28');
INSERT INTO `using_log` VALUES (1476, 1, 'Solution', '', '2013-07-06 15:09:29');
INSERT INTO `using_log` VALUES (1477, 1, 'Management', 'Ping', '2013-07-06 15:09:29');
INSERT INTO `using_log` VALUES (1478, 1, 'Solution', '', '2013-07-06 15:09:33');
INSERT INTO `using_log` VALUES (1479, 1, 'Management', 'Ping', '2013-07-06 15:09:35');
INSERT INTO `using_log` VALUES (1480, 1, 'Solution', '', '2013-07-06 15:09:51');
INSERT INTO `using_log` VALUES (1481, 1, 'Management', 'Ping', '2013-07-06 15:09:52');
INSERT INTO `using_log` VALUES (1482, 1, 'Solution', '', '2013-07-06 15:10:10');
INSERT INTO `using_log` VALUES (1483, 1, 'Management', 'Ping', '2013-07-06 15:10:11');
INSERT INTO `using_log` VALUES (1484, 1, 'Solution', '', '2013-07-06 15:10:21');
INSERT INTO `using_log` VALUES (1485, 1, 'Management', 'Ping', '2013-07-06 15:10:23');
INSERT INTO `using_log` VALUES (1486, 1, 'Solution', '', '2013-07-06 15:10:48');
INSERT INTO `using_log` VALUES (1487, 1, 'Management', 'Ping', '2013-07-06 15:10:49');
INSERT INTO `using_log` VALUES (1488, 1, 'Solution', '', '2013-07-06 15:11:37');
INSERT INTO `using_log` VALUES (1489, 1, 'Solution', '', '2013-07-06 15:12:01');
INSERT INTO `using_log` VALUES (1490, 1, 'Solution', '', '2013-07-06 15:12:02');
INSERT INTO `using_log` VALUES (1491, 1, 'Management', 'Ping', '2013-07-06 15:12:03');
INSERT INTO `using_log` VALUES (1492, 1, 'Solution', '', '2013-07-06 15:12:16');
INSERT INTO `using_log` VALUES (1493, 1, 'Management', 'Ping', '2013-07-06 15:12:17');
INSERT INTO `using_log` VALUES (1494, 1, 'Solution', '', '2013-07-06 15:12:33');
INSERT INTO `using_log` VALUES (1495, 1, 'Management', 'Ping', '2013-07-06 15:12:35');
INSERT INTO `using_log` VALUES (1496, 1, 'Solution', '', '2013-07-06 15:13:01');
INSERT INTO `using_log` VALUES (1497, 1, 'Solution', '', '2013-07-06 15:13:02');
INSERT INTO `using_log` VALUES (1498, 1, 'Management', 'Ping', '2013-07-06 15:13:04');
INSERT INTO `using_log` VALUES (1499, 1, 'Solution', '', '2013-07-06 15:13:05');
INSERT INTO `using_log` VALUES (1500, 1, 'Solution', '', '2013-07-06 15:13:12');
INSERT INTO `using_log` VALUES (1501, 1, 'Solution', '', '2013-07-06 15:13:13');
INSERT INTO `using_log` VALUES (1502, 1, 'Management', 'Ping', '2013-07-06 15:13:14');
INSERT INTO `using_log` VALUES (1503, 1, 'Solution', '', '2013-07-06 15:24:26');
INSERT INTO `using_log` VALUES (1504, 1, 'Management', 'Ping', '2013-07-06 15:24:28');
INSERT INTO `using_log` VALUES (1505, 1, 'Solution', '', '2013-07-06 15:25:27');
INSERT INTO `using_log` VALUES (1506, 1, 'Management', 'Ping', '2013-07-06 15:25:31');
INSERT INTO `using_log` VALUES (1507, 1, 'Solution', '', '2013-07-06 15:25:42');
INSERT INTO `using_log` VALUES (1508, 1, 'Solution', '', '2013-07-06 15:25:43');
INSERT INTO `using_log` VALUES (1509, 1, 'Management', 'Ping', '2013-07-06 15:25:45');
INSERT INTO `using_log` VALUES (1510, 1, 'Solution', '', '2013-07-06 15:26:06');
INSERT INTO `using_log` VALUES (1511, 1, 'Management', 'Ping', '2013-07-06 15:26:11');
INSERT INTO `using_log` VALUES (1512, 1, 'Management', 'Ping', '2013-07-06 15:26:16');
INSERT INTO `using_log` VALUES (1513, 1, 'Management', 'Ping', '2013-07-06 15:26:22');
INSERT INTO `using_log` VALUES (1514, 1, 'Solution', '', '2013-07-06 15:27:22');
INSERT INTO `using_log` VALUES (1515, 1, 'Solution', '', '2013-07-06 15:28:35');
INSERT INTO `using_log` VALUES (1516, 1, 'Management', 'Ping', '2013-07-06 15:28:40');
INSERT INTO `using_log` VALUES (1517, 1, 'Management', 'Ping', '2013-07-06 15:28:46');
INSERT INTO `using_log` VALUES (1518, 1, 'Solution', '', '2013-07-06 15:29:03');
INSERT INTO `using_log` VALUES (1519, 1, 'Management', 'Ping', '2013-07-06 15:29:04');
INSERT INTO `using_log` VALUES (1520, 1, 'Management', 'Ping', '2013-07-06 15:29:13');
INSERT INTO `using_log` VALUES (1521, 1, 'Solution', '', '2013-07-06 15:30:01');
INSERT INTO `using_log` VALUES (1522, 1, 'Management', 'Ping', '2013-07-06 15:30:06');
INSERT INTO `using_log` VALUES (1523, 1, 'Management', 'Ping', '2013-07-06 15:30:10');
INSERT INTO `using_log` VALUES (1524, 1, 'Management', 'Ping', '2013-07-06 15:30:16');
INSERT INTO `using_log` VALUES (1525, 1, 'Management', 'Ping', '2013-07-06 15:32:05');
INSERT INTO `using_log` VALUES (1526, 1, 'Management', 'Ping', '2013-07-06 15:32:09');
INSERT INTO `using_log` VALUES (1527, 1, 'Admin', '', '2013-07-06 15:32:22');
INSERT INTO `using_log` VALUES (1528, 1, 'Equipment', '', '2013-07-06 15:32:25');
INSERT INTO `using_log` VALUES (1529, 1, 'Equipment', '', '2013-07-06 15:32:33');
INSERT INTO `using_log` VALUES (1530, 1, 'equipment', 'update', '2013-07-06 15:32:36');
INSERT INTO `using_log` VALUES (1531, 1, 'Equipment', '', '2013-07-06 15:32:40');
INSERT INTO `using_log` VALUES (1532, 1, 'Equipment', '', '2013-07-06 15:32:44');
INSERT INTO `using_log` VALUES (1533, 1, 'equipment', 'update', '2013-07-06 15:32:46');
INSERT INTO `using_log` VALUES (1534, 1, 'Equipment', '', '2013-07-06 15:32:57');
INSERT INTO `using_log` VALUES (1535, 1, 'Solution', '', '2013-07-06 15:32:58');
INSERT INTO `using_log` VALUES (1536, 1, 'Solution', '', '2013-07-06 15:33:03');
INSERT INTO `using_log` VALUES (1537, 1, 'Management', 'Ping', '2013-07-06 15:33:09');
INSERT INTO `using_log` VALUES (1538, 1, 'Solution', '', '2013-07-06 15:33:24');
INSERT INTO `using_log` VALUES (1539, 1, 'Management', 'Ping', '2013-07-06 15:33:34');
INSERT INTO `using_log` VALUES (1540, 1, 'Management', 'Ping', '2013-07-06 15:34:41');
INSERT INTO `using_log` VALUES (1541, 1, 'Management', 'Ping', '2013-07-06 15:34:57');
INSERT INTO `using_log` VALUES (1542, 1, 'Solution', '', '2013-07-06 15:35:36');
INSERT INTO `using_log` VALUES (1543, 1, 'Solution', '', '2013-07-06 15:37:20');
INSERT INTO `using_log` VALUES (1544, 1, 'Solution', '', '2013-07-06 15:37:41');
INSERT INTO `using_log` VALUES (1545, 1, 'Solution', '', '2013-07-06 15:38:00');
INSERT INTO `using_log` VALUES (1546, 1, 'Solution', '', '2013-07-06 15:39:14');
INSERT INTO `using_log` VALUES (1547, 1, 'Solution', '', '2013-07-06 15:40:45');
INSERT INTO `using_log` VALUES (1548, 1, 'Solution', '', '2013-07-06 15:41:20');
INSERT INTO `using_log` VALUES (1549, 1, 'Solution', 'Index', '2013-07-06 15:41:29');
INSERT INTO `using_log` VALUES (1550, 1, 'solution', 'Index', '2013-07-06 15:41:32');
INSERT INTO `using_log` VALUES (1551, 1, 'solution', 'Index', '2013-07-06 15:41:34');
INSERT INTO `using_log` VALUES (1552, 1, 'Solution', '', '2013-07-06 15:41:43');
INSERT INTO `using_log` VALUES (1553, 1, 'Solution', '', '2013-07-06 15:42:02');
INSERT INTO `using_log` VALUES (1554, 1, 'Solution', '', '2013-07-06 15:42:25');
INSERT INTO `using_log` VALUES (1555, 1, 'Solution', '', '2013-07-06 15:42:42');
INSERT INTO `using_log` VALUES (1556, 1, 'Admin', '', '2013-07-06 16:01:47');
INSERT INTO `using_log` VALUES (1557, 1, 'Equipment', '', '2013-07-06 16:01:49');
INSERT INTO `using_log` VALUES (1558, 1, 'Equipment', '', '2013-07-06 16:01:53');
INSERT INTO `using_log` VALUES (1559, 1, 'equipment', 'update', '2013-07-06 16:01:56');
INSERT INTO `using_log` VALUES (1560, 1, 'Equipment', '', '2013-07-06 16:03:03');
INSERT INTO `using_log` VALUES (1561, 1, 'Solution', '', '2013-07-06 16:03:06');
INSERT INTO `using_log` VALUES (1562, 1, 'Solution', '', '2013-07-06 16:03:24');
INSERT INTO `using_log` VALUES (1563, 1, 'Solution', '', '2013-07-06 16:07:41');
INSERT INTO `using_log` VALUES (1564, 1, '', '', '2013-07-06 16:21:01');
INSERT INTO `using_log` VALUES (1565, 1, 'RequestBooking', '', '2013-07-06 16:21:04');
INSERT INTO `using_log` VALUES (1566, 1, 'RequestBorrow', '', '2013-07-06 16:21:06');
INSERT INTO `using_log` VALUES (1567, 1, 'RequestBorrow', 'Request', '2013-07-06 16:21:08');
INSERT INTO `using_log` VALUES (1568, 1, 'RequestBorrow', 'View', '2013-07-06 16:21:48');
INSERT INTO `using_log` VALUES (1569, 1, 'RequestBorrow', '', '2013-07-06 16:22:04');
INSERT INTO `using_log` VALUES (1570, 1, 'RequestBorrow', '', '2013-07-06 16:39:43');
INSERT INTO `using_log` VALUES (1571, 1, 'RequestBorrow', 'Request', '2013-07-06 16:39:46');
INSERT INTO `using_log` VALUES (1572, 1, 'RequestBorrow', 'View', '2013-07-06 16:40:10');
INSERT INTO `using_log` VALUES (1573, 1, 'RequestBorrow', '', '2013-07-06 16:40:12');
INSERT INTO `using_log` VALUES (1574, 1, 'RequestBorrow', 'Request', '2013-07-06 16:53:13');
INSERT INTO `using_log` VALUES (1575, 1, 'RequestBorrow', 'View', '2013-07-06 16:53:51');
INSERT INTO `using_log` VALUES (1576, 1, 'RequestBorrow', '', '2013-07-06 16:53:54');
INSERT INTO `using_log` VALUES (1577, 1, 'RequestBorrow', '', '2013-07-06 16:55:05');
INSERT INTO `using_log` VALUES (1578, 1, 'RequestBorrow', 'Request', '2013-07-06 16:55:06');
INSERT INTO `using_log` VALUES (1579, 1, 'RequestBorrow', 'View', '2013-07-06 16:55:42');
INSERT INTO `using_log` VALUES (1580, 1, 'RequestBorrow', '', '2013-07-06 16:55:48');
INSERT INTO `using_log` VALUES (1581, NULL, '', '', '2013-07-09 13:51:58');
INSERT INTO `using_log` VALUES (1582, 1, 'RequestBooking', '', '2013-07-09 13:52:02');
INSERT INTO `using_log` VALUES (1583, 1, 'RequestBooking', 'Request', '2013-07-09 13:52:04');
INSERT INTO `using_log` VALUES (1584, 1, 'WebService', 'SendNotification', '2013-07-09 13:53:28');
INSERT INTO `using_log` VALUES (1585, 1, 'WebService', 'SendNotification', '2013-07-09 13:53:30');
INSERT INTO `using_log` VALUES (1586, 1, 'WebService', 'SendNotification', '2013-07-09 13:54:04');
INSERT INTO `using_log` VALUES (1587, 1, 'WebService', 'SendNotification', '2013-07-09 12:54:53');
INSERT INTO `using_log` VALUES (1588, 1, 'WebService', 'SendNotification', '2013-07-09 12:55:07');
INSERT INTO `using_log` VALUES (1589, 1, 'WebService', 'SendNotification', '2013-07-09 13:57:11');
INSERT INTO `using_log` VALUES (1590, 1, 'WebService', 'SendNotification', '2013-07-09 13:57:21');
INSERT INTO `using_log` VALUES (1591, 1, 'WebService', 'SendNotification', '2013-07-09 13:58:16');
INSERT INTO `using_log` VALUES (1592, 1, 'WebService', 'SendNotification', '2013-07-09 13:58:52');
INSERT INTO `using_log` VALUES (1593, 1, 'WebService', 'SendNotification', '2013-07-09 13:59:44');
INSERT INTO `using_log` VALUES (1594, 1, 'WebService', 'SendNotification', '2013-07-09 14:00:07');
INSERT INTO `using_log` VALUES (1595, 1, 'WebService', 'SendNotification', '2013-07-09 13:01:08');
INSERT INTO `using_log` VALUES (1596, 1, 'WebService', 'SendNotification', '2013-07-09 13:14:39');
INSERT INTO `using_log` VALUES (1597, 1, 'WebService', 'SendNotification', '2013-07-09 13:14:48');
INSERT INTO `using_log` VALUES (1598, 1, 'WebService', 'SendNotification', '2013-07-09 13:15:29');
INSERT INTO `using_log` VALUES (1599, 1, 'WebService', 'SendNotification', '2013-07-09 13:20:23');
INSERT INTO `using_log` VALUES (1600, 1, 'WebService', 'SendNotification', '2013-07-09 13:59:11');
INSERT INTO `using_log` VALUES (1601, 1, 'WebService', 'SendNotification', '2013-07-09 14:17:14');
INSERT INTO `using_log` VALUES (1602, 1, '', '', '2013-07-09 15:19:49');
INSERT INTO `using_log` VALUES (1603, 1, '', '', '2013-07-09 16:07:47');
INSERT INTO `using_log` VALUES (1604, 1, 'WebService', 'SendNotification', '2013-07-09 15:07:51');
INSERT INTO `using_log` VALUES (1605, 1, 'WebService', 'SendNotification', '2013-07-09 15:12:01');
INSERT INTO `using_log` VALUES (1606, 1, 'WebService', 'SendNotification', '2013-07-09 15:13:56');
INSERT INTO `using_log` VALUES (1607, 1, 'WebService', 'SendNotification', '2013-07-09 15:17:05');
INSERT INTO `using_log` VALUES (1608, 1, 'WebService', 'SendNotification', '2013-07-09 16:24:42');
INSERT INTO `using_log` VALUES (1609, 1, 'WebService', 'SendNotification', '2013-07-09 16:25:00');
INSERT INTO `using_log` VALUES (1610, 1, 'WebService', 'SendNotification', '2013-07-09 16:42:50');
INSERT INTO `using_log` VALUES (1611, NULL, '', '', '2013-07-10 10:08:40');
INSERT INTO `using_log` VALUES (1612, 2, 'RequestBooking', '', '2013-07-10 10:08:53');
INSERT INTO `using_log` VALUES (1613, 2, 'RequestBooking', 'Request', '2013-07-10 10:09:06');
INSERT INTO `using_log` VALUES (1614, 2, 'RequestBooking', 'Request', '2013-07-10 10:10:11');
INSERT INTO `using_log` VALUES (1615, 2, 'RequestBooking', 'Request', '2013-07-10 10:11:12');
INSERT INTO `using_log` VALUES (1616, 2, 'RequestBooking', 'Request', '2013-07-10 10:13:58');
INSERT INTO `using_log` VALUES (1617, 2, 'RequestBooking', 'View', '2013-07-10 10:14:16');
INSERT INTO `using_log` VALUES (1618, 2, 'RequestBooking', '', '2013-07-10 10:14:19');
INSERT INTO `using_log` VALUES (1619, 2, 'RequestBooking', 'Request', '2013-07-10 10:15:21');
INSERT INTO `using_log` VALUES (1620, 2, 'RequestBooking', 'View', '2013-07-10 10:15:42');
INSERT INTO `using_log` VALUES (1621, 2, 'RequestBooking', '', '2013-07-10 10:15:43');
INSERT INTO `using_log` VALUES (1622, 2, 'RequestBooking', 'Request', '2013-07-10 10:19:24');
INSERT INTO `using_log` VALUES (1623, NULL, '', '', '2013-07-10 10:46:39');
INSERT INTO `using_log` VALUES (1624, 2, 'RequestBooking', '', '2013-07-10 10:46:47');
INSERT INTO `using_log` VALUES (1625, 2, 'RequestBooking', 'Request', '2013-07-10 10:46:48');
INSERT INTO `using_log` VALUES (1626, 2, 'RequestBooking', 'View', '2013-07-10 10:47:10');
INSERT INTO `using_log` VALUES (1627, 2, 'RequestBooking', '', '2013-07-10 10:47:12');
INSERT INTO `using_log` VALUES (1628, NULL, 'management', 'login', '2013-07-10 10:47:50');
INSERT INTO `using_log` VALUES (1629, 1, 'RequestBooking', '', '2013-07-10 10:47:58');
INSERT INTO `using_log` VALUES (1630, 1, 'RequestBooking', 'Request', '2013-07-10 10:50:03');
INSERT INTO `using_log` VALUES (1631, 1, 'RequestBooking', 'View', '2013-07-10 10:52:30');
INSERT INTO `using_log` VALUES (1632, 1, 'RequestBooking', '', '2013-07-10 10:52:58');
INSERT INTO `using_log` VALUES (1633, 1, 'RequestBooking', '', '2013-07-10 10:57:58');
INSERT INTO `using_log` VALUES (1634, 1, 'RequestBooking', '', '2013-07-10 10:57:59');
INSERT INTO `using_log` VALUES (1635, 1, 'RequestBooking', '', '2013-07-10 10:58:00');
INSERT INTO `using_log` VALUES (1636, 1, 'RequestBooking', 'Index', '2013-07-10 10:58:05');
INSERT INTO `using_log` VALUES (1637, 1, 'RequestBooking', 'Index', '2013-07-10 10:58:11');
INSERT INTO `using_log` VALUES (1638, 1, 'RequestBooking', '', '2013-07-10 10:58:16');
INSERT INTO `using_log` VALUES (1639, 1, 'RequestBooking', '', '2013-07-10 10:58:17');
INSERT INTO `using_log` VALUES (1640, 1, 'RequestBooking', 'Index', '2013-07-10 10:58:24');
INSERT INTO `using_log` VALUES (1641, 1, 'RequestBooking', '', '2013-07-10 10:58:27');
INSERT INTO `using_log` VALUES (1642, 1, 'RequestBooking', '', '2013-07-10 10:58:28');
INSERT INTO `using_log` VALUES (1643, 1, 'RequestBorrow', '', '2013-07-10 11:05:00');
INSERT INTO `using_log` VALUES (1644, 1, 'RequestService', '', '2013-07-10 11:05:23');
INSERT INTO `using_log` VALUES (1645, 1, 'RequestBorrow', '', '2013-07-10 11:05:25');
INSERT INTO `using_log` VALUES (1646, 1, 'RequestBooking', '', '2013-07-10 11:38:42');
INSERT INTO `using_log` VALUES (1647, 1, 'RequestBorrow', '', '2013-07-10 11:45:22');
INSERT INTO `using_log` VALUES (1648, 1, 'RequestService', '', '2013-07-10 11:50:23');
INSERT INTO `using_log` VALUES (1649, NULL, '', '', '2013-07-11 10:55:14');
INSERT INTO `using_log` VALUES (1650, 1, 'RequestBooking', '', '2013-07-11 10:55:20');
INSERT INTO `using_log` VALUES (1651, 1, 'Solution', '', '2013-07-11 10:55:22');
INSERT INTO `using_log` VALUES (1652, 1, 'RequestService', '', '2013-07-11 10:55:25');
INSERT INTO `using_log` VALUES (1653, 1, 'RequestService', 'Index', '2013-07-11 10:55:32');
INSERT INTO `using_log` VALUES (1654, 1, 'RequestService', 'Index', '2013-07-11 10:55:34');
INSERT INTO `using_log` VALUES (1655, 1, 'RequestService', 'Index', '2013-07-11 10:55:37');
INSERT INTO `using_log` VALUES (1656, 1, 'RequestService', 'Request', '2013-07-11 10:55:39');
INSERT INTO `using_log` VALUES (1657, 1, 'RequestService', '', '2013-07-11 10:55:41');
INSERT INTO `using_log` VALUES (1658, 1, 'RequestService', 'Request', '2013-07-11 10:55:42');
INSERT INTO `using_log` VALUES (1659, 1, 'RequestService', '', '2013-07-11 10:55:44');
INSERT INTO `using_log` VALUES (1660, 1, 'RequestService', 'Request', '2013-07-11 10:55:44');
INSERT INTO `using_log` VALUES (1661, 1, 'RequestService', 'View', '2013-07-11 10:56:05');
INSERT INTO `using_log` VALUES (1662, 1, 'RequestService', 'update', '2013-07-11 10:56:08');
INSERT INTO `using_log` VALUES (1663, 1, 'RequestService', '', '2013-07-11 10:56:11');
INSERT INTO `using_log` VALUES (1664, 1, 'requestService', 'update', '2013-07-11 10:56:20');
INSERT INTO `using_log` VALUES (1665, 1, 'RequestService', '', '2013-07-11 10:56:26');
INSERT INTO `using_log` VALUES (1666, 1, 'requestService', 'update', '2013-07-11 10:56:30');
INSERT INTO `using_log` VALUES (1667, 1, 'RequestService', '', '2013-07-11 10:56:35');
INSERT INTO `using_log` VALUES (1668, 1, 'RequestService', 'Request', '2013-07-11 10:56:37');
INSERT INTO `using_log` VALUES (1669, 1, 'RequestService', '', '2013-07-11 10:56:38');
INSERT INTO `using_log` VALUES (1670, 1, 'requestService', 'update', '2013-07-11 10:56:44');
INSERT INTO `using_log` VALUES (1671, 1, 'RequestService', '', '2013-07-11 10:56:54');
INSERT INTO `using_log` VALUES (1672, 1, 'requestService', 'update', '2013-07-11 11:02:54');
INSERT INTO `using_log` VALUES (1673, 1, 'RequestService', '', '2013-07-11 11:02:57');
INSERT INTO `using_log` VALUES (1674, NULL, '', '', '2013-07-15 16:06:39');
INSERT INTO `using_log` VALUES (1675, 1, 'RequestBooking', '', '2013-07-15 16:06:46');
INSERT INTO `using_log` VALUES (1676, NULL, 'management', 'login', '2013-07-15 16:06:53');
INSERT INTO `using_log` VALUES (1677, 2, 'RequestBooking', '', '2013-07-15 16:07:00');
INSERT INTO `using_log` VALUES (1678, NULL, 'management', 'login', '2013-07-15 16:09:15');
INSERT INTO `using_log` VALUES (1679, 4, 'RequestBooking', '', '2013-07-15 16:09:21');
INSERT INTO `using_log` VALUES (1680, 4, 'RequestBooking', 'Request', '2013-07-15 16:09:24');
INSERT INTO `using_log` VALUES (1681, 4, 'RequestBooking', 'Request', '2013-07-15 16:09:38');
INSERT INTO `using_log` VALUES (1682, 4, 'RequestBooking', 'Request', '2013-07-15 16:09:59');
INSERT INTO `using_log` VALUES (1683, NULL, '', '', '2013-08-16 21:32:55');
INSERT INTO `using_log` VALUES (1684, NULL, '', '', '2013-08-16 21:34:26');
INSERT INTO `using_log` VALUES (1685, 1, 'RequestBooking', '', '2013-08-16 21:34:31');
INSERT INTO `using_log` VALUES (1686, 1, 'Solution', '', '2013-08-16 21:34:42');
INSERT INTO `using_log` VALUES (1687, 1, 'Admin', '', '2013-08-16 21:34:43');
INSERT INTO `using_log` VALUES (1688, 1, 'Solution', '', '2013-08-16 21:34:46');
INSERT INTO `using_log` VALUES (1689, 1, 'Admin', '', '2013-08-16 21:34:55');
INSERT INTO `using_log` VALUES (1690, 1, 'Equipment', '', '2013-08-16 21:34:59');
INSERT INTO `using_log` VALUES (1691, 1, 'equipment', 'index', '2013-08-16 21:35:13');
INSERT INTO `using_log` VALUES (1692, 1, 'equipment', 'index', '2013-08-16 21:35:14');
INSERT INTO `using_log` VALUES (1693, 1, 'equipment', 'index', '2013-08-16 21:35:15');
INSERT INTO `using_log` VALUES (1694, 1, 'equipment', 'update', '2013-08-16 21:35:25');
INSERT INTO `using_log` VALUES (1695, 1, 'Equipment', '', '2013-08-16 21:35:51');
INSERT INTO `using_log` VALUES (1696, 1, 'equipment', 'index', '2013-08-16 21:35:54');
INSERT INTO `using_log` VALUES (1697, 1, 'Equipment', '', '2013-08-16 21:36:02');
INSERT INTO `using_log` VALUES (1698, 1, 'Solution', '', '2013-08-16 21:36:07');
INSERT INTO `using_log` VALUES (1699, 1, 'Solution', '', '2013-08-16 21:36:39');
INSERT INTO `using_log` VALUES (1700, 1, 'Solution', '', '2013-08-16 21:40:12');
INSERT INTO `using_log` VALUES (1701, 1, '', '', '2013-08-16 22:02:23');
INSERT INTO `using_log` VALUES (1702, 1, 'RequestBooking', '', '2013-08-16 22:02:27');
INSERT INTO `using_log` VALUES (1703, 1, 'Solution', '', '2013-08-16 22:02:30');
INSERT INTO `using_log` VALUES (1704, 1, 'Solution', '', '2013-08-16 22:02:56');
INSERT INTO `using_log` VALUES (1705, 1, 'Solution', '', '2013-08-16 22:04:10');
INSERT INTO `using_log` VALUES (1706, 1, 'Management', 'WakeOnLan', '2013-08-16 22:04:25');
INSERT INTO `using_log` VALUES (1707, 1, 'Management', 'WakeOnLan', '2013-08-16 22:04:30');
INSERT INTO `using_log` VALUES (1708, 1, 'Management', 'WakeOnLan', '2013-08-16 22:11:46');
INSERT INTO `using_log` VALUES (1709, 1, 'Management', 'WakeOnLan', '2013-08-16 22:11:48');
INSERT INTO `using_log` VALUES (1710, 1, 'Management', 'WakeOnLan', '2013-08-16 22:11:49');
INSERT INTO `using_log` VALUES (1711, 1, 'Management', 'WakeOnLan', '2013-08-16 22:11:49');
INSERT INTO `using_log` VALUES (1712, 1, 'Management', 'WakeOnLan', '2013-08-16 22:11:50');
INSERT INTO `using_log` VALUES (1713, 1, 'Management', 'WakeOnLan', '2013-08-16 22:11:51');
INSERT INTO `using_log` VALUES (1714, 1, 'Management', 'WakeOnLan', '2013-08-16 22:11:59');
INSERT INTO `using_log` VALUES (1715, 1, 'Management', 'WakeOnLan', '2013-08-16 22:12:06');
INSERT INTO `using_log` VALUES (1716, 1, 'Solution', '', '2013-08-16 22:13:19');
INSERT INTO `using_log` VALUES (1717, 1, 'Management', 'WakeOnLan', '2013-08-16 22:13:23');
INSERT INTO `using_log` VALUES (1718, 1, 'Solution', '', '2013-08-16 22:14:15');
INSERT INTO `using_log` VALUES (1719, 1, 'Management', 'WakeOnLan', '2013-08-16 22:15:18');
INSERT INTO `using_log` VALUES (1720, 1, 'Solution', '', '2013-08-16 22:17:05');
INSERT INTO `using_log` VALUES (1721, 1, 'Management', 'WakeOnLan', '2013-08-16 22:17:08');
INSERT INTO `using_log` VALUES (1722, 1, 'Solution', '', '2013-08-16 22:42:00');
INSERT INTO `using_log` VALUES (1723, 1, 'Management', 'WakeOnLan', '2013-08-16 22:42:06');
INSERT INTO `using_log` VALUES (1724, 1, 'Solution', '', '2013-08-16 22:47:16');
INSERT INTO `using_log` VALUES (1725, 1, 'Management', 'WakeOnLan', '2013-08-16 22:47:17');
INSERT INTO `using_log` VALUES (1726, 1, 'Solution', '', '2013-08-16 22:47:37');
INSERT INTO `using_log` VALUES (1727, 1, 'Solution', '', '2013-08-16 22:48:06');
INSERT INTO `using_log` VALUES (1728, 1, 'Solution', '', '2013-08-16 22:49:02');
INSERT INTO `using_log` VALUES (1729, 1, 'Management', 'WakeOnLan', '2013-08-16 22:49:04');
INSERT INTO `using_log` VALUES (1730, 1, 'Solution', '', '2013-08-16 22:49:42');
INSERT INTO `using_log` VALUES (1731, 1, 'Management', 'WakeOnLan', '2013-08-16 22:49:43');
INSERT INTO `using_log` VALUES (1732, 1, 'Solution', '', '2013-08-16 22:50:32');
INSERT INTO `using_log` VALUES (1733, 1, 'Management', 'WakeOnLan', '2013-08-16 22:50:33');
INSERT INTO `using_log` VALUES (1734, 1, 'Solution', '', '2013-08-16 22:51:22');
INSERT INTO `using_log` VALUES (1735, 1, 'Management', 'WakeOnLan', '2013-08-16 22:51:23');
INSERT INTO `using_log` VALUES (1736, 1, 'Solution', '', '2013-08-16 22:51:43');
INSERT INTO `using_log` VALUES (1737, 1, 'Solution', '', '2013-08-16 22:52:29');
INSERT INTO `using_log` VALUES (1738, 1, 'Solution', '', '2013-08-16 22:52:44');
INSERT INTO `using_log` VALUES (1739, 1, 'Solution', '', '2013-08-16 22:52:53');
INSERT INTO `using_log` VALUES (1740, 1, 'Solution', '', '2013-08-16 22:53:12');
INSERT INTO `using_log` VALUES (1741, 1, 'Solution', '', '2013-08-16 22:53:22');
INSERT INTO `using_log` VALUES (1742, 1, 'Solution', '', '2013-08-16 22:53:45');
INSERT INTO `using_log` VALUES (1743, 1, 'Solution', '', '2013-08-16 22:53:46');
INSERT INTO `using_log` VALUES (1744, 1, 'Solution', '', '2013-08-16 22:53:58');
INSERT INTO `using_log` VALUES (1745, 1, 'Solution', '', '2013-08-16 22:54:21');
INSERT INTO `using_log` VALUES (1746, 1, 'Solution', '', '2013-08-16 22:54:57');
INSERT INTO `using_log` VALUES (1747, 1, 'Management', 'WakeOnLan', '2013-08-16 22:54:58');
INSERT INTO `using_log` VALUES (1748, 1, 'Solution', '', '2013-08-16 22:55:05');
INSERT INTO `using_log` VALUES (1749, 1, 'Management', 'WakeOnLan', '2013-08-16 22:55:07');
INSERT INTO `using_log` VALUES (1750, 1, 'Solution', '', '2013-08-16 22:55:19');
INSERT INTO `using_log` VALUES (1751, 1, 'Management', 'WakeOnLan', '2013-08-16 22:55:20');
INSERT INTO `using_log` VALUES (1752, 1, 'Solution', '', '2013-08-16 22:56:50');
INSERT INTO `using_log` VALUES (1753, 1, 'Management', 'WakeOnLan', '2013-08-16 22:56:52');
INSERT INTO `using_log` VALUES (1754, 1, 'Solution', '', '2013-08-16 22:57:22');
INSERT INTO `using_log` VALUES (1755, 1, 'Management', 'WakeOnLan', '2013-08-16 22:57:24');
INSERT INTO `using_log` VALUES (1756, 1, 'Solution', '', '2013-08-16 22:57:32');
INSERT INTO `using_log` VALUES (1757, 1, 'Management', 'WakeOnLan', '2013-08-16 22:57:33');
INSERT INTO `using_log` VALUES (1758, 1, 'Solution', '', '2013-08-16 22:57:45');
INSERT INTO `using_log` VALUES (1759, 1, 'Solution', '', '2013-08-16 22:58:15');
INSERT INTO `using_log` VALUES (1760, 1, 'Management', 'WakeOnLan', '2013-08-16 22:58:16');
INSERT INTO `using_log` VALUES (1761, 1, 'Solution', '', '2013-08-16 22:59:17');
INSERT INTO `using_log` VALUES (1762, 1, 'Management', 'WakeOnLan', '2013-08-16 22:59:18');
INSERT INTO `using_log` VALUES (1763, 1, 'Solution', '', '2013-08-16 22:59:37');
INSERT INTO `using_log` VALUES (1764, 1, 'Management', 'WakeOnLan', '2013-08-16 22:59:38');
INSERT INTO `using_log` VALUES (1765, 1, 'Solution', '', '2013-08-16 23:00:46');
INSERT INTO `using_log` VALUES (1766, 1, 'Management', 'WakeOnLan', '2013-08-16 23:00:47');
INSERT INTO `using_log` VALUES (1767, 1, 'Solution', '', '2013-08-16 23:01:12');
INSERT INTO `using_log` VALUES (1768, 1, 'Solution', '', '2013-08-16 23:02:23');
INSERT INTO `using_log` VALUES (1769, 1, 'Solution', '', '2013-08-16 23:03:07');
INSERT INTO `using_log` VALUES (1770, 1, 'Solution', '', '2013-08-16 23:04:28');
INSERT INTO `using_log` VALUES (1771, 1, 'Solution', '', '2013-08-16 23:05:12');
INSERT INTO `using_log` VALUES (1772, 1, 'Solution', '', '2013-08-16 23:07:11');
INSERT INTO `using_log` VALUES (1773, 1, 'Solution', '', '2013-08-16 23:10:22');
INSERT INTO `using_log` VALUES (1774, 1, 'Solution', '', '2013-08-16 23:10:27');
INSERT INTO `using_log` VALUES (1775, 1, 'Solution', '', '2013-08-16 23:12:08');
INSERT INTO `using_log` VALUES (1776, 1, 'Solution', '', '2013-08-16 23:12:33');
INSERT INTO `using_log` VALUES (1777, 1, 'Solution', '', '2013-08-16 23:14:04');
INSERT INTO `using_log` VALUES (1778, 1, 'Solution', '', '2013-08-16 23:14:17');
INSERT INTO `using_log` VALUES (1779, 1, 'Solution', '', '2013-08-16 23:14:35');
INSERT INTO `using_log` VALUES (1780, 1, 'Solution', '', '2013-08-16 23:15:22');
INSERT INTO `using_log` VALUES (1781, 1, 'Solution', '', '2013-08-16 23:18:17');
INSERT INTO `using_log` VALUES (1782, 1, 'Solution', '', '2013-08-16 23:18:39');
INSERT INTO `using_log` VALUES (1783, 1, 'Solution', '', '2013-08-16 23:19:01');
INSERT INTO `using_log` VALUES (1784, 1, 'Solution', '', '2013-08-16 23:19:12');
INSERT INTO `using_log` VALUES (1785, 1, 'Solution', '', '2013-08-16 23:19:46');
INSERT INTO `using_log` VALUES (1786, 1, 'Management', 'WakeOnLan', '2013-08-16 23:19:48');
INSERT INTO `using_log` VALUES (1787, 1, 'Solution', '', '2013-08-16 23:19:56');
INSERT INTO `using_log` VALUES (1788, 1, 'Management', 'WakeOnLan', '2013-08-16 23:19:57');
INSERT INTO `using_log` VALUES (1789, 1, 'Solution', '', '2013-08-16 23:20:21');
INSERT INTO `using_log` VALUES (1790, 1, 'Management', 'WakeOnLan', '2013-08-16 23:20:23');
INSERT INTO `using_log` VALUES (1791, 1, 'Solution', '', '2013-08-16 23:20:44');
INSERT INTO `using_log` VALUES (1792, 1, 'Solution', '', '2013-08-16 23:21:02');
INSERT INTO `using_log` VALUES (1793, 1, 'Solution', '', '2013-08-16 23:40:00');
INSERT INTO `using_log` VALUES (1794, 1, 'Solution', '', '2013-08-16 23:40:04');
INSERT INTO `using_log` VALUES (1795, 1, 'Management', 'WakeOnLan', '2013-08-16 23:40:04');
INSERT INTO `using_log` VALUES (1796, 1, 'Solution', '', '2013-08-16 23:40:19');
INSERT INTO `using_log` VALUES (1797, 1, 'Solution', '', '2013-08-16 23:40:20');
INSERT INTO `using_log` VALUES (1798, 1, 'Solution', '', '2013-08-16 23:40:24');
INSERT INTO `using_log` VALUES (1799, 1, 'Solution', '', '2013-08-16 23:40:27');
INSERT INTO `using_log` VALUES (1800, 1, 'Solution', '', '2013-08-16 23:40:27');
INSERT INTO `using_log` VALUES (1801, 1, 'Solution', '', '2013-08-16 23:40:28');
INSERT INTO `using_log` VALUES (1802, 1, 'Solution', '', '2013-08-16 23:40:30');
INSERT INTO `using_log` VALUES (1803, 1, 'Solution', '', '2013-08-16 23:40:40');
INSERT INTO `using_log` VALUES (1804, 1, 'Solution', '', '2013-08-16 23:40:42');
INSERT INTO `using_log` VALUES (1805, 1, 'Solution', '', '2013-08-16 23:40:54');
INSERT INTO `using_log` VALUES (1806, 1, 'Solution', '', '2013-08-16 23:41:37');
INSERT INTO `using_log` VALUES (1807, 1, 'Solution', '', '2013-08-16 23:43:56');
INSERT INTO `using_log` VALUES (1808, 1, 'Solution', '', '2013-08-17 00:16:51');
INSERT INTO `using_log` VALUES (1809, 1, 'Solution', '', '2013-08-17 00:17:33');
INSERT INTO `using_log` VALUES (1810, 1, 'management', 'CheckAlertToSocialMedia', '2013-08-17 01:11:48');
INSERT INTO `using_log` VALUES (1811, 1, 'WebService', 'SendMail', '2013-08-17 01:13:47');
INSERT INTO `using_log` VALUES (1812, 1, 'WebService', 'SendMail', '2013-08-17 01:14:52');
INSERT INTO `using_log` VALUES (1813, 1, 'WebService', 'SendMail', '2013-08-17 01:17:05');
INSERT INTO `using_log` VALUES (1814, 1, 'WebService', 'SendMail', '2013-08-17 01:20:01');
INSERT INTO `using_log` VALUES (1815, 1, 'WebService', 'SendMail', '2013-08-17 01:20:27');
INSERT INTO `using_log` VALUES (1816, 1, '', '', '2013-08-17 10:52:02');
INSERT INTO `using_log` VALUES (1817, 1, 'RequestBooking', '', '2013-08-17 10:52:05');
INSERT INTO `using_log` VALUES (1818, 1, 'Solution', '', '2013-08-17 10:52:08');
INSERT INTO `using_log` VALUES (1819, 1, 'Solution', '', '2013-08-17 10:52:38');
INSERT INTO `using_log` VALUES (1820, 1, 'Solution', '', '2013-08-17 10:55:05');
INSERT INTO `using_log` VALUES (1821, 1, '', '', '2013-08-17 11:00:10');
INSERT INTO `using_log` VALUES (1822, 1, 'WebService', 'SendMail', '2013-08-17 14:40:10');
INSERT INTO `using_log` VALUES (1823, 1, 'WebService', 'SendMail', '2013-08-17 14:43:17');
INSERT INTO `using_log` VALUES (1824, 1, '', '', '2013-08-17 15:16:17');
INSERT INTO `using_log` VALUES (1825, 1, 'RequestBooking', '', '2013-08-17 15:16:21');
INSERT INTO `using_log` VALUES (1826, 1, 'Admin', '', '2013-08-17 15:16:23');
INSERT INTO `using_log` VALUES (1827, 1, 'EquipmentType', '', '2013-08-17 15:16:25');
INSERT INTO `using_log` VALUES (1828, 1, 'Equipment', '', '2013-08-17 15:16:36');
INSERT INTO `using_log` VALUES (1829, 1, 'EquipmentType', '', '2013-08-17 15:16:45');
INSERT INTO `using_log` VALUES (1830, 1, 'Equipment', '', '2013-08-17 15:17:32');
INSERT INTO `using_log` VALUES (1831, 1, 'EquipmentType', '', '2013-08-17 15:17:39');
INSERT INTO `using_log` VALUES (1832, 1, 'RequestBorrow', '', '2013-08-17 15:18:06');
INSERT INTO `using_log` VALUES (1833, 1, 'RequestService', '', '2013-08-17 15:18:07');
INSERT INTO `using_log` VALUES (1834, 1, 'RequestBooking', '', '2013-08-17 15:18:09');
INSERT INTO `using_log` VALUES (1835, 1, 'RequestBorrow', '', '2013-08-17 15:18:20');
INSERT INTO `using_log` VALUES (1836, 1, 'RequestBooking', '', '2013-08-17 15:18:40');
INSERT INTO `using_log` VALUES (1837, 1, 'RequestBooking', 'CheckStatus', '2013-08-17 15:18:42');
INSERT INTO `using_log` VALUES (1838, 1, 'RequestBooking', '', '2013-08-17 15:20:16');
INSERT INTO `using_log` VALUES (1839, 1, 'Report', '', '2013-08-17 15:20:26');
INSERT INTO `using_log` VALUES (1840, 1, 'Report', 'RequestBookingStatisticReport', '2013-08-17 15:20:38');
INSERT INTO `using_log` VALUES (1841, 1, 'Report', 'RequestBookingUsingStatisticReport', '2013-08-17 15:20:53');
INSERT INTO `using_log` VALUES (1842, 1, 'Report', 'RequestBorrowStatisticReport', '2013-08-17 15:21:02');
INSERT INTO `using_log` VALUES (1843, 1, 'Report', 'EquipmentCrackStatisticReport', '2013-08-17 15:21:07');
INSERT INTO `using_log` VALUES (1844, NULL, '', '', '2013-08-30 22:06:15');
INSERT INTO `using_log` VALUES (1845, 1, '', '', '2013-08-30 22:06:35');
INSERT INTO `using_log` VALUES (1846, 1, 'RequestBooking', '', '2013-08-30 22:06:38');
INSERT INTO `using_log` VALUES (1847, NULL, '', '', '2013-08-30 22:09:56');
INSERT INTO `using_log` VALUES (1848, 1, '', '', '2013-08-30 22:14:19');
INSERT INTO `using_log` VALUES (1849, 1, 'RequestBooking', '', '2013-08-30 22:14:22');
INSERT INTO `using_log` VALUES (1850, 1, 'Solution', '', '2013-08-30 22:14:25');
INSERT INTO `using_log` VALUES (1851, 1, 'Solution', '', '2013-08-30 22:19:22');
INSERT INTO `using_log` VALUES (1852, 1, 'Solution', '', '2013-08-30 22:20:16');
INSERT INTO `using_log` VALUES (1853, 1, 'Solution', '', '2013-08-30 22:21:03');
INSERT INTO `using_log` VALUES (1854, 1, 'Solution', '', '2013-08-30 22:22:42');
INSERT INTO `using_log` VALUES (1855, 1, 'Solution', '', '2013-08-30 22:23:51');
INSERT INTO `using_log` VALUES (1856, 1, 'Solution', '', '2013-08-30 22:30:22');
INSERT INTO `using_log` VALUES (1857, 1, 'Solution', '', '2013-08-30 22:43:46');
INSERT INTO `using_log` VALUES (1858, 1, 'Solution', '', '2013-08-30 22:58:02');
INSERT INTO `using_log` VALUES (1859, 1, 'Solution', '', '2013-08-30 23:08:25');
INSERT INTO `using_log` VALUES (1860, 1, 'Solution', '', '2013-08-30 23:24:42');
INSERT INTO `using_log` VALUES (1861, NULL, '', '', '2013-09-01 21:18:41');
INSERT INTO `using_log` VALUES (1862, NULL, '', '', '2013-09-01 21:23:22');
INSERT INTO `using_log` VALUES (1863, 1, '', '', '2013-09-01 21:23:29');
INSERT INTO `using_log` VALUES (1864, 1, 'RequestBooking', '', '2013-09-01 21:23:32');
INSERT INTO `using_log` VALUES (1865, 1, 'Solution', '', '2013-09-01 21:23:37');
INSERT INTO `using_log` VALUES (1866, 1, 'Solution', '', '2013-09-01 21:24:04');
INSERT INTO `using_log` VALUES (1867, 1, 'Solution', '', '2013-09-01 21:58:19');
INSERT INTO `using_log` VALUES (1868, 1, 'Solution', '', '2013-09-01 22:04:13');
INSERT INTO `using_log` VALUES (1869, 1, 'Solution', '', '2013-09-01 22:21:43');
INSERT INTO `using_log` VALUES (1870, 1, 'Solution', '', '2013-09-01 22:22:23');
INSERT INTO `using_log` VALUES (1871, 1, 'Solution', '', '2013-09-01 22:23:11');
INSERT INTO `using_log` VALUES (1872, 1, 'Solution', '', '2013-09-01 22:52:22');
INSERT INTO `using_log` VALUES (1873, 1, 'Solution', '', '2013-09-01 22:53:54');
INSERT INTO `using_log` VALUES (1874, 1, 'Solution', '', '2013-09-01 22:56:19');
INSERT INTO `using_log` VALUES (1875, 1, 'Solution', '', '2013-09-01 22:57:00');
INSERT INTO `using_log` VALUES (1876, 1, 'Solution', '', '2013-09-01 22:58:11');
INSERT INTO `using_log` VALUES (1877, 1, 'Solution', '', '2013-09-01 22:59:14');
INSERT INTO `using_log` VALUES (1878, 1, 'Solution', '', '2013-09-01 22:59:34');
INSERT INTO `using_log` VALUES (1879, 1, 'Solution', '', '2013-09-01 23:00:07');
INSERT INTO `using_log` VALUES (1880, 1, 'Solution', '', '2013-09-01 23:01:50');
INSERT INTO `using_log` VALUES (1881, 1, 'Solution', '', '2013-09-01 23:02:06');
INSERT INTO `using_log` VALUES (1882, 1, 'Solution', '', '2013-09-01 23:02:50');
INSERT INTO `using_log` VALUES (1883, 1, 'Solution', '', '2013-09-01 23:03:43');
INSERT INTO `using_log` VALUES (1884, 1, 'Solution', '', '2013-09-01 23:04:16');
INSERT INTO `using_log` VALUES (1885, 1, 'Solution', '', '2013-09-01 23:04:39');
INSERT INTO `using_log` VALUES (1886, 1, 'Solution', '', '2013-09-01 23:05:18');
INSERT INTO `using_log` VALUES (1887, 1, 'Solution', '', '2013-09-01 23:05:41');
INSERT INTO `using_log` VALUES (1888, 1, 'Solution', '', '2013-09-01 23:06:06');
INSERT INTO `using_log` VALUES (1889, 1, 'Solution', '', '2013-09-01 23:06:33');
INSERT INTO `using_log` VALUES (1890, 1, '', '', '2013-09-02 09:05:25');
INSERT INTO `using_log` VALUES (1891, 1, '', '', '2013-09-02 09:05:25');
INSERT INTO `using_log` VALUES (1892, 1, 'RequestBooking', '', '2013-09-02 09:05:28');
INSERT INTO `using_log` VALUES (1893, 1, 'Admin', '', '2013-09-02 09:05:30');
INSERT INTO `using_log` VALUES (1894, 1, 'Equipment', '', '2013-09-02 09:05:32');
INSERT INTO `using_log` VALUES (1895, 1, 'equipment', 'create', '2013-09-02 09:05:35');
INSERT INTO `using_log` VALUES (1896, 1, 'equipment', 'create', '2013-09-02 09:05:57');
INSERT INTO `using_log` VALUES (1897, 1, 'equipment', 'create', '2013-09-02 09:06:12');
INSERT INTO `using_log` VALUES (1898, 1, 'equipment', 'create', '2013-09-02 09:06:19');
INSERT INTO `using_log` VALUES (1899, 1, 'Equipment', '', '2013-09-02 09:06:23');
INSERT INTO `using_log` VALUES (1900, 1, 'equipment', 'update', '2013-09-02 09:06:26');
INSERT INTO `using_log` VALUES (1901, 1, 'Equipment', '', '2013-09-02 09:08:48');
INSERT INTO `using_log` VALUES (1902, 1, 'equipment', 'update', '2013-09-02 09:08:51');
INSERT INTO `using_log` VALUES (1903, 1, 'Equipment', '', '2013-09-02 09:09:22');
INSERT INTO `using_log` VALUES (1904, 1, 'equipment', 'create', '2013-09-02 09:09:24');
INSERT INTO `using_log` VALUES (1905, 1, 'Equipment', '', '2013-09-02 09:09:48');
INSERT INTO `using_log` VALUES (1906, 1, 'equipment', 'update', '2013-09-02 09:09:53');
INSERT INTO `using_log` VALUES (1907, 1, 'Equipment', '', '2013-09-02 09:09:55');
INSERT INTO `using_log` VALUES (1908, 1, '', '', '2013-09-02 09:40:07');
INSERT INTO `using_log` VALUES (1909, NULL, '', '', '2013-09-02 09:42:59');
INSERT INTO `using_log` VALUES (1910, 1, '', '', '2013-09-02 09:45:08');
INSERT INTO `using_log` VALUES (1911, 1, '', '', '2013-09-02 09:45:14');
INSERT INTO `using_log` VALUES (1912, 1, 'Management', 'EditProfile', '2013-09-02 09:45:18');
INSERT INTO `using_log` VALUES (1913, 1, 'RequestBooking', '', '2013-09-02 09:45:20');
INSERT INTO `using_log` VALUES (1914, NULL, '', '', '2013-09-02 09:45:26');
INSERT INTO `using_log` VALUES (1915, 1, '', '', '2013-09-02 09:47:25');
INSERT INTO `using_log` VALUES (1916, NULL, '', '', '2013-09-02 09:47:31');
INSERT INTO `using_log` VALUES (1917, 1, '', '', '2013-09-02 09:48:30');
INSERT INTO `using_log` VALUES (1918, NULL, '', '', '2013-09-02 09:48:36');
INSERT INTO `using_log` VALUES (1919, 1, '', '', '2013-09-02 09:52:30');
INSERT INTO `using_log` VALUES (1920, NULL, '', '', '2013-09-02 09:53:04');
INSERT INTO `using_log` VALUES (1921, NULL, '', '', '2013-09-02 09:53:04');
INSERT INTO `using_log` VALUES (1922, 1, '', '', '2013-09-02 10:02:24');
INSERT INTO `using_log` VALUES (1923, 1, '', '', '2013-09-02 10:02:24');
INSERT INTO `using_log` VALUES (1924, NULL, '', '', '2013-09-02 10:02:34');
INSERT INTO `using_log` VALUES (1925, 1, '', '', '2013-09-02 10:02:44');
INSERT INTO `using_log` VALUES (1926, 1, '', '', '2013-09-02 10:02:44');
INSERT INTO `using_log` VALUES (1927, NULL, '', '', '2013-09-02 10:03:28');
INSERT INTO `using_log` VALUES (1928, NULL, '', '', '2013-09-02 10:03:30');
INSERT INTO `using_log` VALUES (1929, NULL, '', '', '2013-09-02 10:03:30');
INSERT INTO `using_log` VALUES (1930, NULL, '', '', '2013-09-02 10:03:31');
INSERT INTO `using_log` VALUES (1931, NULL, '', '', '2013-09-02 10:03:31');
INSERT INTO `using_log` VALUES (1932, NULL, '', '', '2013-09-02 10:03:31');
INSERT INTO `using_log` VALUES (1933, NULL, '', '', '2013-09-02 10:03:31');
INSERT INTO `using_log` VALUES (1934, 1, '', '', '2013-09-02 10:05:35');
INSERT INTO `using_log` VALUES (1935, 1, '', '', '2013-09-02 10:05:36');
INSERT INTO `using_log` VALUES (1936, 1, '', '', '2013-09-02 10:05:38');
INSERT INTO `using_log` VALUES (1937, NULL, '', '', '2013-09-02 10:05:51');
INSERT INTO `using_log` VALUES (1938, NULL, '', '', '2013-09-02 10:06:02');
INSERT INTO `using_log` VALUES (1939, 1, '', '', '2013-09-02 10:42:06');
INSERT INTO `using_log` VALUES (1940, 1, '', '', '2013-09-02 10:50:43');
INSERT INTO `using_log` VALUES (1941, 1, 'RequestBooking', '', '2013-09-02 10:50:46');
INSERT INTO `using_log` VALUES (1942, 1, 'Solution', '', '2013-09-02 10:50:49');
INSERT INTO `using_log` VALUES (1943, 1, 'Solution', '', '2013-09-02 10:51:20');
INSERT INTO `using_log` VALUES (1944, 1, '', '', '2013-09-02 10:51:21');
INSERT INTO `using_log` VALUES (1945, 1, '', '', '2013-09-02 10:51:21');
INSERT INTO `using_log` VALUES (1946, 1, '', '', '2013-09-02 11:27:27');
INSERT INTO `using_log` VALUES (1947, 1, '', '', '2013-09-02 12:51:05');
INSERT INTO `using_log` VALUES (1948, 1, '', '', '2013-09-02 12:57:36');
INSERT INTO `using_log` VALUES (1949, 1, 'Management', 'EditProfile', '2013-09-02 13:00:38');
INSERT INTO `using_log` VALUES (1950, 1, 'RequestBooking', '', '2013-09-02 13:00:41');
INSERT INTO `using_log` VALUES (1951, 1, 'Solution', '', '2013-09-02 13:00:44');
INSERT INTO `using_log` VALUES (1952, 1, 'Admin', '', '2013-09-02 13:00:48');
INSERT INTO `using_log` VALUES (1953, 1, 'Equipment', '', '2013-09-02 13:00:50');
INSERT INTO `using_log` VALUES (1954, 1, 'Equipment', '', '2013-09-02 13:00:56');
INSERT INTO `using_log` VALUES (1955, 1, 'equipment', 'update', '2013-09-02 13:01:00');
INSERT INTO `using_log` VALUES (1956, 1, 'Equipment', '', '2013-09-02 13:01:33');
INSERT INTO `using_log` VALUES (1957, 1, 'Solution', '', '2013-09-02 13:01:43');
INSERT INTO `using_log` VALUES (1958, 1, 'Solution', '', '2013-09-02 13:01:53');
INSERT INTO `using_log` VALUES (1959, 1, '', '', '2013-09-02 14:36:31');
INSERT INTO `using_log` VALUES (1960, 1, 'Management', 'EditProfile', '2013-09-02 14:36:37');
INSERT INTO `using_log` VALUES (1961, 1, 'RequestBooking', '', '2013-09-02 14:36:40');
INSERT INTO `using_log` VALUES (1962, 1, 'Solution', '', '2013-09-02 14:36:45');
INSERT INTO `using_log` VALUES (1963, 1, 'Admin', '', '2013-09-02 14:36:49');
INSERT INTO `using_log` VALUES (1964, 1, 'Equipment', '', '2013-09-02 14:36:51');
INSERT INTO `using_log` VALUES (1965, 1, 'Equipment', '', '2013-09-02 14:36:56');
INSERT INTO `using_log` VALUES (1966, 1, 'equipment', 'update', '2013-09-02 14:36:59');
INSERT INTO `using_log` VALUES (1967, 1, 'Equipment', '', '2013-09-02 14:38:29');
INSERT INTO `using_log` VALUES (1968, 1, 'Equipment', '', '2013-09-02 14:38:33');
INSERT INTO `using_log` VALUES (1969, 1, 'equipment', 'update', '2013-09-02 14:38:37');
INSERT INTO `using_log` VALUES (1970, 1, 'Equipment', '', '2013-09-02 14:38:46');
INSERT INTO `using_log` VALUES (1971, 1, 'equipment', 'create', '2013-09-02 14:38:47');
INSERT INTO `using_log` VALUES (1972, 1, 'Solution', '', '2013-09-02 14:38:51');
INSERT INTO `using_log` VALUES (1973, 1, 'Solution', '', '2013-09-02 14:39:02');
INSERT INTO `using_log` VALUES (1974, NULL, '', '', '2013-09-23 09:34:17');
INSERT INTO `using_log` VALUES (1975, 1, 'management', '', '2013-09-23 09:34:45');
INSERT INTO `using_log` VALUES (1976, 1, 'Solution', '', '2013-09-23 09:38:11');
INSERT INTO `using_log` VALUES (1977, 1, 'Solution', '', '2013-09-23 09:38:38');
INSERT INTO `using_log` VALUES (1978, 1, '', '', '2013-09-23 09:44:21');
INSERT INTO `using_log` VALUES (1979, NULL, 'WebService', 'GetEquipmentInRoom', '2013-09-26 15:13:09');
INSERT INTO `using_log` VALUES (1980, NULL, 'WebService', 'SendMail', '2013-09-26 15:13:20');
INSERT INTO `using_log` VALUES (1981, NULL, 'WebService', 'SendMail', '2013-09-26 15:13:37');
INSERT INTO `using_log` VALUES (1982, NULL, '', '', '2013-09-26 15:18:07');
INSERT INTO `using_log` VALUES (1983, 1, '', '', '2013-09-26 15:30:08');
INSERT INTO `using_log` VALUES (1984, NULL, '', '', '2013-09-26 15:34:10');
INSERT INTO `using_log` VALUES (1985, 1, '', '', '2013-09-26 15:38:45');
INSERT INTO `using_log` VALUES (1986, NULL, '', '', '2013-09-26 15:38:51');
INSERT INTO `using_log` VALUES (1987, 1, '', '', '2013-09-26 15:43:18');
INSERT INTO `using_log` VALUES (1988, NULL, '', '', '2013-09-26 15:43:47');
INSERT INTO `using_log` VALUES (1989, NULL, '', '', '2013-09-26 15:43:50');
INSERT INTO `using_log` VALUES (1990, NULL, 'management', 'login', '2013-09-26 15:45:44');
INSERT INTO `using_log` VALUES (1991, NULL, '', '', '2013-09-26 15:47:53');
INSERT INTO `using_log` VALUES (1992, NULL, '', '', '2013-09-26 15:52:32');
INSERT INTO `using_log` VALUES (1993, 1, '', '', '2013-09-26 16:19:03');
INSERT INTO `using_log` VALUES (1994, 1, '', '', '2013-09-26 16:19:03');
INSERT INTO `using_log` VALUES (1995, NULL, '', '', '2013-09-26 16:19:10');
INSERT INTO `using_log` VALUES (1996, NULL, '', '', '2013-09-26 16:29:11');
INSERT INTO `using_log` VALUES (1997, 1, '', '', '2013-09-26 16:29:52');
INSERT INTO `using_log` VALUES (1998, NULL, 'management', 'login', '2013-09-26 16:29:56');
INSERT INTO `using_log` VALUES (1999, 1, 'RequestBooking', '', '2013-09-26 16:30:17');
INSERT INTO `using_log` VALUES (2000, NULL, 'management', 'login', '2013-09-26 16:30:44');
INSERT INTO `using_log` VALUES (2001, NULL, 'management', 'login', '2013-09-26 16:31:53');
INSERT INTO `using_log` VALUES (2002, 21, '', '', '2013-09-26 16:34:50');
INSERT INTO `using_log` VALUES (2003, NULL, 'management', 'login', '2013-09-26 16:34:52');
INSERT INTO `using_log` VALUES (2004, 21, 'RequestBooking', '', '2013-09-26 16:36:08');
INSERT INTO `using_log` VALUES (2005, 21, 'RequestBooking', '', '2013-09-26 16:36:10');
INSERT INTO `using_log` VALUES (2006, 21, '', '', '2013-09-26 16:36:18');
INSERT INTO `using_log` VALUES (2007, 21, '', '', '2013-09-26 16:36:19');
INSERT INTO `using_log` VALUES (2008, 21, '', '', '2013-09-26 16:36:20');
INSERT INTO `using_log` VALUES (2009, 21, '', '', '2013-09-26 16:36:22');
INSERT INTO `using_log` VALUES (2010, 21, '', '', '2013-09-26 16:36:22');
INSERT INTO `using_log` VALUES (2011, 21, '', '', '2013-09-26 16:37:20');
INSERT INTO `using_log` VALUES (2012, NULL, '', '', '2013-09-26 16:37:25');
INSERT INTO `using_log` VALUES (2013, 1, '', '', '2013-09-26 16:37:33');
INSERT INTO `using_log` VALUES (2014, 1, 'RequestBooking', '', '2013-09-26 16:37:36');
INSERT INTO `using_log` VALUES (2015, 1, 'RequestBorrow', '', '2013-09-26 16:37:38');
INSERT INTO `using_log` VALUES (2016, 1, 'RequestService', '', '2013-09-26 16:37:38');
INSERT INTO `using_log` VALUES (2017, 1, 'Solution', '', '2013-09-26 16:37:40');
INSERT INTO `using_log` VALUES (2018, 1, 'Report', '', '2013-09-26 16:37:40');
INSERT INTO `using_log` VALUES (2019, 1, 'Admin', '', '2013-09-26 16:37:41');
INSERT INTO `using_log` VALUES (2020, 1, '', '', '2013-09-26 16:40:17');
INSERT INTO `using_log` VALUES (2021, NULL, 'management', 'login', '2013-09-26 16:40:20');
INSERT INTO `using_log` VALUES (2022, 1, 'RequestBooking', '', '2013-09-26 16:40:27');
INSERT INTO `using_log` VALUES (2023, 1, 'RequestBorrow', '', '2013-09-26 16:40:28');
INSERT INTO `using_log` VALUES (2024, 1, 'RequestService', '', '2013-09-26 16:40:29');
INSERT INTO `using_log` VALUES (2025, 1, 'Admin', '', '2013-09-26 16:40:30');
INSERT INTO `using_log` VALUES (2026, 1, 'Report', '', '2013-09-26 16:40:31');
INSERT INTO `using_log` VALUES (2027, 1, 'Solution', '', '2013-09-26 16:40:32');
INSERT INTO `using_log` VALUES (2028, 1, 'Solution', '', '2013-09-26 16:40:35');
INSERT INTO `using_log` VALUES (2029, 1, 'Solution', '', '2013-09-26 16:41:09');
INSERT INTO `using_log` VALUES (2030, NULL, 'management', 'login', '2013-09-26 16:41:11');
INSERT INTO `using_log` VALUES (2031, NULL, 'Management', 'Register', '2013-09-26 16:41:13');
INSERT INTO `using_log` VALUES (2032, 1, 'RequestBooking', '', '2013-09-26 16:41:20');
INSERT INTO `using_log` VALUES (2033, 1, 'RequestBooking', '', '2013-09-26 16:41:34');
INSERT INTO `using_log` VALUES (2034, NULL, 'management', 'login', '2013-09-26 16:41:36');
INSERT INTO `using_log` VALUES (2035, 1, 'RequestBooking', '', '2013-09-26 16:41:42');
INSERT INTO `using_log` VALUES (2036, NULL, 'management', 'login', '2013-09-26 16:41:47');
INSERT INTO `using_log` VALUES (2037, NULL, 'management', 'login', '2013-09-26 16:42:12');
INSERT INTO `using_log` VALUES (2038, 1, 'RequestBooking', '', '2013-09-26 16:42:16');
INSERT INTO `using_log` VALUES (2039, 1, 'RequestBooking', '', '2013-09-26 16:42:53');
INSERT INTO `using_log` VALUES (2040, 1, 'RequestBooking', '', '2013-09-26 16:42:56');
INSERT INTO `using_log` VALUES (2041, NULL, 'management', 'login', '2013-09-26 16:42:58');
INSERT INTO `using_log` VALUES (2042, NULL, 'management', 'login', '2013-09-26 16:43:00');
INSERT INTO `using_log` VALUES (2043, NULL, '', '', '2013-09-26 16:43:03');
INSERT INTO `using_log` VALUES (2044, 1, 'RequestBooking', '', '2013-09-26 16:43:06');
INSERT INTO `using_log` VALUES (2045, 1, 'RequestBooking', '', '2013-09-26 16:43:30');
INSERT INTO `using_log` VALUES (2046, NULL, 'management', 'login', '2013-09-26 16:43:32');
INSERT INTO `using_log` VALUES (2047, 1, 'RequestBooking', '', '2013-09-26 16:43:37');
INSERT INTO `using_log` VALUES (2048, NULL, 'management', 'login', '2013-09-26 16:43:42');
INSERT INTO `using_log` VALUES (2049, NULL, 'management', 'login', '2013-09-26 16:43:58');
INSERT INTO `using_log` VALUES (2050, 1, '', '', '2013-09-26 16:47:59');
INSERT INTO `using_log` VALUES (2051, NULL, 'management', 'login', '2013-09-26 16:48:01');
INSERT INTO `using_log` VALUES (2052, NULL, 'management', 'login', '2013-09-26 16:48:59');
INSERT INTO `using_log` VALUES (2053, 21, 'RequestBooking', '', '2013-09-26 16:52:09');
INSERT INTO `using_log` VALUES (2054, NULL, 'management', 'login', '2013-09-26 16:52:11');
INSERT INTO `using_log` VALUES (2055, 21, 'RequestBooking', '', '2013-09-26 16:52:14');
INSERT INTO `using_log` VALUES (2056, NULL, 'management', 'login', '2013-09-26 16:52:17');
INSERT INTO `using_log` VALUES (2057, 1, 'RequestBooking', '', '2013-09-26 16:52:29');
INSERT INTO `using_log` VALUES (2058, 1, 'RequestBooking', '', '2013-09-26 16:55:00');
INSERT INTO `using_log` VALUES (2059, NULL, 'management', 'login', '2013-09-26 16:55:02');
INSERT INTO `using_log` VALUES (2060, 1, 'RequestBooking', '', '2013-09-26 16:55:06');
INSERT INTO `using_log` VALUES (2061, 1, 'RequestBooking', '', '2013-09-26 16:55:27');
INSERT INTO `using_log` VALUES (2062, NULL, 'management', 'login', '2013-09-26 16:56:36');
INSERT INTO `using_log` VALUES (2063, 1, 'RequestBooking', '', '2013-09-26 16:56:41');
INSERT INTO `using_log` VALUES (2064, NULL, 'management', 'login', '2013-09-26 16:56:43');
INSERT INTO `using_log` VALUES (2065, NULL, 'management', 'login', '2013-09-26 16:57:24');
INSERT INTO `using_log` VALUES (2066, 1, 'RequestBooking', '', '2013-09-26 16:57:27');
INSERT INTO `using_log` VALUES (2067, 1, 'RequestBooking', '', '2013-09-26 17:04:44');
INSERT INTO `using_log` VALUES (2068, NULL, 'management', 'login', '2013-09-26 17:04:46');
INSERT INTO `using_log` VALUES (2069, 1, 'RequestBooking', '', '2013-09-26 17:04:50');
INSERT INTO `using_log` VALUES (2070, NULL, 'management', 'login', '2013-09-26 17:04:52');
INSERT INTO `using_log` VALUES (2071, 1, 'RequestBooking', '', '2013-09-26 17:05:10');
INSERT INTO `using_log` VALUES (2072, NULL, '', '', '2013-09-26 17:05:29');
INSERT INTO `using_log` VALUES (2073, 1, '', '', '2013-09-26 17:06:15');
INSERT INTO `using_log` VALUES (2074, NULL, 'management', 'login', '2013-09-26 17:06:17');
INSERT INTO `using_log` VALUES (2075, 1, 'RequestBooking', '', '2013-09-26 17:06:22');
INSERT INTO `using_log` VALUES (2076, NULL, 'management', 'login', '2013-09-26 17:06:24');
INSERT INTO `using_log` VALUES (2077, NULL, '', '', '2013-09-26 17:07:49');
INSERT INTO `using_log` VALUES (2078, NULL, '', '', '2013-09-26 17:07:49');
INSERT INTO `using_log` VALUES (2079, 1, 'RequestBooking', '', '2013-09-26 17:07:54');
INSERT INTO `using_log` VALUES (2080, 1, 'RequestBooking', '', '2013-09-26 17:09:25');
INSERT INTO `using_log` VALUES (2081, NULL, 'management', 'login', '2013-09-26 17:09:29');
INSERT INTO `using_log` VALUES (2082, 1, 'RequestBooking', '', '2013-09-26 17:09:33');
INSERT INTO `using_log` VALUES (2083, NULL, 'management', 'login', '2013-09-26 17:09:35');
INSERT INTO `using_log` VALUES (2084, NULL, '', '', '2013-09-26 17:11:17');
INSERT INTO `using_log` VALUES (2085, 1, 'RequestBooking', '', '2013-09-26 17:11:23');
INSERT INTO `using_log` VALUES (2086, NULL, 'management', 'login', '2013-09-26 17:11:28');
INSERT INTO `using_log` VALUES (2087, NULL, 'management', 'Login', '2013-09-26 17:11:30');
INSERT INTO `using_log` VALUES (2088, 21, 'RequestBooking', '', '2013-09-26 17:11:32');
INSERT INTO `using_log` VALUES (2089, NULL, '', '', '2013-09-26 22:59:51');
INSERT INTO `using_log` VALUES (2090, 1, 'RequestBooking', '', '2013-09-26 22:59:56');
INSERT INTO `using_log` VALUES (2091, 1, 'RequestBorrow', '', '2013-09-26 23:00:10');
INSERT INTO `using_log` VALUES (2092, 1, 'Solution', '', '2013-09-26 23:00:18');
INSERT INTO `using_log` VALUES (2093, 1, 'Report', '', '2013-09-26 23:00:19');
INSERT INTO `using_log` VALUES (2094, 1, 'Report', '', '2013-09-26 23:00:21');
INSERT INTO `using_log` VALUES (2095, 1, 'Admin', '', '2013-09-26 23:00:21');
INSERT INTO `using_log` VALUES (2096, 1, '', '', '2013-09-26 23:00:31');
INSERT INTO `using_log` VALUES (2097, 1, 'Site', 'AllNews', '2013-09-26 23:00:37');
INSERT INTO `using_log` VALUES (2098, 1, 'Site', 'AllNews', '2013-09-26 23:00:44');
INSERT INTO `using_log` VALUES (2099, 1, 'Site', 'AllNews', '2013-09-26 23:00:49');
INSERT INTO `using_log` VALUES (2100, 1, 'Site', 'AllNews', '2013-09-26 23:00:59');
INSERT INTO `using_log` VALUES (2101, 1, '', '', '2013-09-26 23:01:19');
INSERT INTO `using_log` VALUES (2102, 1, 'WebService', 'SendMail', '2013-09-26 23:01:33');
INSERT INTO `using_log` VALUES (2103, NULL, '', '', '2013-09-29 15:18:17');
INSERT INTO `using_log` VALUES (2104, 1, 'RequestBooking', '', '2013-09-29 15:18:22');
INSERT INTO `using_log` VALUES (2105, 1, 'Admin', '', '2013-09-29 15:18:25');
INSERT INTO `using_log` VALUES (2106, 1, 'Department', '', '2013-09-29 15:18:27');
INSERT INTO `using_log` VALUES (2107, 1, 'department', 'index', '2013-09-29 15:18:34');
INSERT INTO `using_log` VALUES (2108, 1, 'department', 'index', '2013-09-29 15:18:34');
INSERT INTO `using_log` VALUES (2109, 1, 'department', 'index', '2013-09-29 15:18:35');
INSERT INTO `using_log` VALUES (2110, 1, 'department', 'index', '2013-09-29 15:18:44');
INSERT INTO `using_log` VALUES (2111, 1, 'department', 'index', '2013-09-29 15:19:14');
INSERT INTO `using_log` VALUES (2112, 1, 'department', 'index', '2013-09-29 15:19:16');
INSERT INTO `using_log` VALUES (2113, 1, 'department', 'index', '2013-09-29 15:19:19');
INSERT INTO `using_log` VALUES (2114, 1, 'department', 'index', '2013-09-29 15:19:20');
INSERT INTO `using_log` VALUES (2115, 1, 'department', 'index', '2013-09-29 15:19:21');
INSERT INTO `using_log` VALUES (2116, 1, 'department', 'index', '2013-09-29 15:19:23');
INSERT INTO `using_log` VALUES (2117, 1, 'department', 'index', '2013-09-29 15:19:43');
INSERT INTO `using_log` VALUES (2118, 1, 'department', 'index', '2013-09-29 15:19:44');
INSERT INTO `using_log` VALUES (2119, 1, 'department', 'index', '2013-09-29 15:19:45');
INSERT INTO `using_log` VALUES (2120, 1, 'RequestBooking', '', '2013-09-29 15:20:38');
INSERT INTO `using_log` VALUES (2121, 1, 'RequestBorrow', '', '2013-09-29 15:20:42');
INSERT INTO `using_log` VALUES (2122, 1, 'RequestBorrow', '', '2013-09-29 15:20:44');
INSERT INTO `using_log` VALUES (2123, 1, 'RequestBorrow', '', '2013-09-29 15:20:44');
INSERT INTO `using_log` VALUES (2124, 1, 'RequestBorrow', 'Request', '2013-09-29 15:20:50');
INSERT INTO `using_log` VALUES (2125, 1, 'RequestService', '', '2013-09-29 15:20:54');
INSERT INTO `using_log` VALUES (2126, 1, 'RequestService', 'Request', '2013-09-29 15:20:56');
INSERT INTO `using_log` VALUES (2127, 1, 'RequestService', '', '2013-09-29 15:25:24');
INSERT INTO `using_log` VALUES (2128, 1, 'RequestBorrow', 'Request', '2013-09-29 15:25:26');
INSERT INTO `using_log` VALUES (2129, 1, 'RequestBorrow', '', '2013-09-29 15:25:28');
INSERT INTO `using_log` VALUES (2130, 1, 'Admin', '', '2013-09-29 15:25:38');
INSERT INTO `using_log` VALUES (2131, 1, 'Department', '', '2013-09-29 15:25:40');
INSERT INTO `using_log` VALUES (2132, 1, 'department', 'index', '2013-09-29 15:25:42');
INSERT INTO `using_log` VALUES (2133, 1, 'RequestBooking', '', '2013-09-29 15:52:48');
INSERT INTO `using_log` VALUES (2134, 1, 'RequestBorrow', '', '2013-09-29 15:52:50');
INSERT INTO `using_log` VALUES (2135, 1, 'RequestBorrow', 'CheckStatus', '2013-09-29 15:52:53');
INSERT INTO `using_log` VALUES (2136, 1, 'RequestBooking', '', '2013-09-29 15:52:55');
INSERT INTO `using_log` VALUES (2137, 1, 'RequestBooking', 'CheckStatus', '2013-09-29 15:52:56');
INSERT INTO `using_log` VALUES (2138, NULL, '', '', '2013-10-04 22:06:29');
INSERT INTO `using_log` VALUES (2139, 1, 'RequestBooking', '', '2013-10-04 22:06:33');
INSERT INTO `using_log` VALUES (2140, 1, 'Solution', '', '2013-10-04 22:06:36');
INSERT INTO `using_log` VALUES (2141, 1, 'Management', 'Shutdown', '2013-10-04 22:06:43');
INSERT INTO `using_log` VALUES (2142, 1, 'Admin', '', '2013-10-04 22:10:44');
INSERT INTO `using_log` VALUES (2143, 1, 'Equipment', '', '2013-10-04 22:10:46');
INSERT INTO `using_log` VALUES (2144, 1, 'equipment', 'update', '2013-10-04 22:10:52');
INSERT INTO `using_log` VALUES (2145, 1, 'equipment', 'update', '2013-10-04 22:23:04');
INSERT INTO `using_log` VALUES (2146, 1, 'Equipment', '', '2013-10-04 22:23:06');
INSERT INTO `using_log` VALUES (2147, 1, 'Solution', '', '2013-10-04 22:23:08');
INSERT INTO `using_log` VALUES (2148, 1, 'Solution', '', '2013-10-04 22:25:32');
INSERT INTO `using_log` VALUES (2149, 1, 'Admin', '', '2013-10-04 22:27:48');
INSERT INTO `using_log` VALUES (2150, 1, 'Equipment', '', '2013-10-04 22:27:49');
INSERT INTO `using_log` VALUES (2151, 1, 'equipment', 'update', '2013-10-04 22:27:55');
INSERT INTO `using_log` VALUES (2152, 1, 'Equipment', '', '2013-10-04 22:29:15');
INSERT INTO `using_log` VALUES (2153, 1, 'Solution', '', '2013-10-04 22:32:39');
INSERT INTO `using_log` VALUES (2154, NULL, '', '', '2013-10-04 22:46:20');
INSERT INTO `using_log` VALUES (2155, 1, 'RequestBooking', '', '2013-10-04 22:46:25');
INSERT INTO `using_log` VALUES (2156, 1, 'Admin', '', '2013-10-04 22:46:28');
INSERT INTO `using_log` VALUES (2157, 1, 'Solution', '', '2013-10-04 22:46:40');
INSERT INTO `using_log` VALUES (2158, 1, 'Admin', '', '2013-10-04 22:46:40');
INSERT INTO `using_log` VALUES (2159, 1, 'Equipment', '', '2013-10-04 22:46:46');
INSERT INTO `using_log` VALUES (2160, 1, 'equipment', 'update', '2013-10-04 22:46:48');
INSERT INTO `using_log` VALUES (2161, 1, 'Equipment', '', '2013-10-04 22:46:52');
INSERT INTO `using_log` VALUES (2162, 1, 'Solution', '', '2013-10-04 22:47:02');
INSERT INTO `using_log` VALUES (2163, 1, 'Solution', '', '2013-10-04 22:47:28');
INSERT INTO `using_log` VALUES (2164, 1, 'Management', 'Shutdown', '2013-10-04 22:47:30');
INSERT INTO `using_log` VALUES (2165, 1, 'Solution', '', '2013-10-04 22:47:55');
INSERT INTO `using_log` VALUES (2166, NULL, '', '', '2013-10-04 22:50:28');
INSERT INTO `using_log` VALUES (2167, 1, 'RequestBooking', '', '2013-10-04 22:50:33');
INSERT INTO `using_log` VALUES (2168, 1, 'Solution', '', '2013-10-04 22:50:43');
INSERT INTO `using_log` VALUES (2169, NULL, '', '', '2013-10-05 11:42:27');
INSERT INTO `using_log` VALUES (2170, NULL, '', '', '2013-10-05 12:12:56');
INSERT INTO `using_log` VALUES (2171, NULL, '', '', '2013-10-05 14:24:19');
INSERT INTO `using_log` VALUES (2172, 1, 'RequestBooking', '', '2013-10-05 14:24:23');
INSERT INTO `using_log` VALUES (2173, 1, 'Solution', '', '2013-10-05 14:24:35');
INSERT INTO `using_log` VALUES (2174, 1, 'Solution', '', '2013-10-05 14:24:52');
INSERT INTO `using_log` VALUES (2175, 1, 'Management', 'Shutdown', '2013-10-05 14:24:54');
INSERT INTO `using_log` VALUES (2176, 1, 'Solution', '', '2013-10-05 14:26:16');
INSERT INTO `using_log` VALUES (2177, 1, 'Management', 'Shutdown', '2013-10-05 14:26:17');
INSERT INTO `using_log` VALUES (2178, 1, 'Solution', '', '2013-10-05 14:27:28');
INSERT INTO `using_log` VALUES (2179, 1, 'Management', 'Shutdown', '2013-10-05 14:27:30');
INSERT INTO `using_log` VALUES (2180, 1, 'Solution', '', '2013-10-05 14:28:17');
INSERT INTO `using_log` VALUES (2181, 1, 'Management', 'Shutdown', '2013-10-05 14:28:20');
INSERT INTO `using_log` VALUES (2182, 1, 'Solution', '', '2013-10-05 14:28:30');
INSERT INTO `using_log` VALUES (2183, 1, 'Management', 'Shutdown', '2013-10-05 14:28:31');
INSERT INTO `using_log` VALUES (2184, 1, 'Solution', '', '2013-10-05 14:28:54');
INSERT INTO `using_log` VALUES (2185, 1, 'Management', 'Shutdown', '2013-10-05 14:28:55');
INSERT INTO `using_log` VALUES (2186, 1, '', '', '2013-10-05 15:00:34');
INSERT INTO `using_log` VALUES (2187, 1, '', '', '2013-10-05 15:04:27');
INSERT INTO `using_log` VALUES (2188, 1, '', '', '2013-10-05 15:04:27');
INSERT INTO `using_log` VALUES (2189, 1, 'RequestBooking', '', '2013-10-05 15:04:31');
INSERT INTO `using_log` VALUES (2190, 1, 'Solution', '', '2013-10-05 15:04:43');
INSERT INTO `using_log` VALUES (2191, 1, 'Solution', '', '2013-10-05 15:05:50');
INSERT INTO `using_log` VALUES (2192, 1, 'Admin', '', '2013-10-05 15:05:50');
INSERT INTO `using_log` VALUES (2193, 1, 'Equipment', '', '2013-10-05 15:05:52');
INSERT INTO `using_log` VALUES (2194, 1, 'Equipment', '', '2013-10-05 15:05:55');
INSERT INTO `using_log` VALUES (2195, 1, 'equipment', 'update', '2013-10-05 15:06:11');
INSERT INTO `using_log` VALUES (2196, 1, 'Equipment', '', '2013-10-05 15:06:26');
INSERT INTO `using_log` VALUES (2197, 1, 'Solution', '', '2013-10-05 15:06:41');
INSERT INTO `using_log` VALUES (2198, 1, 'Solution', '', '2013-10-05 15:07:08');
INSERT INTO `using_log` VALUES (2199, 1, 'Solution', '', '2013-10-05 15:09:55');
INSERT INTO `using_log` VALUES (2200, 1, 'Solution', '', '2013-10-05 15:26:13');
INSERT INTO `using_log` VALUES (2201, 1, 'Solution', '', '2013-10-05 15:28:47');
INSERT INTO `using_log` VALUES (2202, 1, 'Solution', '', '2013-10-05 15:30:18');
INSERT INTO `using_log` VALUES (2203, 1, 'Solution', '', '2013-10-05 15:33:51');
INSERT INTO `using_log` VALUES (2204, 1, 'Solution', '', '2013-10-05 15:54:10');
INSERT INTO `using_log` VALUES (2205, 1, 'Solution', '', '2013-10-05 15:55:31');
INSERT INTO `using_log` VALUES (2206, 1, 'Solution', '', '2013-10-05 15:56:34');
INSERT INTO `using_log` VALUES (2207, 1, 'Solution', '', '2013-10-05 16:02:40');
INSERT INTO `using_log` VALUES (2208, 1, 'Solution', '', '2013-10-05 16:05:42');
INSERT INTO `using_log` VALUES (2209, 1, 'Solution', '', '2013-10-05 16:09:33');
INSERT INTO `using_log` VALUES (2210, 1, 'Solution', '', '2013-10-05 16:12:47');
INSERT INTO `using_log` VALUES (2211, 1, 'Admin', '', '2013-10-05 16:14:32');
INSERT INTO `using_log` VALUES (2212, 1, 'Equipment', '', '2013-10-05 16:14:33');
INSERT INTO `using_log` VALUES (2213, 1, 'Equipment', '', '2013-10-05 16:14:36');
INSERT INTO `using_log` VALUES (2214, 1, 'equipment', 'update', '2013-10-05 16:14:38');
INSERT INTO `using_log` VALUES (2215, 1, 'Solution', '', '2013-10-05 16:15:31');
INSERT INTO `using_log` VALUES (2216, 1, 'Solution', '', '2013-10-05 16:16:02');
INSERT INTO `using_log` VALUES (2217, 1, 'Solution', '', '2013-10-05 16:18:40');
INSERT INTO `using_log` VALUES (2218, 1, 'Management', 'Shutdown', '2013-10-05 16:20:02');
INSERT INTO `using_log` VALUES (2219, 1, 'Solution', '', '2013-10-05 16:22:15');
INSERT INTO `using_log` VALUES (2220, 1, 'Management', 'Shutdown', '2013-10-05 16:22:20');
INSERT INTO `using_log` VALUES (2221, 1, 'Solution', '', '2013-10-05 16:23:35');
INSERT INTO `using_log` VALUES (2222, 1, 'Management', 'Shutdown', '2013-10-05 16:23:42');
INSERT INTO `using_log` VALUES (2223, 1, 'Solution', '', '2013-10-05 16:29:49');
INSERT INTO `using_log` VALUES (2224, 1, 'Solution', '', '2013-10-05 16:38:13');
INSERT INTO `using_log` VALUES (2225, 1, 'Management', 'Shutdown', '2013-10-05 16:38:18');
INSERT INTO `using_log` VALUES (2226, 1, 'Solution', '', '2013-10-05 16:40:31');
INSERT INTO `using_log` VALUES (2227, NULL, '', '', '2013-10-10 00:59:47');
INSERT INTO `using_log` VALUES (2228, NULL, '', '', '2013-10-15 00:16:15');
INSERT INTO `using_log` VALUES (2229, NULL, '', '', '2013-10-20 22:25:28');
INSERT INTO `using_log` VALUES (2230, 1, 'RequestBooking', '', '2013-10-20 22:25:38');
INSERT INTO `using_log` VALUES (2231, 1, 'RequestBooking', 'Approve', '2013-10-20 22:25:41');
INSERT INTO `using_log` VALUES (2232, 1, 'RequestBooking', 'Request', '2013-10-20 22:25:43');
INSERT INTO `using_log` VALUES (2233, 1, 'RequestBooking', 'Request', '2013-10-20 22:25:51');
INSERT INTO `using_log` VALUES (2234, 1, 'RequestBorrow', '', '2013-10-20 22:25:55');
INSERT INTO `using_log` VALUES (2235, 1, 'RequestService', '', '2013-10-20 22:26:00');
INSERT INTO `using_log` VALUES (2236, 1, 'Solution', '', '2013-10-20 22:26:09');
INSERT INTO `using_log` VALUES (2237, 1, 'Admin', '', '2013-10-20 22:26:10');
INSERT INTO `using_log` VALUES (2238, 1, 'Solution', '', '2013-10-20 22:26:19');
INSERT INTO `using_log` VALUES (2239, 1, 'Management', 'Shutdown', '2013-10-20 22:26:24');
INSERT INTO `using_log` VALUES (2240, NULL, '', '', '2013-10-21 10:27:06');
INSERT INTO `using_log` VALUES (2241, NULL, '', '', '2013-10-21 10:27:10');
INSERT INTO `using_log` VALUES (2242, 1, 'RequestBooking', '', '2013-10-21 10:27:19');
INSERT INTO `using_log` VALUES (2243, 1, 'RequestBooking', 'Approve', '2013-10-21 10:27:22');
INSERT INTO `using_log` VALUES (2244, 1, 'RequestBooking', 'Request', '2013-10-21 10:27:23');
INSERT INTO `using_log` VALUES (2245, 1, 'RequestBooking', '', '2013-10-21 10:27:26');
INSERT INTO `using_log` VALUES (2246, 1, 'RequestBooking', 'Approve', '2013-10-21 10:27:28');
INSERT INTO `using_log` VALUES (2247, NULL, '', '', '2013-10-25 21:54:45');
INSERT INTO `using_log` VALUES (2248, 1, 'RequestBooking', '', '2013-10-25 21:54:50');
INSERT INTO `using_log` VALUES (2249, 1, 'RequestBorrow', '', '2013-10-25 21:54:52');
INSERT INTO `using_log` VALUES (2250, 1, 'RequestBorrow', 'Request', '2013-10-25 21:54:55');
INSERT INTO `using_log` VALUES (2251, 1, 'RequestBorrow', 'View', '2013-10-25 21:55:23');
INSERT INTO `using_log` VALUES (2252, 1, 'RequestBorrow', '', '2013-10-25 21:55:26');
INSERT INTO `using_log` VALUES (2253, 1, 'RequestBorrow', 'ListApprove', '2013-10-25 21:55:27');
INSERT INTO `using_log` VALUES (2254, 1, 'RequestBorrow', '', '2013-10-25 22:09:10');
INSERT INTO `using_log` VALUES (2255, 1, 'requestBorrow', 'view', '2013-10-25 22:09:17');
INSERT INTO `using_log` VALUES (2256, 1, 'RequestBorrow', '', '2013-10-25 22:09:18');
INSERT INTO `using_log` VALUES (2257, 1, 'requestBorrow', 'update', '2013-10-25 22:09:20');
INSERT INTO `using_log` VALUES (2258, 1, 'RequestBooking', '', '2013-10-25 22:09:23');
INSERT INTO `using_log` VALUES (2259, 1, 'RequestBooking', '', '2013-10-25 22:09:25');
INSERT INTO `using_log` VALUES (2260, 1, 'RequestBorrow', '', '2013-10-25 22:09:28');
INSERT INTO `using_log` VALUES (2261, 1, 'requestBorrow', 'update', '2013-10-25 22:09:32');
INSERT INTO `using_log` VALUES (2262, 1, 'RequestBooking', '', '2013-10-25 22:09:33');
INSERT INTO `using_log` VALUES (2263, 1, 'RequestBorrow', '', '2013-10-25 22:09:36');
INSERT INTO `using_log` VALUES (2264, NULL, '', '', '2013-10-28 21:09:27');
INSERT INTO `using_log` VALUES (2265, 1, 'RequestBooking', '', '2013-10-28 21:09:31');
INSERT INTO `using_log` VALUES (2266, 1, 'RequestBorrow', '', '2013-10-28 21:09:35');
INSERT INTO `using_log` VALUES (2267, 1, 'RequestBorrow', 'Request', '2013-10-28 21:09:38');
INSERT INTO `using_log` VALUES (2268, 1, 'RequestService', '', '2013-10-28 21:09:42');
INSERT INTO `using_log` VALUES (2269, 1, 'RequestService', 'Request', '2013-10-28 21:09:44');
INSERT INTO `using_log` VALUES (2270, 1, 'RequestService', 'View', '2013-10-28 21:10:02');
INSERT INTO `using_log` VALUES (2271, 1, 'RequestService', '', '2013-10-28 21:10:06');
INSERT INTO `using_log` VALUES (2272, 1, 'RequestService', 'Approve', '2013-10-28 21:10:08');
INSERT INTO `using_log` VALUES (2273, 1, 'RequestService', '', '2013-10-28 21:10:11');
INSERT INTO `using_log` VALUES (2274, 1, 'RequestService', 'Approve', '2013-10-28 21:10:16');
INSERT INTO `using_log` VALUES (2275, 1, 'RequestService', 'Approve', '2013-10-28 21:10:19');
INSERT INTO `using_log` VALUES (2276, 1, 'RequestService', '', '2013-10-28 21:10:22');
INSERT INTO `using_log` VALUES (2277, NULL, 'management', 'login', '2013-10-28 21:20:58');
INSERT INTO `using_log` VALUES (2278, NULL, '', '', '2013-11-02 17:04:07');
INSERT INTO `using_log` VALUES (2279, 1, 'RequestBooking', '', '2013-11-02 17:04:13');
INSERT INTO `using_log` VALUES (2280, 1, 'RequestService', '', '2013-11-02 17:04:15');
INSERT INTO `using_log` VALUES (2281, 1, 'RequestBorrow', '', '2013-11-02 17:04:18');
INSERT INTO `using_log` VALUES (2282, 1, 'RequestBorrow', 'Request', '2013-11-02 17:04:20');
INSERT INTO `using_log` VALUES (2283, 1, 'RequestService', '', '2013-11-02 17:04:24');
INSERT INTO `using_log` VALUES (2284, 1, 'RequestService', 'Approve', '2013-11-02 17:04:26');
INSERT INTO `using_log` VALUES (2285, 1, 'RequestService', 'Request', '2013-11-02 17:04:27');
INSERT INTO `using_log` VALUES (2286, 1, 'RequestService', 'Request', '2013-11-02 17:08:36');
INSERT INTO `using_log` VALUES (2287, 1, 'RequestService', 'Request', '2013-11-02 17:09:33');
INSERT INTO `using_log` VALUES (2288, NULL, '', '', '2013-11-11 09:21:55');
INSERT INTO `using_log` VALUES (2289, 1, 'RequestBooking', '', '2013-11-11 09:22:00');
INSERT INTO `using_log` VALUES (2290, 1, 'RequestBooking', 'CheckStatus', '2013-11-11 09:22:03');
INSERT INTO `using_log` VALUES (2291, 1, 'RequestBooking', '', '2013-11-11 09:22:05');
INSERT INTO `using_log` VALUES (2292, 1, 'RequestBooking', '', '2013-11-11 09:22:54');
INSERT INTO `using_log` VALUES (2293, 1, 'RequestBooking', 'Request', '2013-11-11 09:22:56');
INSERT INTO `using_log` VALUES (2294, 1, 'RequestBooking', 'View', '2013-11-11 09:23:18');
INSERT INTO `using_log` VALUES (2295, 1, 'RequestBooking', '', '2013-11-11 09:23:30');
INSERT INTO `using_log` VALUES (2296, 1, 'RequestBooking', 'CheckStatus', '2013-11-11 09:23:33');
INSERT INTO `using_log` VALUES (2297, 1, 'RequestBooking', 'Request', '2013-11-11 09:23:38');
INSERT INTO `using_log` VALUES (2298, 1, 'RequestBooking', 'Request', '2013-11-11 09:23:59');
INSERT INTO `using_log` VALUES (2299, 1, 'RequestBooking', 'View', '2013-11-11 09:24:29');
INSERT INTO `using_log` VALUES (2300, 1, 'RequestBooking', '', '2013-11-11 09:24:32');
INSERT INTO `using_log` VALUES (2301, 1, 'RequestBooking', 'CheckStatus', '2013-11-11 09:24:36');
INSERT INTO `using_log` VALUES (2302, 1, 'RequestBooking', '', '2013-11-11 09:24:58');
INSERT INTO `using_log` VALUES (2303, 1, 'RequestBooking', 'Approve', '2013-11-11 09:25:09');
INSERT INTO `using_log` VALUES (2304, 1, 'RequestBooking', 'Request', '2013-11-11 09:25:10');
INSERT INTO `using_log` VALUES (2305, 1, 'RequestBooking', 'CheckStatus', '2013-11-11 09:25:11');
INSERT INTO `using_log` VALUES (2306, 1, 'RequestBooking', '', '2013-11-11 09:25:12');
INSERT INTO `using_log` VALUES (2307, NULL, '', '', '2013-11-11 22:06:59');
INSERT INTO `using_log` VALUES (2308, 1, 'RequestBooking', '', '2013-11-11 22:07:04');
INSERT INTO `using_log` VALUES (2309, 1, 'RequestBooking', 'Index', '2013-11-11 22:18:56');
INSERT INTO `using_log` VALUES (2310, 1, 'RequestBooking', 'Index', '2013-11-11 22:19:02');
INSERT INTO `using_log` VALUES (2311, 1, 'RequestBooking', 'Index', '2013-11-11 22:19:03');
INSERT INTO `using_log` VALUES (2312, 1, 'RequestBooking', 'Index', '2013-11-11 22:19:05');
INSERT INTO `using_log` VALUES (2313, 1, 'RequestBooking', 'Index', '2013-11-11 22:19:07');
INSERT INTO `using_log` VALUES (2314, 1, 'RequestBooking', 'Index', '2013-11-11 22:19:08');
INSERT INTO `using_log` VALUES (2315, 1, '', '', '2013-11-11 22:25:45');
INSERT INTO `using_log` VALUES (2316, 1, 'RequestBooking', '', '2013-11-11 22:25:50');
INSERT INTO `using_log` VALUES (2317, 1, 'Admin', '', '2013-11-11 22:28:18');
INSERT INTO `using_log` VALUES (2318, 1, 'RequestBooking', '', '2013-11-11 22:36:35');
INSERT INTO `using_log` VALUES (2319, 1, 'RequestBooking', 'CheckStatus', '2013-11-11 22:37:16');
INSERT INTO `using_log` VALUES (2320, 1, 'RequestBooking', 'Request', '2013-11-11 22:37:23');
INSERT INTO `using_log` VALUES (2321, 1, 'RequestBooking', 'Request', '2013-11-11 22:38:22');
INSERT INTO `using_log` VALUES (2322, 1, 'RequestBooking', 'View', '2013-11-11 22:40:03');
INSERT INTO `using_log` VALUES (2323, 1, 'RequestBooking', '', '2013-11-11 22:40:15');
INSERT INTO `using_log` VALUES (2324, 1, 'requestBooking', 'view', '2013-11-11 22:40:18');
INSERT INTO `using_log` VALUES (2325, 1, 'RequestBooking', 'Approve', '2013-11-11 22:40:24');
INSERT INTO `using_log` VALUES (2326, 1, 'RequestBooking', 'Approve', '2013-11-11 22:40:26');
INSERT INTO `using_log` VALUES (2327, 1, 'RequestBooking', '', '2013-11-11 22:40:32');
INSERT INTO `using_log` VALUES (2328, 1, 'RequestBooking', 'Request', '2013-11-11 22:40:37');
INSERT INTO `using_log` VALUES (2329, 1, 'RequestBooking', 'View', '2013-11-11 22:42:24');
INSERT INTO `using_log` VALUES (2330, 1, 'RequestBooking', '', '2013-11-11 22:42:40');
INSERT INTO `using_log` VALUES (2331, NULL, '', '', '2013-11-12 07:58:31');
INSERT INTO `using_log` VALUES (2332, 1, 'RequestBooking', '', '2013-11-12 07:58:37');
INSERT INTO `using_log` VALUES (2333, 1, 'RequestBooking', 'CheckStatus', '2013-11-12 07:58:44');
INSERT INTO `using_log` VALUES (2334, 1, 'RequestBorrow', '', '2013-11-12 07:58:50');
INSERT INTO `using_log` VALUES (2335, 1, 'RequestBorrow', 'CheckStatus', '2013-11-12 07:58:55');
INSERT INTO `using_log` VALUES (2336, 1, 'RequestBorrow', 'request', '2013-11-12 07:58:58');
INSERT INTO `using_log` VALUES (2337, 1, 'RequestBorrow', 'View', '2013-11-12 07:59:50');
INSERT INTO `using_log` VALUES (2338, 1, 'RequestBorrow', '', '2013-11-12 08:00:17');
INSERT INTO `using_log` VALUES (2339, NULL, 'management', 'login', '2013-11-12 08:03:54');
INSERT INTO `using_log` VALUES (2340, NULL, 'management', 'login', '2013-11-12 08:04:02');
INSERT INTO `using_log` VALUES (2341, 2, 'RequestBooking', '', '2013-11-12 08:04:10');
INSERT INTO `using_log` VALUES (2342, 2, 'RequestBorrow', '', '2013-11-12 08:04:24');
INSERT INTO `using_log` VALUES (2343, 2, 'RequestBorrow', 'ListApprove', '2013-11-12 08:04:34');
INSERT INTO `using_log` VALUES (2344, 2, 'RequestBorrow', '', '2013-11-12 08:04:37');
INSERT INTO `using_log` VALUES (2345, 2, 'RequestBorrow', 'Request', '2013-11-12 08:04:40');
INSERT INTO `using_log` VALUES (2346, 2, 'RequestBorrow', 'View', '2013-11-12 08:05:29');
INSERT INTO `using_log` VALUES (2347, 2, 'RequestBorrow', '', '2013-11-12 08:05:35');
INSERT INTO `using_log` VALUES (2348, 2, 'RequestBorrow', 'ListApprove', '2013-11-12 08:05:42');
INSERT INTO `using_log` VALUES (2349, 2, 'RequestBorrow', '', '2013-11-12 08:05:48');
INSERT INTO `using_log` VALUES (2350, 2, 'RequestBorrow', 'Request', '2013-11-12 08:06:07');
INSERT INTO `using_log` VALUES (2351, 2, 'RequestBorrow', 'View', '2013-11-12 08:07:01');
INSERT INTO `using_log` VALUES (2352, 2, 'RequestBorrow', '', '2013-11-12 08:07:08');
INSERT INTO `using_log` VALUES (2353, 2, 'RequestBorrow', 'ListApprove', '2013-11-12 08:07:13');
INSERT INTO `using_log` VALUES (2354, 2, 'RequestBorrow', '', '2013-11-12 08:07:22');
INSERT INTO `using_log` VALUES (2355, 2, 'RequestBorrow', 'Request', '2013-11-12 08:07:26');
INSERT INTO `using_log` VALUES (2356, 2, 'RequestBorrow', 'CheckStatus', '2013-11-12 08:07:29');
INSERT INTO `using_log` VALUES (2357, 2, 'RequestBorrow', 'request', '2013-11-12 08:07:32');
INSERT INTO `using_log` VALUES (2358, 2, 'RequestBorrow', 'View', '2013-11-12 08:08:21');
INSERT INTO `using_log` VALUES (2359, 2, 'RequestBorrow', '', '2013-11-12 08:08:27');
INSERT INTO `using_log` VALUES (2360, 2, 'RequestBorrow', 'ListApprove', '2013-11-12 08:08:32');
INSERT INTO `using_log` VALUES (2361, 2, 'RequestService', '', '2013-11-12 08:14:07');
INSERT INTO `using_log` VALUES (2362, 2, 'RequestService', 'Request', '2013-11-12 08:14:12');
INSERT INTO `using_log` VALUES (2363, 2, 'RequestBorrow', '', '2013-11-12 08:14:17');
INSERT INTO `using_log` VALUES (2364, 2, 'RequestBorrow', 'Request', '2013-11-12 08:14:19');
INSERT INTO `using_log` VALUES (2365, 2, 'RequestBooking', '', '2013-11-12 09:34:50');
INSERT INTO `using_log` VALUES (2366, 2, 'RequestBorrow', '', '2013-11-12 09:40:33');
INSERT INTO `using_log` VALUES (2367, 2, 'RequestBooking', '', '2013-11-12 09:40:35');
INSERT INTO `using_log` VALUES (2368, 2, 'RequestBorrow', '', '2013-11-12 09:40:36');
INSERT INTO `using_log` VALUES (2369, 2, 'RequestService', '', '2013-11-12 09:40:37');
INSERT INTO `using_log` VALUES (2370, 2, 'Admin', '', '2013-11-12 09:40:38');
INSERT INTO `using_log` VALUES (2371, 2, 'RequestBooking', '', '2013-11-12 09:40:39');
INSERT INTO `using_log` VALUES (2372, 2, '', '', '2013-11-12 10:08:17');
INSERT INTO `using_log` VALUES (2373, NULL, 'management', 'login', '2013-11-12 10:08:21');
INSERT INTO `using_log` VALUES (2374, NULL, 'management', 'login', '2013-11-12 10:08:27');
INSERT INTO `using_log` VALUES (2375, NULL, '', '', '2013-11-12 10:08:36');
INSERT INTO `using_log` VALUES (2376, 3, 'RequestBooking', '', '2013-11-12 10:09:11');
INSERT INTO `using_log` VALUES (2377, NULL, 'management', 'login', '2013-11-12 10:09:34');
INSERT INTO `using_log` VALUES (2378, 3, 'RequestBooking', '', '2013-11-12 10:09:41');
INSERT INTO `using_log` VALUES (2379, 3, 'RequestBorrow', '', '2013-11-12 10:11:08');
INSERT INTO `using_log` VALUES (2380, 3, 'RequestService', '', '2013-11-12 10:11:09');
INSERT INTO `using_log` VALUES (2381, 3, 'RequestBooking', '', '2013-11-12 10:11:11');
INSERT INTO `using_log` VALUES (2382, 3, 'RequestBorrow', '', '2013-11-12 10:11:12');
INSERT INTO `using_log` VALUES (2383, 3, 'RequestService', '', '2013-11-12 10:11:13');
INSERT INTO `using_log` VALUES (2384, 3, 'RequestBorrow', '', '2013-11-12 10:11:14');
INSERT INTO `using_log` VALUES (2385, 3, 'RequestBooking', '', '2013-11-12 10:11:14');
INSERT INTO `using_log` VALUES (2386, 3, 'RequestBorrow', '', '2013-11-12 10:16:38');
INSERT INTO `using_log` VALUES (2387, 3, 'RequestBorrow', 'Request', '2013-11-12 10:16:40');
INSERT INTO `using_log` VALUES (2388, NULL, 'management', 'login', '2013-11-12 10:17:22');
INSERT INTO `using_log` VALUES (2389, 1, 'RequestBooking', '', '2013-11-12 10:17:26');
INSERT INTO `using_log` VALUES (2390, 1, 'Admin', '', '2013-11-12 10:17:28');
INSERT INTO `using_log` VALUES (2391, 1, 'Department', '', '2013-11-12 10:17:32');
INSERT INTO `using_log` VALUES (2392, 1, 'department', 'create', '2013-11-12 10:17:35');
INSERT INTO `using_log` VALUES (2393, 1, 'RequestBooking', '', '2013-11-12 10:18:54');
INSERT INTO `using_log` VALUES (2394, 1, 'RequestBooking', 'Request', '2013-11-12 10:18:58');
INSERT INTO `using_log` VALUES (2395, 1, 'RequestBooking', 'Request', '2013-11-12 10:19:21');
INSERT INTO `using_log` VALUES (2396, 1, 'RequestBooking', 'Approve', '2013-11-12 10:19:22');
INSERT INTO `using_log` VALUES (2397, 1, 'RequestBooking', 'Request', '2013-11-12 10:19:23');
INSERT INTO `using_log` VALUES (2398, 1, 'RequestBooking', '', '2013-11-12 10:19:24');
INSERT INTO `using_log` VALUES (2399, 1, 'RequestBooking', 'CheckStatus', '2013-11-12 10:19:25');
INSERT INTO `using_log` VALUES (2400, 1, 'RequestBooking', 'Request', '2013-11-12 10:19:26');
INSERT INTO `using_log` VALUES (2401, 1, 'RequestBooking', 'View', '2013-11-12 10:20:13');
INSERT INTO `using_log` VALUES (2402, 1, 'RequestBooking', '', '2013-11-12 10:20:50');
INSERT INTO `using_log` VALUES (2403, 1, 'requestBooking', 'view', '2013-11-12 10:20:54');
INSERT INTO `using_log` VALUES (2404, 1, 'RequestBooking', '', '2013-11-12 10:20:58');
INSERT INTO `using_log` VALUES (2405, 1, 'RequestBooking', 'Request', '2013-11-12 10:21:13');
INSERT INTO `using_log` VALUES (2406, 1, 'Admin', '', '2013-11-12 10:21:55');
INSERT INTO `using_log` VALUES (2407, 1, 'Room', '', '2013-11-12 10:22:00');
INSERT INTO `using_log` VALUES (2408, 1, 'Room', '', '2013-11-12 10:22:05');
INSERT INTO `using_log` VALUES (2409, 1, 'Room', '', '2013-11-12 10:22:06');
INSERT INTO `using_log` VALUES (2410, 1, 'Admin', '', '2013-11-12 10:22:07');
INSERT INTO `using_log` VALUES (2411, 1, 'RequestBooking', 'Request', '2013-11-12 10:22:07');
INSERT INTO `using_log` VALUES (2412, 1, 'Admin', '', '2013-11-12 10:22:09');
INSERT INTO `using_log` VALUES (2413, 1, 'RequestBooking', '', '2013-11-12 10:22:32');
INSERT INTO `using_log` VALUES (2414, 1, 'RequestBooking', 'Approve', '2013-11-12 10:22:36');
INSERT INTO `using_log` VALUES (2415, 1, 'RequestBooking', '', '2013-11-12 10:22:43');
INSERT INTO `using_log` VALUES (2416, 1, 'RequestBorrow', '', '2013-11-12 10:24:37');
INSERT INTO `using_log` VALUES (2417, 1, 'RequestBorrow', 'ListApprove', '2013-11-12 10:26:02');
INSERT INTO `using_log` VALUES (2418, 1, 'RequestBorrow', 'Request', '2013-11-12 10:26:23');
INSERT INTO `using_log` VALUES (2419, 1, 'RequestBorrow', 'ListApprove', '2013-11-12 10:32:43');
INSERT INTO `using_log` VALUES (2420, 1, 'RequestBorrow', '', '2013-11-12 10:32:45');
INSERT INTO `using_log` VALUES (2421, 1, 'RequestBorrow', 'CheckStatus', '2013-11-12 10:32:53');
INSERT INTO `using_log` VALUES (2422, 1, 'RequestBorrow', '', '2013-11-12 10:32:57');
INSERT INTO `using_log` VALUES (2423, 1, 'RequestBorrow', 'ListApprove', '2013-11-12 10:33:11');
INSERT INTO `using_log` VALUES (2424, 1, 'RequestBorrow', 'Request', '2013-11-12 10:33:12');
INSERT INTO `using_log` VALUES (2425, 1, 'RequestBorrow', 'View', '2013-11-12 10:33:54');
INSERT INTO `using_log` VALUES (2426, 1, 'RequestBorrow', '', '2013-11-12 10:34:08');
INSERT INTO `using_log` VALUES (2427, 1, 'RequestBorrow', 'ListApprove', '2013-11-12 10:34:10');
INSERT INTO `using_log` VALUES (2428, 1, 'RequestBorrow', '', '2013-11-12 10:34:12');
INSERT INTO `using_log` VALUES (2429, 1, 'RequestBorrow', 'CheckStatus', '2013-11-12 10:34:14');
INSERT INTO `using_log` VALUES (2430, 1, 'RequestBorrow', '', '2013-11-12 10:34:15');
INSERT INTO `using_log` VALUES (2431, 1, 'RequestBorrow', 'Index', '2013-11-12 10:34:22');
INSERT INTO `using_log` VALUES (2432, 1, 'RequestBorrow', 'Index', '2013-11-12 10:34:22');
INSERT INTO `using_log` VALUES (2433, 1, 'RequestBorrow', 'Index', '2013-11-12 10:34:24');
INSERT INTO `using_log` VALUES (2434, 1, 'RequestBorrow', 'Index', '2013-11-12 10:34:24');
INSERT INTO `using_log` VALUES (2435, 1, 'RequestService', '', '2013-11-12 10:38:54');
INSERT INTO `using_log` VALUES (2436, 1, 'RequestService', 'Request', '2013-11-12 10:38:58');
INSERT INTO `using_log` VALUES (2437, 1, 'Report', '', '2013-11-12 10:39:45');
INSERT INTO `using_log` VALUES (2438, 1, 'Report', 'UsingSheet', '2013-11-12 10:39:55');
INSERT INTO `using_log` VALUES (2439, 1, 'Report', 'UsingSheet', '2013-11-12 10:40:01');
INSERT INTO `using_log` VALUES (2440, 1, 'Report', 'UsingSheet', '2013-11-12 10:40:03');
INSERT INTO `using_log` VALUES (2441, 1, 'Report', 'UsingSheet', '2013-11-12 10:40:08');
INSERT INTO `using_log` VALUES (2442, 1, 'Report', 'ExportUsingSheetExcel', '2013-11-12 10:40:11');
INSERT INTO `using_log` VALUES (2443, NULL, '', '', '2013-11-12 20:24:21');
INSERT INTO `using_log` VALUES (2444, 1, 'RequestBooking', '', '2013-11-12 20:24:30');
INSERT INTO `using_log` VALUES (2445, 1, 'RequestBooking', 'CheckStatus', '2013-11-12 20:32:52');
INSERT INTO `using_log` VALUES (2446, 1, 'RequestBooking', 'CheckStatusMeeting', '2013-11-12 20:33:03');
INSERT INTO `using_log` VALUES (2447, 1, 'RequestBooking', 'CheckStatus', '2013-11-12 20:33:08');
INSERT INTO `using_log` VALUES (2448, 1, 'RequestBooking', 'CheckStatus', '2013-11-12 20:33:17');
INSERT INTO `using_log` VALUES (2449, 1, 'RequestBorrow', '', '2013-11-12 20:35:23');
INSERT INTO `using_log` VALUES (2450, 1, 'RequestBorrow', 'Request', '2013-11-12 20:35:25');
INSERT INTO `using_log` VALUES (2451, NULL, '', '', '2013-11-20 08:35:31');
INSERT INTO `using_log` VALUES (2452, NULL, 'management', 'login', '2013-11-20 08:35:33');
INSERT INTO `using_log` VALUES (2453, 1, 'RequestBooking', '', '2013-11-20 08:35:39');
INSERT INTO `using_log` VALUES (2454, 1, '', '', '2013-11-20 08:35:41');
INSERT INTO `using_log` VALUES (2455, 1, 'RequestBooking', 'CheckStatus', '2013-11-20 08:35:43');
INSERT INTO `using_log` VALUES (2456, 1, 'RequestBooking', '', '2013-11-20 08:35:47');
INSERT INTO `using_log` VALUES (2457, 1, 'RequestBooking', 'Request', '2013-11-20 08:35:50');
INSERT INTO `using_log` VALUES (2458, 1, 'Admin', '', '2013-11-20 08:35:54');
INSERT INTO `using_log` VALUES (2459, 1, 'Semester', '', '2013-11-20 08:36:01');
INSERT INTO `using_log` VALUES (2460, 1, 'Period', '', '2013-11-20 08:36:15');
INSERT INTO `using_log` VALUES (2461, 1, 'Position', '', '2013-11-20 08:36:19');
INSERT INTO `using_log` VALUES (2462, 1, 'Room', '', '2013-11-20 08:36:22');
INSERT INTO `using_log` VALUES (2463, 1, 'RequestBooking', '', '2013-11-20 08:36:24');
INSERT INTO `using_log` VALUES (2464, 1, 'RequestBooking', 'Request', '2013-11-20 08:36:25');
INSERT INTO `using_log` VALUES (2465, 1, 'RequestBooking', 'View', '2013-11-20 08:36:45');
INSERT INTO `using_log` VALUES (2466, 1, 'RequestBooking', '', '2013-11-20 08:36:52');
INSERT INTO `using_log` VALUES (2467, 1, 'RequestBooking', 'Request', '2013-11-20 08:36:54');
INSERT INTO `using_log` VALUES (2468, 1, 'RequestBooking', 'CheckStatus', '2013-11-20 08:36:55');
INSERT INTO `using_log` VALUES (2469, 1, 'RequestBooking', 'CheckStatus', '2013-11-20 08:37:09');
INSERT INTO `using_log` VALUES (2470, 1, 'RequestBooking', '', '2013-11-20 08:37:32');
INSERT INTO `using_log` VALUES (2471, 1, 'RequestBorrow', '', '2013-11-20 08:37:35');
INSERT INTO `using_log` VALUES (2472, 1, 'RequestBorrow', 'ListApprove', '2013-11-20 08:37:36');
INSERT INTO `using_log` VALUES (2473, 1, 'RequestBorrow', 'ListApprove', '2013-11-20 08:37:43');
INSERT INTO `using_log` VALUES (2474, 1, 'RequestBorrow', 'CheckStatus', '2013-11-20 08:37:45');
INSERT INTO `using_log` VALUES (2475, 1, 'RequestBooking', '', '2013-11-20 08:37:47');
INSERT INTO `using_log` VALUES (2476, 1, 'RequestBorrow', '', '2013-11-20 08:37:48');
INSERT INTO `using_log` VALUES (2477, 1, 'RequestService', '', '2013-11-20 08:37:50');
INSERT INTO `using_log` VALUES (2478, 1, 'Solution', '', '2013-11-20 08:37:59');
INSERT INTO `using_log` VALUES (2479, 1, '', '', '2013-11-20 22:10:40');
INSERT INTO `using_log` VALUES (2480, 1, 'RequestBooking', '', '2013-11-20 22:10:45');
INSERT INTO `using_log` VALUES (2481, 1, 'RequestBorrow', '', '2013-11-20 22:10:47');
INSERT INTO `using_log` VALUES (2482, 1, 'RequestBorrow', '', '2013-11-20 22:10:47');
INSERT INTO `using_log` VALUES (2483, NULL, '', '', '2013-11-22 16:07:04');
INSERT INTO `using_log` VALUES (2484, 1, 'RequestBooking', '', '2013-11-22 16:07:09');
INSERT INTO `using_log` VALUES (2485, 1, 'RequestBorrow', '', '2013-11-22 16:07:11');
INSERT INTO `using_log` VALUES (2486, 1, 'RequestBorrow', '', '2013-11-22 16:07:16');
INSERT INTO `using_log` VALUES (2487, 1, 'RequestBooking', '', '2013-11-22 16:07:20');
INSERT INTO `using_log` VALUES (2488, 1, 'Solution', '', '2013-11-22 16:07:37');
INSERT INTO `using_log` VALUES (2489, 1, 'RequestService', '', '2013-11-22 16:07:37');
INSERT INTO `using_log` VALUES (2490, 1, 'Admin', '', '2013-11-22 16:07:37');
INSERT INTO `using_log` VALUES (2491, 1, 'RequestBorrow', '', '2013-11-22 16:13:11');
INSERT INTO `using_log` VALUES (2492, 1, '', '', '2013-11-22 16:15:00');
INSERT INTO `using_log` VALUES (2493, 1, 'Management', 'EditProfile', '2013-11-22 16:15:03');
INSERT INTO `using_log` VALUES (2494, 1, 'RequestBorrow', '', '2013-11-22 16:15:04');
INSERT INTO `using_log` VALUES (2495, 1, '', '', '2013-11-22 16:18:55');
INSERT INTO `using_log` VALUES (2496, 1, 'RequestBooking', '', '2013-11-22 16:18:58');
INSERT INTO `using_log` VALUES (2497, 1, 'RequestBooking', 'Request', '2013-11-22 16:19:01');
INSERT INTO `using_log` VALUES (2498, 1, 'RequestBooking', 'View', '2013-11-22 16:19:21');
INSERT INTO `using_log` VALUES (2499, 1, 'RequestBooking', '', '2013-11-22 16:19:23');
INSERT INTO `using_log` VALUES (2500, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 16:19:25');
INSERT INTO `using_log` VALUES (2501, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 16:19:45');
INSERT INTO `using_log` VALUES (2502, 1, 'RequestBooking', 'CheckStatusMeeting', '2013-11-22 16:19:46');
INSERT INTO `using_log` VALUES (2503, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 16:19:48');
INSERT INTO `using_log` VALUES (2504, 1, 'RequestBooking', 'Request', '2013-11-22 16:19:54');
INSERT INTO `using_log` VALUES (2505, 1, 'RequestBorrow', '', '2013-11-22 16:19:56');
INSERT INTO `using_log` VALUES (2506, 1, 'RequestBorrow', '', '2013-11-22 16:20:01');
INSERT INTO `using_log` VALUES (2507, NULL, '', '', '2013-11-22 21:20:31');
INSERT INTO `using_log` VALUES (2508, 1, 'RequestBooking', '', '2013-11-22 21:22:23');
INSERT INTO `using_log` VALUES (2509, 1, 'RequestBorrow', '', '2013-11-22 21:22:25');
INSERT INTO `using_log` VALUES (2510, 1, 'RequestBorrow', '', '2013-11-22 21:22:27');
INSERT INTO `using_log` VALUES (2511, 1, 'RequestBorrow', 'Request', '2013-11-22 21:25:06');
INSERT INTO `using_log` VALUES (2512, 1, 'RequestBorrow', 'View', '2013-11-22 21:25:38');
INSERT INTO `using_log` VALUES (2513, 1, 'RequestBorrow', '', '2013-11-22 21:25:40');
INSERT INTO `using_log` VALUES (2514, 1, 'RequestBooking', '', '2013-11-22 21:25:46');
INSERT INTO `using_log` VALUES (2515, 1, 'RequestBooking', 'Request', '2013-11-22 21:25:50');
INSERT INTO `using_log` VALUES (2516, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:25:53');
INSERT INTO `using_log` VALUES (2517, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:26:03');
INSERT INTO `using_log` VALUES (2518, 1, 'RequestBooking', 'Request', '2013-11-22 21:26:06');
INSERT INTO `using_log` VALUES (2519, 1, 'RequestBooking', 'View', '2013-11-22 21:26:21');
INSERT INTO `using_log` VALUES (2520, 1, 'RequestBooking', '', '2013-11-22 21:26:22');
INSERT INTO `using_log` VALUES (2521, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:26:25');
INSERT INTO `using_log` VALUES (2522, 1, 'RequestBooking', 'Approve', '2013-11-22 21:26:42');
INSERT INTO `using_log` VALUES (2523, 1, 'RequestBooking', '', '2013-11-22 21:26:44');
INSERT INTO `using_log` VALUES (2524, 1, 'RequestBorrow', '', '2013-11-22 21:26:48');
INSERT INTO `using_log` VALUES (2525, 1, 'RequestBorrow', 'ListApprove', '2013-11-22 21:26:52');
INSERT INTO `using_log` VALUES (2526, 1, 'RequestService', '', '2013-11-22 21:26:58');
INSERT INTO `using_log` VALUES (2527, 1, 'RequestService', 'Approve', '2013-11-22 21:27:01');
INSERT INTO `using_log` VALUES (2528, 1, 'RequestService', '', '2013-11-22 21:27:02');
INSERT INTO `using_log` VALUES (2529, 1, 'Solution', '', '2013-11-22 21:27:29');
INSERT INTO `using_log` VALUES (2530, 1, 'Admin', '', '2013-11-22 21:27:29');
INSERT INTO `using_log` VALUES (2531, 1, 'RequestBooking', '', '2013-11-22 21:27:29');
INSERT INTO `using_log` VALUES (2532, 1, 'RequestBorrow', '', '2013-11-22 21:27:31');
INSERT INTO `using_log` VALUES (2533, 1, 'RequestBorrow', 'Request', '2013-11-22 21:27:33');
INSERT INTO `using_log` VALUES (2534, 1, 'RequestBooking', '', '2013-11-22 21:27:35');
INSERT INTO `using_log` VALUES (2535, 1, 'RequestBooking', 'Request', '2013-11-22 21:27:37');
INSERT INTO `using_log` VALUES (2536, 1, 'RequestBooking', 'Request', '2013-11-22 21:28:09');
INSERT INTO `using_log` VALUES (2537, 1, 'RequestBooking', 'Request', '2013-11-22 21:28:39');
INSERT INTO `using_log` VALUES (2538, 1, 'RequestBooking', '', '2013-11-22 21:30:24');
INSERT INTO `using_log` VALUES (2539, 1, 'RequestBooking', 'Request', '2013-11-22 21:30:26');
INSERT INTO `using_log` VALUES (2540, 1, 'RequestBorrow', '', '2013-11-22 21:30:33');
INSERT INTO `using_log` VALUES (2541, 1, 'RequestService', '', '2013-11-22 21:30:35');
INSERT INTO `using_log` VALUES (2542, 1, 'RequestBooking', '', '2013-11-22 21:30:36');
INSERT INTO `using_log` VALUES (2543, 1, 'RequestBorrow', '', '2013-11-22 21:30:38');
INSERT INTO `using_log` VALUES (2544, 1, 'RequestService', '', '2013-11-22 21:30:38');
INSERT INTO `using_log` VALUES (2545, 1, 'Solution', '', '2013-11-22 21:31:02');
INSERT INTO `using_log` VALUES (2546, 1, 'Report', '', '2013-11-22 21:31:02');
INSERT INTO `using_log` VALUES (2547, 1, 'RequestBooking', '', '2013-11-22 21:36:17');
INSERT INTO `using_log` VALUES (2548, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:36:21');
INSERT INTO `using_log` VALUES (2549, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:36:23');
INSERT INTO `using_log` VALUES (2550, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:36:23');
INSERT INTO `using_log` VALUES (2551, 1, 'RequestBooking', 'Approve', '2013-11-22 21:37:15');
INSERT INTO `using_log` VALUES (2552, 1, 'RequestBooking', 'Request', '2013-11-22 21:37:16');
INSERT INTO `using_log` VALUES (2553, 1, 'RequestBooking', 'View', '2013-11-22 21:37:39');
INSERT INTO `using_log` VALUES (2554, 1, 'RequestBooking', '', '2013-11-22 21:37:41');
INSERT INTO `using_log` VALUES (2555, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:37:44');
INSERT INTO `using_log` VALUES (2556, 1, 'RequestBooking', 'Request', '2013-11-22 21:38:38');
INSERT INTO `using_log` VALUES (2557, 1, 'RequestBooking', '', '2013-11-22 21:42:14');
INSERT INTO `using_log` VALUES (2558, 1, 'RequestBorrow', '', '2013-11-22 21:51:43');
INSERT INTO `using_log` VALUES (2559, 1, 'RequestBooking', '', '2013-11-22 21:53:05');
INSERT INTO `using_log` VALUES (2560, 1, 'RequestBorrow', '', '2013-11-22 21:53:06');
INSERT INTO `using_log` VALUES (2561, 1, 'RequestBorrow', 'Request', '2013-11-22 21:53:09');
INSERT INTO `using_log` VALUES (2562, 1, 'RequestService', '', '2013-11-22 21:53:18');
INSERT INTO `using_log` VALUES (2563, 1, 'RequestService', 'Request', '2013-11-22 21:53:20');
INSERT INTO `using_log` VALUES (2564, 1, 'Admin', '', '2013-11-22 21:53:32');
INSERT INTO `using_log` VALUES (2565, 1, 'Equipment', '', '2013-11-22 21:53:36');
INSERT INTO `using_log` VALUES (2566, 1, 'Semester', '', '2013-11-22 21:53:40');
INSERT INTO `using_log` VALUES (2567, 1, 'semester', 'update', '2013-11-22 21:54:02');
INSERT INTO `using_log` VALUES (2568, 1, 'Semester', '', '2013-11-22 21:54:07');
INSERT INTO `using_log` VALUES (2569, 1, 'semester', 'view', '2013-11-22 21:54:11');
INSERT INTO `using_log` VALUES (2570, 1, 'Semester', '', '2013-11-22 21:54:12');
INSERT INTO `using_log` VALUES (2571, 1, 'semester', 'update', '2013-11-22 21:54:21');
INSERT INTO `using_log` VALUES (2572, 1, 'Semester', '', '2013-11-22 21:54:54');
INSERT INTO `using_log` VALUES (2573, 1, 'semester', 'create', '2013-11-22 21:54:57');
INSERT INTO `using_log` VALUES (2574, 1, 'Semester', '', '2013-11-22 21:55:01');
INSERT INTO `using_log` VALUES (2575, 1, 'semester', 'create', '2013-11-22 21:55:02');
INSERT INTO `using_log` VALUES (2576, 1, 'Semester', '', '2013-11-22 21:55:16');
INSERT INTO `using_log` VALUES (2577, 1, 'semester', 'update', '2013-11-22 21:55:29');
INSERT INTO `using_log` VALUES (2578, 1, 'Semester', '', '2013-11-22 21:55:40');
INSERT INTO `using_log` VALUES (2579, 1, 'RequestBooking', '', '2013-11-22 21:55:52');
INSERT INTO `using_log` VALUES (2580, 1, 'RequestBooking', 'Request', '2013-11-22 21:55:53');
INSERT INTO `using_log` VALUES (2581, 1, 'RequestBooking', 'View', '2013-11-22 21:56:19');
INSERT INTO `using_log` VALUES (2582, 1, 'RequestBooking', '', '2013-11-22 21:56:20');
INSERT INTO `using_log` VALUES (2583, 1, 'RequestBooking', 'CheckStatus', '2013-11-22 21:56:22');
INSERT INTO `using_log` VALUES (2584, 1, 'RequestBooking', '', '2013-11-22 21:57:02');
INSERT INTO `using_log` VALUES (2585, 1, 'RequestBooking', 'Request', '2013-11-22 21:57:05');
INSERT INTO `using_log` VALUES (2586, 1, 'RequestBorrow', '', '2013-11-22 21:57:06');
INSERT INTO `using_log` VALUES (2587, 1, 'RequestBorrow', 'CheckStatus', '2013-11-22 21:57:08');
INSERT INTO `using_log` VALUES (2588, 1, 'RequestBorrow', 'Request', '2013-11-22 21:57:09');
INSERT INTO `using_log` VALUES (2589, 1, 'RequestBorrow', 'ListApprove', '2013-11-22 21:57:11');
INSERT INTO `using_log` VALUES (2590, 1, 'RequestBooking', '', '2013-11-22 21:57:13');
INSERT INTO `using_log` VALUES (2591, 1, 'RequestService', '', '2013-11-22 21:57:14');
INSERT INTO `using_log` VALUES (2592, 1, 'Solution', '', '2013-11-22 21:57:38');
INSERT INTO `using_log` VALUES (2593, 1, 'Report', '', '2013-11-22 21:57:38');
INSERT INTO `using_log` VALUES (2594, 1, 'RequestBooking', '', '2013-11-22 21:57:38');
INSERT INTO `using_log` VALUES (2595, 1, 'RequestBooking', '', '2013-11-22 21:57:39');
INSERT INTO `using_log` VALUES (2596, 1, 'requestBooking', 'view', '2013-11-22 21:57:42');
INSERT INTO `using_log` VALUES (2597, 1, 'RequestBooking', '', '2013-11-22 21:57:44');
INSERT INTO `using_log` VALUES (2598, 1, 'RequestBooking', 'Request', '2013-11-22 21:57:48');
INSERT INTO `using_log` VALUES (2599, 1, 'RequestBorrow', '', '2013-11-22 21:57:50');
INSERT INTO `using_log` VALUES (2600, 1, 'RequestBorrow', '', '2013-11-22 21:57:54');
INSERT INTO `using_log` VALUES (2601, 1, 'RequestBooking', '', '2013-11-22 21:57:57');
INSERT INTO `using_log` VALUES (2602, 1, 'RequestBorrow', '', '2013-11-22 21:57:57');
INSERT INTO `using_log` VALUES (2603, 1, 'RequestService', '', '2013-11-22 21:59:07');
INSERT INTO `using_log` VALUES (2604, 1, 'RequestService', 'Request', '2013-11-22 21:59:10');
INSERT INTO `using_log` VALUES (2605, 1, '', '', '2013-11-23 11:20:17');
INSERT INTO `using_log` VALUES (2606, 1, 'RequestBooking', '', '2013-11-23 11:20:20');
INSERT INTO `using_log` VALUES (2607, 1, 'RequestService', '', '2013-11-23 11:20:22');
INSERT INTO `using_log` VALUES (2608, 1, 'RequestBorrow', '', '2013-11-23 11:20:23');
INSERT INTO `using_log` VALUES (2609, 1, 'RequestBooking', '', '2013-11-23 11:20:24');
INSERT INTO `using_log` VALUES (2610, 1, 'RequestBooking', 'Approve', '2013-11-23 11:20:27');
INSERT INTO `using_log` VALUES (2611, 1, 'RequestService', '', '2013-11-23 11:20:29');
INSERT INTO `using_log` VALUES (2612, 1, 'RequestService', 'Request', '2013-11-23 11:20:31');
INSERT INTO `using_log` VALUES (2613, NULL, '', '', '2013-11-26 23:57:08');
INSERT INTO `using_log` VALUES (2614, NULL, '', '', '2013-11-27 20:46:47');
INSERT INTO `using_log` VALUES (2615, 1, 'RequestBooking', '', '2013-11-27 20:46:52');
INSERT INTO `using_log` VALUES (2616, 1, 'RequestBooking', 'Request', '2013-11-27 20:47:09');
INSERT INTO `using_log` VALUES (2617, 1, 'RequestBooking', 'Request', '2013-11-27 20:47:24');
INSERT INTO `using_log` VALUES (2618, 1, 'RequestBorrow', '', '2013-11-27 20:47:28');
INSERT INTO `using_log` VALUES (2619, 1, 'RequestBorrow', 'CheckStatus', '2013-11-27 20:47:30');
INSERT INTO `using_log` VALUES (2620, 1, 'RequestBorrow', 'request', '2013-11-27 20:47:34');
INSERT INTO `using_log` VALUES (2621, 1, 'RequestBorrow', 'CheckStatus', '2013-11-27 20:47:37');
INSERT INTO `using_log` VALUES (2622, 1, 'RequestBorrow', 'Request', '2013-11-27 20:47:39');
INSERT INTO `using_log` VALUES (2623, 1, 'RequestBorrow', 'Request', '2013-11-27 20:51:14');
INSERT INTO `using_log` VALUES (2624, 1, 'RequestBorrow', 'Request', '2013-11-27 20:54:18');
INSERT INTO `using_log` VALUES (2625, 1, 'RequestBorrow', 'Request', '2013-11-27 20:57:47');
INSERT INTO `using_log` VALUES (2626, 1, 'RequestBorrow', 'Request', '2013-11-27 20:58:54');
INSERT INTO `using_log` VALUES (2627, 1, 'RequestBorrow', 'Request', '2013-11-27 21:00:17');
INSERT INTO `using_log` VALUES (2628, 1, 'RequestBorrow', 'Request', '2013-11-27 21:02:14');
INSERT INTO `using_log` VALUES (2629, 1, 'RequestBorrow', 'Request', '2013-11-27 21:02:24');
INSERT INTO `using_log` VALUES (2630, 1, 'RequestBorrow', 'Request', '2013-11-27 21:03:11');
INSERT INTO `using_log` VALUES (2631, 1, 'RequestBorrow', 'View', '2013-11-27 21:03:47');
INSERT INTO `using_log` VALUES (2632, 1, 'RequestBorrow', '', '2013-11-27 21:03:49');
INSERT INTO `using_log` VALUES (2633, 1, 'RequestBorrow', 'CheckStatus', '2013-11-27 21:03:51');
INSERT INTO `using_log` VALUES (2634, 1, 'RequestBorrow', 'Request', '2013-11-27 21:03:53');
INSERT INTO `using_log` VALUES (2635, 1, 'RequestBorrow', 'ListApprove', '2013-11-27 21:03:54');
INSERT INTO `using_log` VALUES (2636, 1, 'RequestBooking', '', '2013-11-27 21:04:31');
INSERT INTO `using_log` VALUES (2637, 1, 'RequestBorrow', '', '2013-11-27 21:04:33');
INSERT INTO `using_log` VALUES (2638, 1, 'RequestBorrow', 'ListApprove', '2013-11-27 21:04:35');
INSERT INTO `using_log` VALUES (2639, 1, 'RequestBorrow', 'ListApprove', '2013-11-27 21:04:46');
INSERT INTO `using_log` VALUES (2640, 1, 'RequestBorrow', '', '2013-11-27 21:04:48');
INSERT INTO `using_log` VALUES (2641, 1, 'RequestBorrow', 'ListApprove', '2013-11-27 21:04:53');
INSERT INTO `using_log` VALUES (2642, 1, 'RequestBorrow', 'CheckStatus', '2013-11-27 21:04:56');
INSERT INTO `using_log` VALUES (2643, 1, 'RequestBorrow', 'Request', '2013-11-27 21:04:58');
INSERT INTO `using_log` VALUES (2644, 1, 'RequestService', '', '2013-11-27 21:04:59');
INSERT INTO `using_log` VALUES (2645, 1, 'RequestService', 'Approve', '2013-11-27 21:05:01');
INSERT INTO `using_log` VALUES (2646, 1, 'RequestService', 'Request', '2013-11-27 21:05:02');
INSERT INTO `using_log` VALUES (2647, 1, 'RequestService', 'View', '2013-11-27 21:05:16');
INSERT INTO `using_log` VALUES (2648, 1, 'RequestService', 'View', '2013-11-27 21:05:20');
INSERT INTO `using_log` VALUES (2649, 1, 'RequestService', '', '2013-11-27 21:05:22');
INSERT INTO `using_log` VALUES (2650, 1, 'RequestService', '', '2013-11-27 21:05:24');
INSERT INTO `using_log` VALUES (2651, 1, 'RequestService', 'Index', '2013-11-27 21:05:30');
INSERT INTO `using_log` VALUES (2652, 1, 'RequestService', 'Index', '2013-11-27 21:05:30');
INSERT INTO `using_log` VALUES (2653, 1, 'RequestService', 'Request', '2013-11-27 21:05:39');
INSERT INTO `using_log` VALUES (2654, 1, 'RequestService', 'Approve', '2013-11-27 21:05:41');
INSERT INTO `using_log` VALUES (2655, 1, 'RequestService', '', '2013-11-27 21:05:42');
INSERT INTO `using_log` VALUES (2656, 1, 'Solution', '', '2013-11-27 21:06:11');
INSERT INTO `using_log` VALUES (2657, 1, 'Report', '', '2013-11-27 21:06:11');
INSERT INTO `using_log` VALUES (2658, NULL, '', '', '2013-11-28 19:40:56');
INSERT INTO `using_log` VALUES (2659, 1, 'RequestBooking', '', '2013-11-28 19:41:05');
INSERT INTO `using_log` VALUES (2660, NULL, '', '', '2013-11-28 19:43:30');
INSERT INTO `using_log` VALUES (2661, NULL, 'management', 'login', '2013-11-28 19:43:36');
INSERT INTO `using_log` VALUES (2662, NULL, 'Management', 'Register', '2013-11-28 19:43:43');
INSERT INTO `using_log` VALUES (2663, NULL, 'Management', 'Register', '2013-11-28 19:44:08');
INSERT INTO `using_log` VALUES (2664, 4, 'RequestBooking', '', '2013-11-28 19:45:18');
INSERT INTO `using_log` VALUES (2665, 4, 'RequestBooking', 'Request', '2013-11-28 19:45:22');
INSERT INTO `using_log` VALUES (2666, 4, 'RequestBooking', 'Request', '2013-11-28 07:45:56');
INSERT INTO `using_log` VALUES (2667, 4, 'RequestBorrow', '', '2013-11-28 07:46:24');
INSERT INTO `using_log` VALUES (2668, 4, 'RequestService', '', '2013-11-28 07:46:25');
INSERT INTO `using_log` VALUES (2669, 4, 'RequestBooking', '', '2013-11-28 07:46:26');
INSERT INTO `using_log` VALUES (2670, 4, 'RequestBooking', 'Request', '2013-11-28 07:46:27');
INSERT INTO `using_log` VALUES (2671, 4, 'RequestBooking', 'View', '2013-11-28 07:49:04');
INSERT INTO `using_log` VALUES (2672, 4, 'RequestBooking', '', '2013-11-28 07:49:14');
INSERT INTO `using_log` VALUES (2673, 4, 'requestBooking', 'view', '2013-11-28 07:49:18');
INSERT INTO `using_log` VALUES (2674, 4, 'RequestBooking', 'View', '2013-11-28 07:49:21');
INSERT INTO `using_log` VALUES (2675, 4, 'RequestBooking', '', '2013-11-28 07:49:23');
INSERT INTO `using_log` VALUES (2676, 1, 'RequestBorrow', '', '2013-11-28 07:49:33');
INSERT INTO `using_log` VALUES (2677, 1, 'RequestBorrow', 'Index', '2013-11-28 07:49:42');
INSERT INTO `using_log` VALUES (2678, 1, 'RequestBorrow', 'Index', '2013-11-28 07:49:42');
INSERT INTO `using_log` VALUES (2679, 1, 'RequestBorrow', 'CheckStatus', '2013-11-28 07:49:48');
INSERT INTO `using_log` VALUES (2680, 1, 'RequestBorrow', '', '2013-11-28 07:49:50');
INSERT INTO `using_log` VALUES (2681, 1, 'RequestBorrow', 'Index', '2013-11-28 07:49:53');
INSERT INTO `using_log` VALUES (2682, 1, 'RequestBorrow', 'Index', '2013-11-28 07:49:53');
INSERT INTO `using_log` VALUES (2683, 4, 'RequestBooking', 'Request', '2013-11-28 07:50:04');
INSERT INTO `using_log` VALUES (2684, 4, 'RequestBooking', 'View', '2013-11-28 07:50:38');
INSERT INTO `using_log` VALUES (2685, 4, 'RequestBooking', '', '2013-11-28 07:50:42');
INSERT INTO `using_log` VALUES (2686, 1, 'RequestBorrow', '', '2013-11-28 07:50:46');
INSERT INTO `using_log` VALUES (2687, 1, 'RequestBorrow', 'Index', '2013-11-28 07:50:49');
INSERT INTO `using_log` VALUES (2688, 1, 'RequestBorrow', 'Index', '2013-11-28 07:50:49');
INSERT INTO `using_log` VALUES (2689, 1, 'RequestBorrow', 'Index', '2013-11-28 07:50:56');
INSERT INTO `using_log` VALUES (2690, 1, 'RequestBorrow', 'Index', '2013-11-28 07:50:56');
INSERT INTO `using_log` VALUES (2691, 1, 'RequestBorrow', 'Index', '2013-11-28 07:50:58');
INSERT INTO `using_log` VALUES (2692, 1, 'RequestBorrow', 'Index', '2013-11-28 07:50:58');
INSERT INTO `using_log` VALUES (2693, 4, 'requestBooking', 'view', '2013-11-28 07:51:11');
INSERT INTO `using_log` VALUES (2694, 4, 'RequestBooking', '', '2013-11-28 07:51:13');
INSERT INTO `using_log` VALUES (2695, 1, 'RequestBorrow', '', '2013-11-28 07:51:18');
INSERT INTO `using_log` VALUES (2696, 1, 'RequestBorrow', 'ListApprove', '2013-11-28 07:51:26');
INSERT INTO `using_log` VALUES (2697, 1, 'RequestBorrow', '', '2013-11-28 07:51:33');
INSERT INTO `using_log` VALUES (2698, 1, 'RequestBorrow', 'Index', '2013-11-28 07:56:29');
INSERT INTO `using_log` VALUES (2699, 1, 'RequestBorrow', 'Index', '2013-11-28 07:56:29');
INSERT INTO `using_log` VALUES (2700, 1, 'RequestBorrow', 'Request', '2013-11-28 07:56:57');
INSERT INTO `using_log` VALUES (2701, 1, 'RequestBorrow', '', '2013-11-28 07:56:59');
INSERT INTO `using_log` VALUES (2702, 4, 'RequestBooking', 'Request', '2013-11-28 07:57:03');
INSERT INTO `using_log` VALUES (2703, 4, 'RequestBooking', 'View', '2013-11-28 07:57:36');
INSERT INTO `using_log` VALUES (2704, 4, 'RequestBooking', '', '2013-11-28 07:57:38');
INSERT INTO `using_log` VALUES (2705, 1, 'RequestBooking', '', '2013-11-28 07:58:21');
INSERT INTO `using_log` VALUES (2706, 1, 'RequestBooking', 'Approve', '2013-11-28 07:58:44');
INSERT INTO `using_log` VALUES (2707, 1, 'RequestBooking', '', '2013-11-28 07:58:52');
INSERT INTO `using_log` VALUES (2708, 1, 'requestBooking', 'view', '2013-11-28 07:59:14');
INSERT INTO `using_log` VALUES (2709, 1, 'RequestBooking', '', '2013-11-28 07:59:18');
INSERT INTO `using_log` VALUES (2710, 1, 'RequestBooking', '', '2013-11-28 08:03:48');
INSERT INTO `using_log` VALUES (2711, 1, 'RequestBooking', 'Approve', '2013-11-28 08:03:52');
INSERT INTO `using_log` VALUES (2712, 1, 'RequestBooking', '', '2013-11-28 08:03:54');
INSERT INTO `using_log` VALUES (2713, 1, 'RequestBooking', 'Index', '2013-11-28 08:04:27');
INSERT INTO `using_log` VALUES (2714, 1, 'RequestBooking', 'Index', '2013-11-28 08:04:30');
INSERT INTO `using_log` VALUES (2715, 1, 'RequestBooking', '', '2013-11-28 08:06:33');
INSERT INTO `using_log` VALUES (2716, 1, 'RequestBooking', '', '2013-11-28 08:06:36');
INSERT INTO `using_log` VALUES (2717, 1, 'RequestBooking', '', '2013-11-28 08:06:38');
INSERT INTO `using_log` VALUES (2718, 4, 'RequestBooking', '', '2013-11-28 08:08:11');
INSERT INTO `using_log` VALUES (2719, 1, 'RequestBooking', '', '2013-11-28 08:08:17');
INSERT INTO `using_log` VALUES (2720, 1, 'RequestBooking', '', '2013-11-28 08:10:00');
INSERT INTO `using_log` VALUES (2721, 1, 'RequestBooking', '', '2013-11-28 08:12:11');
INSERT INTO `using_log` VALUES (2722, 1, 'RequestBooking', '', '2013-11-28 08:12:14');
INSERT INTO `using_log` VALUES (2723, 1, 'RequestBooking', '', '2013-11-28 08:13:28');
INSERT INTO `using_log` VALUES (2724, 1, 'RequestBooking', '', '2013-11-28 08:16:35');
INSERT INTO `using_log` VALUES (2725, 1, 'RequestBooking', 'Index', '2013-11-28 08:16:45');
INSERT INTO `using_log` VALUES (2726, 1, 'RequestBooking', 'Index', '2013-11-28 08:16:50');
INSERT INTO `using_log` VALUES (2727, 1, 'RequestBooking', '', '2013-11-28 08:17:09');
INSERT INTO `using_log` VALUES (2728, 1, 'RequestBooking', '', '2013-11-28 08:22:11');
INSERT INTO `using_log` VALUES (2729, 1, 'RequestBooking', '', '2013-11-28 08:23:27');
INSERT INTO `using_log` VALUES (2730, 1, 'RequestBooking', '', '2013-11-28 08:25:09');
INSERT INTO `using_log` VALUES (2731, 1, 'RequestBooking', '', '2013-11-28 08:25:11');
INSERT INTO `using_log` VALUES (2732, 4, 'RequestBooking', '', '2013-11-28 08:25:27');
INSERT INTO `using_log` VALUES (2733, 4, 'RequestBooking', '', '2013-11-28 08:25:51');
INSERT INTO `using_log` VALUES (2734, 4, 'RequestBooking', '', '2013-11-28 08:26:09');
INSERT INTO `using_log` VALUES (2735, 1, 'RequestBooking', '', '2013-11-28 08:26:21');
INSERT INTO `using_log` VALUES (2736, 1, 'RequestBooking', '', '2013-11-28 08:30:17');
INSERT INTO `using_log` VALUES (2737, 1, 'RequestBooking', '', '2013-11-28 08:30:19');
INSERT INTO `using_log` VALUES (2738, 4, 'RequestBooking', '', '2013-11-28 08:30:24');
INSERT INTO `using_log` VALUES (2739, 1, 'RequestBooking', '', '2013-11-28 08:36:14');
INSERT INTO `using_log` VALUES (2740, 1, 'RequestBooking', '', '2013-11-28 08:36:16');
INSERT INTO `using_log` VALUES (2741, 4, 'RequestBooking', '', '2013-11-28 08:36:20');
INSERT INTO `using_log` VALUES (2742, 4, 'RequestBooking', '', '2013-11-28 08:36:25');
INSERT INTO `using_log` VALUES (2743, 4, 'RequestBooking', '', '2013-11-28 08:36:26');
INSERT INTO `using_log` VALUES (2744, 1, 'RequestBooking', 'Approve', '2013-11-28 08:36:44');
INSERT INTO `using_log` VALUES (2745, 1, 'RequestBooking', '', '2013-11-28 08:36:47');
INSERT INTO `using_log` VALUES (2746, 1, 'requestBooking', 'view', '2013-11-28 08:37:02');
INSERT INTO `using_log` VALUES (2747, 1, 'RequestBooking', '', '2013-11-28 08:37:05');
INSERT INTO `using_log` VALUES (2748, 1, 'RequestBooking', 'Index', '2013-11-28 08:52:22');
INSERT INTO `using_log` VALUES (2749, 1, 'RequestBooking', '', '2013-11-28 08:53:44');
INSERT INTO `using_log` VALUES (2750, 1, 'RequestBooking', '', '2013-11-28 08:56:08');
INSERT INTO `using_log` VALUES (2751, 1, 'RequestBooking', '', '2013-11-28 08:56:50');
INSERT INTO `using_log` VALUES (2752, 4, 'RequestBooking', '', '2013-11-28 08:56:53');
INSERT INTO `using_log` VALUES (2753, 4, 'RequestBooking', '', '2013-11-28 08:56:55');
INSERT INTO `using_log` VALUES (2754, 4, 'RequestBooking', '', '2013-11-28 08:57:08');
INSERT INTO `using_log` VALUES (2755, 4, 'RequestBooking', '', '2013-11-28 08:57:09');
INSERT INTO `using_log` VALUES (2756, 1, 'RequestBooking', '', '2013-11-28 08:57:58');
INSERT INTO `using_log` VALUES (2757, 4, 'RequestBooking', '', '2013-11-28 08:58:04');
INSERT INTO `using_log` VALUES (2758, 1, 'RequestBooking', '', '2013-11-28 08:59:07');
INSERT INTO `using_log` VALUES (2759, 1, 'RequestBooking', '', '2013-11-28 08:59:09');
INSERT INTO `using_log` VALUES (2760, 1, 'RequestBooking', '', '2013-11-28 09:04:33');
INSERT INTO `using_log` VALUES (2761, 1, 'RequestBooking', '', '2013-11-28 09:04:34');
INSERT INTO `using_log` VALUES (2762, 4, 'RequestBooking', '', '2013-11-28 09:04:37');
INSERT INTO `using_log` VALUES (2763, 4, 'RequestBooking', '', '2013-11-28 09:04:39');
INSERT INTO `using_log` VALUES (2764, 4, 'RequestBooking', '', '2013-11-28 09:04:40');
INSERT INTO `using_log` VALUES (2765, 1, 'RequestBooking', '', '2013-11-28 09:04:48');
INSERT INTO `using_log` VALUES (2766, 1, 'RequestBooking', '', '2013-11-28 09:04:50');
INSERT INTO `using_log` VALUES (2767, 1, 'RequestBooking', '', '2013-11-28 09:06:13');
INSERT INTO `using_log` VALUES (2768, 4, 'RequestBooking', '', '2013-11-28 09:06:17');
INSERT INTO `using_log` VALUES (2769, 1, 'RequestBooking', '', '2013-11-28 09:06:19');
INSERT INTO `using_log` VALUES (2770, 4, 'RequestBooking', '', '2013-11-28 09:06:25');
INSERT INTO `using_log` VALUES (2771, 1, 'RequestBooking', '', '2013-11-28 09:06:43');
INSERT INTO `using_log` VALUES (2772, 1, 'RequestBooking', 'Index', '2013-11-28 09:07:03');
INSERT INTO `using_log` VALUES (2773, 1, 'RequestBooking', 'Index', '2013-11-28 09:07:04');
INSERT INTO `using_log` VALUES (2774, 1, 'RequestBooking', 'Index', '2013-11-28 09:07:08');
INSERT INTO `using_log` VALUES (2775, 1, 'RequestBooking', '', '2013-11-28 09:07:30');
INSERT INTO `using_log` VALUES (2776, 4, 'RequestBooking', '', '2013-11-28 09:07:32');
INSERT INTO `using_log` VALUES (2777, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:35');
INSERT INTO `using_log` VALUES (2778, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:36');
INSERT INTO `using_log` VALUES (2779, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:38');
INSERT INTO `using_log` VALUES (2780, 4, 'RequestBooking', '', '2013-11-28 09:07:39');
INSERT INTO `using_log` VALUES (2781, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:40');
INSERT INTO `using_log` VALUES (2782, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:42');
INSERT INTO `using_log` VALUES (2783, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:44');
INSERT INTO `using_log` VALUES (2784, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:46');
INSERT INTO `using_log` VALUES (2785, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:47');
INSERT INTO `using_log` VALUES (2786, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:49');
INSERT INTO `using_log` VALUES (2787, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:52');
INSERT INTO `using_log` VALUES (2788, 4, 'RequestBooking', 'Index', '2013-11-28 09:07:54');
INSERT INTO `using_log` VALUES (2789, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:04');
INSERT INTO `using_log` VALUES (2790, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:06');
INSERT INTO `using_log` VALUES (2791, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:07');
INSERT INTO `using_log` VALUES (2792, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:08');
INSERT INTO `using_log` VALUES (2793, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:09');
INSERT INTO `using_log` VALUES (2794, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:11');
INSERT INTO `using_log` VALUES (2795, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:12');
INSERT INTO `using_log` VALUES (2796, 1, 'RequestBooking', 'Index', '2013-11-28 09:08:14');
INSERT INTO `using_log` VALUES (2797, 1, 'RequestBooking', 'Index', '2013-11-28 09:10:10');
INSERT INTO `using_log` VALUES (2798, 1, 'RequestBooking', '', '2013-11-28 09:11:48');
INSERT INTO `using_log` VALUES (2799, 1, 'RequestBooking', '', '2013-11-28 09:12:17');
INSERT INTO `using_log` VALUES (2800, 1, 'RequestBooking', '', '2013-11-28 09:14:08');
INSERT INTO `using_log` VALUES (2801, 1, 'RequestBooking', '', '2013-11-28 09:15:46');
INSERT INTO `using_log` VALUES (2802, 4, 'RequestBooking', '', '2013-11-28 09:16:41');
INSERT INTO `using_log` VALUES (2803, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:44');
INSERT INTO `using_log` VALUES (2804, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:45');
INSERT INTO `using_log` VALUES (2805, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:47');
INSERT INTO `using_log` VALUES (2806, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:49');
INSERT INTO `using_log` VALUES (2807, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:50');
INSERT INTO `using_log` VALUES (2808, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:52');
INSERT INTO `using_log` VALUES (2809, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:55');
INSERT INTO `using_log` VALUES (2810, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:58');
INSERT INTO `using_log` VALUES (2811, 4, 'RequestBooking', 'Index', '2013-11-28 09:16:59');
INSERT INTO `using_log` VALUES (2812, 4, 'RequestBooking', 'Index', '2013-11-28 09:17:01');
INSERT INTO `using_log` VALUES (2813, 4, 'RequestBooking', 'Index', '2013-11-28 09:17:03');
INSERT INTO `using_log` VALUES (2814, 4, 'RequestBooking', 'Index', '2013-11-28 09:17:06');
INSERT INTO `using_log` VALUES (2815, 1, 'RequestBooking', '', '2013-11-28 09:17:20');
INSERT INTO `using_log` VALUES (2816, 1, 'RequestBooking', '', '2013-11-28 09:28:24');
INSERT INTO `using_log` VALUES (2817, 1, 'RequestBooking', '', '2013-11-28 09:29:28');
INSERT INTO `using_log` VALUES (2818, 4, 'RequestBooking', '', '2013-11-28 09:29:35');
INSERT INTO `using_log` VALUES (2819, 1, 'RequestBooking', '', '2013-11-28 09:29:50');
INSERT INTO `using_log` VALUES (2820, 4, 'RequestBooking', '', '2013-11-28 09:29:55');
INSERT INTO `using_log` VALUES (2821, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:17');
INSERT INTO `using_log` VALUES (2822, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:19');
INSERT INTO `using_log` VALUES (2823, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:22');
INSERT INTO `using_log` VALUES (2824, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:24');
INSERT INTO `using_log` VALUES (2825, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:26');
INSERT INTO `using_log` VALUES (2826, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:27');
INSERT INTO `using_log` VALUES (2827, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:29');
INSERT INTO `using_log` VALUES (2828, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:31');
INSERT INTO `using_log` VALUES (2829, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:34');
INSERT INTO `using_log` VALUES (2830, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:36');
INSERT INTO `using_log` VALUES (2831, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:47');
INSERT INTO `using_log` VALUES (2832, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:50');
INSERT INTO `using_log` VALUES (2833, 1, 'RequestBooking', 'Index', '2013-11-28 09:30:52');
INSERT INTO `using_log` VALUES (2834, 1, 'RequestBooking', 'Index', '2013-11-28 09:31:05');
INSERT INTO `using_log` VALUES (2835, 1, 'RequestBooking', 'Index', '2013-11-28 09:31:06');
INSERT INTO `using_log` VALUES (2836, 1, 'RequestBooking', 'Index', '2013-11-28 09:31:08');
INSERT INTO `using_log` VALUES (2837, 4, 'RequestBooking', '', '2013-11-28 09:33:25');
INSERT INTO `using_log` VALUES (2838, 4, 'RequestBooking', 'Index', '2013-11-28 09:33:27');
INSERT INTO `using_log` VALUES (2839, 4, 'RequestBooking', 'Index', '2013-11-28 09:33:29');
INSERT INTO `using_log` VALUES (2840, 4, 'RequestBooking', 'Index', '2013-11-28 09:33:35');
INSERT INTO `using_log` VALUES (2841, 4, 'RequestBooking', 'Index', '2013-11-28 09:33:37');
INSERT INTO `using_log` VALUES (2842, 4, 'RequestBooking', 'Index', '2013-11-28 09:33:47');
INSERT INTO `using_log` VALUES (2843, 1, 'RequestBooking', 'Request', '2013-11-28 09:34:59');
INSERT INTO `using_log` VALUES (2844, 1, 'RequestBooking', '', '2013-11-28 09:40:43');
INSERT INTO `using_log` VALUES (2845, 4, 'RequestBooking', 'Request', '2013-11-28 09:40:58');
INSERT INTO `using_log` VALUES (2846, 4, 'RequestBooking', 'View', '2013-11-28 09:41:36');
INSERT INTO `using_log` VALUES (2847, 4, 'RequestBooking', '', '2013-11-28 09:41:38');
INSERT INTO `using_log` VALUES (2848, 4, 'RequestBooking', '', '2013-11-28 09:41:47');
INSERT INTO `using_log` VALUES (2849, 4, 'requestBooking', 'view', '2013-11-28 09:41:52');
INSERT INTO `using_log` VALUES (2850, 4, 'RequestBooking', '', '2013-11-28 09:41:57');
INSERT INTO `using_log` VALUES (2851, 4, 'RequestBooking', '', '2013-11-28 09:44:03');
INSERT INTO `using_log` VALUES (2852, 4, 'RequestBooking', 'Request', '2013-11-28 09:44:05');
INSERT INTO `using_log` VALUES (2853, 4, 'RequestBooking', 'View', '2013-11-28 09:44:28');
INSERT INTO `using_log` VALUES (2854, 4, 'RequestBooking', '', '2013-11-28 09:44:30');
INSERT INTO `using_log` VALUES (2855, 1, 'RequestBooking', '', '2013-11-28 09:44:35');
INSERT INTO `using_log` VALUES (2856, 1, 'RequestBooking', '', '2013-11-28 09:47:32');
INSERT INTO `using_log` VALUES (2857, 4, 'RequestBooking', 'Request', '2013-11-28 09:48:43');
INSERT INTO `using_log` VALUES (2858, 4, 'RequestBooking', 'View', '2013-11-28 09:49:01');
INSERT INTO `using_log` VALUES (2859, 4, 'RequestBooking', '', '2013-11-28 09:49:03');
INSERT INTO `using_log` VALUES (2860, 1, 'RequestBooking', '', '2013-11-28 09:49:13');
INSERT INTO `using_log` VALUES (2861, 1, 'RequestBooking', 'Index', '2013-11-28 09:49:45');
INSERT INTO `using_log` VALUES (2862, 1, 'RequestBooking', 'Index', '2013-11-28 09:49:46');
INSERT INTO `using_log` VALUES (2863, 1, 'RequestBooking', 'Index', '2013-11-28 09:49:49');
INSERT INTO `using_log` VALUES (2864, 1, 'RequestBooking', '', '2013-11-28 09:53:14');
INSERT INTO `using_log` VALUES (2865, 1, 'RequestBooking', 'Index', '2013-11-28 09:53:22');
INSERT INTO `using_log` VALUES (2866, 1, 'RequestBooking', 'Index', '2013-11-28 09:53:24');
INSERT INTO `using_log` VALUES (2867, 1, 'RequestBooking', 'Index', '2013-11-28 09:53:31');
INSERT INTO `using_log` VALUES (2868, 1, 'RequestBooking', 'Index', '2013-11-28 09:53:34');
INSERT INTO `using_log` VALUES (2869, 1, 'RequestBooking', 'Index', '2013-11-28 09:53:36');
INSERT INTO `using_log` VALUES (2870, 1, 'RequestBooking', 'Index', '2013-11-28 09:53:41');
INSERT INTO `using_log` VALUES (2871, 1, 'RequestBooking', '', '2013-11-28 09:56:44');
INSERT INTO `using_log` VALUES (2872, 1, 'RequestBooking', 'Index', '2013-11-28 09:56:46');
INSERT INTO `using_log` VALUES (2873, 1, 'RequestBooking', 'Index', '2013-11-28 09:56:49');
INSERT INTO `using_log` VALUES (2874, 1, 'RequestBooking', 'Index', '2013-11-28 09:56:51');
INSERT INTO `using_log` VALUES (2875, 1, 'RequestBooking', 'Index', '2013-11-28 09:56:55');
INSERT INTO `using_log` VALUES (2876, 4, 'RequestBooking', '', '2013-11-28 09:57:00');
INSERT INTO `using_log` VALUES (2877, 4, 'RequestBooking', 'Index', '2013-11-28 09:57:02');
INSERT INTO `using_log` VALUES (2878, 4, 'RequestBooking', 'Index', '2013-11-28 09:57:04');
INSERT INTO `using_log` VALUES (2879, 1, 'RequestBorrow', '', '2013-11-28 09:57:45');
INSERT INTO `using_log` VALUES (2880, 4, 'RequestBorrow', '', '2013-11-28 09:57:55');
INSERT INTO `using_log` VALUES (2881, 1, 'RequestBooking', '', '2013-11-28 09:58:28');
INSERT INTO `using_log` VALUES (2882, 4, 'RequestBooking', '', '2013-11-28 09:58:45');
INSERT INTO `using_log` VALUES (2883, 4, 'Management', 'EditProfile', '2013-11-28 09:59:15');
INSERT INTO `using_log` VALUES (2884, 4, 'Management', 'EditProfile', '2013-11-28 10:01:15');
INSERT INTO `using_log` VALUES (2885, 1, 'Management', 'EditProfile', '2013-11-28 10:03:21');
INSERT INTO `using_log` VALUES (2886, 1, 'Management', 'EditProfile', '2013-11-28 10:03:26');
INSERT INTO `using_log` VALUES (2887, 4, 'Management', 'EditProfile', '2013-11-28 10:03:31');
INSERT INTO `using_log` VALUES (2888, 4, 'RequestBooking', '', '2013-11-28 10:03:36');
INSERT INTO `using_log` VALUES (2889, 4, 'Management', 'EditProfile', '2013-11-28 10:03:40');
INSERT INTO `using_log` VALUES (2890, 4, 'RequestBooking', '', '2013-11-28 10:04:10');
INSERT INTO `using_log` VALUES (2891, 4, 'Management', 'EditProfile', '2013-11-28 10:04:11');
INSERT INTO `using_log` VALUES (2892, 4, 'Management', 'EditProfile', '2013-11-28 10:04:14');
INSERT INTO `using_log` VALUES (2893, 1, 'RequestBooking', '', '2013-11-28 10:04:17');
INSERT INTO `using_log` VALUES (2894, 1, 'RequestBooking', '', '2013-11-28 10:04:19');
INSERT INTO `using_log` VALUES (2895, 1, 'RequestBooking', '', '2013-11-28 10:46:31');
INSERT INTO `using_log` VALUES (2896, 1, 'Management', 'EditProfile', '2013-11-28 10:46:35');
INSERT INTO `using_log` VALUES (2897, 1, 'Management', 'EditProfile', '2013-11-28 10:46:42');
INSERT INTO `using_log` VALUES (2898, 1, 'RequestBooking', '', '2013-11-28 11:54:25');
INSERT INTO `using_log` VALUES (2899, 1, 'Management', 'EditProfile', '2013-11-28 11:54:27');
INSERT INTO `using_log` VALUES (2900, 1, 'Management', 'EditProfile', '2013-11-28 11:54:34');
INSERT INTO `using_log` VALUES (2901, 1, 'RequestBooking', '', '2013-11-28 11:54:37');
INSERT INTO `using_log` VALUES (2902, 1, '', '', '2013-11-28 12:28:33');
INSERT INTO `using_log` VALUES (2903, 1, 'RequestBooking', '', '2013-11-28 12:28:36');
INSERT INTO `using_log` VALUES (2904, 1, 'RequestBooking', 'Approve', '2013-11-28 12:29:12');
INSERT INTO `using_log` VALUES (2905, 1, 'RequestBooking', 'Approve', '2013-11-28 12:29:20');
INSERT INTO `using_log` VALUES (2906, 1, 'RequestBooking', 'Approve', '2013-11-28 12:29:26');
INSERT INTO `using_log` VALUES (2907, 1, 'RequestBooking', 'Approve', '2013-11-28 12:29:41');
INSERT INTO `using_log` VALUES (2908, 1, 'RequestBooking', 'Approve', '2013-11-28 12:31:10');
INSERT INTO `using_log` VALUES (2909, 1, 'RequestBooking', '', '2013-11-28 12:31:17');
INSERT INTO `using_log` VALUES (2910, 1, 'RequestBorrow', '', '2013-11-28 12:31:21');
INSERT INTO `using_log` VALUES (2911, 4, 'RequestBorrow', '', '2013-11-28 12:32:15');
INSERT INTO `using_log` VALUES (2912, 4, 'RequestBorrow', 'Request', '2013-11-28 12:32:18');
INSERT INTO `using_log` VALUES (2913, 4, 'RequestBorrow', 'View', '2013-11-28 12:32:52');
INSERT INTO `using_log` VALUES (2914, 4, 'RequestBorrow', '', '2013-11-28 12:32:55');
INSERT INTO `using_log` VALUES (2915, 4, 'RequestBorrow', 'Index', '2013-11-28 12:32:59');
INSERT INTO `using_log` VALUES (2916, 4, 'RequestBorrow', 'Index', '2013-11-28 12:32:59');
INSERT INTO `using_log` VALUES (2917, 4, 'requestBorrow', 'view', '2013-11-28 12:33:43');
INSERT INTO `using_log` VALUES (2918, 4, 'RequestBorrow', '', '2013-11-28 12:33:45');
INSERT INTO `using_log` VALUES (2919, 1, 'requestBorrow', 'update', '2013-11-28 12:33:53');
INSERT INTO `using_log` VALUES (2920, 1, 'RequestBorrow', '', '2013-11-28 12:34:04');
INSERT INTO `using_log` VALUES (2921, 4, 'RequestBorrow', 'Index', '2013-11-28 12:34:13');
INSERT INTO `using_log` VALUES (2922, 4, 'RequestBorrow', 'Index', '2013-11-28 12:34:13');
INSERT INTO `using_log` VALUES (2923, 4, 'RequestBorrow', '', '2013-11-28 12:34:16');
INSERT INTO `using_log` VALUES (2924, 4, 'RequestBorrow', 'Index', '2013-11-28 12:34:19');
INSERT INTO `using_log` VALUES (2925, 4, 'RequestBorrow', 'Index', '2013-11-28 12:34:19');
INSERT INTO `using_log` VALUES (2926, 1, 'RequestBorrow', 'Index', '2013-11-28 12:34:30');
INSERT INTO `using_log` VALUES (2927, 1, 'RequestBorrow', 'Index', '2013-11-28 12:34:30');
INSERT INTO `using_log` VALUES (2928, 1, 'requestBorrow', 'update', '2013-11-28 12:34:32');
INSERT INTO `using_log` VALUES (2929, 1, 'RequestBorrow', '', '2013-11-28 12:34:35');
INSERT INTO `using_log` VALUES (2930, 1, 'RequestBorrow', 'Index', '2013-11-28 12:34:41');
INSERT INTO `using_log` VALUES (2931, 1, 'RequestBorrow', 'Index', '2013-11-28 12:34:41');
INSERT INTO `using_log` VALUES (2932, 4, 'RequestBorrow', '', '2013-11-28 12:34:47');
INSERT INTO `using_log` VALUES (2933, 4, 'RequestBorrow', 'Index', '2013-11-28 12:34:49');
INSERT INTO `using_log` VALUES (2934, 4, 'RequestBorrow', 'Index', '2013-11-28 12:34:50');
INSERT INTO `using_log` VALUES (2935, 4, 'RequestBorrow', 'CheckStatus', '2013-11-28 12:34:58');
INSERT INTO `using_log` VALUES (2936, 4, 'RequestBorrow', 'Request', '2013-11-28 12:35:00');
INSERT INTO `using_log` VALUES (2937, 4, 'RequestBorrow', 'View', '2013-11-28 12:35:36');
INSERT INTO `using_log` VALUES (2938, 4, 'RequestBorrow', '', '2013-11-28 12:35:38');
INSERT INTO `using_log` VALUES (2939, 4, 'RequestBorrow', 'Index', '2013-11-28 12:35:44');
INSERT INTO `using_log` VALUES (2940, 4, 'RequestBorrow', 'Index', '2013-11-28 12:35:44');
INSERT INTO `using_log` VALUES (2941, 4, 'RequestBorrow', 'Index', '2013-11-28 12:35:46');
INSERT INTO `using_log` VALUES (2942, 4, 'RequestBorrow', 'Index', '2013-11-28 12:35:46');
INSERT INTO `using_log` VALUES (2943, 4, 'RequestBorrow', 'Index', '2013-11-28 12:35:49');
INSERT INTO `using_log` VALUES (2944, 4, 'RequestBorrow', 'Index', '2013-11-28 12:35:49');
INSERT INTO `using_log` VALUES (2945, 1, 'RequestBorrow', '', '2013-11-28 12:35:59');
INSERT INTO `using_log` VALUES (2946, 1, 'RequestBorrow', 'ListApprove', '2013-11-28 12:39:57');
INSERT INTO `using_log` VALUES (2947, 4, 'Management', 'EditProfile', '2013-11-28 12:40:05');
INSERT INTO `using_log` VALUES (2948, 4, 'Management', 'EditProfile', '2013-11-28 12:40:10');
INSERT INTO `using_log` VALUES (2949, 1, 'RequestBorrow', 'ListApprove', '2013-11-28 12:40:13');
INSERT INTO `using_log` VALUES (2950, 1, 'RequestBorrow', 'ListApprove', '2013-11-28 12:40:25');
INSERT INTO `using_log` VALUES (2951, 1, 'RequestBorrow', '', '2013-11-28 12:40:33');
INSERT INTO `using_log` VALUES (2952, 1, 'RequestBorrow', 'Index', '2013-11-28 12:40:39');
INSERT INTO `using_log` VALUES (2953, 1, 'RequestBorrow', 'Index', '2013-11-28 12:40:40');
INSERT INTO `using_log` VALUES (2954, 1, 'RequestBorrow', 'Index', '2013-11-28 12:40:43');
INSERT INTO `using_log` VALUES (2955, 1, 'RequestBorrow', 'Index', '2013-11-28 12:40:43');
INSERT INTO `using_log` VALUES (2956, 4, 'RequestBorrow', '', '2013-11-28 12:40:49');
INSERT INTO `using_log` VALUES (2957, 4, 'RequestBorrow', 'Index', '2013-11-28 12:40:51');
INSERT INTO `using_log` VALUES (2958, 4, 'RequestBorrow', 'Index', '2013-11-28 12:40:51');
INSERT INTO `using_log` VALUES (2959, 4, 'RequestBorrow', 'Index', '2013-11-28 12:40:54');
INSERT INTO `using_log` VALUES (2960, 4, 'RequestBorrow', 'Index', '2013-11-28 12:40:54');
INSERT INTO `using_log` VALUES (2961, 4, 'requestBorrow', 'view', '2013-11-28 12:40:58');
INSERT INTO `using_log` VALUES (2962, 4, 'RequestBorrow', '', '2013-11-28 12:41:02');
INSERT INTO `using_log` VALUES (2963, 1, 'requestBorrow', 'update', '2013-11-28 12:41:06');
INSERT INTO `using_log` VALUES (2964, 4, 'RequestService', '', '2013-11-28 12:41:25');
INSERT INTO `using_log` VALUES (2965, 4, 'RequestService', 'Request', '2013-11-28 12:41:41');
INSERT INTO `using_log` VALUES (2966, 4, 'RequestService', 'View', '2013-11-28 12:42:07');
INSERT INTO `using_log` VALUES (2967, 4, 'RequestService', 'View', '2013-11-28 12:42:16');
INSERT INTO `using_log` VALUES (2968, 4, 'RequestService', '', '2013-11-28 12:42:17');
INSERT INTO `using_log` VALUES (2969, 4, 'RequestService', 'Index', '2013-11-28 12:42:21');
INSERT INTO `using_log` VALUES (2970, 4, 'RequestService', 'Index', '2013-11-28 12:42:21');
INSERT INTO `using_log` VALUES (2971, 4, 'requestService', 'view', '2013-11-28 12:42:23');
INSERT INTO `using_log` VALUES (2972, 4, 'requestService', 'view', '2013-11-28 12:42:27');
INSERT INTO `using_log` VALUES (2973, 4, 'RequestService', '', '2013-11-28 12:42:28');
INSERT INTO `using_log` VALUES (2974, 4, 'RequestService', 'Index', '2013-11-28 12:42:31');
INSERT INTO `using_log` VALUES (2975, 4, 'RequestService', 'Index', '2013-11-28 12:42:31');
INSERT INTO `using_log` VALUES (2976, 1, 'RequestService', '', '2013-11-28 12:42:35');
INSERT INTO `using_log` VALUES (2977, 1, 'RequestService', 'Index', '2013-11-28 12:42:42');
INSERT INTO `using_log` VALUES (2978, 1, 'RequestService', 'Index', '2013-11-28 12:42:42');
INSERT INTO `using_log` VALUES (2979, 1, 'requestService', 'view', '2013-11-28 12:42:55');
INSERT INTO `using_log` VALUES (2980, 1, 'RequestService', 'update', '2013-11-28 12:42:57');
INSERT INTO `using_log` VALUES (2981, 1, 'RequestService', '', '2013-11-28 12:43:05');
INSERT INTO `using_log` VALUES (2982, NULL, 'management', 'login', '2013-11-28 12:43:28');
INSERT INTO `using_log` VALUES (2983, NULL, '', '', '2013-11-28 20:23:31');
INSERT INTO `using_log` VALUES (2984, NULL, '', '', '2013-11-28 20:44:46');
INSERT INTO `using_log` VALUES (2985, NULL, 'management', 'login', '2013-11-28 20:50:17');
INSERT INTO `using_log` VALUES (2986, 1, 'RequestBooking', '', '2013-11-28 20:50:24');
INSERT INTO `using_log` VALUES (2987, 1, 'RequestBorrow', '', '2013-11-28 20:50:26');
INSERT INTO `using_log` VALUES (2988, 1, 'RequestBorrow', 'Request', '2013-11-28 20:50:28');
INSERT INTO `using_log` VALUES (2989, NULL, '', '', '2013-11-29 21:38:09');
INSERT INTO `using_log` VALUES (2990, 34, '', '', '2013-12-03 10:54:33');
INSERT INTO `using_log` VALUES (2991, 34, '', '', '2013-12-03 10:54:41');
INSERT INTO `using_log` VALUES (2992, 34, '', '', '2013-12-03 10:54:53');
INSERT INTO `using_log` VALUES (2993, NULL, 'management', 'login', '2013-12-03 10:54:54');
INSERT INTO `using_log` VALUES (2994, 1, 'RequestBooking', '', '2013-12-03 10:55:00');
INSERT INTO `using_log` VALUES (2995, 1, 'RequestService', '', '2013-12-03 10:55:12');
INSERT INTO `using_log` VALUES (2996, 1, 'RequestBorrow', '', '2013-12-03 10:55:14');
INSERT INTO `using_log` VALUES (2997, 1, 'RequestBooking', '', '2013-12-03 10:55:22');
INSERT INTO `using_log` VALUES (2998, 1, 'RequestBorrow', '', '2013-12-03 10:55:28');
INSERT INTO `using_log` VALUES (2999, 1, 'RequestBorrow', 'Request', '2013-12-03 10:55:30');
INSERT INTO `using_log` VALUES (3000, 1, 'RequestBorrow', 'View', '2013-12-03 10:55:55');
INSERT INTO `using_log` VALUES (3001, 1, 'RequestBorrow', '', '2013-12-03 10:55:57');
INSERT INTO `using_log` VALUES (3002, 1, 'RequestBorrow', 'ListApprove', '2013-12-03 10:56:04');
INSERT INTO `using_log` VALUES (3003, 1, 'RequestService', '', '2013-12-03 10:56:05');
INSERT INTO `using_log` VALUES (3004, 1, 'RequestBorrow', '', '2013-12-03 10:56:07');
INSERT INTO `using_log` VALUES (3005, 1, 'RequestBorrow', 'ListApprove', '2013-12-03 10:56:32');
INSERT INTO `using_log` VALUES (3006, 1, 'RequestBorrow', 'ListApprove', '2013-12-03 10:56:37');
INSERT INTO `using_log` VALUES (3007, 1, 'RequestBorrow', 'ListApprove', '2013-12-03 10:56:39');
INSERT INTO `using_log` VALUES (3008, 1, 'RequestBorrow', '', '2013-12-03 10:56:41');
INSERT INTO `using_log` VALUES (3009, 1, 'RequestService', '', '2013-12-03 10:56:49');
INSERT INTO `using_log` VALUES (3010, 1, 'RequestService', 'Request', '2013-12-03 10:56:52');
INSERT INTO `using_log` VALUES (3011, 1, 'RequestService', 'View', '2013-12-03 10:57:15');
INSERT INTO `using_log` VALUES (3012, 1, 'RequestService', '', '2013-12-03 10:57:18');
INSERT INTO `using_log` VALUES (3013, 1, 'RequestService', 'Index', '2013-12-03 10:57:24');
INSERT INTO `using_log` VALUES (3014, 1, 'RequestService', 'Index', '2013-12-03 10:57:24');
INSERT INTO `using_log` VALUES (3015, NULL, '', '', '2014-01-03 13:53:33');
INSERT INTO `using_log` VALUES (3016, 1, 'RequestBooking', '', '2014-01-03 13:53:37');
INSERT INTO `using_log` VALUES (3017, 1, 'RequestBooking', 'CheckStatus', '2014-01-03 13:53:49');
INSERT INTO `using_log` VALUES (3018, 1, 'RequestBooking', '', '2014-01-03 13:53:51');
INSERT INTO `using_log` VALUES (3019, NULL, 'management', 'login', '2014-01-03 13:53:59');
INSERT INTO `using_log` VALUES (3020, NULL, 'management', 'login', '2014-01-03 13:54:03');
INSERT INTO `using_log` VALUES (3021, 2, 'RequestBooking', '', '2014-01-03 13:54:09');
INSERT INTO `using_log` VALUES (3022, 2, 'Admin', '', '2014-01-03 13:54:13');
INSERT INTO `using_log` VALUES (3023, 2, 'RequestService', '', '2014-01-03 13:54:14');
INSERT INTO `using_log` VALUES (3024, 2, 'RequestBorrow', '', '2014-01-03 13:54:15');
INSERT INTO `using_log` VALUES (3025, 2, 'RequestBooking', '', '2014-01-03 13:54:16');
INSERT INTO `using_log` VALUES (3026, 2, 'RequestBooking', 'Approve', '2014-01-03 13:54:18');
INSERT INTO `using_log` VALUES (3027, 2, 'RequestBooking', 'Request', '2014-01-03 13:54:19');
INSERT INTO `using_log` VALUES (3028, 2, 'RequestBooking', 'View', '2014-01-03 13:55:02');
INSERT INTO `using_log` VALUES (3029, 2, 'RequestBooking', '', '2014-01-03 13:55:33');
INSERT INTO `using_log` VALUES (3030, 2, 'RequestBooking', 'Approve', '2014-01-03 13:55:47');
INSERT INTO `using_log` VALUES (3031, 2, 'RequestBooking', 'Request', '2014-01-03 13:55:49');
INSERT INTO `using_log` VALUES (3032, 2, 'RequestBooking', '', '2014-01-03 13:55:50');
INSERT INTO `using_log` VALUES (3033, 2, 'requestBooking', 'view', '2014-01-03 13:56:26');
INSERT INTO `using_log` VALUES (3034, 2, 'RequestBooking', '', '2014-01-03 13:56:28');
INSERT INTO `using_log` VALUES (3035, 2, '', '', '2014-01-03 14:26:38');
INSERT INTO `using_log` VALUES (3036, NULL, 'management', 'login', '2014-01-03 14:26:42');
INSERT INTO `using_log` VALUES (3037, 1, 'RequestBooking', '', '2014-01-03 14:26:52');
INSERT INTO `using_log` VALUES (3038, 1, 'RequestBooking', 'Index', '2014-01-03 14:27:04');
INSERT INTO `using_log` VALUES (3039, 1, 'RequestBooking', 'Index', '2014-01-03 14:27:06');
INSERT INTO `using_log` VALUES (3040, 1, 'RequestBooking', 'Request', '2014-01-03 14:27:17');
INSERT INTO `using_log` VALUES (3041, 1, 'RequestBooking', 'View', '2014-01-03 14:27:59');
INSERT INTO `using_log` VALUES (3042, 1, 'RequestBooking', '', '2014-01-03 14:28:09');
INSERT INTO `using_log` VALUES (3043, 1, 'RequestBooking', '', '2014-01-03 14:31:39');
INSERT INTO `using_log` VALUES (3044, 1, 'RequestBooking', '', '2014-01-03 14:40:59');
INSERT INTO `using_log` VALUES (3045, 1, 'RequestBooking', '', '2014-01-03 14:45:29');
INSERT INTO `using_log` VALUES (3046, 1, 'RequestBooking', '', '2014-01-03 14:45:47');
INSERT INTO `using_log` VALUES (3047, 1, 'RequestBooking', '', '2014-01-03 18:46:07');
INSERT INTO `using_log` VALUES (3048, 1, 'RequestBooking', '', '2014-01-03 18:46:17');
INSERT INTO `using_log` VALUES (3049, 1, 'RequestBooking', '', '2014-01-03 19:01:26');
INSERT INTO `using_log` VALUES (3050, 1, 'RequestBooking', '', '2014-01-03 19:01:29');
INSERT INTO `using_log` VALUES (3051, 1, 'RequestBooking', '', '2014-01-03 19:03:02');
INSERT INTO `using_log` VALUES (3052, 1, 'RequestBooking', '', '2014-01-03 19:03:37');
INSERT INTO `using_log` VALUES (3053, 1, 'RequestBooking', 'Index', '2014-01-03 19:03:42');
INSERT INTO `using_log` VALUES (3054, 1, 'RequestBooking', 'Index', '2014-01-03 19:03:44');
INSERT INTO `using_log` VALUES (3055, 1, 'RequestBooking', '', '2014-01-03 19:03:56');
INSERT INTO `using_log` VALUES (3056, 1, 'RequestBooking', '', '2014-01-03 19:05:56');
INSERT INTO `using_log` VALUES (3057, 1, 'RequestBooking', '', '2014-01-03 19:05:58');
INSERT INTO `using_log` VALUES (3058, 1, 'RequestBooking', '', '2014-01-03 19:06:40');
INSERT INTO `using_log` VALUES (3059, 1, 'RequestBooking', '', '2014-01-03 19:06:41');
INSERT INTO `using_log` VALUES (3060, 1, 'RequestBooking', '', '2014-01-03 19:06:42');
INSERT INTO `using_log` VALUES (3061, 1, 'RequestBooking', '', '2014-01-03 19:06:43');
INSERT INTO `using_log` VALUES (3062, 1, 'RequestBooking', '', '2014-01-03 19:08:47');
INSERT INTO `using_log` VALUES (3063, 1, 'RequestBooking', 'Index', '2014-01-03 19:08:56');
INSERT INTO `using_log` VALUES (3064, 1, 'RequestBooking', 'Index', '2014-01-03 19:09:33');
INSERT INTO `using_log` VALUES (3065, 1, 'RequestBooking', 'Index', '2014-01-03 19:09:35');
INSERT INTO `using_log` VALUES (3066, 1, 'RequestBooking', '', '2014-01-03 19:09:48');
INSERT INTO `using_log` VALUES (3067, 1, 'RequestBooking', '', '2014-01-03 19:09:52');
INSERT INTO `using_log` VALUES (3068, 1, 'RequestBooking', 'Index', '2014-01-03 19:09:53');
INSERT INTO `using_log` VALUES (3069, 1, 'RequestBooking', 'Index', '2014-01-03 19:09:55');
INSERT INTO `using_log` VALUES (3070, 1, 'RequestBooking', '', '2014-01-03 19:09:56');
INSERT INTO `using_log` VALUES (3071, 1, 'RequestBooking', '', '2014-01-03 19:10:41');
INSERT INTO `using_log` VALUES (3072, 1, 'RequestBooking', '', '2014-02-05 19:10:58');
INSERT INTO `using_log` VALUES (3073, 1, 'RequestBooking', '', '2014-01-03 19:12:07');
INSERT INTO `using_log` VALUES (3074, 1, 'RequestBooking', '', '2014-01-03 19:12:20');
INSERT INTO `using_log` VALUES (3075, 1, 'RequestBooking', '', '2014-01-03 19:12:50');
INSERT INTO `using_log` VALUES (3076, 1, 'RequestBooking', 'Index', '2014-01-03 19:13:02');
INSERT INTO `using_log` VALUES (3077, 1, 'RequestBooking', 'Index', '2014-01-03 19:13:04');
INSERT INTO `using_log` VALUES (3078, 1, 'RequestBooking', 'Index', '2014-01-03 19:13:05');
INSERT INTO `using_log` VALUES (3079, 1, 'RequestBooking', 'Index', '2014-01-03 19:13:08');
INSERT INTO `using_log` VALUES (3080, 1, 'RequestBooking', 'Index', '2014-01-03 19:13:10');
INSERT INTO `using_log` VALUES (3081, 1, 'RequestBooking', '', '2014-01-03 19:13:14');
INSERT INTO `using_log` VALUES (3082, 1, 'RequestBooking', '', '2014-01-03 19:13:16');
INSERT INTO `using_log` VALUES (3083, 1, 'RequestBooking', '', '2014-01-03 19:15:57');
INSERT INTO `using_log` VALUES (3084, 1, 'RequestBooking', 'Index', '2014-01-03 19:15:59');
INSERT INTO `using_log` VALUES (3085, 1, 'RequestBooking', 'Index', '2014-01-03 19:16:00');
INSERT INTO `using_log` VALUES (3086, 1, 'RequestBooking', 'Index', '2014-01-03 19:16:24');
INSERT INTO `using_log` VALUES (3087, 1, 'RequestBooking', 'Index', '2014-01-03 19:16:25');
INSERT INTO `using_log` VALUES (3088, 1, 'RequestBooking', 'Index', '2014-01-03 19:16:26');
INSERT INTO `using_log` VALUES (3089, 1, 'RequestBooking', 'Index', '2014-01-03 19:16:28');
INSERT INTO `using_log` VALUES (3090, 1, 'RequestBooking', 'Approve', '2014-01-03 19:16:28');
INSERT INTO `using_log` VALUES (3091, 1, 'RequestBooking', '', '2014-01-03 19:16:30');
INSERT INTO `using_log` VALUES (3092, 1, 'RequestBooking', '', '2014-01-03 15:57:24');
INSERT INTO `using_log` VALUES (3093, 1, 'RequestBooking', '', '2014-01-03 15:57:28');
INSERT INTO `using_log` VALUES (3094, 1, 'RequestBooking', '', '2014-01-03 16:05:51');
INSERT INTO `using_log` VALUES (3095, 1, 'RequestBooking', '', '2014-01-03 16:05:54');
INSERT INTO `using_log` VALUES (3096, 1, 'RequestBooking', '', '2014-01-03 16:10:12');
INSERT INTO `using_log` VALUES (3097, 1, 'RequestBooking', '', '2014-01-03 16:10:47');
INSERT INTO `using_log` VALUES (3098, 1, 'RequestBooking', '', '2014-01-03 16:10:50');
INSERT INTO `using_log` VALUES (3099, 1, 'RequestBooking', '', '2014-01-03 16:11:02');
INSERT INTO `using_log` VALUES (3100, 1, 'RequestBooking', '', '2014-01-03 16:11:03');
INSERT INTO `using_log` VALUES (3101, 1, 'RequestBooking', '', '2014-01-03 16:11:04');
INSERT INTO `using_log` VALUES (3102, 1, 'RequestBooking', '', '2014-01-03 16:11:05');
INSERT INTO `using_log` VALUES (3103, 1, 'RequestBooking', '', '2014-01-03 16:11:05');
INSERT INTO `using_log` VALUES (3104, 1, 'RequestBooking', '', '2014-01-03 16:11:05');
INSERT INTO `using_log` VALUES (3105, 1, 'RequestBooking', '', '2014-01-03 16:11:06');
INSERT INTO `using_log` VALUES (3106, 1, 'RequestBooking', '', '2014-01-03 16:11:06');
INSERT INTO `using_log` VALUES (3107, 1, 'RequestBooking', '', '2014-01-03 16:11:06');
INSERT INTO `using_log` VALUES (3108, 1, 'RequestBooking', '', '2014-01-03 16:11:06');
INSERT INTO `using_log` VALUES (3109, 1, 'RequestBooking', '', '2014-01-03 16:11:07');
INSERT INTO `using_log` VALUES (3110, 1, 'RequestBooking', '', '2014-01-03 16:11:07');
INSERT INTO `using_log` VALUES (3111, 1, 'RequestBooking', '', '2014-01-03 16:11:07');
INSERT INTO `using_log` VALUES (3112, 1, 'RequestBooking', '', '2014-01-03 16:11:07');
INSERT INTO `using_log` VALUES (3113, 1, 'RequestBooking', '', '2014-01-03 16:11:08');
INSERT INTO `using_log` VALUES (3114, 1, 'RequestBooking', '', '2014-01-03 16:11:08');
INSERT INTO `using_log` VALUES (3115, 1, 'RequestBooking', '', '2014-01-03 16:11:08');
INSERT INTO `using_log` VALUES (3116, 1, 'RequestBooking', '', '2014-01-03 16:11:08');
INSERT INTO `using_log` VALUES (3117, 1, 'RequestBooking', '', '2014-01-03 16:11:09');
INSERT INTO `using_log` VALUES (3118, 1, 'RequestBooking', '', '2014-01-03 16:11:09');
INSERT INTO `using_log` VALUES (3119, 1, 'RequestBooking', '', '2014-01-03 16:11:09');
INSERT INTO `using_log` VALUES (3120, 1, 'RequestBooking', '', '2014-01-03 16:11:09');
INSERT INTO `using_log` VALUES (3121, 1, 'RequestBooking', '', '2014-01-03 16:11:10');
INSERT INTO `using_log` VALUES (3122, 1, 'RequestBooking', '', '2014-01-03 16:11:28');
INSERT INTO `using_log` VALUES (3123, 1, 'RequestBooking', '', '2014-01-03 16:11:36');
INSERT INTO `using_log` VALUES (3124, 1, 'RequestBooking', 'Index', '2014-01-03 16:11:51');
INSERT INTO `using_log` VALUES (3125, 1, 'RequestBooking', 'Index', '2014-01-03 16:11:52');
INSERT INTO `using_log` VALUES (3126, 1, 'RequestBooking', 'Index', '2014-01-03 16:12:01');
INSERT INTO `using_log` VALUES (3127, 1, 'RequestBooking', 'Index', '2014-01-03 16:12:03');
INSERT INTO `using_log` VALUES (3128, 1, 'RequestBooking', '', '2014-01-03 16:12:14');
INSERT INTO `using_log` VALUES (3129, 1, 'RequestBooking', '', '2014-01-03 16:12:34');
INSERT INTO `using_log` VALUES (3130, 1, 'RequestBooking', 'Index', '2014-01-03 16:12:36');
INSERT INTO `using_log` VALUES (3131, 1, 'RequestBooking', 'Index', '2014-01-03 16:12:38');
INSERT INTO `using_log` VALUES (3132, 1, 'RequestBooking', '', '2014-01-03 16:13:38');
INSERT INTO `using_log` VALUES (3133, 1, 'RequestBooking', '', '2014-01-03 10:13:47');
INSERT INTO `using_log` VALUES (3134, 1, 'RequestBooking', '', '2014-01-03 10:15:13');
INSERT INTO `using_log` VALUES (3135, 1, 'RequestBooking', '', '2014-01-03 10:15:15');
INSERT INTO `using_log` VALUES (3136, 1, 'RequestBooking', '', '2014-01-03 10:15:18');
INSERT INTO `using_log` VALUES (3137, 1, 'RequestBooking', 'Index', '2014-01-03 10:15:25');
INSERT INTO `using_log` VALUES (3138, 1, 'RequestBooking', 'Index', '2014-01-03 10:15:29');
INSERT INTO `using_log` VALUES (3139, 1, 'RequestBooking', 'Index', '2014-01-03 10:15:30');
INSERT INTO `using_log` VALUES (3140, 1, 'RequestBooking', '', '2014-01-03 10:15:35');
INSERT INTO `using_log` VALUES (3141, 1, 'RequestBooking', 'Index', '2014-01-03 10:15:39');
INSERT INTO `using_log` VALUES (3142, 1, 'RequestBooking', '', '2014-01-03 10:15:42');
INSERT INTO `using_log` VALUES (3143, 1, 'RequestBorrow', '', '2014-01-03 10:15:52');
INSERT INTO `using_log` VALUES (3144, 1, 'RequestBooking', '', '2014-01-03 10:15:58');
INSERT INTO `using_log` VALUES (3145, 1, 'RequestBorrow', '', '2014-01-03 10:16:02');
INSERT INTO `using_log` VALUES (3146, 1, 'RequestService', '', '2014-01-03 10:16:03');
INSERT INTO `using_log` VALUES (3147, 1, 'Solution', '', '2014-01-03 10:16:18');
INSERT INTO `using_log` VALUES (3148, 35, '', '', '2014-01-04 15:21:19');
INSERT INTO `using_log` VALUES (3149, 35, 'RequestBooking', '', '2014-01-04 15:21:26');
INSERT INTO `using_log` VALUES (3150, 35, 'RequestBooking', '', '2014-01-04 15:21:33');
INSERT INTO `using_log` VALUES (3151, 35, 'RequestBooking', 'Request', '2014-01-04 15:21:37');
INSERT INTO `using_log` VALUES (3152, 35, 'RequestBooking', '', '2014-01-04 15:21:38');
INSERT INTO `using_log` VALUES (3153, 35, '', '', '2014-01-04 15:21:49');
INSERT INTO `using_log` VALUES (3154, NULL, 'management', 'login', '2014-01-04 15:21:51');
INSERT INTO `using_log` VALUES (3155, 1, 'RequestBooking', '', '2014-01-04 15:21:53');
INSERT INTO `using_log` VALUES (3156, 1, 'RequestBooking', '', '2014-01-04 15:21:56');
INSERT INTO `using_log` VALUES (3157, NULL, '', '', '2014-01-04 15:22:38');
INSERT INTO `using_log` VALUES (3158, 1, 'RequestBooking', '', '2014-01-04 15:22:43');
INSERT INTO `using_log` VALUES (3159, NULL, 'management', 'login', '2014-01-04 15:22:49');
INSERT INTO `using_log` VALUES (3160, 2, 'RequestBooking', '', '2014-01-04 15:22:52');
INSERT INTO `using_log` VALUES (3161, NULL, 'management', 'login', '2014-01-04 15:27:13');
INSERT INTO `using_log` VALUES (3162, 6, 'RequestBooking', '', '2014-01-04 15:27:17');
INSERT INTO `using_log` VALUES (3163, 6, 'RequestBooking', 'Request', '2014-01-04 15:27:24');
INSERT INTO `using_log` VALUES (3164, 6, 'RequestBooking', 'View', '2014-01-04 15:28:00');
INSERT INTO `using_log` VALUES (3165, 6, 'RequestBooking', '', '2014-01-04 15:28:02');
INSERT INTO `using_log` VALUES (3166, 6, 'RequestBooking', '', '2014-01-04 15:28:25');
INSERT INTO `using_log` VALUES (3167, 1, 'RequestBooking', '', '2014-01-04 15:28:35');
INSERT INTO `using_log` VALUES (3168, 1, 'RequestBooking', 'Approve', '2014-01-04 15:28:36');
INSERT INTO `using_log` VALUES (3169, 6, 'RequestBooking', '', '2014-01-06 07:29:04');
INSERT INTO `using_log` VALUES (3170, 1, 'RequestBooking', 'Approve', '2014-01-06 07:29:10');
INSERT INTO `using_log` VALUES (3171, 1, 'RequestBooking', 'Approve', '2014-01-06 07:29:11');
INSERT INTO `using_log` VALUES (3172, 1, 'RequestBooking', '', '2014-01-06 07:29:13');
INSERT INTO `using_log` VALUES (3173, 1, 'Admin', '', '2014-01-06 07:30:32');
INSERT INTO `using_log` VALUES (3174, 1, 'Semester', '', '2014-01-06 07:30:35');
INSERT INTO `using_log` VALUES (3175, 1, 'semester', 'create', '2014-01-06 07:30:41');
INSERT INTO `using_log` VALUES (3176, 1, 'Semester', '', '2014-01-06 07:30:44');
INSERT INTO `using_log` VALUES (3177, 1, 'semester', 'create', '2014-01-06 07:30:58');
INSERT INTO `using_log` VALUES (3178, 1, 'Semester', '', '2014-01-06 07:31:13');
INSERT INTO `using_log` VALUES (3179, 1, 'semester', 'update', '2014-01-06 07:31:16');
INSERT INTO `using_log` VALUES (3180, 1, 'Semester', '', '2014-01-06 07:31:21');
INSERT INTO `using_log` VALUES (3181, 1, 'semester', 'update', '2014-01-06 07:31:26');
INSERT INTO `using_log` VALUES (3182, 1, 'Semester', '', '2014-01-06 07:31:28');
INSERT INTO `using_log` VALUES (3183, 1, 'semester', 'update', '2014-01-06 07:31:38');
INSERT INTO `using_log` VALUES (3184, 1, 'Semester', '', '2014-01-06 07:31:41');
INSERT INTO `using_log` VALUES (3185, 6, 'RequestBooking', '', '2014-01-06 07:31:51');
INSERT INTO `using_log` VALUES (3186, 6, 'RequestBooking', 'Request', '2014-01-06 07:31:52');
INSERT INTO `using_log` VALUES (3187, 6, 'RequestBooking', 'Request', '2014-01-06 07:32:30');
INSERT INTO `using_log` VALUES (3188, 6, 'RequestBooking', 'CheckStatus', '2014-01-06 07:32:36');
INSERT INTO `using_log` VALUES (3189, 6, 'RequestBooking', 'Request', '2014-01-06 07:33:01');
INSERT INTO `using_log` VALUES (3190, 6, 'RequestBooking', 'Request', '2014-01-06 07:33:39');
INSERT INTO `using_log` VALUES (3191, 6, 'RequestBooking', 'Request', '2014-01-06 07:35:19');
INSERT INTO `using_log` VALUES (3192, 6, 'RequestBooking', 'View', '2014-01-06 07:35:41');
INSERT INTO `using_log` VALUES (3193, 6, 'RequestBooking', '', '2014-01-06 07:35:44');
INSERT INTO `using_log` VALUES (3194, 6, 'RequestBooking', 'CheckStatus', '2014-01-06 07:36:01');
INSERT INTO `using_log` VALUES (3195, 6, 'RequestBooking', '', '2014-01-06 07:36:26');
INSERT INTO `using_log` VALUES (3196, 6, 'RequestBooking', '', '2014-01-13 07:37:08');
INSERT INTO `using_log` VALUES (3197, 6, 'RequestBooking', 'Request', '2014-01-13 07:37:19');
INSERT INTO `using_log` VALUES (3198, 1, 'RequestBooking', '', '2014-01-13 07:37:29');
INSERT INTO `using_log` VALUES (3199, 6, 'RequestBooking', 'Request', '2014-01-13 07:37:47');
INSERT INTO `using_log` VALUES (3200, 6, 'RequestBooking', 'View', '2014-01-13 07:46:20');
INSERT INTO `using_log` VALUES (3201, 6, 'RequestBooking', 'View', '2014-01-13 07:46:25');
INSERT INTO `using_log` VALUES (3202, 6, 'RequestBooking', '', '2014-01-13 07:46:34');
INSERT INTO `using_log` VALUES (3203, 6, 'RequestBooking', '', '2014-01-21 07:46:52');
INSERT INTO `using_log` VALUES (3204, 6, 'requestBooking', 'view', '2014-01-21 07:46:56');
INSERT INTO `using_log` VALUES (3205, 6, 'RequestBooking', '', '2014-01-21 07:46:58');
INSERT INTO `using_log` VALUES (3206, 6, 'RequestBooking', '', '2014-01-21 07:47:01');
INSERT INTO `using_log` VALUES (3207, 6, 'RequestBooking', '', '2014-01-21 07:47:05');
INSERT INTO `using_log` VALUES (3208, 6, 'RequestBooking', 'Request', '2014-01-21 07:47:08');
INSERT INTO `using_log` VALUES (3209, 6, 'RequestBooking', '', '2014-01-21 07:47:09');
INSERT INTO `using_log` VALUES (3210, 1, 'RequestBooking', '', '2014-01-21 07:48:10');
INSERT INTO `using_log` VALUES (3211, 1, 'RequestBooking', 'Approve', '2014-01-21 07:48:14');
INSERT INTO `using_log` VALUES (3212, 1, 'RequestBooking', 'Approve', '2014-01-21 07:48:26');
INSERT INTO `using_log` VALUES (3213, 6, 'RequestBooking', '', '2014-01-21 07:48:32');
INSERT INTO `using_log` VALUES (3214, 1, 'RequestBooking', '', '2014-01-21 07:49:55');
INSERT INTO `using_log` VALUES (3215, 1, 'requestBooking', 'update', '2014-01-21 07:50:02');
INSERT INTO `using_log` VALUES (3216, 6, 'RequestBooking', 'Request', '2014-01-21 07:51:00');
INSERT INTO `using_log` VALUES (3217, 6, 'RequestBooking', 'Request', '2014-01-21 07:51:29');
INSERT INTO `using_log` VALUES (3218, 6, 'RequestBooking', 'View', '2014-01-21 07:52:12');
INSERT INTO `using_log` VALUES (3219, 6, 'RequestBooking', '', '2014-01-21 07:52:15');
INSERT INTO `using_log` VALUES (3220, 1, 'RequestBooking', '', '2014-01-21 07:52:41');
INSERT INTO `using_log` VALUES (3221, 1, 'RequestBooking', '', '2014-01-22 07:53:08');
INSERT INTO `using_log` VALUES (3222, 1, 'RequestBooking', '', '2014-01-23 07:53:24');
INSERT INTO `using_log` VALUES (3223, 1, 'requestBooking', 'update', '2014-01-23 07:53:31');
INSERT INTO `using_log` VALUES (3224, 1, 'RequestBooking', 'Approve', '2014-01-23 07:53:34');
INSERT INTO `using_log` VALUES (3225, 1, 'RequestBooking', 'Approve', '2014-01-23 07:53:43');
INSERT INTO `using_log` VALUES (3226, 1, 'RequestBooking', '', '2014-01-23 07:53:45');
INSERT INTO `using_log` VALUES (3227, 6, 'RequestBooking', '', '2014-01-23 07:53:54');
INSERT INTO `using_log` VALUES (3228, 6, 'RequestBorrow', '', '2014-01-23 08:14:18');
INSERT INTO `using_log` VALUES (3229, 6, 'RequestBorrow', 'CheckStatus', '2014-01-23 08:14:30');
INSERT INTO `using_log` VALUES (3230, 6, 'RequestBorrow', 'request', '2014-01-23 08:14:40');
INSERT INTO `using_log` VALUES (3231, 6, 'RequestBorrow', 'Request', '2014-01-23 08:14:44');
INSERT INTO `using_log` VALUES (3232, 6, 'RequestBorrow', 'View', '2014-01-23 08:15:52');
INSERT INTO `using_log` VALUES (3233, 6, 'RequestBorrow', '', '2014-01-23 08:16:32');
INSERT INTO `using_log` VALUES (3234, 1, 'RequestBorrow', '', '2014-01-23 08:16:57');
INSERT INTO `using_log` VALUES (3235, 1, 'RequestBorrow', 'ListApprove', '2014-01-23 08:17:24');
INSERT INTO `using_log` VALUES (3236, 1, 'RequestBorrow', 'ListApprove', '2014-01-23 08:17:27');
INSERT INTO `using_log` VALUES (3237, 1, 'RequestBorrow', '', '2014-01-23 08:17:30');
INSERT INTO `using_log` VALUES (3238, 1, 'RequestBorrow', 'ListApprove', '2014-01-23 08:20:56');
INSERT INTO `using_log` VALUES (3239, 1, 'RequestBorrow', '', '2014-01-23 08:21:00');
INSERT INTO `using_log` VALUES (3240, 1, 'requestBorrow', 'update', '2014-01-23 08:21:02');
INSERT INTO `using_log` VALUES (3241, 1, 'RequestBorrow', '', '2014-01-23 08:21:13');
INSERT INTO `using_log` VALUES (3242, 6, 'RequestBorrow', '', '2014-01-23 08:21:23');
INSERT INTO `using_log` VALUES (3243, 6, 'RequestBorrow', 'Request', '2014-01-23 08:21:58');
INSERT INTO `using_log` VALUES (3244, 6, 'RequestBorrow', 'View', '2014-01-23 08:22:47');
INSERT INTO `using_log` VALUES (3245, 6, 'RequestBorrow', '', '2014-01-23 08:23:03');
INSERT INTO `using_log` VALUES (3246, 1, 'RequestBorrow', '', '2014-01-23 08:23:09');
INSERT INTO `using_log` VALUES (3247, 1, 'RequestBorrow', 'ListApprove', '2014-01-23 08:24:42');
INSERT INTO `using_log` VALUES (3248, 1, 'RequestBorrow', 'ListApprove', '2014-01-23 08:25:03');
INSERT INTO `using_log` VALUES (3249, 6, 'RequestBorrow', '', '2014-01-23 08:25:53');
INSERT INTO `using_log` VALUES (3250, 1, 'RequestBorrow', 'ListApprove', '2014-01-23 08:26:00');
INSERT INTO `using_log` VALUES (3251, 1, 'RequestBorrow', '', '2014-01-23 08:26:01');
INSERT INTO `using_log` VALUES (3252, 1, 'requestBorrow', 'update', '2014-01-23 08:26:04');
INSERT INTO `using_log` VALUES (3253, 1, 'RequestBooking', '', '2014-01-23 08:26:08');
INSERT INTO `using_log` VALUES (3254, 6, 'RequestService', '', '2014-01-23 08:35:42');
INSERT INTO `using_log` VALUES (3255, 6, 'RequestService', 'Request', '2014-01-23 08:35:47');
INSERT INTO `using_log` VALUES (3256, 6, 'RequestService', 'View', '2014-01-23 08:36:11');
INSERT INTO `using_log` VALUES (3257, 6, 'RequestService', '', '2014-01-23 08:36:13');
INSERT INTO `using_log` VALUES (3258, 6, 'RequestService', 'Index', '2014-01-23 08:36:33');
INSERT INTO `using_log` VALUES (3259, 6, 'RequestService', 'Index', '2014-01-23 08:36:33');
INSERT INTO `using_log` VALUES (3260, 1, 'RequestService', '', '2014-01-23 08:36:56');
INSERT INTO `using_log` VALUES (3261, 1, 'RequestService', 'Index', '2014-01-23 08:37:00');
INSERT INTO `using_log` VALUES (3262, 1, 'RequestService', 'Index', '2014-01-23 08:37:00');
INSERT INTO `using_log` VALUES (3263, 1, 'requestService', 'update', '2014-01-23 08:37:02');
INSERT INTO `using_log` VALUES (3264, 1, 'RequestService', 'Approve', '2014-01-23 08:37:05');
INSERT INTO `using_log` VALUES (3265, 1, 'RequestService', '', '2014-01-23 08:37:20');
INSERT INTO `using_log` VALUES (3266, 6, 'RequestService', '', '2014-01-23 08:37:24');
INSERT INTO `using_log` VALUES (3267, 6, 'RequestService', 'Index', '2014-01-23 08:37:56');
INSERT INTO `using_log` VALUES (3268, 6, 'RequestService', 'Index', '2014-01-23 08:37:57');
INSERT INTO `using_log` VALUES (3269, 6, 'RequestService', 'Index', '2014-01-23 08:37:58');
INSERT INTO `using_log` VALUES (3270, 6, 'RequestService', 'Index', '2014-01-23 08:37:58');
INSERT INTO `using_log` VALUES (3271, 6, 'RequestService', '', '2014-01-23 08:39:01');
INSERT INTO `using_log` VALUES (3272, 6, 'RequestService', '', '2014-01-23 08:39:42');
INSERT INTO `using_log` VALUES (3273, 6, 'RequestService', '', '2014-01-23 08:39:53');
INSERT INTO `using_log` VALUES (3274, 6, 'RequestService', '', '2014-01-23 08:41:25');
INSERT INTO `using_log` VALUES (3275, 6, 'RequestService', '', '2014-01-23 08:41:47');
INSERT INTO `using_log` VALUES (3276, 1, 'RequestService', '', '2014-01-23 08:42:02');
INSERT INTO `using_log` VALUES (3277, 1, 'requestService', 'update', '2014-01-23 08:42:05');
INSERT INTO `using_log` VALUES (3278, 6, 'RequestService', '', '2014-01-23 08:42:30');
INSERT INTO `using_log` VALUES (3279, 6, 'RequestService', 'Request', '2014-01-23 08:42:33');
INSERT INTO `using_log` VALUES (3280, 6, 'RequestService', 'View', '2014-01-23 08:42:47');
INSERT INTO `using_log` VALUES (3281, 6, 'RequestService', '', '2014-01-23 08:42:49');
INSERT INTO `using_log` VALUES (3282, 1, 'requestService', 'update', '2014-01-23 08:42:56');
INSERT INTO `using_log` VALUES (3283, 1, 'RequestService', 'Approve', '2014-01-23 08:42:57');
INSERT INTO `using_log` VALUES (3284, 1, 'RequestService', '', '2014-01-23 08:42:59');
INSERT INTO `using_log` VALUES (3285, 1, 'requestService', 'update', '2014-01-23 08:43:08');
INSERT INTO `using_log` VALUES (3286, 1, 'RequestService', '', '2014-01-23 08:43:25');
INSERT INTO `using_log` VALUES (3287, 6, 'RequestService', 'Request', '2014-01-23 08:45:44');
INSERT INTO `using_log` VALUES (3288, 6, 'RequestService', 'View', '2014-01-23 08:45:57');
INSERT INTO `using_log` VALUES (3289, 6, 'RequestService', '', '2014-01-23 08:46:00');
INSERT INTO `using_log` VALUES (3290, 1, 'RequestService', '', '2014-01-23 08:46:09');
INSERT INTO `using_log` VALUES (3291, 1, 'RequestService', 'Approve', '2014-01-23 08:46:10');
INSERT INTO `using_log` VALUES (3292, 1, 'RequestService', 'Approve', '2014-01-23 08:46:31');
INSERT INTO `using_log` VALUES (3293, 1, 'RequestService', '', '2014-01-23 08:46:43');
INSERT INTO `using_log` VALUES (3294, 1, 'requestService', 'update', '2014-01-23 08:46:50');
INSERT INTO `using_log` VALUES (3295, 1, 'RequestService', '', '2014-01-23 08:46:53');
INSERT INTO `using_log` VALUES (3296, 6, 'RequestService', 'Request', '2014-01-23 08:51:50');
INSERT INTO `using_log` VALUES (3297, 6, 'RequestService', 'View', '2014-01-23 08:52:08');
INSERT INTO `using_log` VALUES (3298, 6, 'RequestService', 'View', '2014-01-23 08:52:17');
INSERT INTO `using_log` VALUES (3299, 6, 'RequestService', 'View', '2014-01-23 08:52:27');
INSERT INTO `using_log` VALUES (3300, 6, 'RequestService', '', '2014-01-23 08:52:28');
INSERT INTO `using_log` VALUES (3301, 6, 'RequestService', 'View', '2014-01-23 08:52:31');
INSERT INTO `using_log` VALUES (3302, 6, 'RequestService', '', '2014-01-23 08:53:32');
INSERT INTO `using_log` VALUES (3303, 1, 'RequestService', '', '2014-01-23 08:53:39');
INSERT INTO `using_log` VALUES (3304, 1, 'RequestService', 'Approve', '2014-01-23 08:53:43');
INSERT INTO `using_log` VALUES (3305, 1, 'RequestService', 'Approve', '2014-01-23 08:53:50');
INSERT INTO `using_log` VALUES (3306, 1, 'RequestService', 'Approve', '2014-01-23 08:58:14');
INSERT INTO `using_log` VALUES (3307, 1, 'RequestService', '', '2014-01-23 08:58:16');
INSERT INTO `using_log` VALUES (3308, 1, 'requestService', 'update', '2014-01-23 08:58:19');
INSERT INTO `using_log` VALUES (3309, 1, 'RequestBooking', '', '2014-01-23 08:58:38');
INSERT INTO `using_log` VALUES (3310, 1, 'requestBooking', 'update', '2014-01-23 08:58:40');
INSERT INTO `using_log` VALUES (3311, 6, 'RequestService', '', '2014-01-23 09:07:06');
INSERT INTO `using_log` VALUES (3312, 6, 'RequestService', '', '2014-01-23 09:09:24');
INSERT INTO `using_log` VALUES (3313, 6, 'RequestService', '', '2014-01-23 09:09:26');
INSERT INTO `using_log` VALUES (3314, 6, 'RequestService', '', '2014-01-23 09:10:35');
INSERT INTO `using_log` VALUES (3315, 6, 'RequestService', '', '2014-01-23 09:11:38');
INSERT INTO `using_log` VALUES (3316, 6, 'RequestService', '', '2014-01-23 09:11:49');
INSERT INTO `using_log` VALUES (3317, 6, 'RequestService', 'Request', '2014-01-23 09:12:32');
INSERT INTO `using_log` VALUES (3318, 6, 'RequestService', 'View', '2014-01-23 09:12:47');
INSERT INTO `using_log` VALUES (3319, 6, 'RequestService', '', '2014-01-23 09:12:50');
INSERT INTO `using_log` VALUES (3320, 1, 'RequestBooking', '', '2014-01-23 09:12:57');
INSERT INTO `using_log` VALUES (3321, 1, 'RequestService', '', '2014-01-23 09:12:59');
INSERT INTO `using_log` VALUES (3322, 1, 'RequestService', 'Approve', '2014-01-23 09:13:01');
INSERT INTO `using_log` VALUES (3323, 1, 'RequestService', 'Approve', '2014-01-23 09:13:08');
INSERT INTO `using_log` VALUES (3324, 1, 'RequestService', '', '2014-01-23 09:13:09');
INSERT INTO `using_log` VALUES (3325, 6, 'RequestService', '', '2014-01-23 09:13:17');
INSERT INTO `using_log` VALUES (3326, 1, 'requestService', 'update', '2014-01-23 09:13:31');
INSERT INTO `using_log` VALUES (3327, 1, 'RequestService', '', '2014-01-23 09:13:36');
INSERT INTO `using_log` VALUES (3328, 6, 'RequestService', '', '2014-01-23 09:13:47');
INSERT INTO `using_log` VALUES (3329, 6, 'RequestService', 'Request', '2014-01-23 09:17:03');
INSERT INTO `using_log` VALUES (3330, 6, 'RequestService', 'View', '2014-01-23 09:17:21');
INSERT INTO `using_log` VALUES (3331, 6, 'RequestService', '', '2014-01-23 09:17:23');
INSERT INTO `using_log` VALUES (3332, 1, 'RequestService', '', '2014-01-23 09:17:39');
INSERT INTO `using_log` VALUES (3333, 1, 'RequestService', 'Approve', '2014-01-23 09:17:47');
INSERT INTO `using_log` VALUES (3334, 1, 'RequestService', 'Approve', '2014-01-23 09:17:53');
INSERT INTO `using_log` VALUES (3335, 1, 'RequestBorrow', '', '2014-01-23 09:18:13');
INSERT INTO `using_log` VALUES (3336, 1, 'RequestService', '', '2014-01-23 09:18:14');
INSERT INTO `using_log` VALUES (3337, 6, 'RequestService', '', '2014-01-23 09:18:19');
INSERT INTO `using_log` VALUES (3338, 6, 'RequestService', 'Request', '2014-01-23 09:18:31');
INSERT INTO `using_log` VALUES (3339, 6, 'RequestService', 'View', '2014-01-23 09:18:48');
INSERT INTO `using_log` VALUES (3340, 6, 'RequestService', '', '2014-01-23 09:18:49');
INSERT INTO `using_log` VALUES (3341, 1, 'RequestService', '', '2014-01-23 09:18:53');
INSERT INTO `using_log` VALUES (3342, 1, 'RequestService', 'Approve', '2014-01-23 09:18:56');
INSERT INTO `using_log` VALUES (3343, 1, 'RequestService', 'Approve', '2014-01-23 09:19:02');
INSERT INTO `using_log` VALUES (3344, 1, 'RequestService', '', '2014-01-23 09:19:04');
INSERT INTO `using_log` VALUES (3345, 6, 'RequestService', '', '2014-01-23 09:19:11');
INSERT INTO `using_log` VALUES (3346, 1, 'requestService', 'update', '2014-01-23 09:19:15');
INSERT INTO `using_log` VALUES (3347, 1, 'RequestService', '', '2014-01-23 09:19:19');
INSERT INTO `using_log` VALUES (3348, 6, 'RequestService', '', '2014-01-23 09:19:25');
INSERT INTO `using_log` VALUES (3349, 1, 'requestService', 'update', '2014-01-23 09:19:54');
INSERT INTO `using_log` VALUES (3350, 1, 'RequestService', '', '2014-01-23 09:19:57');
INSERT INTO `using_log` VALUES (3351, 1, 'RequestService', '', '2014-01-23 09:20:49');
INSERT INTO `using_log` VALUES (3352, 1, 'requestService', 'update', '2014-01-23 09:20:50');
INSERT INTO `using_log` VALUES (3353, 1, 'RequestService', '', '2014-01-23 09:20:53');
INSERT INTO `using_log` VALUES (3354, 1, 'requestService', 'update', '2014-01-23 09:21:29');
INSERT INTO `using_log` VALUES (3355, 1, 'RequestService', '', '2014-01-23 09:21:32');
INSERT INTO `using_log` VALUES (3356, 1, 'RequestService', '', '2014-01-23 09:21:57');
INSERT INTO `using_log` VALUES (3357, 1, 'requestService', 'update', '2014-01-23 09:22:00');
INSERT INTO `using_log` VALUES (3358, 1, 'requestService', 'update', '2014-01-23 09:22:02');
INSERT INTO `using_log` VALUES (3359, 1, 'requestService', 'update', '2014-01-23 09:23:58');
INSERT INTO `using_log` VALUES (3360, 1, 'requestService', 'update', '2014-01-23 09:24:31');
INSERT INTO `using_log` VALUES (3361, 1, 'requestService', 'update', '2014-01-23 09:24:47');
INSERT INTO `using_log` VALUES (3362, 1, 'requestService', 'update', '2014-01-23 09:25:22');
INSERT INTO `using_log` VALUES (3363, 1, 'RequestService', '', '2014-01-23 09:25:30');
INSERT INTO `using_log` VALUES (3364, 6, 'RequestService', '', '2014-01-23 09:26:17');
INSERT INTO `using_log` VALUES (3365, 6, 'RequestService', 'Request', '2014-01-23 09:34:15');
INSERT INTO `using_log` VALUES (3366, 6, 'RequestService', 'View', '2014-01-23 09:34:28');
INSERT INTO `using_log` VALUES (3367, 6, 'RequestService', 'View', '2014-01-23 09:34:34');
INSERT INTO `using_log` VALUES (3368, 6, 'RequestService', 'View', '2014-01-23 09:35:28');
INSERT INTO `using_log` VALUES (3369, 6, 'RequestService', 'View', '2014-01-23 09:35:49');
INSERT INTO `using_log` VALUES (3370, 6, 'RequestService', '', '2014-01-23 09:35:50');
INSERT INTO `using_log` VALUES (3371, 1, 'RequestService', '', '2014-01-23 09:35:56');
INSERT INTO `using_log` VALUES (3372, 1, 'requestService', 'update', '2014-01-23 09:35:58');
INSERT INTO `using_log` VALUES (3373, 1, 'RequestService', '', '2014-01-23 09:36:00');
INSERT INTO `using_log` VALUES (3374, 1, 'RequestService', 'Request', '2014-01-23 09:36:02');
INSERT INTO `using_log` VALUES (3375, 1, 'RequestService', '', '2014-01-23 09:36:05');
INSERT INTO `using_log` VALUES (3376, 1, 'Admin', '', '2014-01-04 17:39:49');
INSERT INTO `using_log` VALUES (3377, 1, 'Admin', '', '2014-01-04 17:48:44');
INSERT INTO `using_log` VALUES (3378, 1, 'Admin', '', '2014-01-04 17:51:05');
INSERT INTO `using_log` VALUES (3379, 1, 'Admin', '', '2014-01-04 17:51:07');
INSERT INTO `using_log` VALUES (3380, 1, 'Admin', '', '2014-01-04 17:52:36');
INSERT INTO `using_log` VALUES (3381, 1, 'Admin', '', '2014-01-04 17:52:38');
INSERT INTO `using_log` VALUES (3382, 1, 'Semester', '', '2014-01-04 17:52:40');
INSERT INTO `using_log` VALUES (3383, 1, 'Semester', '', '2014-01-04 18:05:16');
INSERT INTO `using_log` VALUES (3384, 1, 'Semester', '', '2014-01-04 18:07:10');
INSERT INTO `using_log` VALUES (3385, 1, 'Approver', '', '2014-01-04 18:08:10');
INSERT INTO `using_log` VALUES (3386, 1, 'Approver', '', '2014-01-04 18:08:27');
INSERT INTO `using_log` VALUES (3387, 1, 'Approver', '', '2014-01-04 18:08:39');
INSERT INTO `using_log` VALUES (3388, 1, 'approver', 'update', '2014-01-04 18:08:41');
INSERT INTO `using_log` VALUES (3389, 1, 'approver', 'update', '2014-01-04 18:09:00');
INSERT INTO `using_log` VALUES (3390, 1, 'Approver', '', '2014-01-04 18:09:03');
INSERT INTO `using_log` VALUES (3391, 1, 'approver', 'update', '2014-01-04 18:09:04');
INSERT INTO `using_log` VALUES (3392, 1, 'Approver', '', '2014-01-04 18:09:07');
INSERT INTO `using_log` VALUES (3393, 1, 'approver', 'update', '2014-01-04 18:09:14');
INSERT INTO `using_log` VALUES (3394, 1, 'Approver', '', '2014-01-04 18:10:06');
INSERT INTO `using_log` VALUES (3395, 1, 'approver', 'update', '2014-01-04 18:10:08');
INSERT INTO `using_log` VALUES (3396, 1, 'Approver', '', '2014-01-04 18:10:10');
INSERT INTO `using_log` VALUES (3397, 1, 'Approver', '', '2014-01-04 18:10:28');
INSERT INTO `using_log` VALUES (3398, 1, 'approver', 'update', '2014-01-04 18:10:30');
INSERT INTO `using_log` VALUES (3399, 1, 'Approver', '', '2014-01-04 18:11:55');
INSERT INTO `using_log` VALUES (3400, 1, 'approver', 'update', '2014-01-04 18:11:57');
INSERT INTO `using_log` VALUES (3401, 1, 'Approver', '', '2014-01-04 18:11:59');
INSERT INTO `using_log` VALUES (3402, 1, 'approver', 'update', '2014-01-04 18:12:01');
INSERT INTO `using_log` VALUES (3403, 1, 'Approver', '', '2014-01-04 18:12:04');
INSERT INTO `using_log` VALUES (3404, 1, 'RequestBooking', '', '2014-01-04 18:16:56');
INSERT INTO `using_log` VALUES (3405, 6, 'RequestBooking', '', '2014-01-04 18:18:30');
INSERT INTO `using_log` VALUES (3406, 6, 'RequestBooking', 'Index', '2014-01-04 18:18:38');
INSERT INTO `using_log` VALUES (3407, 6, 'RequestBooking', 'Request', '2014-01-04 18:19:14');
INSERT INTO `using_log` VALUES (3408, 1, 'Admin', '', '2014-01-04 18:19:34');
INSERT INTO `using_log` VALUES (3409, 1, 'Semester', '', '2014-01-04 18:19:37');
INSERT INTO `using_log` VALUES (3410, 6, 'RequestBooking', 'View', '2014-01-04 18:20:17');
INSERT INTO `using_log` VALUES (3411, 6, 'RequestBooking', '', '2014-01-04 18:20:48');
INSERT INTO `using_log` VALUES (3412, 6, 'RequestBooking', '', '2014-01-04 18:36:19');
INSERT INTO `using_log` VALUES (3413, 6, 'RequestBooking', '', '2014-01-04 18:37:15');
INSERT INTO `using_log` VALUES (3414, 6, 'RequestBooking', '', '2014-01-04 18:37:16');
INSERT INTO `using_log` VALUES (3415, 6, 'RequestBorrow', '', '2014-01-04 18:37:17');
INSERT INTO `using_log` VALUES (3416, 6, 'RequestBooking', '', '2014-01-04 18:37:18');
INSERT INTO `using_log` VALUES (3417, 1, 'RequestBooking', '', '2014-01-04 18:38:03');
INSERT INTO `using_log` VALUES (3418, 6, 'RequestBooking', '', '2014-01-04 18:38:07');
INSERT INTO `using_log` VALUES (3419, 6, 'RequestBooking', '', '2014-01-04 18:43:08');
INSERT INTO `using_log` VALUES (3420, 1, 'RequestBooking', '', '2014-01-04 18:44:44');
INSERT INTO `using_log` VALUES (3421, 1, 'RequestBooking', '', '2014-01-04 18:44:59');
INSERT INTO `using_log` VALUES (3422, 1, 'RequestBooking', '', '2014-01-04 18:45:01');
INSERT INTO `using_log` VALUES (3423, 1, 'RequestBooking', '', '2014-01-04 18:45:02');
INSERT INTO `using_log` VALUES (3424, 1, 'RequestBooking', '', '2014-01-04 18:45:02');
INSERT INTO `using_log` VALUES (3425, 1, 'RequestBooking', '', '2014-01-04 18:45:02');
INSERT INTO `using_log` VALUES (3426, 1, 'RequestBooking', '', '2014-01-04 18:45:03');
INSERT INTO `using_log` VALUES (3427, 1, 'RequestBooking', '', '2014-01-04 18:45:04');
INSERT INTO `using_log` VALUES (3428, 1, 'RequestBooking', '', '2014-01-04 18:45:05');
INSERT INTO `using_log` VALUES (3429, 1, 'RequestBooking', '', '2014-01-04 18:45:53');
INSERT INTO `using_log` VALUES (3430, 6, 'RequestBooking', '', '2014-01-04 18:46:38');
INSERT INTO `using_log` VALUES (3431, 6, 'RequestBooking', 'Request', '2014-01-04 18:46:40');
INSERT INTO `using_log` VALUES (3432, 6, 'RequestBooking', 'View', '2014-01-04 18:47:03');
INSERT INTO `using_log` VALUES (3433, 6, 'RequestBooking', 'View', '2014-01-04 18:47:07');
INSERT INTO `using_log` VALUES (3434, 6, 'RequestBooking', '', '2014-01-04 18:47:09');
INSERT INTO `using_log` VALUES (3435, 6, 'RequestBooking', '', '2014-01-04 18:47:44');
INSERT INTO `using_log` VALUES (3436, 6, 'RequestBooking', 'Request', '2014-01-04 18:48:06');
INSERT INTO `using_log` VALUES (3437, 6, 'RequestBooking', 'View', '2014-01-04 18:48:38');
INSERT INTO `using_log` VALUES (3438, 6, 'RequestBooking', '', '2014-01-04 18:48:40');
INSERT INTO `using_log` VALUES (3439, 6, 'RequestBooking', '', '2014-01-04 18:49:45');
INSERT INTO `using_log` VALUES (3440, 6, 'RequestBooking', '', '2014-01-04 18:49:47');
INSERT INTO `using_log` VALUES (3441, 6, 'RequestBooking', 'Index', '2014-01-04 18:49:54');
INSERT INTO `using_log` VALUES (3442, 6, 'RequestBooking', 'Index', '2014-01-04 18:49:57');
INSERT INTO `using_log` VALUES (3443, 6, 'RequestBooking', '', '2014-01-04 18:50:31');
INSERT INTO `using_log` VALUES (3444, 1, 'RequestBooking', '', '2014-01-04 18:50:47');
INSERT INTO `using_log` VALUES (3445, 1, 'requestBooking', 'update', '2014-01-04 18:51:00');
INSERT INTO `using_log` VALUES (3446, 1, 'RequestBooking', '', '2014-01-04 18:51:10');
INSERT INTO `using_log` VALUES (3447, 6, 'RequestBooking', '', '2014-01-04 18:51:20');
INSERT INTO `using_log` VALUES (3448, 6, 'RequestBooking', '', '2014-01-27 06:51:52');
INSERT INTO `using_log` VALUES (3449, 6, 'RequestBooking', '', '2014-01-04 18:52:39');

-- 
-- Constraints for dumped tables
-- 

-- 
-- Constraints for table `enumeration`
-- 
ALTER TABLE `enumeration`
  ADD CONSTRAINT `enumeration_ibfk_1` FOREIGN KEY (`enumeration_type_code`) REFERENCES `enumeration_type` (`code`);

-- 
-- Constraints for table `equipment`
-- 
ALTER TABLE `equipment`
  ADD CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`equipment_type_id`) REFERENCES `equipment_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `equipment_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`);

-- 
-- Constraints for table `period`
-- 
ALTER TABLE `period`
  ADD CONSTRAINT `period_ibfk_2` FOREIGN KEY (`period_group_id`) REFERENCES `period_group` (`id`),
  ADD CONSTRAINT `period_ibfk_3` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `period_group`
-- 
ALTER TABLE `period_group`
  ADD CONSTRAINT `period_group_ibfk_1` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `permission`
-- 
ALTER TABLE `permission`
  ADD CONSTRAINT `permission_ibfk_1` FOREIGN KEY (`permission_group_id`) REFERENCES `permission_group` (`id`);

-- 
-- Constraints for table `request_booking`
-- 
ALTER TABLE `request_booking`
  ADD CONSTRAINT `request_booking_ibfk_30` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_31` FOREIGN KEY (`request_booking_type_id`) REFERENCES `request_booking_type` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_32` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_33` FOREIGN KEY (`semester_id`) REFERENCES `semester` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_34` FOREIGN KEY (`period_start`) REFERENCES `period` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_35` FOREIGN KEY (`period_end`) REFERENCES `period` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_36` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `request_booking_activity_detail`
-- 
ALTER TABLE `request_booking_activity_detail`
  ADD CONSTRAINT `request_booking_activity_detail_ibfk_10` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`),
  ADD CONSTRAINT `request_booking_activity_detail_ibfk_11` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`),
  ADD CONSTRAINT `request_booking_activity_detail_ibfk_9` FOREIGN KEY (`id`) REFERENCES `request_booking` (`id`) ON DELETE CASCADE;

-- 
-- Constraints for table `request_booking_activity_present_type`
-- 
ALTER TABLE `request_booking_activity_present_type`
  ADD CONSTRAINT `request_booking_activity_present_type_ibfk_3` FOREIGN KEY (`request_booking_activity_id`) REFERENCES `request_booking_activity_detail` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_booking_activity_present_type_ibfk_4` FOREIGN KEY (`present_type_id`) REFERENCES `present_type` (`id`);

-- 
-- Constraints for table `request_booking_alert_log`
-- 
ALTER TABLE `request_booking_alert_log`
  ADD CONSTRAINT `request_booking_alert_log_ibfk_1` FOREIGN KEY (`request_booking_id`) REFERENCES `request_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Constraints for table `request_booking_equipment_type`
-- 
ALTER TABLE `request_booking_equipment_type`
  ADD CONSTRAINT `request_booking_equipment_type_ibfk_5` FOREIGN KEY (`request_booking_id`) REFERENCES `request_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `request_booking_equipment_type_ibfk_6` FOREIGN KEY (`equipment_type_id`) REFERENCES `equipment_type` (`id`);

-- 
-- Constraints for table `request_borrow`
-- 
ALTER TABLE `request_borrow`
  ADD CONSTRAINT `request_borrow_ibfk_11` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
  ADD CONSTRAINT `request_borrow_ibfk_12` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`),
  ADD CONSTRAINT `request_borrow_ibfk_13` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `request_borrow_alert_log`
-- 
ALTER TABLE `request_borrow_alert_log`
  ADD CONSTRAINT `request_borrow_alert_log_ibfk_1` FOREIGN KEY (`request_borrow_id`) REFERENCES `request_borrow` (`id`);

-- 
-- Constraints for table `request_borrow_equipment_type`
-- 
ALTER TABLE `request_borrow_equipment_type`
  ADD CONSTRAINT `request_borrow_equipment_type_ibfk_3` FOREIGN KEY (`request_borrow_id`) REFERENCES `request_borrow` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_borrow_equipment_type_ibfk_4` FOREIGN KEY (`equipment_type_id`) REFERENCES `equipment_type` (`id`);

-- 
-- Constraints for table `request_service`
-- 
ALTER TABLE `request_service`
  ADD CONSTRAINT `request_service_ibfk_6` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`),
  ADD CONSTRAINT `request_service_ibfk_7` FOREIGN KEY (`time_period`) REFERENCES `period` (`id`),
  ADD CONSTRAINT `request_service_ibfk_8` FOREIGN KEY (`status_code`) REFERENCES `status` (`status_code`);

-- 
-- Constraints for table `request_service_detail`
-- 
ALTER TABLE `request_service_detail`
  ADD CONSTRAINT `request_service_detail_ibfk_3` FOREIGN KEY (`request_service_id`) REFERENCES `request_service` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `request_service_detail_ibfk_4` FOREIGN KEY (`request_service_type_detail_id`) REFERENCES `request_service_type_detail` (`id`);

-- 
-- Constraints for table `request_service_type_detail`
-- 
ALTER TABLE `request_service_type_detail`
  ADD CONSTRAINT `request_service_type_detail_ibfk_1` FOREIGN KEY (`request_service_type_id`) REFERENCES `request_service_type` (`id`);

-- 
-- Constraints for table `role_permission`
-- 
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`permission_code`) REFERENCES `permission` (`permission_code`) ON DELETE CASCADE;

-- 
-- Constraints for table `room`
-- 
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`room_group_id`) REFERENCES `room_group` (`id`);

-- 
-- Constraints for table `room_equipment`
-- 
ALTER TABLE `room_equipment`
  ADD CONSTRAINT `room_equipment_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `room_equipment_ibfk_2` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`);

-- 
-- Constraints for table `status`
-- 
ALTER TABLE `status`
  ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`status_group_id`) REFERENCES `status_group` (`id`);

-- 
-- Constraints for table `user_forget_password_request`
-- 
ALTER TABLE `user_forget_password_request`
  ADD CONSTRAINT `user_forget_password_request_ibfk_1` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`id`);

-- 
-- Constraints for table `user_information`
-- 
ALTER TABLE `user_information`
  ADD CONSTRAINT `user_information_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user_login` (`id`) ON DELETE CASCADE;

-- 
-- Constraints for table `user_login`
-- 
ALTER TABLE `user_login`
  ADD CONSTRAINT `user_login_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `user_login_ibfk_4` FOREIGN KEY (`status`) REFERENCES `status` (`status_code`),
  ADD CONSTRAINT `user_login_ibfk_5` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);
