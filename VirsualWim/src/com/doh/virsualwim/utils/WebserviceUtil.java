package com.doh.virsualwim.utils;

/**
 * Created by pawit on 10/18/14 AD.
 */

public class WebserviceUtil {
	
	
    private static final String URL_LOGIN_LOCAL = "http://192.168.105.104/wim_service/WimService/GetLogin?user=%s&pass=%s";
    private static final String URL_LOGIN = "http://61.19.97.185/wim_service/WimService/GetLogin?user=%s&pass=%s";
    private static final String URL_STATION_LOCAL = "http://192.168.105.104/wim_service/WimService/GetStation";
    private static final String URL_STATION = "http://61.19.97.185/wim_service/WimService/GetStation";
    private static final String URL_WIMDATA_LOCAL = "http://192.168.105.104/wim_service/WimService/GetWimData?top=5";
    private static final String URL_WIMDATA = "http://61.19.97.185/wim_service/WimService/GetWimData?top=5";
    
    public static String getUrlLogin(){
    	return (Util.isLocalWifi())? URL_LOGIN_LOCAL : URL_LOGIN;

    }
    
    public static String getUrlStation(){
    	return (Util.isLocalWifi())? URL_STATION_LOCAL : URL_STATION;
    }
    
    public static String getUrlWimData(){
    	return (Util.isLocalWifi())? URL_WIMDATA_LOCAL : URL_WIMDATA;
    }
    
    public WebserviceUtil(){

    }

}
