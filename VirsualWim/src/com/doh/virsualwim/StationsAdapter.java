package com.doh.virsualwim;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import dom.doh.virsualwim.model.Station;

public class StationsAdapter extends SimpleAdapter {
	private List<Station> stations;
	/*
	* Alternating color list -- you could initialize this from anywhere.
	* Note that the colors make use of the alpha here, otherwise they would be
	* opaque and wouldn't give good results!
	*/
	private int[] colors = new int[] { 0x30ffffff, 0x30808080 };

	@SuppressWarnings("unchecked")
	public StationsAdapter(Context context, 
	        List<? extends Map<String, String>> stations, 
	        int resource, 
	        String[] from, 
	        int[] to) {
	  super(context, stations, resource, from, to);
	  this.stations = (List<Station>) stations;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	  View view = super.getView(position, convertView, parent);
	  
	  Station station  = stations.get(position);
	  
	  switch(station.getStationId()){
		  case 0:
			  view.setBackgroundColor(colors[0]);
			  break;
		  case -1:
			  view.setBackgroundColor(Color.parseColor("#FE9A2E"));
			  break;
		  default:
			  int colorPos = position % colors.length;
			  view.setBackgroundColor(colors[colorPos]);
			  break;
	  
	  }
		
//	  int colorPos = position % colors.length;
//	  view.setBackgroundColor(colors[colorPos]);
	  return view;
	}
}
