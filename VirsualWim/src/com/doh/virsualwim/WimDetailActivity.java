package com.doh.virsualwim;

import com.doh.virsualwim.db.DatabaseHandler;
import com.doh.virsualwim.utils.ImageLoader;
import com.doh.virsualwim.utils.Util;

import dom.doh.virsualwim.model.Wim;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class WimDetailActivity  extends Activity implements
ActionBar.OnNavigationListener   {
	
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";
	/**
	 * The dummy content this fragment is presenting.
	 */
	
	public ImageLoader imageLoader;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_wim_detail);
		
		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		String wimId = getIntent().getExtras().getString(ARG_ITEM_ID);
		
		DatabaseHandler db = new DatabaseHandler(this);
		imageLoader = new ImageLoader(this);
		Wim wim = db.getWim(Integer.parseInt(wimId));
		if( wim != null ){
			int  fSize = Util.getFrontSize(this);
			
			TextView txtWimId = (TextView) this.findViewById(R.id.txtWimId);
			txtWimId.setTextSize(fSize);
			TextView txtWimGvw = (TextView) this.findViewById(R.id.txtWimGvw);
			txtWimGvw.setTextSize(fSize);
			TextView txtStation = (TextView) this.findViewById(R.id.txtStation);
			txtStation.setTextSize(fSize);
			TextView txtMaxGvw = (TextView) this.findViewById(R.id.txtMaxGvw);
			txtMaxGvw.setTextSize(fSize);
			TextView txtDate = (TextView) this.findViewById(R.id.txtDate);
			txtDate.setTextSize(fSize);
			TextView txtLength = (TextView) this.findViewById(R.id.txtLength);
			txtLength.setTextSize(fSize);
			TextView txtTime = (TextView) this.findViewById(R.id.txtTime);
			txtTime.setTextSize(fSize);
			TextView txtFrontOverHang = (TextView) this.findViewById(R.id.txtFrontOverHang);
			txtFrontOverHang.setTextSize(fSize);
			TextView txtVehicleNumber = (TextView) this.findViewById(R.id.txtVehicleNumber);
			txtVehicleNumber.setTextSize(fSize);
			TextView txtRearOverHang = (TextView) this.findViewById(R.id.txtRearOverHang);
			txtRearOverHang.setTextSize(fSize);
			TextView txtLane = (TextView) this.findViewById(R.id.txtLane);
			txtLane.setTextSize(fSize);
			TextView txtLicensePlate = (TextView) this.findViewById(R.id.txtLicensePlate);
			txtLicensePlate.setTextSize(fSize);
			TextView txtClass = (TextView) this.findViewById(R.id.txtClass);
			txtClass.setTextSize(fSize);
			TextView txtProvince = (TextView) this.findViewById(R.id.txtLicensePlateProvince);
			txtProvince.setTextSize(fSize);
			TextView txtAxleCount = (TextView) this.findViewById(R.id.txtAxleCount);
			txtAxleCount.setTextSize(fSize);
			TextView txtError = (TextView) this.findViewById(R.id.txtError);
			txtError.setTextSize(fSize);
			TextView txtSpeed = (TextView) this.findViewById(R.id.txtSpeed);
			txtSpeed.setTextSize(fSize);
			TextView txtSortDecition = (TextView) this.findViewById(R.id.txtSortDecition);
			txtSortDecition.setTextSize(fSize);
			TextView txtESAL = (TextView) this.findViewById(R.id.txtESAL);
			txtESAL.setTextSize(fSize);
			TextView txtStatus = (TextView) this.findViewById(R.id.txtStatus);
			txtStatus.setTextSize(fSize);
			

			TextView sep1 = (TextView) this.findViewById(R.id.sep1);
			TextView sep2 = (TextView) this.findViewById(R.id.sep2);
			TextView sep3 = (TextView) this.findViewById(R.id.sep3);
			TextView sep4 = (TextView) this.findViewById(R.id.sep4);
			TextView sep5 = (TextView) this.findViewById(R.id.sep5);
			TextView sep6 = (TextView) this.findViewById(R.id.sep6);
			TextView sep7 = (TextView) this.findViewById(R.id.sep7);
			
			TextView group1 = (TextView) this.findViewById(R.id.group1);
			TextView group2 = (TextView) this.findViewById(R.id.group2);
			TextView group3 = (TextView) this.findViewById(R.id.group3);
			TextView group4 = (TextView) this.findViewById(R.id.group4);
			TextView group5 = (TextView) this.findViewById(R.id.group5);
			TextView group6 = (TextView) this.findViewById(R.id.group6);
			TextView group7 = (TextView) this.findViewById(R.id.group7);
			
			TextView weight1 = (TextView) this.findViewById(R.id.weight1);
			TextView weight2 = (TextView) this.findViewById(R.id.weight2);
			TextView weight3 = (TextView) this.findViewById(R.id.weight3);
			TextView weight4 = (TextView) this.findViewById(R.id.weight4);
			TextView weight5 = (TextView) this.findViewById(R.id.weight5);
			TextView weight6 = (TextView) this.findViewById(R.id.weight6);
			TextView weight7 = (TextView) this.findViewById(R.id.weight7);
			
			ImageView imgClass = (ImageView) this.findViewById(R.id.imgClass);
			ImageView imageView = (ImageView) this.findViewById(R.id.list_image);
			ImageView imageView2 = (ImageView) this.findViewById(R.id.list_image2);
		
			txtWimId.setText(wim.getWIMID()+"");
			txtWimGvw.setText(wim.getGVW()+"");
			txtStation.setText(wim.getStationName()+"");
			txtMaxGvw.setText(wim.getMaxGVW()+"");
			txtDate.setText(wim.getTimeStamp().toString().substring(0, 10)+"");
			txtLength.setText(wim.getLength()+"");
			txtTime.setText(wim.getTimeStamp().toString().substring(12,wim.getTimeStamp().toString().length())+"");
			txtFrontOverHang.setText(wim.getFrontOverHang()+"");
			txtVehicleNumber.setText(wim.getVehicleNumber()+"");
			txtRearOverHang.setText(wim.getRearOverHang()+"");
			txtLane.setText(wim.getLane()+"");
			txtLicensePlate.setText(wim.getLicensePlateNumber()+"");
			txtClass.setText(wim.getVehicleClass()+"");
			txtProvince.setText(wim.getProvinceName()+"");
			txtAxleCount.setText(wim.getAxleCount()+"");
			txtError.setText(wim.getError()+"");
			txtSpeed.setText(wim.getSpeed()+"");
			txtSortDecition.setText(wim.getSortDecision()+"");
			if(wim.getESAL().length()>7){
				txtESAL.setText(wim.getESAL().substring(0, 7)+"");
			}else{
				txtESAL.setText(wim.getESAL()+"");
			}
			txtStatus.setText(wim.getStatusCode()+"");


			sep1.setText(wim.getAxle01Seperation()+"");
			sep2.setText(wim.getAxle02Seperation()+"");
			sep3.setText(wim.getAxle03Seperation()+"");
			sep4.setText(wim.getAxle04Seperation()+"");
			sep5.setText(wim.getAxle05Seperation()+"");
			sep6.setText(wim.getAxle06Seperation()+"");
			sep7.setText(wim.getAxle07Seperation()+"");
			
			group1.setText(wim.getAxle01Group()+"");
			group2.setText(wim.getAxle02Group()+"");
			group3.setText(wim.getAxle03Group()+"");
			group4.setText(wim.getAxle04Group()+"");
			group5.setText(wim.getAxle05Group()+"");
			group6.setText(wim.getAxle06Group()+"");
			group7.setText(wim.getAxle07Group()+"");
			
			weight1.setText(wim.getAxle01Weight()+"");
			weight2.setText(wim.getAxle02Weight()+"");
			weight3.setText(wim.getAxle03Weight()+"");
			weight4.setText(wim.getAxle04Weight()+"");
			weight5.setText(wim.getAxle05Weight()+"");
			weight6.setText(wim.getAxle06Weight()+"");
			weight7.setText(wim.getAxle07Weight()+"");
			
			imageLoader.DisplayImage(wim.getImage01Name(), imageView);
			imageLoader.DisplayImage(wim.getImage02Name(), imageView2);
			imageLoader.DisplayImage("http://61.19.97.185/wim/images/vclass/class"+wim.getVehicleClass()+".jpg", imgClass);
		}
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main_actions, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpTo(this,
					new Intent(this, ItemListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
