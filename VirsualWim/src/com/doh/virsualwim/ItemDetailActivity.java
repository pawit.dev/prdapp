package com.doh.virsualwim;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import com.doh.virsualwim.db.DatabaseHandler;

import dom.doh.virsualwim.model.Config;

/**
 * An activity representing a single Item detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link ItemListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link ItemDetailFragment}.
 */

public class ItemDetailActivity extends Activity {

//	private MenuItem menuItem;
	private DatabaseHandler db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_item_detail);

		// ActionBar actionBar = getActionBar();
		// actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME
		// | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);
		db = DatabaseHandler.getInstance(this);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(ItemDetailFragment.ARG_ITEM_ID, getIntent()
					.getStringExtra(ItemDetailFragment.ARG_ITEM_ID));
			ItemDetailFragment fragment = new ItemDetailFragment();
			fragment.setArguments(arguments);
			getFragmentManager().beginTransaction()
					.add(R.id.item_detail_container, fragment).commit();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main_actions, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Config config = db.getConfig(1);
		switch (item.getItemId()) {
//		case R.id.menu_load:
//			menuItem = item;
//			menuItem.setActionView(R.layout.progress_bar);
//			menuItem.expandActionView();
//			TestTask task = new TestTask();
//			task.execute("test");
//			break;
		case android.R.id.home:
			this.finish();
			NavUtils.navigateUpTo(this,
					new Intent(this, ItemListActivity.class));
			break;
		case R.id.action_all_lane:
			config.setLane("0");
			db.updateConfig(config);
			break;
		case R.id.action_lane_1:
			config.setLane("1");
			db.updateConfig(config);
			break;
		case R.id.action_lane_2:
			config.setLane("2");
			db.updateConfig(config);
			break;
		case R.id.action_logout:
			Intent i = new Intent(this, LoginActivity.class);
			startActivity(i);
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

//	private class TestTask extends AsyncTask<String, Void, String> {
//
//		@Override
//		protected String doInBackground(String... params) {
//			// Simulate something long running
//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(String result) {
//			menuItem.collapseActionView();
//			menuItem.setActionView(null);
//		}
//	};

}
