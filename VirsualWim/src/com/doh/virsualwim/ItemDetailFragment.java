package com.doh.virsualwim;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.doh.virsualwim.db.DatabaseHandler;
import com.doh.virsualwim.utils.Util;
import com.doh.virsualwim.utils.WebserviceUtil;

import dom.doh.virsualwim.model.Wim;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ItemListActivity} in two-pane mode (on tablets) or a
 * {@link ItemDetailActivity} on handsets.
 */
public class ItemDetailFragment extends Fragment {

	private View rootView;
	private ListView list;
	
//	private MenuItem menuItem;
	
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";
	/**
	 * The dummy content this fragment is presenting.
	 */
//	private Station mItem;
	private int stationId;
	private int stationGroupId;
	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static ArrayList<Wim> wimList = new ArrayList<Wim>();
	WimAdapter adapter;
	DatabaseHandler db;

	boolean isUpdateUI = true;
	boolean isCallServiceSuccess = true;
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
		       	// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			String[] selectedKey = getArguments().getString(ARG_ITEM_ID).split(",");
			this.stationGroupId = Integer.parseInt(selectedKey[0]); 
			this.stationId = Integer.parseInt(selectedKey[1]); 
//			mItem = ItemListFragment.STATIONS.get(0);

			db = DatabaseHandler.getInstance(getActivity());

		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_item_detail, container,
				false);
		
		list = ((ListView) rootView.findViewById(R.id.item_detail));


//		menuItem = ((MenuItem) rootView.findViewById(R.id.menu_load));
//		menuItem.setActionView(R.layout.progress_bar);
//		menuItem.expandActionView();
		
		// Show the dummy content as text in a TextView.


			//Log.d("Selected station:", mItem.stationName);
			// Update UI
			final Handler handler = new Handler();
			Timer timer = new Timer();
			TimerTask doAsynchronousTask = new TimerTask() {
				@Override
				public void run() {
					handler.post(new Runnable() {
						public void run() {
							try {

								if (isUpdateUI) {
									new UpdateUiTask(getActivity()).execute();

									isUpdateUI = !isUpdateUI;

									Log.d("Update wim detail",
											"----------- update data ----------");
								}

							} catch (Exception e) {
								// TODO Auto-generated catch block
							}
						}
					});
				}
			};
			
			timer.schedule(doAsynchronousTask, 0, 4000); // execute in every

			//final Handler handler = new Handler();
			//Timer timer = new Timer();
			TimerTask doAsynchronousTask1 = new TimerTask() {
				@Override
				public void run() {
					handler.post(new Runnable() {
						public void run() {
							try {
								if (isCallServiceSuccess) {

									isCallServiceSuccess = !isCallServiceSuccess;

									new WimTask(getActivity()).execute();
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
							}
						}
					});
				}
			};
			timer.schedule(doAsynchronousTask1, 0, 5000); // execute in every
															// 50000 ms
			
			list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					Intent wimDetail = new Intent(view.getContext(),
							WimDetailActivity.class);
					wimDetail.putExtra(WimDetailActivity.ARG_ITEM_ID, wimList
							.get(position).getWIMID() + "");
					getActivity().startActivity(wimDetail);

				}
			});

		

		return rootView;
	}


	public class UpdateUiTask extends AsyncTask<String, Void, Void> {

		//private ProgressDialog Dialog = null;
		
		public UpdateUiTask(Context _context) {
			//Dialog = new ProgressDialog(_context);
		}

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.

			// Start Progress Dialog (Message)
			//Dialog.setMessage("Please wait..");
			//Dialog.show();

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			wimList = db.getAllByStation(stationId,stationGroupId+"");
			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.
			adapter = new WimAdapter(getActivity(), wimList);
			list.setAdapter(adapter);
			isUpdateUI = true;
			// Close progress dialog
			//Dialog.dismiss();

		}
	}
	
	

	public class WimTask extends AsyncTask<String, Void, Void> {

		// Required initialization
		private String Content;
		private String Error = null;
		// private ProgressDialog Dialog = null;
		// private Context context;

		String data = "";
		String serverText = "";

		public WimTask(Context _context) {
			// context = _context;
			// Dialog = new ProgressDialog(_context);
		}

		protected void onPreExecute() {
			// NOTE: You can call UI Element here.

			// Start Progress Dialog (Message)

			// Dialog.setMessage("Please wait..");
			// Dialog.show();

			try {
				// Set Request parameter
				data += "&" + URLEncoder.encode("data", "UTF-8") + "="
						+ serverText;

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			/************ Make Post Call To Web Server ***********/
			BufferedReader reader = null;

			// Send data
			try {

				// Defined URL where to send data
				URL url = new URL(WebserviceUtil.getUrlWimData());

				// Send POST data request

				URLConnection conn = url.openConnection();
				conn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						conn.getOutputStream());
				wr.write(data);
				wr.flush();

				// Get the server response

				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line = null;

				// Read Server Response
				while ((line = reader.readLine()) != null) {
					// Append server response in string
					sb.append(line + "");
				}

				// Append Server Response To Content String
				Content = sb.toString();
			} catch (Exception ex) {
				Error = ex.getMessage();
			} finally {
				try {

					reader.close();
				}

				catch (Exception ex) {
					Log.d("", "" + ex.getMessage());
				}
			}

			/*****************************************************/
			return null;
		}

		protected void onPostExecute(Void unused) {
			// NOTE: You can call UI Element here.

			// Close progress dialog
			// Dialog.dismiss();

			if (Error != null) {
				Log.d("Error", Error);
			} else {
				Log.d("Content", Content);
				/****************** Start Parse Response JSON Data *************/
				JSONArray jsonArr;

				try {

					jsonArr = new JSONArray(Content);
					for (int i = 0; i < jsonArr.length(); i++) {
						Wim item = new Wim();

						JSONObject data = jsonArr.getJSONObject(i);
						@SuppressWarnings("rawtypes")
						Iterator keys = data.keys();

						while (keys.hasNext()) {
							String key = (String) keys.next();

							if (key.equalsIgnoreCase("wimid")) {
								item.setWIMID(data.getInt(key));
							} else if (key.equalsIgnoreCase("imgpath")) {
								item.setImage01Name(data.getString(key));
							} else if (key.equalsIgnoreCase("imgPath2")) {
								item.setImage02Name(data.getString(key));
							}
							// else if (key.equalsIgnoreCase("RefID")) {
							// item.se(data.getString(key));}
							else if (key.equalsIgnoreCase("StationName")) {
								item.setStationName(data.getString(key));
							} else if (key.equalsIgnoreCase("StationId")) {
								item.setStationID(data.getInt(key));
							} else if (key.equalsIgnoreCase("TimeStamp")) {
								item.setTimeStamp(data.getString(key));
								item.setTimeStampInt(Util.dateStringLong(data.getString(key)));
							} else if (key.equalsIgnoreCase("VehicleNumber")) {
								item.setVehicleNumber(data.getString(key));
							}
							// else if (key.equalsIgnoreCase("Lane")) {
							// item.setLane(data.getString(key));}
							else if (key.equalsIgnoreCase("LaneId")) {
								item.setLane(data.getString(key));
							} else if (key.equalsIgnoreCase("VehicleClass")) {
								item.setVehicleClass(data.getString(key));
							} else if (key.equalsIgnoreCase("SortDecision")) {
								item.setSortDecision(data.getString(key));
							} else if (key.equalsIgnoreCase("MaxGvw")) {
								item.setMaxGVW(data.getString(key));
							} else if (key.equalsIgnoreCase("Gvw")) {
								item.setGVW(data.getString(key));
							} else if (key
									.equalsIgnoreCase("LicensePlateNumber")) {
								item.setLicensePlateNumber(data.getString(key));
							} else if (key.equalsIgnoreCase("ProvinceName")) {
								item.setProvinceName(data.getString(key));
							} else if (key.equalsIgnoreCase("statusCode")) {
								item.setStatusCode(data.getString(key));
							} else if (key.equalsIgnoreCase("errorCode")) {
								item.setError(data.getString(key));
							} else if (key.equalsIgnoreCase("StatusColor")) {
								item.setStatusColor(data.getString(key));
							} else if (key.equalsIgnoreCase("Speed")) {
								item.setSpeed(data.getString(key));
							} else if (key.equalsIgnoreCase("ESAL")) {
								item.setESAL(data.getString(key));
							} else if (key.equalsIgnoreCase("AxleCount")) {
								item.setAxleCount(data.getString(key));
							} else if (key.equalsIgnoreCase("Length")) {
								item.setLength(data.getString(key));
							} else if (key.equalsIgnoreCase("FrontOverHang")) {
								item.setFrontOverHang(data.getString(key));
							} else if (key.equalsIgnoreCase("RearOverHang")) {
								item.setRearOverHang(data.getString(key));
							}

							else if (key.equalsIgnoreCase("SeperationCol_1")) {
								item.setAxle01Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_2")) {
								item.setAxle02Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_3")) {
								item.setAxle03Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_4")) {
								item.setAxle04Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_5")) {
								item.setAxle05Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_6")) {
								item.setAxle06Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_7")) {
								item.setAxle07Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_8")) {
								item.setAxle08Seperation(data.getString(key));
							} else if (key.equalsIgnoreCase("SeperationCol_9")) {
								item.setAxle09Seperation(data.getString(key));
							}

							else if (key.equalsIgnoreCase("GroupCol_1")) {
								item.setAxle01Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_2")) {
								item.setAxle02Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_3")) {
								item.setAxle03Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_4")) {
								item.setAxle04Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_5")) {
								item.setAxle05Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_6")) {
								item.setAxle06Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_7")) {
								item.setAxle07Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_8")) {
								item.setAxle08Group(data.getString(key));
							} else if (key.equalsIgnoreCase("GroupCol_9")) {
								item.setAxle09Group(data.getString(key));
							}

							else if (key.equalsIgnoreCase("WeightCol_1")) {
								item.setAxle01Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_2")) {
								item.setAxle02Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_3")) {
								item.setAxle03Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_4")) {
								item.setAxle04Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_5")) {
								item.setAxle05Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_6")) {
								item.setAxle06Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_7")) {
								item.setAxle07Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_8")) {
								item.setAxle08Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("WeightCol_9")) {
								item.setAxle09Weight(data.getString(key));
							} else if (key.equalsIgnoreCase("StationGroupID")) {
								item.setStationGroupID(data.getString(key));
							}
						}
						try {
							Wim wim = db.getWim(item.getWIMID());
							if (wim == null) {
								db.addWim(item);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					isCallServiceSuccess = true;
//					menuItem.collapseActionView();
//					menuItem.setActionView(null);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
