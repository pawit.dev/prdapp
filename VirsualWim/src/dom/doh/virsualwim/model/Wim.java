package dom.doh.virsualwim.model;


public class Wim {
	
	int WIMID;
    int StationID;
    String TimeStamp;
    String TimeStampInt;
    String VehicleNumber;
    String Lane;
    String Error;
    String StatusCode;
    String GVW;
    String MaxGVW;
    String ESAL;
    String Speed;
    String AxleCount;
    String Axle01Seperation;
    String Axle01Weight;
    String Axle01Max;
    String Axle01Group;
    String Axle01TireCode;
    String Axle02Seperation;
    String Axle02Weight;
    String Axle02Max;
    String Axle02Group;
    String Axle02TireCode;
    String Axle03Seperation;
    String Axle03Weight;
    String Axle03Max;
    String Axle03Group;
    String Axle03TireCode;
    String Axle04Seperation;
    String Axle04Weight;
    String Axle04Max;
    String Axle04Group;
    String Axle04TireCode;
    String Axle05Seperation;
    String Axle05Weight;
    String Axle05Max;
    String Axle05Group;
    String Axle05TireCode;
    String Axle06Seperation;
    String Axle06Weight;
    String Axle06Max;
    String Axle06Group;
    String Axle06TireCode;
    String Axle07Seperation;
    String Axle07Weight;
    String Axle07Max;
    String Axle07Group;
    String Axle07TireCode;
    String Axle08Seperation;
    String Axle08Weight;
    String Axle08Max;
    String Axle08Group;
    String Axle08TireCode;
    String Axle09Seperation;
    String Axle09Weight;
    String Axle09Max;
    String Axle09Group;
    String Axle09TireCode;
    String Axle10Seperation;
    String Axle10Weight;
    String Axle10Max;
    String Axle10Group;
    String Axle10TireCode;
    String Axle11Seperation;
    String Axle11Weight;
    String Axle11Max;
    String Axle11Group;
    String Axle11TireCode;
    String Axle12Seperation;
    String Axle12Weight;
    String Axle12Max;
    String Axle12Group;
    String Axle12TireCode;
    String Axle13Seperation;
    String Axle13Weight;
    String Axle13Max;
    String Axle13Group;
    String Axle13TireCode;
    String Axle14Seperation;
    String Axle14Weight;
    String Axle14Max;
    String Axle14Group;
    String Axle14TireCode;
    String Length;
    String FrontOverHang;
    String RearOverHang;
    String VehicleType;
    String VehicleClass;
    String RecordType;
    String ImageCount;
    String Image01Name;
    String Image02Name;
    String Image03Name;
    String Image04Name;
    String Image05Name;
    String Image06Name;
    String Image07Name;
    String Image08Name;
    String Image09Name;
    String Image10Name;
    String SortDecision;
    String LicensePlateNumber;
    String LicensePlateProvinceID;
    String LicensePlateImageName;
    String LicensePlateConfidence;
    String StatusColor;
    String ProvinceName;
    String StationName;
    String StationGroupID;
    


	public Wim(){}

	public Wim(int wIMID, int stationID, String timeStamp, String timeStampInt,
			String vehicleNumber, String lane, String error, String statusCode,
			String gVW, String maxGVW, String eSAL, String speed,
			String axleCount, String axle01Seperation, String axle01Weight,
			String axle01Max, String axle01Group, String axle01TireCode,
			String axle02Seperation, String axle02Weight, String axle02Max,
			String axle02Group, String axle02TireCode, String axle03Seperation,
			String axle03Weight, String axle03Max, String axle03Group,
			String axle03TireCode, String axle04Seperation,
			String axle04Weight, String axle04Max, String axle04Group,
			String axle04TireCode, String axle05Seperation,
			String axle05Weight, String axle05Max, String axle05Group,
			String axle05TireCode, String axle06Seperation,
			String axle06Weight, String axle06Max, String axle06Group,
			String axle06TireCode, String axle07Seperation,
			String axle07Weight, String axle07Max, String axle07Group,
			String axle07TireCode, String axle08Seperation,
			String axle08Weight, String axle08Max, String axle08Group,
			String axle08TireCode, String axle09Seperation,
			String axle09Weight, String axle09Max, String axle09Group,
			String axle09TireCode, String axle10Seperation,
			String axle10Weight, String axle10Max, String axle10Group,
			String axle10TireCode, String axle11Seperation,
			String axle11Weight, String axle11Max, String axle11Group,
			String axle11TireCode, String axle12Seperation,
			String axle12Weight, String axle12Max, String axle12Group,
			String axle12TireCode, String axle13Seperation,
			String axle13Weight, String axle13Max, String axle13Group,
			String axle13TireCode, String axle14Seperation,
			String axle14Weight, String axle14Max, String axle14Group,
			String axle14TireCode, String length, String frontOverHang,
			String rearOverHang, String vehicleType, String vehicleClass,
			String recordType, String imageCount, String image01Name,
			String image02Name, String image03Name, String image04Name,
			String image05Name, String image06Name, String image07Name,
			String image08Name, String image09Name, String image10Name,
			String sortDecision, String licensePlateNumber,
			String licensePlateProvinceID, String licensePlateImageName,
			String licensePlateConfidence, String statusColor,
			String provinceName, String stationName,String stationGroupID) {
		super();
		WIMID = wIMID;
		StationID = stationID;
		TimeStamp = timeStamp;
		TimeStampInt = timeStampInt;
		VehicleNumber = vehicleNumber;
		Lane = lane;
		Error = error;
		StatusCode = statusCode;
		GVW = gVW;
		MaxGVW = maxGVW;
		ESAL = eSAL;
		Speed = speed;
		AxleCount = axleCount;
		Axle01Seperation = axle01Seperation;
		Axle01Weight = axle01Weight;
		Axle01Max = axle01Max;
		Axle01Group = axle01Group;
		Axle01TireCode = axle01TireCode;
		Axle02Seperation = axle02Seperation;
		Axle02Weight = axle02Weight;
		Axle02Max = axle02Max;
		Axle02Group = axle02Group;
		Axle02TireCode = axle02TireCode;
		Axle03Seperation = axle03Seperation;
		Axle03Weight = axle03Weight;
		Axle03Max = axle03Max;
		Axle03Group = axle03Group;
		Axle03TireCode = axle03TireCode;
		Axle04Seperation = axle04Seperation;
		Axle04Weight = axle04Weight;
		Axle04Max = axle04Max;
		Axle04Group = axle04Group;
		Axle04TireCode = axle04TireCode;
		Axle05Seperation = axle05Seperation;
		Axle05Weight = axle05Weight;
		Axle05Max = axle05Max;
		Axle05Group = axle05Group;
		Axle05TireCode = axle05TireCode;
		Axle06Seperation = axle06Seperation;
		Axle06Weight = axle06Weight;
		Axle06Max = axle06Max;
		Axle06Group = axle06Group;
		Axle06TireCode = axle06TireCode;
		Axle07Seperation = axle07Seperation;
		Axle07Weight = axle07Weight;
		Axle07Max = axle07Max;
		Axle07Group = axle07Group;
		Axle07TireCode = axle07TireCode;
		Axle08Seperation = axle08Seperation;
		Axle08Weight = axle08Weight;
		Axle08Max = axle08Max;
		Axle08Group = axle08Group;
		Axle08TireCode = axle08TireCode;
		Axle09Seperation = axle09Seperation;
		Axle09Weight = axle09Weight;
		Axle09Max = axle09Max;
		Axle09Group = axle09Group;
		Axle09TireCode = axle09TireCode;
		Axle10Seperation = axle10Seperation;
		Axle10Weight = axle10Weight;
		Axle10Max = axle10Max;
		Axle10Group = axle10Group;
		Axle10TireCode = axle10TireCode;
		Axle11Seperation = axle11Seperation;
		Axle11Weight = axle11Weight;
		Axle11Max = axle11Max;
		Axle11Group = axle11Group;
		Axle11TireCode = axle11TireCode;
		Axle12Seperation = axle12Seperation;
		Axle12Weight = axle12Weight;
		Axle12Max = axle12Max;
		Axle12Group = axle12Group;
		Axle12TireCode = axle12TireCode;
		Axle13Seperation = axle13Seperation;
		Axle13Weight = axle13Weight;
		Axle13Max = axle13Max;
		Axle13Group = axle13Group;
		Axle13TireCode = axle13TireCode;
		Axle14Seperation = axle14Seperation;
		Axle14Weight = axle14Weight;
		Axle14Max = axle14Max;
		Axle14Group = axle14Group;
		Axle14TireCode = axle14TireCode;
		Length = length;
		FrontOverHang = frontOverHang;
		RearOverHang = rearOverHang;
		VehicleType = vehicleType;
		VehicleClass = vehicleClass;
		RecordType = recordType;
		ImageCount = imageCount;
		Image01Name = image01Name;
		Image02Name = image02Name;
		Image03Name = image03Name;
		Image04Name = image04Name;
		Image05Name = image05Name;
		Image06Name = image06Name;
		Image07Name = image07Name;
		Image08Name = image08Name;
		Image09Name = image09Name;
		Image10Name = image10Name;
		SortDecision = sortDecision;
		LicensePlateNumber = licensePlateNumber;
		LicensePlateProvinceID = licensePlateProvinceID;
		LicensePlateImageName = licensePlateImageName;
		LicensePlateConfidence = licensePlateConfidence;
		StatusColor = statusColor;
		ProvinceName = provinceName;
		StationName = stationName;
		StationGroupID = stationGroupID;
	}

	public int getWIMID() {
		return WIMID;
	}

	public void setWIMID(int wIMID) {
		WIMID = wIMID;
	}

	public int getStationID() {
		return StationID;
	}

	public void setStationID(int stationID) {
		StationID = stationID;
	}

	public String getTimeStamp() {
		return TimeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		TimeStamp = timeStamp;
	}

	public String getTimeStampInt() {
		return TimeStampInt;
	}

	public void setTimeStampInt(String timeStampInt) {
		TimeStampInt = timeStampInt;
	}

	public String getVehicleNumber() {
		return VehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		VehicleNumber = vehicleNumber;
	}

	public String getLane() {
		return Lane;
	}

	public void setLane(String lane) {
		Lane = lane;
	}

	public String getError() {
		return Error;
	}

	public void setError(String error) {
		Error = error;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	public String getGVW() {
		return GVW;
	}

	public void setGVW(String gVW) {
		GVW = gVW;
	}

	public String getMaxGVW() {
		return MaxGVW;
	}

	public void setMaxGVW(String maxGVW) {
		MaxGVW = maxGVW;
	}

	public String getESAL() {
		return ESAL;
	}

	public void setESAL(String eSAL) {
		ESAL = eSAL;
	}

	public String getSpeed() {
		return Speed;
	}

	public void setSpeed(String speed) {
		Speed = speed;
	}

	public String getAxleCount() {
		return AxleCount;
	}

	public void setAxleCount(String axleCount) {
		AxleCount = axleCount;
	}

	public String getAxle01Seperation() {
		return Axle01Seperation;
	}

	public void setAxle01Seperation(String axle01Seperation) {
		Axle01Seperation = axle01Seperation;
	}

	public String getAxle01Weight() {
		return Axle01Weight;
	}

	public void setAxle01Weight(String axle01Weight) {
		Axle01Weight = axle01Weight;
	}

	public String getAxle01Max() {
		return Axle01Max;
	}

	public void setAxle01Max(String axle01Max) {
		Axle01Max = axle01Max;
	}

	public String getAxle01Group() {
		return Axle01Group;
	}

	public void setAxle01Group(String axle01Group) {
		Axle01Group = axle01Group;
	}

	public String getAxle01TireCode() {
		return Axle01TireCode;
	}

	public void setAxle01TireCode(String axle01TireCode) {
		Axle01TireCode = axle01TireCode;
	}

	public String getAxle02Seperation() {
		return Axle02Seperation;
	}

	public void setAxle02Seperation(String axle02Seperation) {
		Axle02Seperation = axle02Seperation;
	}

	public String getAxle02Weight() {
		return Axle02Weight;
	}

	public void setAxle02Weight(String axle02Weight) {
		Axle02Weight = axle02Weight;
	}

	public String getAxle02Max() {
		return Axle02Max;
	}

	public void setAxle02Max(String axle02Max) {
		Axle02Max = axle02Max;
	}

	public String getAxle02Group() {
		return Axle02Group;
	}

	public void setAxle02Group(String axle02Group) {
		Axle02Group = axle02Group;
	}

	public String getAxle02TireCode() {
		return Axle02TireCode;
	}

	public void setAxle02TireCode(String axle02TireCode) {
		Axle02TireCode = axle02TireCode;
	}

	public String getAxle03Seperation() {
		return Axle03Seperation;
	}

	public void setAxle03Seperation(String axle03Seperation) {
		Axle03Seperation = axle03Seperation;
	}

	public String getAxle03Weight() {
		return Axle03Weight;
	}

	public void setAxle03Weight(String axle03Weight) {
		Axle03Weight = axle03Weight;
	}

	public String getAxle03Max() {
		return Axle03Max;
	}

	public void setAxle03Max(String axle03Max) {
		Axle03Max = axle03Max;
	}

	public String getAxle03Group() {
		return Axle03Group;
	}

	public void setAxle03Group(String axle03Group) {
		Axle03Group = axle03Group;
	}

	public String getAxle03TireCode() {
		return Axle03TireCode;
	}

	public void setAxle03TireCode(String axle03TireCode) {
		Axle03TireCode = axle03TireCode;
	}

	public String getAxle04Seperation() {
		return Axle04Seperation;
	}

	public void setAxle04Seperation(String axle04Seperation) {
		Axle04Seperation = axle04Seperation;
	}

	public String getAxle04Weight() {
		return Axle04Weight;
	}

	public void setAxle04Weight(String axle04Weight) {
		Axle04Weight = axle04Weight;
	}

	public String getAxle04Max() {
		return Axle04Max;
	}

	public void setAxle04Max(String axle04Max) {
		Axle04Max = axle04Max;
	}

	public String getAxle04Group() {
		return Axle04Group;
	}

	public void setAxle04Group(String axle04Group) {
		Axle04Group = axle04Group;
	}

	public String getAxle04TireCode() {
		return Axle04TireCode;
	}

	public void setAxle04TireCode(String axle04TireCode) {
		Axle04TireCode = axle04TireCode;
	}

	public String getAxle05Seperation() {
		return Axle05Seperation;
	}

	public void setAxle05Seperation(String axle05Seperation) {
		Axle05Seperation = axle05Seperation;
	}

	public String getAxle05Weight() {
		return Axle05Weight;
	}

	public void setAxle05Weight(String axle05Weight) {
		Axle05Weight = axle05Weight;
	}

	public String getAxle05Max() {
		return Axle05Max;
	}

	public void setAxle05Max(String axle05Max) {
		Axle05Max = axle05Max;
	}

	public String getAxle05Group() {
		return Axle05Group;
	}

	public void setAxle05Group(String axle05Group) {
		Axle05Group = axle05Group;
	}

	public String getAxle05TireCode() {
		return Axle05TireCode;
	}

	public void setAxle05TireCode(String axle05TireCode) {
		Axle05TireCode = axle05TireCode;
	}

	public String getAxle06Seperation() {
		return Axle06Seperation;
	}

	public void setAxle06Seperation(String axle06Seperation) {
		Axle06Seperation = axle06Seperation;
	}

	public String getAxle06Weight() {
		return Axle06Weight;
	}

	public void setAxle06Weight(String axle06Weight) {
		Axle06Weight = axle06Weight;
	}

	public String getAxle06Max() {
		return Axle06Max;
	}

	public void setAxle06Max(String axle06Max) {
		Axle06Max = axle06Max;
	}

	public String getAxle06Group() {
		return Axle06Group;
	}

	public void setAxle06Group(String axle06Group) {
		Axle06Group = axle06Group;
	}

	public String getAxle06TireCode() {
		return Axle06TireCode;
	}

	public void setAxle06TireCode(String axle06TireCode) {
		Axle06TireCode = axle06TireCode;
	}

	public String getAxle07Seperation() {
		return Axle07Seperation;
	}

	public void setAxle07Seperation(String axle07Seperation) {
		Axle07Seperation = axle07Seperation;
	}

	public String getAxle07Weight() {
		return Axle07Weight;
	}

	public void setAxle07Weight(String axle07Weight) {
		Axle07Weight = axle07Weight;
	}

	public String getAxle07Max() {
		return Axle07Max;
	}

	public void setAxle07Max(String axle07Max) {
		Axle07Max = axle07Max;
	}

	public String getAxle07Group() {
		return Axle07Group;
	}

	public void setAxle07Group(String axle07Group) {
		Axle07Group = axle07Group;
	}

	public String getAxle07TireCode() {
		return Axle07TireCode;
	}

	public void setAxle07TireCode(String axle07TireCode) {
		Axle07TireCode = axle07TireCode;
	}

	public String getAxle08Seperation() {
		return Axle08Seperation;
	}

	public void setAxle08Seperation(String axle08Seperation) {
		Axle08Seperation = axle08Seperation;
	}

	public String getAxle08Weight() {
		return Axle08Weight;
	}

	public void setAxle08Weight(String axle08Weight) {
		Axle08Weight = axle08Weight;
	}

	public String getAxle08Max() {
		return Axle08Max;
	}

	public void setAxle08Max(String axle08Max) {
		Axle08Max = axle08Max;
	}

	public String getAxle08Group() {
		return Axle08Group;
	}

	public void setAxle08Group(String axle08Group) {
		Axle08Group = axle08Group;
	}

	public String getAxle08TireCode() {
		return Axle08TireCode;
	}

	public void setAxle08TireCode(String axle08TireCode) {
		Axle08TireCode = axle08TireCode;
	}

	public String getAxle09Seperation() {
		return Axle09Seperation;
	}

	public void setAxle09Seperation(String axle09Seperation) {
		Axle09Seperation = axle09Seperation;
	}

	public String getAxle09Weight() {
		return Axle09Weight;
	}

	public void setAxle09Weight(String axle09Weight) {
		Axle09Weight = axle09Weight;
	}

	public String getAxle09Max() {
		return Axle09Max;
	}

	public void setAxle09Max(String axle09Max) {
		Axle09Max = axle09Max;
	}

	public String getAxle09Group() {
		return Axle09Group;
	}

	public void setAxle09Group(String axle09Group) {
		Axle09Group = axle09Group;
	}

	public String getAxle09TireCode() {
		return Axle09TireCode;
	}

	public void setAxle09TireCode(String axle09TireCode) {
		Axle09TireCode = axle09TireCode;
	}

	public String getAxle10Seperation() {
		return Axle10Seperation;
	}

	public void setAxle10Seperation(String axle10Seperation) {
		Axle10Seperation = axle10Seperation;
	}

	public String getAxle10Weight() {
		return Axle10Weight;
	}

	public void setAxle10Weight(String axle10Weight) {
		Axle10Weight = axle10Weight;
	}

	public String getAxle10Max() {
		return Axle10Max;
	}

	public void setAxle10Max(String axle10Max) {
		Axle10Max = axle10Max;
	}

	public String getAxle10Group() {
		return Axle10Group;
	}

	public void setAxle10Group(String axle10Group) {
		Axle10Group = axle10Group;
	}

	public String getAxle10TireCode() {
		return Axle10TireCode;
	}

	public void setAxle10TireCode(String axle10TireCode) {
		Axle10TireCode = axle10TireCode;
	}

	public String getAxle11Seperation() {
		return Axle11Seperation;
	}

	public void setAxle11Seperation(String axle11Seperation) {
		Axle11Seperation = axle11Seperation;
	}

	public String getAxle11Weight() {
		return Axle11Weight;
	}

	public void setAxle11Weight(String axle11Weight) {
		Axle11Weight = axle11Weight;
	}

	public String getAxle11Max() {
		return Axle11Max;
	}

	public void setAxle11Max(String axle11Max) {
		Axle11Max = axle11Max;
	}

	public String getAxle11Group() {
		return Axle11Group;
	}

	public void setAxle11Group(String axle11Group) {
		Axle11Group = axle11Group;
	}

	public String getAxle11TireCode() {
		return Axle11TireCode;
	}

	public void setAxle11TireCode(String axle11TireCode) {
		Axle11TireCode = axle11TireCode;
	}

	public String getAxle12Seperation() {
		return Axle12Seperation;
	}

	public void setAxle12Seperation(String axle12Seperation) {
		Axle12Seperation = axle12Seperation;
	}

	public String getAxle12Weight() {
		return Axle12Weight;
	}

	public void setAxle12Weight(String axle12Weight) {
		Axle12Weight = axle12Weight;
	}

	public String getAxle12Max() {
		return Axle12Max;
	}

	public void setAxle12Max(String axle12Max) {
		Axle12Max = axle12Max;
	}

	public String getAxle12Group() {
		return Axle12Group;
	}

	public void setAxle12Group(String axle12Group) {
		Axle12Group = axle12Group;
	}

	public String getAxle12TireCode() {
		return Axle12TireCode;
	}

	public void setAxle12TireCode(String axle12TireCode) {
		Axle12TireCode = axle12TireCode;
	}

	public String getAxle13Seperation() {
		return Axle13Seperation;
	}

	public void setAxle13Seperation(String axle13Seperation) {
		Axle13Seperation = axle13Seperation;
	}

	public String getAxle13Weight() {
		return Axle13Weight;
	}

	public void setAxle13Weight(String axle13Weight) {
		Axle13Weight = axle13Weight;
	}

	public String getAxle13Max() {
		return Axle13Max;
	}

	public void setAxle13Max(String axle13Max) {
		Axle13Max = axle13Max;
	}

	public String getAxle13Group() {
		return Axle13Group;
	}

	public void setAxle13Group(String axle13Group) {
		Axle13Group = axle13Group;
	}

	public String getAxle13TireCode() {
		return Axle13TireCode;
	}

	public void setAxle13TireCode(String axle13TireCode) {
		Axle13TireCode = axle13TireCode;
	}

	public String getAxle14Seperation() {
		return Axle14Seperation;
	}

	public void setAxle14Seperation(String axle14Seperation) {
		Axle14Seperation = axle14Seperation;
	}

	public String getAxle14Weight() {
		return Axle14Weight;
	}

	public void setAxle14Weight(String axle14Weight) {
		Axle14Weight = axle14Weight;
	}

	public String getAxle14Max() {
		return Axle14Max;
	}

	public void setAxle14Max(String axle14Max) {
		Axle14Max = axle14Max;
	}

	public String getAxle14Group() {
		return Axle14Group;
	}

	public void setAxle14Group(String axle14Group) {
		Axle14Group = axle14Group;
	}

	public String getAxle14TireCode() {
		return Axle14TireCode;
	}

	public void setAxle14TireCode(String axle14TireCode) {
		Axle14TireCode = axle14TireCode;
	}

	public String getLength() {
		return Length;
	}

	public void setLength(String length) {
		Length = length;
	}

	public String getFrontOverHang() {
		return FrontOverHang;
	}

	public void setFrontOverHang(String frontOverHang) {
		FrontOverHang = frontOverHang;
	}

	public String getRearOverHang() {
		return RearOverHang;
	}

	public void setRearOverHang(String rearOverHang) {
		RearOverHang = rearOverHang;
	}

	public String getVehicleType() {
		return VehicleType;
	}

	public void setVehicleType(String vehicleType) {
		VehicleType = vehicleType;
	}

	public String getVehicleClass() {
		return VehicleClass;
	}

	public void setVehicleClass(String vehicleClass) {
		VehicleClass = vehicleClass;
	}

	public String getRecordType() {
		return RecordType;
	}

	public void setRecordType(String recordType) {
		RecordType = recordType;
	}

	public String getImageCount() {
		return ImageCount;
	}

	public void setImageCount(String imageCount) {
		ImageCount = imageCount;
	}

	public String getImage01Name() {
		return Image01Name;
	}

	public void setImage01Name(String image01Name) {
		Image01Name = image01Name;
	}

	public String getImage02Name() {
		return Image02Name;
	}

	public void setImage02Name(String image02Name) {
		Image02Name = image02Name;
	}

	public String getImage03Name() {
		return Image03Name;
	}

	public void setImage03Name(String image03Name) {
		Image03Name = image03Name;
	}

	public String getImage04Name() {
		return Image04Name;
	}

	public void setImage04Name(String image04Name) {
		Image04Name = image04Name;
	}

	public String getImage05Name() {
		return Image05Name;
	}

	public void setImage05Name(String image05Name) {
		Image05Name = image05Name;
	}

	public String getImage06Name() {
		return Image06Name;
	}

	public void setImage06Name(String image06Name) {
		Image06Name = image06Name;
	}

	public String getImage07Name() {
		return Image07Name;
	}

	public void setImage07Name(String image07Name) {
		Image07Name = image07Name;
	}

	public String getImage08Name() {
		return Image08Name;
	}

	public void setImage08Name(String image08Name) {
		Image08Name = image08Name;
	}

	public String getImage09Name() {
		return Image09Name;
	}

	public void setImage09Name(String image09Name) {
		Image09Name = image09Name;
	}

	public String getImage10Name() {
		return Image10Name;
	}

	public void setImage10Name(String image10Name) {
		Image10Name = image10Name;
	}

	public String getSortDecision() {
		return SortDecision;
	}

	public void setSortDecision(String sortDecision) {
		SortDecision = sortDecision;
	}

	public String getLicensePlateNumber() {
		return LicensePlateNumber;
	}

	public void setLicensePlateNumber(String licensePlateNumber) {
		LicensePlateNumber = licensePlateNumber;
	}

	public String getLicensePlateProvinceID() {
		return LicensePlateProvinceID;
	}

	public void setLicensePlateProvinceID(String licensePlateProvinceID) {
		LicensePlateProvinceID = licensePlateProvinceID;
	}

	public String getLicensePlateImageName() {
		return LicensePlateImageName;
	}

	public void setLicensePlateImageName(String licensePlateImageName) {
		LicensePlateImageName = licensePlateImageName;
	}

	public String getLicensePlateConfidence() {
		return LicensePlateConfidence;
	}

	public void setLicensePlateConfidence(String licensePlateConfidence) {
		LicensePlateConfidence = licensePlateConfidence;
	}

	public String getStatusColor() {
		return StatusColor;
	}

	public void setStatusColor(String statusColor) {
		StatusColor = statusColor;
	}

	public String getProvinceName() {
		return ProvinceName;
	}

	public void setProvinceName(String provinceName) {
		ProvinceName = provinceName;
	}

	public String getStationName() {
		return StationName;
	}

	public void setStationName(String stationName) {
		StationName = stationName;
	}
	
    public String getStationGroupID() {
		return StationGroupID;
	}

	public void setStationGroupID(String stationGroupID) {
		StationGroupID = stationGroupID;
	}
    
    
}
