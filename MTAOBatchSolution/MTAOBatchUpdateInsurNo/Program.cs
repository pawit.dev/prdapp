﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MTAOBatch.Biz;
using System.Data.SqlClient;
using System.Data;
using MTAOBatch.DAO;

namespace MTAOBatchUpdateInsurNo
{
    class Program : IDisposable
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {

            int total = 0;
            int success = 0;

            Console.WriteLine("## Start Run MTAOBatchUpdateInsurNo ##");
            logger.Debug("## Start Run MTAOBatchUpdateInsurNo ##");
            try
            {
                using (MungthaiEntities context = new MungthaiEntities())
                {
                    using (SqlConnection Conn = new SqlConnection(Configurations.BaseMungthai))
                    {
                        Conn.Open();
                        String sqlSumRow = " Select CardSNo as CardSNo,DocuMentNo as DocNo From tbDCusCardS Where    WorkType='999'  and InsurNo is null  and DATALENGTH((LTRIM(RTRIM(DocuMentNo))))=11  order by DocuMentNo";
                        logger.Debug(sqlSumRow + "<br>");
                        SqlDataAdapter daFrom = new SqlDataAdapter(sqlSumRow, Conn);
                        DataSet dsFrom = new DataSet();
                        daFrom.Fill(dsFrom, "tableFrom");
                        DataTable dtFrom = dsFrom.Tables["tableFrom"];
                        if (dtFrom != null)
                        {
                            if (dtFrom.Rows.Count > 0)
                            {
                                total = dtFrom.Rows.Count;
                                for (int j = 0; j <= total - 1; j++)
                                {
                                    String CardSNoFromCusCard = (string)dsFrom.Tables["tableFrom"].Rows[j][0].ToString();
                                    String DocuMentNoFromCusCard = (string)dsFrom.Tables["tableFrom"].Rows[j][1].ToString();
                                    String InsurNoNew = DefaultDAO.GetPolicyNumberByApplicationNumber2(DocuMentNoFromCusCard); //ส่งค่าตัวเเปรไปยัง CallService

                                    if (!InsurNoNew.Split(',')[0].Equals("404"))
                                    {
                                        logger.Debug(j + " DocuMentNoFromCusCard=" + DocuMentNoFromCusCard + " InsurNoNew=" + InsurNoNew + "<br>");
                                        Console.WriteLine(j + " DocuMentNoFromCusCard=" + DocuMentNoFromCusCard + " InsurNoNew=" + InsurNoNew + "<br>");
                                        List<tbDCusCard> tbCC = context.tbDCusCardS.Where(" it.[DocuMentNo]='" + DocuMentNoFromCusCard + "'  ").ToList();

                                        if (tbCC.Count > 0)
                                        {
                                            foreach (tbDCusCard tbdCC in tbCC)
                                            {
                                                tbdCC.CardNo = CardSNoFromCusCard + "";
                                                tbdCC.InsurNo = InsurNoNew + "";
                                                tbdCC.UpdatePolicy = DateTime.Now;
                                                List<tbDCusCard> tbAutoProve = context.tbDCusCardS.Where(" it.[DocuMentNo]='" + DocuMentNoFromCusCard + "' and (it.[FinanceUpDateBy] is null or it.[FinanceUpDateBy] ='')  ").ToList();
                                                if (tbAutoProve.Count > 0)
                                                {
                                                    tbdCC.Status = "1";
                                                    tbdCC.FinanceUpDateBy = "AUTO APPROVE";
                                                    tbdCC.FinanceUpDateDate = DateTime.Now;
                                                }
                                                logger.Debug("InsurNoNew=" + InsurNoNew + " <br>");
                                                Console.WriteLine("InsurNoNew=" + InsurNoNew + " <br>");
                                            }
                                        }
                                        else
                                        {
                                            logger.Debug(String.Format("tbDCusCard size equal 0."));
                                        }
                                    }
                                    else {
                                        logger.Debug(DocuMentNoFromCusCard + ">" + InsurNoNew);
                                        Console.WriteLine(DocuMentNoFromCusCard + ">" + InsurNoNew);
                                    }
                                }
                                //commit.
                                success = context.SaveChanges();
                                Console.WriteLine("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success));
                                logger.Debug(String.Format("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success)));
                            }
                            else
                            {
                                logger.Debug(String.Format("dtFrom size equal 0."));
                            }
                        }
                        else
                        {
                            logger.Debug(String.Format("dtFrom is equal nothing."));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                logger.Error(ex.StackTrace);
            }
            Console.WriteLine("## End Run MTAOBatchUpdateFileOffline ##");
            logger.Debug("## End Run MTAOBatchUpdateInsurNo ##");
            //Console.ReadLine();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}

