﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using MTAOBatch.DAO;
using MTAOBatch.Biz;

namespace MTAOBatchUpdateFileOffline
{
    class Program
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Program));


        static void Main(string[] args)
        {
            Console.WriteLine("## Start Run MTAOBatchUpdateFileOffline ##");
            logger.Debug("## Start Run MTAOBatchUpdateFileOffline ##");
            //::::::::::::::::::::::::::::: UPLOAD :::::::::::::::::::::::::::::


            //DateTime date = (DateTime)DateTime.Now;
            //String filenameH = "MTM";
            //String filenameD = date.Day.ToString("00");
            //String filenameM = date.Month.ToString("00");
            //String fillenameY = date.Year.ToString().Substring(2, 2);
            //String filenameL = "_Offline.TXT";
            //String filename = "";
            //String filePath = filenameH + filenameD + filenameM + fillenameY + filenameL;

            String filePath = String.Format(Configurations.SourcePath, DateTime.Now.ToString("ddMMyy"), "");
            using (StreamReader sr = new StreamReader("" + filePath, Encoding.GetEncoding(874), true, 128))
            {
                using (MungthaiEntities context = new MungthaiEntities())
                {

                    int total = 0;
                    int success = 0;

                    try
                    {
                        String b = "";
                        while (!(sr.EndOfStream))
                        {
                            String line = sr.ReadLine();
                            b += "," + "" + line;
                            //Count total data record
                            if (line[0] == 'D')
                            {
                                total++;
                            }
                        }
                        string v = b.Substring(64);
                        String Tyear = b.Replace(" ", "").Trim().Substring(2, 4);
                        String Tmonth = b.Replace(" ", "").Trim().Substring(6, 2);
                        String Tday = b.Replace(" ", "").Trim().Substring(8, 2);
                        String DatePayment = Tday + "/" + Tmonth + "/" + (Convert.ToUInt32(Tyear) + 543);
                        String srt2 = "";

                        if (v != "")
                        {
                            String[] myArr = v.Split('T');
                            String Coutorder = myArr[1].Substring(0, 8);//จำนวนรายการ
                            String AmountAll = myArr[1].Substring(10, 9);//จำนวนเงินรวม
                            String RoundAll = myArr[1].Substring(19, 2);//จำนวนเงินรวม
                            String[] myArr2 = myArr[0].Split(',');

                            for (int i = 0; i < myArr2.Length; i++)
                            {
                                if (!myArr2[i].Trim().Replace(" ", "").Equals(""))
                                {
                                    srt2 = myArr2[i];
                                    if (srt2.Count() > 0)
                                    {
                                        String paycod = "";
                                        String counterno = "";
                                        String xx = "";
                                        String amount = "0";
                                        String round = "0";
                                        String ryear = "";
                                        String rmonth = "";
                                        String rday = "";

                                        //String datpayR = "";//29
                                        ryear = srt2.Substring(34, 4);
                                        rmonth = srt2.Substring(38, 2);
                                        rday = srt2.Substring(40, 2);
                                        paycod = srt2.Substring(277, 4) + srt2.Substring(302, 8) + "";
                                        amount = srt2.Substring(61, 7);
                                        round = srt2.Substring(68, 2);
                                        round = srt2.Substring(68, 2);
                                        counterno = srt2.Substring(1, 5);
                                        xx = srt2.Substring(2, 5);

                                        if (!String.IsNullOrWhiteSpace(paycod) && !string.IsNullOrWhiteSpace(ryear))
                                        {
                                            List<tbDCusCard> tbDcs = context.tbDCusCardS.Where(" it.[PayInslipCode]='" + paycod + "' ").ToList();

                                            if (tbDcs.Count > 0)
                                            {
                                                foreach (tbDCusCard tbd in tbDcs)
                                                {
                                                    tbd.CounterNo = counterno;
                                                    tbd.PayStatus = "T";
                                                    tbd.PayDate = new DateTime(Convert.ToInt16(ryear) + 543, Convert.ToInt16(rmonth), Convert.ToInt16(rday));
                                                    tbd.CoufirmUpdatBy = "2";
                                                }
                                                if (context.SaveChanges() > 0)
                                                {
                                                    success++;
                                                    logger.Debug("PayInslipCode :" + paycod + " update => Complete.");
                                                }
                                                else {
                                                    logger.Debug("PayInslipCode :" + paycod + " update => Fail.");
                                                }
                                            }
                                            else {
                                                logger.Debug("PayInslipCode :"+paycod +" have no trasaction for update.");
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        
                        Console.WriteLine("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success));
                        logger.Debug(String.Format("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success)));
                        srt2 = "";
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                        logger.Error(ex.StackTrace);
                    }

                }
            }

            Console.WriteLine("## End Run MTAOBatchUpdateFileOffline ##");
            logger.Debug("## End Run MTAOBatchUpdateFileOffline ##");
            //Console.ReadLine();
        }

    }
}
