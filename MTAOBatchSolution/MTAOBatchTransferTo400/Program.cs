﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using MTAOBatch.Biz;
using System.Data.Odbc;
using System.Data;

namespace MTAOBatchTransferTo400
{
    class Program
    {


        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {

            int total = 0;
            int success = 0;

            Console.WriteLine("## Start Run MTAOBatchTransferTo400 ##");
            logger.Debug("## Start Run MTAOBatchTransferTo400 ##");

            DataSet ds = new DataSet();
            try
            {
                //ติดต่อ AO
                using (SqlConnection connBaseMungthai = new SqlConnection(Configurations.BaseMungthai))
                {
                    connBaseMungthai.Open();
                    SqlCommand objCmd = new SqlCommand();
                    string sqlse = " select dc.DocuMentNo as APPNO,dc.InsurNo as POLNO,case when dc.WorkType = '999' then 'N'  else 'R' end as PERIOD,mc.SaveDate,dc.UpdatePolicy  " +
                                    "  from tbDCusCardS dc  " +
                                     "  inner join tbMCardS mc on dc.CardSNo = mc.CardSNo  where dc.InsurNo != ''  and  (dc.SendToAS400 is null  or dc.SendToAS400 ='')  order by dc.InsurNo  ";
                    SqlDataAdapter da = new SqlDataAdapter(sqlse, connBaseMungthai);
                    da.Fill(ds, "tableTo");

                    if (ds != null)
                    {
                        if (ds.Tables["tableTo"].Rows.Count > 0)
                        {
                            total = ds.Tables["tableTo"].Rows.Count;
                            Console.WriteLine(" prepare data is " + total + " reccords.");
                            logger.Debug(" prepare data is " + total + " reccords.");
                            //ติดต่อ AS400
                            using (OdbcConnection connAs400 = new OdbcConnection(Configurations.As400ConStr))
                            {
                                connAs400.Open();

                                for (int j = 0; j <= total - 1; j++)
                                {
                                    //insert เข้า AS400
                                    String APPNO = (string)ds.Tables["tableTo"].Rows[j][0].ToString();
                                    String POLNO = (string)ds.Tables["tableTo"].Rows[j][1].ToString();
                                    String PERIOD = (string)ds.Tables["tableTo"].Rows[j][2].ToString();

                                    string SaveDate = string.Format("{0:yyMMdd}", ds.Tables["tableTo"].Rows[j][3]).ToString();
                                    int savedate2 = Convert.ToInt32(SaveDate);


                                    string TranDate = DateTime.Now.ToString("yyMMdd", new System.Globalization.CultureInfo("th-TH"));
                                    int TranDate2 = Convert.ToInt32(TranDate);
                                    string sqlsm = "";
                                    String getUpdateDate = (string)ds.Tables["tableTo"].Rows[j][4].ToString();
                                    if (getUpdateDate.Trim().Length == 0)
                                    {
                                        sqlsm = " INSERT INTO MTDTALIB.AGJFYC02 ( APPNO,POLNO,PERIOD,KEYDTE,TRFDTE)" +
                                      " VALUES ('" + APPNO + "','" + POLNO + "','" + PERIOD + "','" + savedate2 + "','" + TranDate2 + "' )";

                                    }
                                    else
                                    {
                                        string UpdateDate = Convert.ToDateTime(ds.Tables["tableTo"].Rows[j][4]).ToString("yyMMdd");
                                        sqlsm = " INSERT INTO MTDTALIB.AGJFYC02 ( APPNO,POLNO,PERIOD,KEYDTE,TRFDTE,UPDDTE) " +
                                      " VALUES ('" + APPNO + "','" + POLNO + "','" + PERIOD + "','" + savedate2 + "','" + TranDate2 + "' ,'" + UpdateDate + "')";

                                    }
                                    logger.Debug(sqlsm);

                                    OdbcCommand cmd = new OdbcCommand(sqlsm, connAs400);
                                    cmd.CommandType = CommandType.Text;
                                    cmd.CommandText = sqlsm;
                                    int result = cmd.ExecuteNonQuery();

                                    if (result > 0)
                                    {
                                        // กรณี  insert AS400 ได้แล้ว ก็ Update ที่ tbDCusCards ว่า มีการส่งไปแล้ว
                                        String strSQL1 = " update tbDCusCardS set SendToAS400= '" + "complete" + "' where  InsurNo= '" + POLNO + "' ";

                                        cmd = new OdbcCommand(strSQL1, connAs400);
                                        cmd.CommandType = CommandType.Text;
                                        cmd.CommandText = sqlsm;
                                        result = cmd.ExecuteNonQuery();
                                        if (result > 0)
                                        {
                                            success++;
                                        }
                                    }

                                }
                                Console.WriteLine("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success));
                                logger.Debug(String.Format("## Operation total {0} records, success {1} records , fail {2} records ##", total, success, (total - success)));
                            }
                        }
                        else
                        {
                            logger.Debug(String.Format("ds size equal 0."));
                        }
                    }
                    else
                    {
                        logger.Debug(String.Format("ds size equal nothing."));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                logger.Error(ex.StackTrace);
            }

            Console.WriteLine("## End Run MTAOBatchTransferTo400 ##");
            logger.Debug("## End Run MTAOBatchTransferTo400 ##");
            //Console.ReadLine();
        }
    }
}
