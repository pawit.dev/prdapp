﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace MTAOBatch.Biz
{
    public class Configurations
    {
        public static String As400ConStr
        {
            get { return ConfigurationManager.ConnectionStrings["As400ConStr"].ConnectionString; }
        }

        public static String BaseMungthai
        {
            get { return ConfigurationManager.ConnectionStrings["BaseMungthai"].ConnectionString; }
        }
        public static String BaseMungthai2
        {
            get { return ConfigurationManager.ConnectionStrings["BaseMungthai2"].ConnectionString; }
        }


    }
}
