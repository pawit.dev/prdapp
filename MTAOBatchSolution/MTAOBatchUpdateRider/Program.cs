﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MTAOBatch.DAO;
using System.Data.SqlClient;
using MTAOBatch.Biz;
using System.Data;

namespace MTAOBatchUpdateRider
{
    class Program : IDisposable
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            
            Console.WriteLine("## Start Run MTAOBatchUpdateInsurNo ##");
            logger.Debug("## Start Run MTAOBatchUpdateInsurNo ##");
            try
            {
                using (MungthaiEntities context = new MungthaiEntities())
                {
                    using (SqlConnection Conn = new SqlConnection(Configurations.BaseMungthai))
                    {
                        Conn.Open();
                        using (SqlConnection Conn2 = new SqlConnection(Configurations.BaseMungthai2))
                        {
                            Conn2.Open();

                            //Rider
                            String sqlFrom = " select PLAN_CODE,PLAN_NAME,status_plan from(select distinct (plan_code)as PLAN_CODE ,PLAN_ROLE_NAME as PLAN_NAME,status as status_plan  from TB_MT_PLAN_ROLE where   role_id in ('4','6','11') " +
                                             "  union " +
                                             " select distinct (PACKAGE_ID)as PLAN_CODE,PACKAGE_ROLE_NAME as PLAN_NAME,status as status_plan from TB_MT_PACKAGE_ROLE)newTB order by PLAN_NAME ";
                            SqlDataAdapter daFrom = new SqlDataAdapter(sqlFrom, Conn2);
                            DataSet dsFrom = new DataSet();
                            daFrom.Fill(dsFrom, "tableFrom");
                            DataTable dtFrom = dsFrom.Tables["tableFrom"];

                            if (dtFrom != null)
                            {
                                if (dtFrom.Rows.Count > 0)
                                {

                                    for (int j = 0; j <= dsFrom.Tables["tableFrom"].Rows.Count - 1; j++)
                                    {
                                        String sqlTo = " select  PolicyID, PolicyCode,PolicyName  from  tbTempPolicy where PolicyCode='" + dtFrom.Rows[j][0] + "' ";
                                        SqlDataAdapter daTo = new SqlDataAdapter(sqlTo, Conn);
                                        DataSet dsTo = new DataSet();
                                        daTo.Fill(dsTo, "tableTo");
                                        DataTable dtTo = dsTo.Tables["tableTo"];
                                        if (dsTo.Tables["tableTo"].Rows.Count == 0)
                                        {
                                            String strSQL2 = "insert into tbTempPolicy(PolicyCode,PolicyName,StatusPlan) values('" + dtFrom.Rows[j][0] + "','" + dtFrom.Rows[j][1] + "','" + dtFrom.Rows[j][2] + "') ";

                                            tbTempPolicy tp = new tbTempPolicy();
                                            tp.PolicyCode = dtFrom.Rows[j][0] + "";
                                            tp.PolicyName = dtFrom.Rows[j][1] + "";
                                            tp.StatusPlan = dtFrom.Rows[j][2] + "";
                                            context.AddTotbTempPolicies(tp);


                                            logger.Debug(" insert into tbTempPolicy(PolicyCode,PolicyName) values('" + dtFrom.Rows[j][0] + "','" + dtFrom.Rows[j][1] + "') <br>");
                                            Console.WriteLine(" insert into tbTempPolicy(PolicyCode,PolicyName) values('" + dtFrom.Rows[j][0] + "','" + dtFrom.Rows[j][1] + "') <br>");
                                        }
                                        else
                                        {
                                            if (dsTo.Tables["tableTo"].Rows.Count == 1)
                                            {
                                                String strSQL1 = " update tbTempPolicy set PolicyName=" + dtFrom.Rows[j][1] + ",StatusPlan=" + dtFrom.Rows[j][2] + " where  PolicyCode='" + dtTo.Rows[0][1] + "' ";

                                                List<tbTempPolicy> tbDcs = context.tbTempPolicies.Where(" it.[PolicyCode]='" + dtTo.Rows[0][1] + "' ").ToList();

                                                if (tbDcs.Count > 0)
                                                {
                                                    foreach (tbTempPolicy tbd in tbDcs)
                                                    {
                                                        tbd.PolicyCode = dtTo.Rows[0][1] + "";
                                                        tbd.PolicyName = dtFrom.Rows[j][1] + "";
                                                        tbd.StatusPlan = dtFrom.Rows[j][2] + "";
                                                    }
                                                }

                                            }
                                        }
                                    }
                                    //save change.
                                    context.SaveChanges();
                                }
                                else
                                {
                                    logger.Debug("dtForm size equal 0.");
                                }
                            }
                            else
                            {
                                logger.Debug("dtForm is nothing.");
                            }

                            String sqlPolicyFrom = " select  distinct rider_role_name,rider_code  , " +
                                                  "  case when SUBSTRING( rider_role_name, 1,4)  ='แผน '  " +
                                                  "  or SUBSTRING( rider_role_name, 1,6)  ='วีไอพี '  " +
                                                  "  or SUBSTRING( rider_role_name, 1,8)  ='เหมาจ่าย  ' then ( " +
                                                  "   select case when tb_mt_rider.RIDER_TYPE_ID ='HS'  " +
                                                  "   then tb_mt_rider.rider_name else  (select RIDER_TYPE_NAME from TB_MT_RIDER_type  " +
                                                   "  where RIDER_TYPE_ID =tb_mt_rider.RIDER_TYPE_ID ) end " +
                                                  "   from tb_mt_rider  where RIDER_CODE = tb_mt_plan_role_rider.RIDER_CODE " +
                                                 "   )+' '+ rider_role_name  else RIDER_ROLE_NAME end as RIDER_ROLE_NAME   " +
                                                   " from tb_mt_plan_role_rider order by    tb_mt_plan_role_rider.rider_role_name  ";

                            SqlDataAdapter daPolicyFrom = new SqlDataAdapter(sqlPolicyFrom, Conn2);
                            DataSet dsPolicyFrom = new DataSet();
                            daPolicyFrom.Fill(dsPolicyFrom, "tableExPolicyFrom");
                            DataTable dtPolicyFrom = dsPolicyFrom.Tables["tableExPolicyFrom"];

                            if (dtPolicyFrom != null)
                            {
                                if (dtPolicyFrom.Rows.Count > 0)
                                {
                                    for (int j = 0; j <= dsPolicyFrom.Tables["tableExPolicyFrom"].Rows.Count - 1; j++)
                                    {
                                        String sqlPolicyTo = " select  ExPolicyID, ExPolicyCode,ExPolicyName  from  tbTempExtraPolicy where ExPolicyCode='" + dtPolicyFrom.Rows[j][1] + "'  ";
                                        SqlDataAdapter daPolicyTo = new SqlDataAdapter(sqlPolicyTo, Conn);
                                        DataSet dsPolicyTo = new DataSet();
                                        daPolicyTo.Fill(dsPolicyTo, "tableExPolicyTo");
                                        DataTable dtPolicyTo = dsPolicyTo.Tables["tableExPolicyTo"];
                                        if (dsPolicyTo.Tables["tableExPolicyTo"].Rows.Count == 0)
                                        {
                                            String strPolicySQL2 = "insert into tbTempExtraPolicy(ExPolicyCode,ExPolicyName) values('" + dtPolicyFrom.Rows[j][1] + "','" + dtPolicyFrom.Rows[j][2] + "') ";

                                            tbTempExtraPolicy etp = new tbTempExtraPolicy();
                                            etp.ExPolicyCode = dtPolicyFrom.Rows[j][1] + "";
                                            etp.ExPolicyName = dtPolicyFrom.Rows[j][2] + "";
                                            context.AddTotbTempExtraPolicies(etp);


                                        }
                                        else if (dsPolicyTo.Tables["tableExPolicyTo"].Rows.Count == 1)
                                        {
                                            String strPolicySQL1 = " update tbTempExtraPolicy set ExPolicyName=" + dtPolicyFrom.Rows[j][2] + " where  ExPolicyCode='" + dtPolicyTo.Rows[0][1] + "'";

                                            List<tbTempExtraPolicy> tbExPo = context.tbTempExtraPolicies.Where(" it.[ExPolicyCode]='" + dtPolicyTo.Rows[0][1] + "' ").ToList();

                                            if (tbExPo.Count > 0)
                                            {
                                                foreach (tbTempExtraPolicy tbExP in tbExPo)
                                                {
                                                    tbExP.ExPolicyCode = dtPolicyTo.Rows[0][1] + "";
                                                    tbExP.ExPolicyName = dtPolicyFrom.Rows[j][2] + "";
                                                }

                                            }
                                            logger.Debug(" update tbTempExtraPolicy set ExPolicyName='" + dtPolicyFrom.Rows[j][2] + "' where  ExPolicyCode='" + dtPolicyTo.Rows[0][1] + "' <br>");
                                            Console.WriteLine(" update tbTempExtraPolicy set ExPolicyName='" + dtPolicyFrom.Rows[j][2] + "' where  ExPolicyCode='" + dtPolicyTo.Rows[0][1] + "' <br>");
                                        }
                                    }
                                    //commit.
                                    context.SaveChanges();
                                }
                                else
                                {
                                    logger.Debug("dtPolicyFrom size equal 0.");
                                }
                            }
                            else
                            {
                                logger.Debug("dtPolicyFrom is nothing.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                logger.Error(ex.StackTrace);
            }
            Console.WriteLine("## End Run MTAOBatchUpdateFileOffline ##");
            logger.Debug("## End Run MTAOBatchUpdateInsurNo ##");
            //Console.ReadLine();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
