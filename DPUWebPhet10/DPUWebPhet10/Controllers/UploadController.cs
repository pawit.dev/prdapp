﻿using System;
using System.Web.Mvc;
using DPUWebPhet10.Models;
using System.Data;
using System.Web.Security;

namespace DPUWebPhet10.Controllers
{
    public class UploadController : Controller
    {
        private ChinaPhet10Entities db = new ChinaPhet10Entities();
        private IFileStore _fileStore = new DiskFileStore();
        //
        // GET: /Upload/
        public ActionResult Index()
        {

            if (Session["Phet10School"] == null)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("../");
            }

            TB_APPLICATION_SCHOOL tb_application_school = (TB_APPLICATION_SCHOOL)Session["Phet10School"];

            ViewBag.PageContent = "ส่งเอกสารยืนยันการสมัคร";
            var model = new UploadModel
            {
                school = tb_application_school
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Upload(UploadModel model)
        {
            //if (ModelState.IsValid)
            //{
                try
                {

                    if (Request.Files["photo"] != null) // If uploaded synchronously
                    {
                        model.photo_guid = _fileStore.SaveUploadedFile(Request.Files["photo"]);
                        model.photo_filename = Request.Files["photo"].FileName;
                    }

                    TB_SCHOOL_DOCUMENT schoolDocument = new TB_SCHOOL_DOCUMENT();
                    schoolDocument.SCHOOL_ID = model.school.SCHOOL_ID;
                    schoolDocument.DOCUMENT_PATH = model.photo_filename;
                    db.TB_SCHOOL_DOCUMENT.AddObject(schoolDocument);

                    model.school.SCHOOL_DOC_PATH = model.photo_filename;//model.photo_guid.Value.ToString() + new FileInfo(model.photo_filename).Extension;
                    model.school.SCHOOL_APPROVED_STATUS = 2;
                    db.TB_APPLICATION_SCHOOL.Attach(model.school);
                    db.ObjectStateManager.ChangeObjectState(model.school, EntityState.Modified);


                    db.SaveChanges();
                    return RedirectToAction("../School/Index");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    ModelState.AddModelError("", "ยังไม่ได้เลือกไฟล์");
                }
            //}
            ViewBag.PageContent = "ส่งเอกสารยืนยันการสมัคร";
            return View("Index",model);
        }

        public Guid AsyncUpload()
        {
            return _fileStore.SaveUploadedFile(Request.Files[0]);
        }

    }
}
