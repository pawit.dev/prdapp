﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DPUWebPhet10.Models;
using System.Globalization;
using System.Data;
using DPUWebPhet10.Models.Common;
using System.Data.Objects;
using DPUWebPhet10.Utils;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using PagedList;
namespace DPUWebPhet10.Controllers
{
    public class ManagementController : Controller
    {
        private ChinaPhet10Entities db = new ChinaPhet10Entities();

        public ActionResult Index()
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            return View();
        }


        //[HttpPost]
        public ActionResult SchoolList(ManagementModel model)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }

            List<TB_APPLICATION_SCHOOL> tmpSchoolList = new List<TB_APPLICATION_SCHOOL>();
            List<TB_APPLICATION_SCHOOL> schoolList = new List<TB_APPLICATION_SCHOOL>();


            if (model.startDate == null && model.endDate == null)
            {
                String _startDate = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                String _endDate = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));

                DateTime dt = DateTime.Now;
                var tb_application_school = from a in db.TB_APPLICATION_SCHOOL
                                            where EntityFunctions.TruncateTime(a.SCHOOL_REGISTER_DATE) == dt.Date
                                            select a;
                model.startDate = _startDate;
                model.endDate = _endDate;
                schoolList = tb_application_school.ToList();
            }
            else
            {
                int startDate = Convert.ToInt32(CommonUtils.toDate(model.startDate).ToString("yyyyMMdd", new CultureInfo("th-TH")));
                int endDate = Convert.ToInt32(CommonUtils.toDate(model.endDate).ToString("yyyyMMdd", new CultureInfo("th-TH")));

                if (!String.IsNullOrWhiteSpace(model.schoolName))
                {
                    var school = from a in db.TB_APPLICATION_SCHOOL where a.SCHOOL_NAME.Contains(model.schoolName) select a;
                    if (!String.IsNullOrWhiteSpace(model.approveStatus))
                    {
                        int approvedStatus = Convert.ToInt16(model.approveStatus);
                        tmpSchoolList = school.Where(t => t.SCHOOL_APPROVED_STATUS == approvedStatus).ToList();
                    }
                    else
                    {
                        tmpSchoolList = school.ToList();
                    }
                }
                else
                {
                    var school = from a in db.TB_APPLICATION_SCHOOL select a;
                    if (!String.IsNullOrWhiteSpace(model.approveStatus))
                    {
                        int approvedStatus = Convert.ToInt16(model.approveStatus);
                        tmpSchoolList = school.Where(t => t.SCHOOL_APPROVED_STATUS == approvedStatus).ToList();
                    }
                    else
                    {
                        tmpSchoolList = school.ToList();
                    }
                }

                StringBuilder x = new StringBuilder();
                foreach (TB_APPLICATION_SCHOOL school in tmpSchoolList)
                {
                    int curDate = Convert.ToInt32(school.SCHOOL_REGISTER_DATE.Value.ToString("yyyyMMdd", new CultureInfo("en-US")));
                    if (curDate >= startDate && curDate <= endDate)
                    {
                        x.Append("|" + curDate + "," + startDate + "," + endDate + "|");
                        schoolList.Add(school);
                    }
                }
            }
            List<TB_M_STATUS> tbMStatus = db.TB_M_STATUS.Where(s => s.STATUS_GROUP == 1).ToList();
            ViewBag.approveStatus = new SelectList(tbMStatus, "STATUS_ID", "STATUS_NAME_TH", model.approveStatus);
            if (schoolList != null)
            {
                var pageIndex = model.Page ?? 1;
                model.schools = schoolList.ToPagedList(pageIndex, 15);
            }
            return View(model);
        }

        public ActionResult EditSchool(int id = 0)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }

            TB_APPLICATION_SCHOOL tb_application_school = db.TB_APPLICATION_SCHOOL.Single(t => t.SCHOOL_ID == id);

            /*
             * TITLE PROVINCE
             */
            IList<TB_M_PROVINCE> mProvinceLists = db.TB_M_PROVINCE.ToList<TB_M_PROVINCE>();
            IEnumerable<SelectListItem> provinceLists =
            from s in mProvinceLists
            select new SelectListItem
            {
                Text = s.PROVINCE_NAME,
                Value = s.PROVINCE_ID.ToString()
            };

            /*
             * TITLE AMPHUR
             */
            IList<TB_M_AMPHUR> mAmphur = db.TB_M_AMPHUR.ToList<TB_M_AMPHUR>();
            IEnumerable<SelectListItem> amPhurList =
            from s in mAmphur
            select new SelectListItem
            {
                Text = s.AMPHUR_NAME,
                Value = s.AMPHUR_ID.ToString()
            };

            /*
             * TITLE TUMBON
             */
            IList<TB_M_DISTRICT> mTumbon = db.TB_M_DISTRICT.ToList<TB_M_DISTRICT>();
            IEnumerable<SelectListItem> tumbonLists =
            from s in mTumbon
            select new SelectListItem
            {
                Text = s.DISTRICT_NAME,
                Value = s.DISTRICT_ID.ToString()
            };

            /*
             * SCHOOL TYPE
             */
            List<RadioButtonModel> list = new List<RadioButtonModel>();

            list.Add(new RadioButtonModel() { ID = 1, Name = Resources.Application.Application.SCHOOL_TYPE_01 });//สพฐ
            list.Add(new RadioButtonModel() { ID = 2, Name = Resources.Application.Application.SCHOOL_TYPE_02 });//เอกชน
            list.Add(new RadioButtonModel() { ID = 3, Name = Resources.Application.Application.SCHOOL_TYPE_03 });//กทม
            list.Add(new RadioButtonModel() { ID = 4, Name = Resources.Application.Application.SCHOOL_TYPE_04 });//กทม
            list.Add(new RadioButtonModel() { ID = 5, Name = Resources.Application.Application.SCHOOL_TYPE_OTHER });//กทม
            SelectList schoolTypes = new SelectList(list, "ID", "Name");

            /*
             * STAFF
             */
            var tb_application_staff = from a in db.TB_APPLICATION_STAFF where a.STAFF_SCHOOL_ID == id select a;
            /*
             * STUDENT
             */
            var tb_application_student = from a in db.TB_APPLICATION_STUDENT where a.STD_SCHOOL_ID == id select a;

            var model = new ApplicationModel()
            {
                idSelectedSchoolType = tb_application_school.SCHOOL_TYPE + "",
                SCHOOL_ADDR_PROVINCE = tb_application_school.SCHOOL_ADDR_PROVINCE + "",
                SCHOOL_ADDR_AMPHUR = tb_application_school.SCHOOL_ADDR_AMPHUR + "",
                SCHOOL_ADDR_TOMBON = tb_application_school.SCHOOL_ADDR_TOMBON + "",
                provinceLists = provinceLists,
                tumbonLists = tumbonLists,
                amphurLists = amPhurList,
                rSchoolTypes = schoolTypes,
                school = tb_application_school,
                Staffs = tb_application_staff.ToList(),
                Students = tb_application_student.ToList()
            };

            return View(model);
        }

        public ActionResult DetailSchool(int id = 0)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }

            TB_APPLICATION_SCHOOL tb_application_school = db.TB_APPLICATION_SCHOOL.Single(t => t.SCHOOL_ID == id);

            /*
             * TITLE PROVINCE
             */
            IList<TB_M_PROVINCE> mProvinceLists = db.TB_M_PROVINCE.ToList<TB_M_PROVINCE>();
            IEnumerable<SelectListItem> provinceLists =
            from s in mProvinceLists
            select new SelectListItem
            {
                Text = s.PROVINCE_NAME,
                Value = s.PROVINCE_ID.ToString()
            };

            /*
             * TITLE AMPHUR
             */
            IList<TB_M_AMPHUR> mAmphur = db.TB_M_AMPHUR.ToList<TB_M_AMPHUR>();
            IEnumerable<SelectListItem> amPhurList =
            from s in mAmphur
            select new SelectListItem
            {
                Text = s.AMPHUR_NAME,
                Value = s.AMPHUR_ID.ToString()
            };

            /*
             * TITLE TUMBON
             */
            IList<TB_M_DISTRICT> mTumbon = db.TB_M_DISTRICT.ToList<TB_M_DISTRICT>();
            IEnumerable<SelectListItem> tumbonLists =
            from s in mTumbon
            select new SelectListItem
            {
                Text = s.DISTRICT_NAME,
                Value = s.DISTRICT_ID.ToString()
            };

            /*
             * SCHOOL TYPE
             */
            List<RadioButtonModel> list = new List<RadioButtonModel>();

            list.Add(new RadioButtonModel() { ID = 1, Name = Resources.Application.Application.SCHOOL_TYPE_01 });//สพฐ
            list.Add(new RadioButtonModel() { ID = 2, Name = Resources.Application.Application.SCHOOL_TYPE_02 });//เอกชน
            list.Add(new RadioButtonModel() { ID = 3, Name = Resources.Application.Application.SCHOOL_TYPE_03 });//กทม
            list.Add(new RadioButtonModel() { ID = 4, Name = Resources.Application.Application.SCHOOL_TYPE_04 });//กทม
            list.Add(new RadioButtonModel() { ID = 5, Name = Resources.Application.Application.SCHOOL_TYPE_OTHER });//กทม
            SelectList schoolTypes = new SelectList(list, "ID", "Name");

            /*
             * STAFF
             */
            var tb_application_staff = from a in db.TB_APPLICATION_STAFF where a.STAFF_SCHOOL_ID == id select a;
            /*
             * STUDENT
             */
            var tb_application_student = from a in db.TB_APPLICATION_STUDENT where a.STD_SCHOOL_ID == id select a;

            var model = new ApplicationModel()
            {
                idSelectedSchoolType = tb_application_school.SCHOOL_TYPE + "",
                SCHOOL_ADDR_PROVINCE = tb_application_school.SCHOOL_ADDR_PROVINCE + "",
                SCHOOL_ADDR_AMPHUR = tb_application_school.SCHOOL_ADDR_AMPHUR + "",
                SCHOOL_ADDR_TOMBON = tb_application_school.SCHOOL_ADDR_TOMBON + "",
                provinceLists = provinceLists,
                tumbonLists = tumbonLists,
                amphurLists = amPhurList,
                rSchoolTypes = schoolTypes,
                school = tb_application_school,
                Staffs = tb_application_staff.ToList(),
                Students = tb_application_student.ToList()
            };
            ViewBag.SCHOOL_APPROVED_STATUS = new SelectList(db.TB_M_STATUS, "STATUS_ID", "STATUS_NAME_TH", tb_application_school.SCHOOL_APPROVED_STATUS);
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditSchool([Bind(Exclude = "school_SCHOOL_EMAIL,school_SCHOOL_NAME")]ApplicationModel model)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            if (!ModelState.IsValid)
            {
                /*
                 * TITLE PROVINCE
                 */
                IList<TB_M_PROVINCE> mProvinceLists = db.TB_M_PROVINCE.ToList<TB_M_PROVINCE>();
                IEnumerable<SelectListItem> provinceLists =
                from s in mProvinceLists
                select new SelectListItem
                {
                    Text = s.PROVINCE_NAME,
                    Value = s.PROVINCE_ID.ToString()
                };
                /*
                 * TITLE AMPHUR
                 */
                IList<TB_M_AMPHUR> mAmphur = db.TB_M_AMPHUR.ToList<TB_M_AMPHUR>();
                IEnumerable<SelectListItem> amPhurList =
                from s in mAmphur
                select new SelectListItem
                {
                    Text = s.AMPHUR_NAME,
                    Value = s.AMPHUR_ID.ToString()
                };

                /*
                 * TITLE TUMBON
                 */
                IList<TB_M_DISTRICT> mTumbon = db.TB_M_DISTRICT.ToList<TB_M_DISTRICT>();
                IEnumerable<SelectListItem> tumbonLists =
                from s in mTumbon
                select new SelectListItem
                {
                    Text = s.DISTRICT_NAME,
                    Value = s.DISTRICT_ID.ToString()
                };


                /*
                 * SCHOOL TYPE
                 */
                List<RadioButtonModel> list = new List<RadioButtonModel>();

                list.Add(new RadioButtonModel() { ID = 1, Name = Resources.Application.Application.SCHOOL_TYPE_01 });//สพฐ
                list.Add(new RadioButtonModel() { ID = 2, Name = Resources.Application.Application.SCHOOL_TYPE_02 });//เอกชน
                list.Add(new RadioButtonModel() { ID = 3, Name = Resources.Application.Application.SCHOOL_TYPE_03 });//กทม
                list.Add(new RadioButtonModel() { ID = 4, Name = Resources.Application.Application.SCHOOL_TYPE_04 });//อุดมศึกษา
                list.Add(new RadioButtonModel() { ID = 5, Name = Resources.Application.Application.SCHOOL_TYPE_OTHER });//อื่น ๆ 

                SelectList schoolTypes = new SelectList(list, "ID", "Name");
                model.provinceLists = provinceLists;
                model.amphurLists = amPhurList;
                model.tumbonLists = tumbonLists;
                model.rSchoolTypes = schoolTypes;
                return View(model);
            }


            using (ChinaPhet10Entities context = new ChinaPhet10Entities())
            {
                if (context.Connection.State != ConnectionState.Open)
                    context.Connection.Open();
                using (var transaction = context.Connection.BeginTransaction())
                {
                    try
                    {

                        /*
                         SCHOOL MODEL
                         */
                        //VALIDATE NULL VALUE
                        model.school.SCHOOL_ADDR_PROVINCE = (model.SCHOOL_ADDR_PROVINCE == null) ? -1 : Convert.ToInt32(model.SCHOOL_ADDR_PROVINCE);
                        model.school.SCHOOL_ADDR_AMPHUR = (model.SCHOOL_ADDR_AMPHUR == null) ? -1 : Convert.ToInt32(model.SCHOOL_ADDR_AMPHUR);
                        model.school.SCHOOL_ADDR_TOMBON = (model.SCHOOL_ADDR_TOMBON == null) ? -1 : Convert.ToInt32(model.SCHOOL_ADDR_TOMBON);
                        model.school.SCHOOL_ZONE_EDU = (model.school.SCHOOL_ZONE_EDU == null) ? "" : model.school.SCHOOL_ZONE_EDU;
                        model.school.SCHOOL_ZONE = (model.school.SCHOOL_ZONE == null) ? "" : model.school.SCHOOL_ZONE;
                        model.school.SCHOOL_TYPE_OTHER = (model.school.SCHOOL_TYPE_OTHER == null) ? "" : model.school.SCHOOL_TYPE_OTHER;
                        model.school.SCHOOL_ADDR = (model.school.SCHOOL_ADDR == null) ? "" : model.school.SCHOOL_ADDR;
                        model.school.SCHOOL_ADDR_SOI = (model.school.SCHOOL_ADDR_SOI == null) ? "" : model.school.SCHOOL_ADDR_SOI;
                        model.school.SCHOOL_ADDR_ROAD = (model.school.SCHOOL_ADDR_ROAD == null) ? "" : model.school.SCHOOL_ADDR_ROAD;
                        model.school.SCHOOL_ADDR_ZIPCODE = (model.school.SCHOOL_ADDR_ZIPCODE == null) ? "" : model.school.SCHOOL_ADDR_ZIPCODE;
                        model.school.SCHOOL_ADDR_PHONE = (model.school.SCHOOL_ADDR_PHONE == null) ? "" : model.school.SCHOOL_ADDR_PHONE;
                        model.school.SCHOOL_ADDR_FAX = (model.school.SCHOOL_ADDR_FAX == null) ? "" : model.school.SCHOOL_ADDR_FAX;
                        model.school.SCHOOL_NAME = (model.school.SCHOOL_NAME == null) ? "" : model.school.SCHOOL_NAME;
                        model.school.SCHOOL_PROVINCE = (model.school.SCHOOL_PROVINCE == null) ? -1 : model.school.SCHOOL_PROVINCE;
                        model.school.SCHOOL_PASSWORD = (model.school.SCHOOL_PASSWORD == null) ? "" : model.school.SCHOOL_PASSWORD;
                        model.school.SCHOOL_DOC_PATH = (model.school.SCHOOL_DOC_PATH == null) ? "" : model.school.SCHOOL_DOC_PATH;
                        model.school.SCHOOL_REGISTER_DATE = (model.school.SCHOOL_REGISTER_DATE == null) ? DateTime.Now : model.school.SCHOOL_REGISTER_DATE;


                        model.school.SCHOOL_TYPE = model.idSelectedSchoolType;

                        context.TB_APPLICATION_SCHOOL.Attach(model.school);
                        context.ObjectStateManager.ChangeObjectState(model.school, System.Data.EntityState.Modified);

                        /*
                         STAFF
                         */
                        if (model.Staffs != null)
                        {

                            /*
                             * INSERT STAFF
                             */
                            foreach (TB_APPLICATION_STAFF staff in model.Staffs)
                            {
                                staff.STAFF_SCHOOL_ID = model.school.SCHOOL_ID;
                                staff.STAFF_FOR_LEVEL = 0;
                                if (staff.STAFF_ID > 0)
                                {
                                    //TB_APPLICATION_STAFF staff = context.TB_APPLICATION_STAFF.Where(s => s.STAFF_ID == _staff.STAFF_ID).FirstOrDefault();
                                    //update
                                    context.TB_APPLICATION_STAFF.Attach(staff);
                                    context.ObjectStateManager.ChangeObjectState(staff, System.Data.EntityState.Modified);
                                }
                                else
                                {

                                    //insert
                                    context.TB_APPLICATION_STAFF.AddObject(staff);
                                }

                            }
                        }
                        /*
                         STUDENT
                         */
                        if (model.Students != null)
                        {
                            /*
                             * INSERT STAFF
                             */
                            foreach (TB_APPLICATION_STUDENT student in model.Students)
                            {
                                student.STD_SCHOOL_ID = model.school.SCHOOL_ID;
                                student.STD_IS_CONCERN = "0";
                                #region "CONCERN STUDENT."
                                bool isValidLevel = false;
                                int currentYear = DateTime.Now.Year;
                                if (!String.IsNullOrEmpty(student.STD_BIRTH_DAY))
                                {
                                    currentYear = currentYear - Convert.ToInt16(student.STD_BIRTH_DAY.Split('-')[0]);
                                }

                                if (currentYear <= 9)
                                {
                                    if (student.STD_LEVEL_ID == 1)
                                    {
                                        isValidLevel = true; ;
                                    }
                                }
                                else if (currentYear >= 10 && currentYear <= 12)
                                {
                                    if (student.STD_LEVEL_ID == 2) isValidLevel = true;
                                }
                                else if (currentYear >= 13 && currentYear <= 15)
                                {
                                    if (student.STD_LEVEL_ID == 3) isValidLevel = true;
                                }
                                else if (currentYear >= 16 && currentYear <= 18)
                                {
                                    if (student.STD_LEVEL_ID == 4) isValidLevel = true;
                                }
                                else
                                {
                                    if (student.STD_LEVEL_ID == 5) isValidLevel = true;
                                }
                                if (isValidLevel == false)
                                {
                                    student.STD_IS_CONCERN = "1";
                                }
                                #endregion
                                if (student.STD_ID > 0)
                                {
                                    //update
                                    context.TB_APPLICATION_STUDENT.Attach(student);
                                    context.ObjectStateManager.ChangeObjectState(student, System.Data.EntityState.Modified);
                                }
                                else
                                {

                                    //insert
                                    student.STD_APPROVED_STATUS = 1;
                                    context.TB_APPLICATION_STUDENT.AddObject(student);
                                }
                            }
                        }
                        context.SaveChanges();
                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.InnerException.Message);
                    }
                }
            }



            return View("EditComplete");

        }

        public ActionResult Approve(int id = 0)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            TB_APPLICATION_SCHOOL tb_application_school = db.TB_APPLICATION_SCHOOL.Single(t => t.SCHOOL_ID == id);
            if (tb_application_school == null)
            {
                return HttpNotFound();
            }
            List<TB_M_STATUS> tbMStatus = db.TB_M_STATUS.Where(s => s.STATUS_GROUP == 1).ToList();
            ViewBag.SCHOOL_APPROVED_STATUS = new SelectList(tbMStatus, "STATUS_ID", "STATUS_NAME_TH", tb_application_school.SCHOOL_APPROVED_STATUS);
            return View(tb_application_school);
        }

        [HttpPost]
        public ActionResult Approve(TB_APPLICATION_SCHOOL tb_application_school)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }

            if (tb_application_school.SCHOOL_APPROVED_STATUS == 3)
            {
                List<TB_APPLICATION_STUDENT> studentLists = db.TB_APPLICATION_STUDENT.Where(s => s.STD_SCHOOL_ID == tb_application_school.SCHOOL_ID).ToList();
                if (studentLists != null)
                {
                    foreach (TB_APPLICATION_STUDENT student in studentLists)
                    {
                        student.STD_APPROVED_STATUS = 3;
                        db.ObjectStateManager.ChangeObjectState(student, EntityState.Modified);
                    }
                }
            }
            db.TB_APPLICATION_SCHOOL.Attach(tb_application_school);
            db.ObjectStateManager.ChangeObjectState(tb_application_school, EntityState.Modified);
            db.SaveChanges();

            return RedirectToAction("SchoolList/" + tb_application_school.SCHOOL_ID);

        }

        public ActionResult ApproveStudentList(int id = 0)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }

            List<TB_APPLICATION_STUDENT> _students = db.TB_APPLICATION_STUDENT.Where(s => s.STD_SCHOOL_ID == id).ToList();

            var model = new ApplicationModel { Students = _students };
            return View(model);
        }

        public ActionResult ApproveStudent(int id = 0)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            TB_APPLICATION_STUDENT student = db.TB_APPLICATION_STUDENT.Single(t => t.STD_ID == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            List<TB_M_STATUS> tbMStatus = db.TB_M_STATUS.Where(s => s.STATUS_GROUP == 1).ToList();
            ViewBag.STD_APPROVED_STATUS = new SelectList(tbMStatus, "STATUS_ID", "STATUS_NAME_TH", student.STD_APPROVED_STATUS);

            return View(student);
        }

        [HttpPost]
        public ActionResult ApproveStudent(TB_APPLICATION_STUDENT student)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }

            if (ModelState.IsValid)
            {
                db.TB_APPLICATION_STUDENT.Attach(student);
                db.ObjectStateManager.ChangeObjectState(student, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("ApproveStudentList/" + student.STD_SCHOOL_ID);
            }
            //List<TB_M_STATUS> tbMStatus = db.TB_M_STATUS.Where(s => s.STATUS_GROUP == 1).ToList();
            //ViewBag.STD_APPROVED_STATUS = new SelectList(tbMStatus, "STATUS_ID", "STATUS_NAME_TH", student.STD_APPROVED_STATUS);
            //ViewBag.ResultMsg = "บันทึกข้อมูลเรียบร้อยแล้ว";
            return View(student);
        }

        public ActionResult ConfirmDocumentList()
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            var model = new ConfirmDocumentModel { };
            return View(model);
        }
        [HttpPost]
        public ActionResult ConfirmDocumentList(ConfirmDocumentModel model)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            List<TB_APPLICATION_STUDENT> students = null;
            if (!CommonUtils.isNumber(model.schoolName))
            {
                var student = from s in db.TB_APPLICATION_STUDENT where s.STD_NAME.Contains(model.schoolName) select s;
                students = student.ToList();
                foreach (TB_APPLICATION_STUDENT std in students)
                {
                    if (std.TB_STUDENT_SEAT != null)
                    {
                        std.STD_EMAIL = std.TB_STUDENT_SEAT.SIT_NUMBER_PREFIX + "" + Convert.ToInt16(std.TB_STUDENT_SEAT.SIT_NUMBER).ToString("00");
                    }
                }
                model.students = students;
            }
            else
            {
                int stdNum = Convert.ToInt32(model.schoolName);
                var student = from s in db.TB_APPLICATION_STUDENT where s.TB_STUDENT_SEAT.STUDENT_CODE == stdNum select s;
                students = student.ToList();
                foreach (TB_APPLICATION_STUDENT std in students)
                {
                    std.STD_EMAIL = std.TB_STUDENT_SEAT.SIT_NUMBER_PREFIX + "" + Convert.ToInt16(std.TB_STUDENT_SEAT.SIT_NUMBER).ToString("00");
                }
                model.students = students;
            }
            return View(model);
        }


        public ActionResult SearchStudent()
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            var model = new ConfirmDocumentModel { };
            return View(model);
        }
        [HttpPost]
        public ActionResult SearchStudent(ConfirmDocumentModel model)
        {
            if (Session["USER"] == null)
            {
                return RedirectToAction("../Account/ManagementLogin");
            }
            List<TB_APPLICATION_STUDENT> students = null;
            if (CommonUtils.isNumber(model.schoolName))
            {
                int stdNum = Convert.ToInt32(model.schoolName);
                var student = from s in db.TB_APPLICATION_STUDENT where s.TB_STUDENT_SEAT.STUDENT_CODE == stdNum select s;
                students = student.ToList();
                foreach (TB_APPLICATION_STUDENT std in students)
                {
                    std.STD_EMAIL = std.TB_STUDENT_SEAT.SIT_NUMBER_PREFIX + "" + Convert.ToInt16(std.TB_STUDENT_SEAT.SIT_NUMBER).ToString("00");
                }
                model.students = students;


                if (model.action.Equals("adjust"))
                {
                    //Move Old code from tb_student_seat
                    TB_APPLICATION_STUDENT tmpStd = db.TB_APPLICATION_STUDENT.Single(t => t.TB_STUDENT_SEAT.STUDENT_CODE == stdNum);
                    tmpStd.STD_LEVEL_ID = model.SelectedLevelIDs[0];
                    db.TB_APPLICATION_STUDENT.DeleteObject(tmpStd);
                    TB_STUDENT_SEAT tmpSeat = db.TB_STUDENT_SEAT.Single(t => t.STUDENT_CODE == stdNum);
                    db.TB_STUDENT_SEAT.DeleteObject(tmpSeat);
                    //Commit
                    db.SaveChanges();
                    Console.WriteLine();
                }
            }
            return View(model);
        }

        public ActionResult PrintConfirmDocument(int id = 0)
        {

            CultureInfo ci = (CultureInfo)this.Session["PhetCulture"];
            /*
             * DECLARE VARIABLE
             */
            CommonUtils util = new CommonUtils();


            List<Report01Model> lists = new List<Report01Model>();

            var tb_application_student = from a in db.TB_APPLICATION_STUDENT where a.STD_ID == id select a;


            if (tb_application_student != null)
            {

                List<TB_APPLICATION_STUDENT> students = tb_application_student.ToList();
                int schId = Convert.ToInt16(students[0].STD_SCHOOL_ID);
                TB_APPLICATION_SCHOOL school = db.TB_APPLICATION_SCHOOL.Where(s => s.SCHOOL_ID == schId).FirstOrDefault();
                foreach (TB_APPLICATION_STUDENT student in students)
                {

                    /*
                     * ที่นั่งสอบ
                     */
                    TB_STUDENT_SEAT studentSeat = db.TB_STUDENT_SEAT.SingleOrDefault(t => t.STUDENT_ID == student.STD_ID);
                    Report01Model report = new Report01Model();
                    report.map = CommonUtils.getByteImage(Convert.ToInt16(student.STD_LEVEL_ID));
                    if (studentSeat != null)
                    {
                        report.p_std_id = studentSeat.STUDENT_CODE + "";
                        report.p_exam_room = studentSeat.TB_ROOM.ROOM_NUMBER + "";
                        report.p_exam_seat = studentSeat.SIT_NUMBER_PREFIX + "" + Convert.ToInt16(studentSeat.SIT_NUMBER).ToString("00");
                        report.p_exam_building = studentSeat.TB_ROOM.ROOM_BUILD + "";
                        report.p_exam_floor = studentSeat.TB_ROOM.ROOM_FLOOR + "";

                    }
                    else
                    {
                        report.p_std_id = "-";
                        report.p_exam_room = "-";
                        report.p_exam_seat = "-";
                        report.p_exam_building = "-";
                        report.p_exam_floor = "-";
                    }
                    /*
                     * บอร์ดที่ประกาศผลสอบ
                     */
                    TB_EXAM_ANOUNCE examAnounce = db.TB_EXAM_ANOUNCE.Where(t => t.ANOUNCE_FOR_LEVEL == student.STD_LEVEL_ID).FirstOrDefault();
                    if (examAnounce != null)
                    {
                        report.p_exam_announce_building = examAnounce.ANOUNCE_BUILDING + "";
                        report.p_exam_announce_board = examAnounce.ANOUNCE_BOARD;
                    }
                    else
                    {
                        report.p_exam_announce_building = "-";
                        report.p_exam_announce_board = "-";
                    }

                    report.p_exam_date = "25 สิงหาคม 2556";
                    report.p_exam_time = "09.00-10.00 น.";

                    report.p_student_name = util.getStudentFullName(student);
                    report.p_school_name = school.SCHOOL_NAME;
                    report.p_school_type = getSchoolType(school.SCHOOL_TYPE);
                    report.p_school_province = school.TB_M_PROVINCE_SCHOOL.PROVINCE_NAME;
                    report.p_student_level = student.TB_M_LEVEL.LEVEL_NAME_TH.Split(' ')[0];// +" " + student.STD_GRADE;
                    report.p_student_grade = student.STD_GRADE + "";

                    String phoneLable = (ci.Name.ToUpper().Equals("TH")) ? "เบอร์ติดต่อ  " : "Phone Number  ";

                    var tb_application_staff = from a in db.TB_APPLICATION_STAFF where a.STAFF_SCHOOL_ID == school.SCHOOL_ID select a;
                    //APPEND STAFF
                    if (tb_application_staff != null)
                    {
                        List<TB_APPLICATION_STAFF> staffs = tb_application_staff.ToList();

                        if (staffs.Count >= 1)
                        {
                            report.p_staff01_name = util.getStaffFullName(staffs[0]);
                            report.p_staff01_phone = phoneLable + staffs[0].STAFF_PHONE;
                        }
                        if (staffs.Count >= 2)
                        {
                            report.p_staff02_name = util.getStaffFullName(staffs[1]);
                            report.p_staff02_phone = phoneLable + staffs[1].STAFF_PHONE;
                        }
                        if (staffs.Count >= 3)
                        {
                            report.p_staff03_name = util.getStaffFullName(staffs[2]);
                            report.p_staff03_phone = phoneLable + staffs[2].STAFF_PHONE;
                        }
                        if (staffs.Count >= 4)
                        {
                            report.p_staff04_name = util.getStaffFullName(staffs[3]);
                            report.p_staff04_phone = phoneLable + staffs[3].STAFF_PHONE;
                        }
                        if (staffs.Count >= 5)
                        {
                            report.p_staff05_name = util.getStaffFullName(staffs[4]);
                            report.p_staff05_phone = phoneLable + staffs[4].STAFF_PHONE;
                        }
                    }
                    lists.Add(report);
                }
            }


            ReportClass rptH = new ReportClass();
            rptH.FileName = Server.MapPath("~/Reports/Rpt01_th.rpt");
            rptH.Load();
            rptH.SetDataSource(lists);

            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        /* CUSTOM EVENT*/
        public ActionResult DeleteSchool(ManagementModel model)
        {
            try
            {

                db.TB_APPLICATION_STUDENT.Where(s => s.STD_SCHOOL_ID == model.id).ToList().ForEach(db.TB_APPLICATION_STUDENT.DeleteObject);
                db.TB_APPLICATION_STAFF.Where(s => s.STAFF_SCHOOL_ID == model.id).ToList().ForEach(db.TB_APPLICATION_STAFF.DeleteObject);

                TB_APPLICATION_SCHOOL tb_application_stchool = db.TB_APPLICATION_SCHOOL.Single(t => t.SCHOOL_ID == model.id);
                db.TB_APPLICATION_SCHOOL.DeleteObject(tb_application_stchool);
                db.SaveChanges();
                // delete the record from ID and return true else false
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ViewBag.ResultMsg = "ไม่สามารถลบข้อมูลได้";
                //return View();
                //return Json(ex.InnerException.Message, JsonRequestBehavior.AllowGet);
            }



            List<TB_M_STATUS> tbMStatus = db.TB_M_STATUS.Where(s => s.STATUS_GROUP == 1).ToList();
            ViewBag.approveStatus = new SelectList(tbMStatus, "STATUS_ID", "STATUS_NAME_TH", model.approveStatus);


            int startDate = Convert.ToInt32(CommonUtils.toDate(model.startDate).ToString("yyyyMMdd", new CultureInfo("th-TH")));
            int endDate = Convert.ToInt32(CommonUtils.toDate(model.endDate).ToString("yyyyMMdd", new CultureInfo("th-TH")));

            var varSchool = from a in db.TB_APPLICATION_SCHOOL select a;
            List<TB_APPLICATION_SCHOOL> tmpSchoolList = varSchool.ToList();


            List<TB_APPLICATION_SCHOOL> schoolList = new List<TB_APPLICATION_SCHOOL>();
            StringBuilder x = new StringBuilder();
            foreach (TB_APPLICATION_SCHOOL school in tmpSchoolList)
            {
                int curDate = Convert.ToInt32(school.SCHOOL_REGISTER_DATE.Value.ToString("yyyyMMdd", new CultureInfo("en-US")));
                if (curDate >= startDate && curDate <= endDate)
                {
                    x.Append("|" + curDate + "," + startDate + "," + endDate + "|");
                    schoolList.Add(school);
                }
            }
            if (schoolList != null)
            {
                var pageIndex = model.Page ?? 1;
                model.schools = schoolList.ToPagedList(pageIndex, 15);
            }

            return View("SchoolList", model);
        }

        //public JsonResult DeleteSchool(int ID)
        //{
        //    try
        //    {

        //        db.TB_APPLICATION_STUDENT.Where(s => s.STD_SCHOOL_ID == ID).ToList().ForEach(db.TB_APPLICATION_STUDENT.DeleteObject);
        //        db.TB_APPLICATION_STAFF.Where(s => s.STAFF_SCHOOL_ID == ID).ToList().ForEach(db.TB_APPLICATION_STAFF.DeleteObject);

        //        TB_APPLICATION_SCHOOL tb_application_stchool = db.TB_APPLICATION_SCHOOL.Single(t => t.SCHOOL_ID == ID);
        //        db.TB_APPLICATION_SCHOOL.DeleteObject(tb_application_stchool);
        //        db.SaveChanges();
        //        // delete the record from ID and return true else false
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(ex.InnerException.Message, JsonRequestBehavior.AllowGet);
        //    }
        //    //return new JsonResult { JsonRequestBehavior = JsonRequestBehavior.AllowGet, Data = new { msg = "success" } };
        //    return Json(true, JsonRequestBehavior.AllowGet);
        //}

        //public List<Report19Model> Report19(String _startDate, String _endDate, int periodIndex)
        //{

        //    /**
        //     * รายงานสรุปจำนวนรวมของผู้สมัครในแต่ละในแต่ละวัน 
        //     * "วันที่สมัคร","ระดับชั้น", "สถานศึกษา", "จังหวัด", "ประเทศ" 
        //     */

        //    List<Report19Model> lists = new List<Report19Model>();
        //    int seq = 1;
        //    switch (periodIndex)
        //    {
        //        case 1:
        //            List<Report19Model> tmpList = new List<Report19Model>();

        //            //var myQuery = from i in db.TB_APPLICATION_SCHOOL
        //            //              let dt = i.SCHOOL_REGISTER_DATE
        //            //              group i by new { y = dt.Value.Year, m = dt.Value.Month, d = dt.Value.Day } into g
        //            //              select new { registerDate = g.Key, amount = g.Sum(p => p.TB_APPLICATION_STUDENT.Count) };

        //            //     List <TB_APPLICATION_SCHOOL> xxx = db.TB_APPLICATION_SCHOOL.GroupBy(x => new 
        //            //{
        //            //   month = x.SCHOOL_REGISTER_DATE.Value.Month,
        //            //   year = x.SCHOOL_REGISTER_DATE.Value.Year
        //            //}).ToList();

        //            //var myQuery = from p in db.TB_APPLICATION_SCHOOL
        //            //              orderby p.SCHOOL_REGISTER_DATE descending
        //            //              group p by EntityFunctions.TruncateTime(p.SCHOOL_REGISTER_DATE) into g
        //            //              select new { registerDate = g.Key, amount = g.Sum(p => p.TB_APPLICATION_STUDENT.Count) };

        //            //foreach (var data in myQuery)
        //            //{
        //            //Report19Model report = new Report19Model();
        //            //report.seq = seq;
        //            //report.description = Convert.ToDateTime(data.registerDate).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "";
        //            //report.count = data.amount;
        //            //tmpList.Add(report);
        //            //seq++;
        //            //}
        //            //List<Report19Model> tmp = tmpList.OrderBy(o => o.description).ToList();
        //            //int tmpSeq = 1;
        //            //foreach (Report19Model rpt in tmp)
        //            //{
        //            //    rpt.seq = tmpSeq;
        //            //    tmpSeq++;
        //            //}
        //            //lists = tmp;
        //            break;
        //        case 2:
        //            List<TB_M_LEVEL> levelList = db.TB_M_LEVEL.Where(l => l.LEVEL_ID != 0).ToList();
        //            if (levelList != null)
        //            {
        //                foreach (TB_M_LEVEL level in levelList)
        //                {
        //                    Report19Model report = new Report19Model();
        //                    report.seq = seq;
        //                    report.description = level.LEVEL_NAME_TH;
        //                    report.count = level.TB_APPLICATION_STUDENT.Count;
        //                    lists.Add(report);

        //                    seq++;
        //                }
        //            }
        //            break;
        //        case 3:
        //            List<TB_APPLICATION_SCHOOL> schoolList = db.TB_APPLICATION_SCHOOL.ToList();
        //            if (schoolList != null)
        //            {
        //                foreach (TB_APPLICATION_SCHOOL school in schoolList)
        //                {
        //                    Report19Model report = new Report19Model();
        //                    report.seq = seq;
        //                    report.description = school.SCHOOL_NAME;
        //                    report.count = school.TB_APPLICATION_STUDENT.Count;
        //                    lists.Add(report);

        //                    seq++;
        //                }
        //            }
        //            break;
        //        case 4:

        //            List<Report19Model> tmpProvinceList = new List<Report19Model>();
        //            var myQuery1 = from p in db.TB_APPLICATION_SCHOOL
        //                           orderby p.SCHOOL_REGISTER_DATE descending
        //                           group p by p.TB_M_PROVINCE_SCHOOL.PROVINCE_NAME into g
        //                           select new { province = g.Key, amount = g.Sum(p => p.TB_APPLICATION_STUDENT.Count) };
        //            foreach (var data in myQuery1)
        //            {
        //                Report19Model report = new Report19Model();
        //                report.seq = seq;
        //                report.description = data.province;
        //                report.count = data.amount;
        //                tmpProvinceList.Add(report);
        //                seq++;
        //            }
        //            List<Report19Model> tmp1 = tmpProvinceList.OrderBy(o => o.description).ToList();
        //            int tmpSeq1 = 1;
        //            foreach (Report19Model rpt in tmp1)
        //            {
        //                rpt.seq = tmpSeq1;
        //                tmpSeq1++;
        //            }
        //            lists = tmp1;

        //            break;
        //        case 5:
        //            break;
        //    }



        //    return lists;
        //}

        private String getSchoolType(String type)
        {
            String result = "";
            switch (Convert.ToInt16(type))
            {
                case 1:
                    result = Resources.Application.Application.SCHOOL_TYPE_01;
                    break;
                case 2:
                    result = Resources.Application.Application.SCHOOL_TYPE_02;
                    break;
                case 3:
                    result = Resources.Application.Application.SCHOOL_TYPE_03;
                    break;
                case 4:
                    result = Resources.Application.Application.SCHOOL_TYPE_04;
                    break;
                case 5:
                    result = Resources.Application.Application.SCHOOL_TYPE_OTHER;
                    break;
                default: break;
            }
            return result;
        }




        public PartialViewResult editStaff(String STAFF_TITLE_ID, String STAFF_NAME, String STAFF_SURNAME, String STAFF_PHONE)
        {

            return PartialView("_AddStaffPartial_Edit",
                new TB_APPLICATION_STAFF
                {
                    STAFF_TITLE_ID = Convert.ToInt16(STAFF_TITLE_ID),
                    STAFF_NAME = STAFF_NAME,
                    STAFF_SURNAME = STAFF_SURNAME,
                    STAFF_PHONE = STAFF_PHONE
                }
                );
        }
        public PartialViewResult EditStudent(String STD_LEVEL_ID, String STD_GRADE, String STD_TITLE_ID, String STD_NAME, String STD_SURNAME, String STD_PHONE, String STD_EMAIL, String STD_BIRTH_DAY)
        {
            return PartialView("_AddStudentPartial_Edit",
                new TB_APPLICATION_STUDENT
                {
                    STD_LEVEL_ID = Convert.ToInt16(STD_LEVEL_ID),
                    STD_GRADE = Convert.ToInt16(STD_GRADE),
                    STD_TITLE_ID = Convert.ToInt16(STD_TITLE_ID),
                    STD_NAME = STD_NAME,
                    STD_SURNAME = STD_SURNAME,
                    STD_PHONE = STD_PHONE,
                    STD_EMAIL = STD_EMAIL,
                    STD_BIRTH_DAY = STD_BIRTH_DAY
                }
                );
        }

    }
}
