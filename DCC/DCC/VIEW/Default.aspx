﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DCC.VIEW.Default" %>



<%@ Register src="MyMenu.ascx" tagname="MyMenu" tagprefix="uc1" %>
<%@ Register src="Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>



<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>


<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc1:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>



<asp:Content ID="Content6" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style3
        {
            height: 23px;
        }
        .path
        {
            margin-bottom:10px;
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content7" runat="server" 
    contentplaceholderid="ContentPlaceHolder3">
    <table class="style1">
        <tr>
            <td style="text-align: left; vertical-align: middle;">
                <asp:Button ID="CMD_UPLOAD" runat="server" 
            Text="อัพโหลดเอกสาร" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" />
                <asp:Button ID="CMD_NEW_FOLDER" runat="server" 
            Text="สร้างโฟลเดอร์" BorderColor="#999999" BorderStyle="Solid" 
                    BorderWidth="1px"  />
                <asp:Button ID="CMD_REF" runat="server" Text="ผู้เกี่ยวข้องกับเอกสาร" 
                    BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" />
                <asp:Button ID="Button3" runat="server" onclick="Button3_Click" 
                    Text="Refresh" BorderColor="#999999" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <asp:Label ID="LName" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="LType" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: left" class="style3">
    
                <asp:ImageButton ID="ImageButton2" runat="server" 
                    ImageUrl="~/VIEW/images/document_view.png" />
    
        <asp:Label ID="LPath" runat="server" Text="Label" CssClass="path"></asp:Label>
        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
            &nbsp;<asp:ImageButton ID="ImageButton3" runat="server" 
                    ImageUrl="~/VIEW/images/button_previous.gif" onclick="ImageButton3_Click" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:GridView ID="GridView2" runat="server" BackColor="White" 
                    BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" 
                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID" 
                    DataSourceID="EntityDataSource1" onrowdatabound="GridView1_RowDataBound" 
                    onselectedindexchanged="GridView1_SelectedIndexChanged" 
                    onrowdeleted="GridView2_RowDeleted" onrowdeleting="GridView2_RowDeleting" 
                    Width="100%" EmptyDataText="File not found." ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" 
                            SortExpression="ID" Visible="False" />
                        <asp:TemplateField HeaderText="ชนิด" ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" 
                                    CommandName="Select" AlternateText="Select" />
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Type") %>' 
                                    Visible="False" ></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ชื่อ" SortExpression="Name">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Name") %>' 
                                    Visible="False"></asp:Label>
                                <asp:HyperLink ID="HyperLink2" runat="server">HyperLink</asp:HyperLink>
                                <asp:LinkButton ID="LbName" runat="server" CausesValidation="False" 
                                    CommandName="Select" Text="เลือก"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Width="330px" ForeColor="#333333" CssClass="fontRowGrid" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Modified" HeaderText="แก้ไขเมื่อ" 
                            SortExpression="Modified" />
                        <asp:BoundField DataField="ModifiedBy" HeaderText="แก้ไขโดย" 
                            SortExpression="ModifiedBy" />
                        <asp:BoundField DataField="USER_ID" HeaderText="USER_ID" 
                            SortExpression="USER_ID" Visible="False" />
                        <asp:TemplateField ShowHeader="False" HeaderText="เลือก">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False"
                                    CommandName="Select" Text="Select"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" 
                                ForeColor="#333333" CssClass="fontRowGrid" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" HeaderText="ลบ">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                    CommandName="Delete" ImageUrl="~/VIEW/images/icon_del.gif" AlternateText="Delete" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle HorizontalAlign="Center" Width="100%" />
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <HeaderStyle BackColor="#999999" Font-Bold="True" ForeColor="White" 
                        BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" ForeColor="#333333" />
                    <SelectedRowStyle Font-Bold="False" />
                    <SortedAscendingCellStyle BackColor="#EBEBEB" />
                    <SortedAscendingHeaderStyle BackColor="#999999" />
                    <SortedDescendingCellStyle BackColor="#EBEBEB" />
                    <SortedDescendingHeaderStyle BackColor="#999999" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
                    ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                    AutoGenerateWhereClause ="True"
                    EnableFlattening="False" EntitySetName="TBL_FILEMANAGER" 
                    EnableDelete="True" EntityTypeFilter="TBL_FILEMANAGER">
                </asp:EntityDataSource>
            </td>
        </tr>
        <tr>
            <td>
    
        <asp:Label ID="Label5" runat="server" Text="เอกสารที่เกี่ยวข้อง"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridView3" runat="server" BackColor="White" 
                    BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID" 
                    DataSourceID="EntityDataSource2" onrowdatabound="GridView3_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" 
                            SortExpression="ID" Visible="False" />
                        <asp:TemplateField HeaderText="ชนิด" SortExpression="Type">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("Type") %>' 
                                    Visible="False"></asp:Label>
                                <asp:Image ID="Image4" runat="server" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ชื่อ" SortExpression="Name">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("Name") %>' 
                                    Visible="False"></asp:Label>
                                <asp:HyperLink ID="HyperLink4" runat="server">HyperLink</asp:HyperLink>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemStyle Width="350px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Modified" HeaderText="แก้ไขเมื่อ" 
                            SortExpression="Modified" />
                        <asp:BoundField DataField="ModifiedBy" HeaderText="แก้ไขโดย" 
                            SortExpression="ModifiedBy" />
                    </Columns>
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <HeaderStyle BackColor="#999999" Font-Bold="True" ForeColor="White" 
                        BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" ForeColor="#333333" BorderColor="#999999" 
                        BorderStyle="Solid" BorderWidth="1px" />
                    <SelectedRowStyle Font-Bold="False" BackColor="#E6E6E6" />
                    <SortedAscendingCellStyle BackColor="#EBEBEB" />
                    <SortedAscendingHeaderStyle BackColor="#999999" />
                    <SortedDescendingCellStyle BackColor="#EBEBEB" />
                    <SortedDescendingHeaderStyle BackColor="#999999" />
                </asp:GridView>
                <asp:EntityDataSource ID="EntityDataSource2" runat="server" 
                    ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                    EnableFlattening="False" EntitySetName="TBL_FILEMANAGER">
                </asp:EntityDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>




