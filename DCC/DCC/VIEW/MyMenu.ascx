﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyMenu.ascx.cs" Inherits="DCC.MyMenu" %>
<link href="style/form.css" rel="stylesheet" type="text/css" />                  
<link href="style/structure.css" rel="stylesheet" type="text/css" />                 
<link href="style/style.css" rel="stylesheet" type="text/css" />           
<a href="MyMenu.ascx" />
                            
<asp:Menu ID="Menu1" runat="server" BackColor="White" 
    DynamicHorizontalOffset="2" 
Font-Names="Verdana" Font-Size="0.8em" 
    ForeColor="#333333" StaticSubMenuIndent="10px" 
Width="100%" style="text-align: left" CssClass="title_preview">
    <DynamicHoverStyle BackColor="#999999" ForeColor="White" />
    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <DynamicSelectedStyle BackColor="#507CD1" />
    <DynamicItemTemplate>
        <%# Eval("Text") %>
    </DynamicItemTemplate>
    <Items>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/User/ChangePassword.aspx" 
            Text="เปลี่ยนรหัสผ่าน"></asp:MenuItem>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/Admin/ManageUser.aspx" 
            Text="บัญชีผู้ใช้งาน" Value="0011"></asp:MenuItem>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/Master/MTitle.aspx" 
            Text="ตั้งค่าคำนำหน้าชื่อ"></asp:MenuItem>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/Admin/ManageEmail.aspx" 
            Text="ตั้งค่าอีเมล์ระบบ" Value="0015"></asp:MenuItem>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/Admin/EventLog.aspx" Text="บันทึกการใช้งาน" 
            Value="Event Log"></asp:MenuItem>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/Admin/Stat.aspx" Text="สถิติการใช้งาน" 
            Value="0017"></asp:MenuItem>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/Admin/UserLogs.aspx" Text="บันทึกการเข้าระบบ" 
            Value="User Logs"></asp:MenuItem>
        <asp:MenuItem Text="ออกจากระบบ" Value="005" NavigateUrl="~/VIEW/Logout.aspx"></asp:MenuItem>
    </Items>
    <StaticHoverStyle BackColor="#999999" ForeColor="White" Width="100%" />
    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <StaticSelectedStyle BackColor="#507CD1" />
</asp:Menu>
                        
                        
                        
                        
<asp:Menu ID="Menu2" runat="server" BackColor="White" 
    DynamicHorizontalOffset="2" 
Font-Names="Verdana" Font-Size="0.8em" 
    ForeColor="#333333" StaticSubMenuIndent="10px" 
style="text-align: left" Width="100%" CssClass="title_preview">
    <DynamicHoverStyle BackColor="#999999" ForeColor="White" />
    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <DynamicMenuStyle BackColor="#B5C7DE" />
    <DynamicSelectedStyle BackColor="#507CD1" />
    <DynamicItemTemplate>
        <%# Eval("Text") %>
    </DynamicItemTemplate>
    <Items>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/User/ChangePassword.aspx" 
            Text="เปลี่ยนรหัสผ่าน"></asp:MenuItem>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/Admin/EventLog.aspx" Text="บันทึกการใช้งาน" 
            Value="Event Log"></asp:MenuItem>
        <asp:MenuItem Text="ออกจากระบบ" Value="005" NavigateUrl="~/VIEW/Logout.aspx"></asp:MenuItem>
    </Items>
    <StaticHoverStyle BackColor="#999999" ForeColor="White" Width="100%" />
    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <StaticSelectedStyle BackColor="#507CD1" />
</asp:Menu>
                        
                        
                        
                        
<asp:Menu ID="Menu3" runat="server" BackColor="White" 
    DynamicHorizontalOffset="2" 
Font-Names="Verdana" Font-Size="0.8em" 
    ForeColor="#333333" StaticSubMenuIndent="10px" 
style="text-align: left" Width="100%" CssClass="title_preview">
    <DynamicHoverStyle BackColor="#999999" ForeColor="White" />
    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <DynamicMenuStyle BackColor="#B5C7DE" />
    <DynamicSelectedStyle BackColor="#507CD1" />
    <DynamicItemTemplate>
        <%# Eval("Text") %>
    </DynamicItemTemplate>
    <Items>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/User/ChangePassword.aspx" 
            Text="เปลี่ยนรหัสผ่าน" Value="เปลี่ยนรหัสผ่าน"></asp:MenuItem>
        <asp:MenuItem Text="ออกจากระบบ" Value="005" NavigateUrl="~/VIEW/Logout.aspx"></asp:MenuItem>
    </Items>
    <StaticHoverStyle BackColor="#999999" ForeColor="White" Width="100%" />
    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <StaticSelectedStyle BackColor="#507CD1" />
</asp:Menu>
                        
                        
                        
                        
<asp:Menu ID="Menu4" runat="server" BackColor="White" 
    DynamicHorizontalOffset="2" 
Font-Names="Verdana" Font-Size="0.8em" 
    ForeColor="#333333" StaticSubMenuIndent="10px" 
style="text-align: left" Width="100%" CssClass="title_preview">
    <DynamicHoverStyle BackColor="#999999" ForeColor="White" />
    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <DynamicMenuStyle BackColor="#B5C7DE" />
    <DynamicSelectedStyle BackColor="#507CD1" />
    <DynamicItemTemplate>
        <%# Eval("Text") %>
    </DynamicItemTemplate>
    <Items>
        <asp:MenuItem NavigateUrl="~/VIEW/pages/User/ChangePassword.aspx" 
            Text="เปลี่ยนรหัสผ่าน" Value="0032"></asp:MenuItem>
        <asp:MenuItem Text="ออกจากระบบ" Value="005" NavigateUrl="~/VIEW/Logout.aspx"></asp:MenuItem>
    </Items>
    <StaticHoverStyle BackColor="#999999" ForeColor="White" Width="100%" />
    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
    <StaticSelectedStyle BackColor="#507CD1" />
</asp:Menu>
                        
                        
                        
                        
