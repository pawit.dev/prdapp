﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.Utility;
using DCC.MODEL;
using DCC.DAL;

namespace DCC.VIEW.pages.Admin
{
    public partial class UserLogs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    TXT_BEGIN.Enabled = false;
                    TXT_END.Enabled = false;
                    //check page permission
                    if (!Navigator1.viewPagePermission(user, "001"))
                    {
                        Response.Redirect("~/VIEW/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
            }
            ///
            switch (RCondition.SelectedValue)
            {
                case "0":
                    break;
                case "1":
                    HiddenField1.Value = Util.toDate(TXT_BEGIN.Text);
                    HiddenField2.Value = Util.toDate(TXT_END.Text);
                    EntityDataSource1.AutoGenerateWhereClause = false;
                    EntityDataSource1.Where = "it.[USER_LOG_DATE] between '" + HiddenField1.Value + "' and '" + HiddenField2.Value + "'";
                    break;
            }
        }

        protected void CMD_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            switch (button.ID)
            {
                case "CMD_SEARCH":
                    switch (RCondition.SelectedIndex)
                    {
                        case 0:
                            Response.Redirect("UserLogs.aspx");
                            break;
                        case 1:
                            HiddenField1.Value = Util.toDate(TXT_BEGIN.Text);
                            HiddenField2.Value = Util.toDate(TXT_END.Text);
                            EntityDataSource1.AutoGenerateWhereClause = false;
                            EntityDataSource1.Where = "it.[USER_LOG_DATE] between '" + HiddenField1.Value + "' and '" + HiddenField2.Value + "'";
                            break;
                    }
                    break;
                case "CMD_CANCEL":
                    Response.Redirect("UserLogs.aspx");
                    break;
            }
        }


        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TBL_USERS_PROFILE uProfile = DALUser.getUsers(e.Row.Cells[1].Text);
                if (uProfile != null)
                {
                    e.Row.Cells[2].Text = uProfile.PROFILE_NAME + " " + uProfile.PROFILE_SURNAME;
                    e.Row.Cells[3].Text = Util.toDate4(e.Row.Cells[3].Text);
                    e.Row.Cells[4].Text = Util.toDate4(e.Row.Cells[4].Text);
                }
                else {
                    e.Row.Cells[2].Text = "-- ไม่พบข้อมูล --";
                    e.Row.Cells[3].Text = "-- ไม่พบข้อมูล --";
                    e.Row.Cells[4].Text = "-- ไม่พบข้อมูล --";
                }
            }
        }

        protected void RCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (RCondition.SelectedIndex)
            {
                case 0:
                    TXT_BEGIN.Enabled = false;
                    TXT_END.Enabled = false;
                    break;
                case 1:
                    TXT_BEGIN.Enabled = true;
                    TXT_END.Enabled = true;
                    break;
            }
        }
    }
}