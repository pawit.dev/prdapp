﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="ManageEmail.aspx.cs" Inherits="DCC.VIEW.pages.Admin.ManageEmail" %>
<%@ Register src="../../MyMenu.ascx" tagname="MyMenu" tagprefix="uc1" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
    <p>
    <table class="style1">
        <tr>
            <td colspan="2">
                        <table class="style1">
                            <tr>
                                <td >
                                    <div class="box_header">
                                            <asp:Label ID="Label5" runat="server" Text="ตั้งค่าอีเมล์ระบบ"></asp:Label>
                                        </div>
                                    <div class="box_data">
                                        <table class="style1" >
                                            <tr >
                                                <td colspan="2" style="text-align: left;" >
                                                    &nbsp;</td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label18" runat="server" 
                                                        Text="Email Account"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="TXT_EMAIL_ACCOUNT" runat="server" CssClass="font_textbox" 
                                                        Width="195px" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                        ControlToValidate="TXT_EMAIL_ACCOUNT" ErrorMessage="ยังไม่ได้ป้อนอีเมล์" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="HEMAIL_ID" runat="server" />
                                                </td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label19" runat="server" Text="Password"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="TXT_PASSWORD" runat="server" CssClass="font_textbox" 
                                                        TextMode="Password" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="TXT_PASSWORD" ErrorMessage="ยังไม่ได้ป้อนรหัส" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label28" runat="server" 
                                                        Text="Confirm Password"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="TXT_CONFIRM_PASSWORD" runat="server" CssClass="font_textbox" 
                                                        TextMode="Password" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                        ControlToValidate="TXT_CONFIRM_PASSWORD" 
                                                        ErrorMessage="ยังไม่ได้ป้อนยืนยันรหัส" SetFocusOnError="True" 
                                                        style="color: #FF9933"></asp:RequiredFieldValidator>
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                                            ControlToCompare="TXT_PASSWORD" ControlToValidate="TXT_CONFIRM_PASSWORD" 
                                                            ErrorMessage="ยืนยันรหัสผ่านไม่ตรงกัน" SetFocusOnError="True"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label20" runat="server" Text="Host"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="TXT_HOST" runat="server" CssClass="font_textbox" 
                                                        CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                        ControlToValidate="TXT_HOST" ErrorMessage="ยังไม่ได้ป้อนโฮสต์" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    &nbsp;</td>
                                                <td  style="text-align: right">
                                    <asp:Button ID="บันทึก" runat="server" ForeColor="#006600" 
                                        onclick="CMD_INSERT_Click" Text="บันทึก" />
                                    <asp:Button ID="CMD_CANCEL" runat="server" CausesValidation="False" 
                                        ForeColor="#006600" onclick="CMD_INSERT_Click" Text="ยกเลิก" />
                                                </td>
                                            </tr>
                                        </table>
                                   </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" >
                                    &nbsp;</td>
                            </tr>
                        </table>
                            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 45px">
                    &nbsp;</td>
            <td>
                    &nbsp;</td>
        </tr>
    </table>
</p>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>


<asp:Content ID="Content4" runat="server" contentplaceholderid="head">
</asp:Content>



<asp:Content ID="Content5" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc1:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>




