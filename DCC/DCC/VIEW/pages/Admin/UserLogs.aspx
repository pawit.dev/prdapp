﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="UserLogs.aspx.cs" Inherits="DCC.VIEW.pages.Admin.UserLogs" %>

<%@ Register src="../../MyMenu.ascx" tagname="MyMenu" tagprefix="uc1" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
    <script src="../../scripts/popcalendar.js" type="text/javascript"></script>
    
    <table class="style1">
        <tr>
            <td>
                        <table class="style1">
                            <tr >
                                <td >
                                <div class="box_header">
                                            <asp:Label ID="Label5" runat="server" Text="สถานะการเข้าใช้งานระบบ"></asp:Label>
                                        </div>
                                <div class="box_data">
                                    <table class="style1" >
                                        <tr >
                                            <td style="width: 281px; text-align: right" >
                                                &nbsp;</td>
                                            <td >
                                                &nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td style="width: 281px; text-align: right" >
                                                <asp:Label ID="Label34" runat="server" 
                                                    Text="เงื่อนไขในการค้นหา"></asp:Label>
                                            </td>
                                            <td >
                                                <asp:RadioButtonList ID="RCondition" runat="server" 
                                                    CssClass="font_default4" RepeatDirection="Horizontal" AutoPostBack="True" 
                                                    onselectedindexchanged="RCondition_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True" Value="0">ทั้งหมด</asp:ListItem>
                                                    <asp:ListItem Value="1">ตามช่วงวัน</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td style="width: 281px; text-align: right" >
                                                <asp:Label ID="Label32" runat="server" 
                                                    Text="วันที่เริ่มต้น"></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox styleId="sel2" ID="TXT_BEGIN" runat="server"></asp:TextBox>
                                                    <script language='javascript' type="text/javascript" >
	<!--
                                                        if (!document.layers) {
                                                            document.write("<img src='../../images/cal.gif' style='CURSOR:hand;'onclick='popUpCalendar(this, form1.ctl00$ContentPlaceHolder1$TXT_BEGIN,\"dd/mm/yyyy\")'>")
                                                        }
	//-->
	</script>
                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td style="width: 281px; text-align: right" >
                                                <asp:Label ID="Label33" runat="server" 
                                                    Text="วันที่สิ้นสุด"></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="TXT_END" runat="server"></asp:TextBox>
                                                    <script language='javascript' type="text/javascript" >
	<!--
                                                        if (!document.layers) {
                                                            document.write("<img src='../../images/cal.gif' style='CURSOR:hand;'onclick='popUpCalendar(this, form1.ctl00$ContentPlaceHolder1$TXT_END,\"dd/mm/yyyy\")'>")
                                                        }
	//-->
	</script>                                                
                                                <asp:HiddenField ID="HiddenField2" runat="server" />
                                            </td>
                                        </tr>
                                        <tr >
                                            <td style="width: 281px" >
                                                &nbsp;</td>
                                            <td  style="text-align: right">
                                                <asp:Button ID="CMD_SEARCH" runat="server" ForeColor="#006600" 
                                                    onclick="CMD_Click" Text="ค้นหา" />
                                                <asp:Button ID="CMD_CANCEL" runat="server" ForeColor="#006600" 
                                                    onclick="CMD_Click" Text="ยกเลิก" />
                                            </td>
                                        </tr>
                                        </table>
                                </div>
                                </td>
                            </tr>
                            <tr >
                                <td >
                                    &nbsp;</td>
                            </tr>
                            <tr >
                                <td >
                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                                        CellPadding="4" DataSourceID="EntityDataSource1" ForeColor="Black" 
                                        GridLines="Vertical" AllowPaging="True" 
                                        onrowdatabound="GridView2_RowDataBound" DataKeyNames="LOG_ID" 
                                        AllowSorting="True" PageSize="30">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="LOG_ID" HeaderText="LOG_ID" ReadOnly="True" 
                                                SortExpression="LOG_ID" Visible="False" />
                                            <asp:BoundField DataField="USER_ID" HeaderText="รหัส" 
                                                SortExpression="USER_ID" />
                                            <asp:BoundField HeaderText="ชื่อ-นามสกุล" />
                                            <asp:BoundField DataField="USER_LOGIN_DATE" HeaderText="วันเวลาที่เข้าระบบ" 
                                                SortExpression="USER_LOGIN_DATE" />
                                            <asp:BoundField DataField="USER_LOGOUT_DATE" HeaderText="วันเวลาที่ออกจากระบบ" 
                                                SortExpression="USER_LOGOUT_DATE" />
                                            <asp:BoundField DataField="USER_LOG_DATE" HeaderText="USER_LOG_DATE" 
                                                SortExpression="USER_LOG_DATE" Visible="False" />
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                        <RowStyle BackColor="#F7F7DE" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                                    <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                        EnableFlattening="False" EntitySetName="TBL_USERS_LOGS">
                                    </asp:EntityDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 74px">
                                    &nbsp;</td>
                            </tr>
                        </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>


<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc1:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>



