﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.VIEW.pages.Master;
using DCC.MODEL;
using DCC.DAL;
using DCC.Utility;
using System.Text;

namespace DCC.VIEW.pages.Admin
{
    public partial class ManageUser : System.Web.UI.Page
    {
        private ACTION dbAction = ACTION.INSERT;
        private String formName = "ManageUser.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    //Set initial action
                    Session["ACTION"] = ACTION.INSERT;
                    //check page permission
                    if (!Navigator1.viewPagePermission(user, "001"))
                    {
                        Response.Redirect("~/VIEW/Default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
                CMD_INSERT.Visible = true;
                CMD_INSERT.Text = "เพิ่ม";
                RequiredFieldValidator6.ControlToValidate = "TXT_PASSWORD";
                RequiredFieldValidator7.ControlToValidate = "TXT_CONFIRM_PASS";
                TXT_ID.Enabled = true;
                TXT_USERNAME.Enabled = true;
            }
            EntityDataSource2.AutoGenerateWhereClause = false;
            EntityDataSource2.Select = "it.[DEPARTMENT_ID], it.[DEPARTMENT_NAME]";
            EntityDataSource2.GroupBy = "it.[DEPARTMENT_ID],it.[PARENT_ID],it.[DEPARTMENT_NAME]";
            if (GridView1.Rows.Count == 0)
            {
                LWARN.Text = MyMessage.SEARCH_NOT_FOUND;
            }
            CMD_INSERT.Attributes.Add("onsubmit", "checkForm()");
        }

        protected void CMD_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            switch (button.ID)
            {
                case "CMD_INSERT":                    
                    dbAction = (ACTION)Session["ACTION"];
                        using (DCCDBEntities context = new DCCDBEntities())
                        {
                            switch (dbAction)
                            {
                                //case ACTION.ADD:
                                //    #region "เพิ่ม"
                                //    TXT_ID.Focus();
                                //    TXT_ID.Enabled = true;
                                //    CMD_INSERT.Enabled = true;
                                //    CMD_INSERT.Text = "บันทึก";
                                //    Panel3.Enabled = true;
                                //    Session["ACTION"] = ACTION.INSERT;
                                //    #endregion
                                //    break;
                                case ACTION.INSERT:
                                    #region "บันทึก"
                                    if (!isDataExist())// && validate())
                                    {
                                        TBL_USERS user = new TBL_USERS()
                                        {
                                            USER_ID = TXT_ID.Text,
                                            USER_NAME = TXT_USERNAME.Text,
                                            USER_PASSWORD = MD5.md5(TXT_PASSWORD.Text),
                                            USER_GROUP_ID = DDL_GROUP.SelectedValue,
                                            USER_STATUS = DDL_STATUS.SelectedValue,
                                            USER_CREATEDATE = DateTime.Now
                                        };
                                        TBL_USERS_PROFILE uProfile = new TBL_USERS_PROFILE()
                                        {
                                            PROFILE_USER_ID = user.USER_ID,
                                            PROFILE_TITLE_ID = DDL_TITLE.SelectedValue,
                                            PROFILE_NAME = TXT_NAME.Text,
                                            PROFILE_SURNAME = TXT_SURNAME.Text,
                                            PROFILE_EMAIL = TXT_EMAIL.Text,
                                            PROFILE_DEPARTMENT_ID = DDL_DEPT.SelectedValue,
                                        };
                                        context.TBL_USERS_PROFILE.AddObject(uProfile);
                                        context.TBL_USERS.AddObject(user);
                                        if (context.SaveChanges() > 0)
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.INSERT_SUCCESS, formName));
                                        }
                                        else
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.INSERT_FAILURE, formName));
                                        }
                                        initial();   
                                    }
                                    break;
                                #endregion
                                case ACTION.UPDATE:
                                    #region "แก้ไข"
                                    //if (validate())
                                    //{
                                        TBL_USERS userU = context.TBL_USERS.First(d => d.USER_ID == TXT_ID.Text);
                                        userU.USER_ID = TXT_ID.Text;
                                        userU.USER_NAME = TXT_USERNAME.Text;
                                        if (!TXT_PASSWORD.Text.Equals(""))
                                        {
                                            userU.USER_PASSWORD = MD5.md5(TXT_PASSWORD.Text);
                                        }
                                        userU.USER_GROUP_ID = DDL_GROUP.SelectedValue;
                                        userU.USER_STATUS = DDL_STATUS.SelectedValue;
                                        TBL_USERS_PROFILE userProfileU = context.TBL_USERS_PROFILE.First(d => d.PROFILE_USER_ID == TXT_ID.Text);
                                        userProfileU.PROFILE_USER_ID = userU.USER_ID;
                                        userProfileU.PROFILE_TITLE_ID = DDL_TITLE.SelectedValue;
                                        userProfileU.PROFILE_NAME = TXT_NAME.Text;
                                        userProfileU.PROFILE_SURNAME = TXT_SURNAME.Text;
                                        userProfileU.PROFILE_EMAIL = TXT_EMAIL.Text;
                                        userProfileU.PROFILE_DEPARTMENT_ID = DDL_DEPT.SelectedValue;
                                        if (context.SaveChanges() > 0)
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.UPDATE_SUCCESS, formName));
                                        }
                                        else
                                        {
                                            Response.Write(Util.ShowMessage(MyMessage.UPDATE_FAILURE, formName));
                                        }
                                        initial();   
                                    //}
                                    #endregion
                                    break;
                            }
                        }                                         
                    break;
                case "CMD_SEARCH":
                    #region "ค้นหา"
                    StringBuilder condition = new StringBuilder();
                    if (!TXT_ID.Text.Equals(""))
                    {
                        condition.Append(" it.[PROFILE_USER_ID] == '" + TXT_ID.Text + "'");
                        condition.Append(" AND");
                    }
                    if (!TXT_NAME.Text.Equals(""))
                    {
                        condition.Append(" it.[PROFILE_NAME] LIKE '" + TXT_NAME.Text + "'");
                        condition.Append(" AND");
                    }
                    if (!TXT_SURNAME.Text.Equals(""))
                    {
                        condition.Append(" it.[PROFILE_SURNAME] LIKE '" + TXT_SURNAME.Text + "'");
                        condition.Append(" AND");
                    }
                    if (!DDL_DEPT.SelectedValue.Equals(""))
                    {
                        condition.Append(" it.[PROFILE_DEPARTMENT_ID] LIKE '" + DDL_DEPT.SelectedValue + "'");
                        condition.Append(" AND");
                    }
                    if (condition.ToString().Length > 0)
                    {
                        EntityDataSource4.AutoGenerateWhereClause = false;
                        EntityDataSource4.Where = condition.ToString().Substring(0, condition.ToString().Length - 3);
                    }
                    else
                    {
                        Response.Redirect(formName);
                    }
                    #endregion
                    break;
                case "CMD_CANCEL":
                    initial();
                    Response.Redirect(formName);
                    break;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            TXT_ID.Focus();
            TXT_ID.Enabled = true;
            CMD_INSERT.Enabled = true;
            Panel3.Enabled = true;
            
            //Auto ID
            //using (DCCDBEntities context = new DCCDBEntities())
            //{
            //    List<TBL_USERS> dep = context.TBL_USERS.ToList();

            //    TXT_ID.Text = (Convert.ToInt32(dep[dep.Count - 1].USER_ID) + 1).ToString("000000");
            //}
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["ACTION"] = ACTION.UPDATE;
            //Set label name
            CMD_INSERT.Text = MyMessage.BUTTON_UPDATE;
            CMD_INSERT.Enabled = true;
            Panel3.Visible = true;
            RequiredFieldValidator6.ControlToValidate = "TXT_USERNAME";
            RequiredFieldValidator7.ControlToValidate = "TXT_USERNAME";
            TXT_ID.Enabled = false;
            TXT_USERNAME.Enabled = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_USERS tmp = context.TBL_USERS.Where("it.[USER_ID] = '" + GridView1.SelectedRow.Cells[0].Text + "'").First();
                TXT_ID.Text = tmp.USER_ID;
                TXT_USERNAME.Text = tmp.USER_NAME;
                TXT_PASSWORD.Text = tmp.USER_PASSWORD;
                DDL_GROUP.SelectedValue = tmp.USER_GROUP_ID;
                DDL_STATUS.SelectedValue = tmp.USER_STATUS;
                TBL_USERS_PROFILE tmp1 = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID] = '" + GridView1.SelectedRow.Cells[0].Text + "'").First();
                DDL_TITLE.SelectedValue = tmp1.PROFILE_TITLE_ID;
                TXT_NAME.Text = tmp1.PROFILE_NAME;
                TXT_SURNAME.Text = tmp1.PROFILE_SURNAME;
                TXT_EMAIL.Text = tmp1.PROFILE_EMAIL;
                DDL_DEPT.SelectedValue = tmp1.PROFILE_DEPARTMENT_ID;
            }
        }

        private void initial()
        {

            TXT_ID.Enabled = true;
            TXT_USERNAME.Enabled = true;
            TXT_ID.Text = "";
            TXT_USERNAME.Text = "";
            TXT_PASSWORD.Text = "";

            CMD_INSERT.Visible = false;
            CMD_INSERT.Text = "เพิ่ม";
            TXT_NAME.Text = "";
            TXT_SURNAME.Text = "";
            TXT_EMAIL.Text = "";
            TXT_ID.Focus();
        }

        private bool validate()
        {
            bool bSucces = true;
            StringBuilder msg = new StringBuilder();
            if (TXT_ID.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลรหัส");
                msg.Append("\\n");
            }
            if (DDL_TITLE.SelectedValue.Equals(""))
            {
                msg.Append("ยังไม่ได้เลือกประเภทคำนำหน้า");
                msg.Append("\\n");
            }
            if (TXT_NAME.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลชื่อ");
                msg.Append("\\n");
            }
            if (TXT_SURNAME.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลนามสกุล");
                msg.Append("\\n");
            }
            if (TXT_EMAIL.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลอีเมล์");
                msg.Append("\\n");
            }
            if (!Util.isEmail(TXT_EMAIL.Text))
            {
                msg.Append("รูปแบบของอีเมล์ไม่ถูกต้อง");
                msg.Append("\\n");
            }
            if (DDL_GROUP.SelectedValue.Equals(""))
            {
                msg.Append("ยังไม่ได้เลือกแผนก");
                msg.Append("\\n");
            }
            if (TXT_USERNAME.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลรหัสผู้ใช้");
                msg.Append("\\n");
            }
            if (!TXT_PASSWORD.Text.Equals(TXT_CONFIRM_PASS.Text))
            {
                msg.Append("รหัสผ่านไม่ตรงกัน");
                msg.Append("\\n");
            }
            if (TXT_PASSWORD.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนรหัสผ่านใหม่");
                msg.Append("\\n");
            }
            if (TXT_CONFIRM_PASS.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนยืนยันรหัสผ่าน");
                msg.Append("\\n");
            }
            if (DDL_STATUS.SelectedValue.Equals(""))
            {
                msg.Append("ยังไม่ได้เลือกสถานะ");
                msg.Append("\\n");
            }
            if (msg.ToString().Length > 0)
            {
                bSucces = false;
                CMD_INSERT.Enabled = true;
                Response.Write(Util.ShowMessage(msg.ToString(),formName));
            }
            return bSucces;
        }
        private bool isDataExist()
        {
            bool bExist = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_USERS> lists = context.TBL_USERS.Where("it.[USER_ID]='"+TXT_ID.Text+"'").ToList();
                if (lists.Count > 0)
                {
                    bExist = true;
                    CMD_INSERT.Enabled = true;
                    Response.Write(Util.ShowMessage(MyMessage.DATA_EXIST, "ManageUser.aspx"));
                }
            }
            return bExist;
        }

        protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            initial();
            Response.Redirect(formName);
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton button = (ImageButton)e.Row.FindControl("ImageButton2");
                button.Attributes.Add("onclick", "return confirm('" + MyMessage.CONFIRM_DELETE + "');");
                //if system admin set not allow to delete
                if (e.Row.Cells[0].Text.Equals("999999"))
                {
                    button.Enabled = false;
                }

                using (DCCDBEntities context = new DCCDBEntities())
                {
                    List<TBL_M_DEPARTMENT> depts = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_ID]='" + e.Row.Cells[5].Text + "'").ToList();
                    if (depts.Count>0)
                    {
                        e.Row.Cells[5].Text = depts[0].DEPARTMENT_NAME;
                    }
                    List<TBL_USERS> users = context.TBL_USERS.Where("it.[USER_ID]='"+e.Row.Cells[0].Text +"'").ToList();
                    if(users.Count>0){
                        e.Row.Cells[6].Text = users[0].TBL_M_GROUP.GROUP_NAME;
                    }
                }
            }
        }

    }
}