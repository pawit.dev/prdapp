﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.Utility;
using DCC.MODEL;
using DCC.DAL;

namespace DCC.VIEW.pages.Admin
{
    public partial class EventLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    //Set initial action
                    TXT_BEGIN.Enabled = false;
                    TXT_END.Enabled = false;
                    //check page permission
                    //if (!Navigator1.viewPagePermission(user, "001"))
                    //{
                    //    Response.Redirect("~/VIEW/Default.aspx");
                    //}
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }

            }
            //
            switch (RCondition.SelectedValue)
            {
                case "0": break;
                case "1":
                            EntityDataSource1.AutoGenerateWhereClause = false;
                            EntityDataSource1.Where = "it.[EVENT_DATE] between '" + HiddenField1.Value + "' and '" + HiddenField2.Value + "'";
                    break;
            }
        }

        protected void CMD_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            switch (button.ID)
            {
                case "CMD_SEARCH":
                    switch (RCondition.SelectedIndex)
                    {
                        case 0:
                            Response.Redirect("EventLog.aspx");
                            break;
                        case 1:
                            HiddenField1.Value = Util.toDate(TXT_BEGIN.Text) + "0000";
                            HiddenField2.Value = Util.toDate(TXT_END.Text) + "2359";
                            EntityDataSource1.AutoGenerateWhereClause = false;
                            EntityDataSource1.Where = "it.[EVENT_DATE] between '" + HiddenField1.Value + "' and '" + HiddenField2.Value + "'";
                            break;
                    }
                    break;
                case "CMD_CANCEL":
                    Response.Redirect("EventLog.aspx");
                    break;
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                using (DCCDBEntities context = new DCCDBEntities())
                {
                    try
                    {
                        TBL_DOCUMENT doc = context.TBL_DOCUMENT.Where("it.[DOC_ID] = '" + e.Row.Cells[2].Text + "'").First();
                        if (doc != null)
                        {
                            e.Row.Cells[2].Text = e.Row.Cells[2].Text + " (" + doc.DOC_FILE_NAME + " )";
                        }
                        TBL_USERS_PROFILE uProfile = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + e.Row.Cells[1].Text + "'").First();
                        if (uProfile != null)
                        {
                            e.Row.Cells[1].Text = e.Row.Cells[1].Text + " (" + uProfile.PROFILE_NAME + "  " + uProfile.PROFILE_SURNAME + " )";
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message); }
                }
                e.Row.Cells[3].Text = Util.getStatus(e.Row.Cells[3].Text);
                e.Row.Cells[4].Text = Util.toDate4(e.Row.Cells[4].Text);
            }

        }

        protected void RCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (RCondition.SelectedIndex)
            {
                case 0:
                    TXT_BEGIN.Enabled = false;
                    TXT_END.Enabled = false;
                    break;
                case 1:
                    TXT_BEGIN.Enabled = true;
                    TXT_END.Enabled = true;
                    break;
            }
        }

    }
}