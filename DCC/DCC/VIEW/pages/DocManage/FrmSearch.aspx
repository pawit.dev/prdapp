﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmSearch.aspx.cs" Inherits="DCC.VIEW.pages.Qmr.FrmSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Document Control Center</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
.font_default
{
	font-family: "MS Sans Serif";
    font-size:11px;
    font-weight:normal;
    color:black;
    text-decoration:none;
            text-align: center;
        }
        .style2
        {            text-align: center;
        }
    .font_dropdown
{
	font-family: 'Tahoma';
	font-size: 16px;
	color: #CC6600;
	margin-left: 0px;
}
    </style>
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="box_header">กำหนดผู้เกี่ยวข้องกับเอกสาร</div>
        <div class="box_data">
        <table class="style1" width="100%">
            <tr>
                <td class="style2" style="text-align: center" width="45%">
                                                                                            <asp:Label ID="Label32" runat="server" CssClass="fontRowGrid" 
                                                                                                
                        Text="รายชื่อผู้ใช้ในระบบ"></asp:Label>
                                                                                        </td>
                <td style="text-align: center" width="10%">
                    &nbsp;</td>
                <td style="text-align: center" width="45%">
                                                                                            <asp:Label ID="Label33" runat="server" CssClass="fontRowGrid" 
                                                                                                
                        Text="รายชื่อผู้เกี่ยวข้องกับเอกสาร"></asp:Label>
                                                                                        </td>
            </tr>
            <tr>
                <td class="style2" style="text-align: left" width="45%">
                                                <asp:DropDownList ID="DDL_DEP" runat="server" AutoPostBack="True" 
                                                    CssClass="fontRowGrid" DataSourceID="EntityDataSource2" 
                                                    DataTextField="DEPARTMENT_NAME" DataValueField="DEPARTMENT_ID" 
                                                    onselectedindexchanged="DDL_DEP_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                                                        </td>
                <td style="text-align: center" width="10%">
                    &nbsp;</td>
                <td style="text-align: center" width="45%">
                                                                                            &nbsp;</td>
            </tr>
            <tr>
                <td class="style2" style="text-align: center">
                                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" BackColor="White" BorderColor="#3366CC" 
                                        BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                                        DataKeyNames="PROFILE_USER_ID" DataSourceID="EntityDataSource4" 
                                        onrowdatabound="GridView1_RowDataBound" Width="90%">
                                        <Columns>
                                            <asp:BoundField DataField="PROFILE_USER_ID" HeaderText="รหัส" 
                                                ReadOnly="True" SortExpression="PROFILE_USER_ID" />
                                            <asp:BoundField DataField="PROFILE_TITLE_ID" HeaderText="PROFILE_TITLE_ID" 
                                                SortExpression="PROFILE_TITLE_ID" Visible="False" />
                                            <asp:BoundField DataField="PROFILE_NAME" HeaderText="ชื่อ" 
                                                SortExpression="PROFILE_NAME" />
                                            <asp:BoundField DataField="PROFILE_SURNAME" HeaderText="นามสกุล" 
                                                SortExpression="PROFILE_SURNAME" />
                                            <asp:BoundField DataField="PROFILE_EMAIL" HeaderText="อีเมล์" 
                                                SortExpression="PROFILE_EMAIL" Visible="False" />
                                            <asp:BoundField DataField="PROFILE_DEPARTMENT_ID" 
                                                HeaderText="แผนก" SortExpression="PROFILE_DEPARTMENT_ID" 
                                                Visible="False" />
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                                        <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                                        <RowStyle BackColor="White" ForeColor="#003399" />
                                        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                        <SortedAscendingCellStyle BackColor="#EDF6F6" />
                                        <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                                        <SortedDescendingCellStyle BackColor="#D6DFDF" />
                                        <SortedDescendingHeaderStyle BackColor="#002876" />
                                    </asp:GridView>
                                                                                        </td>
                <td style="text-align: center">
                    <table class="style1">
                        <tr>
                            <td>
                                    <asp:ImageButton ID="ImageButton2" runat="server" 
                                        ImageUrl="~/VIEW/images/navigate_right.png" onclick="ImageButton2_Click" />
                                </td>
                        </tr>
                        <tr>
                            <td>
                                    <asp:ImageButton ID="ImageButton1" runat="server" 
                                        ImageUrl="~/VIEW/images/navigate_left.png" onclick="ImageButton1_Click" />
                                </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: center" align="left">
                                                            <asp:ListBox ID="ListBox1" 
                        runat="server" Height="203px" Width="289px" SelectionMode="Multiple">
                                                            </asp:ListBox>
                                                                                        </td>
            </tr>
            <tr>
                <td class="style2" colspan="3">
                    <asp:Button ID="Button4" runat="server" onclick="Button4_Click" Text="ตกลง" />
                    <asp:Button ID="Button3" runat="server" onclick="Button3_Click" Text="ยกเลิก" />
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="3">
                                    <asp:EntityDataSource ID="EntityDataSource4" runat="server" 
                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                        EnableDelete="True" EnableFlattening="False" EnableInsert="True" 
                                        EnableUpdate="True" EntitySetName="TBL_USERS_PROFILE" AutoGenerateWhereClause="true"
                                        EntityTypeFilter="TBL_USERS_PROFILE">
                                    </asp:EntityDataSource>
                                                <asp:EntityDataSource ID="EntityDataSource2" runat="server" 
                                                    ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                                    EnableFlattening="False" EntitySetName="TBL_M_DEPARTMENT" AutoGenerateWhereClause = "true"
                                                    Select="it.[DEPARTMENT_ID], it.[DEPARTMENT_NAME]">
                                                </asp:EntityDataSource>
                                </td>
            </tr>
            </table>
    </div>
    </div>
    </form>
</body>
</html>
