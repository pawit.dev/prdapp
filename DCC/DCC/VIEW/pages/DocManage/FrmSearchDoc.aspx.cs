﻿using System;
using System.Web.UI;
using DCC.MODEL;
using DCC.Utility;
using DCC.DAL;
using System.Web.UI.WebControls;
using System.IO;

namespace DCC.VIEW.pages.DocManage
{
    public partial class FrmSearchDoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {

                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
                TBL_USERS user = ((TBL_USERS)Session["TBL_USERS"]);
                switch (user.USER_GROUP_ID)
                {
                    case "001":
                    case "002":
                        break;
                    case "003":
                    case "004":
                        string uDir = DALUser.getUserDepFolder(user).Replace('/', ' ').Trim();

                        EntityDataSource1.AutoGenerateWhereClause = false;
                        EntityDataSource1.Where = "it.[DOC_FILE_NAME] LIKE '%" + (uDir) + "%'";
                        break;
                }

            }
            if (GridView2.Rows.Count == 0)
            {
                Label33.Text = MyMessage.SEARCH_NOT_FOUND;
            }
        }

        protected void CMD_SEARCH_Click(object sender, EventArgs e)
        {
            TBL_USERS user = ((TBL_USERS)Session["TBL_USERS"]);
            switch (user.USER_GROUP_ID)
            {
                case "001":
                case "002":
                    EntityDataSource1.AutoGenerateWhereClause = false;
                    EntityDataSource1.Where = "it.[DOC_FILE_NAME] LIKE '%" + (TXT_FN.Text) + "%'";
                    break;
                case "003":
                case "004":
                    string uDir = DALUser.getUserDepFolder(user).Replace('/', ' ').Trim();

                    EntityDataSource1.AutoGenerateWhereClause = false;
                    EntityDataSource1.Where = "it.[DOC_FILE_NAME] LIKE '%" + (uDir + "/" + TXT_FN.Text) + "%'";
                    break;
            }
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label lType = (Label)GridView2.SelectedRow.FindControl("Label1");
            Session["findDoc"] = lType.Text;
            RefreshOpener(Page, "ContentPlaceHolder1_ContentPlaceHolder3_Button3");
        }

        public void RefreshOpener(Page pPage, string strButtonName)
        {
            System.Text.StringBuilder strBuilder = new System.Text.StringBuilder();
            strBuilder.Append("<script>");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("window.opener.document.getElementById('" + strButtonName + "').click();window.close();");
            strBuilder.Append(Environment.NewLine);
            strBuilder.Append("</" + "script>");
            if (!pPage.ClientScript.IsClientScriptBlockRegistered(strBuilder.ToString()))
            {
                pPage.ClientScript.RegisterClientScriptBlock(this.GetType(), "script", strBuilder.ToString());
            }
        }

        protected void CMD_CANCEL_Click(object sender, EventArgs e)
        {
            RefreshOpener(Page, "ContentPlaceHolder1_ContentPlaceHolder3_Button3");
            //RefreshOpener(Page, "ctl00$ContentPlaceHolder1$ContentPlaceHolder3$Button2");
        }

        protected void GridView2_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lType = (Label)e.Row.FindControl("Label1");
                HyperLink hl = (HyperLink)e.Row.FindControl("HyperLink1");
                hl.Text = Path.GetFileName(lType.Text);
                hl.NavigateUrl = "~/VIEW/pages/DocManage/FrmDownload.aspx?doc_path=" + lType.Text;
            }
        }
    }
}