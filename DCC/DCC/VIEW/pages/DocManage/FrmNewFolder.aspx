﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmNewFolder.aspx.cs" Inherits="DCC.VIEW.FrmNewFolder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Document Control Center</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
.font_textbox
{
	font-family: "MS Sans Serif";
    font-size:16px;
    font-weight:normal;
    color:blue;
    text-decoration:none;
	margin-left: 3px;
}
    .font_dropdown
{
	font-family: 'Tahoma';
	font-size: 16px;
	color: #CC6600;
	margin-left: 0px;
}
    </style>
    <link href="../../style/structure.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class ="box_header">สร้างโฟลเดอร์ใหม่</div>
    <div class="box_data">
        <table class="style1">
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label31" runat="server" 
                                                    Text="รหัส" Visible="False"></asp:Label>
                                            </td>
                <td style="text-align: left">
                                                <asp:TextBox ID="TXT_ID" runat="server" CssClass="font_textbox" 
                                                    CausesValidation="True" Visible="False"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                        ControlToValidate="TXT_ID" ErrorMessage="ยังไม่ได้ป้อนรหัส" 
                                                        SetFocusOnError="True" style="color: #FF9933" Visible="False"></asp:RequiredFieldValidator>
                                                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label32" runat="server" 
                                                    Text="ชื่อแผนก"></asp:Label>
                                            </td>
                <td style="text-align: left">
                                                <asp:TextBox ID="TXT_DEPT_NAME" runat="server" CssClass="font_textbox" 
                                                    CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="TXT_DEPT_NAME" ErrorMessage="ไม่ได้ป้อนชื่อ" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label30" runat="server" 
                                                    Text="แผนก"></asp:Label>
                                            </td>
                <td style="text-align: left">
                                                        <asp:DropDownList ID="DDL_DEPT" runat="server" CssClass="font_dropdown" 
                                            DataSourceID="EntityDataSource2" DataTextField="DEPARTMENT_NAME" 
                                            DataValueField="DEPARTMENT_ID">
                                                        </asp:DropDownList>
                                                        <asp:EntityDataSource ID="EntityDataSource2" runat="server" 
                                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                                        AutoGenerateWhereClause = "true"
                                                        EnableFlattening="False" EntitySetName="TBL_M_DEPARTMENT" 
                                                        Select="it.[DEPARTMENT_ID], it.[DEPARTMENT_NAME]">
                                                        </asp:EntityDataSource>
                                                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                                                <asp:Label ID="Label29" runat="server" 
                                                    Text="ชื่อโฟลเดอร์"></asp:Label>
                                            </td>
                <td style="text-align: left">
                                                <asp:TextBox ID="TXT_FOLDER_NAME" runat="server" 
                        CssClass="font_textbox" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                        ControlToValidate="TXT_FOLDER_NAME" ErrorMessage="ยังไม่ได้ป้อนชื่อโฟลเดอร์" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="CMD_OK" runat="server" onclick="CMD_OK_Click" Text="OK" />
                </td>
            </tr>
        </table>
    </div>
    </div>
    </form>
</body>
</html>
