﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="MTitle.aspx.cs" Inherits="DCC.VIEW.pages.Master.MTitle" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc1" %>
<%@ Register src="../../MyMenu.ascx" tagname="MyMenu" tagprefix="uc2" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style1">
        <tr>
            <td colspan="2" style="text-align: right">
                                                    &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
            <div class="box_header">
            คำนำหน้า
            </div>
            <div class="box_data">
                                        <table class="style1" >
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label18" runat="server" CssClass="font_default" Text="ID"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="TXT_ID" runat="server" CssClass="font_textbox" 
                                                        CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                        ControlToValidate="TXT_ID" ErrorMessage="ยังไม่ได้ป้อนรหัส" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label19" runat="server" CssClass="font_default" Text="Name"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="TXT_NAME" runat="server" CssClass="font_textbox" 
                                                        CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="TXT_NAME" ErrorMessage="ยังไม่ได้ป้อนชื่อ" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    &nbsp;</td>
                                                <td  style="text-align: right">
                                    <asp:Button ID="CMD_SEARCH" runat="server" ForeColor="#006600" 
                                        onclick="CMD_Click" Text="ค้นหา" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                                    &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right">
                                    <asp:Button ID="CMD_INSERT" runat="server" ForeColor="#006600" 
                                        onclick="CMD_Click" Text="เพิ่ม" />
                                    <asp:Button ID="CMD_CANCEL" runat="server" CausesValidation="False" 
                                        ForeColor="#006600" onclick="CMD_Click" Text="ยกเลิก" />
                            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                                    &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                                CellPadding="4" DataKeyNames="TITLE_ID" DataSourceID="EntityDataSource1" 
                                ForeColor="Black" GridLines="Vertical" AllowPaging="True" 
                                        onselectedindexchanged="GridView1_SelectedIndexChanged" 
                                        onrowdatabound="GridView1_RowDataBound" 
                                        onrowdeleted="GridView1_RowDeleted">
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="TITLE_ID" HeaderText="รหัส" ReadOnly="True" 
                                                SortExpression="TITLE_ID" />
                                            <asp:BoundField DataField="TITLE_NAME" HeaderText="ชื่อ" 
                                                SortExpression="TITLE_NAME" />
                                            <asp:CommandField ButtonType="Image" 
                                                SelectImageUrl="~/VIEW/images/button_edit.gif" ShowSelectButton="True" />
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                                        CommandName="Delete" ImageUrl="~/VIEW/images/button_remove.gif" Text="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCC99" />
                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                        <RowStyle BackColor="#F7F7DE" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                        <SortedAscendingHeaderStyle BackColor="#848384" />
                                        <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                        <SortedDescendingHeaderStyle BackColor="#575357" />
                                    </asp:GridView>
                            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                                <asp:Label ID="LWARN" runat="server"></asp:Label>
                            </td>
        </tr>
        <tr>
            <td colspan="2">
                                    <asp:EntityDataSource ID="EntityDataSource1" runat="server" 
                                        ConnectionString="name=DCCDBEntities" DefaultContainerName="DCCDBEntities" 
                                        EnableDelete="True" EnableFlattening="False" EnableInsert="True" AutoGenerateWhereClause="true"
                                        EnableUpdate="True" EntitySetName="TBL_TITLE" 
                                    EntityTypeFilter="TBL_TITLE">
                                    </asp:EntityDataSource>
                            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 45px">
                    &nbsp;</td>
            <td>
                    &nbsp;</td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>


<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc2:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>



