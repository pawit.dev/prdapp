﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.Utility;
using DCC.MODEL;
using DCC.DAL;
using System.Text;

namespace DCC.VIEW.pages.User
{
    public partial class ForgotPassword : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TXT_USER.Text = "";
                TXT_USER.Focus();
            }
        }

        protected void CMD_INSERT_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            switch (button.ID)
            {
                case "CMD_REQUEST":
                    if (validate())
                    {
                        StringBuilder msg = new StringBuilder();
                        using (DCCDBEntities context = new DCCDBEntities())
                        {
                            List<TBL_USERS> lists = context.TBL_USERS.Where("it.[USER_NAME] = '" + TXT_USER.Text + "'").ToList();
                            if (lists.Count <= 0)
                            {
                                msg.Append("ไม่พบบัญชีผู้ใช้นี้ในระบบ");
                                msg.Append("\\n");
                            }
                            else
                            {
                                TBL_USERS user = context.TBL_USERS.Where("it.[USER_NAME] = '" + TXT_USER.Text + "'").First();
                                TBL_USERS_PROFILE userProf = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID] = '" + user.USER_ID + "'").First();
                                String pass = RandomPassword.Generate(8, 8);
                                user.USER_PASSWORD = MD5.md5(pass);
                                if (context.SaveChanges() > 0)
                                {
                                    //Send password
                                    TBL_EMAIL email = context.TBL_EMAIL.First();
                                    if (Email.sendMAIL(userProf.PROFILE_EMAIL ,
                                        email.EMAIL_ACCOUNT,
                                        "Forgot password",
                                        "Your new password is " + pass + "",
                                        email.EMAIL_ACCOUNT,
                                        email.EMAIL_PASSWORD,
                                        email.EMAIL_HOST,
                                        null))
                                    {
                                        msg.Append("ระบบได้ส่งรหัสผ่านใหม่ไปยังอีเมล์ที่ได้ลงทะเบียนไว้เรียบร้อยแล้ว");
                                        msg.Append("\\n");
                                    }
                                    else
                                    {
                                        msg.Append("เกิดข้อผิดพลาด โปรดตรวจสอบความถูกต้องของข้อมูลอีกครั้ง");
                                        msg.Append("\\n");
                                    }

                                }
                            }
                        }
                        if (msg.ToString().Length > 0)
                        {
                            Response.Write(Util.ShowMessage(msg.ToString(), "../../Login.aspx"));
                        }
                    }
                    break;
                case "CMD_CANCEL":
                    initial();
                    Response.Redirect("../../Login.aspx");
                    //Response.Write(MD5.md5("password"));
                    break;
            }
        }

        private void initial()
        {
            TXT_USER.Text = "";
        }
        
        private bool validate()
        {
            bool bSucces = true;
            StringBuilder msg = new StringBuilder();
            if (TXT_USER.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนข้อมูลรหัสผู้ใช้");
                msg.Append("\\n");
            }

            if (msg.ToString().Length > 0)
            {
                bSucces = false;
                Response.Write(Util.ShowMessage(msg.ToString(),"ForgotPassword.aspx"));
            }
            return bSucces;
        }
    }
}