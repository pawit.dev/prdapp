﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DCC.MODEL;
using DCC.DAL;
using DCC.Utility;
using System.Text;

namespace DCC.VIEW.pages.User
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        private String formName = "ChangePassword.aspx";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["TBL_USERS"] != null)
                {
                    //set user detail
                    TBL_USERS user = (TBL_USERS)Session["TBL_USERS"];
                    Navigator1.setDetail(DALUser.getUserDetail(user));
                    MyMenu1.setMenu(user);
                    TXT_USER.Text = user.USER_NAME;
                    HUSER_ID.Value = user.USER_ID;
                    TXT_USER.Enabled = false;
                }
                else
                {
                    Response.Redirect("~/View/Logout.aspx");
                }
            }
        }

        protected void CMD_INSERT_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            switch (button.ID)
            {
                case "CMD_UPDATE":
                    if (validate())
                    {
                        using (DCCDBEntities context = new DCCDBEntities())
                        {
                            TBL_USERS user = context.TBL_USERS.First(d => d.USER_ID == HUSER_ID.Value && d.USER_NAME == TXT_USER.Text);
                            user.USER_ID = HUSER_ID.Value;
                            user.USER_NAME = TXT_USER.Text;
                            user.USER_PASSWORD = MD5.md5(TXT_PASS.Text);
                            if (context.SaveChanges() > 0)
                            {
                                Response.Write(Util.ShowMessage(MyMessage.UPDATE_SUCCESS,formName));
                            }
                            else
                            {
                                Response.Write(Util.ShowMessage(MyMessage.UPDATE_FAILURE, formName));
                            }
                        }
                    }
                    break;
                case "ยกเลิก":
                    Response.Redirect(formName);
                    break;
            }
        }
        private bool validate()
        {
            bool bSucces = true;
            StringBuilder msg = new StringBuilder();
            if (TXT_USER.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนรหัสผู้ใช้");
                msg.Append("\\n");
            }
            if (TXT_OLD_PASS.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนรหัสผ่านเก่า");
                msg.Append("\\n");
            }
            if (!TXT_CON_PASS.Text.Equals(TXT_PASS.Text))
            {
                msg.Append("รหัสผ่านไม่ตรงกัน");
                msg.Append("\\n");
            }
            if (TXT_CON_PASS.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนยืนยันรหัสผ่าน");
                msg.Append("\\n");
            }
            if (TXT_PASS.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนรหัสผ่านใหม่");
                msg.Append("\\n");
            }
            //validate old password is correct
            if (!validateOldPass())
            {
                msg.Append("รหัสผ่านเก่าไม่ถูกต้อง");
                msg.Append("\\n");
            }
            if (msg.ToString().Length > 0)
            {
                bSucces = false;
                Response.Write(Util.ShowMessage(msg.ToString(),"ChangePassword.aspx"));
            }
            return bSucces;
        }
       
        private bool validateOldPass()
        {
            bool bExist = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_USERS> users = context.TBL_USERS.Where("it.[USER_ID]='" + HUSER_ID.Value + "' AND it.[USER_NAME]='" + TXT_USER.Text + "' AND it.[USER_PASSWORD]='" + MD5.md5(TXT_OLD_PASS.Text) + "'").ToList();
                if (users.Count > 0)
                {
                    bExist = true;
                }
            }
            return bExist;
        }

    }
}