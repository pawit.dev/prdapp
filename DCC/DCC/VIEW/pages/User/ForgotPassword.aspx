﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme2.Master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="DCC.VIEW.pages.User.ForgotPassword" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style1
        {
            height: 23px;
        width: 100%;
    }
        .style2
        {
            width: 110px;
            height: 28px;
        }
        .style3
        {
            height: 28px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder5">

    <table class="style1" >
                                <tr >
                                    <td >
                                        <div class="box_header">
        ลืมรหัสผ่าน</div>
    <div class="box_data">
                                            <table class="style1" >
                                                <tr >
                                                    <td colspan="2" style="text-align: left;" >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr >
                                                    <td style="text-align: right;"  class="style2">
                                                        <asp:Label ID="Label30" runat="server" CssClass="font_default" 
                                                        Text="ชื่อบัญชีเข้าระบบ"></asp:Label>
                                                    </td>
                                                    <td  class="style3">
                                                        <asp:TextBox ID="TXT_USER" runat="server" Width="164px" CausesValidation="True"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                            ControlToValidate="TXT_USER" ErrorMessage="ยังไม่ได้ป้อนชื่อบัญชี" 
                                                            SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="width: 110px; text-align: right;" >
                                                        &nbsp;</td>
                                                    <td >
                                                        &nbsp;</td>
                                                </tr>
                                                <tr >
                                                    <td style="width: 110px; text-align: right;" >
                                                        &nbsp;</td>
                                                    <td >
                                        <asp:Button ID="CMD_REQUEST" runat="server" ForeColor="Black" 
                                        onclick="CMD_INSERT_Click" Text="ขอรหัสผ่านใหม่" BorderColor="#999999" BorderStyle="Solid" 
                                                            BorderWidth="1px" />
                                        &nbsp;<asp:Button ID="CMD_CANCEL" runat="server" CausesValidation="False" 
                                        ForeColor="Black" onclick="CMD_INSERT_Click" Text="ยกเลิก" BorderColor="#999999" 
                                                            BorderStyle="Solid" BorderWidth="1px" />
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td style="width: 110px; text-align: right;" >
                                                        &nbsp;</td>
                                                    <td >
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr >
                                    <td style="text-align: right" >
                                        &nbsp;</td>
                                </tr>
                            </table>

</asp:Content>


<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>



