﻿<%@ Page Title="" Language="C#" MasterPageFile="~/VIEW/theme/theme1.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="DCC.VIEW.pages.User.ChangePassword" %>
<%@ Register src="../../MyMenu.ascx" tagname="MyMenu" tagprefix="uc1" %>
<%@ Register src="../../Navigator.ascx" tagname="Navigator" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style2">
        <tr>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                        <div class="box_header">
            <asp:Label ID="Label5" runat="server" Text="เปลี่ยนรหัสผ่าน"></asp:Label>
            </div>
            <div class="box_data">
                                        <table class="style1" >
                                            <tr >
                                                <td colspan="2" style="text-align: left;" >
                                                
                                                    &nbsp;</td>
                                            </tr>
                                            <tr >
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label18" runat="server" CssClass="font_default" 
                                                        Text="ชื่อผู้ใช้"></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:TextBox ID="TXT_USER" runat="server" CssClass="font_textbox" Width="164px" 
                                                        CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                        ControlToValidate="TXT_USER" ErrorMessage="ยังไม่ได้ป้อนชื่อผู้ใช้" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="HUSER_ID" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label30" runat="server" CssClass="font_default" 
                                                        Text="รหัสผ่านเก่า"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TXT_OLD_PASS" runat="server" CssClass="font_textbox" 
                                                        TextMode="Password" width="164px" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                        ControlToValidate="TXT_OLD_PASS" ErrorMessage="ยังไม่ได้ป้อนรหัสเก่า" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label19" runat="server" CssClass="font_default" 
                                                        Text="รหัสผ่านใหม่"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TXT_PASS" runat="server" CssClass="font_textbox" 
                                                        TextMode="Password" width="164px" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                        ControlToValidate="TXT_PASS" ErrorMessage="ยังไม่ได้ป้อนรหัสใหม่" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; text-align: right;" >
                                                    <asp:Label ID="Label29" runat="server" CssClass="font_default" 
                                                        Text="ยืนยันรหัสผ่านใหม่"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TXT_CON_PASS" runat="server" CssClass="font_textbox" 
                                                        TextMode="Password" width="164px" CausesValidation="True"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                        ControlToValidate="TXT_CON_PASS" ErrorMessage="ยังไม่ได้ป้อนยืนยันรหัสใหม่" 
                                                        SetFocusOnError="True" style="color: #FF9933"></asp:RequiredFieldValidator>
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                                            ControlToCompare="TXT_PASS" ControlToValidate="TXT_CON_PASS" 
                                                            ErrorMessage="ยืนยันรหัสผ่านไม่ตรงกัน" SetFocusOnError="True"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; text-align: right;" >
                                                    &nbsp;</td>
                                                <td >
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; text-align: right;">
                                                    &nbsp;</td>
                                                <td>
                                    <asp:Button ID="CMD_UPDATE" runat="server" ForeColor="Black" 
                                        onclick="CMD_INSERT_Click" Text="บันทึก" />
                                    &nbsp;<asp:Button ID="CMD_CANCEL" runat="server" CausesValidation="False" 
                                        ForeColor="Black" onclick="CMD_INSERT_Click" Text="ยกเลิก" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; text-align: right;">
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; text-align: right;">
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
            </div>
            </td>
        </tr>

    </table>
    <br />
                                            
            </asp:Content>

<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="ContentPlaceHolder4">
    <uc2:Navigator ID="Navigator1" runat="server" />
</asp:Content>


<asp:Content ID="Content4" runat="server" 
    contentplaceholderid="ContentPlaceHolder2">
    <uc1:MyMenu ID="MyMenu1" runat="server" />
</asp:Content>



<asp:Content ID="Content5" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
    </style>
</asp:Content>




