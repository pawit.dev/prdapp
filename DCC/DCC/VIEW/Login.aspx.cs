﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using DCC.MODEL;
using System.Data.Objects;
using DCC.DAL;
using System.Text;
using DCC.Utility;
namespace DCC.VIEW
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                txtUsername.Focus();
                LoginPanel.Visible = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (validate())
            {
                TBL_USERS user = new TBL_USERS()
                {
                    USER_NAME = txtUsername.Text,
                    USER_PASSWORD = txtPassword.Text

                };
                if (DALUser.checkLogin(ref user))
                {
                    Session["TBL_USERS"] = user;
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    Response.Write(Util.ShowMessage(MyMessage.USERNAMEORPASSWRONG, "Login.aspx"));
                }
            }
        }

        private bool validate()
        {
            bool bSucces = true;
            StringBuilder msg = new StringBuilder();
            if (txtUsername.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนรหัสผู้ใช้");
                msg.Append("\\n");
            }
            if (txtPassword.Text.Equals(""))
            {
                msg.Append("ยังไม่ได้ป้อนรหัสผ่าน");
                msg.Append("\\n");
            }
            if (msg.ToString().Length > 0)
            {
                bSucces = false;
                Response.Write(Util.ShowMessage(msg.ToString(), "Login.aspx"));
            }
            return bSucces;
        }
    }
}
