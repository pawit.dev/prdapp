﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navigator.ascx.cs" Inherits="DCC.VIEW.Navigator" %>

<link href="style/style.css" rel="stylesheet" type="text/css" />
<link href="style/form.css" rel="stylesheet" type="text/css" />
<link href="style/style.css" rel="stylesheet" type="text/css" />
<link href="style/structure.css" rel="stylesheet" type="text/css" />
<table class="navi" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td style="text-align: left">
            <asp:SiteMapPath ID="SiteMapPath1" runat="server" PathSeparator=" : " >
                <CurrentNodeStyle />
                <NodeStyle Font-Bold="True" />
                <PathSeparatorStyle Font-Bold="True"/>
                <RootNodeStyle Font-Bold="True" />
            </asp:SiteMapPath>
        </td>
        <td style="text-align: right">
                <asp:Label ID="L_USER_DETAIL" runat="server" 
                    style="text-align: right" ></asp:Label>
        </td>
    </tr>
</table>


