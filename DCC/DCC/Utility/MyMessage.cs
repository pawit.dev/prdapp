﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCC.Utility
{
    public enum ACTION
    {
        INSERT,
        UPDATE,
        DELETE,
        ADD
    }
    public class MyMessage
    {
        public const String INSERT_SUCCESS = "เพิ่มข้อมูลเรียบร้อยแล้ว";
        public const String INSERT_FAILURE = "เกิดข้อผิดพลาดในการเพิ่มข้อมูล";
        public const String UPDATE_SUCCESS = "แก้ไขข้อมูลเรียบร้อยแล้ว";
        public const String UPDATE_FAILURE = "เกิดข้อผิดพลาดในการแก้ไขข้อมูล";
        public const String DELETE_SUCCESS = "ลบข้อมูลเรียบร้อยแล้ว";
        public const String DELETE_FAILURE = "เกิดข้อผิดพลาดในการลบข้อมูล";
        public const String SEARCH_NOT_FOUND = "ไม่พบข้อมูลที่ท่านค้นหา";
        public const String CONFIRM_DELETE = "คูณต้องการลบข้อมูลนี้หรือไม่";
        public const String BUTTON_INSERT = "เพิ่ม";
        public const String BUTTON_DELETE = "ลบ";
        public const String BUTTON_UPDATE = "แก้ไข";
        public const String BUTTON_SEARCH = "ค้นหา";
        public const String BUTTON_CANCEL = "ยกเลิก";

        public const String DATA_EXIST = "มีข้อมูลนี้อยู่แล้ว";
        public const String FILE_IS_EXIST = "มีไฟล์นี้อยู่ในระบบแล้ว";
        public const String USERNAMEORPASSWRONG = "ไม่สามารถล๊อกอินเข้าสู่ระบบได้ ตรวจสอบรหัสผู้ใช้และรหัสผ่านอีกครั้ง";
        public const String CANNOT_CREATE_DIR = "ไม่สามารถสร้างไดเร็คทอรี่ได้";
        public const String CANNOT_MOVE_DIR = "ไม่สามารถแก้ไขไดเร็คทอรี่ได้";

        public const String CANNOT_DELETE = "ไม่สามารถลบโฟลเดอร์ได้";
        public const String NOT_ASSIGN_DEP_FOLDER = "ยังไม่ได้กำหนด Folder ให้แผนก";
        

    }
}