﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCC.MODEL
{
    public class CListView
    {
        private String type;

        public String Type
        {
            get { return type; }
            set { type = value; }
        }
        private String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        private String title;

        public String Title
        {
            get { return title; }
            set { title = value; }
        }

        private String modified;

        public String Modified
        {
            get { return modified; }
            set { modified = value; }
        }
        private String modifiedBy;

        public String ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }

        private List<CListView> lists;

        public List<CListView> Lists
        {
            get { return lists; }
            set { lists = value; }
        }

        public List<CListView> getListView(List<CListView> list)
        {
            return list;
        }
    }
}