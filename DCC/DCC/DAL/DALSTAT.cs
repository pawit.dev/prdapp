﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DCC.MODEL;
using System.Data.SqlClient;
using System.Data;

namespace DCC.DAL
{
    public class DALSTAT
    {
        public static List<CStat> getStatAll()
        {
            List<CStat> stats = new List<CStat>();
            using (SqlConnection con = new SqlConnection(Connection.cons()))
            {

                SqlCommand cmd = new SqlCommand("GET_STAT_ALL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add(new SqlParameter("@inID", id));
                //cmd.Parameters.Add(new SqlParameter("@intype", type));
                //cmd.Parameters.Add(new SqlParameter("@inFlag", flag));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    CStat stat = new CStat()
                    {
                        Userid = Convert.ToString(dr["EVENT_USER_ID"]),
                        //Docid = Convert.ToString(dr["EVENT_DOC_ID"]),
                        I = Convert.ToInt16(dr["INSERT_DATA"]),
                        D = Convert.ToInt16(dr["DELETE_DATA"]),
                        U = Convert.ToInt16(dr["UPDATE_DATA"]),
                        L = Convert.ToInt16(dr["DOWNLOAD_DATA"])
                    };
                    stats.Add(stat);
                }
                return stats;
            }
        }

        public static List<CStat> getStatByDate(String bDate,String eDate)
        {
            List<CStat> stats = new List<CStat>();
            using (SqlConnection con = new SqlConnection(Connection.cons()))
            {

                SqlCommand cmd = new SqlCommand("GET_STAT_BY_DATE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@bDate", bDate+"0000"));
                cmd.Parameters.Add(new SqlParameter("@eDate", eDate+"2359"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    CStat stat = new CStat()
                    {
                        Userid = Convert.ToString(dr["EVENT_USER_ID"]),
                        //Docid = Convert.ToString(dr["EVENT_DOC_ID"]),
                        I = Convert.ToInt16(dr["INSERT_DATA"]),
                        D = Convert.ToInt16(dr["DELETE_DATA"]),
                        U = Convert.ToInt16(dr["UPDATE_DATA"]),
                        L = Convert.ToInt16(dr["DOWNLOAD_DATA"])
                    };
                    stats.Add(stat);
                }
                return stats;
            }
        }
    }
}