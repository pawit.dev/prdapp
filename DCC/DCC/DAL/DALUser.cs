﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DCC.MODEL;
using System.Data.Objects;
using System.Text;
using DCC.Utility;

namespace DCC.DAL
{
    public class DALUser
    {

        public static StringBuilder message = new StringBuilder();

        public static Boolean validateUser(TBL_USERS user)
        {            
            Boolean bSuccess = true;
            //Clear Old Message
            message.Remove(0, message.Length);

            if (user.USER_NAME.Equals("") || user.USER_NAME == null)
            {
                bSuccess = false;
                message.Append("ยังไม่ได้ป้อนชื่อผู้ใช้");
            }
            else if (user.USER_PASSWORD.Equals("") || user.USER_PASSWORD == null)
            {
                bSuccess = false;
                message.Append("ยังไม่ได้ป้อนรหัส");
            }

            return bSuccess;
        }

        public static Boolean checkLogin(ref TBL_USERS user)
        {
            Boolean bSuccess = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                try
                {
                    TBL_USERS checkUser = context.TBL_USERS.Where("it.[USER_NAME] = '" + user.USER_NAME + "' AND it.[USER_PASSWORD] = '" + MD5.md5(user.USER_PASSWORD) + "' AND it.[USER_STATUS]='A'").First();
                    if (checkUser != null)
                    {
                        if (!checkUser.USER_ID.Equals(""))
                        {                           
                            user = checkUser;
                            userLogin(user.USER_ID);//Timestamp login time
                            bSuccess = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    return false;
                    throw ex;
                }
            }
            return bSuccess;
        }

        public static String getUserDetail(TBL_USERS user)
        {
            StringBuilder sb = new StringBuilder();
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_USERS_PROFILE profile = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + user.USER_ID + "'").First();
                //sb.Append(user.USER_ID+" ");
                sb.Append(profile.TBL_TITLE.TITLE_NAME);
                sb.Append(profile.PROFILE_NAME + " ");
                sb.Append(profile.PROFILE_SURNAME);
                TBL_M_DEPARTMENT dep = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_ID]='"+profile.PROFILE_DEPARTMENT_ID+"'").First();
                sb.Append(" แผนก " + dep.DEPARTMENT_NAME + " [" + profile.TBL_USERS.TBL_M_GROUP.GROUP_NAME + "]");

            }
            return sb.ToString(); ;
        }
        public static String getUserDepFolder(TBL_USERS user)
        {
            StringBuilder sb = new StringBuilder();
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_USERS_PROFILE profile = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + user.USER_ID + "'").First();
                TBL_M_DEPARTMENT dep = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_ID]='" + profile.PROFILE_DEPARTMENT_ID + "'").First();
                sb.Append(dep.DEPARTMENT_FOLDER);

            }
            return sb.ToString();
        }
        public static String getUserDepFolder(String userID)
        {
            StringBuilder sb = new StringBuilder();
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_USERS_PROFILE profile = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + userID + "'").First();
                TBL_M_DEPARTMENT dep = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_ID]='" + profile.PROFILE_DEPARTMENT_ID + "'").First();
                sb.Append(dep.DEPARTMENT_FOLDER);

            }
            return sb.ToString();
        }
        public static TBL_USERS_PROFILE getUsers(String userID)
        {
            TBL_USERS_PROFILE user = null;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_USERS_PROFILE> users = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='" + userID + "'").ToList();
                if (users.Count > 0)
                {
                    user = users.First();
                }
            }
            return user;
        }
        public static String getRootPath(String userID)
        {
            String path = "";
            using (DCCDBEntities context = new DCCDBEntities())
            {
                List<TBL_USERS_PROFILE> profiles = context.TBL_USERS_PROFILE.Where("it.[PROFILE_USER_ID]='"+userID+"'").ToList();
                if (profiles.Count > 0)
                { 
                    List<TBL_M_DEPARTMENT> deps = context.TBL_M_DEPARTMENT.Where("it.[DEPARTMENT_ID]='"+profiles[0].PROFILE_DEPARTMENT_ID+"'").ToList();
                    if(deps.Count>0)
                    {
                        if (!String.IsNullOrEmpty(deps[0].DEPARTMENT_FOLDER))
                        {
                            path = "/"+ deps[0].DEPARTMENT_FOLDER.Split('/')[1];                            
                        }
                    }
                    
                }
            }
            return path;
        }
        //public static String getUserID(TBL_USERS _user)
        //{
        //    if (_user != null)
        //    {
        //        return _user.USER_ID;
        //    }
        //    else {
        //        return "491105";
        //    }
        //}
        public static bool userLogin(String userId)
        {
            Boolean bSuccess = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_USERS_LOGS log = new TBL_USERS_LOGS();
                log.USER_ID = userId;
                log.USER_LOG_DATE = Util.toDate3(DateTime.Now);
                log.USER_LOGIN_DATE = Util.toDate2(DateTime.Now);
                context.TBL_USERS_LOGS.AddObject(log);
                if (context.SaveChanges() > 0)
                {
                    bSuccess = true;
                }
            }
            return bSuccess;
        }

        public static bool userLogOut(String userId)
        {
            Boolean bSuccess = false;
            using (DCCDBEntities context = new DCCDBEntities())
            {
                TBL_USERS_LOGS docEvent = context.TBL_USERS_LOGS.Where(
                    "it.[USER_ID]='" + userId + "' AND it.[USER_LOG_DATE]='"+Util.toDate3(DateTime.Now)+"'"+
                    " AND it.[USER_LOGOUT_DATE] IS NULL"                                
                ).First();
                //TBL_USERS_LOGS docEvent = context.TBL_USERS_LOGS.Where("it.[USER_ID] ='"+userId+"' AND USER_LOGIN=''").First();
                docEvent.USER_LOGOUT_DATE = Util.toDate2(DateTime.Now);
                if (context.SaveChanges() > 0)
                {
                    bSuccess = true;
                }
            }
            return bSuccess;
        }
    }
}