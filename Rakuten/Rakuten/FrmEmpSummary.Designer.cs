﻿namespace Rakuten
{
    partial class FrmEmpSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nickNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rUNNINGDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.guestReservationDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CheckOUT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.incomeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soldMemberDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fineDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Late = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reMainDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surnameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDayDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ageDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cEmployeeSummaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CBO_PERIOD = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.DP_START = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.DTP_DAY = new System.Windows.Forms.DateTimePicker();
            this.RD_EVERYDAY = new System.Windows.Forms.RadioButton();
            this.RD_EVERY15DAY = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BProcess = new System.Windows.Forms.Button();
            this.B_SAVE = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeSummaryBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nickNameDataGridViewTextBoxColumn1,
            this.iDDataGridViewTextBoxColumn1,
            this.CheckIN,
            this.rUNNINGDataGridViewTextBoxColumn1,
            this.guestReservationDataGridViewTextBoxColumn1,
            this.CheckOUT,
            this.incomeDataGridViewTextBoxColumn1,
            this.soldMemberDataGridViewTextBoxColumn1,
            this.fineDataGridViewTextBoxColumn1,
            this.Late,
            this.reMainDataGridViewTextBoxColumn1,
            this.typeDataGridViewTextBoxColumn1,
            this.periodDataGridViewTextBoxColumn1,
            this.statusDataGridViewTextBoxColumn1,
            this.endDateDataGridViewTextBoxColumn1,
            this.startDateDataGridViewTextBoxColumn1,
            this.sexDataGridViewTextBoxColumn1,
            this.titleDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn1,
            this.surnameDataGridViewTextBoxColumn1,
            this.nationDataGridViewTextBoxColumn1,
            this.birthDayDataGridViewTextBoxColumn1,
            this.addressDataGridViewTextBoxColumn1,
            this.mobileDataGridViewTextBoxColumn1,
            this.emailDataGridViewTextBoxColumn1,
            this.ageDataGridViewTextBoxColumn1,
            this.dETAILDataGridViewTextBoxColumn1,
            this.kEYDATEDataGridViewTextBoxColumn1,
            this.lASTEDITDataGridViewTextBoxColumn1,
            this.kEYUSERDataGridViewTextBoxColumn1,
            this.fLAGDataGridViewTextBoxColumn1,
            this.remarkDataGridViewTextBoxColumn1});
            this.dataGridView1.DataSource = this.cEmployeeSummaryBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(16, 118);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(955, 452);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            // 
            // nickNameDataGridViewTextBoxColumn1
            // 
            this.nickNameDataGridViewTextBoxColumn1.DataPropertyName = "NickName";
            this.nickNameDataGridViewTextBoxColumn1.HeaderText = "ชื่อเล่น";
            this.nickNameDataGridViewTextBoxColumn1.Name = "nickNameDataGridViewTextBoxColumn1";
            this.nickNameDataGridViewTextBoxColumn1.ReadOnly = true;
            this.nickNameDataGridViewTextBoxColumn1.Width = 80;
            // 
            // iDDataGridViewTextBoxColumn1
            // 
            this.iDDataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn1.HeaderText = "เบอร์";
            this.iDDataGridViewTextBoxColumn1.Name = "iDDataGridViewTextBoxColumn1";
            this.iDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn1.Width = 60;
            // 
            // CheckIN
            // 
            this.CheckIN.DataPropertyName = "CheckIN";
            this.CheckIN.HeaderText = "เข้างาน";
            this.CheckIN.Name = "CheckIN";
            this.CheckIN.Width = 80;
            // 
            // rUNNINGDataGridViewTextBoxColumn1
            // 
            this.rUNNINGDataGridViewTextBoxColumn1.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn1.HeaderText = "รอบงาน";
            this.rUNNINGDataGridViewTextBoxColumn1.Name = "rUNNINGDataGridViewTextBoxColumn1";
            this.rUNNINGDataGridViewTextBoxColumn1.ReadOnly = true;
            this.rUNNINGDataGridViewTextBoxColumn1.Width = 80;
            // 
            // guestReservationDataGridViewTextBoxColumn1
            // 
            this.guestReservationDataGridViewTextBoxColumn1.DataPropertyName = "GuestReservation";
            this.guestReservationDataGridViewTextBoxColumn1.HeaderText = "แขกจอง";
            this.guestReservationDataGridViewTextBoxColumn1.Name = "guestReservationDataGridViewTextBoxColumn1";
            this.guestReservationDataGridViewTextBoxColumn1.Width = 80;
            // 
            // CheckOUT
            // 
            this.CheckOUT.DataPropertyName = "CheckOUT";
            this.CheckOUT.HeaderText = "ออกงาน";
            this.CheckOUT.Name = "CheckOUT";
            this.CheckOUT.Width = 80;
            // 
            // incomeDataGridViewTextBoxColumn1
            // 
            this.incomeDataGridViewTextBoxColumn1.DataPropertyName = "Income";
            this.incomeDataGridViewTextBoxColumn1.HeaderText = "รายรับ";
            this.incomeDataGridViewTextBoxColumn1.Name = "incomeDataGridViewTextBoxColumn1";
            this.incomeDataGridViewTextBoxColumn1.Width = 80;
            // 
            // soldMemberDataGridViewTextBoxColumn1
            // 
            this.soldMemberDataGridViewTextBoxColumn1.DataPropertyName = "SoldMember";
            this.soldMemberDataGridViewTextBoxColumn1.HeaderText = "ขายสมาชิก";
            this.soldMemberDataGridViewTextBoxColumn1.Name = "soldMemberDataGridViewTextBoxColumn1";
            this.soldMemberDataGridViewTextBoxColumn1.Width = 90;
            // 
            // fineDataGridViewTextBoxColumn1
            // 
            this.fineDataGridViewTextBoxColumn1.DataPropertyName = "Fine";
            this.fineDataGridViewTextBoxColumn1.HeaderText = "หักเงิน";
            this.fineDataGridViewTextBoxColumn1.Name = "fineDataGridViewTextBoxColumn1";
            this.fineDataGridViewTextBoxColumn1.Width = 80;
            // 
            // Late
            // 
            this.Late.DataPropertyName = "Late";
            this.Late.HeaderText = "สาย(บาท)";
            this.Late.Name = "Late";
            // 
            // reMainDataGridViewTextBoxColumn1
            // 
            this.reMainDataGridViewTextBoxColumn1.DataPropertyName = "ReMain";
            this.reMainDataGridViewTextBoxColumn1.HeaderText = "คงเหลือ";
            this.reMainDataGridViewTextBoxColumn1.Name = "reMainDataGridViewTextBoxColumn1";
            this.reMainDataGridViewTextBoxColumn1.Width = 80;
            // 
            // typeDataGridViewTextBoxColumn1
            // 
            this.typeDataGridViewTextBoxColumn1.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn1.HeaderText = "Type";
            this.typeDataGridViewTextBoxColumn1.Name = "typeDataGridViewTextBoxColumn1";
            this.typeDataGridViewTextBoxColumn1.Visible = false;
            // 
            // periodDataGridViewTextBoxColumn1
            // 
            this.periodDataGridViewTextBoxColumn1.DataPropertyName = "Period";
            this.periodDataGridViewTextBoxColumn1.HeaderText = "Period";
            this.periodDataGridViewTextBoxColumn1.Name = "periodDataGridViewTextBoxColumn1";
            this.periodDataGridViewTextBoxColumn1.Visible = false;
            // 
            // statusDataGridViewTextBoxColumn1
            // 
            this.statusDataGridViewTextBoxColumn1.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn1.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn1.Name = "statusDataGridViewTextBoxColumn1";
            this.statusDataGridViewTextBoxColumn1.Visible = false;
            // 
            // endDateDataGridViewTextBoxColumn1
            // 
            this.endDateDataGridViewTextBoxColumn1.DataPropertyName = "EndDate";
            this.endDateDataGridViewTextBoxColumn1.HeaderText = "EndDate";
            this.endDateDataGridViewTextBoxColumn1.Name = "endDateDataGridViewTextBoxColumn1";
            this.endDateDataGridViewTextBoxColumn1.Visible = false;
            this.endDateDataGridViewTextBoxColumn1.Width = 80;
            // 
            // startDateDataGridViewTextBoxColumn1
            // 
            this.startDateDataGridViewTextBoxColumn1.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn1.HeaderText = "StartDate";
            this.startDateDataGridViewTextBoxColumn1.Name = "startDateDataGridViewTextBoxColumn1";
            this.startDateDataGridViewTextBoxColumn1.Visible = false;
            this.startDateDataGridViewTextBoxColumn1.Width = 80;
            // 
            // sexDataGridViewTextBoxColumn1
            // 
            this.sexDataGridViewTextBoxColumn1.DataPropertyName = "Sex";
            this.sexDataGridViewTextBoxColumn1.HeaderText = "Sex";
            this.sexDataGridViewTextBoxColumn1.Name = "sexDataGridViewTextBoxColumn1";
            this.sexDataGridViewTextBoxColumn1.Visible = false;
            // 
            // titleDataGridViewTextBoxColumn1
            // 
            this.titleDataGridViewTextBoxColumn1.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn1.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn1.Name = "titleDataGridViewTextBoxColumn1";
            this.titleDataGridViewTextBoxColumn1.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.Visible = false;
            // 
            // surnameDataGridViewTextBoxColumn1
            // 
            this.surnameDataGridViewTextBoxColumn1.DataPropertyName = "Surname";
            this.surnameDataGridViewTextBoxColumn1.HeaderText = "Surname";
            this.surnameDataGridViewTextBoxColumn1.Name = "surnameDataGridViewTextBoxColumn1";
            this.surnameDataGridViewTextBoxColumn1.Visible = false;
            // 
            // nationDataGridViewTextBoxColumn1
            // 
            this.nationDataGridViewTextBoxColumn1.DataPropertyName = "Nation";
            this.nationDataGridViewTextBoxColumn1.HeaderText = "Nation";
            this.nationDataGridViewTextBoxColumn1.Name = "nationDataGridViewTextBoxColumn1";
            this.nationDataGridViewTextBoxColumn1.Visible = false;
            // 
            // birthDayDataGridViewTextBoxColumn1
            // 
            this.birthDayDataGridViewTextBoxColumn1.DataPropertyName = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn1.HeaderText = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn1.Name = "birthDayDataGridViewTextBoxColumn1";
            this.birthDayDataGridViewTextBoxColumn1.Visible = false;
            // 
            // addressDataGridViewTextBoxColumn1
            // 
            this.addressDataGridViewTextBoxColumn1.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn1.HeaderText = "Address";
            this.addressDataGridViewTextBoxColumn1.Name = "addressDataGridViewTextBoxColumn1";
            this.addressDataGridViewTextBoxColumn1.Visible = false;
            // 
            // mobileDataGridViewTextBoxColumn1
            // 
            this.mobileDataGridViewTextBoxColumn1.DataPropertyName = "Mobile";
            this.mobileDataGridViewTextBoxColumn1.HeaderText = "Mobile";
            this.mobileDataGridViewTextBoxColumn1.Name = "mobileDataGridViewTextBoxColumn1";
            this.mobileDataGridViewTextBoxColumn1.Visible = false;
            // 
            // emailDataGridViewTextBoxColumn1
            // 
            this.emailDataGridViewTextBoxColumn1.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn1.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn1.Name = "emailDataGridViewTextBoxColumn1";
            this.emailDataGridViewTextBoxColumn1.Visible = false;
            // 
            // ageDataGridViewTextBoxColumn1
            // 
            this.ageDataGridViewTextBoxColumn1.DataPropertyName = "Age";
            this.ageDataGridViewTextBoxColumn1.HeaderText = "Age";
            this.ageDataGridViewTextBoxColumn1.Name = "ageDataGridViewTextBoxColumn1";
            this.ageDataGridViewTextBoxColumn1.Visible = false;
            // 
            // dETAILDataGridViewTextBoxColumn1
            // 
            this.dETAILDataGridViewTextBoxColumn1.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn1.HeaderText = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn1.Name = "dETAILDataGridViewTextBoxColumn1";
            this.dETAILDataGridViewTextBoxColumn1.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn1
            // 
            this.kEYDATEDataGridViewTextBoxColumn1.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn1.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn1.Name = "kEYDATEDataGridViewTextBoxColumn1";
            this.kEYDATEDataGridViewTextBoxColumn1.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn1
            // 
            this.lASTEDITDataGridViewTextBoxColumn1.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn1.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn1.Name = "lASTEDITDataGridViewTextBoxColumn1";
            this.lASTEDITDataGridViewTextBoxColumn1.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn1
            // 
            this.kEYUSERDataGridViewTextBoxColumn1.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn1.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn1.Name = "kEYUSERDataGridViewTextBoxColumn1";
            this.kEYUSERDataGridViewTextBoxColumn1.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn1
            // 
            this.fLAGDataGridViewTextBoxColumn1.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn1.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn1.Name = "fLAGDataGridViewTextBoxColumn1";
            this.fLAGDataGridViewTextBoxColumn1.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn1
            // 
            this.remarkDataGridViewTextBoxColumn1.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn1.HeaderText = "REMARK";
            this.remarkDataGridViewTextBoxColumn1.Name = "remarkDataGridViewTextBoxColumn1";
            this.remarkDataGridViewTextBoxColumn1.Visible = false;
            // 
            // cEmployeeSummaryBindingSource
            // 
            this.cEmployeeSummaryBindingSource.DataSource = typeof(Rakuten.Structure.CEmployeeSummary);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CBO_PERIOD);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.DP_START);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox2.Location = new System.Drawing.Point(602, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(297, 62);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "เลือกงวดที่สรุปค่ามือ";
            // 
            // CBO_PERIOD
            // 
            this.CBO_PERIOD.FormattingEnabled = true;
            this.CBO_PERIOD.Items.AddRange(new object[] {
            "งวดที่ 1",
            "งวดที่ 2"});
            this.CBO_PERIOD.Location = new System.Drawing.Point(56, 26);
            this.CBO_PERIOD.Name = "CBO_PERIOD";
            this.CBO_PERIOD.Size = new System.Drawing.Size(73, 21);
            this.CBO_PERIOD.TabIndex = 148;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(135, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 14);
            this.label1.TabIndex = 147;
            this.label1.Text = "ประจำ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(15, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 14);
            this.label15.TabIndex = 146;
            this.label15.Text = "งวดที่";
            // 
            // DP_START
            // 
            this.DP_START.CustomFormat = "MM-yyyy";
            this.DP_START.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DP_START.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DP_START.Location = new System.Drawing.Point(179, 21);
            this.DP_START.Name = "DP_START";
            this.DP_START.Size = new System.Drawing.Size(99, 26);
            this.DP_START.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(85, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(184, 25);
            this.label6.TabIndex = 157;
            this.label6.Text = "สรุปค่ามือพนักงาน";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.DTP_DAY);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(16, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(289, 62);
            this.groupBox1.TabIndex = 158;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ประจำวันที่";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(6, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 14);
            this.label2.TabIndex = 148;
            this.label2.Text = "วันที่";
            // 
            // DTP_DAY
            // 
            this.DTP_DAY.CustomFormat = "dd/MM/yyyy";
            this.DTP_DAY.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_DAY.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP_DAY.Location = new System.Drawing.Point(41, 21);
            this.DTP_DAY.Name = "DTP_DAY";
            this.DTP_DAY.Size = new System.Drawing.Size(125, 26);
            this.DTP_DAY.TabIndex = 2;
            // 
            // RD_EVERYDAY
            // 
            this.RD_EVERYDAY.AutoSize = true;
            this.RD_EVERYDAY.Checked = true;
            this.RD_EVERYDAY.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_EVERYDAY.Location = new System.Drawing.Point(514, 64);
            this.RD_EVERYDAY.Name = "RD_EVERYDAY";
            this.RD_EVERYDAY.Size = new System.Drawing.Size(53, 17);
            this.RD_EVERYDAY.TabIndex = 159;
            this.RD_EVERYDAY.TabStop = true;
            this.RD_EVERYDAY.Text = "ทุกวัน";
            this.RD_EVERYDAY.UseVisualStyleBackColor = true;
            this.RD_EVERYDAY.Visible = false;
            this.RD_EVERYDAY.CheckedChanged += new System.EventHandler(this.RD_EVERYDAY_CheckedChanged);
            // 
            // RD_EVERY15DAY
            // 
            this.RD_EVERY15DAY.AutoSize = true;
            this.RD_EVERY15DAY.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RD_EVERY15DAY.Location = new System.Drawing.Point(514, 89);
            this.RD_EVERY15DAY.Name = "RD_EVERY15DAY";
            this.RD_EVERY15DAY.Size = new System.Drawing.Size(82, 17);
            this.RD_EVERY15DAY.TabIndex = 160;
            this.RD_EVERY15DAY.Text = "ทุก ๆ 15 วัน";
            this.RD_EVERY15DAY.UseVisualStyleBackColor = true;
            this.RD_EVERY15DAY.Visible = false;
            this.RD_EVERY15DAY.CheckedChanged += new System.EventHandler(this.RD_EVERYDAY_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.product;
            this.pictureBox1.Location = new System.Drawing.Point(12, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(67, 50);
            this.pictureBox1.TabIndex = 155;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(989, 50);
            this.pictureBox2.TabIndex = 156;
            this.pictureBox2.TabStop = false;
            // 
            // BProcess
            // 
            this.BProcess.Image = global::Rakuten.Properties.Resources.document_view;
            this.BProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BProcess.Location = new System.Drawing.Point(319, 64);
            this.BProcess.Name = "BProcess";
            this.BProcess.Size = new System.Drawing.Size(94, 42);
            this.BProcess.TabIndex = 3;
            this.BProcess.Text = "แสดงข้อมูล";
            this.BProcess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BProcess.UseVisualStyleBackColor = true;
            this.BProcess.Click += new System.EventHandler(this.BProcess_Click);
            // 
            // B_SAVE
            // 
            this.B_SAVE.Image = global::Rakuten.Properties.Resources.disk_blue_ok;
            this.B_SAVE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.B_SAVE.Location = new System.Drawing.Point(867, 576);
            this.B_SAVE.Name = "B_SAVE";
            this.B_SAVE.Size = new System.Drawing.Size(104, 42);
            this.B_SAVE.TabIndex = 4;
            this.B_SAVE.Text = "บันทึกข้อมูล";
            this.B_SAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.B_SAVE.UseVisualStyleBackColor = true;
            this.B_SAVE.Click += new System.EventHandler(this.B_SAVE_Click);
            // 
            // FrmEmpSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(989, 630);
            this.Controls.Add(this.RD_EVERY15DAY);
            this.Controls.Add(this.RD_EVERYDAY);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.BProcess);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.B_SAVE);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEmpSummary";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "สรุปค่ามือพนักงาน";
            this.Load += new System.EventHandler(this.FrmEmpSummary_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeSummaryBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource cEmployeeSummaryBindingSource;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button BProcess;
        private System.Windows.Forms.Button B_SAVE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DateTimePicker DP_START;
        private System.Windows.Forms.ComboBox CBO_PERIOD;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RD_EVERYDAY;
        private System.Windows.Forms.RadioButton RD_EVERY15DAY;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker DTP_DAY;
        private System.Windows.Forms.DataGridViewTextBoxColumn nickNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CheckIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn guestReservationDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CheckOUT;
        private System.Windows.Forms.DataGridViewTextBoxColumn incomeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn soldMemberDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fineDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Late;
        private System.Windows.Forms.DataGridViewTextBoxColumn reMainDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn surnameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDayDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ageDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn1;
    }
}