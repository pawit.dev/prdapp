﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CSchedule:CMaster
    {
        private string _timeIn;
        private string _timeOut;
        private string _number;

        private string _T1200;
        private string _T1230;

        private string _T1300;
        private string _T1330;

        private string _T1400;
        private string _T1430;

        private string _T1500;
        private string _T1530;

        private string _T1600;
        private string _T1630;

        private string _T1700;
        private string _T1730;

        private string _T1800;
        private string _T1830;

        private string _T1900;
        private string _T1930;

        private string _T2000;
        private string _T2030;

        private string _T2100;
        private string _T2130;

        private string _T2200;
        private string _T2230;

        private string _T2300;
        private string _T2330;

        private string _T0000;
        private string _T0030;

        private string _T0100;
        private string _T0130;

        private string _T0200;
        private string _T0230;

        private string _T0300;
        private string _T0330;

        private string _T0400;
        private string _T0430;

        private string _T0500;
        private string _T0530;

        private string _T0600;
        private string _T0630;

        private string _T0700;
        private string _T0730;

        private string _T0800;
        private string _T0830;

        private string _T0900;
        private string _T0930;

        private string _T1000;
        private string _T1030;

        private string _T1100;
        private string _T1130;
        ////
        public string Number
        {
            set { this._number = value; }
            get { return this._number; }
        }
        public string TimeIn
        {
            set { this._timeIn = value; }
            get { return this._timeIn; }
        }
        public string TimeOut
        {
            set { this._timeOut = value; }
            get { return this._timeOut; }
        }

        public string T1200 
        {
            set { this._T1200 = value; }
            get { return this._T1200; }
        }
        public string T1230
        {
            set { this._T1230 = value; }
            get { return this._T1230; }
        }

        public string T1300
        {
            set { this._T1300 = value; }
            get { return this._T1300; }
        }
        public string T1330
        {
            set { this._T1330 = value; }
            get { return this._T1330; }
        }

        public string T1400
        {
            set { this._T1400 = value; }
            get { return this._T1400; }
        }
        public string T1430
        {
            set { this._T1430 = value; }
            get { return this._T1430; }
        }

        public string T1500
        {
            set { this._T1500 = value; }
            get { return this._T1500; }
        }
        public string T1530
        {
            set { this._T1530 = value; }
            get { return this._T1530; }
        }

        public string T1600
        {
            set { this._T1600 = value; }
            get { return this._T1600; }
        }
        public string T1630
        {
            set { this._T1630 = value; }
            get { return this._T1630; }
        }

        public string T1700
        {
            set { this._T1700 = value; }
            get { return this._T1700; }
        }
        public string T1730
        {
            set { this._T1730 = value; }
            get { return this._T1730; }
        }

        public string T1800
        {
            set { this._T1800 = value; }
            get { return this._T1800; }
        }
        public string T1830
        {
            set { this._T1830 = value; }
            get { return this._T1830; }
        }

        public string T1900
        {
            set { this._T1900 = value; }
            get { return this._T1900; }
        }
        public string T1930
        {
            set { this._T1930 = value; }
            get { return this._T1930; }
        }

        public string T2000
        {
            set { this._T2000 = value; }
            get { return this._T2000; }
        }
        public string T2030
        {
            set { this._T2030 = value; }
            get { return this._T2030; }
        }

        public string T2100
        {
            set { this._T2100 = value; }
            get { return this._T2100; }
        }
        public string T2130
        {
            set { this._T2130 = value; }
            get { return this._T2130; }
        }

        public string T2200
        {
            set { this._T2200 = value; }
            get { return this._T2200; }
        }
        public string T2230
        {
            set { this._T2230 = value; }
            get { return this._T2230; }
        }

        public string T2300
        {
            set { this._T2300 = value; }
            get { return this._T2300; }
        }
        public string T2330
        {
            set { this._T2330 = value; }
            get { return this._T2330; }
        }

        public string T0000
        {
            set { this._T0000 = value; }
            get { return this._T0000; }
        }

        public string T0030
        {
            set { this._T0030 = value; }
            get { return this._T0030; }
        }

        public string T0100
        {
            set { this._T0100 = value; }
            get { return this._T0100; }
        }

        public string T0130
        {
            set { this._T0130 = value; }
            get { return this._T0130; }
        }

        public string T0200
        {
            set { this._T0200 = value; }
            get { return this._T0200; }
        }

        public string T0230
        {
            set { this._T0230 = value; }
            get { return this._T0230; }
        }

        public string T0300
        {
            set { this._T0300 = value; }
            get { return this._T0300; }
        }

        public string T0330
        {
            set { this._T0330 = value; }
            get { return this._T0330; }
        }

        public string T0400
        {
            set { this._T0400 = value; }
            get { return this._T0400; }
        }

        public string T0430
        {
            set { this._T0430 = value; }
            get { return this._T0430; }
        }

        public string T0500
        {
            set { this._T0500 = value; }
            get { return this._T0500; }
        }

        public string T0530
        {
            set { this._T0530 = value; }
            get { return this._T0530; }
        }

        public string T0600
        {
            set { this._T0600 = value; }
            get { return this._T0600; }
        }

        public string T0630
        {
            set { this._T0630 = value; }
            get { return this._T0630; }
        }

        public string T0700
        {
            set { this._T0700 = value; }
            get { return this._T0700; }
        }

        public string T0730
        {
            set { this._T0730 = value; }
            get { return this._T0730; }
        }

        public string T0800
        {
            set { this._T0800 = value; }
            get { return this._T0800; }
        }

        public string T0830
        {
            set { this._T0830 = value; }
            get { return this._T0830; }
        }

        public string T0900
        {
            set { this._T0900 = value; }
            get { return this._T0900; }
        }

        public string T0930
        {
            set { this._T0930 = value; }
            get { return this._T0930; }
        }

        public string T1000
        {
            set { this._T1000 = value; }
            get { return this._T1000; }
        }

        public string T1030
        {
            set { this._T1030 = value; }
            get { return this._T1030; }
        }

        public string T1100
        {
            set { this._T1100 = value; }
            get { return this._T1100; }
        }

        public string T1130
        {
            set { this._T1130 = value; }
            get { return this._T1130; }
        }
    }
}
