﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CReport17 : CCustomerTransaction
    {
        private double therapistPrice;//ค่ามือพนักงานนวด
        private double packagePrice;//ราคา package
        private double priceUsed;//จำนวนที่ใช้ไป

        private string u1;
        private string u2;
        private string u3;
        private string u4;
        private string u5;
        private string u6;
        private string u7;
        private string u8;
        private string u9;
        private string u10;
        private string u11;
        private string u12;
        private string u13;
        private string u14;
        private string u15;
        private string u16;
        private string u17;
        private string u18;
        private string u19;
        private string u20;
        private string u21;
        private string u22;
        private string u23;
        private string u24;
        private string u25;
        private string u26;
        private string u27;
        private string u28;
        private string u29;
        private string u30;
        private string u31;

        public double TherapistPrice
        {
            set { this.therapistPrice = value; }
            get { return this.therapistPrice; }
        }
        public double PackagePrice
        {
            set { this.packagePrice = value; }
            get { return this.packagePrice; }
        }
        public double PriceUsed
        {
            set { this.priceUsed = value; }
            get { return this.priceUsed; }
        }

        public string U1 { set { this.u1 = value; } get { return this.u1; } }
        public string U2 { set { this.u2 = value; } get { return this.u2; } }
        public string U3 { set { this.u3 = value; } get { return this.u3; } }
        public string U4 { set { this.u4 = value; } get { return this.u4; } }
        public string U5 { set { this.u5 = value; } get { return this.u5; } }
        public string U6 { set { this.u6 = value; } get { return this.u6; } }
        public string U7 { set { this.u7 = value; } get { return this.u7; } }
        public string U8 { set { this.u8 = value; } get { return this.u8; } }
        public string U9 { set { this.u9 = value; } get { return this.u9; } }
        public string U10 { set { this.u10 = value; } get { return this.u10; } }
        public string U11 { set { this.u11 = value; } get { return this.u11; } }
        public string U12 { set { this.u12 = value; } get { return this.u12; } }
        public string U13 { set { this.u13 = value; } get { return this.u13; } }
        public string U14 { set { this.u14 = value; } get { return this.u14; } }
        public string U15 { set { this.u15 = value; } get { return this.u15; } }
        public string U16 { set { this.u16 = value; } get { return this.u16; } }
        public string U17 { set { this.u17 = value; } get { return this.u17; } }
        public string U18 { set { this.u18 = value; } get { return this.u18; } }
        public string U19 { set { this.u19 = value; } get { return this.u19; } }
        public string U20 { set { this.u20 = value; } get { return this.u20; } }
        public string U21 { set { this.u21 = value; } get { return this.u21; } }
        public string U22 { set { this.u22 = value; } get { return this.u22; } }
        public string U23 { set { this.u23 = value; } get { return this.u23; } }
        public string U24 { set { this.u24 = value; } get { return this.u24; } }
        public string U25 { set { this.u25 = value; } get { return this.u25; } }
        public string U26 { set { this.u26 = value; } get { return this.u26; } }
        public string U27 { set { this.u27 = value; } get { return this.u27; } }
        public string U28 { set { this.u28 = value; } get { return this.u28; } }
        public string U29 { set { this.u29 = value; } get { return this.u29; } }
        public string U30 { set { this.u30 = value; } get { return this.u30; } }
        public string U31 { set { this.u31 = value; } get { return this.u31; } }

    }
}
