﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CEmployee : CPerson
    {
        private string _empid;
        private string _type;
        private string _period;
        private string _status;
        private double _biz;
        private double _holiday;
        private double _sick;

        public string EmpID
        {
            set { this._empid = value; }
            get { return this._empid; }
        }
        public string Type
        {
            set { this._type = value; }
            get { return this._type; }
        }
        public string Period
        {
            set { this._period = value; }
            get { return this._period; }
        }
        public string Status
        {
            set { this._status = value; }
            get { return this._status; }
        }
        public double Biz
        {
            set { this._biz = value; }
            get { return this._biz; }
        }
        public double Holiday
        {
            set { this._holiday = value; }
            get { return this._holiday; }
        }
        public double Sick
        {
            set { this._sick = value; }
            get { return this._sick; }
        }
    }
}
