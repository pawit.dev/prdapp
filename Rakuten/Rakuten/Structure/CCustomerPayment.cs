﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CCustomerPayment:CMaster
    {
        private string cusTranID;
        private string aCCode;
        private double discount;
        private double amount;
        private string payType;
        private string slip;
        private string bank;
        private string _status;

        public string CustomerTranID
        {
            set { this.cusTranID = value; }
            get { return this.cusTranID; }
        }
        public string AccountCode
        {
            set { this.aCCode = value; }
            get { return this.aCCode; }
        }
        public double Amount
        {
            set { this.amount = value; }
            get { return this.amount; }
        }
        public double Discount
        {
            set { this.discount = value; }
            get { return this.discount; }
        }
        public string paymentType
        {
            set { this.payType = value; }
            get { return this.payType; }
        }
        public string Slip
        {
            set { this.slip = value; }
            get { return this.slip; }
        }
        public string Bank 
        {
            set { this.bank = value; }
            get { return this.bank; }
        }
        public string status
        {
            set { this._status = value; }
            get { return this._status; }
        }
    }
}

