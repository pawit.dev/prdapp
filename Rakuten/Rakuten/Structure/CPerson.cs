﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CPerson : CMaster
    {
        private string _sex;
        private string _title;
        private string _name;
        private string _surname;
        private string _nickname;
        private string _nation;
        private DateTime _birthDay;
        private string _address;
        private string _mobile;
        private string _email;
        private int _age;

        public string Sex 
        {
            set { this._sex = value; }
            get { return this._sex; }
        }
        public string Title 
        {
            set { this._title = value; }
            get { return this._title; }
        }
        public string Name 
        {
            set { this._name = value; }
            get { return this._name; }
        }
        public string Surname 
        {
            set { this._surname = value; }
            get { return this._surname; }
        }
        public string NickName
        {
            set { this._nickname = value; }
            get { return this._nickname; }
        }
        public string Nation 
        {
            set { this._nation = value; }
            get { return this._nation; }
        }
        public DateTime BirthDay 
        {
            set { this._birthDay = value; }
            get { return this._birthDay; }
        }
        public string Address 
        {
            set { this._address = value; }
            get { return this._address; }
        }
        public string Mobile 
        {
            set { this._mobile = value; }
            get { return this._mobile; }
        }
        public string Email 
        {
            set { this._email = value; }
            get { return this._email; }
        }
        public int Age
        {
            set { this._age = value; }
            get { return this._age; }
        }
    }
}
