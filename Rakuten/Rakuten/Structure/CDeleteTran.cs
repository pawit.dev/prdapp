﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CDeleteTran : CMaster
    {
        private string period;

        public string Period
        {
            set { this.period = value; }
            get { return this.period; }
        }
    }
}
