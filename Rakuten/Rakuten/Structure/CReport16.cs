﻿
namespace Rakuten.Structure
{
    public class CReport16:CCustomerTransaction
    {
        private int amount1;
        private int amount2;
        public int Amount1
        {
            set { this.amount1 = value; }
            get { return this.amount1; }
        }
        public int Amount2
        {
            set { this.amount2 = value; }
            get { return this.amount2; }
        }
    }
}
    