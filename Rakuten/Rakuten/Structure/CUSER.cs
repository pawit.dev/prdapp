﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CUSER:CEmployee
    {
        private string pass;
        private string permission;
        private string user;
        private string personid;
        public string USER 
        {
            set { this.user = value; }
            get { return this.user; }
        }
        public string PASSWORD
        {
            set { this.pass = value; }
            get { return this.pass; }
        }
        public string PERMISSION
        {
            set { this.permission = value; }
            get { return this.permission; }
        }
        public string PERSONID 
        {
            set { this.personid = value; }
            get { return this.personid; }
        }
    }
}
