﻿using System;
using System.Collections.Generic;
using System.Text;
using Rakuten.DAL;
using Rakuten.Utility;
using Rakuten.Structure;

namespace Rakuten.Structure
{
    public class CMemberPayment : CMember
    {
        private string accountCode;
        private double amount;
        private double discount;
        private string paytype;
        private string slip;
        private string bank;
        public string AccountCode
        {
            set { this.accountCode = value; }
            get { return this.accountCode; }
        }
        public double Amount
        {
            set { this.amount = value; }
            get { return this.amount; }
        }
        public double Discount
        {
            set { this.discount = value; }
            get { return this.discount; }
        }
        public string PaymentType
        {
            set { this.paytype = value; }
            get { return this.paytype; }
        }
        public string Slip
        {
            set { this.slip = value; }
            get { return this.slip; }
        }
        public string Bank
        {
            set { this.bank = value; }
            get { return this.bank; }
        }
    }
}
