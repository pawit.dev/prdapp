﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CEmployeeSummary:CEmployee
    {
        private int guestReservation;
        private string income;
        private string soldMember;
        private string fine;
        private double late;
        private string reMain;
        private string checkin;
        private string checkout;
        public string CheckIN
        {
            set { this.checkin = value; }
            get { return this.checkin; }
        }
        public string CheckOUT
        {
            set { this.checkout = value; }
            get { return this.checkout; }
        }
        public double Late
        {
            set { this.late = value; }
            get { return this.late; }
        }
        public int GuestReservation
        {
            set { this.guestReservation = value; }
            get { return this.guestReservation; }
        }
        public string Income
        {
            set { this.income = value; }
            get { return this.income; }
        }
        public string SoldMember
        {
            set { this.soldMember = value; }
            get { return this.soldMember; }
        }
        public string Fine
        {
            set { this.fine = value; }
            get { return this.fine; }
        }
        public string ReMain
        {
            set { this.reMain = value; }
            get { return this.reMain; }
        }
    }
}
