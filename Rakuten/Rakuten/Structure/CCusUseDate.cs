﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rakuten.Structure
{
    public class CCusUseDate
    {
        private string RmemberID;
        private string Mname;
        private string Msurname;
        private string RTherapistName;
        private string RTherapistReservName;
        private DateTime RDate;
        private DateTime RTime;

        public string MemberID
        {
            set { this.RmemberID = value; }
            get { return this.RmemberID; }
        }
        public string Name
        {
            set { this.Mname = value; }
            get { return this.Mname; }
        }
        public string Surname
        {
            set { this.Msurname = value; }
            get { return this.Msurname; }
        }
        public string TherapistName
        {
            set { this.RTherapistName = value; }
            get { return this.RTherapistName; }
        }
        public string TherapistReservName
        {
            set { this.RTherapistReservName = value; }
            get { return this.RTherapistReservName; }
        }
        public DateTime useDate
        {
            set { this.RDate = value; }
            get { return this.RDate; }
        }
        public DateTime useTime
        {
            set { this.RTime = value; }
            get { return this.RTime; }
        }
    }
}
