﻿
namespace Rakuten.Structure
{
    public class CReport3 : CScent
    {
        private int rec;
        private int pay;
        private int remain;

        public int REC
        {
            set { this.rec = value; }
            get { return this.rec; }
        }
        public int PAY
        {
            set { this.pay = value; }
            get { return this.pay; }
        }
        public int REMAIN
        {
            set { this.remain = value; }
            get { return this.remain; }
        }
    }
}
