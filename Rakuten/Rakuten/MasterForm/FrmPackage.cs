﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Data.SqlClient;
namespace Rakuten
{
    public partial class FrmPackage : Form
    {
        private FormStatus formStatus = FormStatus.Normal;
        private DBStatus status = DBStatus.Insert;//Initial Status
        public List<CPackage> lists = null;
        public int index;
        private bool bSearch = false;
        public bool bSelected = false;
        public string group = "";
        public FrmPackage()//Normal
        {
            InitializeComponent();
        }

        public FrmPackage(FormStatus _fStatus)//Screen
        {
            InitializeComponent();
            this.formStatus = _fStatus;
        }
        public FrmPackage(string _group,FormStatus _fStatus)//Screen
        {
            InitializeComponent();
            this.formStatus = _fStatus;
            this.group = _group;
        }
        private void FrmPackage_Load(object sender, EventArgs e)
        {
            initial();
            search();
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                search();
            }
        }

        private void TXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void CheckIsNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                Management.ShowMsg(Management.NUMBER_ONLY);
            }
        }

        private void B_Search_Click(object sender, EventArgs e)
        {
            bSearch = true;
            search();
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {
            if (TXT_ID.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_ID.Focus();
            }
            else if (TXT_NAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_NAME.Focus();
            }
            else if(TXT_COST.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_COST.Focus();
            }
            else if(TXT_THB.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_THB.Focus();
            }
            else if(TXT_PERCOURSE.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_PERCOURSE.Focus();
            }
            else
            {
                switch (status)
                {
                    case DBStatus.Insert://INSERT
                        try
                        {

                            CPackage package = new CPackage
                                            {
                                                ID = TXT_ID.Text,
                                                DETAIL = TXT_NAME.Text,
                                                Hour = CBO_HR.SelectedValue.ToString(),
                                                Price = Convert.ToDouble(TXT_COST.Text),
                                                MoneyUnit = TXT_THB.Text,
                                                MassageType = CBO_TYPE.SelectedValue.ToString(),
                                                PerCourse = Convert.ToInt16(TXT_PERCOURSE.Text),
                                                CustomerType = CBO_CUSTYPE.SelectedValue.ToString(),
                                                TherapistPrice = Convert.ToDouble(TXT_THERAPIST_PRICE.Text),
                                                StartDate = DTP_START.Value,
                                                EndDate = DTP_END.Value,
                                                KEYDATE = DateTime.Now,
                                                KEYUSER = Authorize.getUser(),
                                                LASTEDIT = DateTime.Now,
                                                FLAG = Management.Insert//Insert
                                            };
                            if (DALPackage.manageMaster(package) > 0)
                            {
                                MessageBox.Show(Management.INSERT_SUCCESS);
                                initial();
                                search();
                            }
                            else {
                                MessageBox.Show("Test");
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                    case DBStatus.Update://UPDATE
                        try
                        {
                            CPackage package = new CPackage
                            {
                                ID = TXT_ID.Text,
                                DETAIL = TXT_NAME.Text,
                                Hour = CBO_HR.SelectedValue.ToString(),
                                Price = Convert.ToDouble(TXT_COST.Text),
                                MoneyUnit = TXT_THB.Text,
                                MassageType = CBO_TYPE.SelectedValue.ToString(),
                                PerCourse = Convert.ToInt16(TXT_PERCOURSE.Text),
                                CustomerType = CBO_CUSTYPE.SelectedValue.ToString(),
                                TherapistPrice = Convert.ToDouble(TXT_THERAPIST_PRICE.Text),
                                StartDate = DTP_START.Value,
                                EndDate = DTP_END.Value,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.Update//Update
                            };
                            if (DALPackage.manageMaster(package) > 0)
                            {
                                MessageBox.Show(Management.UPDATE_SUCCESS);
                                initial();
                                search();
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                    case DBStatus.Delete://DELETE
                        try
                        {
                            CPackage package = new CPackage
                            {
                                ID = TXT_ID.Text,
                                DETAIL = TXT_NAME.Text,
                                Hour = CBO_HR.SelectedValue.ToString(),
                                Price = Convert.ToDouble(TXT_COST.Text),
                                MoneyUnit = TXT_THB.Text,
                                MassageType = CBO_TYPE.SelectedValue.ToString(),
                                PerCourse = Convert.ToInt16(TXT_PERCOURSE.Text),
                                CustomerType = CBO_CUSTYPE.SelectedValue.ToString(),
                                TherapistPrice = Convert.ToDouble(TXT_THERAPIST_PRICE.Text),
                                StartDate = DTP_START.Value,
                                EndDate = DTP_END.Value,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.Delete//Update
                            };
                            if (DALPackage.manageMaster(package) > 0)
                            {
                                MessageBox.Show(Management.DELETE_SUCCESS);
                                initial();
                                search();
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                }
            }
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            initial();
            search();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView1.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView1.ClearSelection();
                    dataGridView1.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
                bSelected = true;
                Close();
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 3:
                    e.Value = Management.getDetail(MasterList.MHour, e.Value.ToString());
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (formStatus == FormStatus.Normal)
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ \n หรือ คลิ๊กขวาเพื่อ เพิ่ม,ปรับปรุง,ลบ ข้อมูล";
                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ";
                }
            }
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    initial();
                    break;
            }
        }

        private void RD_ALL_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            if (dataGridView1.Rows.Count > 0)
            {
                //Reset Old select row
                dataGridView1.Rows[index].Selected = false;
                dataGridView1.Rows[0].Selected = true;
                txtSearch.Focus();
            }
        }

        private void CMS_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem b = (ToolStripMenuItem)sender;
            tabControl1.TabPages.Add(tabPage2);
            tabPage2.Controls.Add(panel1);
            tabControl1.SelectTab(1);
            groupBox2.Enabled = false;
            RD_ALL.Checked = true;
            txtSearch.Text = "";
            switch (b.Name)
            {
                case "CMS_Insert":
                    status = DBStatus.Insert;
                    B_SAVE.Text = "เพิ่ม";
                    TXT_ID.Text = "";// getIDSequence();
                    TXT_ID.Focus();
                    break;
                case "CMS_Update":
                    status = DBStatus.Update;
                    B_SAVE.Text = "ปรับปรุง";
                    TXT_ID.Text = lists[index].ID;
                    TXT_NAME.Text = lists[index].DETAIL;
                    CBO_TYPE.SelectedValue = lists[index].MassageType;
                    setPriceLabel(lists[index].MassageType);//Show Label
                    CBO_HR.SelectedValue = lists[index].Hour+"";
                    TXT_COST.Text = lists[index].Price+"";
                    TXT_PERCOURSE.Text = lists[index].PerCourse+"";
                    TXT_THB.Text = lists[index].MoneyUnit;
                    DTP_START.Value = lists[index].StartDate;
                    DTP_END.Value = lists[index].EndDate;
                    CBO_CUSTYPE.SelectedValue = lists[index].CustomerType;
                    TXT_THERAPIST_PRICE.Text = lists[index].TherapistPrice.ToString();
                    TXT_NAME.Select();
                    TXT_ID.ReadOnly = true;
                    break;
                case "CMS_Delete":
                    status = DBStatus.Delete;
                    B_SAVE.Text = "ลบ";
                    TXT_ID.Text = lists[index].ID;
                    TXT_NAME.Text = lists[index].DETAIL;
                    CBO_TYPE.SelectedValue = lists[index].MassageType;
                    setPriceLabel(lists[index].MassageType);//Show Label
                    CBO_HR.SelectedValue = lists[index].Hour;
                    TXT_COST.Text = lists[index].Price + "";
                    TXT_PERCOURSE.Text = lists[index].PerCourse + "";
                    TXT_THB.Text = lists[index].MoneyUnit;
                    DTP_START.Value = lists[index].StartDate;
                    DTP_END.Value = lists[index].EndDate;
                    CBO_CUSTYPE.SelectedValue = lists[index].CustomerType;
                    TXT_THERAPIST_PRICE.Text = lists[index].TherapistPrice.ToString();
                    B_SAVE.Focus();
                    TXT_ID.ReadOnly = true;
                    TXT_NAME.ReadOnly = true;
                    break;
            }
        }
        private void B_STYPE_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Name)
            {
                case "B_STYPE":
                    FrmSearch search = new FrmSearch(MasterList.MMassageType, FormStatus.Search);
                    search.ShowDialog();
                    if (search.bSelected)
                    {
                        CBO_TYPE.SelectedValue = search.masters[search.index].ID;
                    }
                    break;
                case "B_SHR":
                    FrmSearch search1 = new FrmSearch(MasterList.MHour, FormStatus.Search);
                    search1.ShowDialog();
                    if (search1.bSelected)
                    {
                        CBO_HR.SelectedValue = search1.masters[search1.index].ID;
                    }
                    break;
            }
        }
        private void search()
        {
            lists = DALPackage.getList(new CPackage
                                        {
                                            ID = txtSearch.Text,
                                            DETAIL = txtSearch.Text,
                                            Hour = "1",
                                            Price = 1,
                                            MoneyUnit = "THB",
                                            MassageType = "",
                                            PerCourse = 1,
                                            CustomerType =(this.group.Equals(""))? "0001": this.group,
                                            TherapistPrice = 0,
                                            StartDate = DateTime.Now,
                                            EndDate = DateTime.Now,
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = (this.group.Equals("")) ? getSearchStatus() : "7"//Searh Flag (7 flag ไว้แสดงกลุ่มของ package)
                                        }
                                    );
            //Check Have Data!!
            if (lists.Count == 0)
            {
                dataGridView1.DataSource = null;
                if (bSearch)
                {
                    Management.ShowMsg(Management.NO_FOUND_DATA);
                }
            }
            else
            {
                dataGridView1.DataSource = lists;
                //L_TOTAL.Text = String.Format("{0:##}", lists.Count);
            }
        }

        private void initial()
        {
            groupBox2.Enabled = true;
            TXT_ID.ReadOnly = false;
            TXT_NAME.ReadOnly = false;
            TXT_ID.Text = "";
            TXT_NAME.Text = "";
            TXT_PERCOURSE.Text = "";
            TXT_THB.Text = "THB";
            TXT_COST.Text = "";
            TXT_THERAPIST_PRICE.Text = "";
            txtSearch.Text = "";
            txtSearch.Focus();
            tabControl1.TabPages.RemoveAt(1);
            //Massage Type
            CBO_TYPE.DataSource = DALMaster.getList(MasterList.MMassageType,
                                        new CMaster
                                        {
                                            ID = "",
                                            DETAIL = "",
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = Management.SearchAll//Searh Flag
                                        }
                                        );
            //Hour
            CBO_HR.DataSource = DALMaster.getList(MasterList.MHour,
                                        new CMaster
                                        {
                                            ID = "",
                                            DETAIL = "",
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = Management.SearchAll//Searh Flag
                                        }
                                        );
            //Customer Type
            CBO_CUSTYPE.DataSource = DALMaster.getList(MasterList.MCustomerType,
                                                        new CMaster
                                                        {
                                                            ID = "",
                                                            DETAIL = "",
                                                            KEYDATE = DateTime.Now,
                                                            KEYUSER = Authorize.getUser(),
                                                            LASTEDIT = DateTime.Now,
                                                            FLAG = Management.SearchAll//Searh Flag
                                                        }
                                        );
            DTP_END.Value = DateTime.Now.AddYears(1);
                
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Set User Permission
            CMS_Insert.Enabled = (formStatus == FormStatus.Normal) ? Authorize.GetAuthorize(this.Name)[0] : false;
            CMS_Update.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[1]) && (dataGridView1.Rows.Count > 0)) ? true : false;
            CMS_Delete.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[2]) && (dataGridView1.Rows.Count > 0)) ? true : false;
        }

        //private string getIDSequence()
        //{
        //    //Reload master data
        //    RD_ALL.Checked = true;
        //    search();
        //    //Begin Gen Sequence
        //    int seq = 1;
        //    for (int i = 0; i < lists.Count; i++)
        //    {
        //        if (seq != Convert.ToInt16(lists[i].ID))
        //        {
        //            break;
        //        }
        //        else
        //        {
        //            seq++;
        //        }
        //    }
        //    return String.Format("{0:0000}", seq); 
        //}

        private string getSearchStatus()
        {
            //Search Status If
            //3:ALL
            //4:ID
            //5:NAME
            return (RD_ALL.Checked) ? Management.SearchAll : ((RD_ID.Checked) ? Management.SearchByID : Management.SearchByDetail);
        }

        private void CBO_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBO_TYPE.SelectedValue != null)
            {
                setPriceLabel(CBO_TYPE.SelectedValue.ToString());
            }
        }
        private void setPriceLabel(string packageID)
        {
            switch (packageID)
            {
                case "0012":
                    label5.Text = "";
                    label16.Text = "มูลค่าที่ได้";
                    break;
                default:
                    label5.Text = "* คอร์ส";
                    label16.Text = "ราคาต่อ";
                    break;
            }
        }

    }
}
