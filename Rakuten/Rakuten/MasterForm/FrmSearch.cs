﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Data.SqlClient;
namespace Rakuten
{
    public partial class FrmSearch : Form
    {
        private FormStatus formStatus = FormStatus.Normal;
        private DBStatus status = DBStatus.Insert;//Initial Status
        private MasterList screen = MasterList.MTypeEmployee;//Initial Screen
        public List<CMaster> masters = null;
        public int index;
        private bool bSearch = false;
        public bool bSelected = false;
        public FrmSearch()//Normal
        {
            InitializeComponent();
        }

        public FrmSearch(MasterList _screen,FormStatus _fStatus)//Screen
        {
            InitializeComponent();
            this.screen = _screen;
            this.formStatus = _fStatus;
            switch (_screen)
            { 
                case MasterList.MAccountCode:
                    label1.Text = "ตั้งค่ารหัสค่าใช้จ่าย";
                    break;
                case MasterList.MBank:
                    label1.Text = "ตั้งค่าธนาคาร";
                    break;
                case MasterList.MCustomerType:
                    label1.Text = "ตั้งค่าประเภทลูกค้า";
                    break;
                case MasterList.MHour:
                    label1.Text = "ตั้งค่าชั่วโมง";
                    break;
                case MasterList.MLeave:
                    label1.Text = "ตั้งค่าวันลา";
                    break;
                case MasterList.MMassageType:
                    label1.Text = "ตั้งค่าประเภทการนวด";
                    break;
                case MasterList.MNation:
                    label1.Text = "ตั้งค่าเชื้อชาติ";
                    break;
                case MasterList.MPaymentStatus:
                    label1.Text = "ตั้งค่าสถานะการชำระเงิน";
                    break;
                case MasterList.MPaymentType:
                    label1.Text = "ตั้งค่าประเภทการชำระเงิน";
                    break;
                case MasterList.MSex:
                    label1.Text = "ตั้งค่าเพศ";
                    break;
                case MasterList.MTitle:
                    label1.Text = "ตั้งค่าคำนำหน้าชื่อ";
                    break;
                case MasterList.MTypeEmployee:
                    label1.Text = "ตั้งค่าประเภทพนักงาน";
                    break;

            }
        }

        private void frmSearch_Load(object sender, EventArgs e)
        {
            initial();
            search();
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                search();
            }
        }

        private void TXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void CheckIsNumber_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void B_Search_Click(object sender, EventArgs e)
        {
            bSearch = true;
            search();
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {
            if (TXT_ID.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_ID.Focus();
            }
            else if (TXT_NAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_NAME.Focus();
            }
            else
            {
                switch (status)
                {
                    case DBStatus.Insert://INSERT
                        try
                        {
                            CMaster master = new CMaster
                                        {
                                            ID = TXT_ID.Text,
                                            DETAIL = TXT_NAME.Text,
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = Management.Insert//Insert
                                        };
                            if (DALMaster.manageMaster(screen, master) > 0)
                            {
                                MessageBox.Show(Management.INSERT_SUCCESS);
                                initial();
                                search();  
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                    case DBStatus.Update://UPDATE
                        try
                        {
                            CMaster master = new CMaster
                            {
                                ID = TXT_ID.Text,
                                DETAIL = TXT_NAME.Text,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.Update//Update
                            };
                            if (DALMaster.manageMaster(screen, master) > 0)
                            {
                                MessageBox.Show(Management.UPDATE_SUCCESS);
                                initial();
                                search();
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                    case DBStatus.Delete://DELETE
                        try
                        {
                            CMaster master = new CMaster
                            {
                                ID = TXT_ID.Text,
                                DETAIL = TXT_NAME.Text,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.Delete//Delete
                            };
                            if (DALMaster.manageMaster(screen, master) > 0)
                            {
                                MessageBox.Show(Management.DELETE_SUCCESS);
                                initial();
                                search();
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                }
            }
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            initial();
            search();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView1.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView1.ClearSelection();
                    dataGridView1.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
                bSelected = true;
                Close();
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.CellStyle.ForeColor = Color.Green;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (formStatus == FormStatus.Normal)
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ \n หรือ คลิ๊กขวาเพื่อ เพิ่ม,ปรับปรุง,ลบ ข้อมูล";
                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ";
                }
            }
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                    initial();
                    break;
            }
        }

        private void RD_ALL_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            if (dataGridView1.Rows.Count > 0)
            {
                //Reset Old select row
                dataGridView1.Rows[index].Selected = false;
                dataGridView1.Rows[0].Selected = true;
                txtSearch.Focus();
            }
        }

        private void CMS_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem b = (ToolStripMenuItem)sender;
            tabControl1.TabPages.Add(tabPage2);
            tabPage2.Controls.Add(panel1);
            tabControl1.SelectTab(1);
            groupBox2.Enabled = false;
            RD_ALL.Checked = true;
            txtSearch.Text = "";
            switch (b.Name)
            {
                case "CMS_Insert":
                    status = DBStatus.Insert;
                    B_SAVE.Text = "เพิ่ม";
                    TXT_ID.Text = getIDSequence();
                    TXT_ID.Focus();
                    break;
                case "CMS_Update":
                    status = DBStatus.Update;
                    B_SAVE.Text = "ปรับปรุง";
                    TXT_ID.Text = masters[index].ID;
                    TXT_NAME.Text = masters[index].DETAIL;
                    TXT_NAME.Select();
                    TXT_ID.ReadOnly = true;
                    break;
                case "CMS_Delete":
                    status = DBStatus.Delete;
                    B_SAVE.Text = "ลบ";
                    TXT_ID.Text = masters[index].ID;
                    TXT_NAME.Text = masters[index].DETAIL;
                    B_SAVE.Focus();
                    TXT_ID.ReadOnly = true;
                    TXT_NAME.ReadOnly = true;
                    break;
            }
        }

        private void search()
        {
            masters = DALMaster.getList(screen, 
                                        new CMaster
                                        {
                                            ID = txtSearch.Text,
                                            DETAIL = txtSearch.Text,
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = getSearchStatus()//Searh Flag
                                        }
                                    );
            //Check Have Data!!
            if (masters.Count == 0)
            {
                dataGridView1.DataSource = null;
                if (bSearch)
                {
                    Management.ShowMsg(Management.NO_FOUND_DATA);
                }
            }
            else
            {
                dataGridView1.DataSource = masters;
                L_TOTAL.Text = String.Format("{0:##}", masters.Count);
            }
        }

        private void initial()
        {
            groupBox2.Enabled = true;
            TXT_ID.ReadOnly = false;
            TXT_NAME.ReadOnly = false;
            TXT_ID.Text = "";
            TXT_NAME.Text = "";
            txtSearch.Text = "";
            txtSearch.Focus();
            tabControl1.TabPages.RemoveAt(1);
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Set User Permission
            CMS_Insert.Enabled = (formStatus == FormStatus.Normal) ? Authorize.GetAuthorize(this.Name)[0] : false;
            CMS_Update.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[1]) && (dataGridView1.Rows.Count > 0)) ? true : false;
            CMS_Delete.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[2]) && (dataGridView1.Rows.Count > 0)) ? true : false;
        }

        private string getIDSequence()
        {
            //Reload master data
            RD_ALL.Checked = true;
            search();
            //Begin Gen Sequence
            int seq = 1;
            for (int i = 0; i < masters.Count; i++)
            {
                if (seq != Convert.ToInt16(masters[i].ID))
                {
                    break;
                }
                else
                {
                    seq++;
                }
            }
            return String.Format("{0:0000}", seq); 
        }

        private string getSearchStatus()
        {
            //Search Status If
            //3:ALL
            //4:ID
            //5:NAME
            return (RD_ALL.Checked) ? Management.SearchAll : ((RD_ID.Checked) ? Management.SearchByID : Management.SearchByDetail);
        }

    }
}
