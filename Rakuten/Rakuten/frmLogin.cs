using System;
using System.Windows.Forms;
using System.Collections.Generic;
using Rakuten.Utility;
using Rakuten.Structure;
using Rakuten.DAL;
using System.Web.Mail;
namespace Rakuten
{
    public partial class frmLogin : Form
    {
        public string click = "";
        private int count = 1;
        public frmLogin()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            click = "CANCEL";
            Application.Exit();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (UsernameTextBox.Text.Equals("-") && PasswordTextBox.Text.Equals("-"))
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Text = "";
                UsernameTextBox.Focus();
            }
            else
            {
                if (checkLogin(UsernameTextBox.Text, PasswordTextBox.Text))
                {
                    click = "OK";
                    Close();
                }
                else
                {
                    MessageBox.Show(Management.LOGIN_FAIL, Application.ProductName);
                    PasswordTextBox.SelectAll();
                    PasswordTextBox.Focus();
                }
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            LAPPTITLE.Text = Application.ProductName;
            LAPPCOMPANY.Text = Application.CompanyName;
            LUPDATE_DATE.Text = "Version "+Application.ProductVersion;
            UsernameTextBox.Focus();
        }

        private void PasswordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {

                if (UsernameTextBox.Text.Equals("admin") && PasswordTextBox.Text.Equals("Config"))
                {
                    count++;
                    if (count == 5)
                    {
                        Close();
                        //frmSetting setting = new frmSetting();
                        //setting.ShowDialog();
                    }
                }
                else
                {
                    if (UsernameTextBox.Text.Equals("-") && PasswordTextBox.Text.Equals("-"))
                    {
                        UsernameTextBox.Text = "";
                        PasswordTextBox.Text = "";
                        UsernameTextBox.Focus();
                    }
                    else
                    {
                        if (checkLogin(UsernameTextBox.Text, PasswordTextBox.Text))
                        {
                            click = "OK";
                            Close();
                        }
                        else
                        {
                            
                            MessageBox.Show(Management.LOGIN_FAIL, Application.ProductName);
                            PasswordTextBox.SelectAll();
                            PasswordTextBox.Focus();
                        }
                    }
                }
            }
        }

        public Boolean checkLogin(string user,string pass)
        {
            Cursor = Cursors.WaitCursor;
            string msg = "n";
            List<CUSER> luser = DALUser.GetLogin(
                                new CUSER
                                {
                                    USER = user,
                                    PASSWORD = pass,
                                    Type = "",
                                    PERMISSION  ="",
                                    KEYUSER = "0000",
                                    FLAG = "4"
                                }
                                );
            if (luser.Count > 0)
            {
                Authorize.setKeyUser(
                    luser[0].ID,
                    luser[0].Name + "  " + luser[0].Surname,
                    luser[0].Type);
                msg = "y";
            }
            else
            {
                msg = "n";
            }
            Cursor = Cursors.Default;
            return (msg == "y") ? true : false;
        }

        public Boolean developerAccess()
        {
                Authorize.setKeyUser(
                    "0000",
                    "pawit saeeaung",
                    "Admin");
            return true;
        }

        private void UsernameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            { 
                case Keys.F1:
                    if (e.KeyCode == Keys.F1)
                    {
                        if (UsernameTextBox.Text.Equals("hui"))//Back Door
                        {
                            developerAccess();
                            click = "OK";
                            Close();
                        }
                    }
                    break;
                case Keys.F2:
                    #region test send email
                    Cursor = Cursors.WaitCursor;
                    //WORK--->
                    List<string> s = new List<string>();
                    s.Add(@"c:\test.xls");
                    SendSMS.sendMAIL(
                        ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "TO")),//to
                        ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "FROM")),//From
                        "test Rakuten spa daily report (" + DateTime.Now.ToString() + ").",//subject
                        "test Rakuten spa daily report (" + DateTime.Now.ToString() + ").",//body
                        ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "USER")),//user
                        ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "PASS")),//pass
                        ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "HOST")),//stmp server
                        s
                                    );
                    //"AM"
                    Cursor = Cursors.Default;
                    #endregion end test
                    break;
            }
        }

    }
}