﻿using System;
using System.Windows.Forms;
using Rakuten.Structure;
using Rakuten.Utility;
using Rakuten.DAL;
using System.Data.SqlClient;

namespace Rakuten
{
    public enum ScheduleType
    { 
        CheckIn,
        CheckOut,
        Delete
    }
    public partial class FrmTimeLine : Form
    {
        private FrmMain main;

        private string Therapist;

        public FrmTimeLine(FrmMain _main, ScheduleType type,string _id)
        {
            InitializeComponent();
            this.main = _main;
            this.Therapist = _id;
            switch (type)
            { 
                case ScheduleType.CheckIn:
                    CMD_CHECKIN.Visible = true;
                    CMD_CHECKOUT.Visible = false;
                    CMD_DELETE.Visible = false;
                    CMD_CHECKIN.Focus();
                    //Set Current Q
                    TXT_Q.Visible = true;   
                    CSchedule sche = new CSchedule
                    {
                        ID = "",
                        Remark = "",
                        KEYUSER = "",
                        KEYDATE = DateTime.Now,
                        FLAG = "9"//ตรวจสอบว่ามีการลงเวลาเข้างานยัง
                    };
                    TXT_Q.Text = DALSchedule.getQ(sche).ToString();
                    break;
                case ScheduleType.CheckOut:
                    CMD_CHECKIN.Visible = false;
                    CMD_CHECKOUT.Visible = true;
                    CMD_DELETE.Visible = false;
                    CMD_CHECKOUT.Focus();
                    TXT_Q.Visible = false;  
                    break;
                case ScheduleType.Delete:
                    CMD_CHECKIN.Visible = false;
                    CMD_CHECKOUT.Visible = false;
                    CMD_DELETE.Visible = true;
                    CMD_DELETE.Focus();
                    TXT_Q.Visible = false;  
                    break;
            }

        }

        private void CMD_CHECKIN_Click(object sender, EventArgs e)
        {
            try
            {
                CSchedule sche1 = new CSchedule
                {
                    ID = Therapist,
                    Remark = TXT_Q.Text,
                    KEYUSER = Authorize.getUser(),
                    KEYDATE = DTP_SERVICE_DATE.Value,
                    FLAG = "4"//ตรวจสอบว่ามีการลงเวลาเข้างานยัง
                };
                CSchedule sche2 = new CSchedule
                {
                    ID = Therapist,
                    Remark = TXT_Q.Text,
                    KEYUSER = Authorize.getUser(),
                    KEYDATE = DTP_SERVICE_DATE.Value,
                    FLAG = "0"//บันทึกเวลาเข้างาน
                };
                if (DALSchedule.checkCount(sche1) > 0)
                {
                    Management.ShowMsg("ไม่สามารถลงเวลาเข้างานซ้ำได้");
                }
                else
                {
                    if (DALEmployee.manageEmployeeSchedule(sche2) > 0)
                    {
                        Management.ShowMsg("ลงเวลาเข้างานเรียบร้อยแล้ว");
                    }
                    else {
                        MessageBox.Show("Test");
                    }
                }
            }
            catch (SqlException ex)
            {
                Management.ShowMsg(ex.Message);
            }
            main.refreshSchedule();
            Close();
        }

        private void CMD_CHECKOUT_Click(object sender, EventArgs e)
        {
            try
            {
                CSchedule sche1 = new CSchedule
                {
                    ID = Therapist,
                    Remark = TXT_Q.Text,
                    KEYUSER = Authorize.getUser(),
                    KEYDATE = DTP_SERVICE_DATE.Value,
                    FLAG = "4"//ตรวจสอบว่ามีการลงเวลาเข้างานยัง
                };
                CSchedule sche2 = new CSchedule
                {
                    ID = Therapist,
                    Remark = TXT_Q.Text,
                    KEYUSER = Authorize.getUser(),
                    KEYDATE = DTP_SERVICE_DATE.Value,
                    FLAG = "1"
                };
                if (DALSchedule.checkCount(sche1) > 0)
                {
                    if (DALEmployee.manageEmployeeSchedule(sche2) > 0)
                    {
                        MessageBox.Show("ลงเวลาออกงานเรียบร้อยแล้ว");
                    }
                }
                else
                {
                    MessageBox.Show("ยังไม่ได้ลงเวลาเข้างาน");
                }
            }
            catch (SqlException ex)
            {
                Management.ShowMsg(ex.Message);
            }
            main.refreshSchedule();
            Close();
        }

        private void CMD_DELETE_Click(object sender, EventArgs e)
        {
            try
            {
                CSchedule sche1 = new CSchedule
                {
                    ID = Therapist,
                    Remark = TXT_Q.Text,
                    KEYUSER = Authorize.getUser(),
                    KEYDATE = DTP_SERVICE_DATE.Value,
                    FLAG = "4"//ตรวจสอบว่ามีการลงเวลาเข้างานยัง
                };
                CSchedule sche2 = new CSchedule
                {
                    ID = Therapist,
                    Remark = TXT_Q.Text,
                    KEYUSER = Authorize.getUser(),
                    KEYDATE = DTP_SERVICE_DATE.Value,
                    FLAG = "2"
                };
                if (DALSchedule.checkCount(sche1) > 0)
                {
                    if (DALEmployee.manageEmployeeSchedule(sche2) > 0)
                    {
                        MessageBox.Show("ลบเวลาเข้างานเรียบร้อยแล้ว");
                    }
                }
                else
                {
                    MessageBox.Show("ยังไม่ได้ลงเวลาเข้างาน");
                }
            }
            catch (SqlException ex)
            {
                Management.ShowMsg(ex.Message);
            }
            main.refreshSchedule();
            Close();
        }
    }
}
