﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Structure;
using Rakuten.Utility;
namespace Rakuten
{
    public partial class FrmCustomerUseDate : Form
    {
        public FrmCustomerUseDate()
        {
            InitializeComponent();
        }

        private void BSearch_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            showData();
            Cursor  = Cursors.Default;
        }

        private void showData()
        {
            string status = (RD_ID.Checked) ? "1" : "2";
            switch (status)
            {
                case "1"://by id
                    List<CCusUseDate> list1 = DALCusUseDate.getCCusUseDate(txtSearch.Text, CBO_TYPE.SelectedValue.ToString(), "1");
                    if (list1.Count > 0)
                    {
                        dataGridView1.DataSource = list1;
                    }
                    else
                    {
                        dataGridView1.DataSource = null;
                        Management.ShowMsg("ไม่พบข้อมูล");
                    }
                    break;
                case "2"://by name
                    List<CCusUseDate> list2 = DALCusUseDate.getCCusUseDate(txtSearch.Text, CBO_TYPE.SelectedValue.ToString(), "2");
                    if (list2.Count > 0)
                    {
                        dataGridView1.DataSource = list2;
                    }
                    else
                    {
                        dataGridView1.DataSource = null;
                        Management.ShowMsg("ไม่พบข้อมูล");
                    }
                    break;
            }
        }
        private void FrmCustomerUseDate_Load(object sender, EventArgs e)
        {
            //Customer Type
            CBO_TYPE.DataSource = DALMaster.getList(MasterList.MCustomerType,
                                    new CMaster
                                    {
                                        ID = "",
                                        DETAIL = "",
                                        KEYDATE = DateTime.Now,
                                        KEYUSER = Authorize.getUser(),
                                        LASTEDIT = DateTime.Now,
                                        FLAG = Management.SearchAll//Searh Flag
                                    }
                                );
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                showData();
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }
    }
}
