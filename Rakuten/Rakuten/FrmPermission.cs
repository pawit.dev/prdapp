﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Structure;
using Rakuten.Utility;
namespace Rakuten
{
    public partial class FrmPermission : Form
    {
        private bool bHaveOldData = false;

        public FrmPermission()
        {
            InitializeComponent();
        }

        private void FrmPermission_Load(object sender, EventArgs e)
        {
            //Data Employee
            CBO_THERAPIST.DataSource = DALEmployee.getList(new CEmployee
                {
                    ID = "",
                    Type = "0003",
                    Sex = "",
                    Title = "",
                    Name = "",
                    Surname = "",
                    NickName = "",
                    Nation = "",
                    BirthDay = DateTime.Now,
                    Address = "",
                    Mobile = "",
                    Email = "",
                    Status = "",
                    Period = "",
                    KEYDATE = DateTime.Now,
                    KEYUSER = Authorize.getUser(),
                    LASTEDIT = DateTime.Now,
                    FLAG = "7"
                }
            );
            BSAVE.Focus();
        }

        private void CBO_THERAPIST_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBO_THERAPIST.SelectedValue != null)
            {
                dataGridView1.DataSource = DALPermission.getList(
                                            new CPermission
                                            {
                                                From = "",
                                                EMPID = CBO_THERAPIST.SelectedValue.ToString(),
                                                Insert = 0,
                                                Delete = 0,
                                                Update = 0,
                                                View = 0,
                                                FLAG = "1"
                                            });
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells[0].Value.ToString().Equals(""))
                    {
                        dataGridView1.Rows[i].Cells[0].Value = CBO_THERAPIST.SelectedValue.ToString();
                        bHaveOldData = true;
                    }
                }
                Console.WriteLine("Result:" + bHaveOldData);
            }
        }

        private void BSAVE_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            Console.WriteLine("::-"+dataGridView1.Rows[0].Cells[4].Value.ToString());  
            switch (bHaveOldData)
            {
                case true:
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        CPermission cp = new CPermission
                        {
                            EMPID = dataGridView1.Rows[i].Cells[0].Value.ToString(),
                            From = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            Insert = Convert.ToByte(dataGridView1.Rows[i].Cells[4].Value.ToString()),
                            Delete = Convert.ToByte(dataGridView1.Rows[i].Cells[5].Value.ToString()),
                            Update = Convert.ToByte(dataGridView1.Rows[i].Cells[6].Value.ToString()),
                            View = Convert.ToByte(dataGridView1.Rows[i].Cells[7].Value.ToString()),
                            FLAG = "2"
                        };
                        Console.WriteLine("Insert Current" + DALPermission.manage(cp));
                    }
                    break;
                case false:
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        CPermission cp = new CPermission
                        {
                            EMPID = dataGridView1.Rows[i].Cells[0].Value.ToString(),
                            From = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            Insert = Convert.ToByte(dataGridView1.Rows[i].Cells[4].Value),
                            Delete = Convert.ToByte(dataGridView1.Rows[i].Cells[5].Value),
                            Update = Convert.ToByte(dataGridView1.Rows[i].Cells[6].Value),
                            View = Convert.ToByte(dataGridView1.Rows[i].Cells[7].Value),
                            FLAG = "3"
                        };
                        Console.WriteLine("Update Current" + DALPermission.manage(cp));
                    }
                    break;
            }
            Cursor = Cursors.Default;
            Management.ShowMsg(Management.INSERT_SUCCESS);
            dataGridView1.DataSource = null;//Clear value.
        }

        private void BCANCEL_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }
    }
}
