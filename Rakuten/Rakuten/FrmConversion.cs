﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.OleDb;
using Rakuten.Structure;
using Rakuten.DAL;

namespace Rakuten
{
    public partial class FrmConversion : Form
    {
        public FrmConversion()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }
        public DataSet RetrieveExcelData(string excelSheetName,string sheetName)
        {
            DataSet ds = new DataSet();
            try
            {
                OleDbConnection conn = new OleDbConnection(@"provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelSheetName + ";Extended Properties=Excel 8.0;");
                OleDbDataAdapter da = new OleDbDataAdapter("select * from [" + sheetName + "$]", conn);
                da.TableMappings.Add("Table", "TestTable");
                da.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
            return ds;
        }

        private void FrmConversion_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void B_BW_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            openFileDialog1.Filter = "Excel2003 file (*.xls)|*.xls";
            TXT_FILENAME.Text = openFileDialog1.FileName;
        }

        private void BProcess_Click(object sender, EventArgs e)
        {
            if (!TXT_FILENAME.Text.Equals(""))
            {
                dataGridView1.DataSource = RetrieveExcelData(TXT_FILENAME.Text, comboBox1.Text).Tables[0];
            }
            else {
                MessageBox.Show("ยังไม่ได้เลือกไฟล์");
            }
        }

        private void B_UPLOAD_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            int result = 0;
            switch (comboBox1.SelectedIndex)
            { 
                case 0://ข้อมูลสมาชิกใหม่
                    //[manageMemberPayment]
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        CMemberPayment customer1 = new CMemberPayment
                        {
                            ID = dataGridView1.Rows[i].Cells[0].Value.ToString(),
                            Type = "0002",
                            Sex = dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            Name = dataGridView1.Rows[i].Cells[2].Value.ToString(),
                            Surname = dataGridView1.Rows[i].Cells[3].Value.ToString(),
                            Age = Convert.ToInt16(dataGridView1.Rows[i].Cells[4].Value.ToString()),
                            Address = dataGridView1.Rows[i].Cells[5].Value.ToString(),
                            Mobile = dataGridView1.Rows[i].Cells[6].Value.ToString(),
                            Email = "-",
                            Nation = dataGridView1.Rows[i].Cells[7].Value.ToString(),
                            StartDate = DateTime.Now,
                            EndDate = DateTime.Now,
                            /////////////////
                            Therapist = dataGridView1.Rows[i].Cells[8].Value.ToString(),
                            Package = dataGridView1.Rows[i].Cells[9].Value.ToString(),
                            ////////////////
                            AccountCode = "0002",
                            Amount = Convert.ToInt16(dataGridView1.Rows[i].Cells[10].Value.ToString()),
                            Discount = Convert.ToInt16(dataGridView1.Rows[i].Cells[11].Value.ToString()),
                            PaymentType = dataGridView1.Rows[i].Cells[12].Value.ToString(),
                            Slip = "-",
                            Bank = dataGridView1.Rows[i].Cells[13].Value.ToString(),
                            RUNNING =Convert.ToInt16(dataGridView1.Rows[i].Cells[13].Value.ToString()),
                            ///////////////
                            KEYDATE = DateTime.Now,
                            KEYUSER = "0000",
                            LASTEDIT = DateTime.Now,
                            FLAG = "4"//Insert
                        };
                        result += DALMember.manageMemberPayment(customer1);
                    }
                    MessageBox.Show("Update Success "+result/3+" Record.");
                    break;
                case 1://ข้อมูลการใช้บริการ
                    //[ConvertTransaction]
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        result += DALConversion.manageMaster(
                            dataGridView1.Rows[i].Cells[0].Value.ToString(),
                            (dataGridView1.Rows[i].Cells[1].Value.ToString().Equals("")) ? "0000" : dataGridView1.Rows[i].Cells[1].Value.ToString(),
                            dataGridView1.Rows[i].Cells[2].Value.ToString(),
                            Convert.ToDateTime(dataGridView1.Rows[i].Cells[3].Value.ToString()),
                            Convert.ToDateTime(dataGridView1.Rows[i].Cells[4].Value.ToString()),
                            dataGridView1.Rows[i].Cells[5].Value.ToString(),
                            dataGridView1.Rows[i].Cells[6].Value.ToString(),
                            Convert.ToDouble(dataGridView1.Rows[i].Cells[7].Value.ToString()),
                            "0001",
                            "0000",
                            0
                            );
                    }
                    MessageBox.Show("Update Success " + result + " Record.");
                    break;
            }
            //Clear Grid
            dataGridView1.DataSource = null;
            Cursor = Cursors.Default;
        }
    }
}
