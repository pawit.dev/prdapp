﻿using System;
using System.Windows.Forms;
using Rakuten.Utility;

namespace Rakuten
{
    public partial class FrmOption : Form
    {
        private int index = 0;//Selected index
        public FrmOption()
        {
            InitializeComponent();
        }

        private void FrmOption_Load(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
            treeView1.SelectedNode = treeView1.Nodes[0].Nodes[0];
            groupBox1.Visible = false;
            groupBox2.Visible = true;
            TXT_TO.Text = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "TO"));
            TXT_FROM.Text = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "FROM"));
            TXT_USER.Text = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "USER"));
            TXT_PASS.Text = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "PASS"));
            TXT_HOST.Text = ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "HOST"));
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {
            switch (index)
            {
                case 0:
                    ManageLOG.writeRegistry("Rakuten", "TO", ManageLOG.enCode(TXT_TO.Text));
                    ManageLOG.writeRegistry("Rakuten", "FROM", ManageLOG.enCode(TXT_FROM.Text));
                    ManageLOG.writeRegistry("Rakuten", "USER", ManageLOG.enCode(TXT_USER.Text));
                    ManageLOG.writeRegistry("Rakuten", "PASS", ManageLOG.enCode(TXT_PASS.Text));
                    ManageLOG.writeRegistry("Rakuten", "HOST", ManageLOG.enCode(TXT_HOST.Text));
                    ManageLOG.writeRegistry("Rakuten", "AM", ManageLOG.enCode(CB_SEND.Checked.ToString()));
                    Management.ShowMsg(Management.INSERT_SUCCESS);
                    break;
                case 1:
                    ManageLOG.writeRegistry("Rakuten", "LATE_FINE", ManageLOG.enCode(TXT_FINE.Text));
                    Management.ShowMsg(Management.INSERT_SUCCESS);
                    break;
                case 2:
                    Management.ShowMsg((DAL.DALConversion.ResetTransData(DP_START.Value,DP_END.Value) > 0) ? "ลบข้อมูลเรียบร้อยแล้ว" : "ลบข้อมูลเรียบร้อยแล้ว");
                    break;
            }
            
            Close();
        }

        private void CheckIsNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
                Management.ShowMsg(Management.NUMBER_ONLY);
            }
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch (e.Node.Index)
            { 
                case 0:
                    groupBox1.Visible = false;
                    groupBox2.Visible = true;
                    groupBox3.Visible = false;
                    index = 0;
                    B_SAVE.Text = "บันทึก";
                    B_SAVE.Image = Properties.Resources.disk_blue_ok;
                    break;
                case 1:
                    groupBox1.Visible = true;
                    groupBox2.Visible = false;
                    groupBox3.Visible = false;
                    index = 1;
                    B_SAVE.Text = "บันทึก";
                    B_SAVE.Image = Properties.Resources.disk_blue_ok;
                    break;
                case 2:
                    groupBox1.Visible = false;
                    groupBox2.Visible = false;
                    groupBox3.Visible = true;
                    index = 2;
                    B_SAVE.Text = "ลบข้อมูล";
                    B_SAVE.Image = Properties.Resources.delete2;
                    break;
                default:
                    groupBox1.Visible = false;
                    groupBox2.Visible = true;
                    groupBox3.Visible = false;
                    index = 0;
                    B_SAVE.Text = "บันทึก";
                    B_SAVE.Image = Properties.Resources.disk_blue_ok;
                    break;
            }
        }
    }
}
