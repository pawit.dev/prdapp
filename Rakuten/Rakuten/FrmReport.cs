﻿using System;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Report;
namespace Rakuten
{
    public partial class FrmReport : Form
    {
        public FrmReport()
        {
            InitializeComponent();
        }

        private void FrmReport_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            B_SHOW.Focus();
        }

        private void B_SHOW_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ShowReport();//Show Report
            Cursor = Cursors.Default;
        }

        public void ShowReport()
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0://##############-สรุปยอดขาย-##############
                    break;
                case 1:
                    //สรุปประเภทการชำระเงิน
                    Report09 rpt09 = new Report09();
                    rpt09.SetDataSource(DALReport.getReport9(DP_START.Value, DP_END.Value));
                    rpt09.SetParameterValue("start", DP_START.Value);
                    rpt09.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt09;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 2:
                    //สรุปจำนวนลูกค้า
                    Report12 rpt12 = new Report12();
                    rpt12.SetDataSource(DALReport.getReport12(DP_START.Value, DP_END.Value));
                    rpt12.SetParameterValue("start", DP_START.Value);
                    rpt12.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt12;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 3:
                    //สรุปยอดขายสมาชิก
                    Report10 rpt10 = new Report10();
                    rpt10.SetDataSource(DALReport.getReport10(DP_START.Value, DP_END.Value));
                    rpt10.SetParameterValue("start", DP_START.Value);
                    rpt10.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt10;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 4:
                    //สรุปยอดขายรายเดือน
                    Report20 rpt20 = new Report20();
                    rpt20.SetDataSource(DALReport.getReport20(DP_START.Value, DP_END.Value));
                    rpt20.SetParameterValue("start", DP_START.Value);
                    rpt20.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt20;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 5:
                    //สรุปจำนวนสมาชิก
                    Report21 rpt21 = new Report21();
                    rpt21.SetDataSource(DALReport.getReport21(DP_START.Value, DP_END.Value));
                    rpt21.SetParameterValue("start", DP_START.Value);
                    rpt21.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt21;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 6:
                    // สรุปจำนวนสมาชิกใหม่
                    Report22 rpt22 = new Report22();
                    rpt22.SetDataSource(DALReport.getReport22(DP_START.Value, DP_END.Value));
                    rpt22.SetParameterValue("start", DP_START.Value);
                    rpt22.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt22;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 7:
                    //สรุป STOCK
                    Report03 rpt03 = new Report03();
                    rpt03.SetDataSource(DALReport.getReport3(DP_START.Value, DP_END.Value));
                    rpt03.SetParameterValue("start", DP_START.Value);
                    rpt03.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt03;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 8:
                    //สรุปค่ามือพนักงาน
                    Report02 rpt02 = new Report02();
                    rpt02.SetDataSource(DALReport.getReport2(DP_START.Value, DP_END.Value));
                    rpt02.SetParameterValue("start", DP_START.Value);
                    rpt02.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt02;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 9:
                    //สรุปการจองพนักงานนวด
                    Report18 rpt19 = new Report18();
                    rpt19.SetDataSource(DALReport.getReport19(DP_START.Value, DP_END.Value));
                    rpt19.SetParameterValue("start", DP_START.Value);
                    rpt19.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt19;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 10:
                    //สรุปลูกค้า
                    Report01 rpt01 = new Report01();
                    rpt01.SetDataSource(DALReport.getReport1(DP_START.Value, DP_END.Value));
                    rpt01.SetParameterValue("start", DP_START.Value);
                    rpt01.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt01;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 11://##############-สถิติ-##############
                    break;
                case 12:
                    //รายงานจำนวนเชื้อชาตินี้เลือก Package การนวดแบบไหน แยกตามเพศ
                    Report06 rpt06 = new Report06();
                    rpt06.SetDataSource(DALReport.getReport6(DP_START.Value, DP_END.Value));
                    crystalReportViewer1.ReportSource = rpt06;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 13:
                    //รายงานช่วงอายุของสมาชิก
                    Report07 rpt07 = new Report07();
                    rpt07.SetDataSource(DALReport.getReport7(DP_START.Value, DP_END.Value));
                    crystalReportViewer1.ReportSource = rpt07;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 14://##############-พนักงาน-##############d
                    break;
                case 15:
                    //สรุป วันลาพนักงาน
                    Report14 rpt14 = new Report14();
                    rpt14.SetDataSource(DALReport.getReport14(DP_START.Value, DP_END.Value));
                    rpt14.SetParameterValue("start", DP_START.Value);
                    rpt14.SetParameterValue("end", DP_END.Value);
                    crystalReportViewer1.ReportSource = rpt14;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 16:
                    //Therapist Income
                    Report15 rpt15 = new Report15();
                    rpt15.SetDataSource(DALReport.getReport15(DP_START.Value, DP_END.Value));
                    crystalReportViewer1.ReportSource = rpt15;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 17:
                    //Therapist JOB
                    Report16 rpt16 = new Report16();
                    rpt16.SetDataSource(DALReport.getReport16(DP_START.Value, DP_END.Value));
                    crystalReportViewer1.ReportSource = rpt16;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 18://##############- สมาชิก-##############
                    break;
                case 19:
                    //สมาชิกมาใช้บริการ (Member)
                    Report17 rpt17 = new Report17();
                    rpt17.SetDataSource(DALReport.getReport17());
                    crystalReportViewer1.ReportSource = rpt17;
                    crystalReportViewer1.Zoom(75);
                    break;
                case 20:
                    //สมาชิกมาใช้บริการ (Specail Package)
                    Report17 rpt18 = new Report17();
                    rpt18.SetDataSource(DALReport.getReport18());
                    crystalReportViewer1.ReportSource = rpt18;
                    crystalReportViewer1.Zoom(75);
                    break;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            LR_DETAIL.Text = comboBox1.Text;
        }
    }
}




















