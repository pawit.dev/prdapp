CREATE DATABASE [DBRakuten] ON  PRIMARY 
( NAME = N'DBRakuten', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\DBRakuten.mdf' , SIZE = 3072KB , FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DBRakuten_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\DATA\DBRakuten_log.ldf' , SIZE = 1024KB , FILEGROWTH = 10%)
GO
EXEC dbo.sp_dbcmptlevel @dbname=N'DBRakuten', @new_cmptlevel=90
GO
ALTER DATABASE [DBRakuten] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBRakuten] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBRakuten] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBRakuten] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBRakuten] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBRakuten] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBRakuten] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DBRakuten] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBRakuten] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBRakuten] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBRakuten] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBRakuten] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBRakuten] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBRakuten] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBRakuten] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBRakuten] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBRakuten] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBRakuten] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBRakuten] SET  READ_WRITE 
GO
ALTER DATABASE [DBRakuten] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DBRakuten] SET  MULTI_USER 
GO
ALTER DATABASE [DBRakuten] SET PAGE_VERIFY CHECKSUM  
GO
USE [DBRakuten]
/****** Object:  StoredProcedure [dbo].[managePackage]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[managePackage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[managePackage]
GO
/****** Object:  StoredProcedure [dbo].[PrintInvoiceData]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrintInvoiceData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PrintInvoiceData]
GO
/****** Object:  StoredProcedure [dbo].[GetMemberReport]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMemberReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMemberReport]
GO
/****** Object:  StoredProcedure [dbo].[manageEmployeeSummary]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployeeSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageEmployeeSummary]
GO
/****** Object:  StoredProcedure [dbo].[GetReport]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetReport]
GO
/****** Object:  StoredProcedure [dbo].[manageMLeave]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMLeave]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMLeave]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMLeave]    Script Date: 06/02/2009 13:02:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMLeave]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMLeave]
GO
/****** Object:  StoredProcedure [dbo].[manageMemberPayment]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMemberPayment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMemberPayment]
GO
/****** Object:  StoredProcedure [dbo].[manageCustomer]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageCustomer]
GO
/****** Object:  StoredProcedure [dbo].[manageCustomerTransaction]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageCustomerTransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageCustomerTransaction]
GO
/****** Object:  StoredProcedure [dbo].[manageEmployee]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageEmployee]
GO
/****** Object:  StoredProcedure [dbo].[manageMUser]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMUser]
GO
/****** Object:  StoredProcedure [dbo].[manageMTypeEmployee]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMTypeEmployee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMTypeEmployee]
GO
/****** Object:  StoredProcedure [dbo].[manageMtitle]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMtitle]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMtitle]
GO
/****** Object:  StoredProcedure [dbo].[GetPermission]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPermission]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPermission]
GO
/****** Object:  StoredProcedure [dbo].[manageRoom]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageRoom]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageRoom]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMPaymentType]    Script Date: 06/02/2009 13:02:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMPaymentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMPaymentType]
GO
/****** Object:  StoredProcedure [dbo].[manageMPaymentType]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMPaymentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMPaymentType]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMNation]    Script Date: 06/02/2009 13:02:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMNation]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMNation]
GO
/****** Object:  StoredProcedure [dbo].[manageMNation]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMNation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMNation]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMCustomerType]    Script Date: 06/02/2009 13:02:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMCustomerType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMCustomerType]
GO
/****** Object:  StoredProcedure [dbo].[manageMCustomerType]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMCustomerType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMCustomerType]
GO
/****** Object:  StoredProcedure [dbo].[manageMBank]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMBank]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMBank]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMassageType]    Script Date: 06/02/2009 13:02:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMassageType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMassageType]
GO
/****** Object:  StoredProcedure [dbo].[manageMMassageType]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMMassageType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMMassageType]
GO
/****** Object:  StoredProcedure [dbo].[PrintMemberInvoice]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrintMemberInvoice]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PrintMemberInvoice]
GO
/****** Object:  UserDefinedFunction [dbo].[GetTherapistCost]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTherapistCost]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetTherapistCost]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageCost]    Script Date: 06/02/2009 13:02:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageCost]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetPackageCost]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageName]    Script Date: 06/02/2009 13:02:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetPackageName]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageHour]    Script Date: 06/02/2009 13:02:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageHour]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetPackageHour]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageType]    Script Date: 06/02/2009 13:02:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetPackageType]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageTotal]    Script Date: 06/02/2009 13:02:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageTotal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetPackageTotal]
GO
/****** Object:  StoredProcedure [dbo].[DeleteTransactions]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteTransactions]
GO
/****** Object:  StoredProcedure [dbo].[GetRunning]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRunning]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetRunning]
GO
/****** Object:  StoredProcedure [dbo].[getCustomerUseDate]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getCustomerUseDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[getCustomerUseDate]
GO
/****** Object:  StoredProcedure [dbo].[manageMAccountCode]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMAccountCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMAccountCode]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMAccountCode]    Script Date: 06/02/2009 13:02:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMAccountCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMAccountCode]
GO
/****** Object:  StoredProcedure [dbo].[GetRoomSchedule]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRoomSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetRoomSchedule]
GO
/****** Object:  StoredProcedure [dbo].[manageEmployeeSchedule]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployeeSchedule]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageEmployeeSchedule]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMhour]    Script Date: 06/02/2009 13:02:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMhour]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMhour]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMSex]    Script Date: 06/02/2009 13:02:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMSex]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMSex]
GO
/****** Object:  StoredProcedure [dbo].[manageTimePeriod]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageTimePeriod]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageTimePeriod]
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeLeaveStatus]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeLeaveStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetEmployeeLeaveStatus]
GO
/****** Object:  StoredProcedure [dbo].[manageEmployeeLeave]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployeeLeave]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageEmployeeLeave]
GO
/****** Object:  StoredProcedure [dbo].[manageMScentTransaction]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMScentTransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMScentTransaction]
GO
/****** Object:  StoredProcedure [dbo].[manageCustomerPayment]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageCustomerPayment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageCustomerPayment]
GO
/****** Object:  Table [dbo].[MLeave]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MLeave]') AND type in (N'U'))
DROP TABLE [dbo].[MLeave]
GO
/****** Object:  UserDefinedFunction [dbo].[CheckISPay]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckISPay]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CheckISPay]
GO
/****** Object:  Table [dbo].[EmployeeSummary]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeSummary]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeSummary]
GO
/****** Object:  Table [dbo].[MUSER]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MUSER]') AND type in (N'U'))
DROP TABLE [dbo].[MUSER]
GO
/****** Object:  Table [dbo].[CustomerTransaction]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerTransaction]
GO
/****** Object:  Table [dbo].[MemberPayment]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MemberPayment]') AND type in (N'U'))
DROP TABLE [dbo].[MemberPayment]
GO
/****** Object:  Table [dbo].[CustomerMemberPackage]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerMemberPackage]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerMemberPackage]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
DROP TABLE [dbo].[Customer]
GO
/****** Object:  StoredProcedure [dbo].[GetCustomerLastID]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCustomerLastID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCustomerLastID]
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageID]    Script Date: 06/02/2009 13:02:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetPackageID]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAgePeriod]    Script Date: 06/02/2009 13:02:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAgePeriod]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetAgePeriod]
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeName]    Script Date: 06/02/2009 13:02:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetEmployeeName]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type in (N'U'))
DROP TABLE [dbo].[Employee]
GO
/****** Object:  Table [dbo].[EmpPermission]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpPermission]') AND type in (N'U'))
DROP TABLE [dbo].[EmpPermission]
GO
/****** Object:  Table [dbo].[MTypeEmployee]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MTypeEmployee]') AND type in (N'U'))
DROP TABLE [dbo].[MTypeEmployee]
GO
/****** Object:  Table [dbo].[MTitle]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MTitle]') AND type in (N'U'))
DROP TABLE [dbo].[MTitle]
GO
/****** Object:  Table [dbo].[FrmMaster]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FrmMaster]') AND type in (N'U'))
DROP TABLE [dbo].[FrmMaster]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Room]') AND type in (N'U'))
DROP TABLE [dbo].[Room]
GO
/****** Object:  Table [dbo].[MPaymentType]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MPaymentType]') AND type in (N'U'))
DROP TABLE [dbo].[MPaymentType]
GO
/****** Object:  Table [dbo].[MNation]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MNation]') AND type in (N'U'))
DROP TABLE [dbo].[MNation]
GO
/****** Object:  Table [dbo].[MCustomerType]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MCustomerType]') AND type in (N'U'))
DROP TABLE [dbo].[MCustomerType]
GO
/****** Object:  Table [dbo].[MBank]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MBank]') AND type in (N'U'))
DROP TABLE [dbo].[MBank]
GO
/****** Object:  Table [dbo].[MMassageType]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MMassageType]') AND type in (N'U'))
DROP TABLE [dbo].[MMassageType]
GO
/****** Object:  Table [dbo].[Package]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Package]') AND type in (N'U'))
DROP TABLE [dbo].[Package]
GO
/****** Object:  Table [dbo].[CustomerPayment]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPayment]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerPayment]
GO
/****** Object:  Table [dbo].[EmployeeSchedule]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeSchedule]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeSchedule]
GO
/****** Object:  Table [dbo].[BOOKINVOICE]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BOOKINVOICE]') AND type in (N'U'))
DROP TABLE [dbo].[BOOKINVOICE]
GO
/****** Object:  Table [dbo].[MAccountCode]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MAccountCode]') AND type in (N'U'))
DROP TABLE [dbo].[MAccountCode]
GO
/****** Object:  StoredProcedure [dbo].[GetLastTranSaction]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLastTranSaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetLastTranSaction]
GO
/****** Object:  Table [dbo].[MHour]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MHour]') AND type in (N'U'))
DROP TABLE [dbo].[MHour]
GO
/****** Object:  Table [dbo].[MPaymentStatus]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MPaymentStatus]') AND type in (N'U'))
DROP TABLE [dbo].[MPaymentStatus]
GO
/****** Object:  Table [dbo].[MScent]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MScent]') AND type in (N'U'))
DROP TABLE [dbo].[MScent]
GO
/****** Object:  Table [dbo].[MSex]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MSex]') AND type in (N'U'))
DROP TABLE [dbo].[MSex]
GO
/****** Object:  StoredProcedure [dbo].[manageMHour]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMHour]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMHour]
GO
/****** Object:  Table [dbo].[MTimePeriod]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MTimePeriod]') AND type in (N'U'))
DROP TABLE [dbo].[MTimePeriod]
GO
/****** Object:  StoredProcedure [dbo].[manageMSex]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMSex]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMSex]
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeData]    Script Date: 06/02/2009 13:02:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeData]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetEmployeeData]
GO
/****** Object:  StoredProcedure [dbo].[manageMPaymentStatus]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMPaymentStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMPaymentStatus]
GO
/****** Object:  Table [dbo].[EmployeeLeave]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeLeave]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeLeave]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMScent]    Script Date: 06/02/2009 13:02:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMScent]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMScent]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMPeriod]    Script Date: 06/02/2009 13:02:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMPeriod]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetMPeriod]
GO
/****** Object:  Table [dbo].[MScentTransaction]    Script Date: 06/02/2009 13:02:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MScentTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[MScentTransaction]
GO
/****** Object:  StoredProcedure [dbo].[manageMScent]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMScent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[manageMScent]
GO
/****** Object:  StoredProcedure [dbo].[updateEmployeeTimePeriod]    Script Date: 06/02/2009 13:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[updateEmployeeTimePeriod]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[updateEmployeeTimePeriod]
GO
/****** Object:  StoredProcedure [dbo].[updateEmployeeTimePeriod]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[updateEmployeeTimePeriod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	¡ÓË¹´ÃÍº§Ò¹ãËé¾¹Ñ¡§Ò¹
-- =============================================
CREATE PROCEDURE [dbo].[updateEmployeeTimePeriod]
(
	@inMPeriod varchar(4),
	@inEno varchar(4)
)
AS
BEGIN
	update Employee set MPeriod = @inMPeriod
	where Eno = @inEno
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMScent]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMScent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageMScent]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMin int,
	@inMax int,
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert

		insert into MScent(DNo,DDetail,Dmin,Dmax,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,@inMin,@inMax,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MScent set 
						DDetail = @inDetail,
						Dmin	= @inMin,
						Dmax = @inMax,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MScent where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MScent order by dno
	else if @inFlag = ''4''--Select
		select * from MScent where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MScent where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 





' 
END
GO
/****** Object:  Table [dbo].[MScentTransaction]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MScentTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MScentTransaction](
	[SID] [int] IDENTITY(1,1) NOT NULL,
	[SCtype] [varchar](4) COLLATE Thai_CI_AS NULL,
	[SCount] [int] NULL,
	[SType] [varchar](1) COLLATE Thai_CI_AS NULL,
	[KeyDate] [datetime] NULL,
	[LastEdit] [datetime] NULL,
	[KeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MScentTransaction] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'MScentTransaction', N'COLUMN',N'SCtype'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'»ÃÐàÀ· ¡ÅÔè¹' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MScentTransaction', @level2type=N'COLUMN',@level2name=N'SCtype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'MScentTransaction', N'COLUMN',N'SType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 ÃÑºÁÒ  2 ¨èÒÂ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MScentTransaction', @level2type=N'COLUMN',@level2name=N'SType'
GO
SET IDENTITY_INSERT [dbo].[MScentTransaction] ON
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (112, N'0001', 1, N'2', CAST(0x00009C1A0186625D AS DateTime), CAST(0x00009C1A0186625D AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (113, N'0001', 1, N'2', CAST(0x00009C1A00FE9433 AS DateTime), CAST(0x00009C1A00FE9433 AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (114, N'0001', 1, N'2', CAST(0x00009C1C008BF403 AS DateTime), CAST(0x00009C1C008BF403 AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (115, N'0001', 1, N'2', CAST(0x00009C2A008C53AA AS DateTime), CAST(0x00009C2A008C53AA AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (116, N'0001', 1, N'2', CAST(0x00009C48008C9935 AS DateTime), CAST(0x00009C48008C9935 AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (117, N'0001', 1, N'2', CAST(0x00009C49008CF8A9 AS DateTime), CAST(0x00009C49008CF8A9 AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (118, N'0001', 1, N'2', CAST(0x00009C4A0095EB03 AS DateTime), CAST(0x00009C4A0095EB03 AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (119, N'0001', 1, N'2', CAST(0x00009C1B00A09B53 AS DateTime), CAST(0x00009C1B00A09B53 AS DateTime), N'0001')
INSERT [dbo].[MScentTransaction] ([SID], [SCtype], [SCount], [SType], [KeyDate], [LastEdit], [KeyUser]) VALUES (120, N'0001', 1, N'2', CAST(0x00009C1C00CC13D7 AS DateTime), CAST(0x00009C1C00CC13D7 AS DateTime), N'0001')
SET IDENTITY_INSERT [dbo].[MScentTransaction] OFF
/****** Object:  UserDefinedFunction [dbo].[GetMPeriod]    Script Date: 06/02/2009 13:02:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMPeriod]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMPeriod]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select Ddetail from MTimePeriod where Dno = @inID)
	return @data
END




' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMScent]    Script Date: 06/02/2009 13:02:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMScent]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetMScent]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select Ddetail from MScent where Dno = @inID)
	return @data
END



' 
END
GO
/****** Object:  Table [dbo].[EmployeeLeave]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeLeave]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeLeave](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmpID] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LStartDate] [datetime] NULL,
	[LEndDate] [datetime] NULL,
	[LCount] [float] NULL,
	[LReason] [varchar](100) COLLATE Thai_CI_AS NULL,
	[Keydate] [datetime] NULL,
	[KeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LastEdit] [datetime] NULL,
 CONSTRAINT [PK_EmployeeLeave] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[EmployeeLeave] ON
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (7, N'2', N'0001', CAST(0x00009BFB01811720 AS DateTime), CAST(0x00009BFB01811712 AS DateTime), 1, N'µÔ´¸ØÃÐ', CAST(0x00009BFB01813247 AS DateTime), N'0001', CAST(0x00009BFB01813247 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (8, N'2', N'0002', CAST(0x00009BFB0181513E AS DateTime), CAST(0x00009BFB0181513E AS DateTime), 3, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB018162A7 AS DateTime), N'0001', CAST(0x00009BFB018162A7 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (9, N'5', N'0001', CAST(0x00009BFB018179C3 AS DateTime), CAST(0x00009BFB018179BF AS DateTime), 4, N'µÔ´¸ØÃÐ', CAST(0x00009BFB01818754 AS DateTime), N'0001', CAST(0x00009BFB01818754 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (10, N'7', N'0002', CAST(0x00009BFB01819227 AS DateTime), CAST(0x00009BFB01819227 AS DateTime), 7, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB0181AC7B AS DateTime), N'0001', CAST(0x00009BFB0181AC7B AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (11, N'8', N'0002', CAST(0x00009BFB0181B980 AS DateTime), CAST(0x00009BFB0181B980 AS DateTime), 1, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB0181C9D1 AS DateTime), N'0001', CAST(0x00009BFB0181C9D1 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (12, N'8', N'0003', CAST(0x00009BFB0181CC87 AS DateTime), CAST(0x00009BFB0181CC87 AS DateTime), 1, N'»èÇÂ', CAST(0x00009BFB0181D26C AS DateTime), N'0001', CAST(0x00009BFB0181D26C AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (13, N'9', N'0001', CAST(0x00009BFB0181D9C9 AS DateTime), CAST(0x00009BFB0181D9C9 AS DateTime), 2.5, N'µÔ´¸ØÃÐ', CAST(0x00009BFB0181E987 AS DateTime), N'0001', CAST(0x00009BFB0181E987 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (14, N'9', N'0002', CAST(0x00009BFB0181ED57 AS DateTime), CAST(0x00009BFB0181ED52 AS DateTime), 1, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB018203C0 AS DateTime), N'0001', CAST(0x00009BFB018203C0 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (15, N'10', N'0001', CAST(0x00009BFB018208F4 AS DateTime), CAST(0x00009BFB018208EF AS DateTime), 1, N'µÔ´¸ØÃÐ', CAST(0x00009BFB01821342 AS DateTime), N'0001', CAST(0x00009BFB01821342 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (16, N'10', N'0002', CAST(0x00009BFB01821693 AS DateTime), CAST(0x00009BFB01821693 AS DateTime), 4, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB01822389 AS DateTime), N'0001', CAST(0x00009BFB01822389 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (17, N'11', N'0001', CAST(0x00009BFB01822BB4 AS DateTime), CAST(0x00009BFB01822BB4 AS DateTime), 6, N'µÔ´¸ØÃÐ', CAST(0x00009BFB018235BC AS DateTime), N'0001', CAST(0x00009BFB018235BC AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (18, N'14', N'0002', CAST(0x00009BFB01824D82 AS DateTime), CAST(0x00009BFB01824D7D AS DateTime), 1, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB01825BEF AS DateTime), N'0001', CAST(0x00009BFB01825BEF AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (19, N'14', N'0003', CAST(0x00009BFB01825E76 AS DateTime), CAST(0x00009BFB01825E76 AS DateTime), 3, N'»èÇÂ', CAST(0x00009BFB01826AF8 AS DateTime), N'0001', CAST(0x00009BFB01826AF8 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (20, N'19', N'0001', CAST(0x00009BFB01827A1C AS DateTime), CAST(0x00009BFB01827A1C AS DateTime), 4, N'µÔ´¸ØÃÐ', CAST(0x00009BFB018283B4 AS DateTime), N'0001', CAST(0x00009BFB018283B4 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (21, N'19', N'0003', CAST(0x00009BFB01828699 AS DateTime), CAST(0x00009BFB01828694 AS DateTime), 1, N'»èÇÂ', CAST(0x00009BFB0182906E AS DateTime), N'0001', CAST(0x00009BFB0182906E AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (22, N'20', N'0001', CAST(0x00009BFB01829A8F AS DateTime), CAST(0x00009BFB01829A8F AS DateTime), 1.5, N'µÔ´¸ØÃÐ', CAST(0x00009BFB0182A5A9 AS DateTime), N'0001', CAST(0x00009BFB0182A5A9 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (23, N'21', N'0001', CAST(0x00009BFB0182CD2B AS DateTime), CAST(0x00009BFB0182CD2B AS DateTime), 3, N'µÔ´¸ØÃÐ', CAST(0x00009BFB0182D23D AS DateTime), N'0001', CAST(0x00009BFB0182D23D AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (24, N'21', N'0002', CAST(0x00009BFB0182D4EA AS DateTime), CAST(0x00009BFB0182D4EA AS DateTime), 3, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB0182DBAF AS DateTime), N'0001', CAST(0x00009BFB0182DBAF AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (25, N'22', N'0001', CAST(0x00009BFB0182DFEB AS DateTime), CAST(0x00009BFB0182DFEB AS DateTime), 0.5, N'µÔ´¸ØÃÐ', CAST(0x00009BFB0182E81D AS DateTime), N'0001', CAST(0x00009BFB0182E81D AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (26, N'25', N'0001', CAST(0x00009BFB0182EFD0 AS DateTime), CAST(0x00009BFB0182EFD0 AS DateTime), 1.5, N'µÔ´¸ØÃÐ', CAST(0x00009BFB0182F523 AS DateTime), N'0001', CAST(0x00009BFB0182F523 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (27, N'26', N'0002', CAST(0x00009BFB0183008C AS DateTime), CAST(0x00009BFB01830087 AS DateTime), 4, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB01830C30 AS DateTime), N'0001', CAST(0x00009BFB01830C30 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (28, N'26', N'0003', CAST(0x00009BFB01830F3B AS DateTime), CAST(0x00009BFB01830F37 AS DateTime), 3, N'»èÇÂ', CAST(0x00009BFB0183180E AS DateTime), N'0001', CAST(0x00009BFB0183180E AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (29, N'28', N'0002', CAST(0x00009BFB01831F62 AS DateTime), CAST(0x00009BFB01831F5D AS DateTime), 3, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB018328FE AS DateTime), N'0001', CAST(0x00009BFB018328FE AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (30, N'31', N'0001', CAST(0x00009BFB018331EE AS DateTime), CAST(0x00009BFB018331EE AS DateTime), 1, N'µÔ´¸ØÃÐ', CAST(0x00009BFB01834B37 AS DateTime), N'0001', CAST(0x00009BFB01834B37 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (31, N'31', N'0002', CAST(0x00009BFB01834E2F AS DateTime), CAST(0x00009BFB01834E2F AS DateTime), 4, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB018353C4 AS DateTime), N'0001', CAST(0x00009BFB018353C4 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (32, N'35', N'0001', CAST(0x00009BFB01836703 AS DateTime), CAST(0x00009BFB018366FE AS DateTime), 2, N'µÔ´¸ØÃÐ', CAST(0x00009BFB01836BB7 AS DateTime), N'0001', CAST(0x00009BFB01836BB7 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (33, N'35', N'0002', CAST(0x00009BFB01836E19 AS DateTime), CAST(0x00009BFB01836E19 AS DateTime), 6, N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BFB01837891 AS DateTime), N'0001', CAST(0x00009BFB01837891 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (34, N'42', N'0001', CAST(0x00009BFB0183800B AS DateTime), CAST(0x00009BFB01838006 AS DateTime), 2.5, N'µÔ´¸ØÃÐ', CAST(0x00009BFB018387A8 AS DateTime), N'0001', CAST(0x00009BFB018387A8 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (35, N'42', N'0003', CAST(0x00009BFB01838DD0 AS DateTime), CAST(0x00009BFB01838DD0 AS DateTime), 1, N'»èÇÂ', CAST(0x00009BFB018394D3 AS DateTime), N'0001', CAST(0x00009BFB018394D3 AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (36, N'5', N'0001', CAST(0x00009C16013D5CEC AS DateTime), CAST(0x00009C15013D5D29 AS DateTime), 1, N'-', CAST(0x00009C15013D6A8B AS DateTime), N'0001', CAST(0x00009C15013D6A8B AS DateTime))
INSERT [dbo].[EmployeeLeave] ([ID], [EmpID], [LType], [LStartDate], [LEndDate], [LCount], [LReason], [Keydate], [KeyUser], [LastEdit]) VALUES (37, N'5', N'0001', CAST(0x00009C17013D98B0 AS DateTime), CAST(0x00009C18013D98B0 AS DateTime), 2, N'-', CAST(0x00009C15013DB0AC AS DateTime), N'0001', CAST(0x00009C15013DB0AC AS DateTime))
SET IDENTITY_INSERT [dbo].[EmployeeLeave] OFF
/****** Object:  StoredProcedure [dbo].[manageMPaymentStatus]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMPaymentStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageMPaymentStatus](
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert

		insert into MPaymentStatus(DNo,DDetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MPaymentStatus set 
						DDetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MPaymentStatus where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MPaymentStatus order by dno
	else if @inFlag = ''4''--Select
		select * from MPaymentStatus where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MPaymentStatus where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeData]    Script Date: 06/02/2009 13:02:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeData]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetEmployeeData]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select Enickname from Employee where Eno = @inID)
	return @data
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMSex]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMSex]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageMSex]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN TRY
	if @inFlag = ''0''--Insert

		insert into MSex(DNo,DDetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MSex set 
						DDetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MSex where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MSex order by dno
	else if @inFlag = ''4''--Select
		select * from MSex where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MSex where Ddetail like ''%''+@inDetail+''%'' order by dno
     END TRY
     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
set @msg  = ''N,''+ERROR_MESSAGE()
		print @msg
     END CATCH
return 
' 
END
GO
/****** Object:  Table [dbo].[MTimePeriod]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MTimePeriod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MTimePeriod](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[DDetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MPStart] [datetime] NULL,
	[MPEnd] [datetime] NULL,
	[MKeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MKeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MTimePeriod] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MTimePeriod] ([DNo], [DDetail], [MPStart], [MPEnd], [MKeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0001', N'ÃÍº·Õè 1', CAST(0x00009BE800BD83A0 AS DateTime), CAST(0x00009BE801624F20 AS DateTime), CAST(0x00009BE800B03C6A AS DateTime), CAST(0x00009BEB000BE983 AS DateTime), N'0001')
INSERT [dbo].[MTimePeriod] ([DNo], [DDetail], [MPStart], [MPEnd], [MKeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0002', N'ÃÍº·Õè 2', CAST(0x00009BE800C5C100 AS DateTime), CAST(0x00009BE800000000 AS DateTime), CAST(0x00009BE900EF1C31 AS DateTime), CAST(0x00009BE900EF1C31 AS DateTime), N'0001')
/****** Object:  StoredProcedure [dbo].[manageMHour]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMHour]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageMHour]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert
		insert into MHour(DNo,DDetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MHour set 
						DDetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MHour where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MHour order by dno
	else if @inFlag = ''4''--Select
		select * from MHour where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MHour where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 


' 
END
GO
/****** Object:  Table [dbo].[MSex]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MSex]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MSex](
	[Dno] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Ddetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MKeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MKeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MSex] PRIMARY KEY CLUSTERED 
(
	[Dno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MSex] ([Dno], [Ddetail], [MKeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0001', N'ªÒÂ', CAST(0x00009BE20095AD41 AS DateTime), CAST(0x00009BE200962FD2 AS DateTime), N'0001')
INSERT [dbo].[MSex] ([Dno], [Ddetail], [MKeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0002', N'Ë­Ô§', CAST(0x00009BE20095B3F9 AS DateTime), CAST(0x00009BE200963823 AS DateTime), N'0001')
/****** Object:  Table [dbo].[MScent]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MScent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MScent](
	[Dno] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Ddetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[DMin] [int] NULL,
	[DMax] [int] NULL,
	[DUnit] [varchar](4) COLLATE Thai_CI_AS NULL,
	[DType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MKeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MScent] PRIMARY KEY CLUSTERED 
(
	[Dno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'MScent', N'COLUMN',N'DUnit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ë¹èÇÂ¹Ñº' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MScent', @level2type=N'COLUMN',@level2name=N'DUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'MScent', N'COLUMN',N'DType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'»ÃÐàÀ·ÇÑÊ´Ø' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MScent', @level2type=N'COLUMN',@level2name=N'DType'
GO
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0001', N'Jasmine', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0002', N'Apple', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0003', N'Base', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0004', N'Lavender', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0005', N'Lemon', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0006', N'Lemon Grass', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0007', N'Mint', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0008', N'Orange', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0009', N'Rosemary', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0010', N'Eucalyptus', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0011', N'Rose', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0012', N'Balancing', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0013', N'Relaxng', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0014', N'Energizing', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0015', N'Grape Fruit', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0016', N'Caraboon', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0017', N'Olive Ginger', 10, 999, NULL, N'0001', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0018', N'Lavender', 10, 999, NULL, N'0002', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0019', N'Pineapple', 10, 999, NULL, N'0002', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0020', N'Jasmine', 10, 999, NULL, N'0002', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0021', N'Eucalyptus', 10, 999, NULL, N'0002', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0022', N'Lemon', 10, 999, NULL, N'0002', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0023', N'K.lime', 10, 999, NULL, N'0002', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0024', N'Crearm', 10, 999, NULL, N'0003', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0025', N'Oil', 10, 999, NULL, N'0003', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0026', N'Scrub', 10, 999, NULL, N'0003', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0027', N'Shampoo', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0028', N'Body Wash 0.5litre', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0029', N'ÅÙ¡»ÃÐ¤º', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0030', N'·ÔªªÙ', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0031', N'à·ÕÂ¹', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0032', N'¡ÅÔè¹¨Ø´ Bumer', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0033', N'¼Å«Ñ¡½Í§(ª¹Ô´¼§ 9kg)', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0034', N'¹éÓÂÒ»ÃÑº¼éÒ¹ØèÁ', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0035', N'à´·µÍÅ', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0036', N'¹éÓÂÒÅéÒ§¨Ò¹', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0037', N'¹éÓÂÒÅéÒ§ËéÍ§¹éÓ 1 littre', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0038', N'¹éÓÂÒ¶Ù¾×é¹', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0039', N'ªÒ', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0040', N'¹éÓÂÒ¢¨Ñ´¤ÃÒºÁÑ¹', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0041', N'ÂÒ¡Ñ¹ÂØ§', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0042', N'¶Ø§´Ó¢¹Ò´ 28x36', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0043', N'¹éÓÂÒÅéÒ§Á×Í/à·éÒ', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
INSERT [dbo].[MScent] ([Dno], [Ddetail], [DMin], [DMax], [DUnit], [DType], [MkeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0044', N'¹éÓÂÒàªç´Ë¹Ñ§', 10, 999, NULL, N'0004', CAST(0x00009BFB018887E2 AS DateTime), CAST(0x00009BFB018887E2 AS DateTime), N'0001')
/****** Object:  Table [dbo].[MPaymentStatus]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MPaymentStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MPaymentStatus](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[DDetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MPaymentStatus] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MPaymentStatus] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'ÂÑ§äÁèä´éªÓÃÐà§Ô¹', CAST(0x00009BE1016245EE AS DateTime), CAST(0x00009BE1016245EE AS DateTime), N'01')
INSERT [dbo].[MPaymentStatus] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'ªÓÃÐà§Ô¹áÅéÇ', CAST(0x00009BE101624DB2 AS DateTime), CAST(0x00009BE101624DB2 AS DateTime), N'01')
/****** Object:  Table [dbo].[MHour]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MHour]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MHour](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[DDetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MHour] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'0.5', CAST(0x00009BE20135573A AS DateTime), CAST(0x00009BF0015D79A4 AS DateTime), N'0001')
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'1.0', CAST(0x00009BE201355D32 AS DateTime), CAST(0x00009C060157C401 AS DateTime), N'0023')
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0003', N'1.5', CAST(0x00009BE2013560FC AS DateTime), CAST(0x00009BF0015D8375 AS DateTime), N'0001')
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0004', N'2.0', CAST(0x00009BE201356627 AS DateTime), CAST(0x00009C060157CA8A AS DateTime), N'0023')
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0005', N'2.5', CAST(0x00009C060157BA7B AS DateTime), CAST(0x00009C060157BA7B AS DateTime), N'0023')
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0006', N'3.5', CAST(0x00009C060157BEAC AS DateTime), CAST(0x00009C060157D358 AS DateTime), N'0023')
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0007', N'4.0', CAST(0x00009C060157DAAB AS DateTime), CAST(0x00009C060157DAAB AS DateTime), N'0023')
INSERT [dbo].[MHour] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0008', N'-', CAST(0x00009C060157F6BD AS DateTime), CAST(0x00009C060157F6BD AS DateTime), N'0023')
/****** Object:  StoredProcedure [dbo].[GetLastTranSaction]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLastTranSaction]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLastTranSaction]
AS
BEGIN
	select max(MID) RID from CustomerMemberPackage
END


' 
END
GO
/****** Object:  Table [dbo].[MAccountCode]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MAccountCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MAccountCode](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[DDetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MKeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MKeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MAccountCode] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MAccountCode] ([DNo], [DDetail], [MKeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0001', N'¤èÒãªéºÃÔ¡ÒÃ', CAST(0x00009BEC01591174 AS DateTime), CAST(0x00009BEC017C3935 AS DateTime), N'0001')
INSERT [dbo].[MAccountCode] ([DNo], [DDetail], [MKeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0002', N'¤èÒÊÁÑ¤ÃÊÁÒªÔ¡', CAST(0x00009BEC015923F3 AS DateTime), CAST(0x00009BEC017C3CEB AS DateTime), N'0001')
INSERT [dbo].[MAccountCode] ([DNo], [DDetail], [MKeyDate], [MLastEdit], [MKeyUser]) VALUES (N'0003', N'µèÍÍÒÂØÊÁÒªÔ¡', CAST(0x00009BEC015981CF AS DateTime), CAST(0x00009BFA015EA5B2 AS DateTime), N'0001')
/****** Object:  Table [dbo].[BOOKINVOICE]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BOOKINVOICE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BOOKINVOICE](
	[BNUM] [int] NOT NULL,
	[BRUNNING] [int] NULL,
 CONSTRAINT [PK_BOOKINVOICE] PRIMARY KEY CLUSTERED 
(
	[BNUM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[BOOKINVOICE] ([BNUM], [BRUNNING]) VALUES (1, 1)
/****** Object:  Table [dbo].[EmployeeSchedule]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeSchedule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeSchedule](
	[ESid] [int] IDENTITY(1,1) NOT NULL,
	[ESno] [varchar](5) COLLATE Thai_CI_AS NOT NULL,
	[EStartTime] [datetime] NULL,
	[ESEndTime] [datetime] NULL,
	[KeyDate] [datetime] NULL,
	[KeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LastEdit] [datetime] NULL,
	[EStatus] [varchar](1) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_EmployeeSchedule_1] PRIMARY KEY CLUSTERED 
(
	[ESid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'EmployeeSchedule', N'COLUMN',N'EStatus'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 ·Ó§Ò¹»¡µÔ, 2 ¢Ò´, 3 ÅÒ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmployeeSchedule', @level2type=N'COLUMN',@level2name=N'EStatus'
GO
/****** Object:  Table [dbo].[CustomerPayment]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPayment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerPayment](
	[SId] [int] IDENTITY(1,1) NOT NULL,
	[SCusTranID] [int] NULL,
	[SACCode] [varchar](4) COLLATE Thai_CI_AS NULL,
	[SAmount] [float] NULL,
	[SDiscount] [float] NULL,
	[SPayType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[SSlip] [varchar](100) COLLATE Thai_CI_AS NULL,
	[SBank] [varchar](4) COLLATE Thai_CI_AS NULL,
	[SStatus] [varchar](1) COLLATE Thai_CI_AS NULL,
	[SKeyDate] [datetime] NULL,
	[SLastEdit] [datetime] NULL,
	[SKeyUser] [nchar](10) COLLATE Thai_CI_AS NULL,
	[SBook] [int] NULL,
	[SRunning] [int] NULL,
	[SFLAG] [varchar](1) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_CustomerPayment] PRIMARY KEY CLUSTERED 
(
	[SId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerPayment', N'COLUMN',N'SStatus'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ê¶Ò¹Ð¡ÒÃªÓÃÐà§Ô¹ T ªÓÃÐà§Ô¹áÅéÇ F ÂÑ§äÁèä´éªÓÃÐ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerPayment', @level2type=N'COLUMN',@level2name=N'SStatus'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerPayment', N'COLUMN',N'SFLAG'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ê¶Ò¹ÐºÍ¡ÇèÒ¹Óä»¤Ô´¤èÒÁ×ÍáÅéÇ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerPayment', @level2type=N'COLUMN',@level2name=N'SFLAG'
GO
/****** Object:  Table [dbo].[Package]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Package]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Package](
	[Pno] [varchar](10) COLLATE Thai_CI_AS NOT NULL,
	[Pdetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[Phour] [varchar](4) COLLATE Thai_CI_AS NULL,
	[Pprice] [float] NULL,
	[PpriceforTherapist] [float] NULL,
	[PmoneyUnit] [varchar](3) COLLATE Thai_CI_AS NULL,
	[PMassageType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[Ppercourse] [int] NULL,
	[PCustomerType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[Pstart] [datetime] NULL,
	[Pend] [datetime] NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED 
(
	[Pno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Pno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Pno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Pdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Pdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pdetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Phour' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Phour' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Phour'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Phour'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Pprice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Pprice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pprice'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pprice'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PmoneyUnit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PmoneyUnit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PmoneyUnit'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PmoneyUnit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Pgroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Pgroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'PMassageType'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'PMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Pstart' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Pstart' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pstart'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pstart'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Pend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Pend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'Pend'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'Pend'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Package', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:24:59' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Package_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'Package', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Package'
GO
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'A_1', N'Aroma 1 hr.', N'0002', 600, 150, N'THB', N'0001', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D6801688F23 AS DateTime), CAST(0x00009BFB0168B3FE AS DateTime), CAST(0x00009BFB0168B3FE AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'A_1.5', N'Aroma 1.5 Hr', N'0003', 700, 200, N'THB', N'0001', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D680168B4FC AS DateTime), CAST(0x00009BFB0168D604 AS DateTime), CAST(0x00009C0B00ECB8F3 AS DateTime), N'0001')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'A_2', N'Aroma 2.0 hrs', N'0004', 700, 200, N'THB', N'0001', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D680168D6E5 AS DateTime), CAST(0x00009BFB0168F529 AS DateTime), CAST(0x00009BFB0168F529 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'AS_1.5', N'Aroma & Body Scrub 1.5 hr.', N'0003', 900, 200, N'THB', N'0003', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D6801695BC2 AS DateTime), CAST(0x00009BFB01698803 AS DateTime), CAST(0x00009BFB01698803 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'AS_2', N'Aroma & Body Scrub 2.0 hrs.', N'0004', 1000, 300, N'THB', N'0003', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D6801698901 AS DateTime), CAST(0x00009BFB0169ACAC AS DateTime), CAST(0x00009BFB0169ACAC AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'F_1', N'Foot 1 Hr.', N'0002', 250, 100, N'THB', N'0005', 1, N'0001', CAST(0x00009BFB01651BCD AS DateTime), CAST(0x00009D6801651726 AS DateTime), CAST(0x00009BFB016636F9 AS DateTime), CAST(0x00009BFB016636F9 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'F_1.5', N'Foot 1.5. Hr.', N'0003', 400, 150, N'THB', N'0005', 1, N'0001', CAST(0x00009BFB01651BCD AS DateTime), CAST(0x00009D68016637E8 AS DateTime), CAST(0x00009BFB0166773D AS DateTime), CAST(0x00009BFB0166BED1 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'F_2', N'Foot 2 Hrs.', N'0004', 500, 200, N'THB', N'0005', 1, N'0001', CAST(0x00009BFB01651BCD AS DateTime), CAST(0x00009D6801667857 AS DateTime), CAST(0x00009BFB0166B026 AS DateTime), CAST(0x00009BFB0166B026 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'FS_1', N'Foot Scrub 1 Hr', N'0002', 350, 120, N'THB', N'0006', 1, N'0001', CAST(0x00009BFB01672CFE AS DateTime), CAST(0x00009D6801672468 AS DateTime), CAST(0x00009BFB016758D3 AS DateTime), CAST(0x00009BFB016BBF12 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'FS_1.5', N'Foot Scrub 1.5 Hr.', N'0003', 450, 150, N'THB', N'0006', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D680167D82E AS DateTime), CAST(0x00009BFB01680965 AS DateTime), CAST(0x00009BFB01680965 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'MEM_T1', N'Member Thai 1 Hr.', N'0002', 5000, 100, N'THB', N'0004', 22, N'0002', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D680169AE52 AS DateTime), CAST(0x00009BFB016A0488 AS DateTime), CAST(0x00009BFB016A4532 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'MEM_T2', N'Member Thai 2 Hr.', N'0004', 8000, 150, N'THB', N'0004', 22, N'0002', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D68016A0563 AS DateTime), CAST(0x00009BFB016A3BD7 AS DateTime), CAST(0x00009BFB016A3BD7 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'MEMA_1', N'Member Aroma 1 Hr.', N'0002', 6000, 150, N'THB', N'0001', 12, N'0002', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D68016A4639 AS DateTime), CAST(0x00009BFB016AAC9D AS DateTime), CAST(0x00009BFB016AAC9D AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'MEMA_1.5', N'Member Aroma 1.5 Hr.', N'0003', 7000, 200, N'THB', N'0001', 12, N'0002', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D68016AAD84 AS DateTime), CAST(0x00009BFB016AD54D AS DateTime), CAST(0x00009BFB016AD54D AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'MEMA_2', N'Member Aroma 2 Hr.', N'0004', 8000, 250, N'THB', N'0001', 12, N'0002', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D68016AD638 AS DateTime), CAST(0x00009BFB016B01D5 AS DateTime), CAST(0x00009BFB016B01D5 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'MEMAS_2', N'Member Aroma & Body Scrub 2 Hr.', N'0004', 10000, 300, N'THB', N'0003', 12, N'0002', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D68016B02C5 AS DateTime), CAST(0x00009BFB016B2B6F AS DateTime), CAST(0x00009BFB016B2B6F AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'S_0.5', N'Body Scrub 0.5 hr.', N'0001', 600, 100, N'THB', N'0002', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D680168F622 AS DateTime), CAST(0x00009BFB0169335D AS DateTime), CAST(0x00009BFB0169335D AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'S_1', N'Body Scrub 1.0 hr.', N'0002', 800, 150, N'THB', N'0002', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D6801693464 AS DateTime), CAST(0x00009BFB01695AF3 AS DateTime), CAST(0x00009BFB01695AF3 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'SP_25000', N'Summer Package 25000', N'0008', 20000, 0, N'THB', N'0007', 25000, N'0002', CAST(0x00009C0601185CA8 AS DateTime), CAST(0x00009D73000034BC AS DateTime), CAST(0x00009C0601588F2D AS DateTime), CAST(0x00009C060182FF51 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'SP_27000', N'Summer Package 27000', N'0008', 20000, 0, N'THB', N'0007', 27000, N'0002', CAST(0x00009C0600C5F6E8 AS DateTime), CAST(0x00009D7301186AB8 AS DateTime), CAST(0x00009C0601578F40 AS DateTime), CAST(0x00009C060182E750 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'T_1', N'Thai 1 Hr.', N'0002', 250, 100, N'THB', N'0004', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D6801680A8D AS DateTime), CAST(0x00009BFB01682FF6 AS DateTime), CAST(0x00009BFB01682FF6 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'T_1.5', N'Thai 1.5 Hr.', N'0003', 350, 150, N'THB', N'0004', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D68016831EB AS DateTime), CAST(0x00009BFB01686200 AS DateTime), CAST(0x00009BFB01686200 AS DateTime), N'0023')
INSERT [dbo].[Package] ([Pno], [Pdetail], [Phour], [Pprice], [PpriceforTherapist], [PmoneyUnit], [PMassageType], [Ppercourse], [PCustomerType], [Pstart], [Pend], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'T_2', N'Thai 2.0 Hrs', N'0004', 400, 150, N'THB', N'0004', 1, N'0001', CAST(0x00009BFB0167DBA4 AS DateTime), CAST(0x00009D6801686357 AS DateTime), CAST(0x00009BFB01688E4B AS DateTime), CAST(0x00009BFB01688E4B AS DateTime), N'0023')
/****** Object:  Table [dbo].[MMassageType]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MMassageType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MMassageType](
	[Dno] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Ddetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_RoomType] PRIMARY KEY CLUSTERED 
(
	[Dno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'RTno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'RTno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'RoomType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'RTthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'RTthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'RoomType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'RoomType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'RoomType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:28:56' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'RoomType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'MMassageType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MMassageType'
GO
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0000', N'-', CAST(0x00009BFB0163F3D4 AS DateTime), CAST(0x00009BFB0163F3D4 AS DateTime), N'0001')
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'Aroma', CAST(0x00009BD501798629 AS DateTime), CAST(0x00009BEF01672E6C AS DateTime), N'0001')
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'Scrub', CAST(0x00009BDB00D059CC AS DateTime), CAST(0x00009BF000BBE9A2 AS DateTime), N'0001')
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0003', N'Aroma & Body Scrub', CAST(0x00009BD5017977C0 AS DateTime), CAST(0x00009BFB01633BAD AS DateTime), N'0001')
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0004', N'Thai', CAST(0x00009BE101566F9C AS DateTime), CAST(0x00009BFB01634595 AS DateTime), N'0001')
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0005', N'Foot', CAST(0x00009BF0015BBD19 AS DateTime), CAST(0x00009BFB01634F44 AS DateTime), N'0001')
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0006', N'Foot Scrub', CAST(0x00009BF0015BD277 AS DateTime), CAST(0x00009BFB0163585A AS DateTime), N'0001')
INSERT [dbo].[MMassageType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0007', N'Special Package', CAST(0x00009BFB0163F429 AS DateTime), CAST(0x00009BFB0163F429 AS DateTime), N'0001')
/****** Object:  Table [dbo].[MBank]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MBank]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MBank](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[DDetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MBank] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MBank] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'SCB', CAST(0x00009BE200C32256 AS DateTime), CAST(0x00009BE200C32256 AS DateTime), N'0001')
INSERT [dbo].[MBank] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'KTB', CAST(0x00009BE200C32793 AS DateTime), CAST(0x00009BE200C32793 AS DateTime), N'0001')
/****** Object:  Table [dbo].[MCustomerType]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MCustomerType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MCustomerType](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[DDetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_CustomerType] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MtypeNo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MtypeNo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CustomerType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MthtypeDetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MthtypeDetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CustomerType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'DDetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'DDetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CustomerType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'CustomerType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:23:03' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'CustomerType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'MCustomerType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MCustomerType'
GO
INSERT [dbo].[MCustomerType] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'General', CAST(0x00009BE200C340E1 AS DateTime), CAST(0x00009BE200C340E1 AS DateTime), N'0001')
INSERT [dbo].[MCustomerType] ([DNo], [DDetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'Member', CAST(0x00009BE200C34B3E AS DateTime), CAST(0x00009BE200C34B3E AS DateTime), N'0001')
/****** Object:  Table [dbo].[MNation]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MNation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MNation](
	[Dno] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Ddetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_Natioin] PRIMARY KEY CLUSTERED 
(
	[Dno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Nno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Nno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Natioin_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Nendetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Nendetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Natioin_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Natioin_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Natioin_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:29:55' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Natioin_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'MNation', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MNation'
GO
INSERT [dbo].[MNation] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'Thai', CAST(0x00009BDB00D14FAB AS DateTime), CAST(0x00009BDB00D14FAB AS DateTime), N'01')
INSERT [dbo].[MNation] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'Japan', CAST(0x00009BDB00D155E1 AS DateTime), CAST(0x00009BEF016171EF AS DateTime), N'0001')
INSERT [dbo].[MNation] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0003', N'English', CAST(0x00009BDB00D15D3D AS DateTime), CAST(0x00009BEF016179D3 AS DateTime), N'0001')
INSERT [dbo].[MNation] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0004', N'Korean', CAST(0x00009BEF01618AF2 AS DateTime), CAST(0x00009BEF01618AF2 AS DateTime), N'0001')
INSERT [dbo].[MNation] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0005', N'India', CAST(0x00009BEF0161929E AS DateTime), CAST(0x00009BEF0161929E AS DateTime), N'0001')
/****** Object:  Table [dbo].[MPaymentType]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MPaymentType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MPaymentType](
	[Dno] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Ddetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_PaymentType] PRIMARY KEY CLUSTERED 
(
	[Dno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PMno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'PMno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PaymentType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Pmthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Pmthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PaymentType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PaymentType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'PaymentType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:27:56' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'PaymentType_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'MPaymentType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MPaymentType'
GO
INSERT [dbo].[MPaymentType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'Cash', CAST(0x00009BD5017E02C7 AS DateTime), CAST(0x00009BDB00D07BDA AS DateTime), N'01')
INSERT [dbo].[MPaymentType] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'Credit Card', CAST(0x00009BD5017F0B2A AS DateTime), CAST(0x00009BDB00D08269 AS DateTime), N'01')
/****** Object:  Table [dbo].[Room]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Room]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Room](
	[Rno] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Rdetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MmassageType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[MRemark] [varchar](100) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[Rno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Rno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Rno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Room_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Rthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Rthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Room_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'Rdetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'Rdetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MRoomType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MRoomType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Room_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MmassageType'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MmassageType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Room_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Room_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Room', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:27:32' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Room_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'27' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'Room', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Room'
GO
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'G1', N'G1', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'G2', N'G2', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'G3', N'G3', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'G4', N'G4', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'G5', N'G5', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'G6', N'G6', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'G7', N'G7', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'L1', N'L1', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'L2', N'L2', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Thai')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'L3', N'L3', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'L4', N'L4', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'L5', N'L5', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T1', N'T1', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T2', N'T2', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T3', N'T3', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T4', N'T4', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T5', N'T5', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T6', N'T6', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T7', N'T7', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T8', N'T8', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
INSERT [dbo].[Room] ([Rno], [Rdetail], [MmassageType], [MkeyDate], [MLastEdit], [MkeyUser], [MRemark]) VALUES (N'T9', N'T9', N'0000', CAST(0x00009BFB0172B270 AS DateTime), CAST(0x00009BFB0172B270 AS DateTime), N'0001', N'Aroma / Aroma & Body Scrub')
/****** Object:  Table [dbo].[FrmMaster]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FrmMaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FrmMaster](
	[FID] [varchar](2) COLLATE Thai_CI_AS NOT NULL,
	[FNAME] [varchar](100) COLLATE Thai_CI_AS NULL,
	[FDESC] [varchar](100) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_FrmMaster] PRIMARY KEY CLUSTERED 
(
	[FID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'01', N'FrmPackage', N'µÑé§¤èÒà¾ç¤à¡¨')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'02', N'FrmRoom', N'µÑé§¤èÒËéÍ§')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'03', N'FrmScent', N'µÑé§¤èÒÇÑÊ´Ø')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'04', N'FrmSearch', N'µÑé§¤èÒ¢éÍÁÙÅËÅÑ¡')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'05', N'FrmTimePeriod', N'µÑé§¤èÒÃÍºàÇÅÒ')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'06', N'FrmEmployee', N'¢éÍÁÙÅ¾¹Ñ¡§Ò¹')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'07', N'FrmEmpSummary', N'¤èÒÁ×Í¾¹Ñ¡§Ò¹')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'08', N'FrmLeave', N'¡ÓË¹´ÇÑ¹ÅÒ')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'09', N'FrmLogin', N'ÅçÍ¤ÍÔ¹')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'10', N'FrmMain', N'Ë¹éÒËÅÑ¡')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'11', N'FrmMateriel', N'àºÔ¡ÇÑÊ´Ø')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'12', N'FrmMember', N'¢éÍÁÙÅÊÁÒªÔ¡')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'13', N'FrmMenu', N'àÁ¹ÙËÅÑ¡')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'14', N'FrmOption', N'Option')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'15', N'FrmPayment', N'ªÓÃÐà§Ô¹')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'16', N'FrmPrintReceipt', N'¾ÔÁ¾ìãºàÊÃç¨')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'17', N'FrmReport', N'ÃÒÂ§Ò¹')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'18', N'FrmRoomStatus', N'Ê¶Ò¹ÐËéÍ§')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'19', N'FrmServiceList', N'»ÃÐàÀ·à¾ç¤à¡¨')
INSERT [dbo].[FrmMaster] ([FID], [FNAME], [FDESC]) VALUES (N'20', N'FrmUseService', N'ãªéºÃÔ¡ÒÃ')
/****** Object:  Table [dbo].[MTitle]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MTitle]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MTitle](
	[Dno] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Ddetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED 
(
	[Dno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Tno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Tno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Title_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Dno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Dno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Tthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Tthdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Title_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Title_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Title_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:29:26' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Title_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTitle', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTitle'
GO
INSERT [dbo].[MTitle] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'Mr', CAST(0x00009BE200B47731 AS DateTime), CAST(0x00009BEF015D1C29 AS DateTime), N'0001')
INSERT [dbo].[MTitle] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'Miss', CAST(0x00009BE200B47CDF AS DateTime), CAST(0x00009BEF015CFE10 AS DateTime), N'0001')
INSERT [dbo].[MTitle] ([Dno], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0003', N'Mrs', CAST(0x00009BEF015D165D AS DateTime), CAST(0x00009BEF015D165D AS DateTime), N'0001')
/****** Object:  Table [dbo].[MTypeEmployee]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MTypeEmployee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MTypeEmployee](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[Ddetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_TypeEmployee] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'EtNo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'EtNo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'TypeEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'DNo'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'DNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Eentdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Eentdetail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'TypeEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'Ddetail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'Ddetail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'TypeEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'TypeEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:24:01' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'TypeEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'MTypeEmployee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MTypeEmployee'
GO
INSERT [dbo].[MTypeEmployee] ([DNo], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0001', N'Admin', CAST(0x00009BD80175F799 AS DateTime), CAST(0x00009BD80175F799 AS DateTime), N'01')
INSERT [dbo].[MTypeEmployee] ([DNo], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0002', N'Operator', CAST(0x00009BD80176021C AS DateTime), CAST(0x00009C13016E145D AS DateTime), N'0003')
INSERT [dbo].[MTypeEmployee] ([DNo], [Ddetail], [MkeyDate], [MLastEdit], [MkeyUser]) VALUES (N'0003', N'Therapist', CAST(0x00009BD8017608E2 AS DateTime), CAST(0x00009C13016E1DB8 AS DateTime), N'0003')
/****** Object:  Table [dbo].[EmpPermission]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmpPermission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmpPermission](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PUID] [varchar](50) COLLATE Thai_CI_AS NULL,
	[PFRM] [varchar](2) COLLATE Thai_CI_AS NULL,
	[PINSERT] [bit] NULL,
	[PDELETE] [bit] NULL,
	[PUPDATE] [bit] NULL,
	[PVIEW] [bit] NULL,
 CONSTRAINT [PK_EmpPermission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[EmpPermission] ON
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (182, N'0001', N'01', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (183, N'0001', N'02', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (184, N'0001', N'03', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (185, N'0001', N'04', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (186, N'0001', N'05', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (187, N'0001', N'06', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (188, N'0001', N'07', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (189, N'0001', N'08', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (190, N'0001', N'09', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (191, N'0001', N'10', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (192, N'0001', N'11', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (193, N'0001', N'12', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (194, N'0001', N'13', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (195, N'0001', N'14', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (196, N'0001', N'15', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (197, N'0001', N'16', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (198, N'0001', N'17', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (199, N'0001', N'18', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (200, N'0001', N'19', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (201, N'0001', N'20', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (202, N'0002', N'01', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (203, N'0002', N'02', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (204, N'0002', N'03', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (205, N'0002', N'04', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (206, N'0002', N'05', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (207, N'0002', N'06', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (208, N'0002', N'07', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (209, N'0002', N'08', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (210, N'0002', N'09', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (211, N'0002', N'10', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (212, N'0002', N'11', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (213, N'0002', N'12', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (214, N'0002', N'13', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (215, N'0002', N'14', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (216, N'0002', N'15', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (217, N'0002', N'16', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (218, N'0002', N'17', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (219, N'0002', N'18', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (220, N'0002', N'19', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (221, N'0002', N'20', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (222, N'0003', N'01', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (223, N'0003', N'02', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (224, N'0003', N'03', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (225, N'0003', N'04', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (226, N'0003', N'05', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (227, N'0003', N'06', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (228, N'0003', N'07', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (229, N'0003', N'08', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (230, N'0003', N'09', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (231, N'0003', N'10', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (232, N'0003', N'11', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (233, N'0003', N'12', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (234, N'0003', N'13', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (235, N'0003', N'14', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (236, N'0003', N'15', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (237, N'0003', N'16', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (238, N'0003', N'17', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (239, N'0003', N'18', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (240, N'0003', N'19', 1, 1, 1, 1)
INSERT [dbo].[EmpPermission] ([ID], [PUID], [PFRM], [PINSERT], [PDELETE], [PUPDATE], [PVIEW]) VALUES (241, N'0003', N'20', 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[EmpPermission] OFF
/****** Object:  Table [dbo].[Employee]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Employee](
	[Eno] [varchar](10) COLLATE Thai_CI_AS NOT NULL,
	[Etype] [varchar](4) COLLATE Thai_CI_AS NULL,
	[Esex] [varchar](1) COLLATE Thai_CI_AS NULL,
	[Etitle] [varchar](4) COLLATE Thai_CI_AS NULL,
	[Ename] [varchar](100) COLLATE Thai_CI_AS NULL,
	[Esurname] [varchar](100) COLLATE Thai_CI_AS NULL,
	[Enickname] [varchar](100) COLLATE Thai_CI_AS NULL,
	[EcontactAddress] [varchar](200) COLLATE Thai_CI_AS NULL,
	[Emobile] [varchar](10) COLLATE Thai_CI_AS NULL,
	[Eemail] [varchar](50) COLLATE Thai_CI_AS NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[MStatus] [varchar](1) COLLATE Thai_CI_AS NULL,
	[MPeriod] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LeaveBiz] [float] NULL,
	[LeaveHoliday] [float] NULL,
	[LeaveSick] [float] NULL,
 CONSTRAINT [PK_StaffEmployee] PRIMARY KEY CLUSTERED 
(
	[Eno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Eno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Eno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Etype' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Etype' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etype'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etype'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Esex' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Esex' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esex'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Etitle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Etitle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Etitle'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Etitle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Ename' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Ename' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Ename'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Ename'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Esurname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Esurname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Esurname'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Esurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Enickname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Enickname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Enickname'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Enickname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'EcontactAddress' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'EcontactAddress' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'EcontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'EcontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Emobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Emobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Emobile'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Emobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Eemail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Eemail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'Eemail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'Eemail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'dd/mm/yyyy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', N'COLUMN',N'MStatus'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A = Active , I = InActive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'MStatus'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:23:34' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:17' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'StaffEmployee_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'Employee', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee'
GO
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'0001', N'0001', N'M', N'0001', N'dev', N'-', N'dev', N'-', N'-', N'-', CAST(0x00009BFB0172B279 AS DateTime), CAST(0x00009C0C01114790 AS DateTime), N'0001', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'0002', N'0001', N'M', N'0002', N'siriluk', N'-', N'k-ying', N'-', N'-', N'-', CAST(0x00009BFB01731275 AS DateTime), CAST(0x00009BFB01733A36 AS DateTime), N'0000', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'0003', N'0002', N'M', N'0001', N'op3', N'-', N'op3', N'-', N'-', N'-', CAST(0x00009C13016163F6 AS DateTime), CAST(0x00009C130163EC0B AS DateTime), N'0001', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'10', N'0003', N'M', N'0002', N'Thai', N'-', N'Thai', N'-', N'-', N'-', CAST(0x00009BFB01748D3B AS DateTime), CAST(0x00009BFB01748D3B AS DateTime), N'0000', N'A', N'0001', 7, 10, 7)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'11', N'0003', N'M', N'0002', N'Fon', N'-', N'Fon', N'-', N'-', N'-', CAST(0x00009BFB0174A7AB AS DateTime), CAST(0x00009BFB0174A7AB AS DateTime), N'0000', N'A', N'0001', 7, 10, 7)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'12', N'0003', N'M', N'0002', N'Emmy', N'-', N'Emmy', N'-', N'-', N'-', CAST(0x00009BFB0174D33B AS DateTime), CAST(0x00009BFB0174D33B AS DateTime), N'0000', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'14', N'0003', N'M', N'0002', N'Mol', N'-', N'Mol', N'-', N'-', N'-', CAST(0x00009BFB0174F5AB AS DateTime), CAST(0x00009BFB0174F5AB AS DateTime), N'0000', N'A', N'0001', 8, 11, 8)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'15', N'0003', N'M', N'0002', N'Teaw', N'-', N'Teaw', N'-', N'-', N'-', CAST(0x00009BFB0175107A AS DateTime), CAST(0x00009BFB0175107A AS DateTime), N'0000', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'19', N'0003', N'M', N'0002', N'Yai', N'-', N'Yai', N'-', N'-', N'-', CAST(0x00009BFB01753DD0 AS DateTime), CAST(0x00009BFB01753DD0 AS DateTime), N'0000', N'A', N'0001', 8, 11, 8)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'2', N'0003', N'M', N'0002', N'Ging', N'-', N'Ging', N'-', N'-', N'-', CAST(0x00009BFB01738784 AS DateTime), CAST(0x00009BFB01738784 AS DateTime), N'0000', N'A', N'0001', 8, 11, 8)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'20', N'0003', N'M', N'0002', N'Nok', N'-', N'Nok', N'-', N'-', N'-', CAST(0x00009BFB01756FD7 AS DateTime), CAST(0x00009BFB01756FD7 AS DateTime), N'0000', N'A', N'0001', 7, 10, 7)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'21', N'0003', N'M', N'0002', N'Joy', N'-', N'Joy', N'-', N'-', N'-', CAST(0x00009BFB01759362 AS DateTime), CAST(0x00009BFB01759362 AS DateTime), N'0000', N'A', N'0001', 10, 13, 10)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'22', N'0003', N'M', N'0002', N'Noon', N'-', N'Noon', N'-', N'-', N'-', CAST(0x00009BFB0175D218 AS DateTime), CAST(0x00009BFB0175D218 AS DateTime), N'0000', N'A', N'0001', 8, 11, 8)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'25', N'0003', N'M', N'0002', N'Nadia', N'-', N'Nadia', N'-', N'-', N'-', CAST(0x00009BFB01760D96 AS DateTime), CAST(0x00009BFB01760D96 AS DateTime), N'0000', N'A', N'0001', 8, 11, 8)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'26', N'0003', N'M', N'0002', N'Aoi', N'-', N'Aoi', N'-', N'-', N'-', CAST(0x00009BFB0176338B AS DateTime), CAST(0x00009BFB0176338B AS DateTime), N'0000', N'A', N'0001', 7, 10, 7)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'27', N'0003', N'M', N'0002', N'Pen', N'-', N'Pen', N'-', N'-', N'-', CAST(0x00009BFB01764F23 AS DateTime), CAST(0x00009BFB01764F23 AS DateTime), N'0000', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'28', N'0003', N'M', N'0002', N'Som', N'-', N'Som', N'-', N'-', N'-', CAST(0x00009BFB0176704C AS DateTime), CAST(0x00009BFB0176704C AS DateTime), N'0000', N'A', N'0001', 10, 13, 10)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'31', N'0003', N'M', N'0002', N'Pat', N'-', N'Pat', N'-', N'-', N'-', CAST(0x00009BFB01769024 AS DateTime), CAST(0x00009BFB01769024 AS DateTime), N'0000', N'A', N'0001', 10, 13, 10)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'33', N'0003', N'M', N'0002', N'Tata', N'-', N'Tata', N'-', N'-', N'-', CAST(0x00009BFB0176B2A5 AS DateTime), CAST(0x00009C0C011035C4 AS DateTime), N'0001', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'35', N'0003', N'M', N'0002', N'Bam', N'-', N'Bam', N'-', N'-', N'-', CAST(0x00009BFB0176EBF3 AS DateTime), CAST(0x00009C0C010FAC87 AS DateTime), N'0001', N'A', N'0001', 7, 10, 7)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'36', N'0003', N'M', N'0002', N'Nat', N'-', N'Nat', N'-', N'-', N'-', CAST(0x00009BFB01770E27 AS DateTime), CAST(0x00009BFB01770E27 AS DateTime), N'0000', N'A', N'0001', 0, 0, 0)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'42', N'0003', N'M', N'0002', N'Poo', N'-', N'Poo', N'-', N'-', N'-', CAST(0x00009BFB01773811 AS DateTime), CAST(0x00009BFB01773811 AS DateTime), N'0000', N'A', N'0001', 8, 11, 8)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'48', N'0003', N'M', N'0002', N'Ket', N'-', N'Ket', N'-', N'-', N'-', CAST(0x00009BFB01776AEF AS DateTime), CAST(0x00009BFB01776AEF AS DateTime), N'0000', N'A', N'0001', 7, 10, 7)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'5', N'0003', N'M', N'0002', N'Chaba', N'-', N'Chaba', N'-', N'-', N'-', CAST(0x00009BFB0173BD13 AS DateTime), CAST(0x00009BFB0173E612 AS DateTime), N'0000', N'A', N'0001', 10, 13, 10)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'7', N'0003', N'M', N'0001', N'Yah', N'-', N'Yah', N'-', N'-', N'-', CAST(0x00009BFB0173DDD5 AS DateTime), CAST(0x00009BFB0173DDD5 AS DateTime), N'0000', N'A', N'0001', 10, 13, 10)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'8', N'0003', N'M', N'0002', N'Sann', N'-', N'Sann', N'-', N'-', N'-', CAST(0x00009BFB01744984 AS DateTime), CAST(0x00009C0C01107AB0 AS DateTime), N'0001', N'A', N'0001', 7, 10, 7)
INSERT [dbo].[Employee] ([Eno], [Etype], [Esex], [Etitle], [Ename], [Esurname], [Enickname], [EcontactAddress], [Emobile], [Eemail], [MkeyDate], [MLastEdit], [MkeyUser], [MStatus], [MPeriod], [LeaveBiz], [LeaveHoliday], [LeaveSick]) VALUES (N'9', N'0003', N'M', N'0002', N'Tip', N'-', N'Tip', N'-', N'-', N'-', CAST(0x00009BFB017463AE AS DateTime), CAST(0x00009C0C01106937 AS DateTime), N'0001', N'A', N'0001', 7, 10, 7)
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeName]    Script Date: 06/02/2009 13:02:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetEmployeeName]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select EnickName from Employee where Eno = @inID)
	return @data
END



' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetAgePeriod]    Script Date: 06/02/2009 13:02:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAgePeriod]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	¡ÓË¹´ ¡ÅØèÁÍÒÂØ
-- =============================================
CREATE FUNCTION [dbo].[GetAgePeriod]
(
	@inGroup varchar(2)
)
RETURNS varchar(100)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @data varchar(100)

	-- Add the T-SQL statements to compute the return value here
		if @inGroup = ''1''
			set @data = ''20-30''
		else if @inGroup = ''2''
			set @data = ''30-40''
		else if @inGroup = ''3''
			set @data = ''40-50''
		else if @inGroup = ''4''
			set @data = ''50-60''
	-- Return the result of the function
	RETURN @data

END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageID]    Script Date: 06/02/2009 13:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	Get Package ID
-- =============================================
CREATE FUNCTION [dbo].[GetPackageID]
(
	@inMID varchar(10),--package id
	@inCus varchar(4),
	@inSp varchar(10)
)
RETURNS varchar(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @MID varchar(10)
	if @inCus = ''0001'' 
		set @MID = @inMID
	else
		if @inSp =''0000''
			set @MID = (select Mpackage from CustomerMemberPackage where MID = @inMID)
		else
			set @MID = @inMID

	-- Return the result of the function
	RETURN @MID

END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetCustomerLastID]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCustomerLastID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCustomerLastID]
(
	@inCusType varchar(4)
)
AS
BEGIN
	select (count(Mno)+1) icount from customer where MType = @inCusType
END

' 
END
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer](
	[MID] [int] IDENTITY(1,1) NOT NULL,
	[Mno] [varchar](25) COLLATE Thai_CI_AS NOT NULL,
	[MType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[Msex] [varchar](4) COLLATE Thai_CI_AS NULL,
	[Mname] [varchar](100) COLLATE Thai_CI_AS NULL,
	[Msurname] [varchar](100) COLLATE Thai_CI_AS NULL,
	[Mnational] [varchar](4) COLLATE Thai_CI_AS NULL,
	[MAge] [int] NULL,
	[McontactAddress] [varchar](100) COLLATE Thai_CI_AS NULL,
	[Mmobile] [varchar](10) COLLATE Thai_CI_AS NULL,
	[Memail] [varchar](50) COLLATE Thai_CI_AS NULL,
	[MstartDate] [datetime] NULL,
	[MendDate] [datetime] NULL,
	[MkeyDate] [datetime] NULL,
	[MLastEdit] [datetime] NULL,
	[MkeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[MID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Mno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Mno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mno'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mno'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Msex' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Msex' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msex'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msex'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Mname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Mname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mname'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Msurname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Msurname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Msurname'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Msurname'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Mnational' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'6' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Mnational' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mnational'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mnational'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Myear' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'7' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Myear' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MAge'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MAge'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'McontactAddress' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'8' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'McontactAddress' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'McontactAddress'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'McontactAddress'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Mmobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'9' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Mmobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Mmobile'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Mmobile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Memail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Memail' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'Memail'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'Memail'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MstartDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'11' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MstartDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MstartDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MstartDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MendDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'12' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MendDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MendDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MendDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyDate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyDate'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'14' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'MkeyUser' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', N'COLUMN',N'MkeyUser'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer', @level2type=N'COLUMN',@level2name=N'MkeyUser'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:22:19' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:18' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Customer_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'Customer', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
/****** Object:  Table [dbo].[CustomerMemberPackage]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerMemberPackage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerMemberPackage](
	[MID] [int] IDENTITY(1,1) NOT NULL,
	[MCusNo] [varchar](25) COLLATE Thai_CI_AS NULL,
	[MPackage] [varchar](10) COLLATE Thai_CI_AS NULL,
	[MUseMember] [int] NULL,
	[KeyDate] [datetime] NULL,
	[KeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LastEdit] [datetime] NULL,
 CONSTRAINT [PK_MemberTransaction] PRIMARY KEY CLUSTERED 
(
	[MID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[MemberPayment]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MemberPayment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MemberPayment](
	[TNo] [int] IDENTITY(1,1) NOT NULL,
	[TMemberID] [varchar](25) COLLATE Thai_CI_AS NULL,
	[TTherapist] [varchar](4) COLLATE Thai_CI_AS NULL,
	[TPackageTranID] [int] NULL,
	[SACCode] [varchar](4) COLLATE Thai_CI_AS NULL,
	[SAmount] [float] NULL,
	[SDiscount] [float] NULL,
	[SPaytype] [varchar](4) COLLATE Thai_CI_AS NULL,
	[SSlip] [varchar](100) COLLATE Thai_CI_AS NULL,
	[SBank] [varchar](4) COLLATE Thai_CI_AS NULL,
	[SkeyDate] [datetime] NULL,
	[SLastEdit] [datetime] NULL,
	[SKeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[TNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AggregateType' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'AggregateType', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AllowZeroLength' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'AllowZeroLength', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'AppendOnly' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'AppendOnly', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'CollatingOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'CollatingOrder', @value=N'1054' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnHidden' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnHidden', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnOrder' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnOrder', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'ColumnWidth' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'ColumnWidth', @value=N'-1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DataUpdatable' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'DataUpdatable', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DisplayControl' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Format' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Format', @value=N'@' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMEMode' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_IMESentMode' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'MS_IMESentMode', @value=N'3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Tno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrdinalPosition' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'OrdinalPosition', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Required' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'Required', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Size' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'Size', @value=N'255' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceField' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'SourceField', @value=N'Tno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'SourceTable' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'SourceTable', @value=N'Transaction_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TextAlign' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'TextAlign', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Type' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'UnicodeCompression' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', N'COLUMN',N'TNo'))
EXEC sys.sp_addextendedproperty @name=N'UnicodeCompression', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment', @level2type=N'COLUMN',@level2name=N'TNo'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Attributes' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Attributes', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DateCreated' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DateCreated', @value=N'21/03/2009 22:26:38' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'DisplayViewsOnSharePointSite' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'DisplayViewsOnSharePointSite', @value=N'1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'FilterOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'FilterOnLoad', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'HideNewField' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'HideNewField', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'LastUpdated' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'LastUpdated', @value=N'21/03/2009 22:33:16' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_DefaultView' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=N'2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_OrderByOn' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Orientation' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=N'0' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Name' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Name', @value=N'Transaction_local' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'OrderByOnLoad' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'OrderByOnLoad', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'RecordCount' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'RecordCount', @value=N'5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TotalsRow' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TotalsRow', @value=N'False' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'Updatable' , N'SCHEMA',N'dbo', N'TABLE',N'MemberPayment', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'Updatable', @value=N'True' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MemberPayment'
GO
/****** Object:  Table [dbo].[CustomerTransaction]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerTransaction](
	[Rid] [int] IDENTITY(1,1) NOT NULL,
	[RmemberID] [varchar](25) COLLATE Thai_CI_AS NULL,
	[RTherapistID] [varchar](4) COLLATE Thai_CI_AS NULL,
	[RTherapistRevID] [varchar](4) COLLATE Thai_CI_AS NULL,
	[RPackage] [varchar](10) COLLATE Thai_CI_AS NULL,
	[RDate] [datetime] NULL,
	[RTime] [datetime] NULL,
	[RRoom] [varchar](4) COLLATE Thai_CI_AS NULL,
	[RCustomerType] [varchar](4) COLLATE Thai_CI_AS NULL,
	[RScent] [varchar](4) COLLATE Thai_CI_AS NULL,
	[RScentcount] [int] NULL,
	[RType] [varchar](1) COLLATE Thai_CI_AS NULL,
	[Remark] [varchar](100) COLLATE Thai_CI_AS NULL,
	[KeyDate] [datetime] NULL,
	[KeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LastEdit] [datetime] NULL,
	[RSpecialPackage] [varchar](10) COLLATE Thai_CI_AS NULL,
	[SFLAG] [varchar](1) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_MemberReservation] PRIMARY KEY CLUSTERED 
(
	[Rid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerTransaction', N'COLUMN',N'RTherapistRevID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'à¡çº¾¹Ñ¡§Ò¹¹Ç´·ÕèÁÕ¡ÒÃ¨Í§' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerTransaction', @level2type=N'COLUMN',@level2name=N'RTherapistRevID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerTransaction', N'COLUMN',N'RDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ÇÑ¹·Õè¨ÐÁÒãªéºÃÔ¡ÒÃ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerTransaction', @level2type=N'COLUMN',@level2name=N'RDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerTransaction', N'COLUMN',N'RType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 ¨Í§, 2 ãºéªÃÔ¡ÒÃ, 3 Â¡àÅÔ¡' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerTransaction', @level2type=N'COLUMN',@level2name=N'RType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerTransaction', N'COLUMN',N'LastEdit'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag à¾×èÍºÍ¡ÇèÒà»ç¹ Special Package' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerTransaction', @level2type=N'COLUMN',@level2name=N'LastEdit'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerTransaction', N'COLUMN',N'RSpecialPackage'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag à¾×èÍºÍ¡ÇèÒãªé Special Package ÍÑ¹äË¹' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerTransaction', @level2type=N'COLUMN',@level2name=N'RSpecialPackage'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'CustomerTransaction', N'COLUMN',N'SFLAG'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FLAG à¾×èÍºÍ¡ÇèÒ¹Óä»¤Ô´¤èÒÁ×ÍáÅéÇ C= Complete ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomerTransaction', @level2type=N'COLUMN',@level2name=N'SFLAG'
GO
/****** Object:  Table [dbo].[MUSER]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MUSER]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MUSER](
	[UREFID] [varchar](25) COLLATE Thai_CI_AS NOT NULL,
	[UPASS] [varchar](8) COLLATE Thai_CI_AS NULL,
	[UTYPE] [varchar](4) COLLATE Thai_CI_AS NULL,
	[UAUTHORIZE] [varchar](100) COLLATE Thai_CI_AS NULL,
	[KEYDATE] [datetime] NULL,
	[KEYUSER] [varchar](4) COLLATE Thai_CI_AS NULL,
	[LASTTIME] [datetime] NULL,
 CONSTRAINT [PK_MUSER_1] PRIMARY KEY CLUSTERED 
(
	[UREFID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'0001', N'dev', N'0001', N'', CAST(0x00009BFB0172B27A AS DateTime), N'0001', CAST(0x00009C1C00CD2422 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'0002', N'-', N'0001', N'', CAST(0x00009BFB01731275 AS DateTime), N'0000', CAST(0x00009BFB01733A36 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'0003', N'1234', N'0002', N'', CAST(0x00009BFB017462DC AS DateTime), N'0001', CAST(0x00009C13017166F9 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'10', N'-', N'0003', N'', CAST(0x00009BFB01748D3B AS DateTime), N'0000', CAST(0x00009BFB01748D3B AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'11', N'-', N'0003', N'', CAST(0x00009BFB0174A7AC AS DateTime), N'0000', CAST(0x00009BFB0174A7AC AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'12', N'-', N'0003', N'', CAST(0x00009BFB0174D33B AS DateTime), N'0000', CAST(0x00009BFB0174D33B AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'14', N'-', N'0003', N'', CAST(0x00009BFB0174F5AC AS DateTime), N'0000', CAST(0x00009BFB0174F5AC AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'15', N'-', N'0003', N'', CAST(0x00009BFB0175107B AS DateTime), N'0000', CAST(0x00009BFB0175107B AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'19', N'-', N'0003', N'', CAST(0x00009BFB01753DD1 AS DateTime), N'0000', CAST(0x00009BFB01753DD1 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'2', N'0000', N'0003', N'', CAST(0x00009BFB01738784 AS DateTime), N'0000', CAST(0x00009BFB01738784 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'20', N'-', N'0003', N'', CAST(0x00009BFB01756FD7 AS DateTime), N'0000', CAST(0x00009BFB01756FD7 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'21', N'-', N'0003', N'', CAST(0x00009BFB01759362 AS DateTime), N'0000', CAST(0x00009BFB01759362 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'22', N'-', N'0003', N'', CAST(0x00009BFB0175D218 AS DateTime), N'0000', CAST(0x00009BFB0175D218 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'25', N'-', N'0003', N'', CAST(0x00009BFB01760D96 AS DateTime), N'0000', CAST(0x00009BFB01760D96 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'26', N'-', N'0003', N'', CAST(0x00009BFB0176338B AS DateTime), N'0000', CAST(0x00009BFB0176338B AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'27', N'-', N'0003', N'', CAST(0x00009BFB01764F24 AS DateTime), N'0000', CAST(0x00009BFB01764F24 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'28', N'-', N'0003', N'', CAST(0x00009BFB0176704D AS DateTime), N'0000', CAST(0x00009BFB0176704D AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'31', N'-', N'0003', N'', CAST(0x00009BFB01769024 AS DateTime), N'0000', CAST(0x00009BFB01769024 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'33', N'-', N'0003', N'', CAST(0x00009BFB0176B2A5 AS DateTime), N'0001', CAST(0x00009C0C011035C4 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'35', N'-', N'0003', N'', CAST(0x00009BFB0176EBF3 AS DateTime), N'0001', CAST(0x00009C0C010FAC8B AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'36', N'-', N'0003', N'', CAST(0x00009BFB01770E27 AS DateTime), N'0000', CAST(0x00009BFB01770E27 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'42', N'-', N'0003', N'', CAST(0x00009BFB01773811 AS DateTime), N'0000', CAST(0x00009BFB01773811 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'48', N'-', N'0003', N'', CAST(0x00009BFB01776AEF AS DateTime), N'0000', CAST(0x00009BFB01776AEF AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'5', N'-', N'0003', N'', CAST(0x00009BFB0173BD13 AS DateTime), N'0000', CAST(0x00009BFB0173E613 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'7', N'0000', N'0003', N'', CAST(0x00009BFB0173DDD5 AS DateTime), N'0000', CAST(0x00009BFB0173DDD5 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'8', N'-', N'0003', N'', CAST(0x00009BFB01744984 AS DateTime), N'0001', CAST(0x00009C0C01107AB0 AS DateTime))
INSERT [dbo].[MUSER] ([UREFID], [UPASS], [UTYPE], [UAUTHORIZE], [KEYDATE], [KEYUSER], [LASTTIME]) VALUES (N'9', N'-', N'0003', N'', CAST(0x00009BFB017463AF AS DateTime), N'0001', CAST(0x00009C0C01106938 AS DateTime))
/****** Object:  Table [dbo].[EmployeeSummary]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeSummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeSummary](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TherapistID] [varchar](4) COLLATE Thai_CI_AS NULL,
	[GuestReserv] [int] NULL,
	[Income] [float] NULL,
	[soldMember] [float] NULL,
	[find] [float] NULL,
	[Remain] [float] NULL,
	[keydate] [datetime] NULL,
	[keyuser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[lastedit] [datetime] NULL,
	[SFLAG] [varchar](10) COLLATE Thai_CI_AS NULL,
 CONSTRAINT [PK_EmployeeSummary] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'EmployeeSummary', N'COLUMN',N'SFLAG'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D = Delete ¢éÍÁÙÅ Transaction áÅéÇ , NULL = ÂÑ§' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmployeeSummary', @level2type=N'COLUMN',@level2name=N'SFLAG'
GO
/****** Object:  UserDefinedFunction [dbo].[CheckISPay]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckISPay]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[CheckISPay]
(
	@inTID int
)
RETURNS int
AS
BEGIN
	declare @icount int
	set @icount = (select count(SId) from customerPayment where ScusTranID = @inTID)
	return @icount
END
' 
END
GO
/****** Object:  Table [dbo].[MLeave]    Script Date: 06/02/2009 13:02:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MLeave]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MLeave](
	[DNo] [varchar](4) COLLATE Thai_CI_AS NOT NULL,
	[DDetail] [varchar](100) COLLATE Thai_CI_AS NULL,
	[MKeyDate] [datetime] NULL,
	[MKeyUser] [varchar](4) COLLATE Thai_CI_AS NULL,
	[MLastEdit] [datetime] NULL,
 CONSTRAINT [PK_MLeave] PRIMARY KEY CLUSTERED 
(
	[DNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[MLeave] ([DNo], [DDetail], [MKeyDate], [MKeyUser], [MLastEdit]) VALUES (N'0001', N'µÔ´¸ØÃÐ', CAST(0x00009BF000EC857B AS DateTime), N'0001', CAST(0x00009BF20180D1F2 AS DateTime))
INSERT [dbo].[MLeave] ([DNo], [DDetail], [MKeyDate], [MKeyUser], [MLastEdit]) VALUES (N'0002', N'ÅÒ¾Ñ¡ÃéÍ¹', CAST(0x00009BF000ED3E24 AS DateTime), N'0001', CAST(0x00009BF20180C570 AS DateTime))
INSERT [dbo].[MLeave] ([DNo], [DDetail], [MKeyDate], [MKeyUser], [MLastEdit]) VALUES (N'0003', N'»èÇÂ', CAST(0x00009BF000ED4BF3 AS DateTime), N'0001', CAST(0x00009BF20180D7BB AS DateTime))
INSERT [dbo].[MLeave] ([DNo], [DDetail], [MKeyDate], [MKeyUser], [MLastEdit]) VALUES (N'0004', N'¢Ò´§Ò¹', CAST(0x00009BF20180ADD2 AS DateTime), N'0001', CAST(0x00009BF20180ADD2 AS DateTime))
/****** Object:  StoredProcedure [dbo].[manageCustomerPayment]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageCustomerPayment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageCustomerPayment](
	@inSid int,
	@inSCusTranID int,
	@inSACCode varchar(4),
	@inSDiscount float,
	@inSAmount float,
	@inSPayType varchar(4),
	@inSSlip varchar(100),
	@inSBank varchar(4),
	@inSStatus varchar(1),
	@inSKeyUser varchar(4),
	@inPackageID varchar(10),
	@inIsSppackage varchar(1),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert
		begin
			declare @BookNumber int
			declare @BookRunning int
			begin
			--Get BookRunning
			set @BookNumber = (select (isnull(max(BNUM),0)+1) BNUM from BookInvoice)
			set @Bookrunning = (select (isnull(max(BRUNNING),0)+1) BRUNNING from BookInvoice where BNUM = @BookNumber-1)
			--Check Running
			if @BookRunning > 999 
				begin
					--Insert 
					insert into BookInvoice(BNUM,BRUNNING)
					values(@BookNumber,''1'')
				end
			else
				begin
					update BookInvoice set
					BRUNNING = @Bookrunning
					where BNUM = @BookNumber-1
				end
			end
		set @BookNumber = (SELECT TOP 1  BNUM FROM BookInvoice ORDER BY BNUM desc)
		set @BookRunning = (SELECT TOP 1  BRUNNING FROM BookInvoice ORDER BY BNUM desc)
		insert into CustomerPayment(SCusTranID,SACCode,SAmount,SPayType,SSlip,SBank,SStatus,SKeyDate,SLastEdit,SKeyUser,SDiscount,SBook,SRunning)
		values(@inSCusTranID,@inSACCode,@inSAmount,@inSPayType,@inSSlip,@inSBank,@inSStatus,GetDate(),GetDate(),@inSKeyUser,@inSDiscount,@BookNumber,@BookRunning)
		
		--¶éÒà»ç¹ ÊÁÒªÔ¡ãËé¨Ó¹Ç¹¡ÒÃãªéà¾ÔèÁ¢Öé¹ 1
		declare @custype varchar(4)
		set @custype =(select RcustomerType from CustomerTransaction where rid = @inSCusTranID)
		if @custype = ''0002'' -- ¶éÒà»ç¹ÊÁÒªÔ¡ãËéà¾ÔèÁ¨Ó¹Ç¹¤ÃÑé§¡ÒÃãªé§Ò¹
			begin
				if @inIsSppackage = ''T''
					update CustomerMemberPackage set	
						MUseMember = (MUseMember+@inSAmount),
						KeyUser = @inSKeyUser,
						LastEdit = GetDate()
					where Mid = @inPackageID
				else
					update CustomerMemberPackage set	
						MUseMember = (MUseMember+1),
						KeyUser = @inSKeyUser,
						LastEdit = GetDate()
					where Mid = @inPackageID
			end

		--»ÃÑº»ÃØ§ÃÒÂ¡ÒÃ¢Í§¹éÓËÍÁ (»ÃÐàÀ·àºÔ¡ÃÒÂ¡ÒÃ)
			begin
				declare @ScentType varchar(4)
				declare @ScentCount int
				set @ScentType =(select RScent from customertransaction where Rid = @inSCusTranID)
				set @ScentCount = (select RScentCount from customertransaction where Rid = @inSCusTranID)
				insert MScentTransaction(SCtype,SCount,Stype,KeyDate,LastEdit,KeyUser)
				values(@ScentType,@ScentCount,''2'',GetDate(),GetDate(),@inSKeyUser)
			end
		end
--	else if @inFlag = ''1''--Update
--		update CustomerPayment set 
--			SCusTranID = @inSCusTranID,
--			SACCode = @inSACCode,
--			SDiscount = @inSDiscount,
--			SAmount = @inSAmount,
--			SPayType = @inSPayType,
--			SSlip = @inSSlip,
--			SBank = @inSBank,
--			SStatus = @inSStatus,
--			SLastEdit = GetDate(),
--			SKeyUser = @inSKeyUser
--		where Sid = @inSid
--	else if @inFlag = ''2''--Delete
--		delete CustomerPayment where Sid = @inSid
--	else if @inFlag = ''3''--Select
--		select * from CustomerPayment order by sid











' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMScentTransaction]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMScentTransaction]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	à¡çº¢éÍÁÙÅ¤ØÃØÀÑ³±ì
-- =============================================
CREATE PROCEDURE [dbo].[manageMScentTransaction]
(
	@inSID int,
	@inSCtype varchar(4),
	@inSCount varchar(4),
	@inStype varchar(4),
	@inKeyUser varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''
		insert MScentTransaction(SCtype,SCount,Stype,KeyDate,LastEdit,KeyUser)
		values(@inSCtype,@inSCount,@inStype,GetDate(),GetDate(),@inKeyUser)
	else if @inFlag = ''1''
		update MScentTransaction set
			SCtype = @inSCtype,
			SCount = @inSCount,
			Stype = @inStype,
			LastEdit = GetDate(),
			KeyUser = @inKeyUser
		where SID = @inSID
	else if @inFlag = ''2''
		delete from MScentTransaction where SID = @inSID
	else if @inFlag = ''3''
		select * from MScentTransaction
	else if @inFlag = ''4''--áÊ´§ÃÒÂ¡ÒÃÇèÒáµèÅÐ»ÃÐàÀ·¡ÅÔè¹ÁÕ¨Ó¹Ç¹ã¹ stock à·èÒäËÃè
		begin
			select Dno,
			DDetail,
			(select (isnull(tt1.scount1,0) - isnull(tt2.scount2,0)) from
			(select sum(Scount) scount1 
			from MscentTransaction 
			where SCtype = t1.Dno
			and SType = ''1''---ÃÑºÁÒ
			)tt1,
			(select sum(Scount) scount2 
			from MscentTransaction 
			where SCtype = t1.Dno
			and SType = ''2''---¨èÒÂÍÍ¡
			)tt2
			) as Remain,
			t1.Dmin
			from MScent t1
			group by t1.Dno,t1.DDetail,t1.Dmin
		end
	else if @inFlag = ''5''--áÊ´§ÃÒÂ¡ÒÃÇèÒáµèÅÐ»ÃÐàÀ·¡ÅÔè¹ÁÕ¨Ó¹Ç¹ã¹ stock à·èÒäËÃè áÂ¡µÒÁÃËÑÊ
		begin
			select Dno,
			DDetail,
			(select (isnull(tt1.scount1,0) - isnull(tt2.scount2,0)) from
			(select sum(Scount) scount1 
			from MscentTransaction 
			where SCtype = t1.Dno
			and SType = ''1''---ÃÑºÁÒ
			)tt1,
			(select sum(Scount) scount2 
			from MscentTransaction 
			where SCtype = t1.Dno
			and SType = ''2''---¨èÒÂÍÍ¡
			)tt2
			) as Remain,
			t1.Dmin
			from MScent t1
			where Dno = @inSCtype
			group by t1.Dno,t1.DDetail,t1.Dmin
		end
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageEmployeeLeave]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployeeLeave]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	à¡çº¢éÍÁÙÅ¡ÒÃÅÒ¢Í§¾¹Ñ¡§Ò¹
-- =============================================
CREATE PROCEDURE [dbo].[manageEmployeeLeave]
(
	@inID int,
	@inEmpID varchar(4),
	@inLType varchar(4),
	@inLStartDate datetime,
	@inLEndDate datetime,
	@inLCount float,
	@inLReason varchar(100),
	@inKeyDate datetime,
	@inKeyUser varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''
		insert into EmployeeLeave(EmpID,Ltype,LstartDate,LEndDate,LCount,LReason,Keydate,KeyUser,LastEdit)
		values(@inEmpID,@inLType,@inLStartDate,@inLEndDate,@inLCount,@inLReason,GetDate(),@inKeyUser,GetDate())
	else if @inFlag = ''1''
		update EmployeeLeave set
			EmpID =@inEmpID,
			Ltype =@inLType,
			LstartDate =@inLStartDate,
			LEndDate =  @inLEndDate,
			LCount =@inLCount,
			LReason =@inLReason,
			KeyUser =@inKeyUser,
			LastEdit = GetDate()
		where ID = @inID
	else if @inFlag = ''2''
		delete from EmployeeLeave where ID = @inID
	else if @inFlag = ''3''
		select * from EmployeeLeave where convert(varchar(30),Keydate,101) = convert(varchar(30),@inKeyDate,101)
	else if @inFlag = ''4''--µÃÇ¨ÊÍºÇèÒãªé ¡ÒÃÅÒáµèÅÐ»ÃÐàÀ·ä»¡Õè¤ÃÑé§áÅéÇ
		select isnull(Lcount,0) Lcount from EmployeeLeave where EmpID =@inEmpID and Ltype = @inLType
	else if @inFlag = ''5''--µÃÇ¨ÊÍº¢éÍÁÙÅ¡ÒÃÅÒ¢Í§áµèÅÐ¤¹
		select id,empid,ename,LType,LStartDate,LEndDate,LCount 
		from employeeleave t1,employee t2 
		where empid = @inEmpID
		and t1.empid = t2.Eno
		and etype = ''0003''--à©¾ÒÐËÁÍ¹Ç´
END






' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeLeaveStatus]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEmployeeLeaveStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description: µÃÇ¨ÊÍºÇèÒÇÑ¹¹Õé¾¹Ñ¡§Ò¹ÅÒËÃ×ÍäÁè
-- =============================================
CREATE PROCEDURE [dbo].[GetEmployeeLeaveStatus]
(
	@EmpID varchar(4)
)
AS
BEGIN
	select (select DDetail from MLeave where Dno =LType) as Ltype,
	LCount, --¨Ó¹Ç¹ÇÑ¹ÅÒ ¡Ã³Õà»ç¹ ¤ÃÖè§ÇÑ¹
	datediff(day,LEndDate,LStartDate) Lcount1 --¨Ó¹Ç¹ÇÑ¹·ÕèÅÒ¤§àËÅ×Í (ÇÑ¹·ÕèÅÒ¶Ö§ - ÇÑ¹·ÕèàÃÔèÁÅÒ),
	from employeeLeave 
	where EmpID = @EmpID
	and convert(varchar(30),GetDate(),101) between convert(varchar(30),LStartDate,101) 
	and convert(varchar(30),LEndDate,101)		
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageTimePeriod]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageTimePeriod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageTimePeriod]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inStart datetime,
	@inEnd datetime,
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert

		insert into MTimePeriod(DNo,DDetail,MPStart,MPEnd,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,@inStart,@inEnd,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MTimePeriod set 
						DDetail = @inDetail,
						MPStart = @inStart,
						MPEnd = @inEnd,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MTimePeriod where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MTimePeriod order by dno
	else if @inFlag = ''4''--Select
		select * from MTimePeriod where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MTimePeriod where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH


' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMSex]    Script Date: 06/02/2009 13:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMSex]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetMSex]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select DDetail from MSex where Dno = @inID)
	return @data
END




' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMhour]    Script Date: 06/02/2009 13:02:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMhour]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMhour]
(
	@inID varchar(4)
)
RETURNS float
AS
BEGIN
	declare @data float
	set @data = (select Ddetail from Mhour where Dno = @inID)
	return @data
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageEmployeeSchedule]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployeeSchedule]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	µÒÃÒ§àÇÅÒ¾¹Ñ¡§Ò¹ (àÇÅÒà¢éÒ,ÍÍ¡§Ò¹)
-- =============================================
CREATE PROCEDURE [dbo].[manageEmployeeSchedule]
(
	@inESno varchar(4),
	@inKeyUser varchar(4),
	@inKeyDate datetime,--àÇÅÒ·Õè¨Í§
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''--Insert
		update EmployeeSchedule set 
						ESno = @inESno,
						EStartTime = GetDate(),
						keyUser = @inkeyUser,
						LastEdit = GetDate()
		where ESno = @inESno 
		and convert(varchar(30),KeyDate,101) = convert(varchar(30),@inKeyDate,101)
	else if @inFlag = ''1''--Update
		update EmployeeSchedule set 
						ESno = @inESno,
						ESEndTime = GetDate(),
						keyUser = @inkeyUser,
						LastEdit = GetDate()
		where ESno = @inESno 
		and convert(varchar(30),KeyDate,101) = convert(varchar(30),@inKeyDate,101)
	else if @inFlag = ''2''--Delete
		delete from EmployeeSchedule where ESno = @inESno 
		and convert(varchar(30),KeyDate,101) = convert(varchar(30),@inKeyDate,101)
	else if @inFlag = ''3''--áÊ´§µÒÃÒ§àÇÅÒà¢éÒ§Ò¹¢Í§¾¹Ñ¡§Ò¹·Ñé§ËÁ´
		select t1.ESno,t1.EStartTime,t1.ESEndTime 
		from EmployeeSchedule t1,employee t2
		where t1.ESno = t2.Eno
		and t2.MStatus = ''A''
		and convert(varchar(30),t1.KeyDate,101) = convert(varchar(30),@inKeyDate,101)
		order by Convert(int,t2.Eno)
	else if @inFlag = ''4''--µÃÇ¨ÊÍºÇèÒÁÕ¡ÒÃÅ§àÇÅÒà¢éÒ§Ò¹ÂÑ§
		select count(ESno) icount from EmployeeSchedule 
		where ESno =@inESno 
		and EStartTime is not null
		and convert(varchar(30),KeyDate,101) = convert(varchar(30),@inKeyDate,101)
	else if @inflag = ''5''--áÊ´§àÇÅÒ·Ó§Ò¹¢Í§¾¹Ñ¡§Ò¹¹Ç´áµèÅÐ¤¹
		select 
				RtherapistID,
				RTime as RTimeBegin,
				Ddetail as RTimeEnd,
				(case when dbo.CheckISPay(Rid) = 0 then Rtype else 99 end) Rtype
		from	customertransaction t1, 
				mhour t2,
				package t3
		where	t3.Pno = dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage) 
				and t3.Phour = t2.Dno 	
				and Rtype not in (''3'',''4'') -- 1 ¨Í§ , 2 ãªéºÃÔ¡ÒÃ , 3 Â¡àÅÔ¡
				and RtherapistID = @inESno
				and convert(varchar(30),Rdate,101) = convert(varchar(30),@inKeyDate,101)
	else if @inFlag = ''6''--ºÑ¹·Ö¡¢éÍÁÙÅ
		begin
			declare @icount int
			set @icount = (select count(ESno) icount 
			from employeeschedule 
			where ESno = @inESno 
			and convert(varchar(30),keydate,101) = convert(varchar(30),@inKeyDate,101))
			if @icount = 0
				insert into EmployeeSchedule(ESno,EStartTime,ESEndTime,KeyDate,KeyUser,lastEdit) 
				values(@inESno,null,null,@inKeyDate,@inKeyUser,GetDate())
		end
END

























' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetRoomSchedule]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRoomSchedule]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description: Get Room Data to show in Schedule
-- =============================================
CREATE PROCEDURE [dbo].[GetRoomSchedule]
(
	@InRoom varchar(4),
	@inKeyDate datetime,
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''--´Ö§¢éÍÁÙÅËéÍ§·Ñé§ËÁ´
		select Rno from Room
	else if @inFlag = ''1''
		begin
			select Rroom,
					RTime RTimeBegin,
					Ddetail RTimeEnd,
					(case when dbo.CheckISPay(Rid) = 0 then Rtype else 99 end) Rtype
			from	customertransaction t1,
					mhour t2,
					package t3
			where	t3.Pno = dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)
					and t3.Phour = t2.Dno 
					and Rroom = @InRoom
					and Rtype not in (''3'',''4'') -- 1 ¨Í§ , 2 ãªéºÃÔ¡ÒÃ , 3 Â¡àÅÔ¡
					and convert(varchar(30),Rdate,101) = convert(varchar(30),@inKeyDate,101)
		end
END


' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMAccountCode]    Script Date: 06/02/2009 13:02:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMAccountCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetMAccountCode]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select DDetail from MAccountCode where Dno = @inID)
	return @data
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMAccountCode]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMAccountCode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageMAccountCode]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY

	if @inFlag = ''0''--Insert
		insert into MAccountCode(Dno,Ddetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)

	else if @inFlag = ''1''--Update
		update MAccountCode set 
						Ddetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where Dno = @inNo
	else if @inFlag = ''2''--Delete
		delete from MAccountCode where Dno = @inNo
	else if @inFlag = ''3''--Select
		select * from MAccountCode order by dno
	else if @inFlag = ''4''--Select
		select * from MAccountCode where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MAccountCode where Ddetail like ''%''+@inDetail+''%'' order by dno
' 
END
GO
/****** Object:  StoredProcedure [dbo].[getCustomerUseDate]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getCustomerUseDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	getCustomerUseDate
-- =============================================
CREATE PROCEDURE [dbo].[getCustomerUseDate]
(
	@inID varchar(50),
	@intype varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''1''
		begin
			--by id
			select RmemberID,
			Mname,
			Msurname,
			RTherapistID,
			isnull(dbo.GetEmployeeName(RTherapistID),''-'') RTherapistName,
			RTherapistRevID,
			isnull(dbo.GetEmployeeName(RTherapistRevID),''-'') RTherapistRevName,
			RDate,
			RTime 
			from customertransaction t1,customerpayment t2,customer t3
			where t1.Rid = t2.SCusTranID
			and t1.RmemberID = t3.Mno
			and RmemberID = @inID
			and mtype = @intype
			order by Rdate desc,Rtime desc
		end
	else if @inFlag = ''2''
		begin
			--by name
			select RmemberID,
			Mname,
			Msurname,
			RTherapistID,
			isnull(dbo.GetEmployeeName(RTherapistID),''-'') RTherapistName,
			RTherapistRevID,
			isnull(dbo.GetEmployeeName(RTherapistRevID),''-'') RTherapistRevName,
			RDate,
			RTime 
			from customertransaction t1,customerpayment t2,customer t3
			where t1.Rid = t2.SCusTranID
			and t1.RmemberID = t3.Mno
			and Mname like ''%''+@inID+''%''
			and mtype = @intype
			order by Rdate desc,Rtime desc
		end
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetRunning]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRunning]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Book/Running
-- =============================================
CREATE PROCEDURE [dbo].[GetRunning]

AS
BEGIN
	declare @BookNumber int
	declare @BookRunning int
	set @BookNumber = (select (isnull(max(BNUM),0)+1) BNUM from BookInvoice)
	set @Bookrunning = (select (isnull(max(BRUNNING),0)+1) BRUNNING from BookInvoice where BNUM = @BookNumber-1)

	--Check Running
	if @BookRunning > 999 
		begin
			--Insert 
			insert into BookInvoice(BNUM,BRUNNING)
			values(@BookNumber,''1'')
		end
	else
		begin
			update BookInvoice set
			BRUNNING = @Bookrunning
			where BNUM = @BookNumber-1
		end
	--Query TOP Data
	SELECT TOP 1 * FROM BookInvoice ORDER BY BNUM desc
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteTransactions]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteTransactions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Delete Transaction
-- =============================================
CREATE PROCEDURE [dbo].[DeleteTransactions]
(
	@inPeriod varchar(10),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''1''-- select Period
		begin
			select 
			distinct((Convert(varchar(2),case when datepart(day,keydate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,keydate))+''-''+Convert(varchar(4),datepart(year,keydate)))) period
			from EmployeeSummary
			where SFLAG is null
		end
	else if @inFlag = ''2''--Delete Transaction
		begin
			--Delete Transaction
			delete CustomerTransaction
			where RCustomerType = ''0001''
			and (Convert(varchar(2),case when datepart(day,RDate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,RDate))+''-''+Convert(varchar(4),datepart(year,RDate))) = @inPeriod
			--Delete Transaction payment
			delete Customerpayment
			where (Convert(varchar(2),case when datepart(day,SKeyDate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,SKeyDate))+''-''+Convert(varchar(4),datepart(year,SKeyDate))) = @inPeriod
			--Update EmployeeSummary Flag
			update EmployeeSummary set SFLAG = @inPeriod
			where (Convert(varchar(2),case when datepart(day,keydate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,keydate))+''-''+Convert(varchar(4),datepart(year,keydate))) = @inPeriod
		end
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageTotal]    Script Date: 06/02/2009 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageTotal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetPackageTotal]
(
	@inPno varchar(10)
)
RETURNS int
AS
BEGIN
	declare @icount int
	set @icount = (select Ppercourse from package where Pno = @inPno)
	return @icount
END


' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageType]    Script Date: 06/02/2009 13:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	Get Package Type
-- =============================================
CREATE FUNCTION [dbo].[GetPackageType]
(
	@inPno varchar(10)
)
RETURNS varchar(4)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @outType varchar(4)

	set @outType = (select PMassageType from package where Pno = @inPno)

	-- Return the result of the function
	RETURN @outType

END


' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageHour]    Script Date: 06/02/2009 13:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageHour]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetPackageHour]
(
	@inID varchar(10)
)
RETURNS varchar(4)
AS
BEGIN
	declare @data varchar(4)
	set @data = (select Phour from package where Pno =@inID)
	return @data
END



' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageName]    Script Date: 06/02/2009 13:02:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageName]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetPackageName]
(
	@inID varchar(10)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select Pdetail from package where Pno =@inID)
	return @data
END





' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPackageCost]    Script Date: 06/02/2009 13:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPackageCost]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetPackageCost]
(
	@inPno varchar(4)
)
RETURNS float
AS
BEGIN
	declare @PackageCost float
	set @PackageCost =(select Pprice from package where Pno = @inPno)
	return @PackageCost
END





' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetTherapistCost]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTherapistCost]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetTherapistCost]
(
	@inPno varchar(10)
)
RETURNS float
AS
BEGIN
	declare @PpriceforTherapist float
	set @PpriceforTherapist =(select PpriceforTherapist
				from package 
				where Pno=@inPno)
	return @PpriceforTherapist
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[PrintMemberInvoice]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrintMemberInvoice]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	¾ÔÁ¾ìãºàÊÃç¨¤èÒÊÁÑ¤ÃÊÁÒªÔ¡ÂéÍ¹ËÅÑ§
-- =============================================
CREATE PROCEDURE [dbo].[PrintMemberInvoice]
	@inMno varchar(5)
AS
BEGIN
	select 
	t1.MID,
	t4.Mno,
	t4.Mname,
	t4.Msurname,
	t4.McontactAddress,
	t3.Pdetail,
	t3.Pprice
	from CustomerMemberPackage t1,MemberPayment t2,Package t3,Customer t4
	where t1.MID = t2.TPackageTranID
	and t1.MPackage = Pno
	and t1.MCusNo = t4.Mno
	and t3.PCustomerType = t4.Mtype
	and t4.Mno = @inMno
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMMassageType]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMMassageType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create PROCEDURE [dbo].[manageMMassageType]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY

	if @inFlag = ''0''--Insert
		insert into MMassageType(Dno,DDetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MMassageType set 
						DDetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where Dno = @inNo
	else if @inFlag = ''2''--Delete
		delete from MMassageType where Dno = @inNo
	else if @inFlag = ''3''--Select
		select * from MMassageType order by dno
	else if @inFlag = ''4''--Select
		select * from MMassageType where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MMassageType where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 








' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMassageType]    Script Date: 06/02/2009 13:02:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMassageType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMassageType]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select Ddetail from MMassageType where Dno = @inID)
	return @data
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMBank]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMBank]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[manageMBank] (
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert

		insert into MBank(DNo,DDetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MBank set 
						DDetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MBank where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MBank order by dno
	else if @inFlag = ''4''--Select
		select * from MBank where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MBank where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 













' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMCustomerType]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMCustomerType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[manageMCustomerType](
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY

	if @inFlag = ''0''--Insert
		insert into MCustomerType(DNo,DDetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MCustomerType set 
						DDetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MCustomerType where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MCustomerType order by dno
	else if @inFlag = ''4''--Select
		select * from MCustomerType where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MCustomerType where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 












' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMCustomerType]    Script Date: 06/02/2009 13:02:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMCustomerType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMCustomerType]
(
	@inID varchar(4)
)
RETURNS varchar(100)
AS
BEGIN
	declare @data varchar(100)
	set @data = (select DDetail from MCustomerType where Dno = @inID)
	return @data
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMNation]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMNation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[manageMNation]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY

	if @inFlag = ''0''--Insert
		insert into MNation(Dno,Ddetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)

	else if @inFlag = ''1''--Update
		update MNation set 
						Ddetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where Dno = @inNo
	else if @inFlag = ''2''--Delete
		delete from MNation where Dno = @inNo
	else if @inFlag = ''3''--Select
		select * from MNation order by dno
	else if @inFlag = ''4''--Select
		select * from MNation where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MNation where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 






' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMNation]    Script Date: 06/02/2009 13:02:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMNation]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetMNation]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select Ddetail from MNation where Dno = @inID)
	return @data
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMPaymentType]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMPaymentType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageMPaymentType]
(
	@inNo varchar(25),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN TRY

	if @inFlag = ''0''--Insert
		insert into MPaymentType(DNo,Ddetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MPaymentType set 
						Ddetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MPaymentType where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MPaymentType order by dno
	else if @inFlag = ''4''--Select
		select * from MPaymentType where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MPaymentType where Ddetail like ''%''+@inDetail+''%'' order by dno
     END TRY
     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
set @msg  = ''N,''+ERROR_MESSAGE()
		print @msg
     END CATCH
return 






' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMPaymentType]    Script Date: 06/02/2009 13:02:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMPaymentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetMPaymentType]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select Ddetail from MPaymentType where Dno = @inID)
	return @data
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageRoom]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageRoom]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[manageRoom]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@intype varchar(4),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert

		insert into room(Rno,Rdetail,MmassageType,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,@intype,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update room set 
						Rdetail = @inDetail,
						MmassageType = @intype,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where RNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from room where RNo = @inNo
	else if @inFlag = ''3''--Select
		select * from room order by RNo
	else if @inFlag = ''4''--Select
		select * from room where RNo = @inNo order by Rno
	else if @inFlag = ''5''--Select
		select * from room where Rdetail like ''%''+@inDetail+''%'' order by RNo
	else if @inFlag = ''6''--Select
		select * from room where MmassageType = @intype order by RNo
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 


' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetPermission]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPermission]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	GetPermission
-- =============================================
CREATE PROCEDURE [dbo].[GetPermission]
(
	@inPUID varchar(10),--Emp ID
	@inPFRM varchar(10),--frm ID
	@inPINSERT bit,
	@inPDELETE bit,
	@inPUPDATE bit,
	@inPVIEW bit,
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''--select initial
		select 
			FID,
			PUID,
			FNAME,
			FDESC,
			isnull(PINSERT,0) PINSERT,
			isnull(PDELETE,0) PDELETE,
			isnull(PUPDATE,0) PUPDATE,
			isnull(PVIEW ,0) PVIEW
		from frmMaster left join EmpPermission
		on frmMaster.FID = EmpPermission.PUID
	if @inFlag = ''1''--select by id
		begin
			declare @pid int
			set @pid = (select count(PUID) from EmpPermission where puid = @inPUID)
			if @pid > 0 
				begin
					select t2.FID,t2.FNAME,t2.FDESC,t1.PUID,t1.PFRM,t1.PINSERT,t1.PDELETE,t1.PUPDATE,t1.PVIEW 
					from EmpPermission t1,frmMaster t2
					where t1.PFRM = t2.FID 
					and t1.puid = @inPUID
				end
			else ----- áººäÁèÁÕ¢éÍÁÙÅ (initial)
				begin
					select
						FID, 
						PUID,
						FNAME,
						FDESC,
						isnull(PINSERT,0) PINSERT,
						isnull(PDELETE,0) PDELETE,
						isnull(PUPDATE,0) PUPDATE,
						isnull(PVIEW ,0) PVIEW
					from frmMaster left join EmpPermission
					on frmMaster.FID = EmpPermission.PUID
				end
		end
	if @inFlag = ''2''--Insert,Update
		begin
			insert into EmpPermission(PUID,PFRM,PINSERT,PDELETE,PUPDATE,PVIEW)
			values(@inPUID,@inPFRM,@inPINSERT,@inPDELETE,@inPUPDATE,@inPVIEW)
		end
	if @inFlag = ''3''--Update 
		begin
			update EmpPermission set
					PINSERT = @inPINSERT,
					PDELETE = @inPDELETE,
					PUPDATE = @inPUPDATE,
					PVIEW = @inPVIEW
			where	PUID = @inPUID and PFRM = @inPFRM
		end
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMtitle]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMtitle]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[manageMtitle]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY

	if @inFlag = ''0''--Insert
		insert into Mtitle(Dno,Ddetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update Mtitle set 
						Ddetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where Dno = @inNo
	else if @inFlag = ''2''--Delete
		delete from Mtitle where Dno = @inNo
	else if @inFlag = ''3''--Select
		select * from Mtitle order by dno
	else if @inFlag = ''4''--Select
		select * from Mtitle where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from Mtitle where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 






' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMTypeEmployee]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMTypeEmployee]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[manageMTypeEmployee]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert
		insert into MTypeEmployee(Dno,Ddetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MTypeEmployee set 
						Ddetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where Dno = @inNo
	else if @inFlag = ''2''--Delete
		delete from MTypeEmployee where Dno = @inNo
	else if @inFlag = ''3''--Select
		select * from MTypeEmployee order by dno
	else if @inFlag = ''4''--Select
		select * from MTypeEmployee where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MTypeEmployee where Ddetail like ''%''+@inDetail+''%'' order by dno
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 






' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMUser]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	user management
-- =============================================
CREATE PROCEDURE [dbo].[manageMUser](
	@inUREFID varchar(25),
	@inUPASS varchar(8),
	@inUTYPE varchar(4),
	@inUAUTHORIZE varchar(100),
	@inKEYUSER varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''
		insert into MUSER(UREFID,UPASS,UTYPE,UAUTHORIZE,KEYDATE,KEYUSER,LASTTIME)
		values(@inUREFID,@inUPASS,@inUTYPE,@inUAUTHORIZE,GetDate(),@inKEYUSER,GetDate())
	else if @inFlag = ''1''
		update MUSER 
		set UPASS =@inUPASS,
			UTYPE =@inUTYPE,
			UAUTHORIZE =@inUAUTHORIZE,
			KEYUSER =@inKEYUSER,
			LASTTIME = GetDate()
		where UREFID =@inUREFID
	else if @inFlag =''3''
		delete from MUSER where UREFID = @inUREFID
	else if @inFlag = ''4''
		select t2.Eno,t2.Ename,t2.Esurname,t3.DDetail,t1.UAUTHORIZE
		from muser t1,Employee t2,MTypeEmployee t3
		where t1.urefid = t2.Eno
		and t1.utype = t3.Dno
		and t1.utype in (''0001'',''0002'')
		and t2.MStatus = ''A''
		and t1.urefid = @inUREFID
		and t1.UPASS = @inUPASS
	else if @inFlag = ''5''--update lastlogin
		update MUSER 
		set LASTTIME = GetDate()
		where UREFID =@inUREFID
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageEmployee]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployee]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[manageEmployee]
(
	@inEno varchar(10),
	@inEtype varchar(4),
	@inEsex varchar(1),
	@inEtitle varchar(4),
	@inEname varchar(100),
	@inEsurname varchar(100),
	@inEnickname varchar(100),
	@inEcontactAddress varchar(100),
	@inEmobile varchar(200),
	@inEemail varchar(10),
	@inMStatus varchar(1),--Ê¶Ò¹Ð¡ÒÃ·Ó§Ò¹
	@inMperiod varchar(4),--¡Ð§Ò¹
	@inMkeyUser varchar(4),
	@inbiz	float,
	@inHoliday float,
	@inSick float,
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert
		insert into Employee(Eno,Etype,Esex,Etitle,Ename,Esurname,Enickname,EcontactAddress,Emobile,Eemail,Mstatus,Mperiod,MkeyDate,MLastEdit,MkeyUser,LeaveBiz,LeaveHoliday,LeaveSick) 
		values(@inEno,@inEtype,@inEsex,@inEtitle,@inEname,@inEsurname,@inEnickname,@inEcontactAddress,@inEmobile,@inEemail,@inMstatus,@inMperiod,GetDate(),GetDate(),@inMkeyUser,@inbiz,@inHoliday,@inSick)
	else if @inFlag = ''1''--Update
		update Employee set 
					Eno = @inEno,
					Etype = @inEtype,
					Esex = @inEsex,
					Etitle = @inEtitle,
					Ename = @inEname,
					Esurname = @inEsurname,
					Enickname = @inEnickname,
					EcontactAddress = @inEcontactAddress,
					Emobile = @inEmobile,
					Eemail = @inEemail,
					Mstatus = @inMstatus,
					Mperiod = @inMperiod,
					MLastEdit = GetDate(),
					MkeyUser = @inMkeyUser,
					LeaveBiz = @inbiz,
					LeaveHoliday = @inHoliday,
					LeaveSick = @inSick
		where Eno = @inEno
	else if @inFlag = ''2''--Delete
		delete from Employee where Eno = @inEno
	else if @inFlag = ''3''--Select
		select * from Employee where Mstatus =''A'' order by Convert(int,Eno)
	else if @inFlag = ''4''--Select by id
		select * from Employee where Eno = @inEno and Mstatus =''A'' order by Convert(int,Eno)
	else if @inFlag = ''5''--Select
		select * from Employee where Ename like ''%''+@inEname+''%'' and Mstatus =''A'' order by Convert(int,Eno)
	else if @inFlag = ''6''-- filter by employee group
		select * from Employee where etype = @inEtype and Mstatus =''A'' order by Convert(int,Eno)
	else if @inFlag = ''7''-- filter by not therapist
		select * from Employee where etype <> @inEtype and Mstatus =''A'' order by Convert(int,Eno)
	else if @inFlag = ''8''--Select
		select * from Employee where Eno = @inEno and etype = @inEtype and Mstatus =''A'' order by Convert(int,Eno)
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 


























' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageCustomerTransaction]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageCustomerTransaction]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Manage Customer Transaction
-- =============================================
CREATE PROCEDURE [dbo].[manageCustomerTransaction]
(
	@inRID	int,
	@inRmemberID  varchar(25),
	@inRTherapistID  varchar(4),
	@inRTherapistRevID varchar(4),
	@inRPackage varchar(10),
	@inRDate datetime,
	@inRTime datetime,
	@inRRoom  varchar(4),
	@inRCustomerType  varchar(4),
	--Customer Detail--
	@inName varchar(100),
	@inSurname varchar(100),
	@inSex varchar(4),
	@inAge int,
	@inNation varchar(4),
	@inMobile varchar(20),
	@inAddress varchar(100),
	--End Customer Detail
	@inScent varchar(4),
	@inScentCount int,
	@inkeyUser varchar(4),
	@inStatus varchar(1),
	@inRemark varchar(100),
	@inRSpecialPackage varchar(10),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''--Insert
		begin
		--Transaction
		insert into CustomerTransaction(RmemberID,RTherapistID,RDate,RTime,RRoom,RCustomerType,KeyDate,KeyUser,LastEdit,Rtype,Remark,RScent,RScentCount,RTherapistRevID,RPackage,RSpecialPackage)
		values(@inRmemberID,@inRTherapistID,@inRDate,@inRTime,@inRRoom,@inRCustomerType,GetDate(),@inkeyUser,GetDate(),@inStatus,@inRemark,@inScent,@inScentCount,@inRTherapistRevID,@inRPackage,@inRSpecialPackage)
		--Customer
		if @inRCustomerType = ''0001''--¶éÒà»ç¹ÅÙ¡¤éÒ·ÑèÇä»¨ÐºÑ¹·Ö¡¢éÍÁÙÅÅÙ¡¤éÒ´éÇÂ â´Â¨ÐäÁèºÑ¹·Ö¡«éÓ
			begin
				declare @icount int
				set @icount = (select count(mno) icount from customer where mtype = ''0001'' and mno = @inRmemberID)
				if @icount = 0 
					insert into Customer(Mno,Mtype,Msex,Mname,Msurname,Mnational,MAge,McontactAddress,Mmobile,Memail,MstartDate,MendDate,Mkeydate,MLastEdit,MkeyUser) 
					values(@inRmemberID,@inRCustomerType,@inSex,@inName,@inSurname,@inNation,@inAge,@inAddress,@inMobile,''-'',GetDate(),DATEADD(YEAR, 1, getdate()),GetDate(),GetDate(),@inkeyUser)
			end
		end
	else if @inFlag = ''1''--(Â¡àÅÔ¡ÃÒÂ¡ÒÃáÅéÇà»ÅÕèÂ¹ÃÒÂ¡ÒÃãËÁè ¾ÃéÍÁÃÐºØàËµØ¼Å áÅÐÊÃéÒ§ record ãËÁè
		begin
			--»ÃÑº»ÃØ§ÃÒÂ¡ÒÃà¡èÒãËéà»ç¹Â¡àÅÔ¡ÃÒÂ¡ÒÃ
			update customerTransaction set 
			KeyUser = @inkeyUser,
			LastEdit =GetDate(),
			Rtype = ''4'',--Â¡àÅÔ¡ÃÒÂ¡ÒÃ
			Remark = @inRemark
			where Rid = @inRID 
			and convert(varchar(30),Rdate,101) = convert(varchar(30),@inRDate,101)

			--ÊÃéÒ§ Customer Transaction ãËÁè
			insert into CustomerTransaction(RmemberID,RTherapistID,RDate,RTime,RRoom,RCustomerType,KeyDate,KeyUser,LastEdit,Rtype,Remark,RScent,RScentCount,RTherapistRevID,RPackage,RSpecialPackage)
			values(@inRmemberID,@inRTherapistID,@inRDate,@inRTime,@inRRoom,@inRCustomerType,GetDate(),@inkeyUser,GetDate(),''2'','''',@inScent,@inScentCount,@inRTherapistRevID,@inRPackage,@inRSpecialPackage)
		end
	else if @inFlag = ''2''--Â¡àÅÔ¡ÃÒÂ¡ÒÃ
		update customerTransaction set 
		KeyUser = @inkeyUser,
		LastEdit =GetDate(),
		Rtype = @inStatus,
		Remark =@inRemark
		where Rid = @inRID 
		and convert(varchar(30),Rdate,101) = convert(varchar(30),@inRDate,101)
	else if @inFlag = ''3''--áÊ´§¢éÍÁÙÅ transaction ·Ñé§ËÁ´
		select  RId,RmemberID,
						RTherapistID,
						RTherapistRevID,
						RPackage RemarkPackageID,
						dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)  RPackage,
						RDate,
						RTime,
						RRoom,
						RCustomerType,
						Mname,
						Msurname,
						Mmobile,
						Rtype,
						MSex,
						Mnational,
						MAge,
						McontactAddress,
						RSpecialPackage,
						dbo.CheckISPay(t1.Rid) isPlay
		from CustomerTransaction t1,Customer t2
		where t1.RmemberID = t2.Mno
		and t1.RCustomerType = t2.Mtype
		and t1.Rtype not in (''3'',''4'')
--		and dbo.CheckISPay(t1.Rid) < 1 -- ¶éÒÁÒ¡¡ÇèÒ 0 ¤×Í ªÓÃÐà§Ô¹áÅéÇ
		and convert(varchar(30),t1.Rdate,101) = convert(varchar(30),@inRDate,101)
		order by t1.Rdate
	else if @inFlag = ''4''--áÊ´§¢éÍÁÙÅ transaction µÒÁÃËÑÊÅÙ¡¤éÒ
		select  RId,RmemberID,
				RTherapistID,
				RTherapistRevID,
				RPackage,
				RDate,
				RTime,
				RRoom,
				RCustomerType,
				Mname,
				Msurname,
				Mmobile,
				Rtype,
				MSex,
				Mnational,
				MAge,
				McontactAddress,
				RSpecialPackage,
				dbo.CheckISPay(t1.Rid) isPlay
		from CustomerTransaction t1,Customer t2
		where t1.RmemberID = t2.Mno
		and t1.RCustomerType = t2.Mtype
		and t1.RmemberID =@inRmemberID
		and convert(varchar(30),t1.Rdate,101) = convert(varchar(30),@inRDate,101)
	else if @inFlag = ''5''--´Ö§¢éÍÁÙÅÁÒáÊ´§µÒÁ rid
		select  RId,RmemberID,
						RTherapistID,
						RTherapistRevID,
						RPackage RemarkPackageID,
						dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)  RPackage,
						RDate,
						RTime,
						RRoom,
						RCustomerType,
						Mname,
						Msurname,
						Mmobile,
						Rtype,
						MSex,
						Mnational,
						MAge,
						dbo.CheckISPay(t1.Rid) isPlay,
						RSpecialPackage,
						MContactAddress
		from CustomerTransaction t1,Customer t2
		where t1.RmemberID = t2.Mno
		and t1.RCustomerType = t2.Mtype
		and t1.Rtype not in (''3'',''4'')
		and t1.RId =@inRID
		and convert(varchar(30),t1.Rdate,101) = convert(varchar(30),@inRDate,101)
END


































' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageCustomer]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageCustomer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[manageCustomer]
(
	@inMno varchar(25),
	@inMtype varchar(4),
	@inMsex varchar(4),
	@inMname varchar(100),
	@inMsurname varchar(100),
	@inMnational varchar(4),
	@inAge int,
	@inMcontactAddress varchar(100),
	@inMmobile varchar(10),
	@inMemail varchar(50),
	@inMstartDate datetime,
	@inMendDate datetime,
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
--BEGIN TRY
	if @inFlag = ''0''--Insert
		insert into Customer(Mno,Mtype,Msex,Mname,Msurname,Mnational,MAge,McontactAddress,Mmobile,Memail,MstartDate,MendDate,Mkeydate,MLastEdit,MkeyUser) 
		values(@inMno,@inMtype,@inMsex,@inMname,@inMsurname,@inMnational,@inAge,@inMcontactAddress,@inMmobile,@inMemail,@inMstartDate,@inMendDate,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update Customer set 
						Mno = @inMno,
						Mtype = @inMtype,
						Msex = @inMsex,
						Mname = @inMname,
						Msurname = @inMsurname,
						Mnational = @inMnational,
						MAge = @inAge,
						McontactAddress = @inMcontactAddress,
						Mmobile = @inMmobile,
						Memail = @inMemail,
						MstartDate = @inMstartDate,
						MendDate = @inMendDate,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where Mno = @inMno
	else if @inFlag = ''2''--Delete
		delete from Customer where Mno = @inMno
	else if @inFlag = ''3''--Select
		select * from Customer order by Mno
	else if @inFlag = ''4''--Select
		select * from Customer where Mno = @inMno order by Mno
--	else if @inFlag = ''5''--Select
--		select * from room where Rdetail like ''%''+@inDetail+''%'' order by RNo
	else if @inFlag = ''6''--Select
		select * from Customer where Mtype = @inMtype order by Mno
	else if @inFlag = ''7''
		select * from Customer where Mno = @inMno and MType = @inMtype
	else if @inFlag = ''8''--filter µÒÁ»ÃÐàÀ· package
		select 
			t1.MID,t1.Mno,t1.MType,t1.Msex,t1.Mname,t1.McontactAddress,t1.memail,t1.Msurname,t1.Mnational,t1.MAge,
			t1.McontactAddress,t1.Mmobile,t1.MstartDate,t1.MendDate,t1.MkeyDate,t1.MLastEdit,t1.MkeyUser
		from customer t1,CustomerMemberpackage t2,memberpayment t3
		where t1.Mno = t2.McusNo
		and t2.MID = t3.TPackageTranID
		and t1.Mtype = ''0002''
		and t2.MPackage = @inMmobile
--     END TRY
--     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
--set @msg  = ''N,''+ERROR_MESSAGE()
--		print @msg
--     END CATCH
return 
















' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMemberPayment]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMemberPayment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[manageMemberPayment]
(
	@inMno varchar(25),
	@inMtype varchar(4),
	@inMsex varchar(4),
	@inMname varchar(100),
	@inMsurname varchar(100),
	@inMnational varchar(4),
	@inAge int,
	@inMcontactAddress varchar(100),
	@inMmobile varchar(10),
	@inMemail varchar(50),
	@inMstartDate datetime,
	@inMendDate datetime,
	------------------------
	@inTTherapist varchar(4),
	@inTPackage varchar(10),
	-----------------------
	@inSACCode varchar(4),
	@inSAmount float,
	@inSDiscount float,
	@inSPaytype varchar(4),
	@inSSlip varchar(100),
	@inSBank varchar(4),
	---------------------
	@inSKeyUser varchar(4),
	@inFlag varchar(1)
)
AS
	if @inFlag = ''0''--ÊÁÑ¤ÃÊÁÒªÔ¡ãËÁè
		begin
		--Insert Customer Data
		insert into Customer(Mno,Mtype,Msex,Mname,Msurname,Mnational,MAge,McontactAddress,Mmobile,Memail,MstartDate,MendDate,Mkeydate,MLastEdit,MkeyUser) 
		values(@inMno,@inMtype,@inMsex,@inMname,@inMsurname,@inMnational,@inAge,@inMcontactAddress,@inMmobile,@inMemail,@inMstartDate,@inMendDate,GetDate(),GetDate(),@inSKeyUser)
		
		--Insert Member Package Data
		insert into CustomerMemberPackage(MCusNo,MPackage,MUseMember,KeyDate,KeyUser,LastEdit)
		values(@inMno,@inTPackage,''0'',GetDate(),@inSKeyUser,GetDate())	
		
		--Insert Member Payment Data
		declare @packageTran int
		set @packageTran =(select max(MID) from CustomerMemberPackage)
		insert into MemberPayment(TMemberID,TTherapist,TPackageTranID,SACCode,SAmount,SDiscount,SPayType,SSlip,SBank,SKeyDate,SLastEdit,SKeyUser)
		values(@inMno,@inTTherapist,@packageTran,@inSACCode,@inSAmount,@inSDiscount,@inSPayType,@inSSlip,@inSBank,GetDate(),GetDate(),@inSKeyUser)
		end
	else if @inFlag = ''1''
		begin
		--Insert Member Package Data
		insert into CustomerMemberPackage(MCusNo,MPackage,MUseMember,KeyDate,KeyUser,LastEdit)
		values(@inMno,@inTPackage,''0'',GetDate(),@inSKeyUser,GetDate())	
		--Insert Member Payment Data
		declare @packageTran1 int
		set @packageTran1 =(select max(MID) from CustomerMemberPackage)
		insert into MemberPayment(TMemberID,TTherapist,TPackageTranID,SACCode,SAmount,SDiscount,SPayType,SSlip,SBank,SKeyDate,SLastEdit,SKeyUser)
		values(@inMno,@inTTherapist,@packageTran1,@inSACCode,@inSAmount,@inSDiscount,@inSPayType,@inSSlip,@inSBank,GetDate(),GetDate(),@inSKeyUser)
		end
return 























' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMLeave]    Script Date: 06/02/2009 13:02:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMLeave]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[GetMLeave]
(
	@inID varchar(4)
)
RETURNS varchar(50)
AS
BEGIN
	declare @data varchar(50)
	set @data = (select DDetail from MLeave where Dno = @inID)
	return @data
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageMLeave]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageMLeave]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	¨Ñ´¡ÒÃ»ÃÐàÀ·¡ÒÃÅÒ
-- =============================================
CREATE PROCEDURE [dbo].[manageMLeave]
(
	@inNo varchar(4),
	@inDetail varchar(100),
	@inMkeyUser varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN
--BEGIN TRY
	if @inFlag = ''0''--Insert
		insert into MLeave(DNo,DDetail,MkeyDate,MLastEdit,MkeyUser) 
		values(@inNo,@inDetail,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update MLeave set 
						DDetail = @inDetail,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where DNo = @inNo
	else if @inFlag = ''2''--Delete
		delete from MLeave where DNo = @inNo
	else if @inFlag = ''3''--Select
		select * from MLeave order by dno
	else if @inFlag = ''4''--Select
		select * from MLeave where Dno = @inNo order by dno
	else if @inFlag = ''5''--Select
		select * from MLeave where Ddetail like ''%''+@inDetail+''%'' order by dno
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetReport]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	All Report.
-- =============================================
CREATE PROCEDURE [dbo].[GetReport]
	@inStartDate datetime,
	@inEndDate datetime,
	@inReportType varchar(2)
AS
BEGIN
	if @inReportType = ''1''--ÊÃØ»ÅÙ¡¤éÒ
		begin	
			select					
					Mno,
					dbo.GetMSex(MSex) Sex,	
					Mname,
					Msurname,
					dbo.GetPackageType(dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)) MassageType,
					RTime,--µÑé§áµèàÇÅÒ
					dbo.GetMhour(dbo.GetPackageHour(dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage))) Hour,
					dbo.GetPackageCost(dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)) PackageCost,
					t3.SAmount,--ÃÒ¤Ò
					dbo.GetMPaymentType(SPayType) PayType,--»ÃÐàÀ·¡ÒÃªÓÃÐ
					dbo.GetMCustomerType(MType) MType,--»ÃÐàÀ·¡ÒÃ¨èÒÂà§Ô¹
					RTherapistID,
					dbo.GetEmployeeData(RTherapistID) Therapist,
					dbo.GetTherapistCost(dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)) TherapistCost
			from 
				CustomerTransaction t1,
				Customer t2,
				customerPayment t3
			where t1.RmemberID = t2.Mno
				and t1.RCustomerType = t2.Mtype
				and t1.Rtype not in (''3'',''4'')
				and t1.rid = t3.SCusTranID
				and (convert(varchar(30),RDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
		end
	else if @inReportType = ''2''--ÊÃØ»¤èÒÁ×Í¾¹Ñ¡§Ò¹ÃÒÂÇÑ¹
		begin	
				select	t2.Enickname,
					dbo.GetMPeriod(t2.MPeriod) Period,
					t3.EstartTime,
					t3.ESEndTime,	
					t1.GuestReserv,
					t1.Income,
					t1.SoldMember,
					t1.Find,
					t1.Remain
				from EmployeeSummary t1,Employee t2,EmployeeSchedule t3
				where t1.TherapistID = t2.Eno
				and t1.TherapistID = t3.ESno
				and t2.Eno = t3.ESno
				and convert(varchar(30),t1.keydate,101) = convert(varchar(30),t3.keyDate,101)
				and (convert(varchar(30),t1.keydate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
		end
	else if @inReportType = ''3''--ÊÃØ» STOCK
		begin	
			select	dbo.GetMScent(SCtype) SCENT_DESC,
					isnull([1],0) as REC,
					isnull([2],0) as OUT,
					(isnull([1],0)-isnull([2],0)) as REMAIN
			from(
				select	SCtype,
						SCount,
						Stype 
				from MScentTransaction

			) MSCT
			PIVOT
			(
				sum(SCount)
				For Stype In ([1],[2])
			) pvt
		end
	else if @inReportType = ''4''--ÊÃØ»¡ÒÃ¢ÒÂÊÁÒªÔ¡áÂ¡µÒÁ»ÃÐàÀ· package
		begin
			select 
			t3.mno,
			t3.mname,
			dbo.GetMSex(t3.Msex) Sex,
			dbo.GetMNation(t3.Mnational) Nation,
			t1.RDate as UseDate,--ÇÑ¹·ÕèãªéºÃÔ¡ÒÃ
			t4.SKeyDate as SoldDate,--ÇÑ¹·Õè¢ÒÂ Package
			isnull(dbo.GetEmployeeName(t4.TMemberID),''-'') TTherapist,
			t5.MPackage
			from CustomerTransaction t1,
			customerpayment t2,
			customer t3,
			memberpayment t4,
			CustomerMemberPackage t5
			where t1.Rid = SCusTranID
			and t1.RmemberID = t3.Mno
			and t1.RCustomerType = t3.MType
			and t1.RmemberID = t4.TMemberID
			and t4.TPackageTranID = t5.MID
			and t3.MType = ''0002''
			and (convert(varchar(30),t4.SKeyDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
			order by t1.rmemberid
		end
	else if @inReportType = ''5''	--ÊÃØ»¡ÒÃ¨Í§ËÁÍ¹Ç´áÂ¡µÒÁà´×Í¹
		begin
			select	
					dbo.GetEmployeeName(RTherapistID) EmpName,	
					it1.SId,
					it2.RDate
			from	
					customerpayment it1,
					customertransaction it2
			where	it1.SCusTranID = it2.Rid
			and (convert(varchar(30),it2.RDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
		end
	else if  @inReportType = ''6''--ÃÒÂ§Ò¹¨Ó¹Ç¹àª×éÍªÒµÔ¹ÕéàÅ×Í¡ Package ¡ÒÃ¹Ç´áººäË¹ áÂ¡µÒÁà¾È
		begin	
			select 
			dbo.GetPackageName(MPackage) MPackage,
			dbo.GetMNation(Mnational) Mnational,
			t1.MID,
			dbo.GetMSex(MSex) MSex
			from Customer t1,
			customermemberpackage t2
			where t1.mtype = ''0002''
			and t1.Mno = t2.MCusNo
		end
	else if  @inReportType = ''7''--ÃÒÂ§Ò¹ªèÇ§ÍÒÂØ¢Í§ÊÁÒªÔ¡ áÂ¡µÒÁªÒÂ ­.
		begin	
			(--20-30
			select dbo.GetAgePeriod(1) PERIOD,
				dbo.GetMSex(MSex) MSex,MID
			from Customer
			where MAge <30
			) 
			union all
			(--30-40
			select dbo.GetAgePeriod(2) PERIOD,
				dbo.GetMSex(MSex) MSex,MID
			from Customer
			where MAge >30 and MAge <40
			)
			union all
			(--40-50
			select dbo.GetAgePeriod(3) PERIOD,
				dbo.GetMSex(MSex) MSex,MID
			from Customer
			where MAge >40 and MAge <50
			)
			union all
			(--50-60
			select dbo.GetAgePeriod(4) PERIOD,
				dbo.GetMSex(MSex) MSex,MID
			from Customer
			where MAge >50 and MAge <60
			)
		end
	else if @inReportType = ''8''--ÃÒÂ§Ò¹Ê¶Ò¹ÐÊÁÒªÔ¡(Expired	Extent	Not Extent)
		begin
			(
			select 1,
			count(t1.MCusNo) icount
			from customerMemberPackage t1,package t2
			where t1.MPackage = Pno
			and t2.pcustomertype = ''0002''--áÊ´§à©¾ÒÐ package ¢Í§ÊÁÒªÔ¡
			and t1.MuseMember = t2.Ppercourse
			)--ÊÃØ»¨Ó¹Ç¹ÅÙ¡¤éÒ·ÕèËÁ´ÍÒÂØ .... ¤¹
			union all
			(
			select 2,
			count(t1.MCusNo) icount
			from customerMemberPackage t1,package t2
			where t1.MPackage = Pno
			and t2.pcustomertype = ''0002''--áÊ´§à©¾ÒÐ package ¢Í§ÊÁÒªÔ¡
			and t1.MuseMember < t2.Ppercourse
			and exists (
			--áÊ´§ÊÁÒªÔ¡·ÕèÁÕ¨Ó¹Ç¹ package ÁÒ¡ÇèÒ 1 
			select
					count(it1.MCusNo) icount
			from	customerMemberPackage it1,package it2
			where	it1.MPackage = Pno
					and it2.pcustomertype = ''0002''--áÊ´§à©¾ÒÐ package ¢Í§ÊÁÒªÔ¡
					and it1.MCusNo = t1.MCusNo
			group by MCusNo
			having count(it1.MCusNo) > 1
			)
			)--ÊÃØ»¨Ó¹Ç¹ÅÙ¡¤éÒ·ÕèµèÍÍÒÂØáÅéÇ .... ¤¹
			union all
			(
			select 3,
			count(t1.MCusNo) icount
			from customerMemberPackage t1,package t2
			where t1.MPackage = Pno
			and t2.pcustomertype = ''0002''--áÊ´§à©¾ÒÐ package ¢Í§ÊÁÒªÔ¡
			and t1.MuseMember = t2.Ppercourse
			)--ÊÃØ»¨Ó¹Ç¹ÅÙ¡¤éÒ·ÕèÂÑ§äÁèµèÍÍÒÂØ..... ¤¹
		end
	else if @inReportType = ''9''--ÊÃØ»»ÃÐàÀ·¡ÒÃªÓÃÐà§Ô¹
		begin	
			select
			dbo.GetMPaymentType(SpayType) SpayType,
			SAmount Amount
			from customerpayment
			where (convert(varchar(30),SkeyDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
		end
	else if @inReportType = ''10''--ÊÃØ»ÂÍ´¢ÒÂÊÁÒªÔ¡
		begin	
			select 
				dbo.GetMAccountCode(SACCode) ACcode,
				SAmount
			from Memberpayment
			where (convert(varchar(30),SKeyDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
		end
	else if @inReportType = ''11''--ÊÃØ»¡ÒÃ¨Ó¹Ç¹¤ÃÑé§ã¹¡ÒÃãªéºÃÔ¡ÒÃ ª/­. áÂ¡µÒÁ»ÃÐàÀ·ÊÁÒªÔ¡
		begin	
			select dbo.GetMSex(t3.MSex) MSex,
			dbo.GetMCustomerType(t1.RCustomerType) RCustomerType,
			t1.Rid 
			from customertransaction t1,customerpayment t2,customer t3
			where t1.Rid = t2.SCusTranID 
			and t1.RCustomerType = t3.MType
			and t1.RmemberID = t3.Mno
			and (convert(varchar(30),t1.RDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
		end
	else if @inReportType = ''12''--ÊÃØ»¡ÒÃ¨Ó¹Ç¹¤ÃÑé§ã¹¡ÒÃãªéºÃÔ¡ÒÃ ª/­. áÂ¡µÒÁ»ÃÐàÀ·ºÃÔ¡ÒÃ
		begin
			select 
			dbo.GetMSex(t3.MSex) MSex,
			dbo.GetPackageType(dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)) RMassageType,
			dbo.GetMCustomerType(RCustomerType) RCustomerType,
			t1.Rid
			from customertransaction t1,customerpayment t2,customer t3
			where t1.Rid = t2.SCusTranID 
			and t1.RCustomerType = t3.MType
			and t1.RmemberID = t3.Mno
			and (convert(varchar(30),t1.RDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
		end
	else if @inReportType = ''13''--ÊÃØ»ÂÍ´ÃÒÂà´×Í¹
		begin	
			select dbo.GetMSex(MSex) Sex,[0001],[0002],[0003],[0004],[0005],[0006],[0007],[0008],[0009],[0010],[0011] from
			(
			select	t3.MSex,
					dbo.GetPackageType(dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)) RMassageType,
					t1.Rid 
			from customertransaction t1,customerpayment t2,customer t3
			where t1.Rid = t2.SCusTranID 
			and t1.RCustomerType = t3.MType
			and t1.RmemberID = t3.Mno
			and t1.RCustomerType = ''0002''--ÊÁÒªÔ¡·ÑèÇä»
			and (convert(varchar(30),t1.RDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
			) MSCT
			pivot
			(
				count(Rid)
				for RMassageType in ([0001],[0002],[0003],[0004],[0005],[0006],[0007],[0008],[0009],[0010],[0011])
			) pvt
		end
	else if @inReportType = ''14''--ÊÃØ» ÇÑ¹ÅÒ¾¹Ñ¡§Ò¹
		begin
			select 
			Enickname,
			LeaveBiz,
			(select isnull(sum(convert(float,Lcount)),0) from EmployeeLeave where LType = ''0001'' and EmpID = Eno) countBiz,
			LeaveHoliday,
			(select isnull(sum(convert(float,Lcount)),0) from EmployeeLeave where LType = ''0002'' and EmpID = Eno) countHoliday,
			LeaveSick,
			(select isnull(sum(convert(float,Lcount)),0) from EmployeeLeave where LType = ''0003'' and EmpID = Eno) countSick
			from Employee
		end
	else if @inReporttype = ''15''-- Therapist Income
		begin
			select  dbo.GetEmployeeName(RTherapistID) RTherapist,
			isnull([1],0) D1,
			isnull([2],0) D2,
			isnull([3],0) D3,
			isnull([4],0) D4,
			isnull([5],0) D5,
			isnull([6],0) D6,
			isnull([7],0) D7,
			isnull([8],0) D8,
			isnull([9],0) D9,
			isnull([10],0) D10,
			isnull([11],0) D11,
			isnull([12],0) D12,
			isnull([13],0) D13,
			isnull([14],0) D14,
			isnull([15],0) D15,
			isnull([16],0) D16,
			isnull([17],0) D17,
			isnull([18],0) D18,
			isnull([19],0) D19,
			isnull([20],0) D20,
			isnull([21],0) D21,
			isnull([22],0) D22,
			isnull([23],0) D23,
			isnull([24],0) D24,
			isnull([25],0) D25,
			isnull([26],0) D26,
			isnull([27],0) D27,
			isnull([28],0) D28,
			isnull([29],0) D29,
			isnull([30],0) D30,
			isnull([31],0) D31
			from 
			(
			select 
					t1.RTherapistID,
					datepart(day,t1.RDate) useDate,--ÇÑ¹·ÕèãªéºÃÔ¡ÒÃ
					t2.SAmount
			from	CustomerTransaction t1,
					CustomerPayment t2,
					Employee t3
			where t1.Rid = SCusTranID
				and t1.RCustomerType = ''0001''
				and t1.RTherapistID = t3.Eno
				and t3.Etype = ''0003''--à©¾ÒÐËÁÍ¹Ç´
				and (convert(varchar(30),t1.RDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
--				and datepart(month,t1.RDate) = datepart(month,GetDate())
			)data
			pivot
			(
				sum(SAmount)
				for  useDate in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31])
			) pvt	
		end
	else if @inReporttype = ''16''-- Therapist Job
		begin
			select	dbo.GetMCustomerType(RCustomerType) CustomerType,
					dbo.GetPackageType(dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)) RMassageType,
					dbo.GetMhour(PHour) RHour,
					dbo.GetMSex(MSex) Sex,
					Pprice,
					PpriceforTherapist,
					dbo.GetEmployeeName(RTherapistID) Therapist,
					Rid
			from	customertransaction t1,
					customerpayment t2,
					package t3,
					customer t4
			where	t1.Rid = t2.SCusTranID 
					and t3.Pno = dbo.GetPackageID(t1.Rpackage ,t1.RCustomerType,t1.RSpecialPackage)
					and t1.RmemberID = t4.Mno
					and t1.RCustomerType = t4.MType
					and (convert(varchar(30),t1.RDate,101) between convert(varchar(30),@inStartDate,101) and convert(varchar(30),@inEndDate,101))
--					and datepart(month,t1.RDate) = datepart(month,GetDate())
		end
END




















' 
END
GO
/****** Object:  StoredProcedure [dbo].[manageEmployeeSummary]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[manageEmployeeSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description: µÒÃÒ§à¡çº¢éÍÁÙÅÊÃØ»¤èÒÁ×Í¾¹Ñ¡§Ò¹
-- =============================================
CREATE PROCEDURE [dbo].[manageEmployeeSummary]
(
	@inId int,
	@inTherapistID varchar(4),
	@inGuestReserv int,
	@inIncome float,
	@inSoldMember float,
	@inFind float,
	@inRemain float,
	@inPeriod varchar(10),
	@inKeyDate datetime,
	@inKeyUser varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''
		insert into EmployeeSummary(therapistID,GuestReserv,Income,SoldMember,Find,Remain,keydate,keyuser,lastedit)
		values(@inTherapistID,@inGuestReserv,@inIncome,@inSoldMember,@inFind,@inRemain,GetDate(),@inKeyUser,GetDate())
	else if @inFlag = ''1''
		update EmployeeSummary set
			therapistID = @inTherapistID,
			GuestReserv = @inGuestReserv,
			Income = @inIncome,
			SoldMember = @inSoldMember,
			Find = @inFind,
			Remain = @inRemain,
			keyuser = @inKeyUser,
			lastedit = GetDate()
		where ID = @inID
	else if @inFlag = ''2''
		delete from EmployeeSummary where ID = @inID
	else if @inFlag = ''3''
		begin
			select t1.Eno,t1.Ename,t2.DDetail,
--			select t1.Eno,t1.Enickname,t3.DDetail,t2.EStartTime,t2.ESEndTime,
			isnull((
				select	isnull(count(it1.SId),0) 
				from	customerpayment it1,customertransaction it2
				where	it1.SCusTranID = it2.Rid
						and (Convert(varchar(2),case when datepart(day,it2.RDate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,it2.RDate))+''-''+Convert(varchar(4),datepart(year,it2.RDate))) = @inPeriod
						and it2.RTherapistRevID <> ''0000''
						and it2.RTherapistID = t1.Eno
				group by it2.RTherapistID
			),0) GuestReservation, --ÅÙ¡¤éÒ¨Í§
			isnull((	
				select isnull(sum(dbo.GetTherapistCost(dbo.GetPackageID(it2.Rpackage ,it2.RCustomerType,it2.RSpecialPackage))),0)
				from	customerpayment it1,
						customertransaction it2
				where	it1.SCusTranID = it2.Rid
						and (Convert(varchar(2),case when datepart(day,it2.RDate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,it2.RDate))+''-''+Convert(varchar(4),datepart(year,it2.RDate))) = @inPeriod
						and it2.RTherapistID = t1.Eno
				group by it2.RTherapistID
			),0) Income,--ÃÒÂÃÑº(ÁÒ¨Ò¡¤èÒÁ×Í¾¹Ñ¡§Ò¹)
			isnull((	select isnull(sum((SAmount*0.01)),0)--1% ¨Ò¡ÃÒ¤ÒÊÁÒªÔ¡
				from	memberpayment it1,
						CustomerMemberPackage it2,
						package it3
				where	it1.TPackageTranID = it2.MID
					and it1.TMemberID = it2.MCusNo
					and it2.Mpackage = it3.Pno
					and it1.TTherapist = t1.Eno
					and (Convert(varchar(2),case when datepart(day,it1.SKeyDate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,it1.SKeyDate))+''-''+Convert(varchar(4),datepart(year,it1.SKeyDate))) = @inPeriod
				group by it1.TTherapist
			),0) SoldMember,--¢ÒÂÊÁÒªÔ¡
			''0'' FineMoney,--ËÑ¡à§Ô¹,
			''0'' RemainMoney--¤§àËÅ×Í
			from employee t1,Mtimeperiod t2
			where etype = ''0003''
			and t1.Mperiod = t2.Dno
			order by Eno
--			from	employee t1,
--					employeeschedule t2,
--					Mtimeperiod t3
--			where	t1.Eno = t2.ESno
--					and t1.Mperiod = t3.Dno
--					and (Convert(varchar(2),case when datepart(day,keydate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,keydate))+''-''+Convert(varchar(4),datepart(year,keydate))) = @inPeriod
--					group by t1.Eno,t1.Enickname,t3.DDetail,t2.EStartTime,t2.ESEndTime --
--			order by t1.Eno
		end
	else if @inFlag =''4''--µÃÇ¨ÊÍºÇèÒÁÕ¢éÍÁÙÅÍÂÙè¡èÍ¹ËÃ×ÍäÁè
		select count(id) icount
		from EmployeeSummary
		where (Convert(varchar(2),case when datepart(day,keydate) <=15 then 1 else 2 end)+''-''+Convert(varchar(2),datepart(month,keydate))+''-''+Convert(varchar(4),datepart(year,keydate))) =@inPeriod
END









' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetMemberReport]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMemberReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Report Member Useed
-- =============================================
CREATE PROCEDURE [dbo].[GetMemberReport]
(
	@inMno varchar(4),
	@inFlag varchar(1)
)
AS
BEGIN
	if @inFlag = ''0''
		begin
			select t1.Mno from Customer t1,CustomerTransaction t2 
			where t1.Mno = t2.RmemberID
			and t1.mtype = t2.RCustomerType
			and t1.MType = ''0002''
			and t2.RSpecialPackage = ''0000''
			group by t1.Mno
		end
	if @inFlag = ''1''
		begin
			select t1.Mno from Customer t1,CustomerTransaction t2 
			where t1.Mno = t2.RmemberID
			and t1.mtype = t2.RCustomerType
			and t1.MType = ''0002''
			and t2.RSpecialPackage <> ''0000''
			group by t1.Mno
		end
	if @inFlag = ''2''
		begin
			select 
				Mno,
				Mname,
				dbo.GetMSex(Msex) Msex,
				dbo.GetMNation(Mnational) Mnational,
				dbo.GetEmployeeName(TTherapist) TTherapist,
				KeyDate,
				dbo.GetMassageType(PackageType) PackageType,
				TherapistCost,
				SAmount,
				MUseMember,
				PackageTotal,
				[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30] 
					from(			
						select	ROW_NUMBER() over (order by Mno) Rownum,
								t1.Mno,
								t1.Mname,
								t1.Msex,
								t1.Mnational,
								t4.TTherapist,
								t2.KeyDate,
								(datepart(month,t3.RDate)*30)+datepart(day,t3.RDate) RTime,
								dbo.GetPackageType(dbo.GetPackageID(t3.Rpackage ,t3.RCustomerType,t3.RSpecialPackage)) PackageType,
								dbo.GetTherapistCost(dbo.GetPackageID(t3.Rpackage ,t3.RCustomerType,t3.RSpecialPackage)) TherapistCost,
								dbo.GetPackageTotal(dbo.GetPackageID(t3.Rpackage ,t3.RCustomerType,t3.RSpecialPackage)) PackageTotal,
								t4.SAmount,
								t2.MUseMember
						from	customer t1,
								customermemberpackage t2,
								customertransaction t3,
								MemberPayment t4
						where	t1.Mno = t2.MCusNo
								and t1.Mno = t3.RmemberID
								and t1.mtype = t3.RCustomerType
								and t1.Mno = t4.TmemberID
								and t1.mtype = ''0002''--à©¾ÒÐÊÁÒªÔ¡
								and t3.RSpecialPackage = ''0000''--à©¾ÒÐ Member
								and t1.Mno = @inMno
			) DATA
			pivot
			(
				sum(RTime)
				for Rownum IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30])
			) pvt
		end
	if @inFlag = ''3''
		begin
			select 
				Mno,
				Mname,
				dbo.GetMSex(Msex) Msex,
				dbo.GetMNation(Mnational) Mnational,
				dbo.GetEmployeeName(TTherapist) TTherapist,
				KeyDate,
				dbo.GetMassageType(PackageType) PackageType,
				TherapistCost,
				SAmount,
				MUseMember,
				PackageTotal,
				[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30] 
					from(			
						select	
								ROW_NUMBER() over (order by Mno) Rownum,
								t1.Mno,
								t1.Mname,
								t1.MType,
								t1.Msex,
								t1.Mnational,
								t4.TTherapist,
								t2.KeyDate,
								(datepart(month,t3.RDate)*30)+datepart(day,t3.RDate) RTime,
								dbo.GetPackageType(t2.Mpackage) PackageType,
								dbo.GetTherapistCost(dbo.GetPackageID(t3.Rpackage ,t3.RCustomerType,t3.RSpecialPackage)) TherapistCost,
								dbo.GetPackageTotal(t2.Mpackage) PackageTotal,
								t4.SAmount,
								t2.MUseMember
						from	customer t1,
								customermemberpackage t2,
								customertransaction t3,
								MemberPayment t4
						where	t1.Mno = t2.MCusNo
								and t1.Mno = t3.RmemberID
								and t1.mtype = t3.RCustomerType
								and t1.Mno = t4.TmemberID
								and t1.mtype = ''0002''--à©¾ÒÐÊÁÒªÔ¡
								and t3.RSpecialPackage <> ''0000''--à©¾ÒÐ Member
								and t1.Mno = @inMno
			) DATA
			pivot
			(
				sum(RTime)
				for Rownum IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30])
			) pvt
		end
END



' 
END
GO
/****** Object:  StoredProcedure [dbo].[PrintInvoiceData]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrintInvoiceData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Print Invoice
-- =============================================
CREATE PROCEDURE [dbo].[PrintInvoiceData]
(
	@inSCusTranID int
)
AS
BEGIN
	select 
			t3.Mname,
			t3.Msurname,
			t3.MContactAddress,
			dbo.GetPackageName(dbo.GetPackageID(t2.Rpackage ,t2.RCustomerType,t2.RSpecialPackage)) RDetail,
			t1.SAmount,
			t1.SDiscount,
			t1.SBook,
			t1.SRunning,
			''1'' Quantity
	from	CustomerPayment t1,
			CustomerTransaction t2,
			Customer t3
	where	t1.SCusTranID = t2.RId
			and t2.RmemberID = t3.Mno
			and t2.RCustomerType = t3.MType
			and t1.SCusTranID = @inSCusTranID
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[managePackage]    Script Date: 06/02/2009 13:02:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[managePackage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[managePackage]
(
	@inNo varchar(10),
	@inDetail varchar(100),
	@inPhour varchar(4),
	@inPprice float,
	@inPmoneyUnit varchar(3),
	@inPMassageType varchar(4),--»ÃÐàÀ· package
	@inPpercourse int,
	@inPCustomerType varchar(4),--package ÊÓËÃÑº (·ÑèÇä»,ÊÁÒªÔ¡)
	@inPpriceforTherapist float,
	@inPstart datetime,
	@inPend datetime,
	@inMkeyUser varchar(4),
	@inFlag varchar(2)
)
AS
BEGIN TRY
	if @inFlag = ''0''--Insert
		insert into Package(Pno,Pdetail,Phour,Pprice,PmoneyUnit,PMassageType,Ppercourse,PCustomerType,PpriceforTherapist,Pstart,Pend,MkeyDate,MlastEdit,MkeyUser) 
		values(@inNo,@inDetail,@inPhour,@inPprice,@inPmoneyUnit,@inPMassageType,@inPpercourse,@inPCustomerType,@inPpriceforTherapist,@inPstart,@inPend,GetDate(),GetDate(),@inMkeyUser)
	else if @inFlag = ''1''--Update
		update Package set 
						Pdetail = @inDetail,
						Phour = @inPhour,
						Pprice = @inPprice,
						PmoneyUnit = @inPmoneyUnit,
						PMassageType = @inPMassageType,
						Ppercourse = @inPpercourse,
						PCustomerType = @inPCustomerType,
						PpriceforTherapist = @inPpriceforTherapist,
						Pstart = @inPstart,
						Pend = @inPend,
						MLastEdit = GetDate(),
						MkeyUser = @inMkeyUser
		where Pno = @inNo
	else if @inFlag = ''2''--Delete
		delete from Package where Pno = @inNo
	else if @inFlag = ''3''--Select
		select * from Package order by PCustomerType,PMassageType,Pno
	else if @inFlag = ''4''--Select
		select * from Package where Pno = @inNo order by Pno
--	else if @inFlag = ''6''--Select
--		select * from package where pgroup = @inPCustomerType order by Pno
	else if @inFlag = ''7''--Select à©¾ÒÐáÂ¡µÒÁª¹Ô´ package 1 ÊÓËÃÑºÊÁÒªÔ¡ 0 ·ÑèÇä»
		select * from package where Pcustomertype = @inPCustomerType
	else if @inFlag = ''8''--Select à©¾ÒÐáÂ¡µÒÁª¹Ô´ package 1 ÊÓËÃÑºÊÁÒªÔ¡ 0 ·ÑèÇä»
		select 
		Pno as Mid,
		PMassageType as PMassageType,
		''0'' as MCusNo,
		Pdetail,
		Phour,
		Pprice,
		''0'' as MUseMember,
		''0'' as Ppercourse 
		from package 
		where Pcustomertype = @inPCustomerType
		and PMassageType = @inPMassageType
	else if @inFlag = ''9''--áÊ´§¢éÍÁÙÅ package ¢Í§ÊÁÒªÔ¡áµèÅÐ¤¹ â´ÂáÊ´§à©¾ÒÐ·ÕèÂÑ§ãªé member äÁèËÁ´
		select t1.MId,PMassageType,MCusNo,Pdetail,Phour,Pprice,MUseMember,Ppercourse 
		from CustomerMemberPackage t1,package t2,customer t3
		where t1.MPackage = t2.Pno
		and t1.mcusno = t3.mno
		and mtype <> ''0001''
		and t1.McusNo = @inNo 
		and dbo.GetPackageTotal(t1.MPackage) <> t1.MUseMember
	else if @inFlag = ''10''--áÊ´§¢éÍÁÙÅ package ¢Í§ÊÁÒªÔ¡áµèÅÐ¤¹ â´ÂáÊ´§à©¾ÒÐ·ÕèÂÑ§ãªé member äÁèËÁ´
		select t1.MId,PMassageType,MCusNo,Pdetail,Phour,Pprice,MUseMember,Ppercourse 
		from CustomerMemberPackage t1,package t2,customer t3
		where t1.MPackage = t2.Pno
		and t1.mcusno = t3.mno
		and mtype <> ''0001''
		and t1.mid = @inNo 
		and dbo.GetPackageTotal(t1.MPackage) <> t1.MUseMember
	else if @inFlag = ''11''--áÊ´§ package ·ÕèÅÙ¡¤éÒ·ÑèÇä»àÅ×Í¡ãªéºÃÔ¡ÒÃ
		select 
		Pno as Mid,
		PMassageType as PMassageType,
		''0'' as MCusNo,
		Pdetail,
		Phour,
		Pprice,
		''0'' as MUseMember,
		''0'' as Ppercourse 
		from package 
		where Pcustomertype = @inPCustomerType 
		and Pno = @inNo
     END TRY
     BEGIN CATCH
--         declare @severity int
--         set @severity = Error_Severity()
         declare @msg varchar(255)
--         set @msg = ''Unable to record transaction in ChargeLog.''
--                  + ''Error('' + ERROR_NUMBER() + ''):'' + ERROR_MESSAGE()
--                  + '' Severity = '' + ERROR_SEVERITY()
--                  + '' State = '' + ERROR_STATE()
--                  + '' Procedure = '' + ERROR_PROCEDURE()
--                  + '' Line num. = '' + ERROR_LINE()
--        INSERT INTO [dbo] . [ErrorLog] ([ErrorNum] , [ErrorType] , [ErrorMsg] , [ErrorSource])
--        VALUES (ERROR_NUMBER(), ''E'', @msg, ERROR_PROCEDURE())
--        RAISERROR (@msg, @severity, 2)
		set @msg  = ''N,''+ERROR_MESSAGE()
		print @msg
     END CATCH
return 































' 
END
GO
