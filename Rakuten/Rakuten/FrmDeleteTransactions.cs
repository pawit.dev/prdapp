﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Structure;
using Rakuten.Utility;

namespace Rakuten
{
    public partial class FrmDeleteTransactions : Form
    {
        private FrmMain main;
        private int index = 0;
        private List<CDeleteTran> list = null;
        public FrmDeleteTransactions(FrmMain _main)
        {
            InitializeComponent();
            this.main = _main;
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void B_SAVE_Click(object sender, EventArgs e)
        {

            if (list != null && list.Count > 0)
            {
                if (MessageBox.Show("คุณต้องการลบข้อมูล " + list[index].Period + " ใช่หรือไม่", "Confirm.", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (DALDeleteTran.manageMaster(new CDeleteTran { Period = list[index].Period, FLAG = "2" }) > 0)
                    {
                        Management.ShowMsg("ลบข้อมูลเรียบร้อยแล้ว โปรดเข้าระบบใหม่อีกครั้ง !!");
                        main.refreshSchedule();
                        Application.Exit();
                    }
                    else
                    {
                        Management.ShowMsg("ไม่สามารถ ลบข้อมูลได้ กรุณาตรวจสอบความถูกต้องของข้อมูล.");
                    }
                }
            }
        }

        private void FrmDeleteTransactions_Load(object sender, EventArgs e)
        {
            label6.Text = this.Text;
            list = DALDeleteTran.getList(new CDeleteTran { Period = "", FLAG = "1" });
            if (list.Count > 0)
            {
                dataGridView2.DataSource = list;
            }
            else {
                Management.ShowMsg("ยังไม่มีข้อมูล Transaction.");
            }
            B_SAVE.Focus();
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
            Cursor = Cursors.Default;
        }
        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView2.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView2.ClearSelection();
                    dataGridView2.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }

        private void FrmDeleteTransactions_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            { 
                case 1:
                    string[] data = e.Value.ToString().Split('-');
                    switch (data[0])
                    { 
                        case "1":
                            e.Value = "วันที่ 1 - 15 เดือน" + DateTime.DaysInMonth(DateTime.Now.Year, Convert.ToInt16(data[1])) + " เดือน" + MyFunction.getMonth(Convert.ToInt16(data[1])) + " ปี " + MyFunction.getYear(Convert.ToInt16(data[2]));
                            break;
                        case "2":
                            e.Value = "วันที่ 16 - " + DateTime.DaysInMonth(DateTime.Now.Year, Convert.ToInt16(data[1])) + " เดือน" + MyFunction.getMonth(Convert.ToInt16(data[1])) + " ปี " + MyFunction.getYear(Convert.ToInt16(data[2]));
                            break;
                    }
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }
    }
}
