﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Structure;

namespace Rakuten
{
    public partial class FrmRoomStatus : Form
    {
        public int index;
        public bool bSelected = false;
        public List<CSchedule> lists = null;
        private DateTime dt;
        public FrmRoomStatus()
        {
            InitializeComponent();
            this.dt = DateTime.Now;
        }
        public FrmRoomStatus(DateTime _dt)
        {
            InitializeComponent();
            this.dt = _dt;
        }
        private void FrmRoomStatus_Load(object sender, EventArgs e)
        {
            if (lists == null)
            {
                lists = DALSchedule.getRoomScheduleList(dt);
                if (lists.Count > 0)
                {
                    dataGridView2.DataSource = lists;
                }
            }
        }
        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (!e.Value.ToString().Equals(""))
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                    case 1:
                        e.CellStyle.BackColor = Color.Orange;
                        e.CellStyle.ForeColor = Color.Black;
                        //When select grid row
                        e.CellStyle.SelectionBackColor = Color.Yellow;
                        e.CellStyle.SelectionForeColor = Color.Green;
                        break;
                    case 27:
                        e.CellStyle.BackColor = Color.Orange;
                        e.CellStyle.ForeColor = Color.Black;
                        //When select grid row
                        e.CellStyle.SelectionBackColor = Color.Yellow;
                        e.CellStyle.SelectionForeColor = Color.Green;
                        break;
                    default:
                        switch (e.Value.ToString())
                        {
                            case "1"://จอง
                                e.CellStyle.BackColor = Color.Red;
                                e.CellStyle.ForeColor = Color.Red;
                                e.CellStyle.SelectionBackColor = Color.Red;
                                e.CellStyle.SelectionForeColor = Color.Red;
                                break;
                            case "2"://ใช้บริการปกติ
                                e.CellStyle.BackColor = Color.Green;
                                e.CellStyle.ForeColor = Color.Green;
                                e.CellStyle.SelectionBackColor = Color.Green;
                                e.CellStyle.SelectionForeColor = Color.Green;
                                break;
                        }
                        break;
                }
            }
            else
            {
                //When select grid row
                if (e.RowIndex == 0)
                {
                    e.CellStyle.BackColor = Color.Black;
                    e.CellStyle.ForeColor = Color.Red;
                    e.CellStyle.SelectionBackColor = Color.Black;
                    e.CellStyle.SelectionForeColor = Color.Red;
                    int ct = DALSchedule.getTimeIndex(DALSchedule.getTimePeriod(DateTime.Now.ToShortTimeString()));
                    if (ct != -1)
                    {
                        if (e.ColumnIndex == (ct + 2))
                        {
                            e.CellStyle.BackColor = Color.Black;
                            e.CellStyle.ForeColor = Color.Red;
                            e.CellStyle.SelectionBackColor = Color.Black;
                            e.CellStyle.SelectionForeColor = Color.Red;
                            e.CellStyle.Font = new Font("Wingdings 3", 14, FontStyle.Bold);
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            e.Value = "";
                        }
                    }
                }
                else
                {
                    e.CellStyle.BackColor = Color.MintCream;
                    e.CellStyle.ForeColor = Color.White;
                    e.CellStyle.SelectionBackColor = Color.Yellow;
                    e.CellStyle.SelectionForeColor = Color.Green;
                }
            }
        }
       
        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView2.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView2.ClearSelection();
                    dataGridView2.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
            Cursor = Cursors.Default;
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
                bSelected = true;
                Close();
            }
            Cursor = Cursors.Default;
        }
    }
}
