﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Utility;
using Rakuten.Structure;
using System.Text;
namespace Rakuten
{
    public partial class FrmMain : Form
    {
        private int menuLevel = 0;
        private int timer = 1;
        private int recID = 0;
        private int timePeriodIndex = 0;
        private string therapist = "0000";
        private int index;
        private DateTime date = DateTime.Now;

        public int RecID
        {
            set { this.recID = value; }
            get { return this.recID; }
        }
        public int TimePeriodIndex
        {
            set { this.timePeriodIndex = value; }
            get { return this.timePeriodIndex; }
        }
        public string Therapist
        {
            set { this.therapist = value; }
            get { return this.therapist; }
        }
        public int Index
        {
            set { this.index = value; }
            get { return this.index; }
        }
        public DateTime Date
        {
            set { this.date = value; }
            get { return this.date; }
        }

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            initial();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (!e.Value.ToString().Equals("") && e.Value != null)
            {
                if (e.RowIndex == 0)
                {
                    //Row 1 Draw Color Black
                    e.CellStyle.BackColor = Color.Black;
                    e.CellStyle.ForeColor = Color.Black;
                    e.CellStyle.SelectionBackColor = Color.Black;
                    e.CellStyle.SelectionForeColor = Color.Black;
                }
                else
                {
                    switch (e.ColumnIndex)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 51:
                        case 60:
                            e.CellStyle.BackColor = Color.Orange;
                            e.CellStyle.ForeColor = Color.Black;
                            //When select grid row
                            e.CellStyle.SelectionBackColor = Color.Yellow;
                            e.CellStyle.SelectionForeColor = Color.Green;
                            break;
                        default:
                            switch (e.Value.ToString())
                            {
                                case "1"://จอง
                                    e.CellStyle.BackColor = Color.Red;
                                    e.CellStyle.ForeColor = Color.Red;
                                    e.CellStyle.SelectionBackColor = Color.Red;
                                    e.CellStyle.SelectionForeColor = Color.Red;
                                    break;
                                case "2"://ใช้บริการปกติ
                                    e.CellStyle.BackColor = Color.Green;
                                    e.CellStyle.ForeColor = Color.Green;
                                    e.CellStyle.SelectionBackColor = Color.Green;
                                    e.CellStyle.SelectionForeColor = Color.Green;
                                    break;
                                case "99"://ใช้บริการเสร็จแล้ว
                                    e.CellStyle.BackColor = Color.Gray;
                                    e.CellStyle.ForeColor = Color.Gray;
                                    e.CellStyle.SelectionBackColor = Color.Gray;
                                    e.CellStyle.SelectionForeColor = Color.Gray;
                                    break;
                                default:
                                    e.CellStyle.BackColor = Color.Orange;
                                    e.CellStyle.ForeColor = Color.Black;
                                    //When select grid row
                                    e.CellStyle.SelectionBackColor = Color.Yellow;
                                    e.CellStyle.SelectionForeColor = Color.Green;
                                    break;
                            }
                            break;
                    }
                }
            }
            else
            {
                if (e.RowIndex == 0)
                {
                    //Row 1 Background
                    e.CellStyle.BackColor = Color.Black;
                    e.CellStyle.ForeColor = Color.Black;
                    e.CellStyle.SelectionBackColor = Color.Black;
                    e.CellStyle.SelectionForeColor = Color.Black;
                    int ct = DALSchedule.getTimeIndex(DALSchedule.getTimePeriod(DateTime.Now.ToShortTimeString()));
                    if (ct != -1)
                    {
                        if (e.ColumnIndex == (ct + 2))
                        {
                            e.CellStyle.BackColor = Color.Black;
                            e.CellStyle.ForeColor = Color.Red;
                            e.CellStyle.SelectionBackColor = Color.Black;
                            e.CellStyle.SelectionForeColor = Color.Red;
                            e.CellStyle.Font = new Font("Wingdings 3", 14, FontStyle.Bold);
                            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            e.Value = "";
                        }
                    }
                }
                else
                {
                    //Draw Background
                    e.CellStyle.BackColor = Color.MintCream;
                    e.CellStyle.ForeColor = Color.White;
                    e.CellStyle.SelectionBackColor = Color.Yellow;
                    e.CellStyle.SelectionForeColor = Color.Green;
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != 0)
            {
                if ((DTP_SERVICE_DATE.Value>DateTime.Now) ||(e.ColumnIndex != 0 && e.ColumnIndex != 1 && e.ColumnIndex != 2) )
                {
                    if ((DTP_SERVICE_DATE.Value > DateTime.Now)||(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString().Equals("")))
                    {
                        if ((dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("")))
                        {
                            if ((DTP_SERVICE_DATE.Value > DateTime.Now)||(!dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString().Equals("")))
                            {
                                this.timePeriodIndex = e.ColumnIndex - 2;//ลบ 2 เพื่อเอา index ของเบอร์พนักงานกับ เวลา ออก
                                this.therapist = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();//เบอร์พนักงาน
                                FrmUseService useService = new FrmUseService(this, DBStatus.Insert);
                                useService.ShowDialog();
                            }
                            else
                            {
                                Management.ShowMsg("พนักยังไม่ได้ลงเวลาเข้างานไม่สามารถทำรายการได้");
                            }
                        }
                        else
                        {
                            Management.ShowMsg("มีการใช้บริการแล้วไม่สามารถทำรายการได้");
                        }
                    }
                    else
                    {
                        Management.ShowMsg("พนักงานได้ลงเวลาออกแล้วไม่สามารถทำรายการได้");
                    }
                }
            }
        }

        private void dataGridView3_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView3.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView3.ClearSelection();
                    dataGridView3.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }

        private void dataGridView3_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 7://Package
                    e.Value = Management.getPackage(e.Value.ToString())[0].DETAIL;
                    break;
                case 13://ห้อง
                    e.Value = Management.getRoom(e.Value.ToString());
                    break;
                case 4://ประเภทลูกค้า
                    e.Value = Management.getDetail(MasterList.MCustomerType, e.Value.ToString());
                    break;
                case 14:
                    switch (e.Value.ToString())
                    {
                        case "1":
                            e.Value = "จอง";
                            break;
                        case "2":
                            e.Value = "ปกติ";
                            break;
                    }
                    break;
                case 30:
                    e.Value = (e.Value.ToString().Equals("1")) ? "จ่ายแล้ว" : "ยังไม่จ่าย";
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void TSM_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem b = (ToolStripMenuItem)sender;
            switch (b.Name)
            {
                case "TSM_UPDATE":
                    doCusTranSactionMenu(CusTranMenu.Update);
                    break;
                case "TSM_CANCEL":
                    doCusTranSactionMenu(CusTranMenu.Cancel);
                    break;
                case "TSM_PAY":
                    doCusTranSactionMenu(CusTranMenu.Pay);
                    break;
                case "TSM_PRINTINVOICE":
                    doCusTranSactionMenu(CusTranMenu.PrintOldReceipt);
                    break;
            }
        }

        private void CMS_CHECKIN_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem cms = (ToolStripMenuItem)sender;
            switch (cms.Name)
            {
                case "CMS_CHECKIN":
                    doTherapistSubMenu(TherapistMenu.CheckIN);
                    break;
                case "CMS_CHECKOUT":
                    doTherapistSubMenu(TherapistMenu.CheckOUT);
                    break;
                case "CMS_LEAVE":
                    doTherapistSubMenu(TherapistMenu.Leave);
                    break;
                case "CMS_P1"://กำหนดให้เป็นกะแรก
                    int p1_count = 0;
                    for (int i = 0; i < listView3.SelectedItems.Count; i++)
                    {
                        string p1 = listView3.SelectedItems[i].Text;
                        p1_count += DALEmployee.updateEmpTimePeriod(
                            p1.Substring(p1.IndexOf('(') + 1, 4),
                            "0001");
                    }
                    if (listView3.SelectedItems.Count == p1_count)
                    {
                        Management.ShowMsg(Management.UPDATE_SUCCESS);
                    }
                    break;
                case "CMS_P2":
                    int p2_count = 0;
                    for (int i = 0; i < listView3.SelectedItems.Count; i++)
                    {
                        string p2 = listView3.SelectedItems[i].Text;
                        p2_count += DALEmployee.updateEmpTimePeriod(
                            p2.Substring(p2.IndexOf('(') + 1, 4),
                            "0002");
                    }
                    if (listView3.SelectedItems.Count == p2_count)
                    {
                        Management.ShowMsg(Management.UPDATE_SUCCESS);
                    }
                    break;
            }
        }

        private void DTP_SERVICE_DATE_ValueChanged(object sender, EventArgs e)
        {
            //ทำรายการล่วงหน้า
            if (DTP_SERVICE_DATE.Value > DateTime.Now)
            {
                label3.Text = "ล่วงหน้า";
            }
            else
            {
                label3.Text = "ปกติ";
            }
            refreshSchedule();
        }

        //Function
        private string getTherapistID()
        {
            string index = "";
            switch (menuLevel)
            {
                case 0:
                    index = listView3.SelectedItems[0].Text;
                    index = index.Substring(index.IndexOf('(') + 1, index.IndexOf(')') - index.IndexOf('(') - 1);
                    break;
            }
            return index;
        }

        private void initial()
        {
            //Initial Schedule
            refreshSchedule();
        }

        private void showSchedule(DateTime dt)
        {
            dataGridView1.DataSource = DALSchedule.getScheduleList(dt);
            dataGridView2.DataSource = DALSchedule.getRoomScheduleList(dt);
        }

        private void showTherapist(DateTime dt)
        {
            //Therapist
            List<CEmployee> emps = DALEmployee.getList(new CEmployee
                        {
                            ID = "",
                            Type = "0003",
                            Sex = "",
                            Title = "",
                            Name = "",
                            Surname = "",
                            NickName = "",
                            Nation = "",
                            BirthDay = DateTime.Now,
                            Address = "",
                            Mobile = "",
                            Email = "",
                            Status = "",
                            Period = "",
                            Biz = 0,
                            Holiday = 0,
                            Sick = 0,
                            KEYDATE = DateTime.Now,
                            KEYUSER = Authorize.getUser(),
                            LASTEDIT = DateTime.Now,
                            FLAG = Management.SearchByGroup//Searh Flag
                        }
                    );
            //Remove OLD
            listView3.Items.Clear();
            //Add New
            foreach (CEmployee emp in emps)
            {
                CSchedule sche = new CSchedule
                {
                    ID = emp.ID,
                    Remark = "",
                    KEYUSER = Authorize.getUser(),
                    KEYDATE = dt,
                    FLAG = "6"//ตั้งค่าเริ่มต้นตารางการทำงานพนักงานนวด
                };
                DALEmployee.manageEmployeeSchedule(sche);
                //นำข้อมูลใน Listview
                ListViewItem lvi = new ListViewItem(emp.Name + "(" + emp.ID + ") " + ((DALSchedule.getCount(emp.ID, DTP_SERVICE_DATE.Value) == 0) ? "" : "R->" + DALSchedule.getCount(emp.ID, DTP_SERVICE_DATE.Value)), (emp.Period.Equals("0001")) ? 12 : 13);
                //Disable Row when Employee is Leave 
                CLeave leave = DALEmployee.GetEmployeeLeaveStatus(emp.ID);
                lvi.ForeColor = (DALEmployee.isCheckIn(emp.ID)) ? Color.Green : Color.Red;
                if (leave.Count > 0)
                {
                    if (leave.FLAG.Equals("0") && leave.Count == 0)
                    {
                        lvi.ForeColor = Color.Red;//ลา
                    }
                    else {
                        lvi.ForeColor = Color.Yellow;//ลา
                    }
                }
                else
                {
                    lvi.ForeColor = (DALEmployee.isCheckIn(emp.ID)) ? Color.Green : Color.Red;
                }
                lvi.Font = new Font("Tahoma", 8, FontStyle.Bold);
                listView3.Items.Add(lvi);
            }
        }

        private void ShowCustomerTransaction(DateTime dt)
        {
            List<CCustomerTransaction> lists = DALCustomerTransaction.getList(
                            new CCustomerTransaction
                            {
                                ID = "",
                                CustomerID = "",
                                TherapistID = "",
                                TherapistRevID = "",
                                Package = "",
                                MassageType = "",
                                Hour = "",
                                Date = dt,
                                Time = DateTime.Now,
                                Room = "",
                                Customertype = "",
                                ScentType = "",
                                ScentCount = 0,
                                ScentType1 = "",
                                ScentCount1 = 0,
                                Name = "",
                                Surname = "",
                                Sex = "",
                                Age = 0,
                                Nation = "",
                                Mobile = "",
                                Address = "",
                                Status = "2",//1 คือ จอง , 2 คือ ใช้งบริการปกติ
                                SpecialPackage = "",
                                KEYUSER = Authorize.getUser(),
                                Remark = "",
                                FLAG = "3"//แสดงข้อมูล transaction ทั้งหมด
                            }
                    );
            //Check Have Data!!
            if (lists.Count == 0)
            {
                dataGridView3.DataSource = null;
            }
            else
            {
                dataGridView3.DataSource = lists;
            }
        }

        public void refreshSchedule()
        {
            //Initial Therapist schedule
            showTherapist(DTP_SERVICE_DATE.Value);
            showSchedule(DTP_SERVICE_DATE.Value);
            ShowCustomerTransaction(DTP_SERVICE_DATE.Value);
            this.date = DTP_SERVICE_DATE.Value;//Set Date
            this.timePeriodIndex = 0;
            this.therapist = "0000";
            menuLevel = 0;
            summarySell();
            summaryCustomer();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if ((timer % 10)==0)//ทุก ๆ 15*60 นาที จะ Refresh
            {
                //หา index เวลาปัจจุบัน
                dataGridView1.Refresh();
                timer = 0;
            }
            timer++;
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (menuLevel == 0 && listView3.SelectedItems.Count > 0)
            {
                CMS_CHECKIN.Enabled = (listView3.SelectedItems[0].ForeColor == Color.Green || listView3.SelectedItems[0].ForeColor == Color.Red || listView3.SelectedItems[0].ForeColor == Color.Yellow) ? true : false;
                CMS_CHECKOUT.Enabled = (listView3.SelectedItems[0].ForeColor == Color.Green || listView3.SelectedItems[0].ForeColor == Color.Red || listView3.SelectedItems[0].ForeColor == Color.Yellow) ? true : false;
                CMS_PERIOD.Enabled = true;
                CMS_LEAVE.Enabled = true;
            }
            else
            {
                CMS_CHECKIN.Enabled = false;
                CMS_CHECKOUT.Enabled = false;
                CMS_PERIOD.Enabled = false;
                CMS_LEAVE.Enabled = false;
            }
        }

        private void contextMenuStrip2_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TSM_UPDATE.Enabled = (dataGridView3.Rows[index].Cells[30].Value.ToString().Equals("1")) ? false : true;
            TSM_CANCEL.Enabled = (dataGridView3.Rows[index].Cells[30].Value.ToString().Equals("1")) ? false : true;
            TSM_PAY.Enabled = (dataGridView3.Rows[index].Cells[30].Value.ToString().Equals("1")) ? false : true;
            TSM_PRINTINVOICE.Enabled = (dataGridView3.Rows[index].Cells[30].Value.ToString().Equals("1")) ? true : false;
        }

        private void checkPrint(int RID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(RID);
            FrmPrintReceipt frmPR = new FrmPrintReceipt(ReceiptType.Single,sb);
            frmPR.ShowDialog();
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            switch (listView1.SelectedItems[0].Index)
            {
                case 0://เปลี่ยนแปลงรายการ
                    doCusTranSactionMenu(CusTranMenu.Update);
                    break;
                case 1://ยกเลิกรายการ
                    doCusTranSactionMenu(CusTranMenu.Cancel);
                    break;
                case 2://พิมพ์ใบเสร็จย้อนหลัง
                    //---------------------------------------------------
                    StringBuilder sb = new StringBuilder();
                    if (!dataGridView3.CurrentRow.Cells[30].Value.ToString().Equals("0"))
                    {
                        for (int i = 0; i < dataGridView3.Rows.Count; i++)
                        {
                            if (dataGridView3.Rows[i].Selected)
                            {
                                sb.Append(dataGridView3.Rows[i].Cells[1].Value.ToString());
                                sb.Append(",");
                            }
                        }
                    }
                    //---------------------------------------------------
                    //Show report form
                    if (sb.ToString().Split(',').Length > 1)
                    {
                        FrmPrintReceipt frmPR = new FrmPrintReceipt(ReceiptType.Many, sb);
                        frmPR.ShowDialog();
                    }
                    else {
                        Management.ShowMsg("ยังไม่ได้ชำระเงิน");
                    }
                    //---------------------------------------------------
                    break;
                case 3://ชำระเงิน
                    doCusTranSactionMenu(CusTranMenu.Pay);
                    break;
                case 4://พิมพ์ใบเสร็จย้อนหลัง
                    doCusTranSactionMenu(CusTranMenu.PrintOldReceipt);
                    break;

            }
        }

        private void listView3_DoubleClick(object sender, EventArgs e)
        {
            switch (menuLevel)
            { 
                case 0:
                    this.Therapist = getTherapistID();//Get ID
                    //Remove OLD
                    listView3.Items.Clear();
                    string[] menu = { "เข้างาน", "ออกงาน","ลบเวลา", "ข้อมูลพนักงาน", "ลางาน", "ย้อนกลับ" };
                    int[] picIndex = { 1, 2, 11, 3, 4, 5, 6 };
                    for (int i = 0; i < menu.Length; i++)
                    {
                        ListViewItem lvi = new ListViewItem(menu[i], picIndex[i]);
                        lvi.ForeColor = Color.Green;//มาทำงานปกติ
                        lvi.Font = new Font("Tahoma", 8, FontStyle.Bold);
                        listView3.Items.Add(lvi);
                    }
                    menuLevel++;//Change to therapist submenu
                    break;
                case 1:
                    switch (listView3.SelectedItems[0].Index)
                    {
                        case 0://"เข้างาน"
                            doTherapistSubMenu(TherapistMenu.CheckIN);
                            break;
                        case 1://"ออกงาน"
                            doTherapistSubMenu(TherapistMenu.CheckOUT);
                            break;
                        case 2://ลบเวลาเข้างาน
                            doTherapistSubMenu(TherapistMenu.Delete);
                            break;
                        case 3://"กำหนดกะงาน"
                            doTherapistSubMenu(TherapistMenu.Employee);
                            break;
                        case 4://"ลางาน"
                            doTherapistSubMenu(TherapistMenu.Leave);
                            break;
                        case 5://"ย้อนกลับ"
                            doTherapistSubMenu(TherapistMenu.Cancel);
                            break;
                    }
                    break;
            }
        }

        private void listView3_Click(object sender, EventArgs e)
        {
            switch (menuLevel)
            {
                case 0://Hilight Cursor
                    this.Therapist = getTherapistID();//Get ID
                    if (listView3.SelectedItems.Count > 0)
                    {
                        dataGridView1.ClearSelection();//Claare All Selected
                        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                        {
                            if (dataGridView1.Rows[i].Cells[0].Value.ToString().Equals(getTherapistID()))
                            {
                                dataGridView1.Rows[i].Selected = true;
                                break;
                            }
                        }
                    }
                    break;
                case 1:
                    break;
            }
        }

        private void doTherapistSubMenu(TherapistMenu menu)
        {
            switch (menu)
            {
                case TherapistMenu.CheckIN:
                    if ((listView3.SelectedItems[0].ForeColor == Color.Red || listView3.SelectedItems[0].ForeColor == Color.Green || listView3.SelectedItems[0].ForeColor == Color.Yellow))
                    {
                        FrmTimeLine tline = new FrmTimeLine(this, ScheduleType.CheckIn, this.therapist);
                        tline.Show();
                    }
                    else
                    {
                        Management.ShowMsg("ท่านอยู่ในช่วงลาไม่สามารถทำรายการได้");
                    }
                    //Refresh Therapist schedule
                    break;
                case TherapistMenu.CheckOUT:
                    if ((listView3.SelectedItems[0].ForeColor == Color.Red || listView3.SelectedItems[0].ForeColor == Color.Green || listView3.SelectedItems[0].ForeColor == Color.Yellow))
                    {
                        FrmTimeLine tline1 = new FrmTimeLine(this, ScheduleType.CheckOut, this.therapist);
                        tline1.Show();
                    }
                    else
                    {
                        Management.ShowMsg("ท่านอยู่ในช่วงลาไม่สามารถทำรายการได้");
                    }
                    //Refresh Therapist schedule
                    break;
                case TherapistMenu.Delete:
                    if ((listView3.SelectedItems[0].ForeColor == Color.Red || listView3.SelectedItems[0].ForeColor == Color.Green || listView3.SelectedItems[0].ForeColor == Color.Yellow))
                    {
                        FrmTimeLine tline1 = new FrmTimeLine(this, ScheduleType.Delete, this.therapist);
                        tline1.Show();
                    }
                    else
                    {
                        Management.ShowMsg("ท่านอยู่ในช่วงลาไม่สามารถทำรายการได้");
                    }
                    //Refresh Therapist schedule
                    break;
                case TherapistMenu.Employee:
                    FrmEmployee fEmp = new FrmEmployee(this);
                    fEmp.ShowDialog();
                    break;
                case TherapistMenu.Leave:
                    FrmLeave leave = new FrmLeave(this);
                    leave.ShowDialog();
                    break;
                case TherapistMenu.Cancel:
                    break;
            }
            refreshSchedule();//Refresh Data
        }

        private void doCusTranSactionMenu(CusTranMenu menu)
        {
            if (dataGridView3.Rows.Count > 0)
            {
                switch (menu)
                {
                    case CusTranMenu.Update:
                        if (!dataGridView3.Rows[index].Cells[30].Value.ToString().Equals("1"))
                        {
                            recID = Convert.ToInt16(dataGridView3.Rows[index].Cells[1].Value);
                            FrmUseService useService = new FrmUseService(this, DBStatus.Update);
                            useService.ShowDialog();
                        }
                        else {
                            Management.ShowMsg("ไม่สามารถเปลี่ยนแปลงรายการได้เนื่อง รายการนี้ชำระเงินแล้ว");
                        }
                        break;
                    case CusTranMenu.Cancel:
                        if (!dataGridView3.Rows[index].Cells[30].Value.ToString().Equals("1"))
                        {
                            recID = Convert.ToInt16(dataGridView3.Rows[index].Cells[1].Value);
                            FrmUseService useService1 = new FrmUseService(this, DBStatus.Delete);
                            useService1.ShowDialog();
                        }
                        else
                        {
                            Management.ShowMsg("ไม่สามารถลบรายการได้เนื่อง รายการนี้ชำระเงินแล้ว");
                        }
                        break;
                    case CusTranMenu.Pay:
                        //if (dataGridView3.Rows[index].Cells[14].Value.ToString().Equals("1"))
                        //{
                        //    Management.ShowMsg("ไม่สามารถชำระเงินรายการที่จองได้-->ต้องเปลี่ยนรายการเป็นใช้งานปกติก่อน");
                        //}
                        if (dataGridView3.Rows[index].Cells[30].Value.ToString().Equals("1"))
                        {
                            Management.ShowMsg("ชำระเงินเรียบร้อยแล้ว");
                        }
                        else
                        {
                            recID = Convert.ToInt16(dataGridView3.Rows[index].Cells[1].Value);
                            FrmPayment payment = new FrmPayment(this);
                            payment.ShowDialog();
                        }
                        break;
                    case CusTranMenu.PrintOldReceipt:
                        //พิมพ์ใบเสร็จย้อนหลัง
                        if (!dataGridView3.CurrentRow.Cells[30].Value.ToString().Equals("0"))
                        {
                            checkPrint(Convert.ToInt16(dataGridView3.Rows[index].Cells[1].Value));
                        }
                        else
                        {
                            Management.ShowMsg("ยังไม่ชำระเงินไม่สามารถพิมพ์ใบเสร็จย้อนหลังได้");
                        }
                        break;
                }
            }
        }

        public string[] getTherapistData(string _therapistID)
        {
            string[] data = new string[3];
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells[0].Equals(_therapistID))
                {
                    data[0] = dataGridView1.Rows[i].Cells[1].Value.ToString();//เข้างาน
                    data[1] = dataGridView1.Rows[i].Cells[2].Value.ToString();//ออกงาน
                    data[2] = dataGridView1.Rows[i].Cells[51].Value.ToString();//รอบงาน
                }
            }
            return data;
        }

        private void summarySell()
        {
            //จำนวนยอดขาย -------------------------------------------------------------------------------
            double amount = 0;
            List<CReport9> lists = DALReport.getReport9(DTP_SERVICE_DATE.Value, DTP_SERVICE_DATE.Value);
            foreach (CReport9 c in lists)
            {
                amount += c.Amount;
            }
            LB_SELL_TOTAL.Text = amount.ToString();
        }
        private void summaryCustomer()
        {
            //จำนวนลูกค้า -------------------------------------------------------------------------------
            int amount = 0;
            List<CReport12> lists = DALReport.getReport12(DTP_SERVICE_DATE.Value, DTP_SERVICE_DATE.Value);
            foreach (CReport12 c in lists)
            {
                amount++;
            }
            LB_CUS_TOTAL.Text = amount.ToString();
        }
    }
}
