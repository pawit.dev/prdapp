﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Rakuten.Structure;
using System.Data.SqlClient;
using Rakuten.DAL;
using System.Data;

namespace Rakuten.Utility
{
    public enum DBStatus
    { 
        Insert,
        Update,
        Delete,
        Member
    }
    public enum FormStatus
    { 
        Normal,
        Search
    }
    public enum TherapistMenu
    { 
        CheckIN,
        CheckOUT,
        Delete,
        Employee,
        Leave,
        Cancel
    }
    public enum CusTranMenu
    { 
        Update,
        Cancel,
        Pay,
        PrintOldReceipt
    }
    class Authorize
    {
        //Get User Authorize
        public static bool[] GetAuthorize(string formName)
        {
            //set user permission
            List<CPermission> lp = DALPermission.getList(
                                new CPermission
                                {
                                    From = "",
                                    EMPID = Authorize.getUser(),
                                    Insert = 0,
                                    Delete = 0,
                                    Update = 0,
                                    View = 0,
                                    FLAG = "1"
                                });
            bool[] bAuthorize = new bool[5];
            foreach (CPermission p in lp)
            {
                if (p.From.Equals(formName))
                {
                    bAuthorize[0] = (p.Insert == 0) ? false : true;
                    bAuthorize[1] = (p.Delete == 0) ? false : true;
                    bAuthorize[2] = (p.Update == 0) ? false : true;
                    bAuthorize[3] = (p.View == 0) ? false : true;
                    break;
                }
            }
            return bAuthorize;
        }

        //public static bool[] GetAuthorize()
        //{
        //    bool[] b = { true, true, true, true };
        //    return b;
        //}

        public static void setKeyUser(string uid, string name,string usertype)
        {
            ConfigurationManager.AppSettings["userid"] = uid;
            ConfigurationManager.AppSettings["namesurname"] = name;
            ConfigurationManager.AppSettings["userType"] = usertype;
        }
        public static string getUser()
        {
            return ConfigurationManager.AppSettings["userid"].ToString();
        }
        public static string getUserName()
        {
            return ConfigurationManager.AppSettings["namesurname"].ToString();
        }
        public static string getUserPermission()
        {
            return ConfigurationManager.AppSettings["userType"].ToString();
        }

    }
}
