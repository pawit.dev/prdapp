﻿using System.Net.Mail;
using Rakuten.Utility;
using System;
using System.Net.Mime;
using System.Collections;
using System.Net;
using System.Collections.Generic;
namespace Rakuten
{
    class SendSMS
    {
        public static string sendMAIL(
            string addrTo,//"info@rakutenspa.com"
            string addrFrom,//"info@rakutenspa.com"
            string subject,
            string body,
            string sUser,
            string sPass,
            string host,//"smtp.gmail.com"
            List<string> fileList
            )
        {
            string status = "Fail";
            MailMessage msg = new MailMessage();
            msg.To.Add(addrTo);
            msg.From = new MailAddress(addrFrom, "Rakuten spa Infomation.", System.Text.Encoding.UTF8);
            msg.Subject = subject;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.Body = body;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.IsBodyHtml = false;
            msg.Priority = MailPriority.Normal;
            foreach (string file in fileList)
            {
                Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
                // Add time stamp information for the file.
                ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(file);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
                msg.Attachments.Add(data);
            }
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(sUser, sPass);
            client.Port = 25;//or use 587 465 
            client.Host = host;
            client.EnableSsl = true;
            
            try
            {
                client.Send(msg);
                status = "Success";
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                status = ex.Message;
            }
            return status;
        }
    }
}
