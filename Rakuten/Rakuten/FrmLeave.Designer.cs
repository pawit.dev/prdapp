﻿namespace Rakuten
{
    partial class FrmLeave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CMD_CANCEL = new System.Windows.Forms.Button();
            this.CMD_SAVE = new System.Windows.Forms.Button();
            this.DTS_END = new System.Windows.Forms.DateTimePicker();
            this.TXT_TOTAL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TXT_USE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TXT_COUNT = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TXT_REMARK = new System.Windows.Forms.TextBox();
            this.DTP_S = new System.Windows.Forms.DateTimePicker();
            this.CBO_TYPE = new System.Windows.Forms.ComboBox();
            this.cMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TXT_ID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TXT_NAME = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.B_SMEMBER = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.periodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bizDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.holidayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sickDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.surnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nickNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rUNNINGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMS_Insert = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.cLeaveBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cEmployeeSummaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLeaveBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeSummaryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.CMD_CANCEL);
            this.groupBox4.Controls.Add(this.CMD_SAVE);
            this.groupBox4.Controls.Add(this.DTS_END);
            this.groupBox4.Controls.Add(this.TXT_TOTAL);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.TXT_USE);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.TXT_COUNT);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.TXT_REMARK);
            this.groupBox4.Controls.Add(this.DTP_S);
            this.groupBox4.Controls.Add(this.CBO_TYPE);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(620, 297);
            this.groupBox4.TabIndex = 142;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ข้อมูลการลาขาด";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(61, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 14);
            this.label5.TabIndex = 149;
            this.label5.Text = "ถึง";
            // 
            // CMD_CANCEL
            // 
            this.CMD_CANCEL.Image = global::Rakuten.Properties.Resources.redo;
            this.CMD_CANCEL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_CANCEL.Location = new System.Drawing.Point(539, 252);
            this.CMD_CANCEL.Name = "CMD_CANCEL";
            this.CMD_CANCEL.Size = new System.Drawing.Size(75, 39);
            this.CMD_CANCEL.TabIndex = 9;
            this.CMD_CANCEL.Text = "ยกเลิก";
            this.CMD_CANCEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CMD_CANCEL.UseVisualStyleBackColor = true;
            this.CMD_CANCEL.Click += new System.EventHandler(this.CMD_CANCEL_Click);
            // 
            // CMD_SAVE
            // 
            this.CMD_SAVE.Image = global::Rakuten.Properties.Resources.disk_blue_ok;
            this.CMD_SAVE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_SAVE.Location = new System.Drawing.Point(458, 252);
            this.CMD_SAVE.Name = "CMD_SAVE";
            this.CMD_SAVE.Size = new System.Drawing.Size(75, 39);
            this.CMD_SAVE.TabIndex = 8;
            this.CMD_SAVE.Text = "บันทึก";
            this.CMD_SAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CMD_SAVE.UseVisualStyleBackColor = true;
            this.CMD_SAVE.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // DTS_END
            // 
            this.DTS_END.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTS_END.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTS_END.Location = new System.Drawing.Point(101, 82);
            this.DTS_END.Name = "DTS_END";
            this.DTS_END.Size = new System.Drawing.Size(131, 26);
            this.DTS_END.TabIndex = 5;
            this.DTS_END.TabStop = false;
            this.DTS_END.ValueChanged += new System.EventHandler(this.DTS_END_ValueChanged);
            // 
            // TXT_TOTAL
            // 
            this.TXT_TOTAL.BackColor = System.Drawing.Color.White;
            this.TXT_TOTAL.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_TOTAL.ForeColor = System.Drawing.Color.ForestGreen;
            this.TXT_TOTAL.Location = new System.Drawing.Point(415, 19);
            this.TXT_TOTAL.Name = "TXT_TOTAL";
            this.TXT_TOTAL.ReadOnly = true;
            this.TXT_TOTAL.Size = new System.Drawing.Size(60, 26);
            this.TXT_TOTAL.TabIndex = 10;
            this.TXT_TOTAL.TabStop = false;
            this.TXT_TOTAL.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(364, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 14);
            this.label2.TabIndex = 147;
            this.label2.Text = "ทั้งหมด";
            // 
            // TXT_USE
            // 
            this.TXT_USE.BackColor = System.Drawing.Color.White;
            this.TXT_USE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_USE.ForeColor = System.Drawing.Color.ForestGreen;
            this.TXT_USE.Location = new System.Drawing.Point(415, 51);
            this.TXT_USE.Name = "TXT_USE";
            this.TXT_USE.ReadOnly = true;
            this.TXT_USE.Size = new System.Drawing.Size(60, 26);
            this.TXT_USE.TabIndex = 11;
            this.TXT_USE.TabStop = false;
            this.TXT_USE.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(320, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 14);
            this.label1.TabIndex = 145;
            this.label1.Text = "จำนวนวันที่เหลือ";
            // 
            // TXT_COUNT
            // 
            this.TXT_COUNT.BackColor = System.Drawing.Color.White;
            this.TXT_COUNT.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_COUNT.ForeColor = System.Drawing.Color.Red;
            this.TXT_COUNT.Location = new System.Drawing.Point(414, 83);
            this.TXT_COUNT.Name = "TXT_COUNT";
            this.TXT_COUNT.Size = new System.Drawing.Size(61, 26);
            this.TXT_COUNT.TabIndex = 6;
            this.TXT_COUNT.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(55, 132);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 14);
            this.label13.TabIndex = 133;
            this.label13.Text = "เหตุผล";
            // 
            // TXT_REMARK
            // 
            this.TXT_REMARK.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_REMARK.Location = new System.Drawing.Point(101, 115);
            this.TXT_REMARK.Multiline = true;
            this.TXT_REMARK.Name = "TXT_REMARK";
            this.TXT_REMARK.Size = new System.Drawing.Size(374, 57);
            this.TXT_REMARK.TabIndex = 7;
            this.TXT_REMARK.TabStop = false;
            // 
            // DTP_S
            // 
            this.DTP_S.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.DTP_S.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTP_S.Location = new System.Drawing.Point(101, 51);
            this.DTP_S.Name = "DTP_S";
            this.DTP_S.Size = new System.Drawing.Size(131, 26);
            this.DTP_S.TabIndex = 4;
            this.DTP_S.TabStop = false;
            // 
            // CBO_TYPE
            // 
            this.CBO_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_TYPE.DisplayMember = "DETAIL";
            this.CBO_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_TYPE.FormattingEnabled = true;
            this.CBO_TYPE.Location = new System.Drawing.Point(101, 19);
            this.CBO_TYPE.Name = "CBO_TYPE";
            this.CBO_TYPE.Size = new System.Drawing.Size(131, 26);
            this.CBO_TYPE.TabIndex = 3;
            this.CBO_TYPE.TabStop = false;
            this.CBO_TYPE.ValueMember = "ID";
            this.CBO_TYPE.SelectedIndexChanged += new System.EventHandler(this.CBO_TYPE_SelectedIndexChanged);
            // 
            // cMasterBindingSource
            // 
            this.cMasterBindingSource.DataSource = typeof(Rakuten.Structure.CMaster);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(14, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 14);
            this.label4.TabIndex = 131;
            this.label4.Text = "ประเภทรายการ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.Location = new System.Drawing.Point(61, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 14);
            this.label15.TabIndex = 130;
            this.label15.Text = "ตั้งแต่";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(320, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 14);
            this.label10.TabIndex = 136;
            this.label10.Text = "จำนวนวันที่จะลา";
            // 
            // TXT_ID
            // 
            this.TXT_ID.BackColor = System.Drawing.SystemColors.MenuText;
            this.TXT_ID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXT_ID.ForeColor = System.Drawing.Color.Gold;
            this.TXT_ID.Location = new System.Drawing.Point(75, 19);
            this.TXT_ID.Name = "TXT_ID";
            this.TXT_ID.Size = new System.Drawing.Size(144, 23);
            this.TXT_ID.TabIndex = 1;
            this.TXT_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TXT_ID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_ID_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(38, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 14);
            this.label3.TabIndex = 149;
            this.label3.Text = "เบอร์";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.Location = new System.Drawing.Point(22, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 14);
            this.label18.TabIndex = 148;
            this.label18.Text = "ชื่อ-สกุล";
            // 
            // TXT_NAME
            // 
            this.TXT_NAME.BackColor = System.Drawing.Color.White;
            this.TXT_NAME.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_NAME.ForeColor = System.Drawing.Color.ForestGreen;
            this.TXT_NAME.Location = new System.Drawing.Point(75, 48);
            this.TXT_NAME.Name = "TXT_NAME";
            this.TXT_NAME.ReadOnly = true;
            this.TXT_NAME.Size = new System.Drawing.Size(382, 26);
            this.TXT_NAME.TabIndex = 2;
            this.TXT_NAME.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.B_SMEMBER);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.TXT_ID);
            this.groupBox1.Controls.Add(this.TXT_NAME);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(10, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(629, 86);
            this.groupBox1.TabIndex = 151;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ข้อมูลพนักงาน";
            // 
            // B_SMEMBER
            // 
            this.B_SMEMBER.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_SMEMBER.Location = new System.Drawing.Point(225, 19);
            this.B_SMEMBER.Name = "B_SMEMBER";
            this.B_SMEMBER.Size = new System.Drawing.Size(30, 26);
            this.B_SMEMBER.TabIndex = 150;
            this.B_SMEMBER.TabStop = false;
            this.B_SMEMBER.Text = "...";
            this.B_SMEMBER.UseVisualStyleBackColor = true;
            this.B_SMEMBER.Click += new System.EventHandler(this.B_SMEMBER_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(83, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 25);
            this.label6.TabIndex = 154;
            this.label6.Text = "กำหนดการลา";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.product;
            this.pictureBox1.Location = new System.Drawing.Point(10, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(67, 50);
            this.pictureBox1.TabIndex = 152;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(-2, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(661, 50);
            this.pictureBox2.TabIndex = 153;
            this.pictureBox2.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 148);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(634, 329);
            this.tabControl1.TabIndex = 159;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(626, 303);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ข้อมูลการลา";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.startDateDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn,
            this.countDataGridViewTextBoxColumn,
            this.periodDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.bizDataGridViewTextBoxColumn,
            this.holidayDataGridViewTextBoxColumn,
            this.sickDataGridViewTextBoxColumn,
            this.sexDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.surnameDataGridViewTextBoxColumn,
            this.nickNameDataGridViewTextBoxColumn,
            this.nationDataGridViewTextBoxColumn,
            this.birthDayDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn,
            this.mobileDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.ageDataGridViewTextBoxColumn,
            this.rUNNINGDataGridViewTextBoxColumn,
            this.dETAILDataGridViewTextBoxColumn,
            this.kEYDATEDataGridViewTextBoxColumn,
            this.lASTEDITDataGridViewTextBoxColumn,
            this.kEYUSERDataGridViewTextBoxColumn,
            this.fLAGDataGridViewTextBoxColumn,
            this.remarkDataGridViewTextBoxColumn});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.DataSource = this.cLeaveBindingSource;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(620, 297);
            this.dataGridView1.TabIndex = 114;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDown);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "รหัส";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "ชื่อ";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Visible = false;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "ประเภท";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "StartDate";
            dataGridViewCellStyle2.Format = "d";
            dataGridViewCellStyle2.NullValue = null;
            this.startDateDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.startDateDataGridViewTextBoxColumn.HeaderText = "วันที่เริ่มลา";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "EndDate";
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            this.endDateDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.endDateDataGridViewTextBoxColumn.HeaderText = "วันที่ลาถึง";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // countDataGridViewTextBoxColumn
            // 
            this.countDataGridViewTextBoxColumn.DataPropertyName = "Count";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.countDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.countDataGridViewTextBoxColumn.HeaderText = "จำนวนวันลา";
            this.countDataGridViewTextBoxColumn.Name = "countDataGridViewTextBoxColumn";
            this.countDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // periodDataGridViewTextBoxColumn
            // 
            this.periodDataGridViewTextBoxColumn.DataPropertyName = "Period";
            this.periodDataGridViewTextBoxColumn.HeaderText = "Period";
            this.periodDataGridViewTextBoxColumn.Name = "periodDataGridViewTextBoxColumn";
            this.periodDataGridViewTextBoxColumn.ReadOnly = true;
            this.periodDataGridViewTextBoxColumn.Visible = false;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn.Visible = false;
            // 
            // bizDataGridViewTextBoxColumn
            // 
            this.bizDataGridViewTextBoxColumn.DataPropertyName = "Biz";
            this.bizDataGridViewTextBoxColumn.HeaderText = "Biz";
            this.bizDataGridViewTextBoxColumn.Name = "bizDataGridViewTextBoxColumn";
            this.bizDataGridViewTextBoxColumn.ReadOnly = true;
            this.bizDataGridViewTextBoxColumn.Visible = false;
            // 
            // holidayDataGridViewTextBoxColumn
            // 
            this.holidayDataGridViewTextBoxColumn.DataPropertyName = "Holiday";
            this.holidayDataGridViewTextBoxColumn.HeaderText = "Holiday";
            this.holidayDataGridViewTextBoxColumn.Name = "holidayDataGridViewTextBoxColumn";
            this.holidayDataGridViewTextBoxColumn.ReadOnly = true;
            this.holidayDataGridViewTextBoxColumn.Visible = false;
            // 
            // sickDataGridViewTextBoxColumn
            // 
            this.sickDataGridViewTextBoxColumn.DataPropertyName = "Sick";
            this.sickDataGridViewTextBoxColumn.HeaderText = "Sick";
            this.sickDataGridViewTextBoxColumn.Name = "sickDataGridViewTextBoxColumn";
            this.sickDataGridViewTextBoxColumn.ReadOnly = true;
            this.sickDataGridViewTextBoxColumn.Visible = false;
            // 
            // sexDataGridViewTextBoxColumn
            // 
            this.sexDataGridViewTextBoxColumn.DataPropertyName = "Sex";
            this.sexDataGridViewTextBoxColumn.HeaderText = "Sex";
            this.sexDataGridViewTextBoxColumn.Name = "sexDataGridViewTextBoxColumn";
            this.sexDataGridViewTextBoxColumn.ReadOnly = true;
            this.sexDataGridViewTextBoxColumn.Visible = false;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            this.titleDataGridViewTextBoxColumn.Visible = false;
            // 
            // surnameDataGridViewTextBoxColumn
            // 
            this.surnameDataGridViewTextBoxColumn.DataPropertyName = "Surname";
            this.surnameDataGridViewTextBoxColumn.HeaderText = "Surname";
            this.surnameDataGridViewTextBoxColumn.Name = "surnameDataGridViewTextBoxColumn";
            this.surnameDataGridViewTextBoxColumn.ReadOnly = true;
            this.surnameDataGridViewTextBoxColumn.Visible = false;
            // 
            // nickNameDataGridViewTextBoxColumn
            // 
            this.nickNameDataGridViewTextBoxColumn.DataPropertyName = "NickName";
            this.nickNameDataGridViewTextBoxColumn.HeaderText = "NickName";
            this.nickNameDataGridViewTextBoxColumn.Name = "nickNameDataGridViewTextBoxColumn";
            this.nickNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nickNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // nationDataGridViewTextBoxColumn
            // 
            this.nationDataGridViewTextBoxColumn.DataPropertyName = "Nation";
            this.nationDataGridViewTextBoxColumn.HeaderText = "Nation";
            this.nationDataGridViewTextBoxColumn.Name = "nationDataGridViewTextBoxColumn";
            this.nationDataGridViewTextBoxColumn.ReadOnly = true;
            this.nationDataGridViewTextBoxColumn.Visible = false;
            // 
            // birthDayDataGridViewTextBoxColumn
            // 
            this.birthDayDataGridViewTextBoxColumn.DataPropertyName = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn.HeaderText = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn.Name = "birthDayDataGridViewTextBoxColumn";
            this.birthDayDataGridViewTextBoxColumn.ReadOnly = true;
            this.birthDayDataGridViewTextBoxColumn.Visible = false;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn.HeaderText = "Address";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            this.addressDataGridViewTextBoxColumn.ReadOnly = true;
            this.addressDataGridViewTextBoxColumn.Visible = false;
            // 
            // mobileDataGridViewTextBoxColumn
            // 
            this.mobileDataGridViewTextBoxColumn.DataPropertyName = "Mobile";
            this.mobileDataGridViewTextBoxColumn.HeaderText = "Mobile";
            this.mobileDataGridViewTextBoxColumn.Name = "mobileDataGridViewTextBoxColumn";
            this.mobileDataGridViewTextBoxColumn.ReadOnly = true;
            this.mobileDataGridViewTextBoxColumn.Visible = false;
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            this.emailDataGridViewTextBoxColumn.ReadOnly = true;
            this.emailDataGridViewTextBoxColumn.Visible = false;
            // 
            // ageDataGridViewTextBoxColumn
            // 
            this.ageDataGridViewTextBoxColumn.DataPropertyName = "Age";
            this.ageDataGridViewTextBoxColumn.HeaderText = "Age";
            this.ageDataGridViewTextBoxColumn.Name = "ageDataGridViewTextBoxColumn";
            this.ageDataGridViewTextBoxColumn.ReadOnly = true;
            this.ageDataGridViewTextBoxColumn.Visible = false;
            // 
            // rUNNINGDataGridViewTextBoxColumn
            // 
            this.rUNNINGDataGridViewTextBoxColumn.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn.HeaderText = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn.Name = "rUNNINGDataGridViewTextBoxColumn";
            this.rUNNINGDataGridViewTextBoxColumn.ReadOnly = true;
            this.rUNNINGDataGridViewTextBoxColumn.Visible = false;
            // 
            // dETAILDataGridViewTextBoxColumn
            // 
            this.dETAILDataGridViewTextBoxColumn.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.HeaderText = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.Name = "dETAILDataGridViewTextBoxColumn";
            this.dETAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.dETAILDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn
            // 
            this.kEYDATEDataGridViewTextBoxColumn.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.Name = "kEYDATEDataGridViewTextBoxColumn";
            this.kEYDATEDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn
            // 
            this.lASTEDITDataGridViewTextBoxColumn.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.Name = "lASTEDITDataGridViewTextBoxColumn";
            this.lASTEDITDataGridViewTextBoxColumn.ReadOnly = true;
            this.lASTEDITDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn
            // 
            this.kEYUSERDataGridViewTextBoxColumn.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.Name = "kEYUSERDataGridViewTextBoxColumn";
            this.kEYUSERDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYUSERDataGridViewTextBoxColumn.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn
            // 
            this.fLAGDataGridViewTextBoxColumn.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.Name = "fLAGDataGridViewTextBoxColumn";
            this.fLAGDataGridViewTextBoxColumn.ReadOnly = true;
            this.fLAGDataGridViewTextBoxColumn.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn
            // 
            this.remarkDataGridViewTextBoxColumn.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn.HeaderText = "Remark";
            this.remarkDataGridViewTextBoxColumn.Name = "remarkDataGridViewTextBoxColumn";
            this.remarkDataGridViewTextBoxColumn.ReadOnly = true;
            this.remarkDataGridViewTextBoxColumn.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMS_Insert,
            this.CMS_Update,
            this.CMS_Delete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 70);
            // 
            // CMS_Insert
            // 
            this.CMS_Insert.Name = "CMS_Insert";
            this.CMS_Insert.Size = new System.Drawing.Size(112, 22);
            this.CMS_Insert.Text = "Insert";
            this.CMS_Insert.Click += new System.EventHandler(this.CMS_Insert_Click);
            // 
            // CMS_Update
            // 
            this.CMS_Update.Name = "CMS_Update";
            this.CMS_Update.Size = new System.Drawing.Size(112, 22);
            this.CMS_Update.Text = "Update";
            this.CMS_Update.Click += new System.EventHandler(this.CMS_Insert_Click);
            // 
            // CMS_Delete
            // 
            this.CMS_Delete.Name = "CMS_Delete";
            this.CMS_Delete.Size = new System.Drawing.Size(112, 22);
            this.CMS_Delete.Text = "Delete";
            this.CMS_Delete.Click += new System.EventHandler(this.CMS_Insert_Click);
            // 
            // cLeaveBindingSource
            // 
            this.cLeaveBindingSource.DataSource = typeof(Rakuten.Structure.CLeave);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(626, 303);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "เพิ่ม/แก้ไข";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cEmployeeSummaryBindingSource
            // 
            this.cEmployeeSummaryBindingSource.DataSource = typeof(Rakuten.Structure.CEmployeeSummary);
            // 
            // FrmLeave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(656, 485);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLeave";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "กำหนดวันลา";
            this.Load += new System.EventHandler(this.FrmLeave_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmLeave_FormClosed);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cLeaveBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeSummaryBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TXT_REMARK;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker DTP_S;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CBO_TYPE;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.BindingSource cEmployeeSummaryBindingSource;
        private System.Windows.Forms.Button CMD_SAVE;
        private System.Windows.Forms.TextBox TXT_ID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TXT_NAME;
        private System.Windows.Forms.Button CMD_CANCEL;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button B_SMEMBER;
        private System.Windows.Forms.BindingSource cMasterBindingSource;
        private System.Windows.Forms.TextBox TXT_COUNT;
        private System.Windows.Forms.TextBox TXT_USE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TXT_TOTAL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker DTS_END;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.BindingSource cLeaveBindingSource;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem CMS_Insert;
        private System.Windows.Forms.ToolStripMenuItem CMS_Update;
        private System.Windows.Forms.ToolStripMenuItem CMS_Delete;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn periodDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bizDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn holidayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sickDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn surnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nickNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn;
    }
}