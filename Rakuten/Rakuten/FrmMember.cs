﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using Rakuten.Structure;
using Rakuten.DAL;
using Rakuten.Utility;
using System.Data.SqlClient;
using Rakuten.Report;
using System.Text;
namespace Rakuten
{
    public partial class FrmMember : Form
    {
        private FormStatus formStatus = FormStatus.Normal;
        private DBStatus status = DBStatus.Insert;//Initial Status
        public List<CMember> lists = null;
        public int index;
        private bool bSearch = false;
        public bool bSelected = false;
        private string group = "";
        public FrmMember()//Normal
        {
            InitializeComponent();
            //if (formStatus == FormStatus.Search)
            //{
            //    groupBox7.Enabled = true;
            //}
            //else
            //{
            //    groupBox7.Enabled = false;
            //}
        }

        public FrmMember(FormStatus _fStatus)//Screen
        {
            InitializeComponent();
            this.formStatus = _fStatus;
            //if (formStatus == FormStatus.Search)
            //{
            //    groupBox7.Enabled = true;
            //}
            //else {
            //    groupBox7.Enabled = false;
            //}
        }

        public FrmMember(FormStatus _fStatus,string _group)//Screen
        {
            InitializeComponent();
            this.formStatus = _fStatus;
            this.group = _group;
            //if (formStatus == FormStatus.Search)
            //{
            //    groupBox7.Enabled = true;
            //}
            //else
            //{
            //    groupBox7.Enabled = false;
            //}
        }

        private void FrmCustomer_Load(object sender, EventArgs e)
        {
            initial();
            search();
            CBO_TYPE1.SelectedValue = (this.group.Equals("")) ? "0002" : this.group;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                search();
            }
        }

        private void TXT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //TXT_TOTAL.Text = Convert.ToString(Convert.ToInt16(TXT_TOTAL.Text) - Convert.ToInt16(TXT_DISCOUNT.Text));
                SendKeys.Send("{TAB}");
            }
        }

        private void CheckIsNumber_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void B_Search_Click(object sender, EventArgs e)
        {
            bSearch = true;
            search();
        }

        private void CBO_CASH_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (CBO_CASH_TYPE.SelectedIndex)
            {
                case 0://Crash
                    CBO_BANK.Enabled = false;
                    TXT_SLIP.Enabled = false;
                    break;
                case 1://Credit Card
                    CBO_BANK.Enabled = true;
                    TXT_SLIP.Enabled = true;
                    break;
            }
        }

        private void TXT_RECIEVE_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TXT_CHANGE.Text = Convert.ToString((Convert.ToInt16(TXT_RECIEVE.Text) + Convert.ToInt16(TXT_DISCOUNT.Text)) - Convert.ToInt16(TXT_TOTAL.Text));
                if (Convert.ToInt16(TXT_CHANGE.Text) < 0)
                {
                    Management.ShowMsg("จำนวนเงินไม่พอ");
                    TXT_CHANGE.Text = "0";
                    TXT_RECIEVE.SelectAll();
                }
                else
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void B_NEXT_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (TXT_ID.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_ID.Focus();
            }
            else if (TXT_NAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_NAME.Focus();
            }
            else if (TXT_SURNAME.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_SURNAME.Focus();
            }
            else if (TXT_TEL.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_TEL.Focus();
            }
            else if (TXT_EMAIL.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_EMAIL.Focus();
            }
            else if (TXT_ADDR.Text.Equals(""))
            {
                Management.ShowMsg(Management.IS_NULL);
                TXT_ADDR.Focus();
            }
            else
            {
                switch (status)
                { 
                    case DBStatus.Insert:
                        tabControl1.TabPages.Add(tabPage3);
                        tabPage3.Controls.Add(panel2);
                        tabControl1.SelectTab(2);
                        CBO_PACKAGE.SelectedIndex = -1;//initial package
                        CBO_PACKAGE.SelectedIndex = 0;//Refresh package
                        CBO_ACCOUNT_CODE.SelectedValue = "0002";
                        //TXT_RECIEVE.Focus();
                        break;
                    case DBStatus.Member:
                        tabControl1.TabPages.Add(tabPage3);
                        tabPage3.Controls.Add(panel2);
                        tabControl1.SelectTab(2);
                        CBO_PACKAGE.SelectedIndex = -1;//initial package
                        CBO_PACKAGE.SelectedIndex = 0;//Refresh package
                        CBO_ACCOUNT_CODE.SelectedValue = "0003";
                        //TXT_RECIEVE.Focus();
                        break;
                    case DBStatus.Update:
                        switch (CBO_TYPE.SelectedValue.ToString())
                        {
                            case "0001":
                                //เป็นลูกค้าทั่วไป ให้บันทึกข้อมูล
                                try
                                {
                                    CMember customer = new CMember
                                    {
                                        ID = TXT_ID.Text,
                                        Type = CBO_TYPE.SelectedValue.ToString(),
                                        Sex = CBO_SEX.SelectedValue.ToString(),
                                        Name = TXT_NAME.Text,
                                        Surname = TXT_SURNAME.Text,
                                        Age = Convert.ToInt16(ND_AGE.Value),
                                        Address = TXT_ADDR.Text,
                                        Mobile = TXT_TEL.Text,
                                        Email = TXT_EMAIL.Text,
                                        Nation = CBO_NATION.SelectedValue.ToString(),
                                        StartDate = DateTime.Now,
                                        EndDate = DateTime.Now.AddYears(Convert.ToInt16(NUD_YEAR.Value)),
                                        KEYDATE = DateTime.Now,
                                        KEYUSER = Authorize.getUser(),
                                        LASTEDIT = DateTime.Now,
                                        FLAG = Management.Update//Update
                                    };
                                    if (DALMember.manageMaster(customer) > 0)
                                    {
                                        Management.ShowMsg(Management.UPDATE_SUCCESS);
                                        initial();
                                        search();
                                    }
                                }
                                catch (SqlException ex)
                                {
                                    Management.ShowMsg(ex.Message);
                                }
                                break;
                            case "0002":
                                //ถ้าเป็นสมาชิกทั่วไปให้ อยู่แล้วให้ next เพื่อปรับปรุงรายการ 
                                Console.WriteLine(" เรียกใช้ store manageMemberPayment เพื่อดึงข้อมูล package ออกมา");
                                tabControl1.TabPages.Add(tabPage3);
                                tabPage3.Controls.Add(panel2);
                                tabControl1.SelectTab(2);
                                CBO_PACKAGE.SelectedIndex = -1;//initial package
                                CBO_PACKAGE.SelectedIndex = 0;//Refresh package
                                CBO_ACCOUNT_CODE.SelectedValue = "0002";
                                try
                                {
                                    List<CMemberPayment> lcp = DALMember.getMemberPackage(
                                        new CMemberPayment
                                        {
                                            ID = dataGridView2.CurrentRow.Cells[1].Value.ToString(),
                                            Type = "",
                                            Sex = "",
                                            Name = "",
                                            Surname = "",
                                            Age = 0,
                                            Address = "",
                                            Mobile = "",
                                            Email = "",
                                            Nation = "",
                                            StartDate = DateTime.Now,
                                            EndDate = DateTime.Now,
                                            /////////////////
                                            Therapist = "",
                                            Package = "",
                                            ////////////////
                                            AccountCode = "",
                                            Amount = 0,
                                            Discount = 0,
                                            RUNNING = 0,
                                            PaymentType = "",
                                            Slip = "",
                                            Bank = "",
                                            ///////////////
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = "2"
                                        }
                                    );
                                    if (lcp.Count > 0)
                                    {

                                        checkBox1.Checked = (lcp[0].Therapist.Equals("0000")) ? true : false;
                                        CBO_THERAPIST.SelectedValue = lcp[0].Therapist;
                                        CBO_PACKAGE.SelectedValue = lcp[0].Package;
                                        CBO_CASH_TYPE.SelectedValue = lcp[0].PaymentType;
                                        CBO_BANK.SelectedValue=lcp[0].Bank;
                                        TXT_SLIP.Text = lcp[0].Slip;
                                        CBO_ACCOUNT_CODE.SelectedValue = lcp[0].AccountCode;
                                        TXT_TOTAL.Text = lcp[0].Amount.ToString();
                                        TXT_DISCOUNT.Text = lcp[0].Discount.ToString();
                                        TXT_RECIEVE.Text = (lcp[0].Amount - lcp[0].Discount).ToString();

                                    }
                                }
                                catch (Exception ex)
                                {
                                    Management.ShowMsg(ex.Message);
                                }
                                break;
                        }
                        break;
                    case DBStatus.Delete:
                        try
                        {
                            CMember customer = new CMember
                            {
                                ID = TXT_ID.Text,
                                Type = CBO_TYPE.SelectedValue.ToString(),
                                Sex = CBO_SEX.SelectedValue.ToString(),
                                Name = TXT_NAME.Text,
                                Surname = TXT_SURNAME.Text,
                                Age = Convert.ToInt16(ND_AGE.Value),
                                Address = TXT_ADDR.Text,
                                Mobile = TXT_TEL.Text,
                                Email = TXT_EMAIL.Text,
                                Nation = CBO_NATION.SelectedValue.ToString(),
                                StartDate = DateTime.Now,
                                EndDate = DateTime.Now.AddYears(Convert.ToInt16(NUD_YEAR.Value)),
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.Delete//Delete
                            };
                            if (DALMember.manageMaster(customer) > 0)
                            {
                                Management.ShowMsg(Management.DELETE_SUCCESS);
                                initial();
                                search();
                            }
                        }
                        catch (SqlException ex)
                        {
                            Management.ShowMsg(ex.Message);
                        }
                        break;
                }
            }
            TXT_DISCOUNT.Focus();
            Cursor = Cursors.Default;
        }

        private void CMD_SAVE_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if ((Convert.ToInt16(TXT_RECIEVE.Text)+Convert.ToInt16(TXT_DISCOUNT.Text)) >= Convert.ToInt16(TXT_TOTAL.Text))
            {
                try
                {
                        switch (status)
                        {
                            case DBStatus.Insert://สมัครสมาชิกใหม่
                                CMemberPayment customer1 = new CMemberPayment
                                {
                                    ID = TXT_ID.Text,
                                    Type = CBO_TYPE.SelectedValue.ToString(),
                                    Sex = CBO_SEX.SelectedValue.ToString(),
                                    Name = TXT_NAME.Text,
                                    Surname = TXT_SURNAME.Text,
                                    Age = Convert.ToInt16(ND_AGE.Value),
                                    Address = TXT_ADDR.Text,
                                    Mobile = TXT_TEL.Text,
                                    Email = TXT_EMAIL.Text,
                                    Nation = CBO_NATION.SelectedValue.ToString(),
                                    StartDate = DateTime.Now,
                                    EndDate = DateTime.Now.AddYears(Convert.ToInt16(NUD_YEAR.Value)),
                                    /////////////////
                                    Therapist = (checkBox1.Checked) ? "0000" : getTherapistList(),//CBO_THERAPIST.SelectedValue.ToString(),
                                    Package = CBO_PACKAGE.SelectedValue.ToString(),
                                    ////////////////
                                    AccountCode = CBO_ACCOUNT_CODE.SelectedValue.ToString(),
                                    Discount = Convert.ToDouble(TXT_DISCOUNT.Text),
                                    Amount = Convert.ToDouble(TXT_RECIEVE.Text) - Convert.ToDouble(TXT_CHANGE.Text),
                                    PaymentType = CBO_CASH_TYPE.SelectedValue.ToString(),
                                    Slip = TXT_SLIP.Text,
                                    Bank = CBO_BANK.SelectedValue.ToString(),
                                    RUNNING = 0,
                                    ///////////////
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = Management.Insert//Insert
                                };
                                if (DALMember.manageMemberPayment(customer1) > 0)
                                {
                                    checkPrint();
                                    Management.ShowMsg(Management.INSERT_SUCCESS);
                                    initial();
                                    search();
                                }
                                break;
                            case DBStatus.Update:
                                CMemberPayment customer3 = new CMemberPayment
                                {
                                    ID = TXT_ID.Text,
                                    Type = CBO_TYPE.SelectedValue.ToString(),
                                    Sex = CBO_SEX.SelectedValue.ToString(),
                                    Name = TXT_NAME.Text,
                                    Surname = TXT_SURNAME.Text,
                                    Age = Convert.ToInt16(ND_AGE.Value),
                                    Address = TXT_ADDR.Text,
                                    Mobile = TXT_TEL.Text,
                                    Email = TXT_EMAIL.Text,
                                    Nation = CBO_NATION.SelectedValue.ToString(),
                                    StartDate = DateTime.Now,
                                    EndDate = DateTime.Now.AddYears(Convert.ToInt16(NUD_YEAR.Value)),
                                    /////////////////
                                    Therapist = (checkBox1.Checked) ? "0000" : getTherapistList(),//CBO_THERAPIST.SelectedValue.ToString(),
                                    Package = dataGridView2.CurrentRow.Cells[1].Value.ToString(),
                                    
                                    ////////////////
                                    AccountCode = CBO_ACCOUNT_CODE.SelectedValue.ToString(),
                                    Discount = Convert.ToDouble(TXT_DISCOUNT.Text),
                                    Amount = Convert.ToDouble(TXT_TOTAL.Text),
                                    PaymentType = CBO_CASH_TYPE.SelectedValue.ToString(),
                                    Slip = TXT_SLIP.Text,
                                    Bank = CBO_BANK.SelectedValue.ToString(),
                                    RUNNING = 0,
                                    ///////////////
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = "3"//Insert
                                };
                                if (DALMember.manageMemberPayment(customer3) > 0)
                                {
                                    checkPrint();
                                    Management.ShowMsg(Management.UPDATE_SUCCESS);
                                    initial();
                                    search();
                                }
                                break;
                            case DBStatus.Member://ต่อ package หรือ ซื้อ package เพิ่ม
                                CMemberPayment customer2 = new CMemberPayment
                                {
                                    ID = TXT_ID.Text,
                                    Type = CBO_TYPE.SelectedValue.ToString(),
                                    Sex = CBO_SEX.SelectedValue.ToString(),
                                    Name = TXT_NAME.Text,
                                    Surname = TXT_SURNAME.Text,
                                    Age = Convert.ToInt16(ND_AGE.Value),
                                    Address = TXT_ADDR.Text,
                                    Mobile = TXT_TEL.Text,
                                    Email = TXT_EMAIL.Text,
                                    Nation = CBO_NATION.SelectedValue.ToString(),
                                    StartDate = DateTime.Now,
                                    EndDate = DateTime.Now.AddYears(Convert.ToInt16(NUD_YEAR.Value)),
                                    /////////////////
                                    Therapist = (checkBox1.Checked) ? "0001" : getTherapistList(),//CBO_THERAPIST.SelectedValue.ToString(),
                                    Package = CBO_PACKAGE.SelectedValue.ToString(),
                                    ////////////////
                                    AccountCode = CBO_ACCOUNT_CODE.SelectedValue.ToString(),
                                    Amount = Convert.ToDouble(TXT_TOTAL.Text),//Convert.ToDouble(TXT_RECIEVE.Text),
                                    Discount = Convert.ToDouble(TXT_DISCOUNT.Text),
                                    PaymentType = CBO_CASH_TYPE.SelectedValue.ToString(),
                                    Slip = TXT_SLIP.Text,
                                    Bank = CBO_BANK.SelectedValue.ToString(),
                                    RUNNING = 0,
                                    ///////////////
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = Management.Update//Insert
                                };
                                if (DALMember.manageMemberPayment(customer2) > 0)
                                {
                                    checkPrint();
                                    Management.ShowMsg(Management.INSERT_SUCCESS);
                                    initial();
                                    search();
                                }
                                break;
                        }
                }
                catch (SqlException ex)
                {
                    Management.ShowMsg(ex.Message);
                }
            }
            else {
                Management.ShowMsg(Management.NOT_ENOUGH_MONEY);
                TXT_RECIEVE.Focus();
            }
            Cursor = Cursors.Default;
        }

        private void B_CANCEL_Click(object sender, EventArgs e)
        {
            initial();
            search();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.dataGridView1.HitTest(e.X, e.Y);
            if (hti.RowIndex != -1)
            {
                if (hti.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView1.ClearSelection();
                    dataGridView1.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
                    index = hti.RowIndex;
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (e.RowIndex != -1)
            {
                index = e.RowIndex;
                bSelected = true;
                Close();
            }
            Cursor = Cursors.Default;
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            switch (e.ColumnIndex)
            { 
                case 0:
                    e.CellStyle.ForeColor = Color.Green;
                    break;
                case 7:
                    e.Value = Management.getDetail(MasterList.MCustomerType, e.Value.ToString());
                    break;
            }
            if (e.RowIndex % 2 == 1)
            {
                e.CellStyle.BackColor = Color.Lavender;
            }
        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (formStatus == FormStatus.Normal)
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ \n หรือ คลิ๊กขวาเพื่อ เพิ่ม,ปรับปรุง,ลบ ข้อมูล";
                }
                else
                {
                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ToolTipText = "Double Click เลือกรายการที่ต้องการ";
                }
            }
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0:
                case 1:
                    initial();
                    break;
            }
        }

        private void RD_ALL_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            if (dataGridView1.Rows.Count > 0)
            {
                //Reset Old select row
                dataGridView1.Rows[index].Selected = false;
                dataGridView1.Rows[0].Selected = true;
                txtSearch.Focus();
            }
        }

        private void CMS_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem b = (ToolStripMenuItem)sender;
            tabControl1.TabPages.Add(tabPage2);
            tabPage2.Controls.Add(panel1);
            tabControl1.SelectTab(1);
            groupBox2.Enabled = false;
            txtSearch.Text = "";
            switch (b.Name)
            {
                case "CMS_Insert":
                    status = DBStatus.Insert;
                    B_NEXT.Text = "ถัดไป";
                    CBO_TYPE.SelectedValue = "0002";
                    switch (CBO_TYPE1.SelectedValue.ToString())
                    { 
                        case "0001":
                            if (lists.Count > 0 && lists != null)
                            {
                                TXT_ID.Text = lists[index].ID;
                                CBO_SEX.SelectedValue = lists[index].Sex;
                                CBO_NATION.SelectedValue = lists[index].Nation;
                                TXT_NAME.Text = lists[index].Name;
                                TXT_SURNAME.Text = lists[index].Surname;
                                ND_AGE.Value = lists[index].Age;
                                TXT_ADDR.Text = lists[index].Address;
                                TXT_TEL.Text = lists[index].Mobile;
                                TXT_EMAIL.Text = lists[index].Email;
                            }
                            break;
                        case "0002":
                            break;
                    }

                    TXT_ID.Text = "";
                    TXT_ID.Focus();
                    break;
                case "CMS_Update":
                    status = DBStatus.Update;
                    switch (CBO_TYPE1.SelectedValue.ToString())
                    {
                        case "0001":
                            B_NEXT.Text = "บันทึก";
                            break;
                        case "0002":
                            B_NEXT.Text = "ถัดไป";
                            break;
                    }
                    TXT_ID.Text = lists[index].ID;
                    CBO_TYPE.SelectedValue = lists[index].Type;
                    CBO_SEX.SelectedValue = lists[index].Sex;
                    CBO_NATION.SelectedValue = lists[index].Nation;
                    TXT_NAME.Text = lists[index].Name;
                    TXT_SURNAME.Text = lists[index].Surname;
                    ND_AGE.Value = lists[index].Age;
                    TXT_ADDR.Text = lists[index].Address;
                    TXT_TEL.Text = lists[index].Mobile;
                    TXT_EMAIL.Text = lists[index].Email;
                    getPackageDetail(lists[index].ID);//show Detail
                    TXT_NAME.Select();
                    TXT_ID.ReadOnly = true;
                    break;
                case "CMS_Delete":
                    status = DBStatus.Delete;
                    B_NEXT.Text = "ลบ";
                    TXT_ID.Text = lists[index].ID;
                    CBO_TYPE.SelectedValue = lists[index].Type;
                    CBO_SEX.SelectedValue = lists[index].Sex;
                    CBO_NATION.SelectedValue = lists[index].Nation;
                    TXT_NAME.Text = lists[index].Name;
                    TXT_SURNAME.Text = lists[index].Surname;
                    ND_AGE.Value = lists[index].Age;
                    TXT_ADDR.Text = lists[index].Address;
                    TXT_TEL.Text = lists[index].Mobile;
                    TXT_EMAIL.Text = lists[index].Email;
                    B_NEXT.Focus();
                    TXT_ID.ReadOnly = true;
                    TXT_NAME.ReadOnly = true;
                    break;
                case "CMS_MEMBER":
                    status = DBStatus.Member;
                    B_NEXT.Text = "ถัดไป";
                    TXT_ID.Text = lists[index].ID;
                    //CBO_TYPE.SelectedValue = lists[index].Type;
                    CBO_TYPE.SelectedValue = "0002";
                    CBO_SEX.SelectedValue = lists[index].Sex;
                    CBO_NATION.SelectedValue = lists[index].Nation;
                    TXT_NAME.Text = lists[index].Name;
                    TXT_SURNAME.Text = lists[index].Surname;
                    ND_AGE.Value = lists[index].Age;
                    TXT_ADDR.Text = lists[index].Address;
                    TXT_TEL.Text = lists[index].Mobile;
                    TXT_EMAIL.Text = lists[index].Email;
                    TXT_NAME.Select();
                    TXT_ID.ReadOnly = true;
                    break;
            }
        }

        private void B_STYPE_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Name)
            {
                case "B_STYPE":
                    FrmSearch search = new FrmSearch(MasterList.MCustomerType, FormStatus.Search);
                    search.ShowDialog();
                    if (search.bSelected)
                    {
                        CBO_TYPE.SelectedValue = search.masters[search.index].ID;
                    }
                    break;
                case "B_SPACKAGE":
                    FrmPackage package = new FrmPackage("0002", FormStatus.Search);
                    package.ShowDialog();
                    if (package.bSelected)
                    {
                        CBO_PACKAGE.SelectedValue = package.lists[package.index].ID;
                    }
                    break;
                case "B_SEX":
                    FrmSearch search1 = new FrmSearch(MasterList.MSex, FormStatus.Search);
                    search1.ShowDialog();
                    if (search1.bSelected)
                    {
                        CBO_SEX.SelectedValue = search1.masters[search1.index].ID;
                    }
                    break;
                case "B_SNation":
                    FrmSearch search2 = new FrmSearch(MasterList.MNation, FormStatus.Search);
                    search2.ShowDialog();
                    if (search2.bSelected)
                    {
                        CBO_NATION.SelectedValue = search2.masters[search2.index].ID;
                    }
                    break;
                case "B_CASHTYPE":
                    FrmSearch search3 = new FrmSearch(MasterList.MPaymentType, FormStatus.Search);
                    search3.ShowDialog();
                    if (search3.bSelected)
                    {
                        CBO_CASH_TYPE.SelectedValue = search3.masters[search3.index].ID;
                    }
                    break;
                case "B_BANK":
                    FrmSearch search4 = new FrmSearch(MasterList.MBank, FormStatus.Search);
                    search4.ShowDialog();
                    if (search4.bSelected)
                    {
                        CBO_BANK.SelectedValue = search4.masters[search4.index].ID;
                    }
                    break;
                case "B_STHERAPIST":
                    //6 คือ flag บอกว่าให้ แสดง เฉพาะ ชื่อหมอนวด (รหัสหมอนวด 0003)
                    FrmEmployee employee1 = new FrmEmployee("6", FormStatus.Search);
                    employee1.ShowDialog();
                    if (employee1.bSelected)
                    {
                        CBO_THERAPIST.SelectedValue = employee1.lists[employee1.index].ID;
                    }
                    break;
                case "B_PAY_TYPE":
                    FrmSearch search5 = new FrmSearch(MasterList.MAccountCode, FormStatus.Search);
                    search5.ShowDialog();
                    if (search5.bSelected)
                    {
                        CBO_ACCOUNT_CODE.SelectedValue = search5.masters[search5.index].ID;
                    }
                    break;
            }
        }

        private void CBO_PACKAGE_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get Price of package
            if (CBO_PACKAGE.SelectedValue != null)
            {
                TXT_TOTAL.Text = DALPackage.getList(new CPackage
                                {
                                    ID = CBO_PACKAGE.SelectedValue.ToString(),
                                    DETAIL = txtSearch.Text,
                                    Hour = "1",
                                    Price = 1,
                                    MoneyUnit = "THB",
                                    MassageType = "0",
                                    PerCourse = 1,
                                    CustomerType = "",
                                    StartDate = DateTime.Now,
                                    EndDate = DateTime.Now,
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = Management.SearchByID//Searh Flag
                                }
                            )[0].Price.ToString();
            }

        }

        private void search()
        {
            lists = DALMember.getList(new CMember
                {
                    ID = "",
                    Type = (this.group.Equals("")) ? CBO_TYPE1.SelectedValue.ToString() : this.group,//แสดงเฉพาะสมาชิก
                    Sex = "",
                    Title = "",
                    Name = "",
                    Surname = "",
                    BirthDay = DateTime.Now,
                    Address = "",
                    Mobile = "",
                    Email = "",
                    Nation = "",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    KEYDATE = DateTime.Now,
                    KEYUSER = Authorize.getUser(),
                    LASTEDIT = DateTime.Now,
                    FLAG = Management.SearchByGroup//Insert
                });
            //Check Have Data!!
            if (lists.Count == 0)
            {
                dataGridView1.DataSource = null;
                if (bSearch)
                {
                    Management.ShowMsg(Management.NO_FOUND_DATA);
                }
            }
            else
            {
                dataGridView1.DataSource = lists;
            }
        }

        private void initial()
        {
            groupBox2.Enabled = true;
            TXT_ID.ReadOnly = false;
            TXT_NAME.ReadOnly = false;
            TXT_ID.Text = "";
            TXT_ID.Text = "";
            TXT_NAME.Text = "";
            TXT_SURNAME.Text = "";
            TXT_ADDR.Text = "";
            TXT_TEL.Text = "";
            TXT_EMAIL.Text = "";
            txtSearch.Text = "";
            txtSearch.Focus();
            ND_AGE.Value = 1;
            NUD_YEAR.Value = 1;
            TXT_SLIP.Text = "";
            CB_PRINT_1.Checked = false;
            CB_PRINT_2.Checked = false;
            TXT_TOTAL.Text = "0";
            TXT_RECIEVE.Text = "0";
            TXT_CHANGE.Text = "0";
            tabControl1.TabPages.RemoveAt(1);
            if (tabControl1.TabPages.Count == 2)
            {
                tabControl1.TabPages.RemoveAt(1);
            }
            //Customer Type
            CBO_TYPE.DataSource = DALMaster.getList(MasterList.MCustomerType,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = txtSearch.Text,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //Customer Type
            CBO_TYPE1.DataSource = DALMaster.getList(MasterList.MCustomerType,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = txtSearch.Text,
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //Package
            CBO_PACKAGE.DataSource = DALPackage.getList(new CPackage
                                        {
                                            ID = txtSearch.Text,
                                            DETAIL = txtSearch.Text,
                                            Hour = "1",
                                            Price = 1,
                                            MoneyUnit = "THB",
                                            MassageType = "1",//1 คือ แสดง package ของ member
                                            PerCourse = 1,
                                            CustomerType = "0002",
                                            StartDate = DateTime.Now,
                                            EndDate = DateTime.Now,
                                            KEYDATE = DateTime.Now,
                                            KEYUSER = Authorize.getUser(),
                                            LASTEDIT = DateTime.Now,
                                            FLAG = "7"//Searh Flag (แสดงเฉพาะที่เป็น package ของ member)
                                        }
                                    );
            //Package 1
            CBO_PACKAGE1.DataSource = DALPackage.getList(new CPackage
            {
                ID = txtSearch.Text,
                DETAIL = txtSearch.Text,
                Hour = "1",
                Price = 1,
                MoneyUnit = "THB",
                MassageType = "1",//1 คือ แสดง package ของ member
                PerCourse = 1,
                CustomerType = "0002",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                KEYDATE = DateTime.Now,
                KEYUSER = Authorize.getUser(),
                LASTEDIT = DateTime.Now,
                FLAG = "7"//Searh Flag (แสดงเฉพาะที่เป็น package ของ member)
            }
                                    );
            //Title
            CBO_SEX.DataSource = DALMaster.getList(MasterList.MSex,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            CBO_NATION.DataSource = DALMaster.getList(MasterList.MNation,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //รูปแบบการชำเงิน (เงินสด,บัตรเครดิต)
            CBO_CASH_TYPE.DataSource = DALMaster.getList(MasterList.MPaymentType,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //ธนาคาร
            CBO_BANK.DataSource = DALMaster.getList(MasterList.MBank,
                            new CMaster
                            {
                                ID = "",
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //ประเภทการชำระ (ค่าสมัครสมาชิก,อื่น ๆ )
            CBO_ACCOUNT_CODE.DataSource = DALMaster.getList(MasterList.MAccountCode,
                            new CMaster
                            {
                                ID = "",//กำหนดให้จ่ายเงินค่าสมาลิก
                                DETAIL = "",
                                KEYDATE = DateTime.Now,
                                KEYUSER = Authorize.getUser(),
                                LASTEDIT = DateTime.Now,
                                FLAG = Management.SearchAll//Searh Flag
                            }
                            );
            //Employee
            CBO_THERAPIST.DataSource = DALEmployee.getList(new CEmployee
            {
                ID = "",
                Type = "0003",
                Sex = "",
                Title = "",
                Name = "",
                Surname = "",
                NickName = "",
                Nation = "",
                BirthDay = DateTime.Now,
                Address = "",
                Mobile = "",
                Email = "",
                Status = "",
                Period = "",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                KEYDATE = DateTime.Now,
                KEYUSER = Authorize.getUser(),
                LASTEDIT = DateTime.Now,
                FLAG = Management.SearchByGroup//Searh Flag
            }
                                    );
            //Initial Cash Type
            CBO_BANK.Enabled = false;
            TXT_SLIP.Enabled = false;
            CBO_THERAPIST.Enabled = false;
            B_STHERAPIST.Enabled = false;
            dataGridView3.Enabled = false;
            B_ADDTherapist.Enabled = false;
            BRemoveTherapist.Enabled = false;
            CBO_TYPE1.SelectedIndex = -1;
            CBO_TYPE1.SelectedIndex = 1;
            checkBox1.Checked = true;
            CBO_THERAPIST.Enabled = false;
            B_STHERAPIST.Enabled = false;
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Set User Permission
            CMS_Insert.Enabled = (formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[0])&& (CBO_TYPE1.SelectedValue.Equals("0002"))? true : false;
            CMS_Update.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[1]) && (dataGridView1.Rows.Count > 0)) ? true : false;
            CMS_Delete.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[2]) && (dataGridView1.Rows.Count > 0)) ? true : false;
            CMS_MEMBER.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[2]) && (dataGridView1.Rows.Count > 0)) ? true : false;
            CMD_PRINT.Enabled = ((formStatus == FormStatus.Normal) && (Authorize.GetAuthorize(this.Name)[2]) && (dataGridView1.Rows.Count > 0)) ? true : false;
        }

        private void checkPrint()
        {
            List<CRpt02> list = new List<CRpt02>();
            CRpt02 crpt02 = new CRpt02
            {
                Name = TXT_NAME.Text,
                Surname = TXT_SURNAME.Text,
                ID = DALCustomerTransaction.getTransactionID().ToString(),
                Address = TXT_ADDR.Text,
                Type = CBO_TYPE.SelectedValue.ToString(),
                ////
                RUNNING = 1,
                DETAIL = CBO_PACKAGE.Text,
                Amount = Convert.ToDouble(TXT_TOTAL.Text)
            };
            list.Add(crpt02);

            //Print Invoice
            if (CB_PRINT_1.Checked)
            {
                Rpt02 rpt02 = new Rpt02();
                rpt02.SetDataSource(list);
                rpt02.PrintToPrinter(1,false,1,1);
            }

            //Print Member List Table
            if (CB_PRINT_2.Checked)
            {
                Rpt01 rpt01 = new Rpt01();
                rpt01.PrintToPrinter(1, false, 1, 1);
            }
        }

        private void CBO_THERAPIST_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBO_THERAPIST.SelectedValue != null)
            {
                checkBox1.Checked = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            switch (checkBox1.Checked)
            { 
                case true:
                    CBO_THERAPIST.Enabled = false;
                    B_STHERAPIST.Enabled = false;
                    dataGridView3.Enabled = false;
                    B_ADDTherapist.Enabled = false;
                    BRemoveTherapist.Enabled = false;
                    break;
                case false:
                    CBO_THERAPIST.Enabled = true;
                    B_STHERAPIST.Enabled = true;
                    dataGridView3.Enabled = true;
                    B_ADDTherapist.Enabled = true;
                    BRemoveTherapist.Enabled = true;
                    break;
            }
        }

        private void CMD_PRINT_Click(object sender, EventArgs e)
        {
            //Print Invoice
            Rpt02 rpt02 = new Rpt02();
            rpt02.SetDataSource(DALMember.printInvoice(dataGridView1.Rows[index].Cells[1].Value.ToString()));
            rpt02.PrintToPrinter(1, false, 1, 1);
        }

        private void CBO_PACKAGE1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBO_PACKAGE1.SelectedValue != null)
            {
                dataGridView1.DataSource =
                        DALMember.getList(new CMember
                        {
                            ID = "",
                            Type = "",
                            Sex = "",
                            Title = "",
                            Name = "",
                            Surname = "",
                            BirthDay = DateTime.Now,
                            Address = "",
                            Mobile = CBO_PACKAGE1.SelectedValue.ToString(),//Flag to select data
                            Email = "",
                            Nation = "",
                            StartDate = DateTime.Now,
                            EndDate = DateTime.Now,
                            KEYDATE = DateTime.Now,
                            KEYUSER = Authorize.getUser(),
                            LASTEDIT = DateTime.Now,
                            FLAG = "8"
                        });
            }
        }

        private void CBO_TYPE1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CBO_TYPE1.SelectedValue != null)
            {
                switch (CBO_TYPE1.SelectedValue.ToString())
                { 
                    case "0001"://Gerneral
                        CBO_PACKAGE1.Enabled = false;
                        break;
                    case "0002"://Member
                        CBO_PACKAGE1.Enabled = true;
                        break;
                }
                search();
            }
        }

        private void getPackageDetail(string _id)
        {
            List<CPackage> list = DALPackage.getList1(new CPackage
                                {
                                    ID = _id,
                                    DETAIL = "",
                                    Hour = "1",
                                    Price = 1,
                                    MoneyUnit = "THB",
                                    MassageType = "0", // 0 คือแสดงเฉพาะ package ทั่วไป 1 คือ แสดงเฉพาะ package ที่เป็นของสมาชิก
                                    PerCourse = 1,
                                    CustomerType = "",
                                    StartDate = DateTime.Now,
                                    EndDate = DateTime.Now,
                                    KEYDATE = DateTime.Now,
                                    KEYUSER = Authorize.getUser(),
                                    LASTEDIT = DateTime.Now,
                                    FLAG = "9"//--แสดงข้อมูล package ของสมาชิกแต่ละคน โดยแสดงเฉพาะที่ยังใช้ member ไม่หมด
                                });
            dataGridView2.DataSource = list;
        }

        private void B_ADDTherapist_Click(object sender, EventArgs e)
        {
            if (!isSame(CBO_THERAPIST.SelectedValue.ToString()))
            {
                dataGridView3.Rows.Add(CBO_THERAPIST.SelectedValue, CBO_THERAPIST.Text);
            }
            else
            {
                MessageBox.Show("ข้อมูลซ้ำ");
                textBox1.Focus();
            }
        }

        private Boolean isSame(string id)
        {
            Boolean isSame = false;

            for (int i = 0; i < dataGridView3.Rows.Count; i++)
            {
                if (dataGridView3.Rows[i].Cells[0].Value.ToString().Equals(id))
                {
                    isSame = true;
                    break;
                }
            }
            return isSame;
        }

        private void BRemoveTherapist_Click(object sender, EventArgs e)
        {
            if (dataGridView3.Rows.Count > 0)
            {
                dataGridView3.Rows.RemoveAt(dataGridView3.CurrentRow.Index);
            }
        }

        private string getTherapistList()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dataGridView3.Rows.Count; i++)
            {
                sb.Append(dataGridView3.Rows[i].Cells[0].Value.ToString());
                sb.Append(",");
            }
            return sb.ToString().Substring(0, sb.ToString().Length - 1);
        }
    }
}
