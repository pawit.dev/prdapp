﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Rakuten.DAL;
using Rakuten.Report;
using Rakuten.Structure;

namespace Rakuten
{
    public enum ReceiptType 
    {
        Single,
        Many
    }
    public partial class FrmPrintReceipt : Form
    {
        private int id;
        private StringBuilder sb;
        private ReceiptType status;
        private List<CRpt02> list = new List<CRpt02>();

        public FrmPrintReceipt()
        {
            InitializeComponent();
        }

        public FrmPrintReceipt(ReceiptType rType,StringBuilder _sb)
        {
            InitializeComponent();
            this.sb = _sb;
            this.status = rType;
            
        }

        private void FrmPrintReceipt_Load(object sender, EventArgs e)
        {
            Rpt02 rpt02 = new Rpt02();
            switch (status)
            { 
                case ReceiptType.Single:
                    this.id = Convert.ToInt16(sb.ToString());
                    rpt02.SetDataSource(DALInvoice.getInvoice(id));
                    break;
                case ReceiptType.Many:
                    string[] data = sb.ToString().Split(',');
                    for (int i = 0; i < data.Length; i++)
                    {
                        if (!data[i].Equals(""))
                        {
                            list.Add(DALInvoice.getInvoice(Convert.ToInt16(data[i]))[0]);
                        }
                    }
                    rpt02.SetDataSource(list);
                    break;
            }
            rpt02.SetParameterValue("PInvoice", "ต้นฉบับใบกำกับภาษี/ใบเสร็จรับเงิน");
            crystalReportViewer1.ReportSource = rpt02;
            crystalReportViewer1.Zoom(70);
            CMD_PRINT.Focus();
        }

        private void CMD_PRINT_Click(object sender, EventArgs e)
        {
            checkPrint();
        }

        private void checkPrint()
        {
            string pStatus = (RD_PRINT1.Checked) ? "1" : (RD_PRINT2.Checked) ? "2" : "1";
            switch (pStatus)
            {
                case "2"://Nomal Reciept
                    Rpt02 rpt02 = new Rpt02();
                    switch (status)
                    {
                        case ReceiptType.Single:
                            rpt02.SetDataSource(DALInvoice.getInvoice(this.id));
                            break;
                        case ReceiptType.Many:
                            rpt02.SetDataSource(list);
                            break;
                    }
                    rpt02.SetParameterValue("PInvoice", "ต้นฉบับใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt02.PrintToPrinter(1, false, 1, 1);
                    rpt02.SetParameterValue("PInvoice", "สำเนาใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt02.PrintToPrinter(1, false, 1, 1);
                    break;
                case "1"://Inc VAT
                    Rpt03 rpt03 = new Rpt03();
                    switch (status)
                    {
                        case ReceiptType.Single:
                            rpt03.SetDataSource(DALInvoice.getInvoice(this.id));
                            break;
                        case ReceiptType.Many:
                            rpt03.SetDataSource(list);
                            break;
                    }
                    rpt03.SetParameterValue("PInvoice", "ต้นฉบับใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt03.PrintToPrinter(1, false, 1, 1);
                    rpt03.SetParameterValue("PInvoice", "สำเนาใบกำกับภาษี/ใบเสร็จรับเงิน");
                    rpt03.PrintToPrinter(1, false, 1, 1);
                    break;
            }
        }

        private void RD_PRINT1_CheckedChanged(object sender, EventArgs e)
        {
            string pStatus = (RD_PRINT1.Checked) ? "1" : (RD_PRINT2.Checked) ? "2" : "1";
            switch (pStatus)
            {
                case "2"://Nomal Reciept
                    Rpt02 rpt02 = new Rpt02();
                    switch (status)
                    {
                        case ReceiptType.Single:
                            rpt02.SetDataSource(DALInvoice.getInvoice(this.id));
                            break;
                        case ReceiptType.Many:
                            rpt02.SetDataSource(list);
                            break;
                    }
                    rpt02.SetParameterValue("PInvoice", "ต้นฉบับใบกำกับภาษี/ใบเสร็จรับเงิน");
                    crystalReportViewer1.ReportSource = rpt02;
                    crystalReportViewer1.Zoom(70);
                    break;
                case "1"://Inc VAT
                    Rpt03 rpt03 = new Rpt03();
                    switch (status)
                    {
                        case ReceiptType.Single:
                            rpt03.SetDataSource(DALInvoice.getInvoice(this.id));
                            break;
                        case ReceiptType.Many:
                            rpt03.SetDataSource(list);
                            break;
                    }
                    rpt03.SetParameterValue("PInvoice", "ต้นฉบับใบกำกับภาษี/ใบเสร็จรับเงิน");
                    crystalReportViewer1.ReportSource = rpt03;
                    crystalReportViewer1.Zoom(70);
                    break;
            }
        }
    }
}
