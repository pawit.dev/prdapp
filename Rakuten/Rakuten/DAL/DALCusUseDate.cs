﻿using System;
using System.Collections.Generic;
using System.Text;
using Rakuten.Structure;
using System.Data.SqlClient;
using System.Data;

namespace Rakuten.DAL
{
    public class DALCusUseDate
    {
        public static List<CCusUseDate> getCCusUseDate(string id,string type, string flag)
        {
            List<CCusUseDate> cuss = new List<CCusUseDate>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("getCustomerUseDate", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inID", id));
                cmd.Parameters.Add(new SqlParameter("@intype", type));
                cmd.Parameters.Add(new SqlParameter("@inFlag", flag));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    CCusUseDate cus = new CCusUseDate
                    {
                        MemberID = Convert.ToString(dr["RmemberID"]),
                        Name = Convert.ToString(dr["Mname"]),
                        Surname = Convert.ToString(dr["Msurname"]),
                        TherapistName = Convert.ToString(dr["RTherapistName"]),
                        TherapistReservName = Convert.ToString(dr["RTherapistRevName"]),
                        useDate = Convert.ToDateTime(dr["RDate"]),
                        useTime = Convert.ToDateTime(dr["RTime"])
                    };
                    cuss.Add(cus);
                }
                dr.Close();
            }
            return cuss;
        }
    }
}
