﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Rakuten.DAL
{
    public class DALConversion
    {
        public static int manageMaster(
                string inRmemberID,
                string inRTherapistID,
                string inRTherapistRevID,
                DateTime inRDate,
                DateTime inRTime,
                string inRPackage,
                string inRSpecialPackage,
                double inSAmount,
                string inSPayType,
                string inSBank,
                double inSDiscount
            )
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("ConvertTransaction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inRmemberID", inRmemberID));
                cmd.Parameters.Add(new SqlParameter("@inRTherapistID", inRTherapistID));
                cmd.Parameters.Add(new SqlParameter("@inRTherapistRevID", inRTherapistRevID));
                cmd.Parameters.Add(new SqlParameter("@inRDate", inRDate));
                cmd.Parameters.Add(new SqlParameter("@inRTime", inRTime));
                cmd.Parameters.Add(new SqlParameter("@inRPackage", inRPackage));
                cmd.Parameters.Add(new SqlParameter("@inRSpecialPackage", inRSpecialPackage));
                cmd.Parameters.Add(new SqlParameter("@inSAmount", inSAmount));
                cmd.Parameters.Add(new SqlParameter("@inSPayType", inSPayType));
                cmd.Parameters.Add(new SqlParameter("@inSBank", inSBank));
                cmd.Parameters.Add(new SqlParameter("@inSDiscount", inSDiscount));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
        //Delete Transaction Data
        public static int ResetTransData(DateTime begin, DateTime end)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DeleteTransactionData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@begin", begin));
                cmd.Parameters.Add(new SqlParameter("@end", end));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;  
        }
    }
}
