﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;

namespace Rakuten.DAL
{
    class DALTimePeriod
    {
        //Get Data
        public static List<CMaster> getList(CMaster _master)
        {
            List<CMaster> lists = new List<CMaster>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageTimePeriod", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _master.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inStart", _master.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inEnd", _master.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CMaster master = new CMaster
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["DNo"]),
                        DETAIL = Convert.ToString(dr["DDetail"]),
                        StartDate = Convert.ToDateTime(dr["MPStart"]),
                        EndDate = Convert.ToDateTime(dr["MPEnd"]),
                        KEYDATE = Convert.ToDateTime(dr["MKeyDate"]),
                        LASTEDIT = Convert.ToDateTime(dr["MLastEdit"]),
                        KEYUSER = Convert.ToString(dr["MKeyUser"])
                    };
                    running++;
                    lists.Add(master);
                }
                dr.Close();
            }
            return lists;
        }
        //Insert,Delete,Update
        public static int manageMaster(CMaster _master)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageTimePeriod", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _master.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inStart", _master.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inEnd", _master.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
