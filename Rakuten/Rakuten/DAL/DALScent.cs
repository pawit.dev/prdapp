﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;
namespace Rakuten.DAL
{
    public class DALScent
    {
        //Get Data
        public static List<CScent> getList(CScent _master)
        {
            List<CScent> lists = new List<CScent>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMScent", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _master.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inMin", _master.Min));
                cmd.Parameters.Add(new SqlParameter("@inMax", _master.Max));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CScent master = new CScent
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["DNO"]),
                        DETAIL = Convert.ToString(dr["DDetail"]),
                        Min = Convert.ToInt16(dr["Dmin"]),
                        Max = Convert.ToInt16(dr["Dmax"]),
                        KEYDATE = Convert.ToDateTime(dr["MKeyDate"]),
                        LASTEDIT = Convert.ToDateTime(dr["MLastEdit"]),
                        KEYUSER = Convert.ToString(dr["MKeyUser"])
                    };
                    running++;
                    lists.Add(master);
                }
                dr.Close();
            }
            return lists;
        }
        //Insert,Delete,Update
        public static int manageMaster(CScent _master)
        {
            int i = 0;
            using(SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMScent", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _master.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inMin", _master.Min));
                cmd.Parameters.Add(new SqlParameter("@inMax", _master.Max));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }

        //Transaction
        //Get Data
        public static List<CScent> getList1(CScent _master)
        {
            List<CScent> lists = new List<CScent>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMScentTransaction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inSID", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inSCtype", _master.ScentType));
                cmd.Parameters.Add(new SqlParameter("@inSCount", _master.Count));
                cmd.Parameters.Add(new SqlParameter("@inStype", _master.ProgramType));
                cmd.Parameters.Add(new SqlParameter("@inkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CScent master = new CScent
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["DNO"]),
                        DETAIL = Convert.ToString(dr["DDetail"]),
                        Count = Convert.ToInt16(dr["Remain"]),
                        Min = Convert.ToInt16(dr["DMin"]),
                        Remark = "-"
                    };
                    running++;
                    lists.Add(master);
                }
                dr.Close();
            }
            return lists;
        }
        public static int manageMaster1(CScent _master)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageMScentTransaction", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inSID", _master.ID));
                cmd.Parameters.Add(new SqlParameter("@inSCtype", _master.ScentType));
                cmd.Parameters.Add(new SqlParameter("@inSCount", _master.Count));
                cmd.Parameters.Add(new SqlParameter("@inStype", _master.ProgramType));
                cmd.Parameters.Add(new SqlParameter("@inkeyUser", _master.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _master.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
