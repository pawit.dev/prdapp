﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using Rakuten.DAL;
using Rakuten.Structure;
using Rakuten.Utility;
using System.Windows.Forms;
using System.Text;

namespace Rakuten.DAL
{
    public enum ReportType
    {
        Report01,   //(สรุปลูกค้า)
        Report02,   //(สรุปค่ามือพนักงานรายวัน)
        Report03,   //(สรุป STOCK)
        Report04,   //
        Report05,   //(สรุปการจองหมอนวดแยกตามเดือน)
        Report06,   //(รายงานจำนวนเชื้อชาตินี้เลือก Package การนวดแบบไหน แยกตามเพศ)
        Report07,   //(รายงานช่วงอายุของสมาชิก แยกตามชาย ญ)
        Report08,   //
        Report09,   //(สรุปประเภทการชำระเงิน)
        Report10,   //(สรุปยอดขายสมาชิก)
        Report11,   //(สรุปการจำนวนครั้งในการใช้บริการ ช/ญ. แยกตามประเภทสมาชิก)
        Report12,   //(สรุปการจำนวนครั้งในการใช้บริการ ช/ญ. แยกตามประเภทบริการ)
        Report13,   //
        Report14,   //(สรุป วันลาพนักงาน)
        Report15,   //(Therapist Income)
        Report16,   //(Therapist JOB)
        Report17,   //สมาชิกมาใช้บริการ (Member)
        Report18    //สมาชิกมาใช้บริการ (Specail Package)
    }
    public class DALReport
    {
        //(สรุปลูกค้า)
        public static List<CReport1> getReport1(DateTime sDate,DateTime eDate)
        {
            List<CReport1> reports = new List<CReport1>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "1"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport1 report = new CReport1
                    {
                        RUNNING = running,
                        Sex = Convert.ToString(dr["Sex"]),
                        Name = Convert.ToString(dr["Mname"]),
                        Surname = Convert.ToString(dr["Msurname"]),
                        Type = Convert.ToString(dr["MassageType"]),
                        StartDate = Convert.ToDateTime(dr["RTime"]),
                        EndDate = Convert.ToDateTime(dr["RTime"]).AddHours(Convert.ToDouble(dr["Hour"])),
                        Amount = Convert.ToString(dr["MType"]).Equals("General") ? Convert.ToDouble(dr["SAmount"]) : 0,
                        PayType = Convert.ToString(dr["PayType"]),
                        CustomerType = Convert.ToString(dr["MType"]),
                        Therapist = Convert.ToString(dr["Therapist"]),
                        TherapistPrice = Convert.ToDouble(dr["TherapistCost"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุปค่ามือพนักงานรายวัน)
        public static List<CReport2> getReport2(DateTime sDate, DateTime eDate)
        {
            List<CReport2> reports = new List<CReport2>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "2"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport2 report = new CReport2
                    {
                        RUNNING = Convert.ToInt16(dr["ecount"]),
                        CheckIN = "",//Convert.ToString(dr["EStart"]),
                        CheckOUT = "",//Convert.ToString(dr["EEnd"]),
                        //Remark = Convert.ToString(dr["ID"]),
                        ID = Convert.ToString(dr["TherapistID"]),
                        NickName = Convert.ToString(dr["tname"]),
                        DETAIL = "",//Convert.ToString(dr["DDetail"]),
                        Late = Convert.ToDouble(dr["Emplatetime"]),
                        GuestReservation = Convert.ToInt16(dr["GuestReserv"]),
                        Income = Convert.ToString(dr["Income"]),
                        SoldMember = Convert.ToString(dr["soldmember"]),
                        Fine = Convert.ToString(dr["find"]),
                        ReMain = Convert.ToString(dr["Remain"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุป STOCK)
        public static List<CReport3> getReport3(DateTime sDate, DateTime eDate)
        {
            List<CReport3> reports = new List<CReport3>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "3"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport3 report = new CReport3
                    {
                        RUNNING = running,
                        DETAIL = Convert.ToString(dr["SCENT_DESC"]),
                        REC = Convert.ToInt16(dr["REC"]),
                        PAY = Convert.ToInt16(dr["OUT"]),
                        REMAIN = Convert.ToInt16(dr["REMAIN"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุปการจองหมอนวดแยกตามเดือน)
        public static List<CReport5> getReport5(DateTime sDate, DateTime eDate)
        {
            List<CReport5> reports = new List<CReport5>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "5"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport5 report = new CReport5
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["SID"]),
                        NickName = Convert.ToString(dr["EmpName"]),
                        StartDate = Convert.ToDateTime(dr["RDate"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(รายงานจำนวนเชื้อชาตินี้เลือก Package การนวดแบบไหน แยกตามเพศ)
        public static List<CReport6> getReport6(DateTime sDate, DateTime eDate)
        {
            List<CReport6> reports = new List<CReport6>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "6"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport6 report = new CReport6
                    {
                        RUNNING = running,
                        Package = Convert.ToString(dr["MPackage"]),
                        Nation = Convert.ToString(dr["Mnational"]),
                        ID = Convert.ToString(dr["MID"]),
                        Sex = Convert.ToString(dr["MSex"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(รายงานช่วงอายุของสมาชิก แยกตามชาย ญ)
        public static List<CReport7> getReport7(DateTime sDate, DateTime eDate)
        {
            List<CReport7> reports = new List<CReport7>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "7"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport7 report = new CReport7
                    {
                        RUNNING = running,

                        ID = Convert.ToString(dr["PERIOD"]),
                        Sex = Convert.ToString(dr["MSex"]),
                        Name = Convert.ToString(dr["MID"]),
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(รายงานสรุปยอดขาย)
        public static List<CReport9> getReport9(DateTime sDate, DateTime eDate)
        {
            List<CReport9> reports = new List<CReport9>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "9"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport9 report = new CReport9
                    {
                        RUNNING = running,
                        PaymentType = Convert.ToString(dr["SpayType"]),
                        Amount = Convert.ToDouble(dr["Amount"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุปยอดขายสมาชิก)
        public static List<CReport10> getReport10(DateTime sDate, DateTime eDate)
        {
            List<CReport10> reports = new List<CReport10>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "10"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport10 report = new CReport10
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["TMemberID"]),
                        COUNT = Convert.ToInt16(dr["icount"]),
                        Remark = Convert.ToString(dr["Nickname"]),
                        Amount = Convert.ToDouble(dr["SAmount"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุปการจำนวนครั้งในการใช้บริการ ช/ญ. แยกตามประเภทสมาชิก)
        public static List<CReport11> getReport11(DateTime sDate, DateTime eDate)
        {
            List<CReport11> reports = new List<CReport11>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "11"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport11 report = new CReport11
                    {
                        RUNNING = running,
                        Sex = Convert.ToString(dr["MSex"]),
                        Type = Convert.ToString(dr["RCustomerType"]),
                        ID = Convert.ToString(dr["Rid"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุปการจำนวนครั้งในการใช้บริการ ช/ญ. แยกตามประเภทบริการ)
        public static List<CReport12> getReport12(DateTime sDate, DateTime eDate)
        {
            List<CReport12> reports = new List<CReport12>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "12"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport12 report = new CReport12
                    {
                        RUNNING = running,
                        Sex = Convert.ToString(dr["MSex"]),
                        Customertype = Convert.ToString(dr["RCustomerType"]),
                        MassageType = Convert.ToString(dr["RMassageType"]),
                        ID = Convert.ToString(dr["Rid"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุป วันลาพนักงาน)
        public static List<CReport14> getReport14(DateTime sDate, DateTime eDate)
        {
            List<CReport14> reports = new List<CReport14>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "14"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport14 report = new CReport14
                    {
                        RUNNING = running,
                        NickName = Convert.ToString(dr["EnickName"]),
                        Biz = Convert.ToDouble(dr["LeaveBiz"]),
                        Holiday = Convert.ToDouble(dr["LeaveHoliday"]),
                        Sick = Convert.ToDouble(dr["LeaveSick"]),
                        Count1 = Convert.ToDouble(dr["countBiz"]),
                        Count2 = Convert.ToDouble(dr["countHoliday"]),
                        Count3 = Convert.ToDouble(dr["countSick"]),
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(Therapist Income)
        public static List<CReport15> getReport15(DateTime sDate, DateTime eDate)
        {
            List<CReport15> reports = new List<CReport15>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "15"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport15 report = new CReport15
                    {
                        RUNNING = running,
                        NickName = Convert.ToString(dr["RTherapist"]),
                        D1 = Convert.ToInt16(dr["D1"]),
                        D2 = Convert.ToInt16(dr["D2"]),
                        D3 = Convert.ToInt16(dr["D3"]),
                        D4 = Convert.ToInt16(dr["D4"]),
                        D5 = Convert.ToInt16(dr["D5"]),
                        D6 = Convert.ToInt16(dr["D6"]),
                        D7 = Convert.ToInt16(dr["D7"]),
                        D8 = Convert.ToInt16(dr["D8"]),
                        D9 = Convert.ToInt16(dr["D9"]),
                        D10 = Convert.ToInt16(dr["D10"]),
                        D11 = Convert.ToInt16(dr["D11"]),
                        D12 = Convert.ToInt16(dr["D12"]),
                        D13 = Convert.ToInt16(dr["D13"]),
                        D14 = Convert.ToInt16(dr["D14"]),
                        D15 = Convert.ToInt16(dr["D15"]),
                        D16 = Convert.ToInt16(dr["D16"]),
                        D17 = Convert.ToInt16(dr["D17"]),
                        D18 = Convert.ToInt16(dr["D18"]),
                        D19 = Convert.ToInt16(dr["D19"]),
                        D20 = Convert.ToInt16(dr["D20"]),
                        D21 = Convert.ToInt16(dr["D21"]),
                        D22 = Convert.ToInt16(dr["D22"]),
                        D23 = Convert.ToInt16(dr["D23"]),
                        D24 = Convert.ToInt16(dr["D24"]),
                        D25 = Convert.ToInt16(dr["D25"]),
                        D26 = Convert.ToInt16(dr["D26"]),
                        D27 = Convert.ToInt16(dr["D27"]),
                        D28 = Convert.ToInt16(dr["D28"]),
                        D29 = Convert.ToInt16(dr["D29"]),
                        D30 = Convert.ToInt16(dr["D30"]),
                        D31 = Convert.ToInt16(dr["D31"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(Therapist JOB)
        public static List<CReport16> getReport16(DateTime sDate, DateTime eDate)
        {
            List<CReport16> reports = new List<CReport16>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "16"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport16 report = new CReport16
                    {
                        RUNNING = running,
                        Customertype = Convert.ToString(dr["CustomerType"]),
                        MassageType = Convert.ToString(dr["RMassageType"]),
                        Hour = Convert.ToString(dr["RHour"]),
                        Sex = Convert.ToString(dr["Sex"]),
                        Amount1 = Convert.ToInt16(dr["Pprice"]),
                        Amount2 = Convert.ToInt16(dr["PpriceforTherapist"]),
                        TherapistID = Convert.ToString(dr["Therapist"]),
                        ID = Convert.ToString(dr["RID"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สมาชิกมาใช้บริการ (Member))
        public static List<CReport17> getReport17()
        {
            List<CReport17> reports = new List<CReport17>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetMemberReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", ""));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "0"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 0;
                while (dr.Read())
                {
                    reports.Add(genMemberData17(Convert.ToString(dr["Mno"])));
                    running++;
                }
                dr.Close();
            }
            return reports;
        }
        //(สมาชิกมาใช้บริการ (Specail Package))
        public static List<CReport17> getReport18()
        {
            List<CReport17> reports = new List<CReport17>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetMemberReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", ""));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "1"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 0;
                while (dr.Read())
                {
                    reports.Add(genMemberData18(Convert.ToString(dr["Mno"])));
                    running++;
                }
                dr.Close();
            }
            return reports;
        }
        //สรุปการจองพนักงานนวด
        public static List<CCustomerTransaction> getReport19(DateTime sDate, DateTime eDate)
        {
            List<CCustomerTransaction> reports = new List<CCustomerTransaction>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "17"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 0;
                while (dr.Read())
                {
                    CCustomerTransaction ct = new CCustomerTransaction
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["RID"]),
                        CustomerID = Convert.ToString(dr["RMemberID"]),
                        TherapistID = Convert.ToString(dr["RTherapistID"]),
                        TherapistRevID = Convert.ToString(dr["RTherapistRevID"]),
                        Package = Convert.ToString(dr["RPackage"]),
                        Date = Convert.ToDateTime(dr["RDate"]),
                        Time = Convert.ToDateTime(dr["RTime"]),
                        EndDate = Convert.ToDateTime(dr["RTime"]).AddHours(Convert.ToDouble(Management.getDetail(MasterList.MHour, Management.getPackage(Convert.ToString(dr["RPackage"]))[0].Hour))),
                        Room = Convert.ToString(dr["RRoom"]),
                        Customertype = Convert.ToString(dr["RCustomerType"]),
                        Name = Convert.ToString(dr["Mname"]),
                        Surname = Convert.ToString(dr["Msurname"]),
                        Mobile = Convert.ToString(dr["Mmobile"]),
                        Type = Convert.ToString(dr["Rtype"]),
                        Nation = Convert.ToString(dr["Mnational"]),
                        Address = Convert.ToString(dr["MContactAddress"]),
                        Sex = Convert.ToString(dr["MSex"]),
                        Age = Convert.ToInt16(dr["MAge"]),
                        Remark = Convert.ToString(dr["isPlay"]),
                        SpecialPackage = Convert.ToString(dr["RSpecialPackage"]),
                        FLAG = Convert.ToString(dr["RemarkPackageID"]),
                        NickName = Convert.ToString(dr["EmpName"])
                    };
                    running++;
                    reports.Add(ct);
                }
                dr.Close();
            }
            return reports;
        }
        //GEN MEMBER
        private static CReport17 genMemberData17(string _mno)
        {
            List<CReport17> lRpt = new List<CReport17>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetMemberReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", _mno));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "2"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 0;
                while (dr.Read())
                {
                    CReport17 report = new CReport17
                    {
                        ID = Convert.ToString(dr["Mno"]),
                        Name = Convert.ToString(dr["Mname"]),
                        Sex = Convert.ToString(dr["Msex"]),
                        Nation = Convert.ToString(dr["Mnational"]),
                        TherapistID = (Convert.ToString(dr["TTherapist"]).Equals("")) ? "" : Convert.ToString(dr["TTherapist"]),//พนักงานนวด
                        KEYDATE = Convert.ToDateTime(dr["KeyDate"]),//วันที่ซื้อ Package
                        Package = Convert.ToString(dr["PackageType"]),//ประเภท Package
                        TherapistPrice = Convert.ToDouble(dr["TherapistCost"]),
                        Status = Convert.ToString(dr["PackageTotal"]),//จำนวน package ทั้งหมด
                        PackagePrice = Convert.ToDouble(dr["SAmount"]),
                        PriceUsed = Convert.ToDouble(dr["MUseMember"]),
                        U1 = getMonthDay(Convert.ToString(dr["1"])),
                        U2 = getMonthDay(Convert.ToString(dr["2"])),
                        U3 = getMonthDay(Convert.ToString(dr["3"])),
                        U4 = getMonthDay(Convert.ToString(dr["4"])),
                        U5 = getMonthDay(Convert.ToString(dr["5"])),
                        U6 = getMonthDay(Convert.ToString(dr["6"])),
                        U7 = getMonthDay(Convert.ToString(dr["7"])),
                        U8 = getMonthDay(Convert.ToString(dr["8"])),
                        U9 = getMonthDay(Convert.ToString(dr["9"])),
                        U10 = getMonthDay(Convert.ToString(dr["10"])),
                        U11 = getMonthDay(Convert.ToString(dr["11"])),
                        U12 = getMonthDay(Convert.ToString(dr["12"])),
                        U13 = getMonthDay(Convert.ToString(dr["13"])),
                        U14 = getMonthDay(Convert.ToString(dr["14"])),
                        U15 = getMonthDay(Convert.ToString(dr["15"])),
                        U16 = getMonthDay(Convert.ToString(dr["16"])),
                        U17 = getMonthDay(Convert.ToString(dr["17"])),
                        U18 = getMonthDay(Convert.ToString(dr["18"])),
                        U19 = getMonthDay(Convert.ToString(dr["19"])),
                        U20 = getMonthDay(Convert.ToString(dr["20"])),
                        U21 = getMonthDay(Convert.ToString(dr["21"])),
                        U22 = getMonthDay(Convert.ToString(dr["22"])),
                        U23 = getMonthDay(Convert.ToString(dr["23"])),
                        U24 = getMonthDay(Convert.ToString(dr["24"])),
                        U25 = getMonthDay(Convert.ToString(dr["25"])),
                        U26 = getMonthDay(Convert.ToString(dr["26"])),
                        U27 = getMonthDay(Convert.ToString(dr["27"])),
                        U28 = getMonthDay(Convert.ToString(dr["28"])),
                        U29 = getMonthDay(Convert.ToString(dr["29"])),
                        U30 = getMonthDay(Convert.ToString(dr["30"])),
                    };
                    lRpt.Add(report);
                    running++;
                }
                dr.Close();
            }
            return (lRpt.Count > 0) ? lRpt[0] : new CReport17 { };
        }
        private static CReport17 genMemberData18(string _mno)
        {
            List<CReport17> lRpt = new List<CReport17>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetMemberReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inMno", _mno));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "3"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 0;
                while (dr.Read())
                {
                    CReport17 report = new CReport17
                    {
                        ID = Convert.ToString(dr["Mno"]),
                        Name = Convert.ToString(dr["Mname"]),
                        //Type = Convert.ToString(dr["MType"]),
                        Sex = Convert.ToString(dr["Msex"]),
                        Nation = Convert.ToString(dr["Mnational"]),
                        TherapistID = (Convert.ToString(dr["TTherapist"]).Equals("")) ? "" : Convert.ToString(dr["TTherapist"]),//พนักงานนวด
                        KEYDATE = Convert.ToDateTime(dr["KeyDate"]),//วันที่ซื้อ Package
                        Package = Convert.ToString(dr["PackageType"]),//ประเภท Package
                        TherapistPrice = Convert.ToDouble(dr["TherapistCost"]),
                        //PackagePrice = Convert.ToDouble(dr["SAmount"]),
                        PriceUsed = Convert.ToDouble(dr["MUseMember"]),
                        Status = Convert.ToString(dr["PackageTotal"]),//จำนวน package ทั้งหมด
                        U1 = getMonthDay(Convert.ToString(dr["1"])),
                        U2 = getMonthDay(Convert.ToString(dr["2"])),
                        U3 = getMonthDay(Convert.ToString(dr["3"])),
                        U4 = getMonthDay(Convert.ToString(dr["4"])),
                        U5 = getMonthDay(Convert.ToString(dr["5"])),
                        U6 = getMonthDay(Convert.ToString(dr["6"])),
                        U7 = getMonthDay(Convert.ToString(dr["7"])),
                        U8 = getMonthDay(Convert.ToString(dr["8"])),
                        U9 = getMonthDay(Convert.ToString(dr["9"])),
                        U10 = getMonthDay(Convert.ToString(dr["10"])),
                        U11 = getMonthDay(Convert.ToString(dr["11"])),
                        U12 = getMonthDay(Convert.ToString(dr["12"])),
                        U13 = getMonthDay(Convert.ToString(dr["13"])),
                        U14 = getMonthDay(Convert.ToString(dr["14"])),
                        U15 = getMonthDay(Convert.ToString(dr["15"])),
                        U16 = getMonthDay(Convert.ToString(dr["16"])),
                        U17 = getMonthDay(Convert.ToString(dr["17"])),
                        U18 = getMonthDay(Convert.ToString(dr["18"])),
                        U19 = getMonthDay(Convert.ToString(dr["19"])),
                        U20 = getMonthDay(Convert.ToString(dr["20"])),
                        U21 = getMonthDay(Convert.ToString(dr["21"])),
                        U22 = getMonthDay(Convert.ToString(dr["22"])),
                        U23 = getMonthDay(Convert.ToString(dr["23"])),
                        U24 = getMonthDay(Convert.ToString(dr["24"])),
                        U25 = getMonthDay(Convert.ToString(dr["25"])),
                        U26 = getMonthDay(Convert.ToString(dr["26"])),
                        U27 = getMonthDay(Convert.ToString(dr["27"])),
                        U28 = getMonthDay(Convert.ToString(dr["28"])),
                        U29 = getMonthDay(Convert.ToString(dr["29"])),
                        U30 = getMonthDay(Convert.ToString(dr["30"])),
                    };
                    lRpt.Add(report);
                    running++;
                }
                dr.Close();
            }
            return lRpt[0];
        }
        //(สรุปยอดขายรายเดือน)
        public static List<CReport20> getReport20(DateTime sDate, DateTime eDate)
        {
            List<CReport20> reports = new List<CReport20>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "18"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport20 report = new CReport20
                    {
                        RUNNING = running,
                        FLAG = Convert.ToString(dr["RCustomerType"]),
                        Amount = Convert.ToDouble(dr["amount"]),
                        Count = Convert.ToInt16(dr["icount"]),
                        PerDay = Convert.ToDouble(dr["perdate"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        //(สรุปจำนวนสมาชิก)
        public static List<CReport21> getReport21(DateTime sDate, DateTime eDate)
        {
            List<CReport21> reports = new List<CReport21>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "19"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport21 report = new CReport21
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["Mno"]),
                        Sex = Convert.ToString(dr["Msex"]),
                        FLAG = Convert.ToString(dr["SACCode"]),
                        Package = Convert.ToString(dr["MPackage"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        // สรุปจำนวนสมาชิกใหม่
        public static List<CReport22> getReport22(DateTime sDate, DateTime eDate)
        {
            List<CReport22> reports = new List<CReport22>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inStartDate", sDate));
                cmd.Parameters.Add(new SqlParameter("@inEndDate", eDate));
                cmd.Parameters.Add(new SqlParameter("@inReportType", "20"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CReport22 report = new CReport22
                    {
                        RUNNING = running,
                        DETAIL = Convert.ToString(dr["descrip"]),
                        Age = Convert.ToInt16(dr["icount"])
                    };
                    running++;
                    reports.Add(report);
                }
                dr.Close();
            }
            return reports;
        }
        private static string getMonthDay(string _data)
        {
            if (_data.Equals("")) return "";
            int month = Convert.ToInt16(_data) / 30;
            int day = Convert.ToInt16(_data) % 30;
            return month.ToString("00") + "/" + day.ToString("");
        }
    }
}
