﻿using System;
using System.Collections.Generic;
using System.Text;
using Rakuten.Structure;
using System.Data.SqlClient;
using System.Data;

namespace Rakuten.DAL
{
    public class DALDeleteTran
    {
        //Get Data
        public static List<CDeleteTran> getList(CDeleteTran c)
        {
            List<CDeleteTran> lists = new List<CDeleteTran>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DeleteTransactions", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inPeriod", c.Period));
                cmd.Parameters.Add(new SqlParameter("@inFlag", c.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CDeleteTran master = new CDeleteTran
                    {
                        RUNNING = running,
                        Period = Convert.ToString(dr["period"])
                    };
                    running++;
                    lists.Add(master);
                }
                dr.Close();
            }
            return lists;
        }
        public static int manageMaster(CDeleteTran c)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("DeleteTransactions", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inPeriod", c.Period));
                cmd.Parameters.Add(new SqlParameter("@inFlag", c.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
