﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;
using Rakuten.Utility;

namespace Rakuten.DAL
{
    public class DALEmployee
    {
        //Get Data
        public static List<CEmployee> getList(CEmployee _emp)
        {
            List<CEmployee> cuss = new List<CEmployee>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployee", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inEno", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inEtype", _emp.Type));
                cmd.Parameters.Add(new SqlParameter("@inEsex", _emp.Sex));
                cmd.Parameters.Add(new SqlParameter("@inEtitle", _emp.Title));
                cmd.Parameters.Add(new SqlParameter("@inEname", _emp.Name));
                cmd.Parameters.Add(new SqlParameter("@inEsurname", _emp.Surname));
                cmd.Parameters.Add(new SqlParameter("@inEnickname", _emp.NickName));
                cmd.Parameters.Add(new SqlParameter("@inEcontactAddress", _emp.Address));
                cmd.Parameters.Add(new SqlParameter("@inEmobile", _emp.Mobile));
                cmd.Parameters.Add(new SqlParameter("@inEemail", _emp.Email));
                cmd.Parameters.Add(new SqlParameter("@inMStatus", _emp.Status));
                cmd.Parameters.Add(new SqlParameter("@inMperiod", _emp.Period));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inbiz", _emp.Biz));
                cmd.Parameters.Add(new SqlParameter("@inHoliday", _emp.Holiday));
                cmd.Parameters.Add(new SqlParameter("@inSick", _emp.Sick));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CEmployee cus = new CEmployee
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["Eno"]),
                        Type = Convert.ToString(dr["Etype"]),
                        Sex = Convert.ToString(dr["Esex"]),
                        Title = Convert.ToString(dr["Etitle"]),
                        Name = Convert.ToString(dr["Ename"]),
                        Surname = Convert.ToString(dr["Esurname"]),
                        NickName = Convert.ToString(dr["Enickname"]),
                        Address = Convert.ToString(dr["EcontactAddress"]),
                        Mobile = Convert.ToString(dr["Emobile"]),
                        Email = Convert.ToString(dr["Eemail"]),
                        Status = Convert.ToString(dr["Mstatus"]),
                        Period = Convert.ToString(dr["Mperiod"]),
                        KEYDATE =(DBNull.Value == dr["MKeyDate"])? DateTime.Now: Convert.ToDateTime(dr["MKeyDate"]),
                        LASTEDIT = (DBNull.Value == dr["MLastEdit"]) ? DateTime.Now : Convert.ToDateTime(dr["MLastEdit"]),
                        KEYUSER = Convert.ToString(dr["MKeyUser"]),
                        Biz = Convert.ToDouble(dr["LeaveBiz"]),
                        Holiday = Convert.ToDouble(dr["LeaveHoliday"]),
                        Sick = Convert.ToDouble(dr["LeaveSick"])
                    };
                    running++;
                    cuss.Add(cus);
                }
                dr.Close();
            }
            return cuss;
        }
        //Insert,Delete,Update
        public static int manageMaster(CEmployee _emp)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployee", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inEno", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inEtype", _emp.Type));
                cmd.Parameters.Add(new SqlParameter("@inEsex", _emp.Sex));
                cmd.Parameters.Add(new SqlParameter("@inEtitle", _emp.Title));
                cmd.Parameters.Add(new SqlParameter("@inEname", _emp.Name));
                cmd.Parameters.Add(new SqlParameter("@inEsurname", _emp.Surname));
                cmd.Parameters.Add(new SqlParameter("@inEnickname", _emp.NickName));
                cmd.Parameters.Add(new SqlParameter("@inEcontactAddress", _emp.Address));
                cmd.Parameters.Add(new SqlParameter("@inEmobile", _emp.Mobile));
                cmd.Parameters.Add(new SqlParameter("@inEemail", _emp.Email));
                cmd.Parameters.Add(new SqlParameter("@inMStatus", (_emp.Status.Equals("0") ? "A" : "I")));
                cmd.Parameters.Add(new SqlParameter("@inMperiod", _emp.Period));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inbiz", _emp.Biz));
                cmd.Parameters.Add(new SqlParameter("@inHoliday", _emp.Holiday));
                cmd.Parameters.Add(new SqlParameter("@inSick", _emp.Sick));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
                cmd.Clone();
            }
            return i;
        }
        //Employee Time Stamp
        public static int manageEmployeeSchedule(CSchedule schedule)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSchedule", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inESno", schedule.ID));
                cmd.Parameters.Add(new SqlParameter("@inQ", schedule.Remark));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", schedule.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", schedule.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inFlag", schedule.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
                cmd.Clone();
            }
            return i;
        }
        //Employee Summary List
        public static List<CEmployeeSummary> getlistEmpSummary(CEmployeeSummary _emp)
        { 
            List<CEmployeeSummary> emps = new List<CEmployeeSummary>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSummary", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inId", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inTherapistID", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inIncome", _emp.Income));
                cmd.Parameters.Add(new SqlParameter("@inGuestReserv", _emp.GuestReservation));
                cmd.Parameters.Add(new SqlParameter("@inSoldMember", _emp.SoldMember));
                cmd.Parameters.Add(new SqlParameter("@inFind", _emp.Fine));
                cmd.Parameters.Add(new SqlParameter("@inRemain", _emp.ReMain));
                cmd.Parameters.Add(new SqlParameter("@inPeriod", _emp.Period));
                cmd.Parameters.Add(new SqlParameter("@inEmpLateTime", _emp.Late));//สาย
                cmd.Parameters.Add(new SqlParameter("@inEStart", _emp.CheckIN));//เข้างาน
                cmd.Parameters.Add(new SqlParameter("@inEEnd", _emp.CheckOUT));//ออกงาน
                cmd.Parameters.Add(new SqlParameter("@inECount", _emp.RUNNING));//จำนวน
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", _emp.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                switch (_emp.FLAG)
                {
                    case "4"://แสดงข้อมูลสรุปค่ามือ
                        while (dr.Read())
                        {
                            CEmployeeSummary emp = new CEmployeeSummary
                            {
                                RUNNING = Convert.ToInt16(dr["icount"]),
                                ID = Convert.ToString(dr["Eno"]),
                                NickName = Convert.ToString(dr["Ename"]),
                                DETAIL = Convert.ToString(dr["DDetail"]),
                                Late = MyFunction.getMinute(Convert.ToDateTime(dr["MCheckInTime"]).ToShortTimeString(), Convert.ToDateTime(dr["MPstart"]).ToShortTimeString()) *0.5,
                                //Convert.ToInt16(ManageLOG.deCode(ManageLOG.getValueFromRegistry("Rakuten", "LATE_FINE"))),
                                GuestReservation = Convert.ToInt16(dr["GuestReservation"]),
                                Income = Convert.ToString(dr["Income"]),
                                SoldMember = Convert.ToString(dr["SoldMember"]),
                                Fine = Convert.ToString(dr["FineMoney"]),
                                CheckIN = Convert.ToString(dr["EStartTime"]).Equals("") ? "" : Convert.ToDateTime(dr["EStartTime"]).ToShortTimeString(),
                                CheckOUT = Convert.ToString(dr["EEndTime"]).Equals("") ? "" : Convert.ToDateTime(dr["EEndTime"]).ToShortTimeString(),
                                ReMain = (Convert.ToDouble(dr["Income"]) + Convert.ToDouble(dr["SoldMember"])).ToString()
                            };
                            running++;
                            emps.Add(emp);
                        }
                        break;
                    case "6"://ดึงข้อมูลสรุปค่ามือที่บันทึกไว้แล้วขึ้นมา
                        while (dr.Read())
                        {
                            CEmployeeSummary emp = new CEmployeeSummary
                            {
                                RUNNING = Convert.ToInt16(dr["ecount"]),
                                CheckIN = Convert.ToString(dr["EStart"]),
                                CheckOUT = Convert.ToString(dr["EEnd"]),
                                Remark = Convert.ToString(dr["ID"]),
                                ID = Convert.ToString(dr["TherapistID"]),
                                NickName = Convert.ToString(dr["tname"]),
                                DETAIL = "",//Convert.ToString(dr["DDetail"]),
                                Late = Convert.ToDouble(dr["Emplatetime"]),
                                GuestReservation = Convert.ToInt16(dr["GuestReserv"]),
                                Income = Convert.ToString(dr["Income"]),
                                SoldMember = Convert.ToString(dr["soldmember"]),
                                Fine = Convert.ToString(dr["find"]),
                                ReMain = Convert.ToString(dr["Remain"]) 
                            };
                            running++;
                            emps.Add(emp);
                        }
                        break;
                }
            }
            return emps; 
        }
        //ตรวจสอบว่าเคยมีการโอนข้อมูลไปก่อนหรือไม่
        public static bool isTransfered(CEmployeeSummary _emp)
        {
            bool bIsTranfered = false;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSummary", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inId", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inTherapistID", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inIncome", _emp.Income));
                cmd.Parameters.Add(new SqlParameter("@inGuestReserv", _emp.GuestReservation));
                cmd.Parameters.Add(new SqlParameter("@inSoldMember", _emp.SoldMember));
                cmd.Parameters.Add(new SqlParameter("@inFind", _emp.Fine));
                cmd.Parameters.Add(new SqlParameter("@inRemain", _emp.ReMain));
                cmd.Parameters.Add(new SqlParameter("@inPeriod", _emp.Period));
                cmd.Parameters.Add(new SqlParameter("@inEStart", _emp.CheckIN));//เข้างาน
                cmd.Parameters.Add(new SqlParameter("@inEEnd", _emp.CheckOUT));//ออกงาน
                cmd.Parameters.Add(new SqlParameter("@inECount", _emp.RUNNING));//จำนวน
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", _emp.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inEmpLateTime", _emp.Late));//สาย
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    bIsTranfered = (Convert.ToInt16(dr["icount"]) > 0) ? true : false;
                }
            }
            return bIsTranfered;
        }
        //manage Employee Summary 
        public static int manageMaster(CEmployeeSummary _emp)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSummary", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inId", _emp.Remark));
                cmd.Parameters.Add(new SqlParameter("@inTherapistID", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inIncome", _emp.Income));
                cmd.Parameters.Add(new SqlParameter("@inGuestReserv", _emp.GuestReservation));
                cmd.Parameters.Add(new SqlParameter("@inSoldMember", _emp.SoldMember));
                cmd.Parameters.Add(new SqlParameter("@inFind", _emp.Fine));
                cmd.Parameters.Add(new SqlParameter("@inRemain", _emp.ReMain));
                cmd.Parameters.Add(new SqlParameter("@inPeriod", _emp.Period));
                cmd.Parameters.Add(new SqlParameter("@inEmpLateTime", _emp.Late));//สาย
                cmd.Parameters.Add(new SqlParameter("@inEStart", _emp.CheckIN));//เข้างาน
                cmd.Parameters.Add(new SqlParameter("@inEEnd", _emp.CheckOUT));//ออกงาน
                cmd.Parameters.Add(new SqlParameter("@inECount", _emp.RUNNING));//จำนวน
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", _emp.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
        //manage Employee Leave
        public static int manageLeave(CEmployeeSummary _emp)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeLeave", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inId", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inEmpID", _emp.EmpID));
                cmd.Parameters.Add(new SqlParameter("@inLType", _emp.Type));
                cmd.Parameters.Add(new SqlParameter("@inLStartDate", _emp.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inLEndDate", _emp.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inLCount", _emp.Income));
                cmd.Parameters.Add(new SqlParameter("@inLReason", _emp.Remark));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", _emp.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
        //Get Employee List
        public static double getEmployeeLeaveCount(CEmployeeSummary _emp)
        {
            double count = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeLeave", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inId", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inEmpID", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inLType", _emp.Type));
                cmd.Parameters.Add(new SqlParameter("@inLStartDate", _emp.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inLEndDate", _emp.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inLCount", _emp.Income));
                cmd.Parameters.Add(new SqlParameter("@inLReason", _emp.Remark));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", _emp.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    count = Convert.ToDouble(dr["Lcount"]);
                }
            }
            return count; 
        }
        //Update Employee Time Period
        public static int updateEmpTimePeriod(string id, string status)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("updateEmployeeTimePeriod", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inEno", id));
                cmd.Parameters.Add(new SqlParameter("@inMPeriod", status));
                con.Open();
                i = cmd.ExecuteNonQuery();
                cmd.Clone();
            }
            return i;
        }
        //ตรวจสอบว่าพนักงานมีการลาในวันนี้หรือไม่
        public static CLeave GetEmployeeLeaveStatus(string EmpID)
        {
            CLeave leave = new CLeave();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetEmployeeLeaveStatus", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@EmpID", EmpID));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "1"));
                
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {

                    leave.Type = DBNull.Value.Equals(dr["Ltype"]) ? "" : Convert.ToString(dr["Ltype"]);
                    leave.Count = DBNull.Value.Equals(dr["LCount"]) ? 0.00 : Convert.ToDouble(dr["LCount"]);//จำนวนวันลา กรณีเป็น ครึ่งวัน
                    leave.FLAG = DBNull.Value.Equals(dr["LCount1"]) ? "0" : Convert.ToString(dr["LCount1"]); //จำนวนวันที่ลาคงเหลือ (วันที่ลาถึง - วันที่เริ่มลา)
                }
            }
            return leave;
        }
        //ตรวจสอบว่ามีการลงเวลาเข้างานยัง
        public static bool isCheckIn(string EmpID)
        {
            bool bCheckIn = false;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("GetEmployeeLeaveStatus", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@EmpID", EmpID));
                cmd.Parameters.Add(new SqlParameter("@inFlag", "2"));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    bCheckIn = Convert.ToString(dr["ESno"]).Equals("") ? false : true;
                }
            }
            return bCheckIn;
        }
        //ดูข้อมูลวันลา
        public static List<CLeave> getLeave(CLeave _emp)
        {
            List<CLeave> lists = new List<CLeave>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeLeave", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inId", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inEmpID", _emp.EmpID));
                cmd.Parameters.Add(new SqlParameter("@inLType", _emp.Type));
                cmd.Parameters.Add(new SqlParameter("@inLStartDate", _emp.StartDate));
                cmd.Parameters.Add(new SqlParameter("@inLEndDate", _emp.EndDate));
                cmd.Parameters.Add(new SqlParameter("@inLCount", _emp.Count));
                cmd.Parameters.Add(new SqlParameter("@inLReason", _emp.Remark));
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", _emp.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CLeave cus = new CLeave
                    {
                        RUNNING = running,
                        
                        ID = Convert.ToString(dr["ID"]),
                        EmpID = Convert.ToString(dr["EmpID"]),
                        Name = Convert.ToString(dr["ename"]),
                        Type = Convert.ToString(dr["LType"]),
                        StartDate = Convert.ToDateTime(dr["LStartDate"]),
                        EndDate = Convert.ToDateTime(dr["LEndDate"]),
                        Count = Convert.ToDouble(dr["LCount"])
                    };
                    running++;
                    lists.Add(cus);
                }
                dr.Close();
            }
            return lists;
        }
        //ดึงข้อมูลค่าขายสมาชิก
        public static List<CEmployeeSummary> getComission(CEmployeeSummary _emp)
        {
            List<CEmployeeSummary> emps = new List<CEmployeeSummary>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageEmployeeSummary", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inId", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inTherapistID", _emp.ID));
                cmd.Parameters.Add(new SqlParameter("@inIncome", _emp.Income));
                cmd.Parameters.Add(new SqlParameter("@inGuestReserv", _emp.GuestReservation));
                cmd.Parameters.Add(new SqlParameter("@inSoldMember", _emp.SoldMember));
                cmd.Parameters.Add(new SqlParameter("@inFind", _emp.Fine));
                cmd.Parameters.Add(new SqlParameter("@inRemain", _emp.ReMain));
                cmd.Parameters.Add(new SqlParameter("@inPeriod", _emp.Period));
                cmd.Parameters.Add(new SqlParameter("@inEmpLateTime", _emp.Late));//สาย
                cmd.Parameters.Add(new SqlParameter("@inEStart", _emp.CheckIN));//เข้างาน
                cmd.Parameters.Add(new SqlParameter("@inEEnd", _emp.CheckOUT));//ออกงาน
                cmd.Parameters.Add(new SqlParameter("@inECount", _emp.RUNNING));//จำนวน
                cmd.Parameters.Add(new SqlParameter("@inKeyDate", _emp.KEYDATE));
                cmd.Parameters.Add(new SqlParameter("@inKeyUser", _emp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _emp.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CEmployeeSummary emp = new CEmployeeSummary
                    {
                        ID = Convert.ToString(dr["TTherapist"]),
                        Income = Convert.ToString(dr["amount"])
                    };
                    running++;
                    emps.Add(emp);
                }
            }
            return emps;
        }
    }
}
