﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Rakuten.Structure;
using System.Data;
using System.Data.SqlClient;

namespace Rakuten.DAL
{
    public class DALCustomerPayment
    {
        //Insert,Delete,Update
        public static int manageMaster(CCustomerPayment cp)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageCustomerPayment", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inSid", cp.ID));
                cmd.Parameters.Add(new SqlParameter("@inSCusTranID", cp.CustomerTranID));
                cmd.Parameters.Add(new SqlParameter("@inSACCode", cp.AccountCode));
                cmd.Parameters.Add(new SqlParameter("@inSAmount", cp.Amount));
                cmd.Parameters.Add(new SqlParameter("@inSDiscount", cp.Discount));
                cmd.Parameters.Add(new SqlParameter("@inSPayType", cp.paymentType));
                cmd.Parameters.Add(new SqlParameter("@inSSlip", cp.Slip));
                cmd.Parameters.Add(new SqlParameter("@inSBank", cp.Bank));
                cmd.Parameters.Add(new SqlParameter("@inSStatus", cp.status));
                cmd.Parameters.Add(new SqlParameter("@inSKeyUser", cp.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inPackageID", cp.Remark));
                cmd.Parameters.Add(new SqlParameter("@inIsSppackage", cp.DETAIL));//Flag เพื่อบอกว่าเป็น specail pacage หรือไม่
                cmd.Parameters.Add(new SqlParameter("@inFlag", cp.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
