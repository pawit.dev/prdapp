﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using Rakuten.DAL;
using Rakuten.Structure;
using Rakuten.Utility;
namespace Rakuten.DAL
{
    public class DALRoom
    {
        //Get Data
        public static List<CRoom> getList(CRoom _room)
        {
            List<CRoom> rooms = new List<CRoom>();
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageRoom", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _room.ID));
                cmd.Parameters.Add(new SqlParameter("@intype", _room.MeassageType));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _room.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _room.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _room.FLAG));
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                int running = 1;
                while (dr.Read())
                {
                    CRoom room = new CRoom
                    {
                        RUNNING = running,
                        ID = Convert.ToString(dr["RNo"]),
                        DETAIL = Convert.ToString(dr["RDetail"]),
                        MeassageType = Convert.ToString(dr["MmassageType"]),
                        KEYDATE = Convert.ToDateTime(dr["MKeyDate"]),
                        LASTEDIT = Convert.ToDateTime(dr["MLastEdit"]),
                        KEYUSER = Convert.ToString(dr["MKeyUser"])
                    };
                    running++;
                    rooms.Add(room);
                }
                dr.Close();
            }
            return rooms;
        }
        //Insert,Delete,Update
        public static int manageMaster(CRoom _room)
        {
            int i = 0;
            using (SqlConnection con = new SqlConnection(Connection.ConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("manageRoom", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@inNo", _room.ID));
                cmd.Parameters.Add(new SqlParameter("@intype", _room.MeassageType));
                cmd.Parameters.Add(new SqlParameter("@inDetail", _room.DETAIL));
                cmd.Parameters.Add(new SqlParameter("@inMkeyUser", _room.KEYUSER));
                cmd.Parameters.Add(new SqlParameter("@inFlag", _room.FLAG));
                con.Open();
                i = cmd.ExecuteNonQuery();
            }
            return i;
        }
    }
}
