﻿namespace Rakuten
{
    partial class FrmPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMS_Insert = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_MEMBER = new System.Windows.Forms.ToolStripMenuItem();
            this.cMemberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TXT_DISCOUNT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TXT_TOTAL = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.TXT_RECIEVE = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.TXT_CHANGE = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.GMember = new System.Windows.Forms.GroupBox();
            this.LSpecialPackage_Detail = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.rUNNINGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dETAILDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.memberUseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perCourseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moneyUnitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lASTEDITDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kEYUSERDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fLAGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPackageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CBO_TYPE = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CBO_ACCOUNT_CODE = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.CBO_CASH_TYPE = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.B_PAY_TYPE = new System.Windows.Forms.Button();
            this.B_CASHTYPE = new System.Windows.Forms.Button();
            this.TXT_SLIP = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.CBO_BANK = new System.Windows.Forms.ComboBox();
            this.B_BANK = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CB_PRINT_2 = new System.Windows.Forms.CheckBox();
            this.CB_PRINT_3 = new System.Windows.Forms.CheckBox();
            this.CB_PRINT_1 = new System.Windows.Forms.CheckBox();
            this.cEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CMD_CANCEL = new System.Windows.Forms.Button();
            this.CMD_SAVE = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NUD_YEAR = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cMemberBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.GMember.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_YEAR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMS_Insert,
            this.CMS_Update,
            this.CMS_Delete,
            this.CMS_MEMBER});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStrip1.Size = new System.Drawing.Size(149, 92);
            // 
            // CMS_Insert
            // 
            this.CMS_Insert.Name = "CMS_Insert";
            this.CMS_Insert.Size = new System.Drawing.Size(148, 22);
            this.CMS_Insert.Text = "เพิ่มรายการ";
            // 
            // CMS_Update
            // 
            this.CMS_Update.Name = "CMS_Update";
            this.CMS_Update.Size = new System.Drawing.Size(148, 22);
            this.CMS_Update.Text = "ปรับปรุงรายการ";
            // 
            // CMS_Delete
            // 
            this.CMS_Delete.Name = "CMS_Delete";
            this.CMS_Delete.Size = new System.Drawing.Size(148, 22);
            this.CMS_Delete.Text = "ลบรายการ";
            // 
            // CMS_MEMBER
            // 
            this.CMS_MEMBER.Name = "CMS_MEMBER";
            this.CMS_MEMBER.Size = new System.Drawing.Size(148, 22);
            this.CMS_MEMBER.Text = "ต่ออายุสมาชิก";
            // 
            // cMasterBindingSource
            // 
            this.cMasterBindingSource.DataSource = typeof(Rakuten.Structure.CMaster);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.GMember);
            this.panel2.Controls.Add(this.CBO_TYPE);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Location = new System.Drawing.Point(2, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(657, 481);
            this.panel2.TabIndex = 1;
            this.panel2.TabStop = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TXT_DISCOUNT);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TXT_TOTAL);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.TXT_RECIEVE);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.TXT_CHANGE);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Location = new System.Drawing.Point(10, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(635, 120);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // TXT_DISCOUNT
            // 
            this.TXT_DISCOUNT.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_DISCOUNT.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_DISCOUNT.ForeColor = System.Drawing.Color.Yellow;
            this.TXT_DISCOUNT.Location = new System.Drawing.Point(135, 60);
            this.TXT_DISCOUNT.Name = "TXT_DISCOUNT";
            this.TXT_DISCOUNT.Size = new System.Drawing.Size(125, 43);
            this.TXT_DISCOUNT.TabIndex = 2;
            this.TXT_DISCOUNT.Text = "0";
            this.TXT_DISCOUNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_DISCOUNT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_DISCOUNT_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.Location = new System.Drawing.Point(55, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 25);
            this.label6.TabIndex = 126;
            this.label6.Text = "ส่วนลด";
            // 
            // TXT_TOTAL
            // 
            this.TXT_TOTAL.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_TOTAL.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_TOTAL.ForeColor = System.Drawing.Color.Yellow;
            this.TXT_TOTAL.Location = new System.Drawing.Point(135, 11);
            this.TXT_TOTAL.Name = "TXT_TOTAL";
            this.TXT_TOTAL.Size = new System.Drawing.Size(125, 43);
            this.TXT_TOTAL.TabIndex = 1;
            this.TXT_TOTAL.Text = "0";
            this.TXT_TOTAL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_TOTAL.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_TOTAL_KeyDown);
            this.TXT_TOTAL.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckIsNumber_KeyPress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label27.Location = new System.Drawing.Point(22, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(107, 25);
            this.label27.TabIndex = 104;
            this.label27.Text = "รวมเป็นเงิน";
            // 
            // TXT_RECIEVE
            // 
            this.TXT_RECIEVE.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_RECIEVE.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_RECIEVE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.TXT_RECIEVE.Location = new System.Drawing.Point(498, 11);
            this.TXT_RECIEVE.Name = "TXT_RECIEVE";
            this.TXT_RECIEVE.Size = new System.Drawing.Size(125, 43);
            this.TXT_RECIEVE.TabIndex = 3;
            this.TXT_RECIEVE.Text = "0";
            this.TXT_RECIEVE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_RECIEVE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_RECIEVE_KeyDown);
            this.TXT_RECIEVE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckIsNumber_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label26.Location = new System.Drawing.Point(435, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 25);
            this.label26.TabIndex = 122;
            this.label26.Text = "รับมา";
            // 
            // TXT_CHANGE
            // 
            this.TXT_CHANGE.BackColor = System.Drawing.SystemColors.InfoText;
            this.TXT_CHANGE.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_CHANGE.ForeColor = System.Drawing.Color.Red;
            this.TXT_CHANGE.Location = new System.Drawing.Point(498, 60);
            this.TXT_CHANGE.Name = "TXT_CHANGE";
            this.TXT_CHANGE.ReadOnly = true;
            this.TXT_CHANGE.Size = new System.Drawing.Size(125, 43);
            this.TXT_CHANGE.TabIndex = 4;
            this.TXT_CHANGE.TabStop = false;
            this.TXT_CHANGE.Text = "0";
            this.TXT_CHANGE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TXT_CHANGE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TXT_CHANGE_KeyDown);
            this.TXT_CHANGE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckIsNumber_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.Location = new System.Drawing.Point(439, 71);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 25);
            this.label25.TabIndex = 124;
            this.label25.Text = "ทอน";
            // 
            // GMember
            // 
            this.GMember.Controls.Add(this.LSpecialPackage_Detail);
            this.GMember.Controls.Add(this.dataGridView2);
            this.GMember.Location = new System.Drawing.Point(10, 164);
            this.GMember.Name = "GMember";
            this.GMember.Size = new System.Drawing.Size(636, 157);
            this.GMember.TabIndex = 22;
            this.GMember.TabStop = false;
            this.GMember.Text = "รายละเอียด Package";
            // 
            // LSpecialPackage_Detail
            // 
            this.LSpecialPackage_Detail.AutoSize = true;
            this.LSpecialPackage_Detail.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.LSpecialPackage_Detail.ForeColor = System.Drawing.Color.DarkGreen;
            this.LSpecialPackage_Detail.Location = new System.Drawing.Point(12, 123);
            this.LSpecialPackage_Detail.Name = "LSpecialPackage_Detail";
            this.LSpecialPackage_Detail.Size = new System.Drawing.Size(0, 19);
            this.LSpecialPackage_Detail.TabIndex = 113;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rUNNINGDataGridViewTextBoxColumn,
            this.iDDataGridViewTextBoxColumn,
            this.dETAILDataGridViewTextBoxColumn,
            this.hourDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.memberUseDataGridViewTextBoxColumn,
            this.perCourseDataGridViewTextBoxColumn,
            this.moneyUnitDataGridViewTextBoxColumn,
            this.kEYDATEDataGridViewTextBoxColumn,
            this.lASTEDITDataGridViewTextBoxColumn,
            this.kEYUSERDataGridViewTextBoxColumn,
            this.fLAGDataGridViewTextBoxColumn,
            this.startDateDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn,
            this.remarkDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.cPackageBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(3, 16);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(630, 135);
            this.dataGridView2.TabIndex = 112;
            this.dataGridView2.TabStop = false;
            this.dataGridView2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView2_CellFormatting);
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // rUNNINGDataGridViewTextBoxColumn
            // 
            this.rUNNINGDataGridViewTextBoxColumn.DataPropertyName = "RUNNING";
            this.rUNNINGDataGridViewTextBoxColumn.HeaderText = "ลำดับ";
            this.rUNNINGDataGridViewTextBoxColumn.Name = "rUNNINGDataGridViewTextBoxColumn";
            this.rUNNINGDataGridViewTextBoxColumn.ReadOnly = true;
            this.rUNNINGDataGridViewTextBoxColumn.Width = 60;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // dETAILDataGridViewTextBoxColumn
            // 
            this.dETAILDataGridViewTextBoxColumn.DataPropertyName = "DETAIL";
            this.dETAILDataGridViewTextBoxColumn.HeaderText = "รายละเอียด";
            this.dETAILDataGridViewTextBoxColumn.Name = "dETAILDataGridViewTextBoxColumn";
            this.dETAILDataGridViewTextBoxColumn.ReadOnly = true;
            this.dETAILDataGridViewTextBoxColumn.Width = 250;
            // 
            // hourDataGridViewTextBoxColumn
            // 
            this.hourDataGridViewTextBoxColumn.DataPropertyName = "Hour";
            this.hourDataGridViewTextBoxColumn.HeaderText = "ชม.";
            this.hourDataGridViewTextBoxColumn.Name = "hourDataGridViewTextBoxColumn";
            this.hourDataGridViewTextBoxColumn.ReadOnly = true;
            this.hourDataGridViewTextBoxColumn.Width = 60;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "ราคา";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            this.priceDataGridViewTextBoxColumn.Width = 60;
            // 
            // memberUseDataGridViewTextBoxColumn
            // 
            this.memberUseDataGridViewTextBoxColumn.DataPropertyName = "MemberUse";
            this.memberUseDataGridViewTextBoxColumn.HeaderText = "ใช้ไป";
            this.memberUseDataGridViewTextBoxColumn.Name = "memberUseDataGridViewTextBoxColumn";
            this.memberUseDataGridViewTextBoxColumn.ReadOnly = true;
            this.memberUseDataGridViewTextBoxColumn.Width = 60;
            // 
            // perCourseDataGridViewTextBoxColumn
            // 
            this.perCourseDataGridViewTextBoxColumn.DataPropertyName = "PerCourse";
            this.perCourseDataGridViewTextBoxColumn.HeaderText = "ทั้งหมด";
            this.perCourseDataGridViewTextBoxColumn.Name = "perCourseDataGridViewTextBoxColumn";
            this.perCourseDataGridViewTextBoxColumn.ReadOnly = true;
            this.perCourseDataGridViewTextBoxColumn.Width = 70;
            // 
            // moneyUnitDataGridViewTextBoxColumn
            // 
            this.moneyUnitDataGridViewTextBoxColumn.DataPropertyName = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.HeaderText = "MoneyUnit";
            this.moneyUnitDataGridViewTextBoxColumn.Name = "moneyUnitDataGridViewTextBoxColumn";
            this.moneyUnitDataGridViewTextBoxColumn.ReadOnly = true;
            this.moneyUnitDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYDATEDataGridViewTextBoxColumn
            // 
            this.kEYDATEDataGridViewTextBoxColumn.DataPropertyName = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.HeaderText = "KEYDATE";
            this.kEYDATEDataGridViewTextBoxColumn.Name = "kEYDATEDataGridViewTextBoxColumn";
            this.kEYDATEDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // lASTEDITDataGridViewTextBoxColumn
            // 
            this.lASTEDITDataGridViewTextBoxColumn.DataPropertyName = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.HeaderText = "LASTEDIT";
            this.lASTEDITDataGridViewTextBoxColumn.Name = "lASTEDITDataGridViewTextBoxColumn";
            this.lASTEDITDataGridViewTextBoxColumn.ReadOnly = true;
            this.lASTEDITDataGridViewTextBoxColumn.Visible = false;
            // 
            // kEYUSERDataGridViewTextBoxColumn
            // 
            this.kEYUSERDataGridViewTextBoxColumn.DataPropertyName = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.HeaderText = "KEYUSER";
            this.kEYUSERDataGridViewTextBoxColumn.Name = "kEYUSERDataGridViewTextBoxColumn";
            this.kEYUSERDataGridViewTextBoxColumn.ReadOnly = true;
            this.kEYUSERDataGridViewTextBoxColumn.Visible = false;
            // 
            // fLAGDataGridViewTextBoxColumn
            // 
            this.fLAGDataGridViewTextBoxColumn.DataPropertyName = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.HeaderText = "FLAG";
            this.fLAGDataGridViewTextBoxColumn.Name = "fLAGDataGridViewTextBoxColumn";
            this.fLAGDataGridViewTextBoxColumn.ReadOnly = true;
            this.fLAGDataGridViewTextBoxColumn.Visible = false;
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "StartDate";
            this.startDateDataGridViewTextBoxColumn.HeaderText = "StartDate";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.startDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "EndDate";
            this.endDateDataGridViewTextBoxColumn.HeaderText = "EndDate";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.endDateDataGridViewTextBoxColumn.Visible = false;
            // 
            // remarkDataGridViewTextBoxColumn
            // 
            this.remarkDataGridViewTextBoxColumn.DataPropertyName = "Remark";
            this.remarkDataGridViewTextBoxColumn.HeaderText = "Remark";
            this.remarkDataGridViewTextBoxColumn.Name = "remarkDataGridViewTextBoxColumn";
            this.remarkDataGridViewTextBoxColumn.ReadOnly = true;
            this.remarkDataGridViewTextBoxColumn.Visible = false;
            // 
            // cPackageBindingSource
            // 
            this.cPackageBindingSource.DataSource = typeof(Rakuten.Structure.CPackage);
            // 
            // CBO_TYPE
            // 
            this.CBO_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_TYPE.DisplayMember = "DETAIL";
            this.CBO_TYPE.Enabled = false;
            this.CBO_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_TYPE.FormattingEnabled = true;
            this.CBO_TYPE.Location = new System.Drawing.Point(463, 6);
            this.CBO_TYPE.Name = "CBO_TYPE";
            this.CBO_TYPE.Size = new System.Drawing.Size(182, 26);
            this.CBO_TYPE.TabIndex = 141;
            this.CBO_TYPE.TabStop = false;
            this.CBO_TYPE.ValueMember = "ID";
            this.CBO_TYPE.SelectedIndexChanged += new System.EventHandler(this.CBO_TYPE_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(385, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 14);
            this.label4.TabIndex = 142;
            this.label4.Text = "ประเภทลูกค้า";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CBO_ACCOUNT_CODE);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.CBO_CASH_TYPE);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.B_PAY_TYPE);
            this.groupBox2.Controls.Add(this.B_CASHTYPE);
            this.groupBox2.Controls.Add(this.TXT_SLIP);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.CBO_BANK);
            this.groupBox2.Controls.Add(this.B_BANK);
            this.groupBox2.Location = new System.Drawing.Point(10, 327);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(636, 94);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "รูปแบบการชำระ";
            // 
            // CBO_ACCOUNT_CODE
            // 
            this.CBO_ACCOUNT_CODE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_ACCOUNT_CODE.DataSource = this.cMasterBindingSource;
            this.CBO_ACCOUNT_CODE.DisplayMember = "DETAIL";
            this.CBO_ACCOUNT_CODE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_ACCOUNT_CODE.FormattingEnabled = true;
            this.CBO_ACCOUNT_CODE.Location = new System.Drawing.Point(393, 52);
            this.CBO_ACCOUNT_CODE.Name = "CBO_ACCOUNT_CODE";
            this.CBO_ACCOUNT_CODE.Size = new System.Drawing.Size(182, 26);
            this.CBO_ACCOUNT_CODE.TabIndex = 145;
            this.CBO_ACCOUNT_CODE.TabStop = false;
            this.CBO_ACCOUNT_CODE.ValueMember = "ID";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.Location = new System.Drawing.Point(300, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 14);
            this.label16.TabIndex = 116;
            this.label16.Text = "ประเภทการชำระ";
            // 
            // CBO_CASH_TYPE
            // 
            this.CBO_CASH_TYPE.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_CASH_TYPE.DataSource = this.cMasterBindingSource;
            this.CBO_CASH_TYPE.DisplayMember = "DETAIL";
            this.CBO_CASH_TYPE.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_CASH_TYPE.FormattingEnabled = true;
            this.CBO_CASH_TYPE.Location = new System.Drawing.Point(70, 20);
            this.CBO_CASH_TYPE.Name = "CBO_CASH_TYPE";
            this.CBO_CASH_TYPE.Size = new System.Drawing.Size(182, 26);
            this.CBO_CASH_TYPE.TabIndex = 142;
            this.CBO_CASH_TYPE.TabStop = false;
            this.CBO_CASH_TYPE.ValueMember = "ID";
            this.CBO_CASH_TYPE.SelectedIndexChanged += new System.EventHandler(this.CBO_CASH_TYPE_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.Location = new System.Drawing.Point(312, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 14);
            this.label28.TabIndex = 120;
            this.label28.Text = "หมายเลขสลิป";
            // 
            // B_PAY_TYPE
            // 
            this.B_PAY_TYPE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_PAY_TYPE.Location = new System.Drawing.Point(581, 52);
            this.B_PAY_TYPE.Name = "B_PAY_TYPE";
            this.B_PAY_TYPE.Size = new System.Drawing.Size(30, 26);
            this.B_PAY_TYPE.TabIndex = 129;
            this.B_PAY_TYPE.TabStop = false;
            this.B_PAY_TYPE.Text = "...";
            this.B_PAY_TYPE.UseVisualStyleBackColor = true;
            this.B_PAY_TYPE.Click += new System.EventHandler(this.B_SPACKAGE_Click);
            // 
            // B_CASHTYPE
            // 
            this.B_CASHTYPE.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_CASHTYPE.Location = new System.Drawing.Point(258, 20);
            this.B_CASHTYPE.Name = "B_CASHTYPE";
            this.B_CASHTYPE.Size = new System.Drawing.Size(30, 26);
            this.B_CASHTYPE.TabIndex = 76;
            this.B_CASHTYPE.TabStop = false;
            this.B_CASHTYPE.Text = "...";
            this.B_CASHTYPE.UseVisualStyleBackColor = true;
            this.B_CASHTYPE.Click += new System.EventHandler(this.B_SPACKAGE_Click);
            // 
            // TXT_SLIP
            // 
            this.TXT_SLIP.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TXT_SLIP.Location = new System.Drawing.Point(393, 20);
            this.TXT_SLIP.Name = "TXT_SLIP";
            this.TXT_SLIP.Size = new System.Drawing.Size(182, 26);
            this.TXT_SLIP.TabIndex = 143;
            this.TXT_SLIP.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label30.Location = new System.Drawing.Point(13, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 14);
            this.label30.TabIndex = 109;
            this.label30.Text = "ชำระโดย";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label29.Location = new System.Drawing.Point(20, 58);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 14);
            this.label29.TabIndex = 115;
            this.label29.Text = "ธนาคาร";
            // 
            // CBO_BANK
            // 
            this.CBO_BANK.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.cMasterBindingSource, "ID", true));
            this.CBO_BANK.DataSource = this.cMasterBindingSource;
            this.CBO_BANK.DisplayMember = "DETAIL";
            this.CBO_BANK.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CBO_BANK.FormattingEnabled = true;
            this.CBO_BANK.Location = new System.Drawing.Point(70, 52);
            this.CBO_BANK.Name = "CBO_BANK";
            this.CBO_BANK.Size = new System.Drawing.Size(182, 26);
            this.CBO_BANK.TabIndex = 144;
            this.CBO_BANK.TabStop = false;
            this.CBO_BANK.ValueMember = "ID";
            // 
            // B_BANK
            // 
            this.B_BANK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.B_BANK.Location = new System.Drawing.Point(258, 52);
            this.B_BANK.Name = "B_BANK";
            this.B_BANK.Size = new System.Drawing.Size(30, 26);
            this.B_BANK.TabIndex = 92;
            this.B_BANK.TabStop = false;
            this.B_BANK.Text = "...";
            this.B_BANK.UseVisualStyleBackColor = true;
            this.B_BANK.Click += new System.EventHandler(this.B_SPACKAGE_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CB_PRINT_2);
            this.groupBox3.Controls.Add(this.CB_PRINT_3);
            this.groupBox3.Controls.Add(this.CB_PRINT_1);
            this.groupBox3.Location = new System.Drawing.Point(10, 427);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(636, 40);
            this.groupBox3.TabIndex = 44;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Print Option";
            // 
            // CB_PRINT_2
            // 
            this.CB_PRINT_2.AutoSize = true;
            this.CB_PRINT_2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CB_PRINT_2.ForeColor = System.Drawing.Color.Maroon;
            this.CB_PRINT_2.Location = new System.Drawing.Point(105, 14);
            this.CB_PRINT_2.Name = "CB_PRINT_2";
            this.CB_PRINT_2.Size = new System.Drawing.Size(127, 18);
            this.CB_PRINT_2.TabIndex = 135;
            this.CB_PRINT_2.TabStop = false;
            this.CB_PRINT_2.Text = "พิมพ์ใบสมาชิก+VAT";
            this.CB_PRINT_2.UseVisualStyleBackColor = true;
            // 
            // CB_PRINT_3
            // 
            this.CB_PRINT_3.AutoSize = true;
            this.CB_PRINT_3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CB_PRINT_3.ForeColor = System.Drawing.Color.Maroon;
            this.CB_PRINT_3.Location = new System.Drawing.Point(239, 14);
            this.CB_PRINT_3.Name = "CB_PRINT_3";
            this.CB_PRINT_3.Size = new System.Drawing.Size(95, 18);
            this.CB_PRINT_3.TabIndex = 134;
            this.CB_PRINT_3.TabStop = false;
            this.CB_PRINT_3.Text = "พิมพ์ใบสมาชิก";
            this.CB_PRINT_3.UseVisualStyleBackColor = true;
            this.CB_PRINT_3.Visible = false;
            // 
            // CB_PRINT_1
            // 
            this.CB_PRINT_1.AutoSize = true;
            this.CB_PRINT_1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.CB_PRINT_1.ForeColor = System.Drawing.Color.Maroon;
            this.CB_PRINT_1.Location = new System.Drawing.Point(16, 14);
            this.CB_PRINT_1.Name = "CB_PRINT_1";
            this.CB_PRINT_1.Size = new System.Drawing.Size(83, 18);
            this.CB_PRINT_1.TabIndex = 133;
            this.CB_PRINT_1.TabStop = false;
            this.CB_PRINT_1.Text = "พิมพ์ใบเสร็จ";
            this.CB_PRINT_1.UseVisualStyleBackColor = true;
            // 
            // cEmployeeBindingSource
            // 
            this.cEmployeeBindingSource.DataSource = typeof(Rakuten.Structure.CEmployee);
            // 
            // CMD_CANCEL
            // 
            this.CMD_CANCEL.Image = global::Rakuten.Properties.Resources.redo;
            this.CMD_CANCEL.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_CANCEL.Location = new System.Drawing.Point(578, 538);
            this.CMD_CANCEL.Name = "CMD_CANCEL";
            this.CMD_CANCEL.Size = new System.Drawing.Size(70, 35);
            this.CMD_CANCEL.TabIndex = 88;
            this.CMD_CANCEL.TabStop = false;
            this.CMD_CANCEL.Text = "ยกเลิก";
            this.CMD_CANCEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CMD_CANCEL.UseVisualStyleBackColor = true;
            this.CMD_CANCEL.Click += new System.EventHandler(this.CMD_CANCEL_Click);
            // 
            // CMD_SAVE
            // 
            this.CMD_SAVE.Image = global::Rakuten.Properties.Resources.disk_blue_ok;
            this.CMD_SAVE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CMD_SAVE.Location = new System.Drawing.Point(502, 538);
            this.CMD_SAVE.Name = "CMD_SAVE";
            this.CMD_SAVE.Size = new System.Drawing.Size(70, 35);
            this.CMD_SAVE.TabIndex = 5;
            this.CMD_SAVE.Text = "บันทึก";
            this.CMD_SAVE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CMD_SAVE.UseVisualStyleBackColor = true;
            this.CMD_SAVE.Click += new System.EventHandler(this.CMD_SAVE_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(56, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(401, 14);
            this.label2.TabIndex = 9;
            this.label2.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label8.Location = new System.Drawing.Point(56, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(195, 14);
            this.label8.TabIndex = 8;
            this.label8.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label9.Location = new System.Drawing.Point(24, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 7;
            this.label9.Text = "หมายเหตุ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label10.Location = new System.Drawing.Point(24, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 14);
            this.label10.TabIndex = 5;
            this.label10.Text = "ชื่อทรัพย์สิน";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(389, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "ลบ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(94, 63);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(370, 59);
            this.textBox1.TabIndex = 4;
            this.textBox1.TabStop = false;
            this.textBox1.Text = "XXXX";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(308, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "เพิ่ม";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(56, 209);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(401, 14);
            this.label11.TabIndex = 9;
            this.label11.Text = "การลบ เลือกทรัพย์สินที่ต้องการลบจากหน้า (แสดงรายการค้นหา) แล้ว (กดปุ่มลบ)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.Location = new System.Drawing.Point(56, 186);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "การเพิ่ม พิมพ์ชื่อทรัพย์สิน แล้ว (กดเพิ่ม)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(24, 161);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 14);
            this.label13.TabIndex = 7;
            this.label13.Text = "หมายเหตุ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.Location = new System.Drawing.Point(24, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 14);
            this.label14.TabIndex = 5;
            this.label14.Text = "ชื่อทรัพย์สิน";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(389, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "ลบ";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(94, 63);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(370, 59);
            this.textBox2.TabIndex = 4;
            this.textBox2.TabStop = false;
            this.textBox2.Text = "XXXX";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(308, 128);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "เพิ่ม";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.Location = new System.Drawing.Point(55, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(87, 14);
            this.label19.TabIndex = 78;
            this.label19.Text = "ระยะเวลาสมาชิก";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.Location = new System.Drawing.Point(7, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 14);
            this.label5.TabIndex = 32;
            this.label5.Text = "เพกเก็ต";
            // 
            // NUD_YEAR
            // 
            this.NUD_YEAR.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.NUD_YEAR.Location = new System.Drawing.Point(149, 47);
            this.NUD_YEAR.Name = "NUD_YEAR";
            this.NUD_YEAR.Size = new System.Drawing.Size(114, 26);
            this.NUD_YEAR.TabIndex = 9;
            this.NUD_YEAR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NUD_YEAR.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(90, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 25);
            this.label1.TabIndex = 147;
            this.label1.Text = "ชำระเงิน";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Rakuten.Properties.Resources.product;
            this.pictureBox1.Location = new System.Drawing.Point(12, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(72, 50);
            this.pictureBox1.TabIndex = 145;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(659, 50);
            this.pictureBox2.TabIndex = 146;
            this.pictureBox2.TabStop = false;
            // 
            // FrmPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(659, 577);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.CMD_CANCEL);
            this.Controls.Add(this.CMD_SAVE);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ชำระเงิน";
            this.Load += new System.EventHandler(this.FrmPayment_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cMemberBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cMasterBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GMember.ResumeLayout(false);
            this.GMember.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cPackageBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_YEAR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem CMS_Insert;
        private System.Windows.Forms.ToolStripMenuItem CMS_Update;
        private System.Windows.Forms.ToolStripMenuItem CMS_Delete;
        private System.Windows.Forms.BindingSource cMasterBindingSource;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox TXT_CHANGE;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox TXT_RECIEVE;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox TXT_TOTAL;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button B_BANK;
        private System.Windows.Forms.ComboBox CBO_BANK;
        private System.Windows.Forms.TextBox TXT_SLIP;
        private System.Windows.Forms.Button B_CASHTYPE;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox CBO_CASH_TYPE;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button CMD_CANCEL;
        private System.Windows.Forms.Button CMD_SAVE;
        private System.Windows.Forms.ComboBox CBO_ACCOUNT_CODE;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button B_PAY_TYPE;
        private System.Windows.Forms.CheckBox CB_PRINT_1;
        private System.Windows.Forms.CheckBox CB_PRINT_3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUD_YEAR;
        private System.Windows.Forms.ToolStripMenuItem CMS_MEMBER;
        private System.Windows.Forms.BindingSource cMemberBindingSource;
        private System.Windows.Forms.BindingSource cEmployeeBindingSource;
        private System.Windows.Forms.BindingSource cPackageBindingSource;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox GMember;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rUNNINGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dETAILDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn memberUseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn perCourseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn moneyUnitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lASTEDITDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kEYUSERDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fLAGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkDataGridViewTextBoxColumn;
        private System.Windows.Forms.CheckBox CB_PRINT_2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TXT_DISCOUNT;
        private System.Windows.Forms.ComboBox CBO_TYPE;
        private System.Windows.Forms.Label LSpecialPackage_Detail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;

    }
}