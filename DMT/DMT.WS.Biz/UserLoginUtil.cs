﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;
using DMT.WS.Dao;



namespace DMT.WS.Biz
{
    public class UserLoginUtil
    {
        public static bool isLogin()
        {
            string isLogin = "" + HttpContext.Current.Session["isLogin"];
            if (isLogin.Equals("true"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static bool authen(string username, string password)
        {
            UserLoginService service = new UserLoginService();
            return service.authen(username, password);
        }

        public static UserLoginBean getCurrentUserLogin()
        {
            return (UserLoginBean)HttpContext.Current.Session["userLoginBean"];
        }

        public static bool hasPermission(int[] userLevel)
        {
            if (getCurrentUserLogin() != null)
            {
                return userLevel.Contains(getCurrentUserLogin().Level);
            }
            else
            {
                return false;
            }
        }

        public static bool hasPermission(string permissionCode)
        {
            if (getCurrentUserLogin() != null)
            {
                int userLevel = getCurrentUserLogin().Level;
                if (userLevel == 1)
                {
                    return true;
                }
                else
                {
                    UserPermissionDTO dto = new UserPermissionDAO().getByUserTypeIdPermissionCode(userLevel, permissionCode);
                    if (dto != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

        }

        public static bool hasPermission(string[] permissionCodes)
        {
            if (getCurrentUserLogin() != null)
            {
                foreach (string permissionCode in permissionCodes)
                {
                    int userLevel = getCurrentUserLogin().Level;
                    UserPermissionDTO dto = new UserPermissionDAO().getByUserTypeIdPermissionCode(userLevel, permissionCode);
                    if (dto != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }
}