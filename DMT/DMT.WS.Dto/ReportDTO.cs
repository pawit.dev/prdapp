﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;

namespace DMT.WS.Dto
{
    public class ReportDTO : DefaultDTO
    {
        private int id;
        private string name;
        private string description;
        private string filePath;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }
    }
}