﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMT.WS.Dto
{
    public class PermissionDTO : DefaultDTO
    {
        private string permissionCode;

        public string PermissionCode
        {
            get { return permissionCode; }
            set { permissionCode = value; }
        }
        private string permissionGroup;

        public string PermissionGroup
        {
            get { return permissionGroup; }
            set { permissionGroup = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
       
    }
}