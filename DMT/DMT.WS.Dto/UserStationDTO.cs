﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMT.WS.Dto
{
    public class UserStationDTO : DefaultDTO
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int userLoginId;

        public int UserLoginId
        {
            get { return userLoginId; }
            set { userLoginId = value; }
        }
        private int stationId;

        public int StationId
        {
            get { return stationId; }
            set { stationId = value; }
        }

        private string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
       
    }
}