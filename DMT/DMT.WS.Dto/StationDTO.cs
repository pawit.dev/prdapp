﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DMT.WS.Dto;

namespace DMT.WS.Dto
{
    public class StationDTO : DefaultDTO
    {
        private int id;
        private string stationCode;
        private string stationNameTh;
        private string stationNameEn;
        private string description;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string StationCode
        {
            get { return stationCode; }
            set { stationCode = value; }
        }
        public string StationNameTh
        {
            get { return stationNameTh; }
            set { stationNameTh = value; }
        }
        public string StationNameEn
        {
            get { return stationNameEn; }
            set { stationNameEn = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }
}