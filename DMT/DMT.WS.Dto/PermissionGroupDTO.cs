﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMT.WS.Dto
{
    public class PermissionGroupDTO : DefaultDTO
    {
        private string groupCode;


        public string GroupCode
        {
            get { return groupCode; }
            set { groupCode = value; }
        }
        private string name;

        public string Name
        {
            get { return name; } 
            set { name = value; }
        }
        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
       
    }
}