﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMT.WS.Dto
{
    public class UserPermissionDTO : DefaultDTO
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int userTypeId;

        public int UserTypeId
        {
            get { return userTypeId; }
            set { userTypeId = value; }
        }
        private string permissionCode;

        public string PermissionCode
        {
            get { return permissionCode; }
            set { permissionCode = value; }
        }
       
    }
}