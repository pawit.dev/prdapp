﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace DMT.WS.Utils
{
    public class ConfigurationUtil
    {
        public static int USER_LEVEL_ADMIN = 1;
        public static int USER_LEVEL_MANAGER = 2;
        public static int USER_LEVEL_STAFF = 3;

        public static int USER_STATUS_INACTIVE = 0;
        public static int USER_STATUS_ACTIVE = 1;

        public static string IMG_PATH = "/";

        public static int ROW_PER_PAGE = 15;

        public static Dictionary<int, string> getUserLevel()
        {
            Dictionary<int, string> userLevels = new Dictionary<int, string>();
            userLevels.Add(USER_LEVEL_ADMIN, "Admin");
            userLevels.Add(USER_LEVEL_MANAGER, "Manager");
            userLevels.Add(USER_LEVEL_STAFF, "Staff");
            return userLevels;
        }

        public static Dictionary<int, string> getUserStatus()
        {
            Dictionary<int, string> userStatus = new Dictionary<int, string>();
            userStatus.Add(USER_STATUS_INACTIVE, "Inactive");
            userStatus.Add(USER_STATUS_ACTIVE, "Active");
            return userStatus;
        }


      
    }
}