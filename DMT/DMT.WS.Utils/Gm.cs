﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMT.WS.Utils
{
    public class Gm
    {
        public static string toSafeString(object obj)
        {
            if (obj is DBNull || obj == null)
                return "";
            if (obj is string)
                return (string)obj;

            try
            {
                return obj.ToString();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return "";
            }
        }

        public static double toSafeDouble(object obj)
        {
            if (obj is DBNull || obj == null)
                return 0;
            if (obj is double)
                return (double)obj;

            try
            {
                double n = 0;
                if (double.TryParse(obj.ToString(), out n))
                    return n;

                return 0;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return 0;
            }
        }

        public static int toSafeInt(object obj)
        {
            if (obj is DBNull || obj == null)
                return 0;
            if (obj is int)
                return (int)obj;

            return (int)toSafeDouble(obj);
        }

        // if failed return DateTime.MinValue
        public static DateTime toSafeDateTime(object obj)
        {
            if (obj is DBNull || obj == null)
                return DateTime.MinValue;

            if (obj is DateTime)
                return (DateTime)obj;

            string s = toSafeString(obj);
            try
            {
                DateTime d;
                if (DateTime.TryParse(s, out d))
                    return d;

                return DateTime.MinValue;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return DateTime.MinValue;
            }
        }

        // if failed return false
        public static bool toSafeBoolean(object obj)
        {
            if (obj is DBNull || obj == null)
                return false;

            if (obj is bool)
                return (bool)obj;

            string s = toSafeString(obj).ToUpper();

            if (s.Equals("1") || s.Equals("TRUE"))
                return true;
            else
                return false;
        }


        // dd/mm/yyyy ==> yyyy-mm-dd
        public static String toThaiDateToSqlDate(string ddmmyyyy)
        {
            string[] ss = ddmmyyyy.Split('/');
            if (ss.Length != 3)
                return "";

            return ss[2] + "-" + ss[1] + "-" + ss[0];
        }

        public static String toThaiDateTimeToSqlDate(string ddMMyyyyHHmm)
        {
            string[] ss = ddMMyyyyHHmm.Split(' ');
            if (ss.Length != 2)
                return "";
            string[] sss = ss[0].Split('/');
            if (sss.Length != 3)
                return "";
            return sss[2] + "-" + sss[1] + "-" + sss[0] + " " + ss[1];
        }

        // H:i:s.ms
        public static String toThaiDateToSqlTime(string hr, string min)
        {
            return hr + ":" + min + ":00.000";
        }
    }
}
