﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using DMT.WS.Dto;


namespace DMT.WS.Dao
{
    public class LaneDAO:DefaultDAO
    {

        public override DefaultDTO save(DefaultDTO stationDTO)
        {
            if (!(stationDTO is StationDTO))
                throw new WrongTypeException(this, "StationDTO", stationDTO);

            string insertSQL = @"INSERT INTO [Station]
                                   ([StationCode]
                                   ,[StationNameTh]
                                   ,[StationNameEn]
                                   ,[Description])
                                VALUES
                                   (@StationCode
                                   ,@StationNameTh
                                   ,@StationNameEn
                                   ,@Description)";

            StationDTO dto = stationDTO as StationDTO;

            bool result = false;
            SqlCommand cmd = new SqlCommand(insertSQL);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@StationCode", System.Data.SqlDbType.NVarChar).Value = dto.StationCode;
            cmd.Parameters.Add("@StationNameTh", System.Data.SqlDbType.NVarChar).Value = dto.StationNameTh;
            cmd.Parameters.Add("@StationNameEn", System.Data.SqlDbType.NVarChar).Value = dto.StationNameEn;
            cmd.Parameters.Add("@Description", System.Data.SqlDbType.NVarChar).Value = dto.Description;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                result = true;
            }
            connection.CloseConnection();

            if (result)
                return dto;
            else
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count == 1 && pks.ContainsKey("ID")))
                throw new Exception("StationDAO getByPrimaryKey need only ID.");

            int id = toInt(pks["ID"]);
            return getByPrimaryKey(id);
        }

        public DefaultDTO getByPrimaryKey(int id)
        {
            string sql = "SELECT * FROM Station Lane ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            LaneDTO laneDto = null;
            if (data.Read())
            {
                laneDto = new LaneDTO();
                laneDto.ID = toInt(data["ID"]);
                laneDto.StationId = toInt(data["StationID"]);
                laneDto.LaneName = toStr(data["LaneName"]);
            }
            connection.CloseConnection();
            return laneDto;
        }
        public override bool update(DefaultDTO stationDTO)
        {
            if (!(stationDTO is StationDTO))
                throw new WrongTypeException(this, "StationDTO", stationDTO);

            StationDTO dto = stationDTO as StationDTO;

            bool updateResult = false;
            if (dto != null && dto.ID > 0)
            {
                string sql = "UPDATE Station SET [StationNameTh] = @StationNameTh, " +
                    "[StationNameEn] = @StationNameEn, " +
                    "[Description] = @Description " +
                    "WHERE ID = @id";
                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = dto.ID;
                cmd.Parameters.Add("@StationNameTh", System.Data.SqlDbType.NVarChar).Value = dto.StationNameTh;
                cmd.Parameters.Add("@StationNameEn", System.Data.SqlDbType.NVarChar).Value = dto.StationNameEn;
                cmd.Parameters.Add("@Description", System.Data.SqlDbType.NVarChar).Value = dto.Description;
                ConnectionDB connection = new ConnectionDB();
                connection.OpenConnection();
                int row = connection.ExecuteNonQuery(cmd);

                if (row > 0)
                {
                    updateResult = true;
                }
                connection.CloseConnection();
            }
            return updateResult;
        }

        public override bool delete(DefaultDTO stationDTO)
        {
            if (!(stationDTO is StationDTO))
                throw new WrongTypeException(this, "StationDTO", stationDTO);

            StationDTO dto = stationDTO as StationDTO;
            int id = dto.ID;

            return deleteById(id);
        }


        public bool deleteById(int id)
        {
            bool deleteResult = false;
            string sql = "DELETE Station WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }

        public List<LaneDTO> getAll()
        {
            string sql = "SELECT * FROM Lane Where StationID=1";
            SqlCommand cmd = new SqlCommand(sql);

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<LaneDTO> lanes = new List<LaneDTO>();

            while (data.Read())
            {
                LaneDTO laneDto = new LaneDTO();
                laneDto.ID = toInt(data["ID"]);
                laneDto.StationId = toInt(data["StationID"]);
                laneDto.LaneName = toStr(data["LaneName"]);
                lanes.Add(laneDto);
            }
            connection.CloseConnection();
            return lanes;
            
        }

        //public List<StationDTO> getByStation(int stationId)
        //{
        //    string sql = "SELECT * FROM Lane where StationID="+stationId;
        //    SqlCommand cmd = new SqlCommand(sql);

        //    ConnectionDB connection = new ConnectionDB();
        //    connection.OpenConnection();
        //    SqlDataReader data = connection.ExecuteQuery(cmd);

        //    List<StationDTO> stations = new List<StationDTO>();

        //    while (data.Read())
        //    {
        //        LaneDTO laneDto = new LaneDTO();
        //        laneDto.ID = toInt(data["ID"]);
        //        laneDto.StationId = toInt(data["StationID"]);
        //        laneDto.LaneName = toStr(data["LaneName"]);
        //    }
        //    connection.CloseConnection();
        //    return stations;

        //}

        public List<LaneDTO> getLaneByStation(int stationId)
        {
            List<LaneDTO> lists = new List<LaneDTO>();
            string sql = "SELECT * FROM Lane where StationID=" + stationId;
            SqlCommand cmd = new SqlCommand(sql);

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<StationDTO> stations = new List<StationDTO>();

            while (data.Read())
            {
                LaneDTO laneDto = new LaneDTO();
                laneDto.ID = toInt(data["ID"]);
                laneDto.StationId = toInt(data["StationID"]);
                laneDto.LaneName = toStr(data["LaneName"]);
                lists.Add(laneDto);
            }
            connection.CloseConnection();
            return lists;

        }
    }
}