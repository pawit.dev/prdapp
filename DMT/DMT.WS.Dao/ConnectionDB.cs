﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using DMT.WS.Utils;

namespace DMT.WS.Dao
{
    public class ConnectionDB
    {
        private SqlConnection connection = null;

        public ConnectionDB()
        {
            connection = new SqlConnection(Configurations.ConnectionString);
        }
        public SqlConnection GetConnection()
        {
            return this.connection;
        }
        public int ExecuteNonQuery(string sql)
        {
            SqlCommand command = new SqlCommand(sql, connection);
            return command.ExecuteNonQuery();
        }

        public int ExecuteNonQuery(SqlCommand cmd)
        {
            cmd.Connection = connection;
            return cmd.ExecuteNonQuery();
        }

        public SqlDataReader ExecuteQuery(string sql)
        {
            SqlCommand command = new SqlCommand(sql, connection);
            return command.ExecuteReader();
        }

        public SqlDataReader ExecuteQuery(SqlCommand cmd)
        {
            cmd.Connection = connection;
            return cmd.ExecuteReader();
        }

        public void OpenConnection()
        {
            this.connection.Open();
        }
        public void CloseConnection()
        {
            this.connection.Close();
        }
    }
}