﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using DMT.WS.Dao;
using DMT.WS.Dto;
using DMT.WS.Utils;


namespace DMT.WS.Dao
{
    public class UserLoginDAO : DefaultDAO
    {
        public override DefaultDTO save(DefaultDTO userLoginDTO)
        {
            if (!(userLoginDTO is UserLoginDTO))
                throw new WrongTypeException(this, "UserLoginDTO", userLoginDTO);

            string insertSQL = @"INSERT INTO [UserLogin]
                                   ([Username]
                                   ,[Password]
                                   ,[FullName]
                                   ,[Email]
                                   ,[Status]
                                   ,[UserLevel])
                                VALUES
                                   (@Username
                                   ,@Password
                                   ,@FullName
                                   ,@Email
                                   ,@Status
                                   ,@UserLevel)";

            UserLoginDTO dto = userLoginDTO as UserLoginDTO;

            bool result = false;
            SqlCommand cmd = new SqlCommand(insertSQL);
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.Add("@ID", System.Data.SqlDbType.Int).Value = dto.ID;
            cmd.Parameters.Add("@Username", System.Data.SqlDbType.VarChar).Value = dto.Username;
            cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar).Value = dto.Password;
            cmd.Parameters.Add("@FullName", System.Data.SqlDbType.VarChar).Value = dto.FullName;
            cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar).Value = dto.Email;
            cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int).Value = dto.Status;
            cmd.Parameters.Add("@UserLevel", System.Data.SqlDbType.Int).Value = dto.Level;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                result = true;
                UserLoginDTO u = getByUsername(dto.Username) as UserLoginDTO;
                dto.ID = u.ID;
            }
            connection.CloseConnection();

            if (result)
                return dto;
            else
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count == 1 && pks.ContainsKey("ID")))
                throw new Exception("UserLoginDAO getByPrimaryKey need only ID.");

            int id = toInt(pks["ID"]);
            return getByPrimaryKey(id);
        }

        public DefaultDTO getByPrimaryKey(int id)
        {
            string sql = "SELECT * FROM UserLogin WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            UserLoginDTO userLoginDTO = null;
            if (data.Read())
            {
                userLoginDTO = new UserLoginDTO();
                userLoginDTO.ID = toInt(data["ID"]);
                userLoginDTO.Username = toStr(data["Username"]);
                userLoginDTO.Password = toStr(data["Password"]);
                userLoginDTO.Email = toStr(data["Email"]);
                userLoginDTO.FullName = toStr(data["FullName"]);
                userLoginDTO.Status = toInt(data["Status"]);
                userLoginDTO.Level = toInt(data["UserLevel"]);
            }
            connection.CloseConnection();
            return userLoginDTO;
        }

        public DefaultDTO getByUsername(string username)
        {
            string sql = "SELECT * FROM UserLogin WHERE Username = @Username";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@Username", System.Data.SqlDbType.VarChar).Value = username;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            UserLoginDTO userLoginDTO = null;
            if (data.Read())
            {
                userLoginDTO = new UserLoginDTO();
                userLoginDTO.ID = toInt(data["ID"]);
                userLoginDTO.Username = toStr(data["Username"]);
                userLoginDTO.Password = toStr(data["Password"]);
                userLoginDTO.Email = toStr(data["Email"]);
                userLoginDTO.FullName = toStr(data["FullName"]);
                userLoginDTO.Status = toInt(data["Status"]);
                userLoginDTO.Level = toInt(data["UserLevel"]);
            }
            connection.CloseConnection();
            return userLoginDTO;
        }

        public DefaultDTO getByPrimaryUsernameAndPassword(string username, string password)
        {
            string sql = "SELECT * FROM UserLogin WHERE Username = @username and Password = @password and Status = @status";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = username;
            cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = password;
            cmd.Parameters.Add("@status", System.Data.SqlDbType.Int).Value = ConfigurationUtil.USER_STATUS_ACTIVE;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            UserLoginDTO userLoginDTO = null;
            if (data.Read())
            {
                userLoginDTO = new UserLoginDTO();
                userLoginDTO.ID = toInt(data["ID"]);
                userLoginDTO.Username = toStr(data["Username"]);
                userLoginDTO.Password = toStr(data["Password"]);
                userLoginDTO.Email = toStr(data["Email"]);
                userLoginDTO.FullName = toStr(data["FullName"]);
                userLoginDTO.Status = toInt(data["Status"]);
                userLoginDTO.Level = toInt(data["UserLevel"]);
            }
            connection.CloseConnection();
            return userLoginDTO;
        }


        public override bool update(DefaultDTO userLoginDTO)
        {
            if (!(userLoginDTO is UserLoginDTO))
                throw new WrongTypeException(this, "UserLoginDTO", userLoginDTO);

            UserLoginDTO dto = userLoginDTO as UserLoginDTO;

            bool updateResult = false;
            if (dto != null && dto.ID > 0)
            {
                string sql = "UPDATE UserLogin SET Username = @Username, " +
                    ((dto.Password.Length == 0) ?  "" : "Password = @Password, ") +
                    "FullName = @FullName, " +
                    "Email = @Email, " +
                    "Status = @Status, " +
                    "UserLevel = @UserLevel " +
                    "WHERE ID = @id";
                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = dto.ID;
                cmd.Parameters.Add("@Username", System.Data.SqlDbType.VarChar).Value = dto.Username;
                if (dto.Password.Length > 0)
                    cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar).Value = dto.Password;
                cmd.Parameters.Add("@FullName", System.Data.SqlDbType.VarChar).Value = dto.FullName;
                cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar).Value = dto.Email;
                cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int).Value = dto.Status;
                cmd.Parameters.Add("@UserLevel", System.Data.SqlDbType.Int).Value = dto.Level;

                ConnectionDB connection = new ConnectionDB();
                connection.OpenConnection();
                int row = connection.ExecuteNonQuery(cmd);

                if (row > 0)
                {
                    updateResult = true;
                }
                connection.CloseConnection();
            }
            return updateResult;
        }

        public override bool delete(DefaultDTO userLoginDTO)
        {
            if (!(userLoginDTO is UserLoginDTO))
                throw new WrongTypeException(this, "UserLoginDTO", userLoginDTO);

            UserLoginDTO dto = userLoginDTO as UserLoginDTO;
            int id = dto.ID;

            return deleteById(id);
        }


        public bool deleteById(int id)
        {
            bool deleteResult = false;
            string sql = "DELETE UserLogin WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }
    }
}