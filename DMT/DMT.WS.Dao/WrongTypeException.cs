﻿using System;

namespace DMT.WS.Dao
{
    public class WrongTypeException : DefaultException
    {
        private string message = "";

        public WrongTypeException(object sender, String expectedType, object instance)
        {
            level = 5; // important

            message = "Wrong Type Exception from ["
                + sender.GetType().Name + "] : expect [" + expectedType + "] : type is [" + instance.GetType().Name + "]";
        }

        public override string Message
        {
            get
            {
                return message;
            }
        }
    }
}