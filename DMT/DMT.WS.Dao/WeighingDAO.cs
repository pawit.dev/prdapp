﻿using System;
using System.Collections.Generic;

using System.Data.SqlClient;


using DMT.WS.Dto;
using DMT.WS.Utils;
using System.IO;
using System.Net;

namespace DMT.WS.Dao
{
    public class WeighingDAO : DefaultDAO
    {

        public override DefaultDTO save(DefaultDTO weightingDTO)
        {
            if (!(weightingDTO is WeighingDTO))
                throw new WrongTypeException(this, "WeighingDTO", weightingDTO);

            string insertSQL = @"INSERT INTO [Weighing]
                                   (
                                   [TimeStamp]
                                   ,[RunningNumber]
                                   ,[StationID]
                                   ,[LaneID]
                                   ,[NumberAxles]
                                   ,[Weight_Axle01]
                                   ,[Weight_Axle02]
                                   ,[Weight_Axle03]
                                   ,[Weight_Axle04]
                                   ,[Weight_Axle05]
                                   ,[Weight_Axle06]
                                   ,[Weight_Axle07]
                                   ,[Weight_Axle08]
                                   ,[Weight_Axle09]
                                   ,[Weight_Axle10]
                                   ,[Weight_GVW]
                                   ,[Weight_MaxGVW]
                                   ,[Weight_MaxAXW]
                                   ,[IsOverWeight]
                                   ,[RecordStatus]
                                   ,[Image01]
                                   ,[Image02])
                                VALUES
                                   (
                                   @TimeStamp
                                   ,@RunningNumber
                                   ,@StationID
                                   ,@LaneID
                                   ,@NumberAxles
                                   ,@Weight_Axle01
                                   ,@Weight_Axle02
                                   ,@Weight_Axle03
                                   ,@Weight_Axle04
                                   ,@Weight_Axle05
                                   ,@Weight_Axle06
                                   ,@Weight_Axle07
                                   ,@Weight_Axle08
                                   ,@Weight_Axle09
                                   ,@Weight_Axle10
                                   ,@Weight_GVW
                                   ,@Weight_MaxGVW
                                   ,@Weight_MaxAXW
                                   ,@IsOverWeight
                                   ,@RecordStatus
                                   ,@Image01
                                   ,@Image02)";

            WeighingDTO dto = weightingDTO as WeighingDTO;

            bool result = false;
            SqlCommand cmd = new SqlCommand(insertSQL);
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = dto.ID;
            cmd.Parameters.Add("@TimeStamp", System.Data.SqlDbType.DateTime).Value = dto.Timestamp;
            cmd.Parameters.Add("@RunningNumber", System.Data.SqlDbType.Int).Value = dto.RunningNumber;
            cmd.Parameters.Add("@StationID", System.Data.SqlDbType.Int).Value = dto.StationID;
            cmd.Parameters.Add("@LaneID", System.Data.SqlDbType.Int).Value = dto.LaneID;
            cmd.Parameters.Add("@NumberAxles", System.Data.SqlDbType.Int).Value = dto.NumberAxles;
            cmd.Parameters.Add("@Weight_Axle01", System.Data.SqlDbType.Int).Value = dto.WeightAxle01;
            cmd.Parameters.Add("@Weight_Axle02", System.Data.SqlDbType.Int).Value = dto.WeightAxle02;
            cmd.Parameters.Add("@Weight_Axle03", System.Data.SqlDbType.Int).Value = dto.WeightAxle03;
            cmd.Parameters.Add("@Weight_Axle04", System.Data.SqlDbType.Int).Value = dto.WeightAxle04;
            cmd.Parameters.Add("@Weight_Axle05", System.Data.SqlDbType.Int).Value = dto.WeightAxle05;
            cmd.Parameters.Add("@Weight_Axle06", System.Data.SqlDbType.Int).Value = dto.WeightAxle06;
            cmd.Parameters.Add("@Weight_Axle07", System.Data.SqlDbType.Int).Value = dto.WeightAxle07;
            cmd.Parameters.Add("@Weight_Axle08", System.Data.SqlDbType.Int).Value = dto.WeightAxle08;
            cmd.Parameters.Add("@Weight_Axle09", System.Data.SqlDbType.Int).Value = dto.WeightAxle09;
            cmd.Parameters.Add("@Weight_Axle10", System.Data.SqlDbType.Int).Value = dto.WeightAxle10;
            cmd.Parameters.Add("@Weight_GVW", System.Data.SqlDbType.Int).Value = dto.WeightGVW;
            cmd.Parameters.Add("@Weight_MaxGVW", System.Data.SqlDbType.Int).Value = dto.WeightMaxGVW;
            cmd.Parameters.Add("@Weight_MaxAXW", System.Data.SqlDbType.Int).Value = dto.WeightMaxAXW;
            cmd.Parameters.Add("@IsOverWeight", System.Data.SqlDbType.Int).Value = (dto.IsOverWeight) ? 1 : 0;
            cmd.Parameters.Add("@RecordStatus", System.Data.SqlDbType.NChar).Value = dto.RecordStatus;
            cmd.Parameters.Add("@Image01", System.Data.SqlDbType.NChar).Value = dto.Image01;
            cmd.Parameters.Add("@Image02", System.Data.SqlDbType.NChar).Value = dto.Image02;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                result = true;
            }
            connection.CloseConnection();

            if (result)
                return dto;
            else
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count==1 && pks.ContainsKey("ID")))
                throw new Exception("WeighingDAO getByPrimaryKey need only ID.");

            int id = toInt(pks["ID"]);
            return getByPrimaryKey(id);
        }

        public DefaultDTO getByPrimaryKey(int id)
        {
            var webClient = new WebClient();

            string sql = "SELECT * FROM Weighing WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            WeighingDTO weighingDTO = null;
            if (data.Read())
            {
                weighingDTO     = new WeighingDTO();
                weighingDTO.ID  = toInt(data["ID"]);
                weighingDTO.Timestamp    = toDateTime(data["TimeStamp"]);
                weighingDTO.RunningNumber = toInt(data["RunningNumber"]);
                weighingDTO.StationID    = toInt(data["StationID"]);
                weighingDTO.LaneID       = toInt(data["LaneID"]);
                weighingDTO.NumberAxles  = toInt(data["NumberAxles"]);
                weighingDTO.WeightAxle01 = toInt(data["Weight_Axle01"]);
                weighingDTO.WeightAxle02 = toInt(data["Weight_Axle02"]);
                weighingDTO.WeightAxle03 = toInt(data["Weight_Axle03"]);
                weighingDTO.WeightAxle04 = toInt(data["Weight_Axle04"]);
                weighingDTO.WeightAxle05 = toInt(data["Weight_Axle05"]);
                weighingDTO.WeightAxle06 = toInt(data["Weight_Axle06"]);
                weighingDTO.WeightAxle07 = toInt(data["Weight_Axle07"]);
                weighingDTO.WeightAxle08 = toInt(data["Weight_Axle08"]);
                weighingDTO.WeightAxle09 = toInt(data["Weight_Axle09"]);
                weighingDTO.WeightAxle10 = toInt(data["Weight_Axle10"]);
                weighingDTO.WeightGVW    = toInt(data["Weight_GVW"]);
                weighingDTO.WeightMaxGVW = toInt(data["Weight_MaxGVW"]);
                weighingDTO.WeightMaxAXW = toInt(data["Weight_MaxAXW"]);
                weighingDTO.IsOverWeight = toBoolean(data["IsOverWeight"]);
                weighingDTO.RecordStatus = toStr(data["RecordStatus"]);
                weighingDTO.Image01 = Configurations.ImageFilePathURL + toStr(data["Image01"]);
                weighingDTO.Image02 = Configurations.ImageFilePathURL + toStr(data["Image02"]);
                if (weighingDTO.Image01 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image01)))
                    {
                        try
                        {
                            weighingDTO.img1 = webClient.DownloadData(weighingDTO.Image01);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                if (weighingDTO.Image02 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image02)))
                    {
                        try
                        {
                            weighingDTO.img2 = webClient.DownloadData(weighingDTO.Image02);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                weighingDTO.sign = (weighingDTO.IsOverWeight) ? "/DMT/images/ico_circle_red.png" : "/DMT/images/ico_circle_green.png";
                weighingDTO.logo = Configurations.ImageFilePathURL + "DMTWIM/images/logo.jpg";
            }           
            connection.CloseConnection();
            return weighingDTO;
        }
        public override bool update(DefaultDTO weightingDTO)
        {
            if (!(weightingDTO is WeighingDTO))
                throw new WrongTypeException(this, "WeighingDTO", weightingDTO);

            WeighingDTO dto = weightingDTO as WeighingDTO;

            bool updateResult = false;
            if (dto != null && dto.ID > 0)
            {
                string sql = "UPDATE Weighing SET TimeStamp = @TimeStamp, " +
                    "RunningNumber = @RunningNumber, " +
                    "StationID = @StationID, " +
                    "LaneID = @LaneID, " +
                    "NumberAxles = @NumberAxles, " +
                    "Weight_Axle01 = @Weight_Axle01, " +
                    "Weight_Axle02 = @Weight_Axle02, " +
                    "Weight_Axle03 = @Weight_Axle03, " +
                    "Weight_Axle04 = @Weight_Axle04, " +
                    "Weight_Axle05 = @Weight_Axle05, " +
                    "Weight_Axle06 = @Weight_Axle06, " +
                    "Weight_Axle07 = @Weight_Axle07, " +
                    "Weight_Axle08 = @Weight_Axle08, " +
                    "Weight_Axle09 = @Weight_Axle09, " +
                    "Weight_Axle10 = @Weight_Axle10, " +
                    "Weight_GVW    = @Weight_GVW, " +
                    "Weight_MaxGVW = @Weight_MaxGVW, " +
                    "Weight_MaxAXW = @Weight_MaxAXW, " +
                    "IsOverWeight = @IsOverWeight, " +
                    "RecordStatus = @RecordStatus, " +
                    "Image01 = @Image01, " +
                    "Image02 = @Image02 " +
                    "WHERE ID = @id";
                SqlCommand cmd = new SqlCommand(sql);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = dto.ID;
                cmd.Parameters.Add("@TimeStamp", System.Data.SqlDbType.DateTime).Value = dto.Timestamp;
                cmd.Parameters.Add("@RunningNumber", System.Data.SqlDbType.Int).Value = dto.RunningNumber;
                cmd.Parameters.Add("@StationID", System.Data.SqlDbType.Int).Value = dto.StationID;
                cmd.Parameters.Add("@LaneID",    System.Data.SqlDbType.Int).Value = dto.LaneID;
                cmd.Parameters.Add("@NumberAxles", System.Data.SqlDbType.Int).Value = dto.NumberAxles;
                cmd.Parameters.Add("@Weight_Axle01", System.Data.SqlDbType.Int).Value = dto.WeightAxle01;
                cmd.Parameters.Add("@Weight_Axle02", System.Data.SqlDbType.Int).Value = dto.WeightAxle02;
                cmd.Parameters.Add("@Weight_Axle03", System.Data.SqlDbType.Int).Value = dto.WeightAxle03;
                cmd.Parameters.Add("@Weight_Axle04", System.Data.SqlDbType.Int).Value = dto.WeightAxle04;
                cmd.Parameters.Add("@Weight_Axle05", System.Data.SqlDbType.Int).Value = dto.WeightAxle05;
                cmd.Parameters.Add("@Weight_Axle06", System.Data.SqlDbType.Int).Value = dto.WeightAxle06;
                cmd.Parameters.Add("@Weight_Axle07", System.Data.SqlDbType.Int).Value = dto.WeightAxle07;
                cmd.Parameters.Add("@Weight_Axle08", System.Data.SqlDbType.Int).Value = dto.WeightAxle08;
                cmd.Parameters.Add("@Weight_Axle09", System.Data.SqlDbType.Int).Value = dto.WeightAxle09;
                cmd.Parameters.Add("@Weight_Axle10", System.Data.SqlDbType.Int).Value = dto.WeightAxle10;
                cmd.Parameters.Add("@Weight_GVW", System.Data.SqlDbType.Int).Value = dto.WeightGVW;
                cmd.Parameters.Add("@Weight_MaxGVW", System.Data.SqlDbType.Int).Value = dto.WeightMaxGVW;
                cmd.Parameters.Add("@Weight_MaxAXW", System.Data.SqlDbType.Int).Value = dto.WeightMaxAXW;
                cmd.Parameters.Add("@IsOverWeight", System.Data.SqlDbType.Int).Value = (dto.IsOverWeight) ? 1 : 0;
                cmd.Parameters.Add("@RecordStatus", System.Data.SqlDbType.NChar).Value = dto.RecordStatus;
                cmd.Parameters.Add("@Image01", System.Data.SqlDbType.NChar).Value = dto.Image01;
                cmd.Parameters.Add("@Image02", System.Data.SqlDbType.NChar).Value = dto.Image02;

                ConnectionDB connection = new ConnectionDB();
                connection.OpenConnection();
                int row = connection.ExecuteNonQuery(cmd);

                if (row > 0)
                {
                    updateResult = true;
                }
                connection.CloseConnection();
            }
            return updateResult;
        }

        public override bool delete(DefaultDTO weightingDTO)
        {
            if (!(weightingDTO is WeighingDTO))
                throw new WrongTypeException(this, "WeighingDTO", weightingDTO);

            WeighingDTO dto = weightingDTO as WeighingDTO;
            int id = dto.ID;

            return deleteById(id);
        }


        public bool deleteById(int id)
        {
            bool deleteResult = false;
            string sql = "DELETE Weighing WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }

        public List<WeighingDTO> getAll(string stationId,string laneId, string fromDate, string toDate, string isOverWeight, int page)
        {
            int rowPerPage = ConfigurationUtil.ROW_PER_PAGE;
            return getAll(stationId,laneId, fromDate, toDate, isOverWeight, page, rowPerPage);
        }

        public List<WeighingDTO> getAll(string stationId,string laneId, string fromDate, string toDate, string isOverWeight, int page, int rowPerPage)
        {
            var webClient = new WebClient();
            if (page < 1)
            {
                page = 1;
            }

            string sql = "SELECT * FROM ( ";
            sql += "SELECT ROW_NUMBER() OVER(ORDER BY ID) AS [RowNum], * FROM Weighing WHERE 1=1 ";
            if (stationId != null && stationId != "")
            {
                sql += "AND StationID = @StationID ";
            }
            if (laneId != null && laneId != "")
            {
                sql += "AND LaneID = @LaneId ";
            }
            if (fromDate != null && fromDate != "")
            {
                sql += "AND TimeStamp >= '" + fromDate + "' ";
            }
            if (toDate != null && toDate != "")
            {
                sql += "AND TimeStamp <= '" + toDate + "' ";
            }
            if (isOverWeight != null && isOverWeight != "")
            {
                sql += "AND IsOverWeight = @IsOverWeight ";
            }
            sql += ") A ";
            if ( rowPerPage > 0)
            {
                int endRow = page * rowPerPage;
                int startRow = (page * rowPerPage) - rowPerPage + 1;
                sql += "WHERE [RowNum] BETWEEN " + startRow + " AND " + endRow+"";
            }
            sql += " ORDER BY TimeStamp DESC";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            if (stationId != null && stationId != "")
            {
                cmd.Parameters.Add("@StationID", System.Data.SqlDbType.Int).Value = toInt(stationId);
            }
            if (laneId != null && laneId != "")
            {
                cmd.Parameters.Add("@LaneId", System.Data.SqlDbType.Int).Value = toInt(laneId);
            }
            /*if (fromDate != null && fromDate != "")
            {
                cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.Text).Value = toStr(fromDate);
            }
            if (toDate != null && toDate != "")
            {
                cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.Text).Value = toStr(toDate);
            }*/
            if (isOverWeight != null && isOverWeight != "")
            {
                cmd.Parameters.Add("@IsOverWeight", System.Data.SqlDbType.Bit).Value = toInt(isOverWeight);
            }

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<WeighingDTO> weighingDTOs = new List<WeighingDTO>();
            while (data.Read())
            {
                WeighingDTO weighingDTO = new WeighingDTO();
                weighingDTO.ID = toInt(data["ID"]);
                weighingDTO.Timestamp = toDateTime(data["TimeStamp"]);
                weighingDTO.RunningNumber = toInt(data["RunningNumber"]);
                weighingDTO.StationID = toInt(data["StationID"]);
                weighingDTO.LaneID = toInt(data["LaneID"]);
                //weighingDTO.stationName =
                weighingDTO.NumberAxles = toInt(data["NumberAxles"]);
                weighingDTO.WeightAxle01 = toInt(data["Weight_Axle01"]);
                weighingDTO.WeightAxle02 = toInt(data["Weight_Axle02"]);
                weighingDTO.WeightAxle03 = toInt(data["Weight_Axle03"]);
                weighingDTO.WeightAxle04 = toInt(data["Weight_Axle04"]);
                weighingDTO.WeightAxle05 = toInt(data["Weight_Axle05"]);
                weighingDTO.WeightAxle06 = toInt(data["Weight_Axle06"]);
                weighingDTO.WeightAxle07 = toInt(data["Weight_Axle07"]);
                weighingDTO.WeightAxle08 = toInt(data["Weight_Axle08"]);
                weighingDTO.WeightAxle09 = toInt(data["Weight_Axle09"]);
                weighingDTO.WeightAxle10 = toInt(data["Weight_Axle10"]);
                weighingDTO.WeightGVW = toInt(data["Weight_GVW"]);
                weighingDTO.WeightMaxGVW = toInt(data["Weight_MaxGVW"]);
                weighingDTO.WeightMaxAXW = toInt(data["Weight_MaxAXW"]);
                weighingDTO.IsOverWeight = toBoolean(data["IsOverWeight"]);
                weighingDTO.RecordStatus = toStr(data["RecordStatus"]);
                weighingDTO.Image01 = Configurations.ImageFilePathURL + toStr(data["Image01"]);
                weighingDTO.Image02 = Configurations.ImageFilePathURL + toStr(data["Image02"]);

                if (weighingDTO.Image01 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image01)))
                    {
                        try
                        {
                            weighingDTO.img1 = webClient.DownloadData(weighingDTO.Image01);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                if (weighingDTO.Image02 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image02)))
                    {
                        try
                        {
                            weighingDTO.img2 = webClient.DownloadData(weighingDTO.Image02);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                weighingDTOs.Add(weighingDTO);


            }
            connection.CloseConnection();
            return weighingDTOs;
        }

        public string getSqlQuery(string stationId, string fromDate, string toDate, string isOverWeight, int page)
        {
            var webClient = new WebClient();
            if (page < 1)
            {
                page = 1;
            }
            int rowPerPage = ConfigurationUtil.ROW_PER_PAGE;
            int endRow = page * rowPerPage;
            int startRow = (page * rowPerPage) - rowPerPage + 1;

            string sql = "SELECT * FROM ( ";
            sql += "SELECT ROW_NUMBER() OVER(ORDER BY ID) AS [RowNum], * FROM Weighing WHERE 1=1 ";
            if (stationId != null && stationId != "")
            {
                sql += "AND StationID = @StationID ";
            }
            if (fromDate != null && fromDate != "")
            {
                sql += "AND TimeStamp >= '" + fromDate + "' ";
            }
            if (toDate != null && toDate != "")
            {
                sql += "AND TimeStamp <= '" + toDate + "' ";
            }
            if (isOverWeight != null && isOverWeight != "")
            {
                sql += "AND IsOverWeight = @IsOverWeight ";
            }
            sql += ") A WHERE [RowNum] BETWEEN " + startRow + " AND " + endRow;


            return sql;
        }

        public List<WeighingDTO> getAll(string stationId, string fromDate, string toDate, string isOverWeight)
        {
            var webClient = new WebClient();
            string sql = "SELECT * FROM Weighing WHERE 1=1 ";
            if (stationId != null && stationId != "")
            {
                sql += "AND StationID = @StationID ";
            }
            if (fromDate != null && fromDate != "")
            {
                sql += "AND TimeStamp >= '" + fromDate + "' ";
            }
            if (toDate != null && toDate != "")
            {
                sql += "AND TimeStamp <= '" + toDate + "' ";
            }
            if (isOverWeight != null && isOverWeight != "")
            {
                sql += "AND IsOverWeight = @IsOverWeight ";
            }


            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            if (stationId != null && stationId != "")
            {
                cmd.Parameters.Add("@StationID", System.Data.SqlDbType.Int).Value = toInt(stationId);
            }
            /*
            if (fromDate != null && fromDate != "")
            {
                cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = toDateTime(fromDate);
            }
            if (toDate != null && toDate != "")
            {
                cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = toDateTime(toDate);
            }
             */
            if (isOverWeight != null && isOverWeight != "")
            {
                cmd.Parameters.Add("@IsOverWeight", System.Data.SqlDbType.Bit).Value = toInt(isOverWeight);
            }

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<WeighingDTO> weighingDTOs = new List<WeighingDTO>();
            while (data.Read())
            {
                WeighingDTO weighingDTO = new WeighingDTO();
                weighingDTO.ID = toInt(data["ID"]);
                weighingDTO.Timestamp = toDateTime(data["TimeStamp"]);
                weighingDTO.RunningNumber = toInt(data["RunningNumber"]);
                weighingDTO.StationID = toInt(data["StationID"]);
                weighingDTO.LaneID = toInt(data["LaneID"]);
                weighingDTO.NumberAxles = toInt(data["NumberAxles"]);
                weighingDTO.WeightAxle01 = toInt(data["Weight_Axle01"]);
                weighingDTO.WeightAxle02 = toInt(data["Weight_Axle02"]);
                weighingDTO.WeightAxle03 = toInt(data["Weight_Axle03"]);
                weighingDTO.WeightAxle04 = toInt(data["Weight_Axle04"]);
                weighingDTO.WeightAxle05 = toInt(data["Weight_Axle05"]);
                weighingDTO.WeightAxle06 = toInt(data["Weight_Axle06"]);
                weighingDTO.WeightAxle07 = toInt(data["Weight_Axle07"]);
                weighingDTO.WeightAxle08 = toInt(data["Weight_Axle08"]);
                weighingDTO.WeightAxle09 = toInt(data["Weight_Axle09"]);
                weighingDTO.WeightAxle10 = toInt(data["Weight_Axle10"]);
                weighingDTO.WeightGVW = toInt(data["Weight_GVW"]);
                weighingDTO.WeightMaxGVW = toInt(data["Weight_MaxGVW"]);
                weighingDTO.WeightMaxAXW = toInt(data["Weight_MaxAXW"]);
                weighingDTO.IsOverWeight = toBoolean(data["IsOverWeight"]);
                weighingDTO.RecordStatus = toStr(data["RecordStatus"]);
                weighingDTO.Image01 = Configurations.ImageFilePathURL + toStr(data["Image01"]);
                weighingDTO.Image02 = Configurations.ImageFilePathURL + toStr(data["Image02"]);
                if (weighingDTO.Image01 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image01)))
                    {
                        try
                        {
                            weighingDTO.img1 = webClient.DownloadData(weighingDTO.Image01);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                if (weighingDTO.Image02 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image02)))
                    {
                        try
                        {
                            weighingDTO.img2 = webClient.DownloadData(weighingDTO.Image02);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                weighingDTOs.Add(weighingDTO);
            }
            connection.CloseConnection();
            return weighingDTOs;
        }

        public int getTotalPage(string stationId, string fromDate, string toDate, string isOverWeight)
        {
            string sql = "SELECT COUNT(ID) as TotalRecord FROM Weighing WHERE 1=1 ";
            if (stationId != null && stationId != "")
            {
                sql += "AND StationID = @StationID ";
            }
            if (fromDate != null && fromDate != "")
            {
                sql += "AND TimeStamp >= '" + fromDate + "' ";
            }
            if (toDate != null && toDate != "")
            {
                sql += "AND TimeStamp <= '" + toDate + "' ";
            }
            if (isOverWeight != null && isOverWeight != "")
            {
                sql += "AND IsOverWeight = @IsOverWeight ";
            }


            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            if (stationId != null && stationId != "")
            {
                cmd.Parameters.Add("@StationID", System.Data.SqlDbType.Int).Value = toInt(stationId);
            }
            /*
            if (fromDate != null && fromDate != "")
            {
                cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime).Value = toDateTime(fromDate);
            }
            if (toDate != null && toDate != "")
            {
                cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime).Value = toDateTime(toDate);
            }
             */
            if (isOverWeight != null && isOverWeight != "")
            {
                cmd.Parameters.Add("@IsOverWeight", System.Data.SqlDbType.Bit).Value = toInt(isOverWeight);
            }

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            int row = 0;
            if (data.Read())
            {
                row = toInt(data["TotalRecord"]);
            }
            connection.CloseConnection();
            row = row / ConfigurationUtil.ROW_PER_PAGE;
            row += 1;
            return row;
        }
        public DefaultDTO getByTimeStamp(DateTime dt)
        {
            var webClient = new WebClient();
            string sql = "SELECT * FROM Weighing WHERE convert(varchar,TimeStamp,120) = '" + dt.ToString("yyyy'-'MM'-'dd HH':'mm':'ss") + "'";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.Add("@dt", System.Data.SqlDbType.Char).Value = dt.ToString("yyyy-mm-dd hh:mm:ss");

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            WeighingDTO weighingDTO = null;
            if (data.Read())
            {
                weighingDTO = new WeighingDTO();
                weighingDTO.ID = toInt(data["ID"]);
                weighingDTO.Timestamp = toDateTime(data["TimeStamp"]);
                weighingDTO.RunningNumber = toInt(data["RunningNumber"]);
                weighingDTO.StationID = toInt(data["StationID"]);
                weighingDTO.LaneID = toInt(data["LaneID"]);
                weighingDTO.NumberAxles = toInt(data["NumberAxles"]);
                weighingDTO.WeightAxle01 = toInt(data["Weight_Axle01"]);
                weighingDTO.WeightAxle02 = toInt(data["Weight_Axle02"]);
                weighingDTO.WeightAxle03 = toInt(data["Weight_Axle03"]);
                weighingDTO.WeightAxle04 = toInt(data["Weight_Axle04"]);
                weighingDTO.WeightAxle05 = toInt(data["Weight_Axle05"]);
                weighingDTO.WeightAxle06 = toInt(data["Weight_Axle06"]);
                weighingDTO.WeightAxle07 = toInt(data["Weight_Axle07"]);
                weighingDTO.WeightAxle08 = toInt(data["Weight_Axle08"]);
                weighingDTO.WeightAxle09 = toInt(data["Weight_Axle09"]);
                weighingDTO.WeightAxle10 = toInt(data["Weight_Axle10"]);
                weighingDTO.WeightGVW = toInt(data["Weight_GVW"]);
                weighingDTO.WeightMaxGVW = toInt(data["Weight_MaxGVW"]);
                weighingDTO.WeightMaxAXW = toInt(data["Weight_MaxAXW"]);
                weighingDTO.IsOverWeight = toBoolean(data["IsOverWeight"]);
                weighingDTO.RecordStatus = toStr(data["RecordStatus"]);
                weighingDTO.Image01 = Configurations.ImageFilePathURL + toStr(data["Image01"]);
                weighingDTO.Image02 = Configurations.ImageFilePathURL + toStr(data["Image02"]);
                if (weighingDTO.Image01 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image01)))
                    {
                        try
                        {
                            weighingDTO.img1 = webClient.DownloadData(weighingDTO.Image01);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                if (weighingDTO.Image02 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image02)))
                    {
                        try
                        {
                            weighingDTO.img2 = webClient.DownloadData(weighingDTO.Image02);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            connection.CloseConnection();
            return weighingDTO;
        }

        public List<WeighingDTO> getReportByCondition(string stationId,string lane, string fromDate, string toDate, string isOver)
        {
            var webClient = new WebClient();
            string sql = "select w.*,s.ID,s.StationNameTh from Weighing w,Station s Where w.StationID = s.ID ";
            //string sql = "select isnull(w.ID,0) ID,w.TimeStamp,isnull(w.RunningNumber,0) RunningNumber,isnull(w.StationID,0) StationID,isnull(w.LaneID,0) LaneID ,isnull(w.NumberAxles,0) NumberAxles,isnull(w.Weight_Axle01,0) Weight_Axle01,isnull(w.Weight_Axle02,0) Weight_Axle02,isnull(w.Weight_Axle03,0) Weight_Axle03,isnull(w.Weight_Axle04,0) Weight_Axle04,isnull(w.Weight_Axle05,0) Weight_Axle05,isnull(w.Weight_Axle06,0) Weight_Axle06,isnull(w.Weight_Axle07,0) Weight_Axle07,isnull(w.Weight_Axle08,0) Weight_Axle08,isnull(w.Weight_Axle09,0) Weight_Axle09,isnull(w.Weight_Axle10,0) Weight_Axle10,isnull(w.Weight_GVW,0) Weight_GVW,isnull(w.Weight_MaxGVW,0) Weight_MaxGVW,isnull(w.Weight_MaxAXW,0) Weight_MaxAXW,w.IsOverWeight,w.RecordStatus,w.Image01,w.Image02,s.StationNameTh from Weighing w left join Station s on w.StationID = s.ID full outer join Axles x on w.NumberAxles = x.ID ";
            
            if (stationId != null && stationId != "0")
            {
                sql += " AND w.StationID = '" + stationId + "' ";
            }

            if (lane != null && lane != "0")
            {
                sql += " AND w.LaneID = '" + lane + "' ";
            }
            if (fromDate != null && fromDate != "")
            {
                if (toDate != null && toDate != "")
                {
                    sql += "AND w.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(toDate) + "' ";
                }
                else
                {
                    sql += "AND w.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' ";
                }
                
            }

            if (isOver != null && isOver != "-1")
            {
                sql += "AND w.IsOverWeight = '" + isOver + "' ";
            }


            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<WeighingDTO> weighingDTOs = new List<WeighingDTO>();
            while (data.Read())
            {
                WeighingDTO weighingDTO = new WeighingDTO();
                weighingDTO.ID = toInt(data["ID"]);
                weighingDTO.Timestamp = toDateTime(data["TimeStamp"]);
                weighingDTO.RunningNumber = toInt(data["RunningNumber"]);
                weighingDTO.StationID = toInt(data["StationID"]);
                weighingDTO.StationName = "" + data["StationNameTh"];
                weighingDTO.LaneID = toInt(data["LaneID"]);
                weighingDTO.NumberAxles = toInt(data["NumberAxles"]);
                weighingDTO.WeightAxle01 = toInt(data["Weight_Axle01"]);
                weighingDTO.WeightAxle02 = toInt(data["Weight_Axle02"]);
                weighingDTO.WeightAxle03 = toInt(data["Weight_Axle03"]);
                weighingDTO.WeightAxle04 = toInt(data["Weight_Axle04"]);
                weighingDTO.WeightAxle05 = toInt(data["Weight_Axle05"]);
                weighingDTO.WeightAxle06 = toInt(data["Weight_Axle06"]);
                weighingDTO.WeightAxle07 = toInt(data["Weight_Axle07"]);
                weighingDTO.WeightAxle08 = toInt(data["Weight_Axle08"]);
                weighingDTO.WeightAxle09 = toInt(data["Weight_Axle09"]);
                weighingDTO.WeightAxle10 = toInt(data["Weight_Axle10"]);
                weighingDTO.WeightGVW = toInt(data["Weight_GVW"]);
                weighingDTO.WeightMaxGVW = toInt(data["Weight_MaxGVW"]);
                weighingDTO.WeightMaxAXW = toInt(data["Weight_MaxAXW"]);
                weighingDTO.IsOverWeight = toBoolean(data["IsOverWeight"]);
                weighingDTO.RecordStatus = toStr(data["RecordStatus"]);
                weighingDTO.Image01 = Configurations.ImageFilePathURL + toStr(data["Image01"]);
                weighingDTO.Image02 = Configurations.ImageFilePathURL + toStr(data["Image02"]);

                if (weighingDTO.Image01 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image01)))
                    {
                        try
                        {
                            weighingDTO.img1 = webClient.DownloadData(weighingDTO.Image01);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                if (weighingDTO.Image02 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image02)))
                    {
                        try
                        {
                            weighingDTO.img2 = webClient.DownloadData(weighingDTO.Image02);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                weighingDTOs.Add(weighingDTO);
            }
            connection.CloseConnection();
            return weighingDTOs;
        }

        public List<_Rpt03> getReportByConditionForReport03(string stationId, string lane, string fromDate, string toDate,bool splitLane)
        {

            string sql = "";

            if (!splitLane)
            {
                sql = "SELECT StationNameTh,(isnull([True],0)+isnull([False],0)) as Total,isnull([True],0) as IsOver,isnull([False],0) as IsNormal " +
                "FROM ( SELECT s.StationNameTh,isOverWeight = IsOverWeight, Cnt = Count(M.ID) FROM Weighing M,Station s Where M.StationID = s.ID ";
            }
            else {
                sql = "SELECT StationNameTh,LaneID,(isnull([True],0)+isnull([False],0)) as Total,isnull([True],0) as IsOver,isnull([False],0) as IsNormal " +
                "FROM ( SELECT s.StationNameTh,M.LaneID,isOverWeight = IsOverWeight, Cnt = Count(M.ID) FROM Weighing M,Station s Where M.StationID = s.ID ";
            }
            if (stationId != null && stationId != "0")
            {
                sql += " AND M.StationID = '" + stationId + "' ";
            }

            if (lane != null && lane != "0")
            {
                sql += " AND M.LaneID = '" + lane + "' ";
            }
            if (fromDate != null && fromDate != "")
            {
                if (toDate != null && toDate != "")
                {
                    sql += " AND M.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(toDate) + "' ";
                }
                else
                {
                    sql += " AND M.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' ";
                }

            }
            if (!splitLane)
            {
                sql += " GROUP BY  M.IsOverWeight,s.StationNameTh ) src pivot(sum(Cnt) FOR isOverWeight IN ([True], [False])) pvt order by StationNameTh";
            }
            else {
                sql += " GROUP BY  M.IsOverWeight,s.StationNameTh,M.LaneID ) src pivot(sum(Cnt) FOR isOverWeight IN ([True], [False])) pvt order by StationNameTh,LaneID";
            }

            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            double weightOverTotal = 0;
            double weightNormalTotal = 0;
            List<_Rpt03> report03 = new List<_Rpt03>();
            while (data.Read())
            {
                _Rpt03 rpt03 = new _Rpt03();
                rpt03.StationName = "" + data["StationNameTh"];
                if (splitLane)
                {
                    rpt03.LaneID =toInt(data["LaneID"]);
                }
                rpt03.WeightTotal = toDouble(data["Total"]);
                rpt03.CountOfWeightOver = toDouble(data["IsOver"]);
                rpt03.CountOfWeightNormal = toDouble(data["IsNormal"]);
                
                weightOverTotal += rpt03.CountOfWeightOver;
                weightNormalTotal += rpt03.CountOfWeightNormal;

                report03.Add(rpt03);
            }
            //Addtotal
            foreach (_Rpt03 tmp in report03)
            {
                tmp.CountOfWeightOverTotal = weightOverTotal;
                tmp.CountOfWeightNormalTotal = weightNormalTotal;
            }
            connection.CloseConnection();
            return report03;
        }
        public List<WeighingDTO> getReportByConditionForReport04(string stationId, string lane, string fromDate, string toDate, string isOver)
        {

            string sql = "select StationNameTh,LaneID, isnull([1],0) [AX1], isnull([2],0) [AX2], isnull([3],0) [AX3], isnull([4],0) [AX4], isnull([5],0) [AX5], isnull([6],0) [AX6], isnull([7],0) [AX7], isnull([8],0) [AX8], isnull([9],0) [AX9], isnull([10],0)[AX10],(isnull([1],0)+isnull([2],0)+isnull([3],0)+isnull([4],0)+isnull([5],0)+isnull([6],0)+isnull([7],0)+isnull([8],0)+isnull([9],0)+isnull([10],0)) AS Total " +
             " from( " +
             " SELECT s.StationNameTh,w.LaneID, axles = w.NumberAxles, Cnt = Count(w.ID) " +
             " FROM Weighing w,Station s Where w.StationID = s.ID ";

            if (stationId != null && stationId != "0")
            {
                sql += " AND w.StationID = '" + stationId + "' ";
            }

            if (lane != null && lane != "0")
            {
                sql += " AND w.LaneID = '" + lane + "' ";
            }
            if (fromDate != null && fromDate != "")
            {
                if (toDate != null && toDate != "")
                {
                    sql += " AND w.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(toDate) + "' ";
                }
                else
                {
                    sql += " AND w.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' ";
                }

            }

           if (isOver != null && isOver != "-1")
            {
                sql += " AND w.IsOverWeight = '" + isOver + "' ";
            }

                sql += " GROUP BY  w.NumberAxles,s.StationNameTh,w.LaneID " +
                ") src pivot(sum(Cnt) FOR axles IN ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10])) pvt order by StationNameTh,LaneID ";

            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);
            List<WeighingDTO> report04 = new List<WeighingDTO>();
            while (data.Read())
            {
                WeighingDTO weighingDTO = new WeighingDTO();
                weighingDTO.StationName = "" + data["StationNameTh"];
                weighingDTO.LaneID = toInt(data["LaneID"]);
                weighingDTO.WeightAxle01 = toInt(data["Ax1"]);
                weighingDTO.WeightAxle02 = toInt(data["Ax2"]);
                weighingDTO.WeightAxle03 = toInt(data["Ax3"]);
                weighingDTO.WeightAxle04 = toInt(data["Ax4"]);
                weighingDTO.WeightAxle05 = toInt(data["Ax5"]);
                weighingDTO.WeightAxle06 = toInt(data["Ax6"]);
                weighingDTO.WeightAxle07 = toInt(data["Ax7"]);
                weighingDTO.WeightAxle08 = toInt(data["Ax8"]);
                weighingDTO.WeightAxle09 = toInt(data["Ax9"]);
                weighingDTO.WeightAxle10 = toInt(data["Ax10"]);
                report04.Add(weighingDTO);
            }
            connection.CloseConnection();
            return report04;
        }
        public List<WeighingDTO> getReportByConditionForReport04_Graph(string stationId, string lane, string fromDate, string toDate, string isOver)
        {

            string sql = "select StationNameTh, isnull([1],0) [AX1], isnull([2],0) [AX2], isnull([3],0) [AX3], isnull([4],0) [AX4], isnull([5],0) [AX5], isnull([6],0) [AX6], isnull([7],0) [AX7], isnull([8],0) [AX8], isnull([9],0) [AX9], isnull([10],0)[AX10],(isnull([1],0)+isnull([2],0)+isnull([3],0)+isnull([4],0)+isnull([5],0)+isnull([6],0)+isnull([7],0)+isnull([8],0)+isnull([9],0)+isnull([10],0)) AS Total " +
             " from( " +
             " SELECT s.StationNameTh, axles = w.NumberAxles, Cnt = Count(w.ID) " +
             " FROM Weighing w,Station s Where w.StationID = s.ID ";

            if (stationId != null && stationId != "0")
            {
                sql += " AND w.StationID = '" + stationId + "' ";
            }

            if (lane != null && lane != "0")
            {
                sql += " AND w.LaneID = '" + lane + "' ";
            }
            if (fromDate != null && fromDate != "")
            {
                if (toDate != null && toDate != "")
                {
                    sql += " AND w.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(toDate) + "' ";
                }
                else
                {
                    sql += " AND w.TimeStamp Between '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' AND  '" + Gm.toThaiDateTimeToSqlDate(fromDate) + "' ";
                }

            }

            if (isOver != null && isOver != "-1")
            {
                sql += " AND w.IsOverWeight = '" + isOver + "' ";
            }

            sql += " GROUP BY  w.NumberAxles,s.StationNameTh " +
            ") src pivot(sum(Cnt) FOR axles IN ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10])) pvt order by StationNameTh ";

            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);
            List<WeighingDTO> report04 = new List<WeighingDTO>();
            while (data.Read())
            {
                WeighingDTO weighingDTO = new WeighingDTO();
                weighingDTO.StationName = "" + data["StationNameTh"];
                weighingDTO.WeightAxle01 = toInt(data["Ax1"]);
                weighingDTO.WeightAxle02 = toInt(data["Ax2"]);
                weighingDTO.WeightAxle03 = toInt(data["Ax3"]);
                weighingDTO.WeightAxle04 = toInt(data["Ax4"]);
                weighingDTO.WeightAxle05 = toInt(data["Ax5"]);
                weighingDTO.WeightAxle06 = toInt(data["Ax6"]);
                weighingDTO.WeightAxle07 = toInt(data["Ax7"]);
                weighingDTO.WeightAxle08 = toInt(data["Ax8"]);
                weighingDTO.WeightAxle09 = toInt(data["Ax9"]);
                weighingDTO.WeightAxle10 = toInt(data["Ax10"]);
                report04.Add(weighingDTO);
            }
            connection.CloseConnection();
            return report04;
        }
        public WeighingDTO getReportByConditionAsFirst(int pkId)
        {
            var webClient = new WebClient();
            string sql = "select w.*,s.ID,s.StationNameTh from Weighing w,Station s Where w.StationID = s.ID ";
            //string sql = "select isnull(w.ID,0) ID,w.TimeStamp,isnull(w.RunningNumber,0) RunningNumber,isnull(w.StationID,0) StationID,isnull(w.LaneID,0) LaneID ,isnull(w.NumberAxles,0) NumberAxles,isnull(w.Weight_Axle01,0) Weight_Axle01,isnull(w.Weight_Axle02,0) Weight_Axle02,isnull(w.Weight_Axle03,0) Weight_Axle03,isnull(w.Weight_Axle04,0) Weight_Axle04,isnull(w.Weight_Axle05,0) Weight_Axle05,isnull(w.Weight_Axle06,0) Weight_Axle06,isnull(w.Weight_Axle07,0) Weight_Axle07,isnull(w.Weight_Axle08,0) Weight_Axle08,isnull(w.Weight_Axle09,0) Weight_Axle09,isnull(w.Weight_Axle10,0) Weight_Axle10,isnull(w.Weight_GVW,0) Weight_GVW,isnull(w.Weight_MaxGVW,0) Weight_MaxGVW,isnull(w.Weight_MaxAXW,0) Weight_MaxAXW,w.IsOverWeight,w.RecordStatus,w.Image01,w.Image02,s.StationNameTh from Weighing w left join Station s on w.StationID = s.ID full outer join Axles x on w.NumberAxles = x.ID ";
            if (pkId > 0)
            {
                sql += " AND w.ID = " + pkId ;
            }

            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            WeighingDTO weighingDTO = new WeighingDTO();
            while (data.Read())
            {
                weighingDTO.ID = toInt(data["ID"]);
                weighingDTO.Timestamp = toDateTime(data["TimeStamp"]);
                weighingDTO.RunningNumber = toInt(data["RunningNumber"]);
                weighingDTO.StationID = toInt(data["StationID"]);
                weighingDTO.StationName = "" + data["StationNameTh"];
                weighingDTO.LaneID = toInt(data["LaneID"]);
                weighingDTO.NumberAxles = toInt(data["NumberAxles"]);
                weighingDTO.WeightAxle01 = toInt(data["Weight_Axle01"]);
                weighingDTO.WeightAxle02 = toInt(data["Weight_Axle02"]);
                weighingDTO.WeightAxle03 = toInt(data["Weight_Axle03"]);
                weighingDTO.WeightAxle04 = toInt(data["Weight_Axle04"]);
                weighingDTO.WeightAxle05 = toInt(data["Weight_Axle05"]);
                weighingDTO.WeightAxle06 = toInt(data["Weight_Axle06"]);
                weighingDTO.WeightAxle07 = toInt(data["Weight_Axle07"]);
                weighingDTO.WeightAxle08 = toInt(data["Weight_Axle08"]);
                weighingDTO.WeightAxle09 = toInt(data["Weight_Axle09"]);
                weighingDTO.WeightAxle10 = toInt(data["Weight_Axle10"]);
                weighingDTO.WeightGVW = toInt(data["Weight_GVW"]);
                weighingDTO.WeightMaxGVW = toInt(data["Weight_MaxGVW"]);
                weighingDTO.WeightMaxAXW = toInt(data["Weight_MaxAXW"]);
                weighingDTO.IsOverWeight = toBoolean(data["IsOverWeight"]);
                weighingDTO.RecordStatus = toStr(data["RecordStatus"]);
                weighingDTO.Image01 = Configurations.ImageFilePathURL + toStr(data["Image01"]);
                weighingDTO.Image02 = Configurations.ImageFilePathURL + toStr(data["Image02"]);

                if (weighingDTO.Image01 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image01)))
                    {
                        try
                        {
                            weighingDTO.img1 = webClient.DownloadData(weighingDTO.Image01);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                if (weighingDTO.Image02 != null)
                {
                    if (!String.IsNullOrEmpty(Path.GetExtension(weighingDTO.Image02)))
                    {
                        try
                        {
                            weighingDTO.img2 = webClient.DownloadData(weighingDTO.Image02);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

            }

            connection.CloseConnection();

            return weighingDTO;
        }
    }
}