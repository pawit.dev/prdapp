﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using DMT.WS.Dao;
using DMT.WS.Dto;



namespace DMT.WS.Dao
{
    public class UserStationDAO : DefaultDAO
    {
        public override DefaultDTO save(DefaultDTO userStationDTO)
        {
            if (!(userStationDTO is UserStationDTO))
                throw new WrongTypeException(this, "UserStationDTO", userStationDTO);

            string insertSQL = @"INSERT INTO [UserStation]
                                   ([UserLoginID]
                                   ,[StationID]
                                   ,[Status])
                                VALUES
                                   (@UserLoginID
                                   ,@StationID, @Status)";

            UserStationDTO dto = userStationDTO as UserStationDTO;

            bool result = false;
            SqlCommand cmd = new SqlCommand(insertSQL);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@UserLoginID", System.Data.SqlDbType.Int).Value = dto.UserLoginId;
            cmd.Parameters.Add("@StationID", System.Data.SqlDbType.Int).Value = dto.StationId;
            cmd.Parameters.Add("@Status", System.Data.SqlDbType.NVarChar).Value = dto.Status;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                result = true;
            }
            connection.CloseConnection();

            if (result)
                return dto;
            else
                return null;
        }

        public override DefaultDTO getByPrimaryKey(Dictionary<string, string> pks)
        {
            if (!(pks.Count == 1 && pks.ContainsKey("ID")))
                throw new Exception("UserStationDAO getByPrimaryKey need only ID.");

            int id = toInt(pks["ID"]);
            return getByPrimaryKey(id);
        }

        public DefaultDTO getByPrimaryKey(int id)
        {
            string sql = "SELECT * FROM UserStation WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            UserStationDTO userStationDTO = null;
            if (data.Read())
            {
                userStationDTO = new UserStationDTO();
                userStationDTO.Id = toInt(data["ID"]);
                userStationDTO.UserLoginId = toInt(data["UserLoginID"]);
                userStationDTO.StationId = toInt(data["StationID"]);
                userStationDTO.Status = toStr(data["Status"]);
            }
            connection.CloseConnection();
            return userStationDTO;
        }

        public List<UserStationDTO> getByUserLoginId(int userLoginId)
        {
            string sql = "SELECT * FROM UserStation WHERE UserLoginID = @UserLoginID";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@userLoginId", System.Data.SqlDbType.Int).Value = userLoginId;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);
            List<UserStationDTO> userStationDTOs = new List<UserStationDTO>();
            while (data.Read())
            {
                UserStationDTO userStationDTO = new UserStationDTO();
                userStationDTO.Id = toInt(data["ID"]);
                userStationDTO.UserLoginId = toInt(data["UserLoginID"]);
                userStationDTO.StationId = toInt(data["StationID"]);
                userStationDTO.Status = toStr(data["Status"]);
                userStationDTOs.Add(userStationDTO);
            }
            connection.CloseConnection();
            return userStationDTOs;
        }

        public UserStationDTO getActiveByUserLoginId(int userLoginId)
        {
            string sql = "SELECT * FROM UserStation WHERE UserLoginID = @UserLoginID AND Status = 'ACTIVE'";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@userLoginId", System.Data.SqlDbType.Int).Value = userLoginId;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);
            UserStationDTO userStationDTO = null;
            if (data.Read())
            {
                userStationDTO = new UserStationDTO();
                userStationDTO.Id = toInt(data["ID"]);
                userStationDTO.UserLoginId = toInt(data["UserLoginID"]);
                userStationDTO.StationId = toInt(data["StationID"]);
                userStationDTO.Status = toStr(data["Status"]);
            }
            connection.CloseConnection();
            return userStationDTO;
        }

        public bool setInactiveAllByUserLoginId(int userLoginId)
        {
            string sql = "Update UserStation SET Status = 'INACTIVE' WHERE UserLoginID = @UserLoginID";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@UserLoginID", System.Data.SqlDbType.Int).Value = userLoginId;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            bool result = false;
            if (row > 0)
            {
                result = true;
            }
            connection.CloseConnection();
            return result;
        }


        public override bool update(DefaultDTO userStationDTO)
        {
            return false;
        }

        public override bool delete(DefaultDTO userStationDTO)
        {
            if (!(userStationDTO is UserStationDTO))
                throw new WrongTypeException(this, "UserStationDTO", userStationDTO);

            UserStationDTO dto = userStationDTO as UserStationDTO;
            int id = dto.Id;

            return deleteById(id);
        }


        public bool deleteById(int id)
        {
            bool deleteResult = false;
            string sql = "DELETE UserStation WHERE ID = @id";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }

        public bool deleteByUserLogin(int userLoginId)
        {
            bool deleteResult = false;
            string sql = "DELETE FROM UserStation WHERE UserLoginID = @UserLoginID";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("@UserLoginID", System.Data.SqlDbType.Int).Value = userLoginId;

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            int row = connection.ExecuteNonQuery(cmd);

            if (row > 0)
            {
                deleteResult = true;
            }
            connection.CloseConnection();
            return deleteResult;
        }
    }
}