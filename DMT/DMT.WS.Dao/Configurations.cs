﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace DMT.WS.Dao
{
    public class Configurations
    {

        #region "Shared"

        public static String ImageFilePath               
        {
            get { return ConfigurationManager.AppSettings["imageFilePath"]; }
        }
        public static String ImageWebPath
        {
            get { return ConfigurationManager.AppSettings["imageWebPath"]; }
        }
        public static String ImageFilePathURL
        {
            get { return ConfigurationManager.AppSettings["imageFilePathURL"]; }
        }
        public static String ReportPath
        {
            get { return ConfigurationManager.AppSettings["reportPath"]; }
        }
        public static String ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString; }
        }
        #endregion
    }
}
