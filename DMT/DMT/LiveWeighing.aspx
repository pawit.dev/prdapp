﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeBehind="LiveWeighing.aspx.cs" Inherits="DMT.LiveWeighing" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1" ClientIDMode="Static">
    <link rel="stylesheet" href="Styles/colorbox.css" />
    <script type="text/javascript" src="Scripts/jquery.colorbox.js"></script>
    <script type="text/javascript" src="Scripts/LiveWeighing.js"></script>
    <script type="text/javascript">
        $(function () {
            prepareSystem();
            //setInterval(test(),1000);
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#from_date").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#to_date").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#to_date").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#from_date").datepicker("option", "maxDate", selectedDate);
                }
            });
            $("#from_date,#to_date").datepicker("option", "dateFormat", "dd/mm/yy");
        });


        function searchByPage(pageno) {
            $('#txtPageNo').val(pageno);
            $('#btSearchByPage').click();
        }

        function changeStation(sel) {
            $("#wContent").empty();
        };


</script>
    
    
    <h1>ข้อมูลรถเข้าด่านชั่ง <img src="images/live_butt.png" alt=""/></h1>
    <br />
    <div>

    <div class="box1">
    <form action="AllWeighing.aspx" method="post">
        Search : 
        <select id="station" name="station" runat="server" enableviewstate="true"></select> | 
        From <input type="text" name="from_date" id="from_date" runat="server" enableviewstate="true" /> 
        <select runat="server" name="hr_from" id="cboHrFrom" enableviewstate="true" ></select>:
        <select runat="server" name="min_from" id="cboMinFrom" enableviewstate="true"></select>
        To 
        <input type="text" runat="server" name="to_date" id="to_date" enableviewstate="true" /> 
                <select runat="server" name="hr_to" id="cboHrTo" enableviewstate="true"></select>:
        <select runat="server" name="min_to" id="cboMinTo" enableviewstate="true"></select>
|
        <select name="overweight" id="overweight" runat="server" enableviewstate="true">
            <option value="">-All Weight-</option>
            <option value="0">Normal Weight</option>
            <option value="1">Over Weight</option>
        </select>
        <input type="submit" name="btnSearch" value="Search" />
        </form>
    </div>
    </div>
    <div class="box1">
    <div>
        Filter Station : 
        <select name="filter_station" id="filter_station"  onchange="changeStation(this);">
            <option value="">- All Station -</option>
            <%
                foreach (DMT.WS.Dto.StationDTO station in new DMT.WS.Dao.StationDAO().getAll())
                {
                    %>
                    <option value="<%=station.ID %>"><%=station.StationNameTh%></option>
                    <%
                }
            %>
        </select>

        Filter Lane : 
        <select name="filter_lane" id="filter_lane">
            <option value="">- All Lane -</option>
            <%
                foreach (DMT.WS.Dto.LaneDTO lane in new DMT.WS.Dao.LaneDAO().getAll())
                {
                    %>
                    <option value="<%=lane.ID %>"><%=lane.LaneName%></option>
                    <%
                }
            %>
        </select>
    </div>
    </div>
    <div id="divWimData" style="width: 100%;  overflow-y: scroll; padding-bottom: 10px;" runat="server">
        <table id="tbReport" border="1" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th rowspan="2">Time</th>
                <th rowspan="2">Station</th>
                <th rowspan="2">Lane</th>
                <th rowspan="2">Axles</th>
                <th colspan="10">Axle Weight</th>
                <th rowspan="2">GVW</th>
                <th rowspan="2">M.GVW</th>
                <th rowspan="2">M.AXW</th>
                <th rowspan="2">Front Image</th>
                <th rowspan="2">Back Image</th>
                <th rowspan="2" width="10%">Result</th>
                <th rowspan="2" width="5%">Status</th>
            </tr>
            <tr class="tr_rowspan">
                <th width="5%">1</th>
                <th width="5%">2</th>
                <th width="5%">3</th>
                <th width="5%">4</th>
                <th width="5%">5</th>
                <th width="5%">6</th>
                <th width="5%">7</th>
                <th width="5%">8</th>
                <th width="5%">9</th>
                <th width="5%">10</th>
            </tr>
            </thead>
            <tbody id="wContent">
            <!-- Data -->
            
            </tbody>
       </table>
    </div>
           
</asp:Content>