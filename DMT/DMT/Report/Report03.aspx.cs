﻿using CrystalDecisions.CrystalReports.Engine;
using DMT.WS.Biz;
using DMT.WS.Dao;
using DMT.WS.Dto;
using DMT.WS.Utils;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DMT.Report
{
    public partial class Report03 : System.Web.UI.Page
    {
        protected List<WeighingSummaryAllStationDTO> weighings = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_ALL_WEIGHING"))
                Response.Redirect("LiveWeighing.aspx");
            // first load
            if (!IsPostBack)
            {
                initialPage();
            }
        }

        private void initialPage()
        {

            List<StationDTO> stations = new StationDAO().getAll();
            cboStation.Items.Clear();
            cboStation.DataSource = stations;
            cboStation.DataBind();
            cboStation.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));

            List<LaneDTO> lanes = new LaneDAO().getAll();
            cboLane.Items.Clear();
            cboLane.DataSource = lanes;
            cboLane.DataBind();
            cboLane.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));

            cboLane.Enabled = false;
        }



        private void beginSearch()
        {
            List<_Rpt03> beans = new WeighingDAO().getReportByConditionForReport03(cboStation.SelectedValue.ToString(), cboLane.SelectedValue.ToString(), txtStartDate.Value.ToString(), txtEndDate.Value, cbLane.Checked);
            if (beans != null && beans.Count > 0)
            {
                ReportDocument rptH = new ReportDocument();
                rptH.FileName = ((cbLane.Checked) ? Server.MapPath("~/Rpt/Report03_1.rpt") : Server.MapPath("~/Rpt/Report03_2.rpt"));


                rptH.SetDataSource(beans);

                rptH.SetParameterValue("START_DATE", (txtStartDate.Value.Length == 10) ? txtStartDate.Value : "-");
                rptH.SetParameterValue("END_DATE", (txtEndDate.Value.Length == 10) ? "" + txtEndDate.Value : "-");
                if (cboStation.Text.Length != 0)
                {
                    rptH.SetParameterValue("P_STATION", cboStation.SelectedItem.Text);
                }
                else
                {
                    rptH.SetParameterValue("P_STATION", "ทั้งหมด");
                }
                try
                {
                    string appPath = HttpContext.Current.Request.ApplicationPath;
                    string physicalPath = HttpContext.Current.Request.MapPath(appPath);
                    string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
                    rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

                    string url = Configurations.ReportPath + "/" + filename;
                    this.ifContent.Attributes["height"] = "900px";
                    this.ifContent.Attributes["src"] = url;
                    //string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
                    //ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

                    //rptH.PrintToPrinter(1, false, 0, 0);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string graphType = hdGraphType.Value;
            if (graphType != "0")
            {
                this.ifContent.Attributes["height"] = "0px";
                this.ifContent.Attributes["src"] = null;
                ScriptManager.RegisterStartupScript(this, GetType(), "loadReport", "loadReport();", true);
            }
            else
            {
                beginSearch();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            cboLane.Enabled = false;
            cboStation.SelectedIndex = 0;
            this.ifContent.Attributes["src"] = null;
        }

        protected void cboStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stationId = Convert.ToInt32(cboStation.SelectedValue);
            if (stationId == 0)
            {
                cboLane.Enabled = false;
            }
            else
            {
                List<LaneDTO> lanes = new LaneDAO().getLaneByStation(stationId);
                cboLane.Items.Clear();
                cboLane.DataSource = lanes;
                cboLane.DataBind();
                cboLane.Items.Insert(0, new ListItem("-ทั้งหมด-", "0"));
                cboLane.Enabled = true;
            }
        }


    }
}