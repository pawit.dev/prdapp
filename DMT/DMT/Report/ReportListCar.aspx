﻿<%@ Page Title="Default" Language="C#" AutoEventWireup="true" MasterPageFile="~/Main.Master" CodeBehind="ReportListCar.aspx.cs" Inherits="DMT.ReportListCar" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">

    <script type="text/javascript">
        $(function () {
            $("#txtStartDate").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtEndDate").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#txtEndDate").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd/mm/yy",
                onClose: function (selectedDate) {
                    $("#txtStartDate").datepicker("option", "maxDate", selectedDate);
                }
            });
        });
    </script>
    <h1>รายงานแสดง List ของรายการเข้าชั่ง</h1>
    <form id="Form1" runat="server">
    <table>
        <tr>
            <td align="right">Station : </td>
            <td>
                <select runat="server" id="cboStation"></select></td>
        </tr>
        <tr>
            <td align="right">Start : </td>
            <td>
                <input type="text" runat="server" id="txtStartDate" /></td>
        </tr>
        <tr>
            <td align="right">End : </td>
            <td>
                <input type="text" runat="server" id="txtEndDate" /></td>
        </tr>
        <tr>
            <td align="right">Data Format : </td>
            <td>
                <asp:RadioButtonList Style="display: inline" RepeatDirection="Horizontal" ID="radioGraphType" runat="server"></asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" Text="Submit" runat="server" OnClick="btnSubmit_Click" />
                <input type="text" id="txtDebug" runat="server" enableviewstate="false" style="width: 600px;" visible="false" />
            </td>
        </tr>
    </table>
</form>
    <%--    <a href="Report.aspx">ย้อนกลับ (Back)</a>--%>
</asp:Content>
