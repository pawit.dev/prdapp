﻿function loadReport() {
    //$('#reportArea').html('Loading Report...');
    $('#container').html('');

    var station = $('#cboStation').val();
    var isOver = $('#cboOver').val();
    if (isOver == 'True') {
        isOver = '1';
    } else if (isOver == '') {
        isOver = '';
    } else {
        isOver = '0';
    }

    var sDate = $('#txtStartDate').val();
    var sHour = $('#cboHrFrom').val();
    var sMin = $('#cboMinFrom').val();
    var startDateTime = '';
    if (sDate != '') {
        var sDay = sDate.substring(0, 2);
        var sMonth = sDate.substring(3, 5);
        var sYear = sDate.substring(6, 10);
        startDateTime = sYear + '-' + sMonth + '-' + sDay + ' ' + sHour + ':' + sMin + ':00.000';
    }

    var eDate = $('#txtEndDate').val();
    var eHour = $('#cboHrTo').val();
    var eMin = $('#cboMinTo').val();
    var endDateTime = '';
    if (eDate != '') {
        var eDay = eDate.substring(0, 2);
        var eMonth = eDate.substring(3, 5);
        var eYear = eDate.substring(6, 10);
        endDateTime = eYear + '-' + eMonth + '-' + eDay + ' ' + eHour + ':' + eMin + ':00.000';
    }

    var periodType = $('input[name=periodType]');
    var periodType = periodType.filter(':checked').val();

    var graphType = $('#hdGraphType').val();

    //var graphType = graphType.filter(':checked').val();
    //alert(graphType);
    $.ajax({
        url: "/DMT/Ajax.aspx?type=GetReport3&tmp=" + new Date().getTime(),
        type: "POST",
        data: { stationId: station, startDate: startDateTime, endDate: endDateTime, isOver: isOver, periodType: periodType },
        dataType: "json",
        success: function (data) {
            if (data != null && data != 'null') {
                //renderReport(data);
                if (graphType == 2) {
                    loadBarChart(data);
                } else {
                    loadLineChart(data);
                }
            }
        }
    });
}

function renderReport(data) {

    if (data != null) {
        var tbl = $('<table id="tbReport" width="100%"></table>');
        tbl.append($('<tr><th></th><th colspan="10" align="center">จำนวนเพลา</th><th rowspan="2">รวม</th></tr>'));
        tbl.append($('<tr class="tr_rowspan"><th>Period</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th></tr>'));
        var rowId = 0;
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            var rowClassName = '';
            if (rowId == 0) {
                rowClassName = 'class="row1"';
                rowId = 1;
            } else {
                rowClassName = 'class="row2"';
                rowId = 0;
            }
            tbl.append("<tr " + rowClassName + ">" +
                "<td>" + item['Period'] + "</td>" +
                "<td>" + item['1'] + "</td>" +
                "<td>" + item['2'] + "</td>" +
                "<td>" + item['3'] + "</td>" +
                "<td>" + item['4'] + "</td>" +
                "<td>" + item['5'] + "</td>" +
                "<td>" + item['6'] + "</td>" +
                "<td>" + item['7'] + "</td>" +
                "<td>" + item['8'] + "</td>" +
                "<td>" + item['9'] + "</td>" +
                "<td>" + item['10'] + "</td>" +
                "<td>" + item['Total'] + "</td>" +
            "</tr>");
        }


        $('#reportArea').html('');
        $('#reportArea').append(tbl);
    }
}

function loadBarChart(data) {
    var mainData = data;
    var cats = new Array();
    var series = new Array();
    var awx1 = new Array();
    var awx2 = new Array();
    var awx3 = new Array();
    var awx4 = new Array();
    var awx5 = new Array();
    var awx6 = new Array();
    var awx7 = new Array();
    var awx8 = new Array();
    var awx9 = new Array();
    var awx10 = new Array();
    var total = new Array();
    for (var i = 0; i < mainData.length; i++) {
        cats[cats.length] = mainData[i]['Period'];
        awx1[awx1.length] = parseInt(mainData[i]['1']);
        awx2[awx2.length] = parseInt(mainData[i]['2']);
        awx3[awx3.length] = parseInt(mainData[i]['3']);
        awx4[awx4.length] = parseInt(mainData[i]['4']);
        awx5[awx5.length] = parseInt(mainData[i]['5']);
        awx6[awx6.length] = parseInt(mainData[i]['6']);
        awx7[awx7.length] = parseInt(mainData[i]['7']);
        awx8[awx8.length] = parseInt(mainData[i]['8']);
        awx9[awx9.length] = parseInt(mainData[i]['9']);
        awx10[awx10.length] = parseInt(mainData[i]['10']);
        total[total.length] = parseInt(mainData[i]['Total']);
    }
    $('#container').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            text: 'Don Muang Tollway Public Company Limited'
        },
        subtitle: {
            text: 'รายงานสรุปภาพรวมปริมาณรถเข้าชั่ง'
        },

        xAxis: {
            categories:cats
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'จำนวนรถเข้าชั่ง (คัน)'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        series: [{
            name: 'Axle 1',
            data: awx1
        }, {
            name: 'Axle 2',
            data: awx2

        }, {
            name: 'Axle 3',
            data: awx3

        }, {
            name: 'Axle 4',
            data: awx4

        }, {
            name: 'Axle 5',
            data: awx5

        }, {
            name: 'Axle 6',
            data: awx6

        }, {
            name: 'Axle 7',
            data: awx7

        }, {
            name: 'Axle 8',
            data: awx8

        }, {
            name: 'Axle 9',
            data: awx9

        }, {
            name: 'Axle 10',
            data: awx10

        }]
    });


}

function loadLineChart(data) {
    var mainData = data;
    var cats = new Array();
    var series = new Array();
    var awx1 = new Array();
    var awx2 = new Array();
    var awx3 = new Array();
    var awx4 = new Array();
    var awx5 = new Array();
    var awx6 = new Array();
    var awx7 = new Array();
    var awx8 = new Array();
    var awx9 = new Array();
    var awx10 = new Array();
    var total = new Array();
    for (var i = 0; i < mainData.length; i++) {
        cats[cats.length] = mainData[i]['Period'];
        awx1[awx1.length] = parseInt(mainData[i]['1']);
        awx2[awx2.length] = parseInt(mainData[i]['2']);
        awx3[awx3.length] = parseInt(mainData[i]['3']);
        awx4[awx4.length] = parseInt(mainData[i]['4']);
        awx5[awx5.length] = parseInt(mainData[i]['5']);
        awx6[awx6.length] = parseInt(mainData[i]['6']);
        awx7[awx7.length] = parseInt(mainData[i]['7']);
        awx8[awx8.length] = parseInt(mainData[i]['8']);
        awx9[awx9.length] = parseInt(mainData[i]['9']);
        awx10[awx10.length] = parseInt(mainData[i]['10']);
        total[total.length] = parseInt(mainData[i]['Total']);
    }

    $('#container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Don Muang Tollway Public Company Limited'
        },
        subtitle: {
            text: 'รายงานสรุปภาพรวมปริมาณรถเข้าชั่ง'
        },
        xAxis: {
            categories: cats
        },
        yAxis: {
            title: {
                text: 'จำนวนรถเข้าชั่ง (คัน)'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
        },

        series: [{
            name: 'Axle 1',
            data: awx1
        }, {
            name: 'Axle 2',
            data: awx2

        }, {
            name: 'Axle 3',
            data: awx3

        }, {
            name: 'Axle 4',
            data: awx4

        }, {
            name: 'Axle 5',
            data: awx5

        }, {
            name: 'Axle 6',
            data: awx6

        }, {
            name: 'Axle 7',
            data: awx7

        }, {
            name: 'Axle 8',
            data: awx8

        }, {
            name: 'Axle 9',
            data: awx9

        }, {
            name: 'Axle 10',
            data: awx10

        }]
    });
}


