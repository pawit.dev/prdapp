﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using DMT.WS.Biz;
using DMT.WS.Dto;

namespace DMT
{
    public partial class MyAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string submit = Request.Params["submit"];

            if (submit != null && submit != "")
            {
                string newPassword = Request.Params["new_password"];
                string reNewPassword = Request.Params["re_new_password"];
                string oldPassword = Request.Params["old_password"];
                string fullName = Request.Params["fullname"];
                string email = Request.Params["email"];
                bool valdated = true;

                UserLoginService service = new UserLoginService();
                UserLoginBean bean = (UserLoginBean)Session["userLoginBean"];
                if (bean.Password != oldPassword)
                {
                    Session["resultMessage"] = "Wrong password.";
                    Session["resultClass"] = "error";
                    valdated = false;
                    Response.Redirect("MyAccount.aspx");
                }
                if (newPassword != null && newPassword != "")
                {
                    if (newPassword != reNewPassword)
                    {
                        Session["resultMessage"] = "Password must be the same.";
                        Session["resultClass"] = "error";
                        valdated = false;
                        Response.Redirect("MyAccount.aspx");
                    }
                }

                if (valdated)
                {
                    if (service.editProfile(bean.ID, newPassword, email, fullName))
                    {
                        Session["resultMessage"] = "Profile has been updated.";
                        Session["resultClass"] = "success";
                        Response.Redirect("MyAccount.aspx");
                    }
                    else
                    {
                        Session["resultMessage"] = "Update fail.";
                        Session["resultClass"] = "error";
                        Response.Redirect("MyAccount.aspx");
                    }
                }

            }
        }
    }
}
