﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMT.WS.Biz;
using DMT.WS.Dto;
using DMT.WS.Utils;
using DMT.WS.Dao;
using CrystalDecisions.CrystalReports.Engine;

namespace DMT
{
    public partial class PrintWeighingDetail : System.Web.UI.Page
    {
        protected string ErrorMessage
        {
            get;
            set;
        }

        protected WeighingDTO WeighingDTO
        {
            get;
            private set;
        }
        protected StationDTO StationDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_STATION"))
                Response.Redirect("LiveWeighing.aspx");


            //StationDAO stationDao = new StationDAO();
            //ErrorMessage = "";
            //WeighingDTO = null;

            //if (!IsPostBack)
            //{
            //    int id = Gm.toSafeInt(Request.QueryString["WID"]);

            //    if (id > 0)
            //    {
            //        // Load User Info

            //        WeighingBean bean = new WeighingBAO().getByPK(id);
            //        if (bean != null)
            //        {

            //            ReportDocument rptH = new ReportDocument();
            //            rptH.FileName = Server.MapPath("~/Rpt/ReportEachCar.rpt");
            //            List<WeighingBean> tmps = new List<WeighingBean>();
            //            tmps.Add(bean);

            //            rptH.SetDataSource(tmps);
            //            //rptH.SetParameterValue("START_DATE", bean.Timestamp.ToString("dd/MM/yyyy hh:mm"));
            //            //rptH.SetParameterValue("END_DATE", "");

            //            StationDTO stationDto = (StationDTO)new StationDAO().getByPrimaryKey(bean.Station.ID);

            //            rptH.SetParameterValue("P_STATION", stationDto.StationNameTh);


            //            try
            //            {
            //                string appPath = HttpContext.Current.Request.ApplicationPath;
            //                string physicalPath = HttpContext.Current.Request.MapPath(appPath);
            //                string filename = "tmpreport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
            //                rptH.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, physicalPath + "\\Rpt_tmp\\" + filename);

            //                string url = Configurations.ReportPath + "/" + filename;
            //                string s = "window.open('" + url + "', 'popup_window', 'width=800,height=600,left=100,top=100,resizable=yes');";
            //                ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

            //                //rptH.PrintToPrinter(1, false, 0, 0);
            //            }
            //            catch (Exception ex)
            //            {
            //                Console.WriteLine(ex.Message);
            //            }

            //            //rptH.PrintToPrinter(1, false, 0, 0);

            //            //WeighingDTO = weighing;
            //            //StationDTO = new StationDAO().getByPrimaryKey(weighing.StationID) as StationDTO;
            //        }
            //        else
            //        {
            //            ErrorMessage = "ไม่พบรายการที่ระบุ";
            //        }
            //    }
            //    else
            //    {
            //        ErrorMessage = "Error";
            //    }
            //}
        }
    }
}