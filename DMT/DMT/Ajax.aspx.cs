﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Script.Serialization;
using System.Data.SqlClient;
using DMT.WS.Biz;
using DMT.WS.Dao;
using DMT.WS.Utils;
using DMT.WS.Dto;
using System.Net;


namespace DMT
{
    public partial class Ajax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = Request.Params["type"];
            if (type == "GetNewContent")
            {
                try
                {
                    int maxId = int.Parse(Request.Params["maxid"]);
                    string station = Request.Params["station"];
                    string lane = Request.Params["lane"];
                    getNewContent(maxId, station, lane);
                }
                catch (Exception ex)
                {
                    string json = new JavaScriptSerializer().Serialize(null);
                    Response.Write(json);
                }
            }
            else if (type == "GetReport3")
            {
                try
                {
                    int? stationId = null;
                    int? isOver = null;
                    string startDate = null;
                    string endDate = null;
                    int periodType = int.Parse(Request.Params["periodType"]);

                    try { stationId = int.Parse(Request.Params["stationId"]); }
                    catch (Exception ex) { }
                    try { isOver = int.Parse(Request.Params["isOver"]); }
                    catch (Exception ex) { }
                    startDate = Request.Params["startDate"];
                    endDate = Request.Params["endDate"];

                    if (startDate == "") startDate = null;
                    if (endDate == "") endDate = null;

                    getReport3(stationId, isOver, startDate, endDate, periodType);
                }
                catch (Exception ex)
                {
                    string json = new JavaScriptSerializer().Serialize(null);
                    Response.Write(json);
                }
            }
            else if (type == "GetReport4")
            {
                try
                {
                    string stationId = Request.Params["stationId"];
                    string cboLane = Request.Params["laneId"];
                    string startDate = Request.Params["startDate"];
                    string endDate = Request.Params["endDate"];
                    string weightover = Request.Params["weightover"];
                    getReport3_1(stationId, cboLane, startDate, endDate, weightover);
                }
                catch (Exception ex)
                {
                    string json = new JavaScriptSerializer().Serialize(null);
                    Response.Write(json);
                }
            }
            else if (type == "GetReport4_1")
            {
                try
                {
                    string stationId = Request.Params["stationId"];
                    string cboLane = Request.Params["laneId"];
                    string startDate = Request.Params["startDate"];
                    string endDate = Request.Params["endDate"];
                    bool splitLane = Convert.ToBoolean(Request.Params["splitLane"]);

                    getReport4(stationId, cboLane, startDate, endDate, splitLane);

                }
                catch (Exception ex)
                {
                    string json = new JavaScriptSerializer().Serialize(null);
                    Response.Write(json);
                }
            }
        }

        private void getNewContent(int id, string station, string lane)
        {
            if (!UserLoginUtil.isLogin())
            {
                return;
            }
            string sql = "";
            if (UserLoginUtil.getCurrentUserLogin().Level == ConfigurationUtil.USER_LEVEL_ADMIN)
            {
                if (station == "")
                {

                    sql = "SELECT * FROM (SELECT TOP 6 w.ID ,w.TimeStamp,w.RunningNumber ,w.StationID,w.LaneID ,w.NumberAxles ,w.Weight_Axle01,w.Weight_Axle02,w.Weight_Axle03,w.Weight_Axle04 ,w.Weight_Axle05,w.Weight_Axle06 ,w.Weight_Axle07 ,w.Weight_Axle08 ,w.Weight_Axle09 ,w.Weight_Axle10 ,w.Weight_GVW,w.Weight_MaxGVW,w.Weight_MaxAXW ,w.IsOverWeight,w.RecordStatus,w.Image01,w.Image02 ,s.StationNameTh FROM Weighing w,Station s WHERE w.StationID = s.ID  order by w.TimeStamp desc) A ORDER BY TimeStamp DESC";
                    if (lane != "")
                    {
                        sql = "SELECT * FROM (SELECT TOP 6 w.ID ,w.TimeStamp,w.RunningNumber ,w.StationID,w.LaneID ,w.NumberAxles ,w.Weight_Axle01,w.Weight_Axle02,w.Weight_Axle03,w.Weight_Axle04 ,w.Weight_Axle05,w.Weight_Axle06 ,w.Weight_Axle07 ,w.Weight_Axle08 ,w.Weight_Axle09 ,w.Weight_Axle10 ,w.Weight_GVW,w.Weight_MaxGVW,w.Weight_MaxAXW ,w.IsOverWeight,w.RecordStatus,w.Image01,w.Image02 ,s.StationNameTh FROM Weighing w,Station s WHERE w.StationID = s.ID and w.LaneID = @laneId  order by w.TimeStamp desc) A ORDER BY TimeStamp DESC";
                    }
                }
                else
                {
                    sql = "SELECT * FROM (SELECT TOP 6 w.ID ,w.TimeStamp,w.RunningNumber ,w.StationID,w.LaneID ,w.NumberAxles ,w.Weight_Axle01,w.Weight_Axle02,w.Weight_Axle03,w.Weight_Axle04 ,w.Weight_Axle05,w.Weight_Axle06 ,w.Weight_Axle07 ,w.Weight_Axle08 ,w.Weight_Axle09 ,w.Weight_Axle10 ,w.Weight_GVW,w.Weight_MaxGVW,w.Weight_MaxAXW ,w.IsOverWeight,w.RecordStatus,w.Image01,w.Image02 ,s.StationNameTh FROM Weighing w,Station s WHERE w.StationID = @stationID and w.StationID = s.ID  order by w.TimeStamp desc) A ORDER BY TimeStamp DESC";
                    if (lane != "")
                    {
                        sql = "SELECT * FROM (SELECT TOP 6 w.ID ,w.TimeStamp,w.RunningNumber ,w.StationID,w.LaneID ,w.NumberAxles ,w.Weight_Axle01,w.Weight_Axle02,w.Weight_Axle03,w.Weight_Axle04 ,w.Weight_Axle05,w.Weight_Axle06 ,w.Weight_Axle07 ,w.Weight_Axle08 ,w.Weight_Axle09 ,w.Weight_Axle10 ,w.Weight_GVW,w.Weight_MaxGVW,w.Weight_MaxAXW ,w.IsOverWeight,w.RecordStatus,w.Image01,w.Image02 ,s.StationNameTh FROM Weighing w,Station s WHERE w.StationID = @stationID and w.StationID = s.ID and w.LaneID = @laneId  order by w.TimeStamp desc) A ORDER BY TimeStamp DESC";
                    }
                }

            }
            else
            {
                UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(UserLoginUtil.getCurrentUserLogin().ID);

                int stationId = -1;
                if (userStation != null)
                {
                    stationId = userStation.StationId;
                }
                //sql = "SELECT * FROM (SELECT TOP 6 * FROM Weighing WHERE ID > @id and [StationID] = '" + stationId + "' order by ID desc) A ORDER BY ID";
                sql = "SELECT * FROM (SELECT TOP 6 w.ID ,w.TimeStamp,w.RunningNumber ,w.StationID,w.LaneID ,w.NumberAxles ,w.Weight_Axle01,w.Weight_Axle02,w.Weight_Axle03,w.Weight_Axle04 ,w.Weight_Axle05,w.Weight_Axle06 ,w.Weight_Axle07 ,w.Weight_Axle08 ,w.Weight_Axle09 ,w.Weight_Axle10 ,w.Weight_GVW,w.Weight_MaxGVW,w.Weight_MaxAXW ,w.IsOverWeight,w.RecordStatus,w.Image01,w.Image02 ,s.StationNameTh FROM Weighing w,Station s WHERE  [StationID] = '" + stationId + "' and w.StationID = s.ID and [LaneID] = '" + lane + "'   order by w.TimeStamp desc) A ORDER BY TimeStamp";
            }
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
            if (station != "")
            {
                cmd.Parameters.Add("@stationID", System.Data.SqlDbType.Int).Value = station;
            }
            if (lane != "")
            {
                cmd.Parameters.Add("@laneId", System.Data.SqlDbType.Int).Value = lane;
            }
            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
            while (data.Read())
            {
                Dictionary<string, string> item = new Dictionary<string, string>();
                item["ID"] = "" + data["ID"];
                if (data["TimeStamp"] != null && data["TimeStamp"].ToString() != "" && data["TimeStamp"].ToString().ToLower() != "null")
                {
                    item["TimeStamp"] = "" + data["TimeStamp"].ToString(); //Gm.toSafeDateTime(data["TimeStamp"]).ToString("dd/MM/yyy hh:MM tt");
                }
                else
                {
                    item["TimeStamp"] = "-";
                }
                item["RunningNumber"] = "" + data["RunningNumber"];
                item["StationID"] = "" + data["StationNameTh"];
                item["LaneID"] = "" + data["LaneID"];
                item["NumberAxles"] = "" + data["NumberAxles"];
                item["Weight_Axle01"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle01"]);
                item["Weight_Axle02"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle02"]);
                item["Weight_Axle03"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle03"]);
                item["Weight_Axle04"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle04"]);
                item["Weight_Axle05"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle05"]);
                item["Weight_Axle06"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle06"]);
                item["Weight_Axle07"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle07"]);
                item["Weight_Axle08"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle08"]);
                item["Weight_Axle09"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle09"]);
                item["Weight_Axle10"] = "" + CommonUtils.customNumberFormat("" + data["Weight_Axle10"]);
                item["Weight_GVW"] = "" + CommonUtils.customNumberFormat("" + data["Weight_GVW"]);
                item["Weight_MaxGVW"] = "" + CommonUtils.customNumberFormat("" + data["Weight_MaxGVW"]);
                item["Weight_MaxAXW"] = "" + CommonUtils.customNumberFormat("" + data["Weight_MaxAXW"]);
                item["IsOverWeight"] = "" + data["IsOverWeight"];
                item["RecordStatus"] = "" + data["RecordStatus"];
                item["Image01"] = ("" + Configurations.ImageFilePathURL + data["Image01"]).Trim();
                item["Image02"] = ("" + Configurations.ImageFilePathURL + data["Image02"]).Trim();
                items.Add(item);
            }
            connection.CloseConnection();
            string json = new JavaScriptSerializer().Serialize(items);
            Response.Write(json);
        }

        private void getReport3(int? station, int? isOver, string startDate, string endDate, int periodType)
        {
            if (!UserLoginUtil.isLogin())
            {
                return;
            }
            string sql = "EXEC [dbo].[SPC_Report3]	@StationID = @sId, @IsOverWeight = @isOver, @StartDate = @sDate, @EndDate = @eDate, @PeriodType = @pType";

            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.Add("pType", System.Data.SqlDbType.Int).Value = periodType;
            if (station != null)
            {
                cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = station;
            }
            else
            {
                cmd.Parameters.Add("@sId", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
            }

            if (isOver != null)
            {
                cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = isOver;
            }
            else
            {
                cmd.Parameters.Add("@isOver", System.Data.SqlDbType.Int).Value = System.Data.SqlTypes.SqlInt32.Null;
            }

            if (startDate != null)
            {
                cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = startDate;
            }
            else
            {
                cmd.Parameters.Add("@sDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
            }
            if (endDate != null)
            {
                cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = endDate;
            }
            else
            {
                cmd.Parameters.Add("@eDate", System.Data.SqlDbType.DateTime).Value = System.Data.SqlTypes.SqlDateTime.Null;
            }

            ConnectionDB connection = new ConnectionDB();
            connection.OpenConnection();
            SqlDataReader data = connection.ExecuteQuery(cmd);

            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
            while (data.Read())
            {
                Dictionary<string, string> item = new Dictionary<string, string>();
                item["Period"] = "" + data["Period"];
                item["1"] = "" + Gm.toSafeInt(data["1"]);
                item["2"] = "" + Gm.toSafeInt(data["2"]);
                item["3"] = "" + Gm.toSafeInt(data["3"]);
                item["4"] = "" + Gm.toSafeInt(data["4"]);
                item["5"] = "" + Gm.toSafeInt(data["5"]);
                item["6"] = "" + Gm.toSafeInt(data["6"]);
                item["7"] = "" + Gm.toSafeInt(data["7"]);
                item["8"] = "" + Gm.toSafeInt(data["8"]);
                item["9"] = "" + Gm.toSafeInt(data["9"]);
                item["10"] = "" + Gm.toSafeInt(data["10"]);
                item["Total"] = "" + Gm.toSafeInt(data["Total"]);
                items.Add(item);
            }
            connection.CloseConnection();
            string json = new JavaScriptSerializer().Serialize(items);
            Response.Write(json);
        }
        private void getReport3_1(string stationId, string laneId, string startDate, string endDate, string weightover)
        {
            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
            List<WeighingDTO> beans = new WeighingDAO().getReportByConditionForReport04_Graph(stationId, laneId, startDate, endDate, weightover);
            if (beans != null && beans.Count > 0)
            {
                
                foreach (WeighingDTO w in beans)
                {
                    Dictionary<string, string> item = new Dictionary<string, string>();
                    item["Station"] = "" + w.StationName;
                    item["NumberAxles"] = "" + w.NumberAxles;
                    item["1"] = "" + w.WeightAxle01;
                    item["2"] = "" + w.WeightAxle02;
                    item["3"] = "" + w.WeightAxle03;
                    item["4"] = "" + w.WeightAxle04;
                    item["5"] = "" + w.WeightAxle05;
                    item["6"] = "" + w.WeightAxle06;
                    item["7"] = "" + w.WeightAxle07;
                    item["8"] = "" + w.WeightAxle08;
                    item["9"] = "" + w.WeightAxle09;
                    item["10"] = "" + w.WeightAxle10;
                    items.Add(item);
                }
            }
            
            string json = new JavaScriptSerializer().Serialize(items);
            Response.Write(json);
        }


        private void getReport4(string stationId, string laneId, string startDate, string endDate, bool splitLane)
        {
            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
            List<_Rpt03> beans = new WeighingDAO().getReportByConditionForReport03(stationId, laneId, startDate, endDate, splitLane);
            if (beans != null)
            {

                foreach (_Rpt03 b in beans)
                {
                    Dictionary<string, string> item = new Dictionary<string, string>();
                    item["StationNameTh"] = "" + b.StationName;
                    item["True"] = "" + b.CountOfWeightOver;
                    item["False"] = "" + b.CountOfWeightNormal;
                    item["TruePercent"] = "" + b.CountOfWeightOverPercent;
                    item["FalsePercent"] = "" + b.CountOfWeightNormalPercent;
                    items.Add(item);
                }


                Console.WriteLine();
            }
            string json = new JavaScriptSerializer().Serialize(items);
            Response.Write(json);
        }
    }
}