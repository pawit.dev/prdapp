﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DMT.WS.Dto;
using DMT.WS.Utils;
using DMT.WS.Dao;
using DMT.WS.Biz;

namespace DMT
{
    public partial class Permission_Edit : System.Web.UI.Page
    {
        protected Boolean SaveSucceed
        {
            get;
            set;
        }

        protected string SaveResultMessage
        {
            get;
            set;
        }

        protected UserLoginDTO UserDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission(new int[] { ConfigurationUtil.USER_LEVEL_ADMIN }))
                Response.Redirect("LiveWeighing.aspx");

            SaveSucceed = false;
            SaveResultMessage = "";
            UserDTO = null;

            if (!IsPostBack)
            {
                prepareData();
                int id = Gm.toSafeInt(Request.Params["UserLevel"]);

                if ((id == ConfigurationUtil.USER_LEVEL_ADMIN) || (id == ConfigurationUtil.USER_LEVEL_MANAGER) || (id == ConfigurationUtil.USER_LEVEL_STAFF))
                {
                    if (id == ConfigurationUtil.USER_LEVEL_ADMIN)
                    {
                        lblUserLevel.Text = "Admin";
                    }
                    else if (id == ConfigurationUtil.USER_LEVEL_MANAGER)
                    {
                        lblUserLevel.Text = "Manager";
                    }
                    else if (id == ConfigurationUtil.USER_LEVEL_STAFF)
                    {
                        lblUserLevel.Text = "Staff";
                    }
                }
                else
                {
                    SaveResultMessage = "ไม่สามารถแก้ไขรายการที่ระบุได้";
                }
            }
        }

        private void prepareData()
        {
            int id = Gm.toSafeInt(Request.Params["UserLevel"]);
            ViewState["UserLevel"] = id;
            List<string> permissionCodes = new List<string>();
            List<UserPermissionDTO> userPermissions = new UserPermissionDAO().getByUserTypeId(id);
            foreach (UserPermissionDTO userPermission in userPermissions)
            {
                permissionCodes.Add(userPermission.PermissionCode);
            }
            List<PermissionDTO> permissions = new PermissionDAO().getAll();
            foreach (PermissionDTO permission in permissions)
            {
                ListItem item = new ListItem(permission.Name, permission.PermissionCode);
                if (permissionCodes.Contains(permission.PermissionCode))
                {
                    item.Selected = true;
                }
                chkPermissions.Items.Add(item);
            }
        }

        protected void btSave_OnClick(object Source, EventArgs e)
        {
            int id = Gm.toSafeInt(ViewState["UserLevel"]);
            UserPermissionDAO dao = new UserPermissionDAO();
            dao.deleteByUserType(id);
            foreach (ListItem item in chkPermissions.Items)
            {
                if (item.Selected)
                {
                    UserPermissionDTO dto = new UserPermissionDTO();
                    dto.PermissionCode = item.Value;
                    dto.UserTypeId = id;
                    if (dao.save(dto) != null)
                    {
                        SaveResultMessage = "บันทึกสำเร็จ";
                    }
                    else
                    {
                        SaveResultMessage = "บันทึกล้มเหลว";
                    }
                }
            }
            /*
            UserLoginDTO user = new UserLoginDTO();
            user.ID = Gm.toSafeInt(ViewState["UserID"]);
            user.Username = Gm.toSafeString(ViewState["Username"]);
            user.Password = txtPassword.Value;
            user.FullName = txtFullName.Value;
            user.Email = txtEMail.Value;
            user.Status = Gm.toSafeInt(lstUserStatus.Value);
            user.Level = Gm.toSafeInt(lstUserLevel.Value);

            UserLoginDAO dao = new UserLoginDAO();

            try
            {
                SaveSucceed = dao.update(user);
            }
            catch (Exception)
            {
                SaveSucceed = false;
            }

            SaveResultMessage = (SaveSucceed) ? "บันทึกข้อมูลผู้ใช้ใหม่เรียบร้อยแล้ว" : "บันทึกไม่สำเร็จ (Save Failed) !!!";
            */
        }
    }
}