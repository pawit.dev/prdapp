﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMT.WS.Biz;


namespace DMT
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserLoginService service = new UserLoginService();
            service.logout();
            Response.Redirect("Login.aspx");

        }
    }
}