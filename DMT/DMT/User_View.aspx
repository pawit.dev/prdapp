﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="User_View.aspx.cs" Inherits="DMT.User_View" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tbAddNewUser 
        {
            table-layout: fixed;
            border: 0px;
        }
        
        #tbAddNewUser td
        {
            padding-top: 0.3em;
            padding-bottom: 0.1em;
        }
        
        #tbAddNewUser td.label 
        {
            width: 25em;
            text-align: right;
            padding-right: 2em;
        }
        
        #tbAddNewUser td.field 
        {
            width: 30em;
            text-align: left;
        }
    </style>

    <style type="text/css">
        #divWrapper
        {
            display:inline-block;
            padding:0px;
            margin:0px;
            border:0px;
            width:100%;
            text-align: center;
        }
        
        #divSaveResult
        {
            display: inline-block;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            width: 450px;
            height: auto;
            border: 1px solid #DDD;
            border-radius: 4px;
            margin-top: 50px;
            background-color: #F7F7F7;
            margin-bottom: 10px;
        }
        
        #divSaveResult h1 
        {
            padding: 0px;
            margin: 0px;
            color: #269CCB;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ClientIDMode="Static">
<% if (ErrorMessage.Length > 0)
   { %>
    <div id="divWrapper">
        <div id="divSaveResult">
            <h1><%=ErrorMessage%></h1>
        </div>
        <br />
        <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>
    </div>
    <% }
   else
   { %>

    <h1>ข้อมูลผู้ใช้ระบบ (User Detail)</h1>
    <br />
    <table id="tbAddNewUser" cellpadding="0" cellspacing="0">
        <tr>
            <td class="label">Username :</td>
            <td class="field"><input id="txtUsername" type="text" runat="server" readonly /></td>
        </tr>
        <tr>
            <td class="label">Full Name :</td>
            <td class="field"><input id="txtFullName" type="text" runat="server" readonly /></td>
        </tr>
        <tr>
            <td class="label">E-Mail :</td>
            
            <td class="field"><input id="txtEMail" type="text" runat="server" readonly /></td>
        </tr>
        <tr>
            <td class="label">Status :</td>
            <td class="field">
                <select id="lstUserStatus" runat="server" disabled="disabled">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label">Level :</td>
            <td class="field">
                <select id="lstUserLevel" runat="server" disabled="disabled">
                    <option value="2">Manager</option>
                    <option value="3">Staff</option>
                    <option value="1">Admin</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="label">Level :</td>
            <td class="field">
                <select id="cboStation" runat="server" disabled="disabled">
                </select>
            </td>
        </tr>
    </table>

    <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>

    <% } %>
</asp:Content>
