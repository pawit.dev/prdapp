﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMT.WS.Utils;
using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Biz;


namespace DMT
{
    public partial class Report_View : System.Web.UI.Page
    {
        protected string ErrorMessage
        {
            get;
            set;
        }

        protected ReportDTO ReportDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_REPORT"))
                Response.Redirect("LiveWeighing.aspx");

            ErrorMessage = "";
            ReportDTO = null;

            if (!IsPostBack)
            {
                int id = Gm.toSafeInt(Request.QueryString["ReportID"]);

                if (id > 0)
                {
                    // Load User Info
                    ReportDAO dao = new ReportDAO();
                    ReportDTO report = dao.getByPrimaryKey(id) as ReportDTO;

                    if (report != null)
                    {
                        lblReportName.InnerHtml = report.Name;
                        lblReportDescription.InnerHtml = report.Description;
                        lblFilePath.InnerHtml = report.FilePath;
                        ReportDTO = report;
                    }
                    else
                    {
                        ErrorMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    ErrorMessage = "Error";
                }
            }
        }
    }
}