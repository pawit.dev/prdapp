﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using DMT.WS.Biz;
using DMT.WS.Utils;
using DMT.WS.Dao;
using DMT.WS.Dto;

namespace DMT
{
    public partial class LiveWeighing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("Login.aspx");
            if (!UserLoginUtil.hasPermission("VIEW_LIVE_WEIGHING"))
                Response.Redirect("NotFound.aspx");

            if (!IsPostBack)
            {
                prepareDropdownStation();
                prepareDropdownTimeSearch();
                //doSearch(1);
            }
        }

        private void prepareDropdownTimeSearch()
        {
            string hrFrom = Request.Params["hr_from"];
            string minFrom = Request.Params["min_from"];
            string hrTo = Request.Params["hr_to"];
            string minTo = Request.Params["min_to"];
            for (int i = 0; i < 24; i++)
            {
                string value = i.ToString();
                if (i < 10)
                {
                    value = "0" + i;
                }
                ListItem item1 = new ListItem(value, value);
                ListItem item2 = new ListItem(value, value);

                if (hrFrom == value)
                {
                    item1.Selected = true;
                }
                if (hrTo == value)
                {
                    item2.Selected = true;
                }

                cboHrFrom.Items.Add(item1);
                cboHrTo.Items.Add(item2);
            }

            for (int i = 0; i <= 59; i++)
            {
                string value = i.ToString();
                if (i < 10)
                {
                    value = "0" + i;
                }
                ListItem item1 = new ListItem(value, value);
                ListItem item2 = new ListItem(value, value);

                if (minFrom == value)
                {
                    item1.Selected = true;
                }
                if (minTo == value)
                {
                    item2.Selected = true;
                }

                cboMinFrom.Items.Add(item1);
                cboMinTo.Items.Add(item2);
            }
        }

        private void prepareDropdownStation()
        {
            if (UserLoginUtil.getCurrentUserLogin().Level == ConfigurationUtil.USER_LEVEL_ADMIN)
            {
                string requestStation = Request.Params["station"];
                station.Items.Add(new ListItem("All Station", ""));
                StationDAO dao = new StationDAO();
                List<StationDTO> dtos = dao.getAll();
                foreach (StationDTO dto in dtos)
                {
                    ListItem item = new ListItem(dto.StationNameEn, dto.ID.ToString());
                    station.Items.Add(item);
                    if (requestStation != null && requestStation == dto.ID.ToString())
                    {

                        station.SelectedIndex = station.Items.IndexOf(item);
                    }
                }
            }
            else
            {
                UserStationDTO userStation = new UserStationDAO().getActiveByUserLoginId(UserLoginUtil.getCurrentUserLogin().ID);
                //string requestStation = Request.Params["station"];
                //station.Items.Add(new ListItem("All Station", ""));
                if (userStation != null)
                {
                    StationDAO dao = new StationDAO();
                    StationDTO dto = dao.getByPrimaryKey(userStation.StationId) as StationDTO;
                    if (dto != null)
                    {
                        ListItem item = new ListItem(dto.StationNameEn, dto.ID.ToString());
                        station.Items.Add(item);
                    }
                }
            }
        }
        

        [WebMethod]
        public static void AjaxTest()
        {
            throw new Exception("You must supply an email address.");
        }
    }
}
