﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMT.WS.Biz;


namespace DMT
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserLoginUtil.isLogin())
            {
                Response.Redirect("LiveWeighing.aspx");
            }
            string submit = Request.Params["submit"];

            if (submit != null && submit != "")
            {
                string username = Request.Params["username"];
                string password = Request.Params["password"];

                UserLoginService service = new UserLoginService();                
                if (service.authen(username, password))
                {
                    Response.Redirect("LiveWeighing.aspx");
                }
                else
                {
                    Session["wrongpass"] = "true";
                    Response.Redirect("Login.aspx");
                }               
            }
            

            if (Session["wrongpass"] != null && Session["wrongpass"].ToString() != "")
            {
                wrongpassword.Visible = true;
                Session.Remove("wrongpass");
            }
            else
            {
                wrongpassword.Visible = false;
            }
        }
    }
}