﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMT.WS.Utils;
using DMT.WS.Dto;
using DMT.WS.Dao;
using DMT.WS.Biz;


namespace DMT
{
    public partial class Station_View : System.Web.UI.Page
    {
        protected string ErrorMessage
        {
            get;
            set;
        }

        protected StationDTO StationDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_STATION"))
                Response.Redirect("LiveWeighing.aspx");

            ErrorMessage = "";
            StationDTO = null;

            if (!IsPostBack)
            {
                int id = Gm.toSafeInt(Request.QueryString["StationID"]);

                if (id > 0)
                {
                    // Load User Info
                    StationDAO dao = new StationDAO();
                    StationDTO station = dao.getByPrimaryKey(id) as StationDTO;

                    if (station != null)
                    {

                        lblStationCode.InnerHtml = station.StationCode;
                        lblStationNameTH.InnerHtml = station.StationNameTh;
                        lblStationNameEN.InnerHtml = station.StationNameEn;
                        lblStationDescription.InnerHtml = station.Description;
                        StationDTO = station;
                    }
                    else
                    {
                        ErrorMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    ErrorMessage = "Error";
                }
            }
        }
    }
}