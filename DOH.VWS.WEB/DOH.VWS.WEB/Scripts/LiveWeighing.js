﻿
// System Param
var maxId = 0;
setInterval(checkData, 1000);

function checkData() {

    var $stationID = $('#MainContent_Ddl_Station  option:selected').val();
    var $year = 2013;//   new Date().getFullYear();
    $.ajax({
        url: "LiveViewAjax.aspx?type=GetNewContent&maxid=" + maxId + "&tmp=" + new Date().getTime() + "&stationID=" + $stationID + "&Year=" + $year,
        //    type: "POST",
        data: '',
        dataType: "json",
        success: function (data) {
            if (data.length > 0) {

                var str = '';
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    str += "<div id=\"entry-author-info\">" +
                "<div id=\"author-avatar\"><a href=\"" + item['Image01Name'] + "\" class=\"" + 'pic' + (i + 1) + " cboxElement\" title=\"Img Front\"><img alt src=\"" + item['Image01Name'] + "\" class=\"avator avatar-84 photo\" height=\"84\" width=\"84\"></a></div>" +
                "<div  onclick=\"window.location ='Weighing_View.aspx?WID=" + item['ID'] + "&Year=" + $year + "&StationID=" + $stationID + "'\"  id=\"author-description\">" +
                    "<h2 class=\"author\"> Station: " + item['StationName'] +" Lane: "+ item['Lane'] +" Time: "+ item['TimeStamp'] + "</h2>" +
                            "            <table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" width=\"700\">                    " +
                            "                <tr>                              " +
                            "                    <td   class=\"title4\">         " +
                            "                        Vehicle</td>              " +
                            "                    <td class=\"title4\">         " +
                            "                        Class</td>                " +
                            "                    <td class=\"title4\">         " +
                            "                        Axle</td>                 " +
                            "                    <td class=\"title4\">         " +
                            "                        Length (cm)</td>          " +
                            "                    <td class=\"title4\">         " +
                            "                        Speed (kph)</td>          " +
                            "                </tr>                             " +
                            "                <tr>                              " +
                            "                    <td class=\"title3\">" + item['VehicleNumber'] + "</td>               " +
                            "                    <td class=\"title3\">" + item['VehicleClass'] + "</td>               " +
                            "                    <td class=\"title3\">" + item['AxleCount'] + "</td>               " +
                            "                    <td class=\"title3\">" + item['Length'] + "</td>               " +
                            "                    <td class=\"title3\">" + item['Speed'] + "</td>               " +
                            "                    </tr>                         " +
                            "                <tr>                              " +
                            "                    <td class=\"title4\">         " +
                            "                        GVW (kg)</td>             " +
                            "                    <td class=\"title4\">         " +
                            "                        Max GVW</td>              " +
                            "                    <td class=\"title4\">         " +
                            "                        Max GVW (%)</td>          " +
                            "                    <td class=\"title4\">         " +
                            "                        Status</td>               " +
                            "                    <td>                          " +
                            "                        &nbsp;</td>               " +
                            "                    </tr>                         " +
                            "                <tr>                              " +
                            "                    <td class=\"title3\">" + item['GVW'] + "</td>               " +
                            "                    <td class=\"title3\">" + item['MaxGVW'] + "</td>               " +
                            "                    <td class=\"title3\">" + item['MaxGVWP'] + "</td>               " +
                            "                    <td class=\"title3\">         " +
                            ((item['Isover'] == "T")? "<img src=\"/images/ico_circle_red.png\" />":"<img src=\"/images/ico_circle_green.png\" />")+

                            "                        &nbsp;</td>               " +
                            "                    <td class=\"title3\">         " +
                            "                        &nbsp;</td>               " +
                            "                    </tr>                         " +
                            "                <tr>                              " +
                            "                    <td colspan=\"5\"><img border=\"0\" src=\"/images/vclass/class" + item['VehicleClass'] + ".jpg\" width=\"130\" height=\"37\" /></td>               " +
                            "                    </tr>                         " +
                            "            </table>                              " +

                    //======================================================= DESC ===================================================    
                "<\/div><\/div>";
                }


                $("#tbReport").html(str);
            }
        }
    });
}



