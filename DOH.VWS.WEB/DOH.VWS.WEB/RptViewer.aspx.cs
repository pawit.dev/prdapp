﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;

namespace DOH.VWSz.WEB
{
    public partial class RptViewer : System.Web.UI.Page
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RptViewer));
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ReportDocument rptDoc = (ReportDocument)Session["RptDoc"];
                if (rptDoc != null)
                {

                    rptDoc.SetParameterValue("P_START_DATE", Convert.ToDateTime(Session["P_START_DATE"]));
                    rptDoc.SetParameterValue("P_END_DATE", Convert.ToDateTime(Session["P_END_DATE"]));
                    
                    //CrystalReportViewer1.ReportSource = rptDoc;
                    //CrystalReportViewer1.DataBind();

                }
                else
                {
                    logger.Debug("rptDoc is null.");
                }
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                logger.Error(ex.StackTrace);
            }
        }
    }
}