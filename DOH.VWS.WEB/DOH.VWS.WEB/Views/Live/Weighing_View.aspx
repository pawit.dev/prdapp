﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Weighing_View.aspx.cs" Inherits="DOH.VWS.WEB.Views.Live.Weighing_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            font-family: Tahoma, Arial, Geneva, Sans-Serif;
            font-size: 12px;
            text-align: left;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $(".pic1").colorbox({ rel: 'pic1' });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="title2">
        Weighing Detail<br />
    </div>

    <asp:HiddenField ID="HID" runat="server" />
    <fieldset>
        <table>
            <tr>
                <td width="15%" style="padding: 10px;">
                <% 
                    if (model != null)
                    {
                    %>
                    <a href="<%= model.LicensePlateImageName %>" class="pic1" title="Img Front">
                        <img alt="" border="0" src="<%= model.Image01Name%>" width="120" /></a><br />
                    <br />
                    <a href="<%= model.LicensePlateImageName %>" class="pic1" title="Img Front">
                        <img alt="" border="0" src="<%= model.Image01Name%>" width="120" /></a></td>
                        <%} %>
                <td valign="top">
                    <table width="100%" class="tbl1">
                        <tr>
                            <th class="style1">
                                Vehicle
                            </th>
                            <th class="style1">
                                Class
                            </th>
                            <th class="style1">
                                Axle
                            </th>
                            <th class="style1">
                                Length (cm)
                            </th>
                            <th class="style1">
                                Speed (kph)
                            </th>
                            <th class="style1">
                                GVW (kg)
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="l_vehicle" runat="server" Text="-"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="l_class" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                <asp:Label ID="l_axle" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                <asp:Label ID="l_length" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                <asp:Label ID="l_speed" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                <asp:Label ID="l_gvw" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                <strong style="text-align: center">Max Gvw(kg)</strong>
                            </td>
                            <td class="style1">
                                <strong>Max Gvw(%)</strong>
                            </td>
                            <td class="style1">
                                <strong>License Plate Jurisdiction</strong>
                            </td>
                            <td class="style1">
                                <strong>License Plate Number</strong>
                            </td>
                            <td class="style1">
                                <strong>LP Number Confidence</strong>
                            </td>
                            <td class="number">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="l_maxGvw" runat="server" Text="-"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="l_maxGvwPercent" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                <asp:Label ID="l_LicensePlateJurisdiction" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                <asp:Label ID="l_LicensePlateNumber" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                <asp:Label ID="l_LPNumberConfidence" runat="server" Text="-"></asp:Label>
                            </td>
                            <td class="number">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <br />
                                WIM Compliance<br />
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                                    BorderColor="Tan" BorderWidth="1px" CellPadding="2" CssClass="tb_ResultList" ForeColor="Black" GridLines="None">
                                    <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                    <Columns>
                                        <asp:BoundField DataField="Axle" HeaderText="Axle" />
                                        <asp:BoundField DataField="Separation" HeaderText="Separation (cm)" />
                                        <asp:BoundField DataField="LeftWeight" HeaderText="Left Weight (kg)" />
                                        <asp:BoundField DataField="RightWeight" HeaderText="Right Weight (kg)" />
                                        <asp:BoundField DataField="TotalWeight" HeaderText="Total Weight (kg)" />
                                        <asp:BoundField DataField="AllowableWeight" HeaderText="Allowable Weight" />
                                        <asp:BoundField DataField="WeightViolation" HeaderText="Weight Violation" />
                                        <asp:BoundField DataField="GroupType" HeaderText="Group Type" />
                                        <asp:BoundField DataField="GroupWeight" HeaderText="Group Weight(kg)" />
                                        <asp:BoundField DataField="GroupAllowableWeight" HeaderText="Group Allowable Weight(kg)" />
                                    </Columns>
                                    <FooterStyle BackColor="Tan" />
                                    <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" 
                                        HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                    <SortedAscendingCellStyle BackColor="#FAFAE7" />
                                    <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                                    <SortedDescendingCellStyle BackColor="#E1DB9C" />
                                    <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                                </asp:GridView>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                            &nbsp;&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" 
                                    ImageUrl="~/Images/pdf.png" onclick="ImageButton1_Click" style="width: 16px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Content>
