﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Biz;
using System.Data;
using DOH.VWS.Dto;
using DOH.VWS.Utils;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;

namespace DOH.VWS.WEB.Views.Live
{
    public partial class LiveView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/Login.aspx");
            //Load Default
            LoadDefault();
        }

        private void LoadDefault()
        { 
            StationBAO bao = new StationBAO();
            DataSet ds = bao.LoadStation();

            if (ds != null)
            {
                
                Ddl_Station.DataSource = ds;
                Ddl_Station.DataValueField = "StationCode";
                Ddl_Station.DataTextField = "StationNameTh";

                Ddl_Station.DataBind();
                
                
            }
            else { 
                
            }
           
        }

        private void getNewContent(int id, int station, int year)
        {


            List<Dictionary<string, string>> items = new List<Dictionary<string, string>>();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["WimConStr"].ConnectionString))
            {
                connection.Open();
                StringBuilder sql = new StringBuilder();
                sql.Append("select top 5 VehicleNumber,VehicleClass,AxleCount,Axle01Seperation,Speed,GVW,MaxGVW,* from WimData ");
                sql.Append((station == 0) ? " Where Year([TimeStamp])=" + year : " Where StationID=" + station + " and Year([TimeStamp])=" + year);
                sql.Append(" order by timestamp desc");

                using (SqlCommand command = new SqlCommand(sql.ToString(), connection))
                {
                    // Make sure the command object does not already have
                    // a notification object associated with it.
                    command.Notification = null;

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                    int running = 1;
                    while (dr.Read())
                    {

                        Dictionary<string, string> item = new Dictionary<string, string>();
                        DateTime dt = Gm.toSafeDateTime(dr["TimeStamp"]);
                        item["ID"] = "" + dr["ID"];
                        item["StationID"] = "" + dr["StationID"].ToString();
                        item["Year"] = "" + dt.Year;

                        item["Image01Name"] = "" + String.Format(Configurations.ImageWebPath, Convert.ToInt16(dr["StationID"]).ToString("00"), dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), dr["Image01Name"]);

                        if (String.IsNullOrEmpty(Path.GetExtension(item["Image01Name"])))
                        {
                            item["Image01Name"] = "/images/inf.jpg";
                        }

                        item["VehicleNumber"] = "" + CommonUtils.convert2UShor((dr["VehicleNumber"].ToString()));

                        item["VehicleClass"] = "" + dr["VehicleClass"];
                        item["AxleCount"] = "" + dr["AxleCount"];
                        item["Length"] = "" + dr["Length"];
                        item["Speed"] = "" + dr["Speed"];
                        item["GVW"] = "" + CommonUtils.customNumberFormat(dr["GVW"].ToString());
                        item["MaxGVW"] = "" + CommonUtils.customNumberFormat(dr["MaxGVW"].ToString());
                        item["MaxGVWP"] = "" + Math.Round((Convert.ToDouble(dr["GVW"]) / Convert.ToDouble(dr["MaxGVW"])) * 100);
                        item["Isover"] = (Convert.ToInt32(dr["GVW"].ToString()) > Convert.ToInt32(dr["MaxGVW"].ToString())) ? "T" : "F";
                        item["TimeStamp"] = "" + dt.ToString("dd/MM/yyy hh:MM tt");
                        item["Lane"] = "" + dr["Lane"];
                        item["StationName"] = "" + (dr["StationID"].ToString().Equals("01") ? "Banna" : "NakornChaiSri");

                        running++;
                        items.Add(item);
                    }
                    dr.Close();
                }
            }
            string json = new JavaScriptSerializer().Serialize(items);
            Response.Write(json);
        }
    }
}