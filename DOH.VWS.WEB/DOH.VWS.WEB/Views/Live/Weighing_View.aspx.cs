﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Utils;
using DOH.VWS.Biz;
using DOH.VWS.Dao;
using DOH.VWS.Model;
using DOH.VWS.Dto;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using DOH.VWS.WEB.ReportObjects;
using System.Net;

namespace DOH.VWS.WEB.Views.Live
{
    public partial class Weighing_View : System.Web.UI.Page
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Weighing_View));

        protected ModelVehicleRecord model
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int id = Gm.toSafeInt(Request.QueryString["WID"]);
                int year = Gm.toSafeInt(Request.QueryString["Year"]);
                int station = Gm.toSafeInt(Request.QueryString["StationID"]);

                VehicleRecordBiz biz = new VehicleRecordBiz();
                ProvinceDAO provinceDao = new ProvinceDAO();
                VehicleRecordDAO dao = new VehicleRecordDAO();
                model = dao.getByPK( id);
                if (model != null)
                {
                    HID.Value = model.Id+"";
                    DateTime dt = Gm.toSafeDateTime(model.TimeStamp);

                    model.Image01Name = String.Format(Configurations.ImageWebPath, Convert.ToInt16(model.StationID).ToString("00"), dt.Year, dt.Month.ToString("00"), dt.Day.ToString("00"), model.Image01Name);
                    if (String.IsNullOrEmpty(Path.GetExtension(model.Image01Name)))
                    {
                        model.Image01Name = "/images/inf.jpg";
                    }

                    model.LicensePlateImageName = model.Image01Name;
                    l_vehicle.Text = CommonUtils.convert2UShor(model.VehicleNumber+"")+"";
                    l_class.Text = model.VehicleClass + "";
                    l_axle.Text = model.AxleCount + "";
                    l_length.Text = model.Length + "";
                    l_speed.Text = model.Speed + "";
                    l_gvw.Text = CommonUtils.customNumberFormat(model.GVW+"") + "";
                    l_maxGvw.Text = CommonUtils.customNumberFormat(model.MaxGVW+"")+"";
                    l_maxGvwPercent.Text = Math.Round(Convert.ToDouble(model.GVW) / Convert.ToDouble(model.MaxGVW) * 100) + "";

                    ProvinceDTO prov = (ProvinceDTO)provinceDao.getByPrimaryKey(model.LicensePlateProvinceID);
                    if (prov != null)
                    {
                        l_LicensePlateJurisdiction.Text = prov.ProvinceName + "";
                    }
                    l_LicensePlateNumber.Text = model.LicensePlateNumber + "";
                    l_LPNumberConfidence.Text = model.LicensePlateConfidence + "";

                    GridView1.DataSource = biz.getCompliance( id);
                    GridView1.DataBind();
                }
                else {
                    logger.Debug(" Vehicle data is null.");
                }

            }

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

                var webClient = new WebClient();
                ProvinceDAO provinceDao = new ProvinceDAO();
                VehicleRecordDAO dao = new VehicleRecordDAO();
                ModelVehicleRecord mv = dao.getByPK(Convert.ToInt32(HID.Value));
                if (mv != null)
                {
                List<Rpt102Model> List = new List<Rpt102Model>();

                Rpt102Model tmp = new Rpt102Model();
                tmp.VehicleNumber = Convert.ToInt32(CommonUtils.convert2UShor(mv.VehicleNumber.ToString())); ;
                tmp.VehicleClass = mv.VehicleClass;
                tmp.AxleCount = mv.AxleCount;
                tmp.Speed = mv.Speed;
                tmp.Length = mv.Length;
                tmp.GVW = mv.GVW;
                tmp.MaxGVW = mv.MaxGVW;
                tmp.Lane = mv.Lane;
                tmp.LicensePlateNumber = mv.LicensePlateNumber;
                tmp.LicensePlateConfidence = mv.LicensePlateConfidence;


                ProvinceDTO prov = (ProvinceDTO)provinceDao.getByPrimaryKey(tmp.LicensePlateProvinceID);
                if (prov != null)
                {
                    tmp.provinceName = prov.ProvinceName + "";
                }

                String imgPath = String.Format(Configurations.ImageWebPath, mv.StationID.ToString("00"), mv.TimeStamp.Year, mv.TimeStamp.Month.ToString("00"), mv.TimeStamp.Day.ToString("00"), mv.Image01Name);

                if (String.IsNullOrEmpty(Path.GetExtension(imgPath)))
                {
                    imgPath = "http://" + HttpContext.Current.Request.Url.Authority + "/images/inf.jpg";
                }
                if (!String.IsNullOrEmpty(Path.GetExtension(imgPath)))
                {
                    try
                    {
                        tmp.frontImage = webClient.DownloadData(imgPath);
                        tmp.backImage = tmp.frontImage;
                    }
                    catch (Exception ex)
                    {

                    }
                }
                List.Add(tmp);

                VehicleRecordBiz biz = new VehicleRecordBiz();


                ReportDocument rptH = new ReportDocument();
                rptH.Load(Server.MapPath("~/ReportObjects/RPT101_2.rpt"));
                rptH.SetDataSource(List);
                rptH.Subreports["Rpt102_2_sub.rpt"].SetDataSource(biz.getCompliance(Convert.ToInt32(mv.Id)));
                rptH.PrintToPrinter(1, false, 1, 1);
                //Session["RptDoc"] = rptH;
                //Response.Redirect("/ReportViewer.aspx");
            }
        }

    }
}