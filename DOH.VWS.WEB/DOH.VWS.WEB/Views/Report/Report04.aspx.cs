﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Model;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using DOH.VWS.Dao;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using DOH.VWS.WEB.ReportObjects;
using DOH.VWS.Biz;
using System.Data;
using DOH.VWS.Dto;
using DOH.VWS.Utils;
using System.Net;

namespace DOH.VWS.WEB.Views.Report
{

    public partial class Report04 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.isLogin())
                Response.Redirect("/Login.aspx");

            //Load Default
            if (!Page.IsPostBack)
            {
                LoadDefault();
            }
        }

        protected void BtnShowReport_Click(object sender, EventArgs e)
        {
            VehicleRecordDAO dao = new VehicleRecordDAO();
            List<R101_1Model> SearchResult = new List<R101_1Model>();

            //criteria
            int year = Convert.ToDateTime(txtStart.Text).Year;
            int stationID = Convert.ToInt16(Ddl_Station.SelectedValue);
            StringBuilder sbCri = new StringBuilder();
            #region "Create Criteria"
            if (!String.IsNullOrWhiteSpace(Ddl_Station.SelectedValue))
            {
                if (!Ddl_Station.SelectedValue.Equals("00"))
                {
                    sbCri.Append("and StationID=" + stationID);
                }
            }
            if (Cb_OverWeight.Checked)
            {
                sbCri.Append("and GVW > MaxGVW ");
            }
            if (!String.IsNullOrWhiteSpace(txtStart.Text) && !String.IsNullOrWhiteSpace(txtEnd.Text))
            {
                sbCri.Append("and CONVERT(VARCHAR(10),TimeStamp,121) between CONVERT(VARCHAR(10),'" + Convert.ToDateTime(txtStart.Text).ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-Us")) + "',121) and CONVERT(VARCHAR(10),'" + Convert.ToDateTime(txtEnd.Text).ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-Us")) + "',121) ");
            }
            if (!String.IsNullOrWhiteSpace(Ddl_Class.SelectedValue))
            {
                sbCri.Append("and VehicleClass=" + Convert.ToInt16(Ddl_Class.SelectedValue));
            }
            if (!String.IsNullOrWhiteSpace(txtLicensePlate.Text))
            {
                sbCri.Append("and LicensePlateNumber='" + txtLicensePlate.Text + "'");
            }
            if (!String.IsNullOrWhiteSpace(Ddl_Province.SelectedValue))
            {
                sbCri.Append("and LicensePlateProvinceID=" + Ddl_Province.SelectedValue);
            }

            int count = Regex.Matches(sbCri.ToString(), "and").Count;
            String cri = "";
            if (count > 0)
            {
                cri = "where " + sbCri.ToString().Substring(4, sbCri.ToString().Length - 4);
            }
            #endregion



            List<ModelVehicleRecord> lists = dao.select(cri);
            if (lists.Count > 0)
            {
                foreach (ModelVehicleRecord vr in lists)
                {
                    R101_1Model r101 = new R101_1Model();
                    r101.id = Convert.ToInt64(vr.Id);
                    r101.Date = Convert.ToDateTime(vr.TimeStamp).ToString("yyyyMMdd", new CultureInfo("en-Us"));
                    r101.Time = Convert.ToDateTime(vr.TimeStamp).ToString("hh:mm:ss", new CultureInfo("en-Us"));
                    r101.StationID = vr.StationID;
                    r101.Lane = Convert.ToInt16(vr.Lane);
                    r101.VehNumber = Convert.ToInt32(CommonUtils.convert2UShor(vr.VehicleNumber.ToString()));
                    r101.VehicleClass = Convert.ToInt16(vr.VehicleClass);
                    r101.MaxGVW = Convert.ToInt32(vr.MaxGVW);
                    r101.GVW = Convert.ToInt32(vr.GVW);
                    r101.Axles = Convert.ToInt16(vr.AxleCount);
                    r101.LicensePlate = vr.LicensePlateNumber;
                    r101.Province = vr.LicensePlateProvinceID + "";
                    r101.Status = vr.StatusCode + "";
                    SearchResult.Add(r101);

                }
            }
            else
            {
                //lblDataNotFound.Text = "- ไม่พบข้อมูล -";
            }

            if (SearchResult != null)
            {
                GridView1.DataSource = SearchResult;
                GridView1.DataBind();
                Session["SearchResult"] = SearchResult;
            }

        }
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Report04.aspx");
        }


        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex < 0) return;
            GridView gv = (GridView)sender;
            gv.DataSource = Session["SearchResult"];
            gv.PageIndex = e.NewPageIndex;
            gv.DataBind();
        }

        protected void Img_Command(object sender, CommandEventArgs e)
        {
            String commandName = e.CommandName;
            int id = Convert.ToInt32(e.CommandArgument.ToString().Split(',')[0]);
            switch (e.CommandName.ToUpper())
            {
                case "VIEW":
                    var webClient = new WebClient();
                    int year = Convert.ToDateTime(txtStart.Text).Year;
                    int stationID = Convert.ToInt16(Ddl_Station.SelectedValue);

                    ProvinceDAO provinceDao = new ProvinceDAO();
                    VehicleRecordDAO dao = new VehicleRecordDAO();
                    ModelVehicleRecord mv = dao.getByPK(id);

                    List<Rpt102Model> List = new List<Rpt102Model>();

                    Rpt102Model model = new Rpt102Model();
                    model.VehicleNumber = Convert.ToInt32(CommonUtils.convert2UShor(mv.VehicleNumber.ToString())); ;
                    model.VehicleClass = mv.VehicleClass;
                    model.AxleCount = mv.AxleCount;
                    model.Speed = mv.Speed;
                    model.Length = mv.Length;
                    model.GVW = mv.GVW;
                    model.MaxGVW = mv.MaxGVW;
                    model.Lane = mv.Lane;
                    model.LicensePlateNumber = mv.LicensePlateNumber;
                    model.LicensePlateConfidence = mv.LicensePlateConfidence;


                    ProvinceDTO prov = (ProvinceDTO)provinceDao.getByPrimaryKey(model.LicensePlateProvinceID);
                    if (prov != null)
                    {
                        model.provinceName = prov.ProvinceName + "";
                    }

                    String imgPath = String.Format(Configurations.ImageWebPath, mv.StationID.ToString("00"), mv.TimeStamp.Year, mv.TimeStamp.Month.ToString("00"), mv.TimeStamp.Day.ToString("00"), mv.Image01Name);

                    if (String.IsNullOrEmpty(Path.GetExtension(imgPath)))
                    {
                        imgPath = "http://" + HttpContext.Current.Request.Url.Authority + "/images/inf.jpg";
                    }
                    if (!String.IsNullOrEmpty(Path.GetExtension(imgPath)))
                    {
                        try
                        {
                            model.frontImage = webClient.DownloadData(imgPath);
                            model.backImage = model.frontImage;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    List.Add(model);

                    VehicleRecordBiz biz = new VehicleRecordBiz();



                    ReportDocument rptH = new ReportDocument();
                    rptH.Load(Server.MapPath("~/ReportObjects/RPT101_2.rpt"));
                    rptH.SetDataSource(List);
                    rptH.Subreports["Rpt102_2_sub.rpt"].SetDataSource(biz.getCompliance(id));
                    Session["RptDoc"] = rptH;
                    Response.Redirect("/ReportViewer.aspx");
                    break;
            }

        }

        private void LoadDefault()
        {
            StationBAO StationBao = new StationBAO();
            VehicleClassDAO vehicleDao = new VehicleClassDAO();
            ProvinceDAO provinceDao = new ProvinceDAO();

            DataSet dsStation = StationBao.LoadStation();
            if (dsStation != null)
            {
                Ddl_Station.DataSource = dsStation;
                Ddl_Station.DataValueField = "StationCode";
                Ddl_Station.DataTextField = "StationNameTh";
                Ddl_Station.DataBind();
            }

            List<VehicleClassDTO> listVc = vehicleDao.getAll();
            if (listVc != null && listVc.Count > 0)
            {
                Ddl_Class.DataSource = listVc;
                Ddl_Class.DataValueField = "vehicleID";
                Ddl_Class.DataTextField = "vehicleName";
                Ddl_Class.DataBind();
                Ddl_Class.Items.Insert(0, new ListItem("Select", ""));
            }

            List<ProvinceDTO> listProvince = provinceDao.getAll();
            if (listProvince != null && listProvince.Count > 0)
            {
                Ddl_Province.DataSource = listProvince;
                Ddl_Province.DataValueField = "ProvinceID";
                Ddl_Province.DataTextField = "ProvinceName";
                Ddl_Province.DataBind();
                Ddl_Province.Items.Insert(0, new ListItem("Select", ""));
            }
        }


    }
}