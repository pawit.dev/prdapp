﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Report04.aspx.cs" Inherits="DOH.VWS.WEB.Views.Report.Report04" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                ค้นหา - รายการรถบรรทุกเข้าชั่ง<br />
            </div>
            <fieldset>
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            เฉพาะรถที่มีน้ำหนักเกิน
                        </td>
                        <td>
                            <asp:CheckBox ID="Cb_OverWeight" runat="server" />
                        </td>
                        <td class="HeadRow">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            สถานี
                        </td>
                        <td>
                            <asp:DropDownList ID="Ddl_Station" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="01">นครชัยศรีฯ</asp:ListItem>
                                <asp:ListItem Value="02">บ้านนา</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="HeadRow">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            เวลาเริ่มต้น:
                        </td>
                        <td>
                            <asp:TextBox ID="txtStart" runat="server" required></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStart">
                            </asp:CalendarExtender>
                        </td>
                        <td class="HeadRow">
                            เวลาสิ้นสุด:
                        </td>
                        <td>
                            <asp:TextBox ID="txtEnd" runat="server" required></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtEnd" runat="server">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            ประเภท
                        </td>
                        <td>
                            <asp:DropDownList ID="Ddl_Class" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="HeadRow">
                            เลขทะเบียนรถ </th>
                            <td>
                                <asp:TextBox ID="txtLicensePlate" runat="server"></asp:TextBox>
                            </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            จังหวัด
                        </td>
                        <td>
                            <asp:DropDownList ID="Ddl_Province" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td class="HeadRow">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <th valign="top">
                            &nbsp;
                        </th>
                        <td>
                            <asp:Button ID="BtnShowReport" runat="server" OnClick="BtnShowReport_Click" Text="แสดงรายงาน" />
                            <asp:Button ID="BtnCancel" runat="server" OnClick="BtnCancel_Click" 
                                Text="ยกเลิก" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <table class="tb_Result">
                    <tr>
                        <td class="left">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="center">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
                                BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None"
                                CssClass="tb_ResultList" AllowPaging="True" 
                                OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="20">
                                <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                    <asp:BoundField DataField="Time" HeaderText="Time" />
                                    <asp:BoundField DataField="StationID" HeaderText="StationID" />
                                    <asp:BoundField DataField="Lane" HeaderText="Lane" />
                                    <asp:BoundField DataField="VehNumber" HeaderText="VehNumber" />
                                    <asp:BoundField DataField="VehicleClass" HeaderText="VehicleClass" />
                                    <asp:BoundField DataField="MaxGVW" HeaderText="MaxGVW" />
                                    <asp:BoundField DataField="GVW" HeaderText="GVW" />
                                    <asp:BoundField DataField="Axles" HeaderText="Axles" />
                                    <asp:BoundField DataField="LicensePlate" HeaderText="LicensePlate" />
                                    <asp:BoundField DataField="Province" HeaderText="Province" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" 
                                                ImageUrl="~/Images/search.png" Text="Select" OnCommand="Img_Command" CommandName="View" CommandArgument='<%# Eval("id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
<%--                    <PagerTemplate>
                        <div class="RowDiv">
                            <div class="DivPaging">
                                <asp:LinkButton ID="btnFirst" runat="server" CssClass="btnArrowFirst" CommandName="Page"
                                    CommandArgument="First" CausesValidation="false" ToolTip="First Page" />
                                <asp:LinkButton ID="btnPrev" runat="server" CssClass="btnArrowBack" CommandName="Page"
                                    CommandArgument="Prev" CausesValidation="false" ToolTip="Previous Page" />
                                <asp:Panel ID="PnlNumberPage" runat="server" CssClass="DivNumberPage" />
                                <asp:LinkButton ID="btnNext" runat="server" CssClass="btnArrowNext" CommandName="Page"
                                    CommandArgument="Next" CausesValidation="false" ToolTip="Next Page" />
                                <asp:LinkButton ID="btnLast" runat="server" CssClass="btnArrowLast" CommandName="Page"
                                    CommandArgument="Last" CausesValidation="false" ToolTip="Last Page" />
                            </div>
                        </div>
                    </PagerTemplate>--%>
                                <EmptyDataTemplate>

                                    <asp:Label ID="lblDataNotFound" runat="server" Text="- ไม่พบข้อมูล -" CssClass="validate" />
                                </EmptyDataTemplate>
                                <FooterStyle BackColor="Tan" />
                                <HeaderStyle BackColor="Tan" Font-Bold="True" />
                                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
