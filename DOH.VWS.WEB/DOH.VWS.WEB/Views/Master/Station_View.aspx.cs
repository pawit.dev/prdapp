﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Utils;
using DOH.VWS.Dto;
using DOH.VWS.Dao;
using DOH.VWS.Biz;


namespace DOH.VWS.WEB.Views.Master
{
    public partial class Station_View : System.Web.UI.Page
    {
        protected string ErrorMessage
        {
            get;
            set;
        }

        protected StationDTO StationDTO
        {
            get;
            private set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("VIEW_STATION"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            ErrorMessage = "";
            StationDTO = null;

            if (!IsPostBack)
            {
                int id = Gm.toSafeInt(Request.QueryString["StationID"]);

                if (id > 0)
                {
                    // Load User Info
                    StationDAO dao = new StationDAO();
                    StationDTO station = dao.getByPrimaryKey(id) as StationDTO;

                    if (station != null)
                    {

                        lblStationCode.InnerHtml = station.StationCode;
                        lblStationNameTH.InnerHtml = station.StationNameTh;
                        lblStationNameEN.InnerHtml = station.StationNameEn;
                        lblStationDescription.InnerHtml = station.Description;
                        StationDTO = station;
                    }
                    else
                    {
                        ErrorMessage = "ไม่พบรายการที่ระบุ";
                    }
                }
                else
                {
                    ErrorMessage = "Error";
                }
            }
        }
    }
}