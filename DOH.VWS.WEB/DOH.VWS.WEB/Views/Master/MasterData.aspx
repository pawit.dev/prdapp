﻿<%@ Page Title="Default" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="MasterData.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.MasterData" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                Menu
            </div>
            <div class="icon ico-user-management">
                <a href="UserManagement.aspx">Users</a></div>
            <div class="icon ico-permission">
                <a href="ManagePermission.aspx">Permission</a></div>
            <div class="icon ico-station">
                <a href="StationManagement.aspx">Station</a></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
