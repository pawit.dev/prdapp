﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DOH.VWS.Utils;
using DOH.VWS.Dto;
using DOH.VWS.Dao;
using DOH.VWS.Biz;




namespace DOH.VWS.WEB.Views.Master
{
    public partial class Station_Add : System.Web.UI.Page
    {
        protected Boolean SaveSucceed
        {
            get;
            set;
        }

        protected string SaveResultMessage
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserLoginUtil.hasPermission("ADD_STATION"))
                Response.Redirect(ConfigurationUtil.getDefaultPage());

            SaveSucceed = false;
            SaveResultMessage = "";
        }

        protected void btSave_OnClick(object Source, EventArgs e)
        {
            StationDTO station = new StationDTO();
            station.StationCode = txtCode.Value;
            station.StationNameEn = txtNameEN.Value;
            station.StationNameTh = txtNameTH.Value;
            station.Description = txtDescription.Value;

            StationDAO dao = new StationDAO();
            StationDTO stationResult = null;

            try
            {
                stationResult = dao.save(station) as StationDTO;
            }
            catch (Exception)
            {
                stationResult = null;
            }

            SaveSucceed = (stationResult != null);
            SaveResultMessage = (SaveSucceed) ? "บันทึกข้อมูลใหม่เรียบร้อยแล้ว" : "บันทึกไม่สำเร็จ (Save Failed) !!!";

            if (SaveSucceed)
            {
                // clear input
                txtCode.Value = "";
                txtNameEN.Value = "";
                txtNameTH.Value = "";
                txtDescription.Value = "";
            }
        }
    }
}