﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="User_View.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.User_View" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                ข้อมูลผู้ใช้ระบบ (User Detail)<br />
            </div>
            <fieldset>
                <% if (ErrorMessage.Length > 0)
                   { %>
                <div id="divWrapper">
                    <div id="divSaveResult">
                        <h1>
                            <%=ErrorMessage%></h1>
                    </div>
                    <br />
                    <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>
                </div>
                <% }
                   else
                   { %>

                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Username :
                        </td>
                        <td class="field">
                            <input id="txtUsername" type="text" runat="server" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Full Name :
                        </td>
                        <td class="field">
                            <input id="txtFullName" type="text" runat="server" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            E-Mail :
                        </td>
                        <td class="field">
                            <input id="txtEMail" type="text" runat="server" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Status :
                        </td>
                        <td class="field">
                            <select id="lstUserStatus" runat="server" disabled="disabled">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Level :
                        </td>
                        <td class="field">
                            <select id="lstUserLevel" runat="server" disabled="disabled">
                                <option value="2">Manager</option>
                                <option value="3">Staff</option>
                                <option value="1">Admin</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Level :
                        </td>
                        <td class="field">
                            <select id="cboStation" runat="server" disabled="disabled">
                            </select>
                        </td>
                    </tr>
                </table>
                <a href="UserManagement.aspx">ย้อนกลับ (Back)</a>
                <% } %>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
