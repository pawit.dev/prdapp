﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Station_Edit.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.Station_Edit" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function ValidateInputForm() {
            return true;
        }


        $(function () {
            $('#btClear').click(function () {
                window.location = 'StationManagement.aspx';
            });
        });

    </script>
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                แก้ไขข้อมูลผู้ใช้ระบบ (Edit User)<br />
            </div>
            <fieldset>
                <% if (SaveResultMessage.Length > 0)
                   { %>
                <div id="divWrapper">
                    <div id="divSaveResult">
                        <h1>
                            <%=SaveResultMessage%></h1>
                    </div>
                    <br />
                    <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
                </div>
                <% }
                   else
                   { %>
                <form id="Form1" action="Station_Edit.aspx" method="post" onsubmit="return ValidateInputForm()">
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Station Code :
                        </td>
                        <td class="field">
                            <label runat="server" id="lblStationCode">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Name TH :
                        </td>
                        <td class="field">
                            <input id="txtNameTH" type="text" runat="server" enableviewstate="false" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Name EN :
                        </td>
                        <td class="field">
                            <input id="txtNameEN" type="text" runat="server" enableviewstate="false" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Description :
                        </td>
                        <td class="field">
                            <input id="txtDescription" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            &nbsp;
                        </td>
                        <td class="field">
                            <input id="Submit1" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                            <input id="Reset1" type="reset" value="Cancel" />
                        </td>
                    </tr>
                </table>
                </form>
                <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
                <% } %>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
