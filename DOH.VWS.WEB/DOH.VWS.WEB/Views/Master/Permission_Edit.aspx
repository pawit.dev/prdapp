﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Permission_Edit.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.Permission_Edit" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                แก้ไขสิทธิ์การเข้าถึง
            </div>
            <fieldset>
                <% if (SaveResultMessage.Length > 0)
                   { %>
                <div id="divWrapper">
                    <div id="divSaveResult">
                        <h1>
                            <%=SaveResultMessage%></h1>
                    </div>
                    <br />
                    <a href="ManagePermission.aspx">ย้อนกลับ (Back)</a>
                </div>
                <% }
                   else
                   { %>
                <form id="Form1" action="Permission_Edit.aspx" method="post" onsubmit="return ValidateInputForm()">
                <br />
                <% if (SaveResultMessage.Length > 0)
                   { %>
                <div id="div1">
                    <div id="div2">
                        <h1>
                            <%=SaveResultMessage%></h1>
                    </div>
                </div>
                <% } %>
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            User Level :
                        </td>
                        <td class="field">
                            <asp:Label ID="lblUserLevel" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Permission :
                        </td>
                        <td class="field">
                            <asp:CheckBoxList ID="chkPermissions" runat="server">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            &nbsp;
                        </td>
                        <td colspan="2" class="Search">
                            <input id="btSave" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                            <%--                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" />--%>
                            <input id="btClear" type="reset" value="Cancel" />
                        </td>
                    </tr>
                </table>
                </form>
                <a href="ManagePermission.aspx">ย้อนกลับ (Back)</a>
                <% } %>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(function () {
            $('#btClear').click(function () {
                window.location = 'ManagePermission.aspx';
            });
        });

    </script>
</asp:Content>
