﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Station_View.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.Station_View" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                Station Detail<br />
            </div>
            <fieldset>
                <% if (ErrorMessage.Length > 0)
                   { %>
                <div id="divWrapper">
                    <div id="divSaveResult">
                        <h1>
                            <%=ErrorMessage%></h1>
                    </div>
                    <br />
                    <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
                </div>
                <% }
                   else
                   { %>
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Code :
                        </td>
                        <td class="field">
                            <label runat="server" id="lblStationCode">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Name TH :
                        </td>
                        <td class="field">
                            <label runat="server" id="lblStationNameTH">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Name EN :
                        </td>
                        <td class="field">
                            <label runat="server" id="lblStationNameEN">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Description :
                        </td>
                        <td class="field">
                            <label runat="server" id="lblStationDescription">
                            </label>
                        </td>
                    </tr>
                </table>
                <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
                <% } %>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
