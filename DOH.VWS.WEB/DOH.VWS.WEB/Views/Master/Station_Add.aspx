﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Station_Add.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.Station_Add" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function ValidateInputForm() {
            return true;
        }

        $(function () {
            $('#btClear').click(function () {
                $('#txtCode,#txtNameTH,#txtNameEN,#txtDescription').val('');
            });


            $('#btClear').click();
        });

    </script>
    <asp:UpdatePanel ID="updatePnl" runat="server">
        <ContentTemplate>
            <div class="title2">
                Add New Station<br />
            </div>
            <fieldset>
                <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
                <% if (SaveResultMessage.Length > 0)
                   { %>
                <div id="divWrapper">
                    <div id="divSaveResult">
                        <h1>
                            <%=SaveResultMessage%></h1>
                    </div>
                </div>
                <% } %>
                <form action="Station_Add.aspx" method="post" onsubmit="return ValidateInputForm()">
                <table class="tbSearch">
                    <tr>
                        <td class="HeadRow">
                            Station Code :
                        </td>
                        <td class="field">
                            <input id="txtCode" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Name TH :
                        </td>
                        <td class="field">
                            <input id="txtNameTH" type="text" runat="server" enableviewstate="false" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Name EN :
                        </td>
                        <td class="field">
                            <input id="txtNameEN" type="text" runat="server" enableviewstate="false" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="HeadRow">
                            Description :
                        </td>
                        <td class="field">
                            <input id="txtDescription" type="text" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            &nbsp;
                        </td>
                        <td class="field">
                            <input id="btSave" type="submit" runat="server" value="Save" onserverclick="btSave_OnClick" />
                            <input id="btClear" type="reset" value="Cancel" />
                        </td>
                    </tr>
                </table>
                </form>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
