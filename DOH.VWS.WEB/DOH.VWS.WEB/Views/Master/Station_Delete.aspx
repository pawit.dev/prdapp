﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Station_Delete.aspx.cs" Inherits="DOH.VWS.WEB.Views.Master.Station_Delete" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" >
    <div id="divWrapper">
        <div id="divDeleteResult">
            <h1><%=DeleteResultMessage%></h1>
        </div>
        <br />
        <a href="StationManagement.aspx">ย้อนกลับ (Back)</a>
    </div>

</asp:Content>
