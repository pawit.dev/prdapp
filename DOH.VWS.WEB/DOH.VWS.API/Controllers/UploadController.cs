﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DMT.WS.API.Controllers
{
    public class UploadController : Controller
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UploadController));
        //
        // GET: /Upload/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(IEnumerable<HttpPostedFileBase> files)
        {
            logger.Debug("::XXXX ");
            bool bResult = false;
            string resp = string.Empty;
            try
            {
                if (files != null)
                {

                    ViewData["response"] = string.Empty;
                    foreach (var file in files)
                    {
                        if (file.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(file.FileName);
                            
                            if (fileName.EndsWith("txt"))
                            {
                                //if (file.FileName.Length != 24)
                                //{
                                //    logger.Error("File " + file.FileName + " length must be equest 24 digit in format yyyyMMdd-HHmmss-xxxx.yyy .");
                                //    resp =  "\t" + "1,Fail" + "\r\n";                                    
                                //}
                                //else
                                //{
                                StreamReader stream = new StreamReader(file.InputStream);
                                string data = stream.ReadToEnd();
                                if (!data.Substring(0, 15).Equals(fileName.Substring(0, 15)))
                                {
                                    logger.Error("File " + file.FileName + " the date data in file content is not match with filename prefix.");
                                    //resp = "\t" + "1,Fail" + "\r\n";
                                }
                                else
                                {

                                }
                                //}
                            }
                            else if (fileName.ToLower().EndsWith("jpg") || fileName.ToLower().EndsWith("png"))
                            {
                                String yyyyMMdd = fileName.Split('-')[0];
                                String station = Path.GetFileNameWithoutExtension(fileName).Split('-')[4];
                                int year = Convert.ToInt16(yyyyMMdd.Substring(0, 4));
                                int month = Convert.ToInt16(yyyyMMdd.Substring(4, 2));
                                int day = Convert.ToInt16(yyyyMMdd.Substring(6, 2));
                                //String station = getStatioin(1,year,fileName);
                                //if (station.Equals(""))
                                //{
                                //    station = getStatioin(2, year, fileName);
                                //}
                                if (!station.Equals(""))
                                {
                                    string[] tmp = fileName.Split('-');
                                    String path = String.Format(ImageFilePath, station, year, month.ToString("00"), day.ToString("00"));
                                    var fullpath = Path.Combine(path, fileName);
                                    logger.Debug("Full path "+fullpath+".");
                                    if (!Directory.Exists(path))
                                    {
                                        Directory.CreateDirectory(path);
                                    }

                                    if (!System.IO.File.Exists(fullpath))
                                    {
                                        logger.Debug(String.Format(" Source {0} Station {1} Year {2} Month {3} Day {4} => Save file Success.", fileName,station,year,month,day));
                                        file.SaveAs(fullpath);
                                        bResult = true;
                                    }
                                    else
                                    {
                                        //resp = "\t" + "1,Duplicate image." + "\r\n";
                                        logger.Error(fileName + "Duplicate image.");
                                    }
                                }
                                else {
                                    logger.Debug(fileName+" not found station.");
                                }
                            }
                        }
                    }

                }
                else
                {

                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                ViewData["response"] = e.Message;
            }

            if (bResult)
            {
                resp += "\t" + "OK" + "\r\n";               
            }
            else
            {
                resp += "\t" + "FAIL" + "\r\n";
            }
            ViewData["response"] = resp;
            return View();
        }

        //public String getStatioin(int station,int year,String imgName)
        //{
        //    String stationId = "";
        //    String conStr = ConfigurationManager.ConnectionStrings["WimConStr"].ConnectionString;
        //    try
        //    {
        //        using (SqlConnection con = new SqlConnection(conStr))
        //        {
        //            SqlCommand cmd = new SqlCommand(String.Format("select StationID from WIMData_{0}_{1}.dbo.VehicleRecord where Image01Name ='" + imgName + "'", station.ToString("00"), year), con);
        //            cmd.CommandType = CommandType.Text;
        //            con.Open();
        //            SqlDataReader dr = cmd.ExecuteReader();
        //            while (dr.Read())
        //            {
        //                if (dr["StationID"] != DBNull.Value)
        //                {
        //                    stationId = Convert.ToInt16(dr["StationID"]).ToString("00");
        //                }
        //            }
        //            dr.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Console.WriteLine(ex.Message);
        //        logger.Debug(ex.Message);
        //        logger.Error(ex.StackTrace);
        //        return "";
        //    }
        //    return stationId;
        //}



        private static String ImageFilePath
        {
            get { return ConfigurationManager.AppSettings["imageFilePath"]; }
        }
    }
}
