<?php
return array(
		'name'=>'MUIC',
		'defaultController'=>'Dashboard',
		'import'=>array(
				'application.models.*',
				'application.components.*'
		),
		'components'=>array(
				'urlManager'=>array(
						'urlFormat'=>'path'
				),
				'db'=>array(
						'class'=>'CDbConnection',
						'connectionString' => 'mysql:host=localhost;dbname=muicmlea_muicv2',

						'emulatePrepare' => true,

						'username' => 'root',
						'password' => 'password',
						'charset' => 'utf8',
				),


				/*
				 UNCOMMENT extension=php_openssl.dll
				'connectionString' => 'mysql:host=localhost;dbname=prdappne_itechservicedb',
				'connectionString' => 'mysql:host=61.19.64.10;dbname=prdappne_itechservicedb',
				'username' => 'prdappne_root',
				'password' => 'password',
				'Smtpmail'=>array(
				'class'=>'application.extensions.smtpmail.PHPMailer',
				'Host'=>"smtp.gmail.com",
				'Username'=>'pawitvaap@gmail.com',
				'Password'=>'xxxxxx',
				'Mailer'=>'smtp',
				'Port'=>465,
				'SMTPAuth'=>true,
				'SMTPSecure'=>'ssl',
				'SMTPDebug'=>1
				),
				*/
					
		),
);
?>