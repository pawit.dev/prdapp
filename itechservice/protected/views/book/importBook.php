﻿<script type="text/javascript">
$(function(){
	$('#users-form').submit(function(){
		return (validateForm() && confirm('Confirm ?'));
		});
});
function validateForm(){
	var isResult = true;

		return isResult;	
}
</script>

<div class="full_w">
	<div class="h_title">Management-Import-Book</div>
	<?php 
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'users-form',
			'enableAjaxValidation' => true,
			'htmlOptions'=>array('enctype' => 'multipart/form-data')
	));
	?>



	<div class="element">
		<label for="name">Import File <b style="color: red">* Allow only *.xls</b>
		</label>
		<?php echo $form->hiddenField(Book::model(), 'book_cover1', array('size' => 50, 'maxlength' => 255)); ?>

		<? $this->widget('ext.EAjaxUpload.EAjaxUpload',
				array(
			        'id'=>'book_cover1',
			        'config'=>array(
			               'action'=>Yii::app()->createUrl('Book/upload'),
			               'allowedExtensions'=>array("jpg","png","xls"),//array("jpg","jpeg","gif","exe","mov" and etc...
			               'sizeLimit'=>5*1024*1024,// maximum file size in bytes
			              // 'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
			              'onComplete'=>"js:function(id, fileName, responseJSON){ $('#Book_book_cover1').val(fileName); }",
			               //'messages'=>array(
			               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
			               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
			               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
			               //                  'emptyError'=>"{file} is empty, please select files again without it.",
			               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
			               //                 ),
			               //'showMessage'=>"js:function(message){ alert(message); }"
			              )
					)); ?>
	</div>

	<div class="entry">
		<!-- 			<button type="submit">Preview</button> -->
		<button type="submit" class="add">Save</button>
		<button type="reset" class="cancel"
			onClick="javascript:history.back();">Cancel</button>
	</div>
	<?php $this->endWidget(); ?>
</div>
