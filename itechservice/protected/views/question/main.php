﻿<div class="full_w">
	<div class="h_title">Management-Question</div>
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'my-model-grid',
			'dataProvider' => $data->search(),
			'htmlOptions' => array('style' => 'width: 700px;'),
			'ajaxUpdate'=>true,
			'columns' => array(
					array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
							'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
					),
					array(
							'name'=>'question',
							'htmlOptions'=>array('width'=>'15%'),
					),
					array(
							'name'=>'status',
							'htmlOptions'=>array('width'=>'5%', 'align'=>'center'),
					),
					array(            // display a column with "view", "update" and "delete" buttons
							'class'=>'CButtonColumn',
							'template'=>'{update} {delete} {answer}',
							'htmlOptions'=>array('width'=>'10%', 'align'=>'center'),
							'buttons'=>array
							(
									'answer' => array
									(
											//'label'=>'Answer',
											'imageUrl'=>Yii::app()->request->baseUrl.'/images/i_contact.png',
											'url'=>'Yii::app()->createUrl("QuestionAnswer/create", array("id"=>$data->id))',
											'visible'=>'!CommonUtil::isAlreadyReadQuestion("$data->id")'
									),
							),
					),
			),
	));
	?>
	<div class="entry">
		<div class="sep"></div>
		<?php echo CHtml::link('Add New',array('question/create'), array('class'=>'button add'));?>
	</div>
</div>

<div class="clear"></div>
