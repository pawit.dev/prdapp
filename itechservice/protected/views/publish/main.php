﻿<div class="full_w">
	<div class="h_title">Management-Puslish-Package</div>
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'my-model-grid',
			'dataProvider' => $data->search(),
			'ajaxUpdate'=>true,
			'htmlOptions'=>array('width'=>'45px', 'align'=>'center'),
			'columns' => array(
					array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
							'htmlOptions'=>array('width'=>'5px', 'align'=>'center'),
					),
					array(
							'name'=>'item',
							'htmlOptions'=>array('width'=>'20%', 'align'=>'center'),
					),
					array(
							'name'=>'image',
							'type'=>'raw',
							'value'=>'CHtml::image(Yii::app()->request->baseUrl."/images/".$data->image,
							"",
							array(\'width\'=>48, \'height\'=>48))',
							'htmlOptions'=>array('width'=>'20%', 'align'=>'center'),
					),
					array(
							'name'=>'version',
							'htmlOptions'=>array('width'=>'20%', 'align'=>'center'),
					),
					array(
							'name'=>'Action',
							'type'=>'raw',
							'value'=>'CHtml::link(\'Publish\',array(\'Publish\GeneratePackage\app_id/\'.$data->id), array(\'class\'=>\'button add\'))',


					),
			),
	));
	?>
</div>

<div class="clear"></div>
