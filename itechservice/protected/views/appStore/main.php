﻿<div class="full_w">
	<div class="h_title">Management-Store</div>
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'my-model-grid',
			'dataProvider' => $data->search(),
			'htmlOptions' => array('style' => 'width: 600px;'),
			'ajaxUpdate'=>true,
			'columns' => array(
					array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
							'htmlOptions'=>array('width'=>'5', 'align'=>'center'),
					),
					array(
							'name'=>'name',
							'htmlOptions'=>array('width'=>'20'),
					),
					array(
							'name'=>'bundle',
							'htmlOptions'=>array('width'=>'150'),
					),
					array(
							'name'=>'platform',
							'htmlOptions'=>array('width'=>'15', 'align'=>'center'),
					),
					array(
							'name'=>'version',
							'htmlOptions'=>array('width'=>'15', 'align'=>'center'),
					),
										array(
							'name'=>'questionnaire_url',
							'htmlOptions'=>array('width'=>'5', 'align'=>'center'),
					),
					array(            // display a column with "view", "update" and "delete" buttons
							'class'=>'CButtonColumn',
							'template'=>'{view} {update} {delete}',
							'htmlOptions'=>array('width'=>'20', 'align'=>'center'),
							'buttons'=>array
							(
							),
					),
			),
	));
	?>
	<div class="entry">
		<div class="sep"></div>
		<?php echo CHtml::link('Add New',array('appStore/create'), array('class'=>'button add'));?>
	</div>
</div>

<div class="clear"></div>
