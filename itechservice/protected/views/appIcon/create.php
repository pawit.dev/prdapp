﻿
<script
	type="text/javascript"
	src="<?php echo Yii::app()->request->baseUrl;?>/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(function(){
	$('#users-form').submit(function(){
		return (validateForm() && confirm('Confirm ?'));
		});
});
function validateForm(){
	var isResult = true;
	var txtbook_name = $('#book_name').val();

	var txtRecommented  =$('#txtRecommented').val();
	var active = $('#active :selected').val();
	var type = $('#type :selected').val();
	var flag = $('#flag :selected').val(););
	if(txtbook_name==""){
				$("#book_name").focus();
				$('#txtbook_name').html('<b style="color:red">*Book Name invalid</b>');
				isResult = false;
				return false;
	}else{
				$('#txtbook_name').html('');
	}
	if(txtRecommented==""){
		$("#txtRecommented").focus();
		$('#txtRecommented').html('<b style="color:red">*Recommented invalid</b>');
		isResult = false;
		return false;
		}else{
				$('#txtRecommented').html('');
		}	
	if(flag==0){
		$('#txtflag').html('<b style="color:red">*New Arrival invalid</b>');
		isResult = false;
		return false;
	}else{
			$('#txtflag').html('');
	}
	if(type==0){
		$('#type').html('<b style="color:red">*type invalid</b>');
		isResult = false;
		return false;
	}else{
			$('#txttype').html('');
	}
	if(active==0){
		$('#txtactive').html('<b style="color:red">*Status invalid</b>');
		isResult = false;
		return false;
	}else{
			$('#txtactive').html('');
	}
	
	if(isResult == false){
				alert("Please correct data.");
			
			}
		return isResult;	
}
</script>

<div class="full_w">
	<div class="h_title">Management-Create-Icon</div>
	<?php 
	$form = $this->beginWidget('CActiveForm', array(
			'id' => 'users-form',
			'enableAjaxValidation' => true,
			'htmlOptions'=>array('enctype' => 'multipart/form-data')
	));
	?>


	<div class="element">
		<label for="name">Icon (Size 40*40) <b style="color: red">*Allow
				Only *.jpg,*.png</b>
		</label>
		<?php echo $form->hiddenField(AppIcon::model(), 'icon_path', array('size' => 50, 'maxlength' => 255)); ?>

		<? $this->widget('ext.EAjaxUpload.EAjaxUpload',
				array(
			        'id'=>'icon_path',
			        'config'=>array(
			               'action'=>Yii::app()->createUrl('AppIcon/upload'),
			               'allowedExtensions'=>array("jpg","png"),//array("jpg","jpeg","gif","exe","mov" and etc...
			               'sizeLimit'=>5*1024*1024,// maximum file size in bytes
			              // 'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
			              'onComplete'=>"js:function(id, fileName, responseJSON){ $('#AppIcon_icon_path').val(fileName); }",
			               //'messages'=>array(
			               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
			               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
			               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
			               //                  'emptyError'=>"{file} is empty, please select files again without it.",
			               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
			               //                 ),
			               //'showMessage'=>"js:function(message){ alert(message); }"
			              )
					)); ?>
	</div>


	<div class="entry">
		<!-- 			<button type="submit">Preview</button> -->
		<button type="submit" class="add">Save</button>
		<button type="reset" class="cancel"
			onClick="javascript:history.back();">Cancel</button>
	</div>
	<?php $this->endWidget(); ?>
</div>
