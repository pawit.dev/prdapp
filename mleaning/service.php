<?php
function connectdB() {

	mysql_connect('localhost', 'muicmlea_db','0luYXT7i');
	mysql_select_db('muicmlea_db');
}

function closedB() {

	// 	mysql_close($dbhandle);

}

////////////////////////////////////////////////////////////////
//Function to sanitize input if its in the correct data format
///////////////////////////////////////////////////////////////


function sanitize($ID){

	//remove spaces from the text input

	$ID=trim($ID);

	//convert special characters to html entities
	//to prevent possibilty of XSS attacks

	$ID=htmlspecialchars($ID);

	//sanitize inputs using mysql_real_escape_string

	$sanitized = mysql_real_escape_string(stripslashes($ID));

	return $ID;

}

/////////////////////////////////////////////////////////////
//Function to validate fresh inputs from the public user
//////////////////////////////////////////////////////////////

function validateinputs($ID) {

	//Validate input from GET

	if ((preg_match('/^[-a-z.-@,\'\s]*$/i',$ID)) && ((strlen($ID))<>0))
	{
		return TRUE;
	}
	else
	{
		return FALSE;

		//ERROR HANDLING
		$errors="INPUT validation from GET fails.";
		logerrors($errors);
	}
}

////////////////////////////////////////////////////////////
//Function to log errors in the application
////////////////////////////////////////////////////////////

function logerrors($errors){
	echo ''.$errors;
	// 	$file = "/mleaning/logs/errorlog.txt";
	// 	$fh = fopen($file, 'a') or die("can't open file");
	// 	$newline="\r\n";
	// 	fwrite($fh,$errors.$newline);
	// 	fclose($fh);
}

///////////////////////////////////////////////////////////
//Function to stream the video
///////////////////////////////////////////////////////////

function streamthisvideonow($ID) {

	//retrieve real video path from MySQL database
	$sql = "select media_realurl from tb_media where media_key='".$ID."';" ;

	$result = mysql_query($sql);


	If ($result==true)
	{

		while($item = mysql_fetch_assoc($result)){
			$direction = $item['media_realurl'];
		}
		//change the path to use absolute server path, revised on February 4, 2011
		$path=$direction;
		header('Content-type: video/mpeg');

		//2015236 is the exact file size of the video, change it to according to your video file size
		header('Content-Length: '.filesize($path));
		header("Expires: -1");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);

		//finally stream the video content
		readfile($path);

	} else {

		//ERROR HANDLING
		$errors= "SELECT statement error:";
		logerrors($errors);

	}
}

///////////////////////////////////
//Use and run the functions
//////////////////////////////////

//Let's retrieve the inputs from get

$ID= $_GET['ID'];

//Validate this input

If (validateinputs($ID)) {
	//Input validation is true

	//Connect to database first

	connectdB();

	//Sanitize inputs

	$ID= sanitize($ID);

	//Stream the video

	streamthisvideonow($ID);

	//Close Database
	closedB();
}
?>