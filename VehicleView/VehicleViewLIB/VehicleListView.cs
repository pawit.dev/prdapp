﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DBChangeDetection;
using System.IO;
using System.Net;

namespace VehicleViewLIB
{
    public delegate void VehicleListViewSelectionChanged (ViewType viewtype, string refID);

    public partial class VehicleListView : UserControl
    {
        #region INITIALIZE

        class ViewItem
        {
            public Image Image1 { get; set; }
            public Image Image2 { get; set; }
            public string Text1 { get; set; }
            public string Text2 { get; set; }
            public string Text3 { get; set; }
            public Color StatusColor { get; set; }
            public string RefID { get; set; }
        }

        public event VehicleListViewSelectionChanged OnSelectionChanged;
        public bool LiveView { get; set; }
        public bool Show2ndImage = false;

        string sqlText = string.Empty;
        string sqlText_listener = string.Empty;
        ViewType viewType;

        string logSource = "ViewItem";
        int ID = 0;
        int WarningFromStationID = 0;

        List<ViewItem> listViewItems = new List<ViewItem>();
        
        readonly int padding = 5;
        readonly int rowHeight = 110;
        readonly int rowDividerHeight = 5;

        readonly int col0_width = 12; //statusBand
        readonly int col1_width = 135; // image1
        readonly int col2_width = 135; // image2
        int col3_width = 75; // text1
        int col4_width = 135; // text2
        int col5_width = 105; // text3

        Color normalBackColor = Color.DarkGray;
        Color hoverBackColor = Color.LightGray;

        public VehicleListView()
        {
            InitializeComponent();
            InitDataGridView();
            AddColumns();
            LiveView = true;          
        }

        public void Initialize(int id, string sql1, string sql2, ViewType type)
        {
            this.ID = id;
            this.sqlText = sql1;
            this.sqlText_listener = sql2;
            this.viewType = type;
            RefreshListView();
            ListenToDBChange();
        }

        #endregion INITIALIZE

        #region DATAGRIDVIEW

        void InitDataGridView()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.DimGray;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
                       
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(5);
            
            dataGridViewCellStyle1.BackColor = normalBackColor;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
                   
            dataGridViewCellStyle1.SelectionBackColor = hoverBackColor;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;

            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.GridColor = System.Drawing.Color.DimGray;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowCellErrors = false;
            this.dataGridView1.ShowCellToolTips = false;
            this.dataGridView1.ShowEditingIcon = false;
        }

        void AddColumns()
        {
            DataGridViewImageColumn c0 = new DataGridViewImageColumn();
            c0.Width = col0_width;
            c0.DefaultCellStyle.Padding = new Padding(3, padding, 3, padding);

            DataGridViewImageColumn c1 = new DataGridViewImageColumn();
            c1.Width = col1_width;
            c1.DefaultCellStyle.Padding = new Padding(padding, padding, padding, padding);
            c1.ImageLayout = DataGridViewImageCellLayout.Stretch;
            c1.DefaultCellStyle.NullValue = null;

            DataGridViewImageColumn c2 = new DataGridViewImageColumn();
            c2.Width = col2_width;
            c2.DefaultCellStyle.Padding = new Padding(padding, padding, padding, padding);
            c2.ImageLayout = DataGridViewImageCellLayout.Stretch;
            c2.DefaultCellStyle.NullValue = null;

            DataGridViewTextBoxColumn c3 = new DataGridViewTextBoxColumn();
            c3.Width = col3_width;
            c3.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            DataGridViewTextBoxColumn c4 = new DataGridViewTextBoxColumn();
            c4.Width = col4_width;
            c4.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            DataGridViewTextBoxColumn c5 = new DataGridViewTextBoxColumn();
            c5.Width = col5_width;
            c5.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            

            this.dataGridView1.Columns.Add(c0);
            this.dataGridView1.Columns.Add(c1);
            this.dataGridView1.Columns.Add(c2);
            this.dataGridView1.Columns.Add(c3);
            this.dataGridView1.Columns.Add(c4);
            this.dataGridView1.Columns.Add(c5);

            this.dataGridView1.Columns[2].Visible = Show2ndImage;
            this.dataGridView1.SelectionChanged += new EventHandler(dataGridView1_SelectionChanged);
        
        }

        public void ClearRowSelection()
        {
            this.dataGridView1.ClearSelection();
        }

        public void ScrollDown()
        {
            this.dataGridView1.FirstDisplayedScrollingRowIndex++;
        }

        public void ScrollUp()
        {
            this.dataGridView1.FirstDisplayedScrollingRowIndex--;
        }

        void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows.Count == 0)
            {
                return;
            }
            string tag = (string)this.dataGridView1.SelectedRows[0].Tag;
            if (tag == null)
            {
                return;
            }
            if (tag.Contains(","))
            {
                string[] tags = tag.Split(new char[] { ',' });
                string type = tags[0];
                string refID = tags[1];

                ViewType viewtype = (ViewType)Enum.Parse(typeof(ViewType), type, true);

                if (OnSelectionChanged != null)
                {
                    OnSelectionChanged(viewtype, refID);
                }
            }          
        }

        #endregion DATAGRIDVIEW

        #region DBLISTENING

        DBChangedDetector dbDetector = new DBChangedDetector();

        void ListenToDBChange()
        {           
            dbDetector.ConnectionString = AppParameters.ConnectionString;
            dbDetector.SQLText = this.sqlText_listener;
            dbDetector.ID = this.ID.ToString();
            
            dbDetector.OnDBChangedDetected -= new DBChangedDetected(dbDetector_OnDBChangedDetected);
            dbDetector.OnDBChangedDetected += new DBChangedDetected(dbDetector_OnDBChangedDetected);
            dbDetector.OnError += new DBChangedError(dbDetector_OnInitializedError);
            dbDetector.StartListening();
            GC.Collect();
        }

        void dbDetector_OnInitializedError(object sender, string message)
        {
            Messaging.LogAndShow("VehicleListView", "Error in module: ListentoDBChange " + "\r\n" + message + "\r\n");
        }

        public void Close()
        {
            if (dbDetector != null)
            {
                dbDetector.StopListening();
            }
        }

        delegate void actionDelegate(object sender, System.Data.DataTable dataTable, string info);

        void dbDetector_OnDBChangedDetected(object sender, System.Data.DataTable dataTable, string info)
        {
            if (!LiveView)
            {
                return;
            }

            DBChangedDetector detector = (DBChangedDetector)sender;
            detector.OnDBChangedDetected -= dbDetector_OnDBChangedDetected;

            ISynchronizeInvoke i = (ISynchronizeInvoke)this;
            if (i.InvokeRequired)
            {
                object[] args = { sender, dataTable, info };
                actionDelegate actiond = new actionDelegate(DoDBChangedActions);
                i.BeginInvoke(actiond, args);
            }
            else
            {
                DoDBChangedActions(sender, dataTable, info);
            }

            detector.OnDBChangedDetected += new DBChangedDetected(dbDetector_OnDBChangedDetected);
        }

        private void DoDBChangedActions(object sender, System.Data.DataTable dataTable, string info)
        {
            Timer timer1 = new Timer();            
            timer1.Interval = AppParameters.ImageDelay;
            timer1.Tick += new EventHandler(timer1_Tick);          
            timer1.Start();            
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            Timer timer1 = (Timer)sender;
            timer1.Stop();
            RefreshListView();

            if (viewType == ViewType.WARNING_Avoiding || viewType == ViewType.WARNING_RunningScale ||
                    viewType == ViewType.WARNING_OverWeight || viewType == ViewType.WARNING_BGOpened)
            {
                WarningSignal.TurnONWarningSignal(viewType, AppParameters.WarningSignalDuration, WarningFromStationID);
            }

            GC.Collect();
        }

        #endregion DBLISTENING

        #region REFRESH_LIST_VIEW

        public void RefreshListView()
        {
            try
            {
                DateTime dt1 = DateTime.Now;
               
                DataTable table = Utils.GetDataTable(sqlText);
                this.dataGridView1.Rows.Clear();

                DateTime dt2 = DateTime.Now;

                if (viewType == ViewType.WIM)
                {
                    listViewItems = Create_List_Items_WIM(table);
                }
                else if (viewType == ViewType.STATIC)
                {
                    listViewItems = Create_List_Items_STATIC(table);
                }
                else if (viewType == ViewType.WARNING_Avoiding || viewType == ViewType.WARNING_RunningScale ||
                    viewType == ViewType.WARNING_OverWeight || viewType == ViewType.WARNING_BGOpened)
                {
                    Show2ndImage = false;
                    this.col3_width = 88;
                    this.col4_width = 0;
                    this.col5_width = 0;

                    listViewItems = Create_List_Items_Warning(table);
                    if (table.Rows.Count > 0)
                    {
                        WarningFromStationID = (int)table.Rows[0]["StationID"];
                    }
                }
                else
                {
                    return;
                }

                DateTime dt3 = DateTime.Now;

                this.dataGridView1.Columns[2].Visible = Show2ndImage;
                this.dataGridView1.Columns[3].Width = col3_width;
                this.dataGridView1.Columns[4].Width = col4_width;
                this.dataGridView1.Columns[5].Width = col5_width;

                this.dataGridView1.Width = this.Width + 50;


                if (listViewItems == null)
                {
                    return;
                }

                foreach (ViewItem item in listViewItems)
                {
                    int i = this.dataGridView1.Rows.Add();
                    DataGridViewRow row = this.dataGridView1.Rows[i];
                    row.Height = rowHeight;
                    row.DividerHeight = rowDividerHeight;

                    row.Cells[0].Value = CreateBitmap(item.StatusColor);

                    row.Cells[1].Value = item.Image1;

                    if (this.Show2ndImage == false)
                    {
                        if (item.Image1 == null)
                        {
                            row.Cells[1].Value = item.Image2;
                        }
                    }
                    
                    row.Cells[2].Value = item.Image2;
                    row.Cells[3].Value = item.Text1;
                    row.Cells[4].Value = item.Text2;
                    row.Cells[5].Value = item.Text3;

                    row.Tag = item.RefID;
                }

                if (dataGridView1.Rows.Count > 0)
                {
                    // To trigger the first-row-SelectionChanged event
                    this.dataGridView1.Rows[0].Selected = false;
                    this.dataGridView1.Rows[0].Selected = true;
                }

                string t1 = (dt2 - dt1).TotalMilliseconds.ToString();
                string t2 = (dt3 - dt2).TotalMilliseconds.ToString();
               // MessageBox.Show("T1=" + t1 + "   T2=" + t2);

            }
            catch (Exception ex)
            {
                Messaging.LogOnly(logSource, "Exception_RefreshListView: " + "\r\n" + ex.Message);
            }
            GC.Collect();

            

        }

        Bitmap CreateBitmap(Color color)
        {
            Bitmap bmp = new Bitmap(col0_width, rowHeight);

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    bmp.SetPixel(i, j, color);
                }
            }
            return bmp;
        }

        #endregion REFRESH_LIST_VIEW

        #region CREATE_LIST_ITEMS_WIM

        List<VehicleListView.ViewItem> Create_List_Items_WIM(DataTable table)
        {
            List<VehicleListView.ViewItem> items = new List<VehicleListView.ViewItem>();
            try
            {
                foreach (DataRow row in table.Rows)
                {
                    VehicleListView.ViewItem item = new VehicleListView.ViewItem();

                    item.RefID = ViewType.WIM.ToString() +  "," + ((long)row["WIMID"]).ToString();

                    string imgName = (string)row["Image01Name"];

                    // OPTION #1 Image from mapped-drive

                    //string fullPath = Utils.GetImageFolder(imgName) + @"\" + imgName;

                    //if (File.Exists(fullPath))
                    //{
                    //    item.Image1 = Bitmap.FromFile(fullPath);
                    //}

                    // OPTION #2 Image from http://

                    string imgUrl = Utils.GetImageURL(imgName);
                    try
                    {
                        var webClient1 = new WebClient();
                        webClient1.Proxy = null;
                        byte[] bytes = webClient1.DownloadData(imgUrl);
                        MemoryStream ms = new MemoryStream(bytes);
                        item.Image1 = Image.FromStream(ms);
                    }
                    catch { }

                    string text1 = string.Empty;
                    text1 += ("") + (string)row["StationName"] + "\r\n";
                    text1 += ("(") + ((int)row["VehicleNumber"]).ToString() + ")\r\n";


                    text1 += ("Lane: ") + ((byte)row["Lane"]+1).ToString() + "\r\n";
                    text1 += ("Class: ") + ((byte)row["VehicleClass"]).ToString() + "\r\n";


                    string text2 = string.Empty;
                    text2 += ("") + ((DateTime)row["TimeStamp"]).ToString("dd-MM-yyyy HH:mm:ss") + "\r\n";
                    text2 += ("Sort: ") + ((VRSortDecisionCode)((byte)row["SortDecision"])).ToString() + "\r\n";                   
                    text2 += ("Max: ") + ((int)row["MAxGVW"]).ToString("N0") + " Kg" + "\r\n";
                    text2 += ("GVW: ") + ((int)row["GVW"]).ToString("N0") + " Kg" + "\r\n";

                    string text3 = string.Empty;
                    text3 += row["LicensePlateNumber"].ToString() + "\r\n";
                    text3 += row["ProvinceName"].ToString() + "\r\n\r\n";

                    VRStatusCode statusCode = (VRStatusCode)((int)row["StatusCode"]);
                    VRErrorCode errorCode = (VRErrorCode)((byte)row["Error"]);
                    VRSortDecisionCode decisionCode = (VRSortDecisionCode)((byte)row["SortDecision"]);

                    text3 += (statusCode == VRStatusCode.StatusClear) ? "" : Utils.GetStatusCodeText(statusCode) + "\r\n";
                    text3 += (errorCode == VRErrorCode.None) ? "" : errorCode.ToString() + "\r\n";

                    item.Text1 = text1;
                    item.Text2 = text2;
                    item.Text3 = text3;
                    item.StatusColor = Utils.GetSortDecisionColorCode(decisionCode);

                    items.Add(item);

                }
            }
            catch (Exception ex)
            {
                Messaging.LogOnly(logSource, "Exception_CreateListItemWIM: " + "\r\n" + ex.Message);
            }
            return items;
        }
        #endregion CREATE_LIST_ITEMS_WIM

        #region CREATE_LIST_ITEMS_STATIC

        List<VehicleListView.ViewItem> Create_List_Items_STATIC(DataTable table)
        {
            List<VehicleListView.ViewItem> items = new List<VehicleListView.ViewItem>();
            try
            {
                foreach (DataRow row in table.Rows)
                {
                    VehicleListView.ViewItem item = new VehicleListView.ViewItem();

                    item.RefID = ViewType.STATIC.ToString() + "," + ((long)row["ENFID"]).ToString();

                    string imgName1 = (string)row["LPR_ImageName"];
                    string fullPath1 = Utils.GetImageFolder(imgName1) + @"\" + imgName1;
                    if (File.Exists(fullPath1))
                    {
                        item.Image1 = Bitmap.FromFile(fullPath1);
                    }

                    //string imgUrl1 = Utils.GetImageURL(imgName1);
                    //try
                    //{
                    //    var webClient1 = new WebClient();
                    //    byte[] bytes = webClient1.DownloadData(imgUrl1);
                    //    MemoryStream ms = new MemoryStream(bytes);
                    //    item.Image1 = Image.FromStream(ms);
                    //}
                    //catch { }

                    string imgName2 = (string)row["VehicleImageName"];
                    string fullPath2 = Utils.GetImageFolder(imgName2) + @"\" + imgName2;
                    if (File.Exists(fullPath2))
                    {
                        item.Image2 = Bitmap.FromFile(fullPath2);
                    }

                    //string imgUrl2 = Utils.GetImageURL(imgName2);
                    //try
                    //{
                    //    var webClient2 = new WebClient();
                    //    byte[] bytes = webClient2.DownloadData(imgUrl2);
                    //    MemoryStream ms = new MemoryStream(bytes);
                    //    item.Image2 = Image.FromStream(ms);
                    //}
                    //catch { }

                    string text1 = string.Empty;
                    text1 += (string)row["StationName"] + "\r\n";
                    text1 += "(" + ((int)row["SeqNumber"]).ToString() + ")\r\n";
                    text1 += "static\r\n";
                    text1 += "Class: " + ((int)row["VehicleClassID"]).ToString() + "\r\n";

                    string text2 = string.Empty;
                    text2 += ((DateTime)row["TimeStamp"]).ToString("dd-MM-yyyy HH:mm:ss") + "\r\n";
                    text2 += ("") + (string)row["LPR_Number"] + "  " + (string)row["ProvinceName"] + "\r\n";
                    text2 += ("Max: ") + ((int)row["GVW_Weight_Max"]).ToString("N0") + " Kg" + "\r\n";
                   // text2 += ("WIM: ") + ((int)row["GVW_Weight_WIM"]).ToString("N0") + " Kg" + "\r\n";
                    text2 += ("GVW: ") + ((int)row["GVW_Weight_Measured"]).ToString("N0") + " Kg" + "\r\n";


                    string text3 = string.Empty;
                    item.StatusColor = Utils.GetIsOverWeightColorCode((string)row["IsOverWeight"]);
                    string status = (string)row["IsOverWeight"];
                    if (status == "Y")
                    {
                        text3 += "OverGVW";                       
                    }
                    else if (status == "N")
                    {
                        text3 += "PASS";
                    }

                    item.Text1 = text1;
                    item.Text2 = text2;
                    item.Text3 = text3;

                    items.Add(item);

                }
            }
            catch (Exception ex)
            {
                Messaging.LogOnly(logSource, "Exception_CreateListItemStatic: " + "\r\n" + ex.Message);
            }
            return items;
        }

        #endregion CREATE_LIST_ITEMS_STATIC

        #region CREATE_LIST_ITEMS_WARNING

        List<VehicleListView.ViewItem> Create_List_Items_Warning(DataTable table)
        {
            List<VehicleListView.ViewItem> items = new List<VehicleListView.ViewItem>();
            try
            {
                foreach (DataRow row in table.Rows)
                {
                    VehicleListView.ViewItem item = new VehicleListView.ViewItem();

                    item.RefID = viewType.ToString() + "," + ((long)row["REFID"]).ToString();

                    //string imgName1 = (string)row["ImageName"];

                    //string imgUrl1 = Utils.GetImageURL(imgName1);
                    //try
                    //{
                    //    var webClient1 = new WebClient();
                    //    byte[] bytes = webClient1.DownloadData(imgUrl1);
                    //    MemoryStream ms = new MemoryStream(bytes);
                    //    item.Image1 = Image.FromStream(ms);
                       
                    //}
                    //catch
                    //{
                    //    string imgName2 = (string)row["ImageName2"];
                    //    string imgUrl2 = Utils.GetImageURL(imgName2);

                    //    var webClient2 = new WebClient();
                    //    byte[] bytes = webClient2.DownloadData(imgUrl2);
                    //    MemoryStream ms = new MemoryStream(bytes);
                    //    item.Image1 = Image.FromStream(ms);

                    //}

                    string imgName1 = (string)row["ImageName"];
                    string fullPath1 = Utils.GetImageFolder(imgName1) + @"\" + imgName1;
                    if (File.Exists(fullPath1))
                    {
                        item.Image1 = Bitmap.FromFile(fullPath1);
                    }
                    else
                    {
                        string imgName2 = (string)row["ImageName2"];

                        string fullPath2 = Utils.GetImageFolder(imgName2) + @"\" + imgName2;
                        if (File.Exists(fullPath2))
                        {
                            item.Image1 = Bitmap.FromFile(fullPath2);
                        }
                    }

                    string text1 = string.Empty;
                    text1 += ("") + (string)row["StationName"] + "\r\n";
                    text1 += ("(") + ((int)row["RefNumber"]).ToString() + ")\r\n";
                    int type = (int)row["Type"];
                    text1 += ("Type: ") + type.ToString() + "\r\n";
                    text1 += ((DateTime)row["TimeStamp"]).ToString("dd-MM-yyyy") + "\r\n";
                    text1 += ((DateTime)row["TimeStamp"]).ToString("HH:mm:ss") ;

                    item.StatusColor = Utils.GetWarningTypeColorCode(type);                   
                    item.Text1 = text1;
                  
                    items.Add(item);

                }
            }
            catch (Exception ex)
            {
                Messaging.LogOnly(logSource, "Exception_CreateListItemWarning: " + "\r\n" + ex.Message);
            }
            return items;
        }

        #endregion CREATE_LIST_ITEMS_WARNING
    }
}
