﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VehicleViewLIB
{
    public partial class Clock : UserControl
    {
        Timer timer = new Timer();
        public Clock()
        {
            InitializeComponent();
            timer.Interval = 1000;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            this.label1.Text = DateTime.Now.ToString("dd-MM-yyyy  HH:mm:ss");
        }
    }
}
