﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace VehicleViewLIB
{
    public class WDCUpdateControls
    {
        private delegate void SetControlPropertyThreadSafeDelegate(Control control, string propertyName, object propertyValue);
        private static void SetControlPropertyThreadSafe(Control control, string propertyName, object propertyValue)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new SetControlPropertyThreadSafeDelegate(SetControlPropertyThreadSafe), new object[] { control, propertyName, propertyValue });
            }
            else
            {
                control.GetType().InvokeMember(propertyName, BindingFlags.SetProperty, null, control, new object[] { propertyValue });
            }
        }

        public static void Update(Control control, string propertyName, object propertyValue)
        {
            try
            {
                SetControlPropertyThreadSafe(control, propertyName, propertyValue);
            }
            catch (Exception ex)
            {
                Messaging.LogAndShow("WDCControls", "ERROR_UpdateControl" + "\r\n\r\n" + ex.Message + "\r\n" + control.GetType().ToString() + "\t" + propertyName + "\t" + propertyValue.ToString());
            }
        }

    }
}
