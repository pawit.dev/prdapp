﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO.Ports;

namespace VehicleViewLIB
{
    class WarningSignal
    {
        static string logSource = "WarningSignal";

        public static void TurnONWarningSignal(ViewType viewType, int msec, int stationID)
        {
            object[] args = new object[] { viewType, msec, stationID };
            BackgroundWorker bw1 = new BackgroundWorker();
            bw1.DoWork += new DoWorkEventHandler(bw1_DoWork);
            bw1.RunWorkerAsync(args);
        }

        static void bw1_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] args = (object[])e.Argument;
            ViewType viewType = (ViewType)args[0];
            int msec = (int)args[1];
            int stationID = (int)args[2];
            DoTurnONWarningSignal(viewType, msec, stationID);
        }

        static void DoTurnONWarningSignal(ViewType viewType, int msec, int stationID)
        {
            try
            {
                string turnON = "ON_";
                string turnOFF = "OFF_";
                string keyword = string.Empty;
                bool enabled = false;

                if (viewType == ViewType.WARNING_Avoiding)
                {
                    keyword = "AVOIDING";
                    enabled = AppParameters.WarningSignalType1Enable;
                }
                else if (viewType == ViewType.WARNING_RunningScale)
                {
                    keyword = "RUNNINGSCALE";
                    enabled = AppParameters.WarningSignalType2Enable;
                }
                else if (viewType == ViewType.WARNING_OverWeight)
                {
                    keyword = "OVERWEIGHT";
                    enabled = AppParameters.WarningSignalType3Enable;
                }
                else if (viewType == ViewType.WARNING_BGOpened)
                {
                    keyword = "BGOPENED";
                    enabled = AppParameters.WarningSignalType4Enable;
                }

                if (enabled)
                {
                    // Play Sound File
                    try
                    {
                        string soundFilePath = AppParameters.SoundFilePath;
                        System.Media.SoundPlayer player = new System.Media.SoundPlayer(soundFilePath);
                        player.Play();
                    }
                    catch
                    { }

                    // TurnON
                    string code = "[" + stationID.ToString() + turnON + keyword + "]";

                    Messaging.LogOnly(logSource, "Sent: " + code);

                    SerialPort sp = new SerialPort();
                    sp.PortName = AppParameters.WarningSignalPort;
                    sp.BaudRate = 9600;
                    sp.DataBits = 8;
                    sp.StopBits = StopBits.One;
                    sp.Parity = Parity.None;

                    if (!sp.IsOpen)
                    {
                        sp.Open();
                    }
                    sp.WriteLine(code);

                    System.Threading.Thread.Sleep(msec);

                    // TurnOFF                    
                    code = "[" + stationID.ToString() + turnOFF + keyword + "]";

                    Messaging.LogOnly(logSource, "Sent: " + code);

                    sp.WriteLine(code);

                    sp.Close();
                }

            }
            catch (Exception ex)
            {              
                Messaging.LogOnly(logSource, "Exception_TurnONWarningSignal" + 
                    "\t" + viewType.ToString()  +
                    "\t" + msec.ToString() + " msec" + 
                    "\r\n" + ex.Message);
            }        
        }
    }
}
