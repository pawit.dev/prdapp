﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VehicleViewLIB
{
    public enum ViewType
    {
        WIM, STATIC, WARNING, WARNING_Avoiding, WARNING_RunningScale, WARNING_OverWeight, WARNING_BGOpened
    }

    public enum VRErrorCode
    {
        None = 0,
        VehicleBelowMinumumSpeed = 1,
        AxleQueueOverFlow = 2,
        SampleQueueOverFlow = 3,
        UpstreamLoopOnly = 4,
        VehicleTooFast = 5,
        UnequalAxleCount = 6,
        DownstreamLoopOnly = 7,
        UpstreamLoopBounce = 8,
        NumberOfAxlesExceedMaxAllowed = 9,
        ZeroAxlesDetected = 10,
        OneAxleDetected = 11,
        AxleOnSensorTooLong = 12,
        AxleSensorActivationInWrongOrder = 13,
        LoopSensorActivationOutOfOrder = 14,
        InvalidVehicle = 15,
        ParkingLotVehicle = 16,
        LoopsInWrongOrder = 17,
        UnmatchedAviTagVehicle = 18
    }

    public enum VRStatusCode
    {
        StatusClear = 0x00000000,
        OffScalesensoractivated = 0x00000001,
        Overheight = 0x00000002,
        OnScalesensornotactivated = 0x00000004,
        SpeedChange = 0x00000008,
        WeightDiff = 0x00000010,
        PartialAxle = 0x00000020,
        UneqlAxlCount = 0x00000040,
        Tailgating = 0x00000080,
        WrongLane = 0x00000100,
        RunningScale = 0x00000200,
        NotInWIMLane = 0x00000400,
        Overlength = 0x00000800,
        Overweight = 0x00001000,
        OverGVW = 0x00002000,
        Random = 0x00004000,
        OverSpeedLimit = 0x00008000,
        VehicleLate = 0x00010000,
        UnexpectedVehicle = 0x00020000,
        VehiclePastDue = 0x00040000,
        NoMatch = 0x00080000,
        BadLateralPosition = 0x00100000,
        NoCompliance = 0x00200000,
        OverrideFailed = 0x00400000,
        CredentialFailed = 0x00800000
    }

    public enum VRRecordTypeCode
    {
        NormalRecord_NoWeightSensor = 10,
        NormalRecord_OneWeightSensor = 11,
        NormalRecord_TwoWeightSensors = 12,
        CalibrationRecord_NoWeightSensor = 30,
        CalibrationRecord_OneWeightSensor = 31,
        CalibrationRecord_TwoWeightSensors = 32,
        DiagnosticRecord_NoWeightSensor = 40,
        DiagnosticRecord_OneWeightSensor = 41,
        DiagnosticRecord_TwoWeightSensors = 42
    }

    public enum VRSortDecisionCode
    {
        Off = 0,
        Sorting = 1,
        Report = 2,
        Bypass = 3,
        CredentialReport = 4
    }
}
