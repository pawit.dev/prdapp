﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using System.Diagnostics;

namespace VehicleViewLIB
{
    public class Messaging
    {      
        private static void WriteLine(string text)
        {
            if (Environment.UserInteractive)
            {
                Console.WriteLine(text);
            }
        }

        public static void Show(string source, string message)
        {
            string m = DateTime.Now.ToString("s").Replace("T"," ") + " [" + source + "] > " + message;
            WriteLine(m);
            SendOutMessage(m);
        }

        public static void LogAndShow(string source, string message)
        {
            string m = DateTime.Now.ToString("s").Replace("T", " ") + " [" + source + "] > " + message;
            WriteLine(m);
            AppendTextFile(source + ".txt", m);
            SendOutMessage(m);
        }

        public static void LogOnly(string source, string message)
        {
            string m = DateTime.Now.ToString("s").Replace("T", " ") + " [" + source + "] > " + message;    
            AppendTextFile(source + ".txt", m);
        }

        public static void LogEvent(string source, string message)
        {
            string m = DateTime.Now.ToString("s").Replace("T", " ") + " [" + source + "] > " + message;    
            AppendTextFile("Event.txt", m);   
        }

        public static void LogProg(string source, string message)
        {
            try
            {
                string m = DateTime.Now.ToString("s").Replace("T", " ") + " [" + source + "] > " + message;
                WriteLine(m);
                string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                using (StreamWriter sw = File.AppendText(path + @"\ProgramLog.txt"))
                {
                    sw.WriteLine(m);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static object locker = new object();
        private static void AppendTextFile(string logFileName, string text)
        {
            lock (locker)
            {
                try
                {
                    string subFolder = string.Empty;
                    subFolder += @"\" + AppParameters.StationID.PadLeft(2, '0');
                    subFolder += @"\" + DateTime.Now.Year.ToString() + @"\" +
                                               DateTime.Now.Month.ToString("d2") + @"\" +
                                               DateTime.Now.Day.ToString("d2");
                    subFolder += @"\" + AppParameters.APPFolderName;

                    string logFilePath = AppParameters.LogLocation + @"\LOGS" + subFolder;
                    string logFileFullPath = logFilePath + @"\" + logFileName;

                    if (File.Exists(logFileFullPath))
                    {
                        FileInfo finfo = new FileInfo(logFileFullPath);
                        if (finfo.Length > (Convert.ToInt32(AppParameters.MaxLogFileSizeKB) * 1024))
                        {
                            File.Delete(logFileFullPath);
                        }
                    }
                    if (!Directory.Exists(logFilePath))
                    {
                        Directory.CreateDirectory(logFilePath);
                    }
                    using (StreamWriter sw = File.AppendText(logFileFullPath))
                    {
                        sw.WriteLine(text);
                    }
                }
                catch (Exception ex)
                {
                    WriteLine("Error-AppendTextFile:\t" + ex.Message);
                    LogProg("Messaging", "Error-AppendTextFile:\t" + ex.Message);
                    LogProg("Messaging", text);
                }
            }
        }

        private static void SendOutMessage(string text)
        {
            try
            {
               // MessageServer.SendMessage(text + "\r\n");
            }
            catch (Exception ex)
            {
                WriteLine("Error-SendOutMessage:\t" + ex.Message);
                LogProg("Messaging", "Error-SendOutMessage:\t" + ex.Message);
                LogProg("Messaging", text);
            }
        }
    }

}
