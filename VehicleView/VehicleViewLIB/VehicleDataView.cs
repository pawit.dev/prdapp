﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace VehicleViewLIB
{
    public partial class VehicleDataView : UserControl
    {

        #region INITIALIZE

        public VehicleDataView()
        {
            InitializeComponent();
            EmptyControls();
            this.panel1.Visible = false;
        }

        public void Clear()
        {
            EmptyControls();
            this.panel1.Visible = false;
        }

        void EmptyControls()
        {
            this.pictureBox1.Image = null;
            this.pictureBox2.Image = null;
            this.dataGridView1.Rows.Clear();
            this.dataGridView2.Rows.Clear();
            this.dataGridView3.Rows.Clear();
        }

        public void RefreshView(ViewType type, string refID)
        {
            if (type == ViewType.STATIC)
            {
                this.dataGridView3.Visible = false;
                this.pictureBox3.Visible = true;
                RefreshView_STATIC(refID);
            }
            else if (type == ViewType.WIM)
            {
                this.dataGridView3.Visible = true;
                this.pictureBox3.Visible = true;
                RefreshView_WIM(refID);
            }
            else if (type == ViewType.WARNING_Avoiding)
            {
                this.dataGridView3.Visible = false;
                this.pictureBox3.Visible = false;
                RefreshView_Avoiding(refID);
                this.panel_StatusColor.BackColor = Utils.GetWarningTypeColorCode(1);
            }
            else if (type == ViewType.WARNING_RunningScale)
            {
                this.dataGridView3.Visible = true;
                this.pictureBox3.Visible = true;
                RefreshView_WIM(refID);
                this.panel_StatusColor.BackColor = Utils.GetWarningTypeColorCode(2);
            }
            else if (type == ViewType.WARNING_OverWeight)
            {
                this.dataGridView3.Visible = false;
                this.pictureBox3.Visible = true;
                RefreshView_STATIC(refID);
                this.panel_StatusColor.BackColor = Utils.GetWarningTypeColorCode(3);
            }
            else if (type == ViewType.WARNING_BGOpened)
            {
                this.dataGridView3.Visible = false;
                this.pictureBox3.Visible = true;
                RefreshView_STATIC(refID);
                this.panel_StatusColor.BackColor = Utils.GetWarningTypeColorCode(4);
            }
        }

        #endregion INITIALIZE

        #region REFRESHVIEW_STATIC

        void RefreshView_STATIC(string RefID)
        {
            EmptyControls();
            this.panel1.Visible = true;
 
            DataTable table = GetData_STATIC(RefID);
            if (table.Rows.Count == 0)
            {
                return;
            }

            DataRow datarow = table.Rows[0];

            // Display Image1
            string imgName1 = (string)datarow["LPR_ImageName"];
            string fullPath1 = Utils.GetImageFolder(imgName1) + @"\" + imgName1;
            if (File.Exists(fullPath1))
            {
                this.pictureBox1.Image = Bitmap.FromFile(fullPath1);
            }

            // Display Image2
            string imgName2 = (string)datarow["VehicleImageName"];
            string fullPath2 = Utils.GetImageFolder(imgName2) + @"\" + imgName2;
            if (File.Exists(fullPath2))
            {
                this.pictureBox2.Image = Bitmap.FromFile(fullPath2);
            }

            // Display Image3
            string imgName3 =  "class" + ((int)datarow["VehicleClassID"]).ToString() + ".jpg";
            string currentpath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            string fullPath3 = currentpath + @"\vclass_images\" + imgName3;
            if (File.Exists(fullPath3))
            {
                this.pictureBox3.Image = Bitmap.FromFile(fullPath3);
            }

            this.dataGridView1.Rows.Add(10);
            DataGridViewRowCollection dvr1 = this.dataGridView1.Rows;

            dvr1[0].Cells[0].Value = "ENFID";
            dvr1[0].Cells[1].Value = ((long)datarow["ENFID"]).ToString();

            dvr1[1].Cells[0].Value = "Station";
            string stationID = ((int)datarow["StationID"]).ToString();
            dvr1[1].Cells[1].Value = (string)datarow["StationName"] + " (" + stationID + ")";

            dvr1[2].Cells[0].Value = "Date";
            dvr1[2].Cells[1].Value = ((DateTime)datarow["TimeStamp"]).ToString("dd MMM yyyy");

            dvr1[3].Cells[0].Value = "Time";
            dvr1[3].Cells[1].Value = ((DateTime)datarow["TimeStamp"]).ToString("HH:mm:ss");

            dvr1[4].Cells[0].Value = "Seq Number";
            dvr1[4].Cells[1].Value = ((int)datarow["SeqNumber"]).ToString();

            dvr1[5].Cells[0].Value = "User";

            string userName = (string)datarow["FirstName"] + " " + (string)datarow["LastName"];

            dvr1[5].Cells[1].Value = userName;

            dvr1[6].Cells[0].Value = "Vehicle Class";
            dvr1[6].Cells[1].Value = ((int)datarow["VehicleClassID"]).ToString();

            dvr1[7].Cells[0].Value = "LP Number";
            dvr1[7].Cells[1].Value = (string)datarow["LPR_Number"];

            dvr1[8].Cells[0].Value = "Province";
            dvr1[8].Cells[1].Value = (string)datarow["ProvinceName"];

            dvr1[9].Cells[0].Value = "Comply Method";
            dvr1[9].Cells[1].Value = ((byte)datarow["ComplyMethod"] == 0)?"GVW":"AGL";

            this.dataGridView2.Rows.Add(10);
            DataGridViewRowCollection dvr2 = this.dataGridView2.Rows;

            dvr2[0].Cells[0].Value = "WIMID";
            dvr2[0].Cells[1].Value = ((long)datarow["WIMID"]).ToString();

            dvr2[1].Cells[0].Value = "WIM GVW";
            dvr2[1].Cells[1].Value = ((int)datarow["GVW_Weight_WIM"]).ToString("N0") + " Kg";

            dvr2[2].Cells[0].Value = "Static GVW";
            dvr2[2].Cells[1].Value = ((int)datarow["GVW_Weight_Measured"]).ToString("N0") + " Kg";

            dvr2[3].Cells[0].Value = "Max GVW";
            dvr2[3].Cells[1].Value = ((int)datarow["GVW_Weight_Max"]).ToString("N0") + " Kg";

            dvr2[4].Cells[0].Value = "Status";
            dvr2[4].Cells[1].Value = ((string)datarow["IsOverWeight"] == "Y")? "OverGVW" : "PASS";
            this.panel_StatusColor.BackColor = Utils.GetIsOverWeightColorCode((string)datarow["IsOverWeight"]);

            dvr2[5].Cells[0].Value = "Barrier Gate";
            dvr2[5].Cells[1].Value = ((string)datarow["IsBGOpened"] == "Y")? "Opened" : "Closed";

            dvr2[6].Cells[0].Value = "Goods";
            dvr2[6].Cells[1].Value = (string)datarow["Material"];

            dvr2[7].Cells[0].Value = "Departed From";
            dvr2[7].Cells[1].Value = (string)datarow["DepartedFrom"];

            dvr2[8].Cells[0].Value = "Destination";
            dvr2[8].Cells[1].Value = (string)datarow["Destination"];

            dvr2[9].Cells[0].Value = "Notes";
            dvr2[9].Cells[1].Value = (string)datarow["Notes"];

        }

        DataTable GetData_STATIC(string refID)
        {
            string sql = SQLText.GetSQLText_STATIC(refID);
            return Utils.GetDataTable(sql);
        }

        #endregion REFRESHVIEW_STATIC

        #region REFRESHVIEW_WIM

        void RefreshView_WIM(string RefID)
        {
            EmptyControls();
            this.panel1.Visible = true;

            DataTable table = GetData_WIM(RefID);
            if (table.Rows.Count == 0)
            {
                return;
            }

            DataRow datarow = table.Rows[0];

            // Display Image1
            string imgName1 = (string)datarow["Image01Name"];

            //string fullPath1 = Utils.GetImageFolder(imgName1) + @"\" + imgName1;
            //if (File.Exists(fullPath1))
            //{
            //    this.pictureBox1.Image = Bitmap.FromFile(fullPath1);
            //}

            string imgUrl1 = Utils.GetImageURL(imgName1);
            try
            {
                var webClient1 = new WebClient();
                webClient1.Proxy = null;
                byte[] bytes = webClient1.DownloadData(imgUrl1);
                MemoryStream ms = new MemoryStream(bytes);
                this.pictureBox1.Image = Image.FromStream(ms);
            }
            catch { }

            // Display Image2
            string imgName2 = (string)datarow["LicensePlateImageName"];

            //string fullPath2 = Utils.GetImageFolder(imgName2) + @"\" + imgName2;
            //if (File.Exists(fullPath2))
            //{
            //    this.pictureBox2.Image = Bitmap.FromFile(fullPath2);
            //}

            string imgUrl2 = Utils.GetImageURL(imgName2);
            try
            {
                var webClient2 = new WebClient();
                webClient2.Proxy = null;
                byte[] bytes = webClient2.DownloadData(imgUrl2);
                MemoryStream ms = new MemoryStream(bytes);
                this.pictureBox2.Image = Image.FromStream(ms);
            }
            catch { }
          
            // Display Image3
            string imgName3 = "class" + ((byte)datarow["VehicleClass"]).ToString() + ".jpg";

            //string currentpath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            //string fullPath3 = currentpath + @"\vclass_images\" + imgName3;
            //if (File.Exists(fullPath3))
            //{
            //    this.pictureBox3.Image = Bitmap.FromFile(fullPath3);
            //}

            string imgUrl3 = Utils.GetImageURL(imgName3);
            try
            {
                var webClient3 = new WebClient();
                webClient3.Proxy = null;
                byte[] bytes = webClient3.DownloadData(imgUrl3);
                MemoryStream ms = new MemoryStream(bytes);
                this.pictureBox3.Image = Image.FromStream(ms);
            }
            catch { }

            // Display Vehicle Info
            this.dataGridView1.Rows.Add(10);
            DataGridViewRowCollection dvr1 = this.dataGridView1.Rows;
            int n = 0;

            dvr1[n].Cells[0].Value = "WIMID";
            dvr1[n].Cells[1].Value = ((long)datarow["WIMID"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Station";
            string stationID = ((byte)datarow["StationID"]).ToString();
            dvr1[n].Cells[1].Value = (string)datarow["StationName"] + " (" + stationID + ")";
            n++;

            dvr1[n].Cells[0].Value = "Date";
            dvr1[n].Cells[1].Value = ((DateTime)datarow["TimeStamp"]).ToString("dd MMM yyyy");
            n++;

            dvr1[n].Cells[0].Value = "Time";
            dvr1[n].Cells[1].Value = ((DateTime)datarow["TimeStamp"]).ToString("HH:mm:ss");
            n++;

            dvr1[n].Cells[0].Value = "Vehicle Number";
            dvr1[n].Cells[1].Value = ((int)datarow["VehicleNumber"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Lane";
            byte laneID = (byte)datarow["Lane"];
            string laneName = Utils.GetLaneName(laneID);
            dvr1[n].Cells[1].Value = laneName + " (" + (laneID + 1).ToString() + ")";
            n++;

            dvr1[n].Cells[0].Value = "Vehicle Class";
            dvr1[n].Cells[1].Value = ((byte)datarow["VehicleClass"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Axle Count";
            dvr1[n].Cells[1].Value = ((byte)datarow["AxleCount"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Speed";
            dvr1[n].Cells[1].Value = ((byte)datarow["Speed"]).ToString() + " Km/h";
            n++;

            dvr1[n].Cells[0].Value = "ESAL";
            dvr1[n].Cells[1].Value = ((double)datarow["ESAL"]).ToString("N2");
            
            //  last row
            dvr1[n].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;
            dvr1[n].Height = 50;
            n++;

            this.dataGridView2.Rows.Add(10);
            dvr1 = this.dataGridView2.Rows;
            n = 0;
            dvr1[n].Cells[0].Value = "WIM GVW";
            dvr1[n].Cells[1].Value = ((int)datarow["GVW"]).ToString("N0") + " Kg";
            n++;

            dvr1[n].Cells[0].Value = "Max GVW";
            dvr1[n].Cells[1].Value = ((int)datarow["MaxGVW"]).ToString("N0") + " Kg";
            n++;

            dvr1[n].Cells[0].Value = "Length";
            dvr1[n].Cells[1].Value = ((short)datarow["Length"]).ToString() + " cm";
            n++;

            dvr1[n].Cells[0].Value = "Front OverHang";
            dvr1[n].Cells[1].Value = ((short)datarow["FrontOverHang"]).ToString() + " cm";
            n++;

            dvr1[n].Cells[0].Value = "Rear OverHang";
            dvr1[n].Cells[1].Value = ((short)datarow["RearOverHang"]).ToString() + " cm";
            n++;

            dvr1[n].Cells[0].Value = "License Plate";
            dvr1[n].Cells[1].Value = (datarow["LicensePlateNumber"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Province";
            dvr1[n].Cells[1].Value = (datarow["ProvinceName"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Error";
            dvr1[n].Cells[1].Value = ((VRErrorCode)(byte)datarow["Error"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Sort Decision";
            VRSortDecisionCode sortDCS = (VRSortDecisionCode)(byte)datarow["SortDecision"];
            dvr1[n].Cells[1].Value = sortDCS.ToString();

            this.panel_StatusColor.BackColor = Utils.GetSortDecisionColorCode(sortDCS);

            n++;

            dvr1[n].Cells[0].Value = "Status";
            dvr1[n].Cells[1].Value = (Utils.GetStatusCodeText((VRStatusCode)datarow["StatusCode"]));

            //  last row
            dvr1[n].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft;
            dvr1[n].Height = 50;

            // Display Axle Info

            this.dataGridView3.Rows.Add(6);
            DataGridViewRowCollection dvr3 = this.dataGridView3.Rows;
            this.dataGridView3.Columns[0].Visible = true;
            this.dataGridView3.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dvr3[0].Cells[0].Value = "Seperation";
            dvr3[1].Cells[0].Value = "Group";
            dvr3[2].Cells[0].Value = "TireCode";
            dvr3[2].Visible = false;
            dvr3[3].Cells[0].Value = "Max";
            dvr3[3].Visible = false;
            dvr3[4].Cells[0].Value = "Weight";
            dvr3[5].Cells[0].Value = "Over?";
            dvr3[5].Visible = false;

            int axles = (int)((byte)datarow["AxleCount"]);
            for (int i = 1; i <= axles; i++)
            {
                this.dataGridView3.Columns[i].Visible = true;
                string f = "Axle" + i.ToString().PadLeft(2, '0');

                dvr3[0].Cells[i].Value = ((short)datarow[f + "Seperation"]).ToString();
                dvr3[1].Cells[i].Value = Utils.GetAxleGroupText((byte)datarow[f + "Group"]);
                dvr3[1].Cells[i].Style.Font = new Font("Calibri", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));

                dvr3[2].Cells[i].Value = ((byte)datarow[f + "TireCode"]).ToString();

                short maxwt = (short)datarow[f + "Max"];
                short wt = (short)datarow[f + "Weight"];
                string over = (wt > maxwt) ? "*" : "";
                dvr3[3].Cells[i].Value = (maxwt.ToString("N0"));
                dvr3[4].Cells[i].Value = (wt.ToString("N0"));
                dvr3[5].Cells[i].Value = over;
            }
        }

        DataTable GetData_WIM(string refID)
        {
            string sql = SQLText.GetSQLText_WIM(refID);
            return Utils.GetDataTable(sql);
        }

        #endregion REFRESHVIEW_WIM

        #region REFRESHVIEW_Avoiding

        void RefreshView_Avoiding(string RefID)
        {
            EmptyControls();
            this.panel1.Visible = true;

            DataTable table = GetData_RadarDetection(RefID);
            if (table.Rows.Count == 0)
            {
                return;
            }

            DataRow datarow = table.Rows[0];

            // Display Image1
            string imgName1 = (string)datarow["ImageName"];
            string fullPath1 = Utils.GetImageFolder(imgName1) + @"\" + imgName1;
            if (File.Exists(fullPath1))
            {
                this.pictureBox1.Image = Bitmap.FromFile(fullPath1);
            }

            // RSID StationID TimeStamp SeqNumber LaneID Range Duration Speed Class Length MinLength ImageName
            // Display Vehicle Info
            this.dataGridView1.Rows.Add(6);
            DataGridViewRowCollection dvr1 = this.dataGridView1.Rows;
            int n = 0;

            dvr1[n].Cells[0].Value = "RSID";
            dvr1[n].Cells[1].Value = ((long)datarow["RSID"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Station";
            string stationID = ((int)datarow["StationID"]).ToString();
            dvr1[n].Cells[1].Value = (string)datarow["StationName"] + " (" + stationID + ")";
            n++;

            dvr1[n].Cells[0].Value = "Date";
            dvr1[n].Cells[1].Value = ((DateTime)datarow["TimeStamp"]).ToString("dd MMM yyyy");
            n++;

            dvr1[n].Cells[0].Value = "Time";
            dvr1[n].Cells[1].Value = ((DateTime)datarow["TimeStamp"]).ToString("HH:mm:ss");
            n++;

            dvr1[n].Cells[0].Value = "Seq Number";
            dvr1[n].Cells[1].Value = ((int)datarow["SeqNumber"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Lane";
            int laneID = (int)datarow["LaneID"];
            string laneName = "Main";
            dvr1[n].Cells[1].Value = laneName + " " + (laneID + 1).ToString() ;
            n++;

           
            this.dataGridView2.Rows.Add(6);
            dvr1 = this.dataGridView2.Rows;
            n = 0;
            dvr1[n].Cells[0].Value = "Range";
            dvr1[n].Cells[1].Value = ((decimal)datarow["Range"]).ToString("N2") + " m";
            n++;

            dvr1[n].Cells[0].Value = "Duration";
            dvr1[n].Cells[1].Value = ((decimal)datarow["Duration"]).ToString("N2") + " sec";
            n++;

            dvr1[n].Cells[0].Value = "Speed";
            dvr1[n].Cells[1].Value = ((decimal)datarow["Speed"]).ToString("N2") + " Km/h";
            n++;

            dvr1[n].Cells[0].Value = "Class";
            dvr1[n].Cells[1].Value = ((int)datarow["Class"]).ToString();
            n++;

            dvr1[n].Cells[0].Value = "Length";
            dvr1[n].Cells[1].Value = ((decimal)datarow["Length"]).ToString("N2") + " m";
            n++;

            dvr1[n].Cells[0].Value = "MinLength";
            dvr1[n].Cells[1].Value = ((decimal)datarow["MinLength"]).ToString("N2") + " m";

            this.panel_StatusColor.BackColor = Utils.GetWarningTypeColorCode(1);
            

           
        }

        DataTable GetData_RadarDetection(string refID)
        {
            string sql = SQLText.GetSQLText_RADAR(refID);
            return Utils.GetDataTable(sql);
        }

        #endregion REFRESHVIEW_Avoiding


    }
}
