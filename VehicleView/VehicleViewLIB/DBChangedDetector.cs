﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Security.Permissions;
using System.ComponentModel;
using VehicleViewLIB;
using System.Threading;

namespace DBChangeDetection
{
    public delegate void DBChangedDetected(object sender, DataTable dataTable, string info);
    public delegate void DBChangedError(object sender, string message);

    class DBChangedDetector
    {
        public string ConnectionString { get; set; }
        public string SQLText { get; set; }
        public event DBChangedDetected OnDBChangedDetected;
        public event DBChangedError OnError;

        public string ID { get; set; }
        string serviceID = string.Empty;
       
        SqlDependency dependency = new SqlDependency();

        const int interval = 1000 * 60 * 1;
        Timer timer;
        
        bool IsNotified = false;

        string logSource = "Detector";

        public void StartListening()
        {
            try
            {
                if (timer != null)
                {
                    timer.Dispose();
                }

                IsNotified = false;
                TimerCallback timerDelegate = new TimerCallback(TimerTick);
                timer = new Timer(timerDelegate, null, interval, interval);
                
                if (dependency != null)
                {
                    dependency.OnChange -= new OnChangeEventHandler(dependency_OnChange);
                    dependency = null;
                }

                StopListening();

                Register();

                string a = "Col=" + this.ID + "\tStarted\tNewListenID=" + serviceID;
                Messaging.LogOnly(logSource, a);
            }
            catch (Exception ex)
            {
                if (OnError != null)
                {
                    OnError(this, "Exception_Initialize: " + ex.Message);
                }
            }
        }

        public void Register()
        {
            try
            {
                IsNotified = false;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(SQLText, connection))
                    {
                        command.Notification = null;

                        dependency = new SqlDependency(command);
                        dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                        command.ExecuteNonQuery();
                        serviceID = GetServiceID();                        
                    }
                }
            }
            catch (Exception ex)
            {
                if (OnError != null)
                {
                    OnError(this, "Exception_Register: " + ex.Message);
                }          
            }        
        }

        void TimerTick(object state)
        {
            string a = "Col=" + this.ID + "\t" + "TimerTicked: " ; 
            
            if (!IsNotified)
            {
                if (dependency != null)
                {
                    dependency.OnChange -= new OnChangeEventHandler(dependency_OnChange);
                    dependency = null;
                }
                Register();
                a += "\t" + "Refresh" + "\t" + "NewListenID=" + serviceID;
            }

            Messaging.LogOnly(logSource, a);
        }
     
        void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            try
            {
                IsNotified = true;

                Register();

                string a = "Col=" + ID.ToString() + "\t" + "Detected=" + e.Info.ToString();
                a += "\t" + "NewListenID=" + serviceID;
                Messaging.LogOnly(logSource, a);

                if (e.Info != SqlNotificationInfo.Invalid)
                {
                    if (OnDBChangedDetected != null)
                    {
                        OnDBChangedDetected(this, null, ID);
                    }
                }
            }
            catch (Exception ex)
            {
                if (OnError != null)
                {
                    OnError(this, "Exception_OnChange: " + ex.Message);
                }
            }
            GC.Collect();
        }

        public void StopListening()
        {
            string killresult = string.Empty;
            if (serviceID != string.Empty)
            {
                killresult = KillServiceID(serviceID);

                Messaging.LogOnly(logSource, "Col=" + ID.ToString() + "\tStopListening - Kill ServiceID " + serviceID + "  " + killresult);

                if (killresult == string.Empty)
                {
                    serviceID = string.Empty;
                }               
            }          
        }

        string GetServiceID()
        {
            string output = string.Empty;

            string sql = "SELECT TOP 1 * FROM sys.dm_qn_subscriptions;";
            DataTable table = VehicleViewLIB.Utils.GetDataTable(sql);
            if (table.Rows.Count > 0)
            {
                output = ((int)table.Rows[0]["id"]).ToString();
            }
            return output;
        }

        string KillServiceID(string id)
        {
            string sql = "KILL QUERY NOTIFICATION SUBSCRIPTION " + id + ";";
            object output = new object();
            bool success = Utils.ExecuteSQLCommand(sql, out output);

            string a = string.Empty;

            if (success)
            {
                a = (string)output;
            }
            else
            {
                Exception e = (Exception)output;
                a = e.Message;
            }
            
            if (a == null)
                a = string.Empty;
            GC.Collect();

            return a;
        }
    
    }

}
