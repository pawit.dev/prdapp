﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;
using System.ComponentModel;

namespace VehicleViewLIB
{
    public class WIMSortingInfo
    {
        public bool result = false;
        public int SignMode { get; set; }
        public decimal PercentRandom { get; set; }
        public decimal PercentWeight { get; set; }
        public int VehicleCounts { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    }
    public class WIMVehicleCount
    {
        public int[] laneVehCount { get; set; }
        public int totalVehCount { get; set; }
    }

    public delegate void OnDataRecived(WIMSortingInfo info);
    public delegate void OnMessage(string msg);
    public delegate void OnVehicleCountUpdate(WIMVehicleCount vehCount);

    public class WIMController
    {
        public int ID { get; set; }
        public event OnDataRecived ISINCDataReceived;
        public event OnMessage ISINCConnectionMessage;
        public event OnVehicleCountUpdate ISINCVehicleCountUpdate;
        
        byte[] sendingData = new byte[0];
        List<byte> receivedData = new List<byte>();

        ClientSocket socket = new ClientSocket();

        WIMSortingInfo info = new WIMSortingInfo();
        WIMVehicleCount vehCount = new WIMVehicleCount();

        byte[] header_FF01 = new byte[] { 0x01, 0xFF, 0x00, 0x00 };
        byte[] header_FF0E = new byte[] { 0x0E, 0xFF, 0x00, 0x00 };

        List<byte> Buff_FF01 = new List<byte>();
        List<byte> Buff_FF0E = new List<byte>();

        int signMode = 0;
        int randomFactor = 0;
        int weightFactor = 0;
        int vehicleCount = 0;


        static string logSource = "WIMController";

        public WIMController()
        {
        }

        public void Initialize(string ISINCAddress)
        {
            string IP = ISINCAddress;
            int PORT = 3085;
            IPEndPoint EP = new IPEndPoint(IPAddress.Parse(IP), PORT);
            socket.RemoteEndPoint = EP;

            socket.OnConnectError += new ConnectionConnectError(socket_OnConnectError);
            socket.OnDisconnected += new ConnectionDisconnected(socket_OnDisconnected);
            socket.OnConnected += new ConnectionConnected(socket_OnConnected);
            socket.OnReceived += new ConnectionReceived(socket_OnReceived);
        
        }

        public void GetInfo()
        {
            info.Message = "Getting data";

            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage(info.Message + " ..");
            }

            byte[] sending = WIMSorterInfoCommand.GetBytes_GetInfo();
            Send(sending);

        }

        public void SetSignMode(int mode)
        {
            info.Message = "Set sign mode = " + mode.ToString();

            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage(info.Message + " ..");
            }
            
            signMode = mode;
            byte[] sending = WIMSorterInfoCommand.GetBytes_SignControl(mode);
            Send(sending);
        }

        public void ResetVehCount()
        {
            info.Message = "Reset vehicle count";

            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage(info.Message + " ..");
            }

            byte[] sending = WIMSorterInfoCommand.GetBytes_ResetVehicleCount();
            Send(sending);
        }

        public void ResetTracker()
        {
            info.Message = "Reset tracker";

            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage(info.Message + " ..");
            }

            byte[] sending = WIMSorterInfoCommand.GetBytes_ResetTracker();
            Send(sending);
        }

        public void SetRandom(int percent)
        {
            info.Message = "Set random factor = " + percent.ToString();

            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage(info.Message + " ..");
            }

            randomFactor = percent;

            byte[] sending = WIMSorterInfoCommand.GetBytes_RandomFactor(percent);
            Send(sending);
        }

        public void SetWeight(int percent)
        {
            info.Message = "Set weight factor = " + percent.ToString();

            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage(info.Message + " ..");
            }

            weightFactor = percent;

            byte[] sending = WIMSorterInfoCommand.GetBytes_WeightFactor(percent);
            Send(sending);
        }

        public void Close()
        {
            this.socket.Close();
        }

        void Send(byte[] data)
        {
            sendingData = data;

            if (!socket.IsConnected())
            {
                socket.Connect();
            }
            else
            {
                Send();
            }
        }

        void Send()
        {
            socket.SendBytes(sendingData);
            Messaging.LogAndShow(logSource, "Sent: " + "\r\n\r\n" + BitConverter.ToString(sendingData) + "\r\n");

            //info.Status = "Sent: " + sendingData.Length + " bytes";
            info.Status = "Command was sent";

            if (ISINCConnectionMessage != null)
            {               
                ISINCConnectionMessage(info.Message + " - " + info.Status);
            }
        }

        void socket_OnReceived(ClientSocket client, byte[] receiveData)
        {
            Messaging.LogAndShow(logSource, "Received:" + "\r\n\r\n" + BitConverter.ToString(receiveData) + "\r\n");
            SearchFF01(receiveData);
            SearchFF0E(receiveData);
        }

        void SearchFF01(byte[] receiveData)
        {
            try
            {
               
                // move read data to pendingData (List of byte)
                for (int i = 0; i < receiveData.Length; i++)
                {
                    Buff_FF01.Add(receiveData[i]);
                }

                while (true)
                {
                    // search for header
                    List<int> header_pos = SearchBytePattern(header_FF01, Buff_FF01.ToArray());

                    // case header not found 
                    if (header_pos.Count == 0)
                    {
                        if (Buff_FF01.Count >= header_FF01.Length)
                        {
                            // clear pendingData because it's useless
                            Buff_FF01.Clear();
                        }
                        // exit and wait for next round
                        break;
                    }

                    // Frame Format: [Header][DataSize][Data]
                    // DataSize length is fixed to 4 bytes as it converts from int, complete frame must equal to 635
                    // Totol Frame Size = Header + DataSize + Data = 4 + 4 + 635 = 643

                    int datasize_pos = header_pos[0] + header_FF01.Length;
                    int data_pos = datasize_pos + 4;
                    int data_size = BitConverter.ToInt32(Buff_FF01.ToArray(), datasize_pos);
                    int frame_size = header_FF01.Length + 4 + data_size;
                    int pendingData_size = Buff_FF01.Count - header_pos[0];

                    // case data frame has header but not complete
                    if (pendingData_size < frame_size)
                    {
                        // exit and wait for next round
                        break;
                    }

                    // now the complete data is here - extract from pendingData                
                    byte[] data = new byte[frame_size];
                    Buff_FF01.CopyTo(0, data, 0, frame_size);
                    // Console.WriteLine("FZ = " + frame_size.ToString());
                    // Console.WriteLine("DZ = " + data_size.ToString());

                    // remove out the complete frame
                    Buff_FF01.RemoveRange(header_pos[0], frame_size);

                    BackgroundWorker background_ProcessReceivedDataFF01 = new BackgroundWorker();
                    background_ProcessReceivedDataFF01.DoWork += new DoWorkEventHandler(background_ProcessReceivedData_DoWorkFF01);
                    if (background_ProcessReceivedDataFF01.IsBusy != true)
                    {
                        background_ProcessReceivedDataFF01.RunWorkerAsync(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Messaging.LogAndShow(logSource, "ERROR_SearchFF01:" + "\r\n" + ex.Message);
            }
            GC.Collect();
        }

        void SearchFF0E(byte[] receiveData)
        {
            try
            {
                // move read data to pendingData (List of byte)
                for (int i = 0; i < receiveData.Length; i++)
                {
                    Buff_FF0E.Add(receiveData[i]);
                }

                while (true)
                {
                    // search for header
                    List<int> header_pos = SearchBytePattern(header_FF0E, Buff_FF0E.ToArray());

                    // case header not found 
                    if (header_pos.Count == 0)
                    {
                        if (Buff_FF0E.Count >= header_FF0E.Length)
                        {
                            // clear pendingData because it's useless
                            Buff_FF0E.Clear();
                        }
                        // exit and wait for next round
                        break;
                    }

                    // Frame Format: [Header][DataSize][Data]
                    // DataSize length is fixed to 4 bytes as it converts from int, complete frame must equal to 635
                    // Totol Frame Size = Header + DataSize + Data = 4 + 4 + 635 = 643

                    int datasize_pos = header_pos[0] + header_FF0E.Length;
                    int data_pos = datasize_pos + 4;
                    int data_size = BitConverter.ToInt32(Buff_FF0E.ToArray(), datasize_pos);
                    int frame_size = header_FF0E.Length + 4 + data_size;
                    int pendingData_size = Buff_FF0E.Count - header_pos[0];

                    // case data frame has header but not complete
                    if (pendingData_size < frame_size)
                    {
                        // exit and wait for next round
                        break;
                    }

                    // now the complete data is here - extract from pendingData                
                    byte[] data = new byte[frame_size];
                    Buff_FF0E.CopyTo(0, data, 0, frame_size);
                    // Console.WriteLine("FZ = " + frame_size.ToString());
                    // Console.WriteLine("DZ = " + data_size.ToString());

                    // remove out the complete frame
                    Buff_FF0E.RemoveRange(header_pos[0], frame_size);

                    BackgroundWorker background_ProcessReceivedDataFF0E = new BackgroundWorker();
                    background_ProcessReceivedDataFF0E.DoWork += new DoWorkEventHandler(background_ProcessReceivedData_DoWorkFF0E);
                    if (background_ProcessReceivedDataFF0E.IsBusy != true)
                    {
                        background_ProcessReceivedDataFF0E.RunWorkerAsync(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Messaging.LogAndShow(logSource, "ERROR_SearchFF0E:" + "\r\n" + ex.Message);
            }
            GC.Collect();
        }

        private List<int> SearchBytePattern(byte[] pattern, byte[] bytes)
        {
            List<int> positions = new List<int>();
            int patternLength = pattern.Length;
            int totalLength = bytes.Length;
            byte firstMatchByte = pattern[0];

            try
            {
                for (int i = 0; i < totalLength; i++)
                {
                    if (firstMatchByte == bytes[i] && totalLength - i >= patternLength)
                    {
                        byte[] match = new byte[patternLength];
                        Array.Copy(bytes, i, match, 0, patternLength);
                        if (match.SequenceEqual<byte>(pattern))
                        {
                            positions.Add(i);
                            i += patternLength - 1;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR\tSearchBytePattern\t" + ex.Message);
            }

            return positions;
        }

        void background_ProcessReceivedData_DoWorkFF01(object sender, DoWorkEventArgs e)
        {
            byte[] data = (byte[])e.Argument;
            ProcessReceivedDataFF01(data);
        }

        private void ProcessReceivedDataFF01(byte[] data)
        {
            try
            {
                int ContentLength = BitConverter.ToInt32(data, 4);
                if (data.Length == 120 && ContentLength == 112)
                {                  
                   ParseSorterInfo(data);
                }
            }
            catch (Exception ex)
            {
                Messaging.LogAndShow(logSource, "Error_ProcessReceivedDataFF01: \r\n" + ex.Message);
            }
        }

        WIMSortingInfo ParseSorterInfo(byte[] data)
        {
            int SorterInstance = (int)data[119];
            if (SorterInstance == 0)
            {

                info.Status = "Received: " + data.Length + " bytes";

                if (ISINCConnectionMessage != null)
                {
                    ISINCConnectionMessage(info.Message + " " + info.Status);
                }

                info.SignMode = (int)data[27];
                info.PercentRandom = (int)data[28];
                float f_ran = (float)BitConverter.ToSingle(data, 32) * 100;
                info.PercentWeight = (int)(f_ran);

                info.Status = "";

                if (info.Message.Contains("Getting data"))
                {
                    info.Status += "success";
                }
                if (info.SignMode == signMode && info.Message.Contains("sign mode"))
                {
                    info.Status += "success";
                }
                if (info.PercentRandom == randomFactor && info.Message.Contains("random factor"))
                {
                    info.Status += "success";
                }
                if (info.PercentWeight == weightFactor && info.Message.Contains("weight factor"))
                {
                    info.Status += "success";
                }
                if (info.VehicleCounts == vehicleCount && info.Message.Contains("vehicle count"))
                {
                    info.Status += "success";
                }


                if (ISINCConnectionMessage != null)
                {
                    ISINCConnectionMessage(info.Message + "  " + info.Status);
                }

                if (ISINCDataReceived != null)
                {
                    ISINCDataReceived(info);
                }
            }
            return info;
        }

        void background_ProcessReceivedData_DoWorkFF0E(object sender, DoWorkEventArgs e)
        {
            byte[] data = (byte[])e.Argument;
            ProcessReceivedDataFF0E(data);
        }

        private void ProcessReceivedDataFF0E(byte[] data)
        {
            try
            {
                int ContentLength = BitConverter.ToInt32(data, 4);
                if (data.Length == 136 && ContentLength == 128)
                {
                    vehCount.laneVehCount = new int[16];
                    vehCount.totalVehCount = 0;
                    for (int i = 0; i < vehCount.laneVehCount.Length; i++)
                    {
                        vehCount.laneVehCount[i] = (int)(BitConverter.ToInt16(data, 8 * (i+1)));
                        vehCount.totalVehCount += vehCount.laneVehCount[i];
                    }

                    if (ISINCVehicleCountUpdate != null)
                    {
                        ISINCVehicleCountUpdate(vehCount);
                    }

                }
            }
            catch (Exception ex)
            {
                Messaging.LogAndShow(logSource, "Error_ProcessReceivedDataFF0E: \r\n" + ex.Message);
            }
        }

        void socket_OnConnected(ClientSocket client)
        {
            info.Status = "ISINC Connected";

            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage(info.Message + " " + info.Status);
            }
            Send();
        }

        void socket_OnDisconnected(ClientSocket client)
        {
            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage("ISINC Disconnected");
            }
        }

        void socket_OnConnectError(ClientSocket client, string ErrorMessage)
        {
            if (ISINCConnectionMessage != null)
            {
                ISINCConnectionMessage("Error connecting to ISINC");
            }
        } 
    }


    class WIMSorterInfoCommand
    {
        public static byte[] GetBytes_SignControl(int mode)
        {
            byte[] data = new byte[] 
                {
                    0x01, 0xFF, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 
                    0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 
                    0x4C, 0x4C, 0x97, 0x03, 0x65, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x21, 0x00, 0x00, 
                                   //--This Byte--//
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00
                };

            // change byte 27 to math required mode
            // 00 65 -- off
            // 01 65 -- sorting
            // 02 65 -- report
            // 03 65 -- bypass

            data[27] = (byte)mode;

            return data;
        }

        public static byte[] GetBytes_GetVehicleCount()
        {
            return new byte[] { 0x0A, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        }

        public static byte[] GetBytes_ResetVehicleCount()
        {
            return new byte[] { 0x0D, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };           
        }

        public static byte[] GetBytes_ResetTracker()
        {
            return new byte[] { 0x05, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        }

        public static byte[] GetBytes_RandomFactor(int percent)
        {
            byte[] data = new byte[] 
                {
                    0x01, 0xFF, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 
                    0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 
                    0x4C, 0x4C, 0x97, 0x05, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x21, 0x00, 0x00, 
                                     //--This^Byte--//
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00
                };

            // change byte 28 to math required mode
            // 00 -- 0
            // 05 -- 5
            // 0A -- 15
            // 14 -- 20
            // 19 -- 25

            data[28] = (byte)percent;

            return data;
        }

        public static byte[] GetBytes_WeightFactor(int percent)
        {
            byte[] data = new byte[] 
                {
                    0x01, 0xFF, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 
                    0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 
                    0x4C, 0x4C, 0x97, 0x05, 0x65, 0x00, 0x00, 0x00, 0x33, 0x33, 0x73, 0x3F, 0x04, 0x21, 0x00, 0x00, 
                                                                  //|----This^ 4 Bytes----|//
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00
                };

            // change byte 32 33 34 35 - mathing to float byte array

            int n = percent;
            float x = (float)n;
            float y = x / 100;
            byte[] b = BitConverter.GetBytes(y);

            data[32] = b[0];
            data[33] = b[1];
            data[34] = b[2];
            data[35] = b[3];

            return data;
        }

        public static byte[] GetBytes_GetInfo()
        {
            return new byte[] { 0x08, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        }
    }

}

    