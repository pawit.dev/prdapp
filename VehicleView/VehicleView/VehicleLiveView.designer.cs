﻿namespace VehicleView
{
    partial class VehicleLiveView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.vehicleListView3 = new VehicleViewLIB.VehicleListView();
            this.vehicleDataView1 = new VehicleViewLIB.VehicleDataView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label_COL3_Title = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.vehicleListView2 = new VehicleViewLIB.VehicleListView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label_COL2_Title = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.vehicleListView1 = new VehicleViewLIB.VehicleListView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label_COL1_Title = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.clock1 = new VehicleViewLIB.Clock();
            this.button_Layout = new System.Windows.Forms.Button();
            this.label_StationName = new System.Windows.Forms.Label();
            this.label_MAIN_Title = new System.Windows.Forms.Label();
            this.button_Goto = new System.Windows.Forms.Button();
            this.button_Filters = new System.Windows.Forms.Button();
            this.button_Exit = new System.Windows.Forms.Button();
            this.button_Refresh = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1600, 762);
            this.panel1.TabIndex = 1;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.DimGray;
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(1590, 40);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(10, 678);
            this.panel12.TabIndex = 23;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DimGray;
            this.panel8.Controls.Add(this.vehicleListView3);
            this.panel8.Controls.Add(this.vehicleDataView1);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(990, 40);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(610, 678);
            this.panel8.TabIndex = 20;
            // 
            // vehicleListView3
            // 
            this.vehicleListView3.BackColor = System.Drawing.Color.Gray;
            this.vehicleListView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vehicleListView3.LiveView = true;
            this.vehicleListView3.Location = new System.Drawing.Point(0, 45);
            this.vehicleListView3.Name = "vehicleListView3";
            this.vehicleListView3.Size = new System.Drawing.Size(610, 633);
            this.vehicleListView3.TabIndex = 3;
            // 
            // vehicleDataView1
            // 
            this.vehicleDataView1.BackColor = System.Drawing.Color.DarkGray;
            this.vehicleDataView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vehicleDataView1.Location = new System.Drawing.Point(0, 45);
            this.vehicleDataView1.Name = "vehicleDataView1";
            this.vehicleDataView1.Size = new System.Drawing.Size(610, 633);
            this.vehicleDataView1.TabIndex = 4;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.DimGray;
            this.panel9.Controls.Add(this.label_COL3_Title);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(610, 45);
            this.panel9.TabIndex = 0;
            // 
            // label_COL3_Title
            // 
            this.label_COL3_Title.AutoSize = true;
            this.label_COL3_Title.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_COL3_Title.ForeColor = System.Drawing.Color.LightGray;
            this.label_COL3_Title.Location = new System.Drawing.Point(17, 15);
            this.label_COL3_Title.Name = "label_COL3_Title";
            this.label_COL3_Title.Size = new System.Drawing.Size(45, 19);
            this.label_COL3_Title.TabIndex = 4;
            this.label_COL3_Title.Text = "DATA";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.DimGray;
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(980, 40);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(10, 678);
            this.panel11.TabIndex = 22;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.vehicleListView2);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(500, 40);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(480, 678);
            this.panel6.TabIndex = 19;
            // 
            // vehicleListView2
            // 
            this.vehicleListView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vehicleListView2.LiveView = true;
            this.vehicleListView2.Location = new System.Drawing.Point(0, 45);
            this.vehicleListView2.Name = "vehicleListView2";
            this.vehicleListView2.Size = new System.Drawing.Size(480, 633);
            this.vehicleListView2.TabIndex = 2;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.DimGray;
            this.panel7.Controls.Add(this.label_COL2_Title);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(480, 45);
            this.panel7.TabIndex = 0;
            // 
            // label_COL2_Title
            // 
            this.label_COL2_Title.AutoSize = true;
            this.label_COL2_Title.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_COL2_Title.ForeColor = System.Drawing.Color.LightGray;
            this.label_COL2_Title.Location = new System.Drawing.Point(6, 15);
            this.label_COL2_Title.Name = "label_COL2_Title";
            this.label_COL2_Title.Size = new System.Drawing.Size(107, 19);
            this.label_COL2_Title.TabIndex = 4;
            this.label_COL2_Title.Text = "LANE: REPORT";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.DimGray;
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(490, 40);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(10, 678);
            this.panel10.TabIndex = 21;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.vehicleListView1);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(10, 40);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(480, 678);
            this.panel4.TabIndex = 18;
            // 
            // vehicleListView1
            // 
            this.vehicleListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vehicleListView1.LiveView = true;
            this.vehicleListView1.Location = new System.Drawing.Point(0, 45);
            this.vehicleListView1.Name = "vehicleListView1";
            this.vehicleListView1.Size = new System.Drawing.Size(480, 633);
            this.vehicleListView1.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DimGray;
            this.panel5.Controls.Add(this.label_COL1_Title);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(480, 45);
            this.panel5.TabIndex = 0;
            // 
            // label_COL1_Title
            // 
            this.label_COL1_Title.AutoSize = true;
            this.label_COL1_Title.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_COL1_Title.ForeColor = System.Drawing.Color.LightGray;
            this.label_COL1_Title.Location = new System.Drawing.Point(6, 15);
            this.label_COL1_Title.Name = "label_COL1_Title";
            this.label_COL1_Title.Size = new System.Drawing.Size(86, 19);
            this.label_COL1_Title.TabIndex = 4;
            this.label_COL1_Title.Text = "LANE: WIM";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DimGray;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(10, 718);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1590, 44);
            this.panel3.TabIndex = 17;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.DimGray;
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 40);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(10, 722);
            this.panel14.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.clock1);
            this.panel2.Controls.Add(this.button_Layout);
            this.panel2.Controls.Add(this.label_StationName);
            this.panel2.Controls.Add(this.label_MAIN_Title);
            this.panel2.Controls.Add(this.button_Goto);
            this.panel2.Controls.Add(this.button_Filters);
            this.panel2.Controls.Add(this.button_Exit);
            this.panel2.Controls.Add(this.button_Refresh);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1600, 40);
            this.panel2.TabIndex = 15;
            // 
            // clock1
            // 
            this.clock1.Location = new System.Drawing.Point(340, 5);
            this.clock1.Name = "clock1";
            this.clock1.Size = new System.Drawing.Size(172, 30);
            this.clock1.TabIndex = 8;
            // 
            // button_Layout
            // 
            this.button_Layout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Layout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Layout.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Layout.ForeColor = System.Drawing.Color.Black;
            this.button_Layout.Location = new System.Drawing.Point(1132, 7);
            this.button_Layout.Name = "button_Layout";
            this.button_Layout.Size = new System.Drawing.Size(80, 26);
            this.button_Layout.TabIndex = 7;
            this.button_Layout.TabStop = false;
            this.button_Layout.Text = "Layout";
            this.button_Layout.UseVisualStyleBackColor = true;
            this.button_Layout.Visible = false;
            this.button_Layout.Click += new System.EventHandler(this.button_Layout_Click);
            // 
            // label_StationName
            // 
            this.label_StationName.AutoSize = true;
            this.label_StationName.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_StationName.ForeColor = System.Drawing.Color.Black;
            this.label_StationName.Location = new System.Drawing.Point(185, 8);
            this.label_StationName.Name = "label_StationName";
            this.label_StationName.Size = new System.Drawing.Size(100, 23);
            this.label_StationName.TabIndex = 6;
            this.label_StationName.Text = "All Stations";
            // 
            // label_MAIN_Title
            // 
            this.label_MAIN_Title.AutoSize = true;
            this.label_MAIN_Title.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MAIN_Title.ForeColor = System.Drawing.Color.Black;
            this.label_MAIN_Title.Location = new System.Drawing.Point(6, 8);
            this.label_MAIN_Title.Name = "label_MAIN_Title";
            this.label_MAIN_Title.Size = new System.Drawing.Size(89, 23);
            this.label_MAIN_Title.TabIndex = 2;
            this.label_MAIN_Title.Text = "LIVE VIEW";
            // 
            // button_Goto
            // 
            this.button_Goto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Goto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Goto.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Goto.ForeColor = System.Drawing.Color.Black;
            this.button_Goto.Location = new System.Drawing.Point(1408, 7);
            this.button_Goto.Name = "button_Goto";
            this.button_Goto.Size = new System.Drawing.Size(80, 26);
            this.button_Goto.TabIndex = 5;
            this.button_Goto.TabStop = false;
            this.button_Goto.Text = "Go to";
            this.button_Goto.UseVisualStyleBackColor = true;
            this.button_Goto.Click += new System.EventHandler(this.button_Goto_Click);
            // 
            // button_Filters
            // 
            this.button_Filters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Filters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Filters.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Filters.ForeColor = System.Drawing.Color.Black;
            this.button_Filters.Location = new System.Drawing.Point(1218, 7);
            this.button_Filters.Name = "button_Filters";
            this.button_Filters.Size = new System.Drawing.Size(80, 26);
            this.button_Filters.TabIndex = 4;
            this.button_Filters.TabStop = false;
            this.button_Filters.Text = "Station";
            this.button_Filters.UseVisualStyleBackColor = true;
            this.button_Filters.Click += new System.EventHandler(this.button_Filters_Click);
            // 
            // button_Exit
            // 
            this.button_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Exit.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Exit.ForeColor = System.Drawing.Color.Black;
            this.button_Exit.Location = new System.Drawing.Point(1494, 7);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(80, 26);
            this.button_Exit.TabIndex = 3;
            this.button_Exit.TabStop = false;
            this.button_Exit.Text = "Exit";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // button_Refresh
            // 
            this.button_Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Refresh.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Refresh.ForeColor = System.Drawing.Color.Black;
            this.button_Refresh.Location = new System.Drawing.Point(1304, 7);
            this.button_Refresh.Name = "button_Refresh";
            this.button_Refresh.Size = new System.Drawing.Size(80, 26);
            this.button_Refresh.TabIndex = 1;
            this.button_Refresh.TabStop = false;
            this.button_Refresh.Text = "Refresh";
            this.button_Refresh.UseVisualStyleBackColor = true;
            this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Webdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(1132, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(37, 26);
            this.button1.TabIndex = 9;
            this.button1.TabStop = false;
            this.button1.Text = "4";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Webdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(1175, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(37, 26);
            this.button2.TabIndex = 10;
            this.button2.TabStop = false;
            this.button2.Text = ";";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // VehicleLiveView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1600, 762);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VehicleLiveView";
            this.Text = "Vehicle View";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel8;
        private VehicleViewLIB.VehicleListView vehicleListView3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label_COL3_Title;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel6;
        private VehicleViewLIB.VehicleListView vehicleListView2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label_COL2_Title;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel4;
        private VehicleViewLIB.VehicleListView vehicleListView1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label_COL1_Title;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_Layout;
        private System.Windows.Forms.Label label_StationName;
        private System.Windows.Forms.Label label_MAIN_Title;
        private System.Windows.Forms.Button button_Goto;
        private System.Windows.Forms.Button button_Filters;
        private System.Windows.Forms.Button button_Exit;
        private System.Windows.Forms.Button button_Refresh;
        private VehicleViewLIB.VehicleDataView vehicleDataView1;
        private VehicleViewLIB.Clock clock1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;



    }
}

