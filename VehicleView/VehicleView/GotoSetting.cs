﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VehicleViewLIB;

namespace VehicleView
{
    public partial class GotoSetting : Form
    {
        public int Selection { get; set; }

        public GotoSetting(int type)
        {
            InitializeComponent();
            InitDatagridView(type);
            Selection = 0;
            this.Deactivate += new EventHandler(GotoSetting_Deactivate);
        }

        void GotoSetting_Deactivate(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void InitDatagridView(int type)
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.Rows.Clear();

            //this.dataGridView1.Rows.Add(4);
            this.dataGridView1.Rows.Add(2);

            this.dataGridView1.Rows[0].Cells[0].Value = "LIVE VIEW";
            this.dataGridView1.Rows[1].Cells[0].Value = "WIM HISTORY VIEW";
            //this.dataGridView1.Rows[2].Cells[0].Value = "STATIC HISTORY VIEW";
            //this.dataGridView1.Rows[3].Cells[0].Value = "WIM STATION CONTROL";

            if (this.dataGridView1.Rows.Count > type)
            {
                this.dataGridView1.Rows[type].Visible = false;
            }
            this.dataGridView1.ClearSelection();

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            if (this.dataGridView1.SelectedRows.Count > 0)
            {
                Selection = this.dataGridView1.SelectedRows[0].Index;
            }

            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

    }
}
