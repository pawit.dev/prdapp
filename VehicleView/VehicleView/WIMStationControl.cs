﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VehicleViewLIB;


namespace VehicleView
{
    public partial class WIMStationControl : Form
    {
        public int stationID { get; set; }
        string stationName = string.Empty;
        WIMController isinc;
        List<WIMController> isincList = new List<WIMController>();

        Color backcolor_normal = Color.LightGray;
        Color backcolor_hilight = Color.Turquoise;

        List<StationItem> list = new List<StationItem>();
        Color selectionBackColor = Color.LightGray;

        private class StationItem
        {
            public int StationID { get; set; }
            public string StationName { get; set; }
        }

        public WIMStationControl()
        {
            
            InitializeComponent();
            
            stationID = Properties.Settings.Default.StationID;
            
            InitComboStationID();
            this.comboBox_Station.DrawMode = DrawMode.OwnerDrawFixed;
            this.comboBox_Station.DrawItem += new DrawItemEventHandler(comboBox_Station_DrawItem);

            this.linkLabel_RandomSet.MouseEnter += new EventHandler(linkLabel_RandomSet_MouseEnter);
            this.linkLabel_WeightSet.MouseEnter += new EventHandler(linkLabel_WeightSet_MouseEnter);
        }

        void comboBox_Station_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0)
                return;

            ComboBox combo = sender as ComboBox;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e.Graphics.FillRectangle(new SolidBrush(selectionBackColor),
                                         e.Bounds);
            else
                e.Graphics.FillRectangle(new SolidBrush(combo.BackColor),
                                         e.Bounds);

            string a = list[e.Index].StationName;

            e.Graphics.DrawString(a, e.Font,
                                  new SolidBrush(combo.ForeColor),
                                  new Point(e.Bounds.X, e.Bounds.Y));

            e.DrawFocusRectangle();
        }

        public void GetUpdatedInfo()
        {
            try
            {
                WDCUpdateControls.Update(this.panel1, "Visible", false);               
                WDCUpdateControls.Update(this.label_StationName, "Text", stationName);              
               
                if (stationID == 0)
                    return;

                string ISINCAddress = GetISINCAddress(stationID);

                if (isinc != null)
                {
                    isinc.ISINCConnectionMessage -= new OnMessage(isinc_ISINCConnectionMessage);
                    isinc.ISINCDataReceived -= new OnDataRecived(isinc_ISINCDataReceived);
                    isinc.ISINCVehicleCountUpdate -= new OnVehicleCountUpdate(isinc_ISINCVehicleCountUpdate);
                }

                int i = isincList.FindIndex(x => x.ID == stationID);

                if (isincList.Count == 0 || i < 0)
                {
                    isinc = new WIMController();
                    isinc.ID = stationID;
                    isinc.Initialize(ISINCAddress);
                    isincList.Add(isinc);
                }
                else
                {
                    isinc = isincList[i];
                }

                if (isinc == null)
                    return;

                isinc.ISINCConnectionMessage += new OnMessage(isinc_ISINCConnectionMessage);
                isinc.ISINCDataReceived += new OnDataRecived(isinc_ISINCDataReceived);
                isinc.ISINCVehicleCountUpdate += new OnVehicleCountUpdate(isinc_ISINCVehicleCountUpdate);

                isinc.GetInfo();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception_GetUpdatedInfo:" + "\r\n\r\n" + ex.Message);
            }
        
        }

        private void InitComboStationID()
        {
            this.comboBox_Station.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboBox_Station.DataSource = null;
            this.comboBox_Station.Items.Clear();

            string sql = "SELECT * FROM Station WHERE StationID > 0";
            DataTable table = Utils.GetDataTable(sql);

            if (table.Rows.Count > 0)
            {
                foreach (DataRow r in table.Rows)
                {
                    StationItem st = new StationItem();
                    st.StationID = (int)r["StationID"];
                    st.StationName = (string)r["StationName"];
                    list.Add(st);
                }

                BindingSource bs = new BindingSource();
                bs.DataSource = list;

                this.comboBox_Station.Items.Clear();
                this.comboBox_Station.DataSource = list;
                this.comboBox_Station.DisplayMember = "StationName";
                this.comboBox_Station.ValueMember = "StationID";

                //if (this.comboBox_Station.Items.Count > stationID)
                //{
                //    this.comboBox_Station.SelectedIndex = stationID;
                //}
                this.comboBox_Station.SelectedIndex = -1;
            }

            this.comboBox_Station.SelectedIndexChanged += new EventHandler(comboBox_Station_SelectedIndexChanged);
        }

        void comboBox_Station_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox_Station.SelectedIndex >= 0)
            {
                stationID = (int)this.comboBox_Station.SelectedValue;
                stationName = this.comboBox_Station.Text;
            }
        }

        void isinc_ISINCVehicleCountUpdate(WIMVehicleCount vehCount)
        {
            WDCUpdateControls.Update(this.label_VehicleCount, "Text",vehCount.totalVehCount.ToString());
            WDCUpdateControls.Update(this.label_VehicleCount, "BackColor", backcolor_hilight);
            System.Threading.Thread.Sleep(500);
            WDCUpdateControls.Update(this.label_VehicleCount, "BackColor", backcolor_normal);
        }

        void linkLabel_WeightSet_MouseEnter(object sender, EventArgs e)
        {
            this.linkLabel_WeightSet.Focus();
        }

        void linkLabel_RandomSet_MouseEnter(object sender, EventArgs e)
        {
            this.linkLabel_RandomSet.Focus();
        }

        void isinc_ISINCDataReceived(WIMSortingInfo info)
        {
            WDCUpdateControls.Update(this.panel1, "Visible", true);
    
            LinkLabel[] links = new LinkLabel[] {this.linkLabel_OFF, this.linkLabel_SORT, this.linkLabel_REPORT, this.linkLabel_BYPASS};
            foreach (LinkLabel lb in links)
            {
                WDCUpdateControls.Update(lb, "BackColor", backcolor_normal);                
            }

            WDCUpdateControls.Update(links[info.SignMode], "BackColor", backcolor_hilight);

            WDCUpdateControls.Update(this.numericUpDown_Random, "Value", info.PercentRandom);
            WDCUpdateControls.Update(this.numericUpDown_Weight, "Value", info.PercentWeight);

            WDCUpdateControls.Update(this.numericUpDown_Random, "BackColor", backcolor_hilight);
            WDCUpdateControls.Update(this.numericUpDown_Weight, "BackColor", backcolor_hilight);

            System.Threading.Thread.Sleep(500);

            WDCUpdateControls.Update(this.numericUpDown_Random, "BackColor", backcolor_normal);
            WDCUpdateControls.Update(this.numericUpDown_Weight, "BackColor", backcolor_normal);
        }

        void isinc_ISINCConnectionMessage(string msg)
        {
            WDCUpdateControls.Update(this.label_Status, "Text", msg);
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            if (isinc != null)
            {
                isinc.Close();
            }
            
            this.Close();
        }

        private void linkLabel_OFF_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            isinc.SetSignMode(0);
        }

        private void linkLabel_SORT_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            isinc.SetSignMode(1);
        }

        private void linkLabel_REPORT_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            isinc.SetSignMode(2);
        }

        private void linkLabel_BYPASS_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            isinc.SetSignMode(3);
        }

        private void linkLabel_RESETTracker_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            isinc.ResetTracker();
        }

        private void linkLabel_RESETCount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            isinc.ResetVehCount();
        }

        private void linkLabel_RandomSet_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            int rand = (int)this.numericUpDown_Random.Value;
            isinc.SetRandom(rand);
        }

        private void linkLabel_WeightSet_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            int w = (int)this.numericUpDown_Weight.Value;
            isinc.SetWeight(w);
        }

        private void button_Refresh_Click(object sender, EventArgs e)
        {
            GetUpdatedInfo();
        }

        string GetISINCAddress(int stationID)
        {
            string address = string.Empty;
            //if (stationID == 1)
            //{
            //    address = Properties.Settings.Default.ISINCAddress01;
            //}
            //else if (stationID == 2)
            //{
            //    address = Properties.Settings.Default.ISINCAddress02;
            //}
            //else if (stationID == 3)
            //{
            //    address = Properties.Settings.Default.ISINCAddress03;
            //}
            //else if (stationID == 4)
            //{
            //    address = Properties.Settings.Default.ISINCAddress04;
            //}
            //else if (stationID == 5)
            //{
            //    address = Properties.Settings.Default.ISINCAddress05;
            //}
            return address;
        }
    
    }


    class AdvancedComboBox : ComboBox
    {
        new public System.Windows.Forms.DrawMode DrawMode { get; set; }
        public Color HighlightColor { get; set; }

        public AdvancedComboBox()
        {
            base.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.HighlightColor = Color.Gray;
            this.DrawItem += new DrawItemEventHandler(AdvancedComboBox_DrawItem);
        }

        void AdvancedComboBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0)
                return;

            ComboBox combo = sender as ComboBox;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e.Graphics.FillRectangle(new SolidBrush(HighlightColor),
                                         e.Bounds);
            else
                e.Graphics.FillRectangle(new SolidBrush(combo.BackColor),
                                         e.Bounds);

            e.Graphics.DrawString(combo.Items[e.Index].ToString(), e.Font,
                                  new SolidBrush(combo.ForeColor),
                                  new Point(e.Bounds.X, e.Bounds.Y));

            e.DrawFocusRectangle();
        }
    }
}
