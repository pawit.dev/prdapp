﻿namespace VehicleView
{
    partial class WIMStationControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_main = new System.Windows.Forms.Panel();
            this.label_StationName = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.linkLabel_BYPASS = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel_REPORT = new System.Windows.Forms.LinkLabel();
            this.linkLabel_OFF = new System.Windows.Forms.LinkLabel();
            this.linkLabel_SORT = new System.Windows.Forms.LinkLabel();
            this.linkLabel_RESETTracker = new System.Windows.Forms.LinkLabel();
            this.linkLabel_RESETCount = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_Random = new System.Windows.Forms.NumericUpDown();
            this.label_VehicleCount = new System.Windows.Forms.Label();
            this.numericUpDown_Weight = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel_RandomSet = new System.Windows.Forms.LinkLabel();
            this.label_Tracker = new System.Windows.Forms.Label();
            this.linkLabel_WeightSet = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.comboBox_Station = new System.Windows.Forms.ComboBox();
            this.button_Refresh = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label_Status = new System.Windows.Forms.Label();
            this.button_Close = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel_main.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Random)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Weight)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_main
            // 
            this.panel_main.BackColor = System.Drawing.Color.LightGray;
            this.panel_main.Controls.Add(this.label_StationName);
            this.panel_main.Controls.Add(this.label7);
            this.panel_main.Controls.Add(this.panel1);
            this.panel_main.Controls.Add(this.panel3);
            this.panel_main.Controls.Add(this.label6);
            this.panel_main.Controls.Add(this.label_Status);
            this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_main.Location = new System.Drawing.Point(0, 44);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(710, 327);
            this.panel_main.TabIndex = 96;
            // 
            // label_StationName
            // 
            this.label_StationName.BackColor = System.Drawing.Color.Transparent;
            this.label_StationName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_StationName.ForeColor = System.Drawing.Color.Black;
            this.label_StationName.Location = new System.Drawing.Point(308, 13);
            this.label_StationName.Name = "label_StationName";
            this.label_StationName.Size = new System.Drawing.Size(332, 20);
            this.label_StationName.TabIndex = 117;
            this.label_StationName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(254, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 15);
            this.label7.TabIndex = 111;
            this.label7.Text = "Station:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.linkLabel_RESETTracker);
            this.panel1.Controls.Add(this.linkLabel_RESETCount);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.numericUpDown_Random);
            this.panel1.Controls.Add(this.label_VehicleCount);
            this.panel1.Controls.Add(this.numericUpDown_Weight);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.linkLabel_RandomSet);
            this.panel1.Controls.Add(this.label_Tracker);
            this.panel1.Controls.Add(this.linkLabel_WeightSet);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(221, 71);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(489, 256);
            this.panel1.TabIndex = 114;
            this.panel1.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.LightGray;
            this.panel4.Controls.Add(this.linkLabel_BYPASS);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.linkLabel_REPORT);
            this.panel4.Controls.Add(this.linkLabel_OFF);
            this.panel4.Controls.Add(this.linkLabel_SORT);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(489, 73);
            this.panel4.TabIndex = 117;
            // 
            // linkLabel_BYPASS
            // 
            this.linkLabel_BYPASS.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.linkLabel_BYPASS.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_BYPASS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_BYPASS.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_BYPASS.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_BYPASS.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_BYPASS.Location = new System.Drawing.Point(354, 25);
            this.linkLabel_BYPASS.Name = "linkLabel_BYPASS";
            this.linkLabel_BYPASS.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_BYPASS.TabIndex = 92;
            this.linkLabel_BYPASS.TabStop = true;
            this.linkLabel_BYPASS.Text = "BYPASS";
            this.linkLabel_BYPASS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_BYPASS.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_BYPASS.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_BYPASS_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 19);
            this.label1.TabIndex = 95;
            this.label1.Text = "Sign Mode:";
            // 
            // linkLabel_REPORT
            // 
            this.linkLabel_REPORT.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.linkLabel_REPORT.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_REPORT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_REPORT.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_REPORT.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_REPORT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_REPORT.Location = new System.Drawing.Point(282, 25);
            this.linkLabel_REPORT.Name = "linkLabel_REPORT";
            this.linkLabel_REPORT.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_REPORT.TabIndex = 91;
            this.linkLabel_REPORT.TabStop = true;
            this.linkLabel_REPORT.Text = "REPORT";
            this.linkLabel_REPORT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_REPORT.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_REPORT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_REPORT_LinkClicked);
            // 
            // linkLabel_OFF
            // 
            this.linkLabel_OFF.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.linkLabel_OFF.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_OFF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_OFF.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_OFF.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_OFF.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_OFF.Location = new System.Drawing.Point(139, 25);
            this.linkLabel_OFF.Name = "linkLabel_OFF";
            this.linkLabel_OFF.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_OFF.TabIndex = 89;
            this.linkLabel_OFF.TabStop = true;
            this.linkLabel_OFF.Text = "OFF";
            this.linkLabel_OFF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_OFF.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_OFF.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_OFF_LinkClicked);
            // 
            // linkLabel_SORT
            // 
            this.linkLabel_SORT.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.linkLabel_SORT.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_SORT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_SORT.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_SORT.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_SORT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_SORT.Location = new System.Drawing.Point(211, 25);
            this.linkLabel_SORT.Name = "linkLabel_SORT";
            this.linkLabel_SORT.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_SORT.TabIndex = 90;
            this.linkLabel_SORT.TabStop = true;
            this.linkLabel_SORT.Text = "SORT";
            this.linkLabel_SORT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_SORT.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_SORT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_SORT_LinkClicked);
            // 
            // linkLabel_RESETTracker
            // 
            this.linkLabel_RESETTracker.ActiveLinkColor = System.Drawing.Color.Silver;
            this.linkLabel_RESETTracker.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_RESETTracker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_RESETTracker.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_RESETTracker.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_RESETTracker.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_RESETTracker.Location = new System.Drawing.Point(314, 211);
            this.linkLabel_RESETTracker.Name = "linkLabel_RESETTracker";
            this.linkLabel_RESETTracker.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_RESETTracker.TabIndex = 93;
            this.linkLabel_RESETTracker.TabStop = true;
            this.linkLabel_RESETTracker.Text = "RESET";
            this.linkLabel_RESETTracker.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_RESETTracker.Visible = false;
            this.linkLabel_RESETTracker.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_RESETTracker.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_RESETTracker_LinkClicked);
            // 
            // linkLabel_RESETCount
            // 
            this.linkLabel_RESETCount.ActiveLinkColor = System.Drawing.Color.Silver;
            this.linkLabel_RESETCount.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_RESETCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_RESETCount.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_RESETCount.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_RESETCount.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_RESETCount.Location = new System.Drawing.Point(314, 183);
            this.linkLabel_RESETCount.Name = "linkLabel_RESETCount";
            this.linkLabel_RESETCount.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_RESETCount.TabIndex = 94;
            this.linkLabel_RESETCount.TabStop = true;
            this.linkLabel_RESETCount.Text = "RESET";
            this.linkLabel_RESETCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_RESETCount.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_RESETCount.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_RESETCount_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(95, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 19);
            this.label4.TabIndex = 100;
            this.label4.Text = "Random factor (%):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(95, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 19);
            this.label5.TabIndex = 101;
            this.label5.Text = "Weight factor (%):";
            // 
            // numericUpDown_Random
            // 
            this.numericUpDown_Random.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Random.Location = new System.Drawing.Point(242, 98);
            this.numericUpDown_Random.Name = "numericUpDown_Random";
            this.numericUpDown_Random.Size = new System.Drawing.Size(45, 26);
            this.numericUpDown_Random.TabIndex = 102;
            this.numericUpDown_Random.TabStop = false;
            // 
            // label_VehicleCount
            // 
            this.label_VehicleCount.AutoSize = true;
            this.label_VehicleCount.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_VehicleCount.Location = new System.Drawing.Point(236, 184);
            this.label_VehicleCount.Name = "label_VehicleCount";
            this.label_VehicleCount.Size = new System.Drawing.Size(17, 19);
            this.label_VehicleCount.TabIndex = 109;
            this.label_VehicleCount.Text = "0";
            // 
            // numericUpDown_Weight
            // 
            this.numericUpDown_Weight.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Weight.Location = new System.Drawing.Point(242, 126);
            this.numericUpDown_Weight.Name = "numericUpDown_Weight";
            this.numericUpDown_Weight.Size = new System.Drawing.Size(45, 26);
            this.numericUpDown_Weight.TabIndex = 103;
            this.numericUpDown_Weight.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(95, 213);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 19);
            this.label2.TabIndex = 96;
            this.label2.Text = "Tracker:";
            this.label2.Visible = false;
            // 
            // linkLabel_RandomSet
            // 
            this.linkLabel_RandomSet.ActiveLinkColor = System.Drawing.Color.Silver;
            this.linkLabel_RandomSet.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_RandomSet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_RandomSet.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_RandomSet.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_RandomSet.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_RandomSet.Location = new System.Drawing.Point(314, 99);
            this.linkLabel_RandomSet.Name = "linkLabel_RandomSet";
            this.linkLabel_RandomSet.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_RandomSet.TabIndex = 104;
            this.linkLabel_RandomSet.TabStop = true;
            this.linkLabel_RandomSet.Text = "SET";
            this.linkLabel_RandomSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_RandomSet.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_RandomSet.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_RandomSet_LinkClicked);
            // 
            // label_Tracker
            // 
            this.label_Tracker.AutoSize = true;
            this.label_Tracker.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Tracker.Location = new System.Drawing.Point(236, 213);
            this.label_Tracker.Name = "label_Tracker";
            this.label_Tracker.Size = new System.Drawing.Size(17, 19);
            this.label_Tracker.TabIndex = 108;
            this.label_Tracker.Text = "0";
            this.label_Tracker.Visible = false;
            // 
            // linkLabel_WeightSet
            // 
            this.linkLabel_WeightSet.ActiveLinkColor = System.Drawing.Color.Silver;
            this.linkLabel_WeightSet.BackColor = System.Drawing.Color.Gainsboro;
            this.linkLabel_WeightSet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel_WeightSet.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel_WeightSet.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel_WeightSet.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_WeightSet.Location = new System.Drawing.Point(314, 127);
            this.linkLabel_WeightSet.Name = "linkLabel_WeightSet";
            this.linkLabel_WeightSet.Size = new System.Drawing.Size(65, 23);
            this.linkLabel_WeightSet.TabIndex = 105;
            this.linkLabel_WeightSet.TabStop = true;
            this.linkLabel_WeightSet.Text = "SET";
            this.linkLabel_WeightSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel_WeightSet.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_WeightSet.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_WeightSet_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(95, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 19);
            this.label3.TabIndex = 97;
            this.label3.Text = "Vehicle Counts:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Controls.Add(this.comboBox_Station);
            this.panel3.Controls.Add(this.button_Refresh);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(221, 327);
            this.panel3.TabIndex = 116;
            // 
            // comboBox_Station
            // 
            this.comboBox_Station.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboBox_Station.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Station.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_Station.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboBox_Station.FormattingEnabled = true;
            this.comboBox_Station.Location = new System.Drawing.Point(18, 11);
            this.comboBox_Station.Name = "comboBox_Station";
            this.comboBox_Station.Size = new System.Drawing.Size(177, 26);
            this.comboBox_Station.TabIndex = 112;
            this.comboBox_Station.TabStop = false;
            // 
            // button_Refresh
            // 
            this.button_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Refresh.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Refresh.Location = new System.Drawing.Point(21, 50);
            this.button_Refresh.Name = "button_Refresh";
            this.button_Refresh.Size = new System.Drawing.Size(67, 25);
            this.button_Refresh.TabIndex = 106;
            this.button_Refresh.TabStop = false;
            this.button_Refresh.Text = "Get Data";
            this.button_Refresh.UseVisualStyleBackColor = true;
            this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(254, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 15);
            this.label6.TabIndex = 113;
            this.label6.Text = "Status:";
            // 
            // label_Status
            // 
            this.label_Status.BackColor = System.Drawing.Color.Transparent;
            this.label_Status.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Status.ForeColor = System.Drawing.Color.DimGray;
            this.label_Status.Location = new System.Drawing.Point(308, 42);
            this.label_Status.Name = "label_Status";
            this.label_Status.Size = new System.Drawing.Size(332, 20);
            this.label_Status.TabIndex = 112;
            this.label_Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button_Close
            // 
            this.button_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Close.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Close.Location = new System.Drawing.Point(622, 9);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(65, 25);
            this.button_Close.TabIndex = 98;
            this.button_Close.TabStop = false;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(166, 19);
            this.label8.TabIndex = 98;
            this.label8.Text = "WIM STAION CONTROL";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel2.Controls.Add(this.button_Close);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(710, 44);
            this.panel2.TabIndex = 112;
            // 
            // Form_WIMControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(710, 371);
            this.ControlBox = false;
            this.Controls.Add(this.panel_main);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_WIMControl";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WIM Station";
            this.panel_main.ResumeLayout(false);
            this.panel_main.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Random)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Weight)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_main;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel_OFF;
        private System.Windows.Forms.LinkLabel linkLabel_RESETCount;
        private System.Windows.Forms.LinkLabel linkLabel_SORT;
        private System.Windows.Forms.LinkLabel linkLabel_RESETTracker;
        private System.Windows.Forms.LinkLabel linkLabel_REPORT;
        private System.Windows.Forms.LinkLabel linkLabel_BYPASS;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkLabel_WeightSet;
        private System.Windows.Forms.LinkLabel linkLabel_RandomSet;
        private System.Windows.Forms.NumericUpDown numericUpDown_Weight;
        private System.Windows.Forms.NumericUpDown numericUpDown_Random;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_VehicleCount;
        private System.Windows.Forms.Label label_Tracker;
        private System.Windows.Forms.Button button_Refresh;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label_Status;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label_StationName;
        private System.Windows.Forms.ComboBox comboBox_Station;
    }
}