﻿using doh.vehicleview.service.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.Mvc;
using VehicleViewLIB;


namespace doh.vehicleview.service.Controllers
{
    public class WimServiceController : Controller
    {
        int MAXROW = 10;
        string a_wim_1 = "WIM-1";
        string a_wim_2 = "WIM-2";
        string a_report = "REPORT";
        string a_repbyp = "REPORT-BYPASS";
        string a_bypass = "BYPASS";
        string a_static = "STATIC";
        string a_data = "DATA";


        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(WimServiceController));
        //
        // GET: /WimService/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetStation()
        {
            List<StationModel> data = GetStationList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        ////http://localhost:64708/WimService/GetWimByLane?_lane=1&_stationId=1
        public ActionResult GetWimData(string top)
        {
            //DataFilters df = new DataFilters();
            //df.stationID = _stationId;
            //df.top = MAXROW.ToString();
            //df.lane = _lane;
            List<WimModel> data = Create_List_Items_WIM(Convert.ToInt16(top));


            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetLogin(string user, string pass)
        {
            List<UserModel> data = checkUser(user, pass);

            return Json(data, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetWimData_TEST()
        {

            List<WimModel> data = Create_List_Items_WIM_Test();


            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #region Custom Function


        List<StationModel> GetStationList()
        {
            List<StationModel> items = new List<StationModel>();
            DataTable table = null;
            try
            {

                table = Utils.GetDataTableNormal("SELECT * from Station Where StationID>0");
                foreach (DataRow row in table.Rows)
                {
                    StationModel item = new StationModel();
                    item.StationID = (int)row["StationID"];
                    item.StationName = (string)row["StationName"] + "";
                    //item.StationDescription = (string)row["StationDescription"] + "";
                    //item.LocationDescription = (string)row["LocationDescription"] + "";
                    //item.ProvinceID = (int)row["ProvinceID"];
                    //item.Latitude = (double)row["Latitude"];
                    //item.Longtitude = (double)row["Longtitude"];

                    items.Add(item);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return items;
        }
         List<StationModel> GetStation101List()
        {
            List<StationModel> items = new List<StationModel>();
            DataTable table = null;
            try
            {
                table = Utils.GetDataTableNormal("SELECT * FROM [LINK_DB101].WIMBASE_00.[dbo].Station  Where StationID>0");
                foreach (DataRow row in table.Rows)
                {
                    StationModel item = new StationModel();
                    item.StationID = (int)row["StationID"];
                    item.StationName = (string)row["StationName"] + "";
                    //item.StationDescription = (string)row["StationDescription"] + "";
                    //item.LocationDescription = (string)row["LocationDescription"] + "";
                    //item.ProvinceID = (int)row["ProvinceID"];
                    //item.Latitude = (double)row["Latitude"];
                    //item.Longtitude = (double)row["Longtitude"];

                    items.Add(item);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return items;
        }
         StationModel GetStation101ByStationID(int stationId)
         {
             StationModel item = new StationModel();
             DataTable table = null;
             try
             {
                 table = Utils.GetDataTableNormal("SELECT StationID,StationName FROM [LINK_DB101].WIMBASE_00.[dbo].Station  Where StationID=" + stationId);
                 foreach (DataRow row in table.Rows)
                 {

                     item.StationID = (int)row["StationID"];
                     item.StationName = (string)row["StationName"] + "";

                 }
             }
             catch (Exception ex)
             {
                 logger.Error(ex.Message);
             }
             return item;
         }
        List<StationModel> GetStationListByPermission(int userId)
        {
            List<StationModel> items = new List<StationModel>();
            DataTable table = null;
            try
            {


                table = Utils.GetDataTableNormal("SELECT ug.StationGroupId,ug.StationId,(CASE ug.StationGroupId WHEN 1 THEN  (select StationName from dbo.Station where StationID=ug.StationId)END) StationName From Users u inner join UsersForStationGroup ug on u.UserID = ug.UserID Where u.UserID= " + userId + " Order by StationGroupID ASC");
                 
                foreach (DataRow row in table.Rows)
                {
                    StationModel item = new StationModel();
                    item.StationID = (int)row["StationID"];
                    item.StationGroupID = Convert.ToInt32(row["StationGroupID"]);
                    
                    switch (item.StationGroupID)
                    {
                        case 1:
                            item.StationName = (string)row["StationName"] + "";
                            break;
                        case 2:
                            item.StationName = GetStation101ByStationID(item.StationID).StationName;
                            break;
                    }
                    items.Add(item);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return items;
        }
        List<WimModel> Create_List_Items_WIM(int top)
        {
            List<WimModel> items = new List<WimModel>();

            int size = GetStationList().Count;
            string sql = "Select * from(";
            for (int i = 0; i < size; i++)
            {
                sql += "SELECT top " + top + " 1 as StationGroupID,StationName, ProvinceName, WIM.* ";
                sql += "FROM dbo.WIM INNER JOIN ";
                sql += "dbo.Station ON dbo.WIM.StationID = dbo.Station.StationID ";
                sql += "INNER JOIN ";
                sql += "dbo.Province ON dbo.WIM.LicensePlateProvinceID = dbo.Province.ProvinceID ";
                sql += " Where WIM.StationID=" + (i) + " ORDER BY TimeStamp DESC";
                if (i < (size - 1))
                {
                    sql += " UNION ";
                }
            }
            sql += ") x ORDER BY TimeStamp DESC";


            DataTable table = null;


            table = Utils.GetDataTableNormal(sql);
            foreach (DataRow row in table.Rows)
            {
                try
                {
                    WimModel item = new WimModel();

                    item.wimid = row["WIMID"].ToString();
                    item.RefID = VehicleViewLIB.ViewType.WIM.ToString() + "," + ((long)row["WIMID"]).ToString();
                    item.StationName = ("") + (string)row["StationName"] + "";
                    item.StationId = row["StationID"].ToString();
                    item.VehicleNumber = ("(") + ((int)row["VehicleNumber"]).ToString() + ")";
                    item.Lane = ((byte)row["Lane"] + 1).ToString() + "";
                    item.LaneId = ((byte)row["Lane"] + 1).ToString() + "";
                    item.VehicleClass = ((byte)row["VehicleClass"]).ToString() + "";
                    item.TimeStamp = ((DateTime)row["TimeStamp"]).ToString("dd-MM-yyyy HH:mm:ss") + "";
                    item.SortDecision = ((VRSortDecisionCode)((byte)row["SortDecision"])).ToString() + "";
                    item.MaxGvw = ((int)row["MAxGVW"]).ToString("N0") + " Kg" + "";
                    item.Gvw = ((int)row["GVW"]).ToString("N0") + " Kg" + "";
                    item.LicensePlateNumber = row["LicensePlateNumber"].ToString() + "";
                    item.ProvinceName = row["ProvinceName"].ToString() + "";

                    VRStatusCode statusCode = (VRStatusCode)((int)row["StatusCode"]);
                    VRErrorCode errorCode = (VRErrorCode)((byte)row["Error"]);
                    VRSortDecisionCode decisionCode = (VRSortDecisionCode)((byte)row["SortDecision"]);

                    item.statusCode = (statusCode == VRStatusCode.StatusClear) ? "" : Utils.GetStatusCodeText(statusCode) + "";
                    item.errorCode = (errorCode == VRErrorCode.None) ? "" : errorCode.ToString() + "";
                    item.StatusColor = Utils.GetSortDecisionColorCode(decisionCode).ToString();





                    item.Speed = row["Speed"].ToString() + "";
                    item.ESAL = row["ESAL"].ToString() + "";
                    item.AxleCount = row["AxleCount"].ToString() + "";
                    item.Length = row["Length"].ToString() + "";
                    item.FrontOverHang = row["FrontOverHang"].ToString() + "";
                    item.RearOverHang = row["RearOverHang"].ToString() + "";
                    /*
                     Seperation,Group,Weight
                     */
                    String axleName = "Seperation";
                    item.SeperationCol_1 = Utils.customNumberFormat((row["Axle01" + axleName]).ToString());
                    item.SeperationCol_2 = Utils.customNumberFormat((row["Axle02" + axleName]).ToString());
                    item.SeperationCol_3 = Utils.customNumberFormat((row["Axle03" + axleName]).ToString());
                    item.SeperationCol_4 = Utils.customNumberFormat((row["Axle04" + axleName]).ToString());
                    item.SeperationCol_5 = Utils.customNumberFormat((row["Axle05" + axleName]).ToString());
                    item.SeperationCol_6 = Utils.customNumberFormat((row["Axle06" + axleName]).ToString());
                    item.SeperationCol_7 = Utils.customNumberFormat((row["Axle07" + axleName]).ToString());
                    //item.SeperationCol_8 = ((short)row["Axle08" + axleName]).ToString();




                    axleName = "Group";
                    item.GroupCol_1 = Utils.customNumberFormat((row["Axle01" + axleName]).ToString());
                    item.GroupCol_2 = Utils.customNumberFormat((row["Axle02" + axleName]).ToString());
                    item.GroupCol_3 = Utils.customNumberFormat((row["Axle03" + axleName]).ToString());
                    item.GroupCol_4 = Utils.customNumberFormat((row["Axle04" + axleName]).ToString());
                    item.GroupCol_5 = Utils.customNumberFormat((row["Axle05" + axleName]).ToString());
                    item.GroupCol_6 = Utils.customNumberFormat((row["Axle06" + axleName]).ToString());
                    item.GroupCol_7 = Utils.customNumberFormat((row["Axle07" + axleName]).ToString());
                    //item.GroupCol_8 = (row["Axle08" + axleName]).ToString();

                    axleName = "Weight";
                    item.WeightCol_1 = Utils.customNumberFormat(((short)row["Axle01" + axleName]).ToString());
                    item.WeightCol_2 = Utils.customNumberFormat(((short)row["Axle02" + axleName]).ToString());
                    item.WeightCol_3 = Utils.customNumberFormat(((short)row["Axle03" + axleName]).ToString());
                    item.WeightCol_4 = Utils.customNumberFormat(((short)row["Axle04" + axleName]).ToString());
                    item.WeightCol_5 = Utils.customNumberFormat(((short)row["Axle05" + axleName]).ToString());
                    item.WeightCol_6 = Utils.customNumberFormat(((short)row["Axle06" + axleName]).ToString());
                    item.WeightCol_7 = Utils.customNumberFormat(((short)row["Axle07" + axleName]).ToString());

                    item.StationGroupID = Convert.ToInt16(row["StationGroupID"].ToString());

                    string imgName = (string)row["Image01Name"];
                    if (!imgName.Equals(""))
                    {
                        string imgUrl = Utils.GetImageURL(imgName);
                        item.imgpath = imgUrl;
                        imgUrl = Utils.GetImageURL(Path.GetFileNameWithoutExtension(imgName).Substring(0, Path.GetFileNameWithoutExtension(imgName).Length - 1) + "9" + Path.GetExtension(imgName));
                        item.imgPath2 = imgUrl;
                    }
                    else
                    {
                        item.imgpath = @"http://61.19.97.185/wim/images/inf.jpg";
                        item.imgPath2 = @"http://61.19.97.185/wim/images/inf.jpg";
                    }



                    //item.WeightCol_8 = ((short)row["Axle08" + axleName]).ToString();
                    items.Add(item);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
            }
            ////////
            sql = "Select * from(";
            size = GetStation101List().Count;
            for (int i = 0; i < size; i++)
            {
                sql += " SELECT top " + top + " 2 as StationGroupID,StationName, ProvinceName, w.* ";
                sql += "FROM [LINK_DB101].WIMBASE_00.dbo.WIM w INNER JOIN ";
                sql += "[LINK_DB101].WIMBASE_00.dbo.Station s ON w.StationID = s.StationID ";
                sql += "INNER JOIN ";
                sql += "[LINK_DB101].WIMBASE_00.dbo.Province p ON w.LicensePlateProvinceID = p.ProvinceID ";
                sql += " Where w.StationID=" + (i) + " ORDER BY TimeStamp DESC";
                if (i < (size - 1))
                {
                    sql += " UNION ";
                }
            }
            sql += ") x ORDER BY TimeStamp DESC";


            table = Utils.GetDataTableNormal(sql);
            foreach (DataRow row in table.Rows)
            {
                try
                {
                    WimModel item = new WimModel();

                    item.wimid = row["WIMID"].ToString();
                    item.RefID = VehicleViewLIB.ViewType.WIM.ToString() + "," + ((long)row["WIMID"]).ToString();
                    item.StationName = ("") + (string)row["StationName"] + "";
                    item.StationId = row["StationID"].ToString();
                    item.VehicleNumber = ("(") + ((int)row["VehicleNumber"]).ToString() + ")";
                    item.Lane = ((byte)row["Lane"] + 1).ToString() + "";
                    item.LaneId = ((byte)row["Lane"] + 1).ToString() + "";
                    item.VehicleClass = ((byte)row["VehicleClass"]).ToString() + "";
                    item.TimeStamp = ((DateTime)row["TimeStamp"]).ToString("dd-MM-yyyy HH:mm:ss") + "";
                    item.SortDecision = ((VRSortDecisionCode)((byte)row["SortDecision"])).ToString() + "";
                    item.MaxGvw = ((int)row["MAxGVW"]).ToString("N0") + " Kg" + "";
                    item.Gvw = ((int)row["GVW"]).ToString("N0") + " Kg" + "";
                    item.LicensePlateNumber = row["LicensePlateNumber"].ToString() + "";
                    item.ProvinceName = row["ProvinceName"].ToString() + "";

                    VRStatusCode statusCode = (VRStatusCode)((int)row["StatusCode"]);
                    VRErrorCode errorCode = (VRErrorCode)((byte)row["Error"]);
                    VRSortDecisionCode decisionCode = (VRSortDecisionCode)((byte)row["SortDecision"]);

                    item.statusCode = (statusCode == VRStatusCode.StatusClear) ? "" : Utils.GetStatusCodeText(statusCode) + "";
                    item.errorCode = (errorCode == VRErrorCode.None) ? "" : errorCode.ToString() + "";
                    item.StatusColor = Utils.GetSortDecisionColorCode(decisionCode).ToString();






                    item.Speed = row["Speed"].ToString() + "";
                    item.ESAL = row["ESAL"].ToString() + "";
                    item.AxleCount = row["AxleCount"].ToString() + "";
                    item.Length = row["Length"].ToString() + "";
                    item.FrontOverHang = row["FrontOverHang"].ToString() + "";
                    item.RearOverHang = row["RearOverHang"].ToString() + "";
                    /*
                     Seperation,Group,Weight
                     */
                    String axleName = "Seperation";
                    item.SeperationCol_1 = Utils.customNumberFormat((row["Axle01" + axleName]).ToString());
                    item.SeperationCol_2 = Utils.customNumberFormat((row["Axle02" + axleName]).ToString());
                    item.SeperationCol_3 = Utils.customNumberFormat((row["Axle03" + axleName]).ToString());
                    item.SeperationCol_4 = Utils.customNumberFormat((row["Axle04" + axleName]).ToString());
                    item.SeperationCol_5 = Utils.customNumberFormat((row["Axle05" + axleName]).ToString());
                    item.SeperationCol_6 = Utils.customNumberFormat((row["Axle06" + axleName]).ToString());
                    item.SeperationCol_7 = Utils.customNumberFormat((row["Axle07" + axleName]).ToString());
                    //item.SeperationCol_8 = ((short)row["Axle08" + axleName]).ToString();




                    axleName = "Group";
                    item.GroupCol_1 = Utils.customNumberFormat((row["Axle01" + axleName]).ToString());
                    item.GroupCol_2 = Utils.customNumberFormat((row["Axle02" + axleName]).ToString());
                    item.GroupCol_3 = Utils.customNumberFormat((row["Axle03" + axleName]).ToString());
                    item.GroupCol_4 = Utils.customNumberFormat((row["Axle04" + axleName]).ToString());
                    item.GroupCol_5 = Utils.customNumberFormat((row["Axle05" + axleName]).ToString());
                    item.GroupCol_6 = Utils.customNumberFormat((row["Axle06" + axleName]).ToString());
                    item.GroupCol_7 = Utils.customNumberFormat((row["Axle07" + axleName]).ToString());
                    //item.GroupCol_8 = (row["Axle08" + axleName]).ToString();

                    axleName = "Weight";
                    item.WeightCol_1 = Utils.customNumberFormat(((short)row["Axle01" + axleName]).ToString());
                    item.WeightCol_2 = Utils.customNumberFormat(((short)row["Axle02" + axleName]).ToString());
                    item.WeightCol_3 = Utils.customNumberFormat(((short)row["Axle03" + axleName]).ToString());
                    item.WeightCol_4 = Utils.customNumberFormat(((short)row["Axle04" + axleName]).ToString());
                    item.WeightCol_5 = Utils.customNumberFormat(((short)row["Axle05" + axleName]).ToString());
                    item.WeightCol_6 = Utils.customNumberFormat(((short)row["Axle06" + axleName]).ToString());
                    item.WeightCol_7 = Utils.customNumberFormat(((short)row["Axle07" + axleName]).ToString());

                    item.StationGroupID = Convert.ToInt16(row["StationGroupID"].ToString());
                    string imgName = (string)row["Image01Name"];
                    if (!imgName.Equals(""))
                    {
                        string imgUrl = Utils.GetImageURL_2(imgName);
                        item.imgpath = imgUrl;
                        imgUrl = Utils.GetImageURL_2(Path.GetFileNameWithoutExtension(imgName).Substring(0, Path.GetFileNameWithoutExtension(imgName).Length - 1) + "9" + Path.GetExtension(imgName));
                        item.imgPath2 = imgUrl;
                    }
                    else
                    {
                        item.imgpath = @"http://61.19.97.185/wim/images/inf.jpg";
                        item.imgPath2 = @"http://61.19.97.185/wim/images/inf.jpg";
                    }
                    items.Add(item);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
            }


            return items;
        }



        List<WimModel> Create_List_Items_WIM_Test()
        {
            List<WimModel> items = new List<WimModel>();
            try
            {
                for (int i = 0; i < 15; i++)
                {

                    WimModel item = new WimModel();
                    item.RefID = VehicleViewLIB.ViewType.WIM.ToString() + ",220100" + (i).ToString();
                    item.StationName = ("") + (string)"นครปฐม";
                    item.VehicleNumber = "(60233)";
                    item.Lane = "Lane: 1";
                    item.VehicleClass = "Class: 5";
                    item.TimeStamp = "11-09-2014 11:02:38";
                    item.SortDecision = "Sort: Off";
                    item.MaxGvw = "Max: 25,000 Kg";
                    item.Gvw = "GVW: 14,274 Kg";
                    item.LicensePlateNumber = "กง8031";
                    item.ProvinceName = "ตรัง";
                    item.statusCode = "SpeedChange";
                    item.errorCode = "";
                    item.StatusColor = "Color[Silver]";

                    string imgName = "wim-02-20140911-105142-60107-0.jpg";
                    string imgUrl = Utils.GetImageURL(imgName);

                    item.imgpath = imgUrl;
                    items.Add(item);

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return items;
        }


        List<UserModel> checkUser(String user, String pass)
        {
            
            string sql = "";
            sql += "SELECT UserID,LoginID,Password From Users Where LoginID = '" + user + "' And Password ='" + pass + "'";


            //UsersForStationGroup
            List<UserModel> items = new List<UserModel>();
            DataTable table = null;
            try
            {

                table = Utils.GetDataTableNormal(sql);
                foreach (DataRow row in table.Rows)
                {

                    UserModel item = new UserModel();

                    item.UserID = Convert.ToInt32(row["UserID"].ToString());
                    item.FirstName = row["UserID"].ToString();
                    item.LastName = row["UserID"].ToString();
                    item.LoginID = row["LoginID"].ToString();
                    item.Title = row["UserID"].ToString();
                    item.Password = row["Password"].ToString();
                    item.stations = GetStationListByPermission(item.UserID);
                    items.Add(item);

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
           
            return items;
        }
        #endregion
    }
}
