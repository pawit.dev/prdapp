﻿using System;
using System.Collections.Generic;

namespace doh.vehicleview.service.Models
{
    public class UserModel : UserGroupModel
    {
        public int UserID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Title { get; set; }
        public String LoginID { get; set; }
        public String Password { get; set; }
        public int UserGroupID { get; set; }
        public int ActiveD { get; set; }
        public String MonitorStation { get; set; }
        public String stationGroupId { get; set; }
        public List<StationModel> stations { get; set; }
    }
}