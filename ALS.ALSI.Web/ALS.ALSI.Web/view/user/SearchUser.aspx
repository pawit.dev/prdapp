﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SearchUser.aspx.cs" Inherits="ALS.ALSI.Web.view.user.SearchUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            App.init();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form2" method="post" runat="server" class="form-horizontal">
        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            You have some form errors. Please check below.
        </div>
        <div class="alert alert-success hide">
            <button class="close" data-dismiss="alert"></button>
            Your form validation is successful!
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet">
                    <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Search Condition</h4>
                        <div class="actions">
                            <div class="btn-group">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn mini yellow" OnClick="btnSearch_Click1"><i class="icon-search"></i> Search</asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" class="btn mini black" OnClick="btnCancel_Click"><i class="icon-refresh"></i> Cancel</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtUserName">User Name:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtUserName" runat="server" class="m-wrap span6"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtName">Phone:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtPhone" runat="server" class="m-wrap span6"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <h4><i class="icon-bar-chart"></i>Search Result</h4>
                        <div class="actions">
                            <div class="btn-group">
                                <asp:LinkButton ID="btnAdd" runat="server" class="btn mini blue" OnClick="lbAdd_Click"><i class="icon-pencil"></i> Add</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <asp:Label ID="lbTotalRecords" runat="server" Text=""></asp:Label>
                        <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            CssClass="table table-striped table-hover table-bordered" ShowHeaderWhenEmpty="True" DataKeyNames="id" OnRowCommand="gvResult_RowCommand" OnRowDeleting="gvResult_RowDeleting" OnPageIndexChanging="gvResult_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Role" DataField="role" ItemStyle-HorizontalAlign="Left" SortExpression="role" />
                                <asp:BoundField HeaderText="Username" DataField="username" ItemStyle-HorizontalAlign="Left" SortExpression="username" />
                                <asp:BoundField HeaderText="Email" DataField="email" ItemStyle-HorizontalAlign="Left" SortExpression="email" />
                                <asp:BoundField HeaderText="Last login" DataField="latest_login" ItemStyle-HorizontalAlign="Left" SortExpression="latest_login" DataFormatString="{0:dd-MM-yyyy HH:mm:ss}" />
                                <asp:BoundField HeaderText="Create date" DataField="create_date" ItemStyle-HorizontalAlign="Left" SortExpression="create_date" DataFormatString="{0:dd-MM-yyyy}" />
                                <asp:BoundField HeaderText="Status" DataField="status" ItemStyle-HorizontalAlign="Left" SortExpression="status" />

                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("id")%>'><i class="icon-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                            CommandArgument='<%# Eval("id")%>'><i class="icon-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <div class="pagination">
                                    <ul>
                                        <li>
                                            <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                        </li>
                                        <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                        <li>
                                            <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                <div class="data-not-found">
                                    <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
