﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

namespace ALS.ALSI.Web.view.template
{
    public partial class Seagate_HPA_Filtration_Rev_BE_1_4_WorkingSheet : System.Web.UI.UserControl
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Seagate_DHS_Component_Rev_BE_1_5_WorkingSheet));

        #region "Property"

        public List<template_seagate_hpa_coverpage> Hpas
        {
            get { return (List<template_seagate_hpa_coverpage>)Session[GetType().Name + "Hpas"]; }
            set { Session[GetType().Name + "Hpas"] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            //Session.Remove(GetType().Name + "tbCas");
            //Session.Remove(GetType().Name + "coverpages");
            Session.Remove(GetType().Name + "Hpas");
            Session.Remove(GetType().Name + Constants.PREVIOUS_PATH);
            Session.Remove(GetType().Name + "SampleID");
        }

        private void initialPage()
        {
            this.Hpas = template_seagate_hpa_coverpage.FindAllBySampleID(this.SampleID);

            #region "US-LPC(0.3)"
            template_seagate_hpa_coverpage khz68_03 = Hpas.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString());
            if (khz68_03 != null)
            {
                txt_UsLPC03_B14.Text = khz68_03.us_b14;
                txt_UsLPC03_B15.Text = khz68_03.us_b15;
                txt_UsLPC03_B16.Text = khz68_03.us_b16;
                txt_UsLPC03_B17.Text = khz68_03.us_b17;

                txt_UsLPC03_C14.Text = khz68_03.us_c14;
                txt_UsLPC03_C15.Text = khz68_03.us_c15;
                txt_UsLPC03_C16.Text = khz68_03.us_c16;
                txt_UsLPC03_C17.Text = khz68_03.us_c17;

                txt_UsLPC03_D14.Text = khz68_03.us_d14;
                txt_UsLPC03_D15.Text = khz68_03.us_d15;
                txt_UsLPC03_D16.Text = khz68_03.us_d16;
                txt_UsLPC03_D17.Text = khz68_03.us_d17;

                txt_UsLPC03_E14.Text = khz68_03.us_e14;
                txt_UsLPC03_E15.Text = khz68_03.us_e15;
                txt_UsLPC03_E16.Text = khz68_03.us_e16;
                txt_UsLPC03_E17.Text = khz68_03.us_e17;

                txt_UsLPC03_F14.Text = khz68_03.us_f14;
                txt_UsLPC03_F15.Text = khz68_03.us_f15;
                txt_UsLPC03_F16.Text = khz68_03.us_f16;
                txt_UsLPC03_F17.Text = khz68_03.us_f17;

                txt_UsLPC03_G14.Text = khz68_03.us_g14;
                txt_UsLPC03_G15.Text = khz68_03.us_g15;
                txt_UsLPC03_G16.Text = khz68_03.us_g16;
                txt_UsLPC03_G17.Text = khz68_03.us_g17;

                txt_UsLPC03_B20.Text = Hpas[0].ws_b3;
                txt_UsLPC03_B21.Text = Hpas[0].ws_b9;
                txt_UsLPC03_B22.Text = Hpas[0].ws_b5;

                #region "Worksheet for HPA - Filtration"

                txtB3.Text = khz68_03.ws_b3;
                txtB4.Text = khz68_03.ws_b4;
                txtB5.Text = khz68_03.ws_b5;
                txtB6.Text = khz68_03.ws_b6;
                txtB7.Text = khz68_03.ws_b7;
                txtB8.Text = khz68_03.ws_b8;
                txtB9.Text = khz68_03.ws_b9;

                #region "B"
                txt_WsB13.Text = khz68_03.ws_b13;
                txt_WsB14.Text = khz68_03.ws_b14;
                txt_WsB15.Text = khz68_03.ws_b15;
                txt_WsB16.Text = khz68_03.ws_b16;
                txt_WsB17.Text = khz68_03.ws_b17;
                txt_WsB18.Text = khz68_03.ws_b18;
                txt_WsB19.Text = khz68_03.ws_b19;
                txt_WsB20.Text = khz68_03.ws_b20;
                txt_WsB21.Text = khz68_03.ws_b21;
                txt_WsB22.Text = khz68_03.ws_b22;
                txt_WsB23.Text = khz68_03.ws_b23;
                txt_WsB24.Text = khz68_03.ws_b24;
                txt_WsB25.Text = khz68_03.ws_b25; //
                txt_WsB26.Text = khz68_03.ws_b26;
                txt_WsB27.Text = khz68_03.ws_b27;
                txt_WsB28.Text = khz68_03.ws_b28;
                txt_WsB29.Text = khz68_03.ws_b29; //
                txt_WsB30.Text = khz68_03.ws_b30;
                txt_WsB31.Text = khz68_03.ws_b31;
                txt_WsB32.Text = khz68_03.ws_b32;
                txt_WsB33.Text = khz68_03.ws_b33; //
                txt_WsB34.Text = khz68_03.ws_b34;
                txt_WsB35.Text = khz68_03.ws_b35;
                txt_WsB36.Text = khz68_03.ws_b36; //
                txt_WsB37.Text = khz68_03.ws_b37;
                txt_WsB38.Text = khz68_03.ws_b38;
                txt_WsB39.Text = khz68_03.ws_b39;
                txt_WsB40.Text = khz68_03.ws_b40;
                txt_WsB41.Text = khz68_03.ws_b41;
                txt_WsB43.Text = khz68_03.ws_b43; //
                txt_WsB42.Text = khz68_03.ws_b42;
                txt_WsB44.Text = khz68_03.ws_b44;
                txt_WsB45.Text = khz68_03.ws_b45;
                txt_WsB46.Text = khz68_03.ws_b46;
                txt_WsB47.Text = khz68_03.ws_b47; //
                txt_WsB48.Text = khz68_03.ws_b48;
                txt_WsB49.Text = khz68_03.ws_b49;
                txt_WsB50.Text = khz68_03.ws_b50;
                txt_WsB51.Text = khz68_03.ws_b51;
                txt_WsB52.Text = khz68_03.ws_b52;
                txt_WsB53.Text = khz68_03.ws_b53;
                txt_WsB54.Text = khz68_03.ws_b54;
                txt_WsB55.Text = khz68_03.ws_b55;
                txt_WsB56.Text = khz68_03.ws_b56;
                txt_WsB57.Text = khz68_03.ws_b57; //
                txt_WsB58.Text = khz68_03.ws_b58;
                txt_WsB59.Text = khz68_03.ws_b59;
                txt_WsB60.Text = khz68_03.ws_b60;
                txt_WsB61.Text = khz68_03.ws_b61;
                txt_WsB62.Text = khz68_03.ws_b62;
                txt_WsB63.Text = khz68_03.ws_b63;
                txt_WsB64.Text = khz68_03.ws_b64;
                txt_WsB65.Text = khz68_03.ws_b65; //
                txt_WsB66.Text = khz68_03.ws_b66;
                txt_WsB67.Text = khz68_03.ws_b67;
                txt_WsB68.Text = khz68_03.ws_b68;
                txt_WsB69.Text = khz68_03.ws_b69;
                txt_WsB70.Text = khz68_03.ws_b70;
                txt_WsB71.Text = khz68_03.ws_b71;
                txt_WsB72.Text = khz68_03.ws_b72; //
                txt_WsB73.Text = khz68_03.ws_b73;
                txt_WsB74.Text = khz68_03.ws_b74;
                txt_WsB75.Text = khz68_03.ws_b75; //
                txt_WsB76.Text = khz68_03.ws_b76; //

                #endregion
                #region "C"
                txt_WsC13.Text = khz68_03.ws_c13;
                txt_WsC14.Text = khz68_03.ws_c14;
                txt_WsC15.Text = khz68_03.ws_c15;
                txt_WsC16.Text = khz68_03.ws_c16;
                txt_WsC17.Text = khz68_03.ws_c17;
                txt_WsC18.Text = khz68_03.ws_c18;
                txt_WsC19.Text = khz68_03.ws_c19;
                txt_WsC20.Text = khz68_03.ws_c20;
                txt_WsC21.Text = khz68_03.ws_c21;
                txt_WsC22.Text = khz68_03.ws_c22;
                txt_WsC23.Text = khz68_03.ws_c23;
                txt_WsC24.Text = khz68_03.ws_c24;
                txt_WsC25.Text = khz68_03.ws_c25; //
                txt_WsC26.Text = khz68_03.ws_c26;
                txt_WsC27.Text = khz68_03.ws_c27;
                txt_WsC28.Text = khz68_03.ws_c28;
                txt_WsC29.Text = khz68_03.ws_c29; //
                txt_WsC30.Text = khz68_03.ws_c30;
                txt_WsC31.Text = khz68_03.ws_c31;
                txt_WsC32.Text = khz68_03.ws_c32;
                txt_WsC33.Text = khz68_03.ws_c33; //
                txt_WsC34.Text = khz68_03.ws_c34;
                txt_WsC35.Text = khz68_03.ws_c35;
                txt_WsC36.Text = khz68_03.ws_c36; //
                txt_WsC37.Text = khz68_03.ws_c37;
                txt_WsC38.Text = khz68_03.ws_c38;
                txt_WsC39.Text = khz68_03.ws_c39;
                txt_WsC40.Text = khz68_03.ws_c40;
                txt_WsC41.Text = khz68_03.ws_c41;
                txt_WsC43.Text = khz68_03.ws_c43; //
                txt_WsC42.Text = khz68_03.ws_c42;
                txt_WsC44.Text = khz68_03.ws_c44;
                txt_WsC45.Text = khz68_03.ws_c45;
                txt_WsC46.Text = khz68_03.ws_c46;
                txt_WsC47.Text = khz68_03.ws_c47; //
                txt_WsC48.Text = khz68_03.ws_c48;
                txt_WsC49.Text = khz68_03.ws_c49;
                txt_WsC50.Text = khz68_03.ws_c50;
                txt_WsC51.Text = khz68_03.ws_c51;
                txt_WsC52.Text = khz68_03.ws_c52;
                txt_WsC53.Text = khz68_03.ws_c53;
                txt_WsC54.Text = khz68_03.ws_c54;
                txt_WsC55.Text = khz68_03.ws_c55;
                txt_WsC56.Text = khz68_03.ws_c56;
                txt_WsC57.Text = khz68_03.ws_c57; //
                txt_WsC58.Text = khz68_03.ws_c58;
                txt_WsC59.Text = khz68_03.ws_c59;
                txt_WsC60.Text = khz68_03.ws_c60;
                txt_WsC61.Text = khz68_03.ws_c61;
                txt_WsC62.Text = khz68_03.ws_c62;
                txt_WsC63.Text = khz68_03.ws_c63;
                txt_WsC64.Text = khz68_03.ws_c64;
                txt_WsC65.Text = khz68_03.ws_c65; //
                txt_WsC66.Text = khz68_03.ws_c66;
                txt_WsC67.Text = khz68_03.ws_c67;
                txt_WsC68.Text = khz68_03.ws_c68;
                txt_WsC69.Text = khz68_03.ws_c69;
                txt_WsC70.Text = khz68_03.ws_c70;
                txt_WsC71.Text = khz68_03.ws_c71;
                txt_WsC72.Text = khz68_03.ws_c72; //
                txt_WsC73.Text = khz68_03.ws_c73;
                txt_WsC74.Text = khz68_03.ws_c74;
                txt_WsC75.Text = khz68_03.ws_c75; //
                txt_WsC76.Text = khz68_03.ws_c76; //

                #endregion
                #region "D"
                txt_WsD13.Text = khz68_03.ws_d13;
                txt_WsD14.Text = khz68_03.ws_d14;
                txt_WsD15.Text = khz68_03.ws_d15;
                txt_WsD16.Text = khz68_03.ws_d16;
                txt_WsD17.Text = khz68_03.ws_d17;
                txt_WsD18.Text = khz68_03.ws_d18;
                txt_WsD19.Text = khz68_03.ws_d19;
                txt_WsD20.Text = khz68_03.ws_d20;
                txt_WsD21.Text = khz68_03.ws_d21;
                txt_WsD22.Text = khz68_03.ws_d22;
                txt_WsD23.Text = khz68_03.ws_d23;
                txt_WsD24.Text = khz68_03.ws_d24;
                txt_WsD25.Text = khz68_03.ws_d25; //
                txt_WsD26.Text = khz68_03.ws_d26;
                txt_WsD27.Text = khz68_03.ws_d27;
                txt_WsD28.Text = khz68_03.ws_d28;
                txt_WsD29.Text = khz68_03.ws_d29; //
                txt_WsD30.Text = khz68_03.ws_d30;
                txt_WsD31.Text = khz68_03.ws_d31;
                txt_WsD32.Text = khz68_03.ws_d32;
                txt_WsD33.Text = khz68_03.ws_d33; //
                txt_WsD34.Text = khz68_03.ws_d34;
                txt_WsD35.Text = khz68_03.ws_d35;
                txt_WsD36.Text = khz68_03.ws_d36; //
                txt_WsD37.Text = khz68_03.ws_d37;
                txt_WsD38.Text = khz68_03.ws_d38;
                txt_WsD39.Text = khz68_03.ws_d39;
                txt_WsD40.Text = khz68_03.ws_d40;
                txt_WsD41.Text = khz68_03.ws_d41;
                txt_WsD43.Text = khz68_03.ws_d43; //
                txt_WsD42.Text = khz68_03.ws_d42;
                txt_WsD44.Text = khz68_03.ws_d44;
                txt_WsD45.Text = khz68_03.ws_d45;
                txt_WsD46.Text = khz68_03.ws_d46;
                txt_WsD47.Text = khz68_03.ws_d47; //
                txt_WsD48.Text = khz68_03.ws_d48;
                txt_WsD49.Text = khz68_03.ws_d49;
                txt_WsD50.Text = khz68_03.ws_d50;
                txt_WsD51.Text = khz68_03.ws_d51;
                txt_WsD52.Text = khz68_03.ws_d52;
                txt_WsD53.Text = khz68_03.ws_d53;
                txt_WsD54.Text = khz68_03.ws_d54;
                txt_WsD55.Text = khz68_03.ws_d55;
                txt_WsD56.Text = khz68_03.ws_d56;
                txt_WsD57.Text = khz68_03.ws_d57; //
                txt_WsD58.Text = khz68_03.ws_d58;
                txt_WsD59.Text = khz68_03.ws_d59;
                txt_WsD60.Text = khz68_03.ws_d60;
                txt_WsD61.Text = khz68_03.ws_d61;
                txt_WsD62.Text = khz68_03.ws_d62;
                txt_WsD63.Text = khz68_03.ws_d63;
                txt_WsD64.Text = khz68_03.ws_d64;
                txt_WsD65.Text = khz68_03.ws_d65; //
                txt_WsD66.Text = khz68_03.ws_d66;
                txt_WsD67.Text = khz68_03.ws_d67;
                txt_WsD68.Text = khz68_03.ws_d68;
                txt_WsD69.Text = khz68_03.ws_d69;
                txt_WsD70.Text = khz68_03.ws_d70;
                txt_WsD71.Text = khz68_03.ws_d71;
                txt_WsD72.Text = khz68_03.ws_d72; //
                txt_WsD73.Text = khz68_03.ws_d73;
                txt_WsD74.Text = khz68_03.ws_d74;
                txt_WsD75.Text = khz68_03.ws_d75; //
                txt_WsD76.Text = khz68_03.ws_d76; //
                #endregion
                #endregion
            }
            #endregion
            #region "US-LPC(0.6)"
            template_seagate_hpa_coverpage khz68_06 = Hpas.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString());
            if (khz68_06 != null)
            {
                txt_UsLPC06_B14.Text = khz68_06.us_b14;
                txt_UsLPC06_B15.Text = khz68_06.us_b15;
                txt_UsLPC06_B16.Text = khz68_06.us_b16;
                txt_UsLPC06_B17.Text = khz68_06.us_b17;

                txt_UsLPC06_C14.Text = khz68_06.us_c14;
                txt_UsLPC06_C15.Text = khz68_06.us_c15;
                txt_UsLPC06_C16.Text = khz68_06.us_c16;
                txt_UsLPC06_C17.Text = khz68_06.us_c17;

                txt_UsLPC06_D14.Text = khz68_06.us_d14;
                txt_UsLPC06_D15.Text = khz68_06.us_d15;
                txt_UsLPC06_D16.Text = khz68_06.us_d16;
                txt_UsLPC06_D17.Text = khz68_06.us_d17;

                txt_UsLPC06_E14.Text = khz68_06.us_e14;
                txt_UsLPC06_E15.Text = khz68_06.us_e15;
                txt_UsLPC06_E16.Text = khz68_06.us_e16;
                txt_UsLPC06_E17.Text = khz68_06.us_e17;

                txt_UsLPC06_F14.Text = khz68_06.us_f14;
                txt_UsLPC06_F15.Text = khz68_06.us_f15;
                txt_UsLPC06_F16.Text = khz68_06.us_f16;
                txt_UsLPC06_F17.Text = khz68_06.us_f17;

                txt_UsLPC06_G14.Text = khz68_06.us_g14;
                txt_UsLPC06_G15.Text = khz68_06.us_g15;
                txt_UsLPC06_G16.Text = khz68_06.us_g16;
                txt_UsLPC06_G17.Text = khz68_06.us_g17;

                txt_UsLPC06_B20.Text = Hpas[0].ws_b3;
                txt_UsLPC06_B21.Text = Hpas[0].ws_b4;
                txt_UsLPC06_B22.Text = Hpas[0].ws_b5;
            }
            #endregion

            CalculateCas();

            //initial component
            btnUsLPC03.CssClass = "btn green";
            btnUsLPC06.CssClass = "btn blue";
            btnWorksheetForHPAFiltration.CssClass = "btn blue";
            pUS_LPC03.Visible = true;
            pUS_LPC06.Visible = false;
            pWorksheetForHPAFiltration.Visible = false;

            btnSubmit.Enabled = false;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            #region "US-LPC(0.3)"
            template_seagate_hpa_coverpage khz68_03 = Hpas.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString());
            if (khz68_03 != null)
            {
                khz68_03.us_b14 = txt_UsLPC03_B14.Text;
                khz68_03.us_b15 = txt_UsLPC03_B15.Text;
                khz68_03.us_b16 = txt_UsLPC03_B16.Text;
                khz68_03.us_b17 = txt_UsLPC03_B17.Text;

                khz68_03.us_c14 = txt_UsLPC03_C14.Text;
                khz68_03.us_c15 = txt_UsLPC03_C15.Text;
                khz68_03.us_c16 = txt_UsLPC03_C16.Text;
                khz68_03.us_c17 = txt_UsLPC03_C17.Text;

                khz68_03.us_d14 = txt_UsLPC03_D14.Text;
                khz68_03.us_d15 = txt_UsLPC03_D15.Text;
                khz68_03.us_d16 = txt_UsLPC03_D16.Text;
                khz68_03.us_d17 = txt_UsLPC03_D17.Text;

                khz68_03.us_e14 = txt_UsLPC03_E14.Text;
                khz68_03.us_e15 = txt_UsLPC03_E15.Text;
                khz68_03.us_e16 = txt_UsLPC03_E16.Text;
                khz68_03.us_e17 = txt_UsLPC03_E17.Text;

                khz68_03.us_f14 = txt_UsLPC03_F14.Text;
                khz68_03.us_f15 = txt_UsLPC03_F15.Text;
                khz68_03.us_f16 = txt_UsLPC03_F16.Text;
                khz68_03.us_f17 = txt_UsLPC03_F17.Text;

                khz68_03.us_g14 = txt_UsLPC03_G14.Text;
                khz68_03.us_g15 = txt_UsLPC03_G15.Text;
                khz68_03.us_g16 = txt_UsLPC03_G16.Text;
                khz68_03.us_g17 = txt_UsLPC03_G17.Text;

                khz68_03.us_b25 = txt_UsLPC03_B25.Text;
                khz68_03.us_d25 = txt_UsLPC03_D25.Text;
                khz68_03.us_f25 = txt_UsLPC03_F25.Text;
            }
            #endregion
            #region "US-LPC(0.3)"
            template_seagate_hpa_coverpage khz68_06 = Hpas.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString());
            if (khz68_06 != null)
            {
                khz68_06.us_b14 = txt_UsLPC06_B14.Text;
                khz68_06.us_b15 = txt_UsLPC06_B15.Text;
                khz68_06.us_b16 = txt_UsLPC06_B16.Text;
                khz68_06.us_b17 = txt_UsLPC06_B17.Text;

                khz68_06.us_c14 = txt_UsLPC06_C14.Text;
                khz68_06.us_c15 = txt_UsLPC06_C15.Text;
                khz68_06.us_c16 = txt_UsLPC06_C16.Text;
                khz68_06.us_c17 = txt_UsLPC06_C17.Text;

                khz68_06.us_d14 = txt_UsLPC06_D14.Text;
                khz68_06.us_d15 = txt_UsLPC06_D15.Text;
                khz68_06.us_d16 = txt_UsLPC06_D16.Text;
                khz68_06.us_d17 = txt_UsLPC06_D17.Text;

                khz68_06.us_e14 = txt_UsLPC06_E14.Text;
                khz68_06.us_e15 = txt_UsLPC06_E15.Text;
                khz68_06.us_e16 = txt_UsLPC06_E16.Text;
                khz68_06.us_e17 = txt_UsLPC06_E17.Text;

                khz68_06.us_f14 = txt_UsLPC06_F14.Text;
                khz68_06.us_f15 = txt_UsLPC06_F15.Text;
                khz68_06.us_f16 = txt_UsLPC06_F16.Text;
                khz68_06.us_f17 = txt_UsLPC06_F17.Text;

                khz68_06.us_g14 = txt_UsLPC06_G14.Text;
                khz68_06.us_g15 = txt_UsLPC06_G15.Text;
                khz68_06.us_g16 = txt_UsLPC06_G16.Text;
                khz68_06.us_g17 = txt_UsLPC06_G17.Text;

                khz68_06.us_b25 = txt_UsLPC06_B25.Text;
                khz68_06.us_d25 = txt_UsLPC06_D25.Text;
                khz68_06.us_f25 = txt_UsLPC06_F25.Text;
            }
            #endregion

            #region "Worksheet for HPA - Filtration"
            if (this.Hpas != null)
            {
                foreach (template_seagate_hpa_coverpage ws in this.Hpas)
                {
                    ws.cvp_e19 = txtB3.Text;
                    ws.cvp_c19 = txtB5.Text;
                    ws.cvp_e20 = txtB3.Text;
                    ws.cvp_c20 = txtB5.Text;

                    ws.ws_b3 = txtB3.Text;
                    ws.ws_b4 = txtB4.Text;
                    ws.ws_b5 = txtB5.Text;
                    ws.ws_b6 = txtB6.Text;
                    ws.ws_b7 = txtB7.Text;
                    ws.ws_b8 = txtB8.Text;
                    ws.ws_b9 = txtB9.Text;


                    #region "B"
                    ws.ws_b13 = txt_WsB13.Text;
                    ws.ws_b14 = txt_WsB14.Text;
                    ws.ws_b15 = txt_WsB15.Text;
                    ws.ws_b16 = txt_WsB16.Text;
                    ws.ws_b17 = txt_WsB17.Text;
                    ws.ws_b18 = txt_WsB18.Text;
                    ws.ws_b19 = txt_WsB19.Text;
                    ws.ws_b20 = txt_WsB20.Text;
                    ws.ws_b21 = txt_WsB21.Text;
                    ws.ws_b22 = txt_WsB22.Text;
                    ws.ws_b23 = txt_WsB23.Text;
                    ws.ws_b24 = txt_WsB24.Text;
                    ws.ws_b25 = txt_WsB25.Text; //
                    ws.ws_b26 = txt_WsB26.Text;
                    ws.ws_b27 = txt_WsB27.Text;
                    ws.ws_b28 = txt_WsB28.Text;
                    ws.ws_b29 = txt_WsB29.Text; //
                    ws.ws_b30 = txt_WsB30.Text;
                    ws.ws_b31 = txt_WsB31.Text;
                    ws.ws_b32 = txt_WsB32.Text;
                    ws.ws_b33 = txt_WsB33.Text; //
                    ws.ws_b34 = txt_WsB34.Text;
                    ws.ws_b35 = txt_WsB35.Text;
                    ws.ws_b36 = txt_WsB36.Text; //
                    ws.ws_b37 = txt_WsB37.Text;
                    ws.ws_b38 = txt_WsB38.Text;
                    ws.ws_b39 = txt_WsB39.Text;
                    ws.ws_b40 = txt_WsB40.Text;
                    ws.ws_b41 = txt_WsB41.Text;
                    ws.ws_b43 = txt_WsB43.Text; //
                    ws.ws_b42 = txt_WsB42.Text;
                    ws.ws_b44 = txt_WsB44.Text;
                    ws.ws_b45 = txt_WsB45.Text;
                    ws.ws_b46 = txt_WsB46.Text;
                    ws.ws_b47 = txt_WsB47.Text; //
                    ws.ws_b48 = txt_WsB48.Text;
                    ws.ws_b49 = txt_WsB49.Text;
                    ws.ws_b50 = txt_WsB50.Text;
                    ws.ws_b51 = txt_WsB51.Text;
                    ws.ws_b52 = txt_WsB52.Text;
                    ws.ws_b53 = txt_WsB53.Text;
                    ws.ws_b54 = txt_WsB54.Text;
                    ws.ws_b55 = txt_WsB55.Text;
                    ws.ws_b56 = txt_WsB56.Text;
                    ws.ws_b57 = txt_WsB57.Text; //
                    ws.ws_b58 = txt_WsB58.Text;
                    ws.ws_b59 = txt_WsB59.Text;
                    ws.ws_b60 = txt_WsB60.Text;
                    ws.ws_b61 = txt_WsB61.Text;
                    ws.ws_b62 = txt_WsB62.Text;
                    ws.ws_b63 = txt_WsB63.Text;
                    ws.ws_b64 = txt_WsB64.Text;
                    ws.ws_b65 = txt_WsB65.Text; //
                    ws.ws_b66 = txt_WsB66.Text;
                    ws.ws_b67 = txt_WsB67.Text;
                    ws.ws_b68 = txt_WsB68.Text;
                    ws.ws_b69 = txt_WsB69.Text;
                    ws.ws_b70 = txt_WsB70.Text;
                    ws.ws_b71 = txt_WsB71.Text;
                    ws.ws_b72 = txt_WsB72.Text; //
                    ws.ws_b73 = txt_WsB73.Text;
                    ws.ws_b74 = txt_WsB74.Text;
                    ws.ws_b75 = txt_WsB75.Text; //
                    ws.ws_b76 = txt_WsB76.Text; //

                    #endregion
                    #region "C"
                    ws.ws_c13 = txt_WsC13.Text;
                    ws.ws_c14 = txt_WsC14.Text;
                    ws.ws_c15 = txt_WsC15.Text;
                    ws.ws_c16 = txt_WsC16.Text;
                    ws.ws_c17 = txt_WsC17.Text;
                    ws.ws_c18 = txt_WsC18.Text;
                    ws.ws_c19 = txt_WsC19.Text;
                    ws.ws_c20 = txt_WsC20.Text;
                    ws.ws_c21 = txt_WsC21.Text;
                    ws.ws_c22 = txt_WsC22.Text;
                    ws.ws_c23 = txt_WsC23.Text;
                    ws.ws_c24 = txt_WsC24.Text;
                    ws.ws_c25 = txt_WsC25.Text; //
                    ws.ws_c26 = txt_WsC26.Text;
                    ws.ws_c27 = txt_WsC27.Text;
                    ws.ws_c28 = txt_WsC28.Text;
                    ws.ws_c29 = txt_WsC29.Text; //
                    ws.ws_c30 = txt_WsC30.Text;
                    ws.ws_c31 = txt_WsC31.Text;
                    ws.ws_c32 = txt_WsC32.Text;
                    ws.ws_c33 = txt_WsC33.Text; //
                    ws.ws_c34 = txt_WsC34.Text;
                    ws.ws_c35 = txt_WsC35.Text;
                    ws.ws_c36 = txt_WsC36.Text; //
                    ws.ws_c37 = txt_WsC37.Text;
                    ws.ws_c38 = txt_WsC38.Text;
                    ws.ws_c39 = txt_WsC39.Text;
                    ws.ws_c40 = txt_WsC40.Text;
                    ws.ws_c41 = txt_WsC41.Text;
                    ws.ws_c43 = txt_WsC43.Text; //
                    ws.ws_c42 = txt_WsC42.Text;
                    ws.ws_c44 = txt_WsC44.Text;
                    ws.ws_c45 = txt_WsC45.Text;
                    ws.ws_c46 = txt_WsC46.Text;
                    ws.ws_c47 = txt_WsC47.Text; //
                    ws.ws_c48 = txt_WsC48.Text;
                    ws.ws_c49 = txt_WsC49.Text;
                    ws.ws_c50 = txt_WsC50.Text;
                    ws.ws_c51 = txt_WsC51.Text;
                    ws.ws_c52 = txt_WsC52.Text;
                    ws.ws_c53 = txt_WsC53.Text;
                    ws.ws_c54 = txt_WsC54.Text;
                    ws.ws_c55 = txt_WsC55.Text;
                    ws.ws_c56 = txt_WsC56.Text;
                    ws.ws_c57 = txt_WsC57.Text; //
                    ws.ws_c58 = txt_WsC58.Text;
                    ws.ws_c59 = txt_WsC59.Text;
                    ws.ws_c60 = txt_WsC60.Text;
                    ws.ws_c61 = txt_WsC61.Text;
                    ws.ws_c62 = txt_WsC62.Text;
                    ws.ws_c63 = txt_WsC63.Text;
                    ws.ws_c64 = txt_WsC64.Text;
                    ws.ws_c65 = txt_WsC65.Text; //
                    ws.ws_c66 = txt_WsC66.Text;
                    ws.ws_c67 = txt_WsC67.Text;
                    ws.ws_c68 = txt_WsC68.Text;
                    ws.ws_c69 = txt_WsC69.Text;
                    ws.ws_c70 = txt_WsC70.Text;
                    ws.ws_c71 = txt_WsC71.Text;
                    ws.ws_c72 = txt_WsC72.Text; //
                    ws.ws_c73 = txt_WsC73.Text;
                    ws.ws_c74 = txt_WsC74.Text;
                    ws.ws_c75 = txt_WsC75.Text; //
                    ws.ws_c76 = txt_WsC76.Text; //

                    #endregion
                    #region "D"
                    ws.ws_d13 = txt_WsD13.Text;
                    ws.ws_d14 = txt_WsD14.Text;
                    ws.ws_d15 = txt_WsD15.Text;
                    ws.ws_d16 = txt_WsD16.Text;
                    ws.ws_d17 = txt_WsD17.Text;
                    ws.ws_d18 = txt_WsD18.Text;
                    ws.ws_d19 = txt_WsD19.Text;
                    ws.ws_d20 = txt_WsD20.Text;
                    ws.ws_d21 = txt_WsD21.Text;
                    ws.ws_d22 = txt_WsD22.Text;
                    ws.ws_d23 = txt_WsD23.Text;
                    ws.ws_d24 = txt_WsD24.Text;
                    ws.ws_d25 = txt_WsD25.Text; //
                    ws.ws_d26 = txt_WsD26.Text;
                    ws.ws_d27 = txt_WsD27.Text;
                    ws.ws_d28 = txt_WsD28.Text;
                    ws.ws_d29 = txt_WsD29.Text; //
                    ws.ws_d30 = txt_WsD30.Text;
                    ws.ws_d31 = txt_WsD31.Text;
                    ws.ws_d32 = txt_WsD32.Text;
                    ws.ws_d33 = txt_WsD33.Text; //
                    ws.ws_d34 = txt_WsD34.Text;
                    ws.ws_d35 = txt_WsD35.Text;
                    ws.ws_d36 = txt_WsD36.Text; //
                    ws.ws_d37 = txt_WsD37.Text;
                    ws.ws_d38 = txt_WsD38.Text;
                    ws.ws_d39 = txt_WsD39.Text;
                    ws.ws_d40 = txt_WsD40.Text;
                    ws.ws_d41 = txt_WsD41.Text;
                    ws.ws_d43 = txt_WsD43.Text; //
                    ws.ws_d42 = txt_WsD42.Text;
                    ws.ws_d44 = txt_WsD44.Text;
                    ws.ws_d45 = txt_WsD45.Text;
                    ws.ws_d46 = txt_WsD46.Text;
                    ws.ws_d47 = txt_WsD47.Text; //
                    ws.ws_d48 = txt_WsD48.Text;
                    ws.ws_d49 = txt_WsD49.Text;
                    ws.ws_d50 = txt_WsD50.Text;
                    ws.ws_d51 = txt_WsD51.Text;
                    ws.ws_d52 = txt_WsD52.Text;
                    ws.ws_d53 = txt_WsD53.Text;
                    ws.ws_d54 = txt_WsD54.Text;
                    ws.ws_d55 = txt_WsD55.Text;
                    ws.ws_d56 = txt_WsD56.Text;
                    ws.ws_d57 = txt_WsD57.Text; //
                    ws.ws_d58 = txt_WsD58.Text;
                    ws.ws_d59 = txt_WsD59.Text;
                    ws.ws_d60 = txt_WsD60.Text;
                    ws.ws_d61 = txt_WsD61.Text;
                    ws.ws_d62 = txt_WsD62.Text;
                    ws.ws_d63 = txt_WsD63.Text;
                    ws.ws_d64 = txt_WsD64.Text;
                    ws.ws_d65 = txt_WsD65.Text; //
                    ws.ws_d66 = txt_WsD66.Text;
                    ws.ws_d67 = txt_WsD67.Text;
                    ws.ws_d68 = txt_WsD68.Text;
                    ws.ws_d69 = txt_WsD69.Text;
                    ws.ws_d70 = txt_WsD70.Text;
                    ws.ws_d71 = txt_WsD71.Text;
                    ws.ws_d72 = txt_WsD72.Text; //
                    ws.ws_d73 = txt_WsD73.Text;
                    ws.ws_d74 = txt_WsD74.Text;
                    ws.ws_d75 = txt_WsD75.Text; //
                    ws.ws_d76 = txt_WsD76.Text; //
                    #endregion
                }
            }
            #endregion
            new template_seagate_hpa_coverpage().UpdateList(this.Hpas);
            //Commit
            GeneralManager.Commit();

            Response.Redirect(this.PreviousPath);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnLoadFile_Click(object sender, EventArgs e)
        {
            String error = validateDSHFile(btnUpload.PostedFiles);

            if (!String.IsNullOrEmpty(txt_UsLPC03_B21.Text) && !String.IsNullOrEmpty(txt_UsLPC06_B21.Text))
            {
                if (String.IsNullOrEmpty(error))
                {
                    #region "LOAD"
                    String yyyMMdd = DateTime.Now.ToString("yyyyMMdd");

                    String[] B1_03 = new String[4];
                    String[] B1_06 = new String[4];
                    String[] S1_03 = new String[4];
                    String[] S1_06 = new String[4];
                    List<Sample> samples = new List<Sample>();
                    List<Blank> blanks = new List<Blank>();
                    for (int i = 0; i < btnUpload.PostedFiles.Count; i++)
                    {
                        HttpPostedFile _postedFile = btnUpload.PostedFiles[i];
                        try
                        {
                            if (_postedFile.ContentLength > 0)
                            {
                                String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                                String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, _postedFile.FileName);
                                if (!Directory.Exists(sourceFileFolder))
                                {
                                    Directory.CreateDirectory(sourceFileFolder);
                                }

                                _postedFile.SaveAs(savefilePath);

                                using (FileStream fs = new FileStream(savefilePath, FileMode.Open, FileAccess.Read))
                                {
                                    HSSFWorkbook wd = new HSSFWorkbook(fs);

                                    ISheet sheet = wd.GetSheet("Sheet1");
                                    if (sheet != null)
                                    {
                                        switch (Path.GetFileNameWithoutExtension(savefilePath))
                                        {
                                            case "S":
                                            case "B":
                                                #region "LPC (S,B)"
                                                int index_03 = 0;
                                                int index_06 = 0;
                                                for (int row = 22; row <= sheet.LastRowNum; row++)
                                                {
                                                    if (sheet.GetRow(row) != null) //null is when the row only contains empty cells 
                                                    {
                                                        String CS = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                                        #region "0.300"
                                                        if (CS.Equals("0.300"))
                                                        {
                                                            String CD = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                                            switch (Path.GetFileNameWithoutExtension(savefilePath))
                                                            {
                                                                case "B": B1_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDouble(CD), 2));
                                                                    break;
                                                                case "S": S1_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDouble(CD), 2));
                                                                    break;
                                                            }
                                                            index_03++;
                                                        }
                                                        #endregion
                                                        #region "0.600"
                                                        if (CS.Equals("0.600"))
                                                        {
                                                            String CD = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                                            switch (Path.GetFileNameWithoutExtension(savefilePath))
                                                            {
                                                                case "B": B1_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDouble(CD), 2));
                                                                    break;
                                                                case "S": S1_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDouble(CD), 2));
                                                                    break;
                                                            }
                                                            index_06++;
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                #endregion
                                                break;
                                            case "HPA(B)":
                                                #region "HPA(B)"

                                                for (int row = 1; row <= sheet.LastRowNum; row++)
                                                {
                                                    Blank blank = new Blank();
                                                    blank.Feature = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(0));
                                                    blank.Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(1));
                                                    blank.Field = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(2));
                                                    blank.Rank = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(3));
                                                    blank.SST400s = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(4));
                                                    blank.Cabased = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(5));
                                                    blank.Snbased = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(6));
                                                    blank.NiFe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(7));
                                                    blank.others = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(8));
                                                    blank.Rejected_Manual = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                                    blank.Area_sqµm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(10));
                                                    blank.AspectRatio = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(11));
                                                    blank.BeamX_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(12));
                                                    blank.BeamY_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(13));
                                                    blank.Breadth_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(14));
                                                    blank.Direction_degrees = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(15));
                                                    blank.ECD_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(16));
                                                    blank.Length_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(17));
                                                    blank.Perimeter_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                                    blank.Shape = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(19));
                                                    blank.Meangrey = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(20));
                                                    blank.SpectrumArea = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(21));
                                                    blank.StageX_mm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(22));
                                                    blank.StageY_mm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(23));
                                                    blank.StageZ_mm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(24));
                                                    blank.O_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(25));
                                                    blank.Na_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(26));
                                                    blank.Mg_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(27));
                                                    blank.Al_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(28));
                                                    blank.Si_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(29));
                                                    blank.P_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(30));
                                                    blank.S_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(31));
                                                    blank.Cl_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(32));
                                                    blank.K_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(33));
                                                    blank.Ca_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(34));
                                                    blank.Ti_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(35));
                                                    blank.Cr_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(36));
                                                    blank.Fe_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(37));
                                                    blank.Ni_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(38));
                                                    blank.Cu_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(39));
                                                    blank.Zn_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(40));
                                                    blank.Sn_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(41));
                                                    blanks.Add(blank);
                                                }
                                                #endregion
                                                break;
                                            case "HPA(S)":
                                                #region "HPA(S)"
                                                for (int row = 0; row <= sheet.LastRowNum; row++)
                                                {
                                                    Sample sample = new Sample();
                                                    sample.Feature = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(0));
                                                    sample.Area = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(1));
                                                    sample.Field = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(2));
                                                    sample.Rank = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(3));
                                                    sample.SiO = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(4));
                                                    sample.SST300s = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(5));
                                                    sample.SST400s = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(6));
                                                    sample.Cabased = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(7));
                                                    sample.Febased = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(8));
                                                    sample.FeO = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                                    sample.MnCrS = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(10));
                                                    sample.NiP = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(11));
                                                    sample.MgSiO = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(12));
                                                    sample.Sbased = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(13));
                                                    sample.NiFe = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(14));
                                                    sample.others = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(15));
                                                    sample.FeSiO = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(16));
                                                    sample.Rejected_Manual = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(17));
                                                    sample.Rejected_ED = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                                    sample.Area_sqµm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(19));
                                                    sample.AspectRatio = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(20));
                                                    sample.BeamX_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(21));
                                                    sample.BeamY_pixels = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(22));
                                                    sample.Breadth_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(23));
                                                    sample.Direction_degrees = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(24));
                                                    sample.ECD_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(25));
                                                    sample.Length_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(26));
                                                    sample.Perimeter_µm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(27));
                                                    sample.Shape = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(28));
                                                    sample.Meangrey = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(29));
                                                    sample.SpectrumArea = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(30));
                                                    sample.StageX_mm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(31));
                                                    sample.StageY_mm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(32));
                                                    sample.StageZ_mm = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(33));
                                                    sample.O_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(34));
                                                    sample.Na_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(35));
                                                    sample.Mg_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(36));
                                                    sample.Al_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(37));
                                                    sample.Si_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(38));
                                                    sample.P_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(39));
                                                    sample.S_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(40));
                                                    sample.Cl_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(41));
                                                    sample.K_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(42));
                                                    sample.Ca_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(43));
                                                    sample.Ti_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(44));
                                                    sample.Cr_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(45));
                                                    sample.Mn_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(46));
                                                    sample.Fe_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(47));
                                                    sample.Ni_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(48));
                                                    sample.Sr_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(49));
                                                    sample.Mo_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(50));
                                                    sample.Te_Wt_Percent = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(51));
                                                    samples.Add(sample);
                                                }
                                                #endregion
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            logger.Error(Ex.Message);
                            Console.WriteLine();
                        }
                    }
                    #endregion
                    #region "SET DATA TO FORM"
                    #region "US-LPC(0.3)"
                    txt_UsLPC03_B14.Text = B1_03[0];
                    txt_UsLPC03_B15.Text = B1_03[1];
                    txt_UsLPC03_B16.Text = B1_03[2];
                    txt_UsLPC03_B17.Text = B1_03[3];

                    txt_UsLPC03_C14.Text = S1_03[0];
                    txt_UsLPC03_C15.Text = S1_03[1];
                    txt_UsLPC03_C16.Text = S1_03[2];
                    txt_UsLPC03_C17.Text = S1_03[3];

                    #endregion
                    #region "US-LPC(0.6)"
                    txt_UsLPC06_B14.Text = B1_06[0];
                    txt_UsLPC06_B15.Text = B1_06[1];
                    txt_UsLPC06_B16.Text = B1_06[2];
                    txt_UsLPC06_B17.Text = B1_06[3];

                    txt_UsLPC06_C14.Text = S1_06[0];
                    txt_UsLPC06_C15.Text = S1_06[1];
                    txt_UsLPC06_C16.Text = S1_06[2];
                    txt_UsLPC06_C17.Text = S1_06[3];

                    #endregion

                    #region "Worksheet for HPA - Filtration"
                    #region "B"
                    txt_WsB13.Text = "0";//AlMgO
                    txt_WsB14.Text = "0";//AlO
                    txt_WsB15.Text = "0";//AlOTiC
                    txt_WsB16.Text = "0";//CrO
                    txt_WsB17.Text = "0";//FeSiO
                    txt_WsB18.Text = "0";//NbB
                    txt_WsB19.Text = "0";//SiC
                    txt_WsB20.Text = "0";//SiO
                    txt_WsB21.Text = "0";//TiB
                    txt_WsB22.Text = "0";//TiC
                    txt_WsB23.Text = "0";//TiO
                    txt_WsB24.Text = "0";//TiSn
                    txt_WsB25.Text = "0";//TiV
                    txt_WsB26.Text = "0";//WC
                    txt_WsB27.Text = "0";//ZrC
                    txt_WsB28.Text = "0";//ZrO
                    txt_WsB29.Text = (
                        Convert.ToInt32(txt_WsB13.Text) +
                        Convert.ToInt32(txt_WsB14.Text) +
                        Convert.ToInt32(txt_WsB15.Text) +
                        Convert.ToInt32(txt_WsB16.Text) +
                        Convert.ToInt32(txt_WsB17.Text) +
                        Convert.ToInt32(txt_WsB18.Text) +
                        Convert.ToInt32(txt_WsB19.Text) +
                        Convert.ToInt32(txt_WsB20.Text) +
                        Convert.ToInt32(txt_WsB21.Text) +
                        Convert.ToInt32(txt_WsB22.Text) +
                        Convert.ToInt32(txt_WsB23.Text) +
                        Convert.ToInt32(txt_WsB24.Text) +
                        Convert.ToInt32(txt_WsB25.Text) +
                        Convert.ToInt32(txt_WsB26.Text) +
                        Convert.ToInt32(txt_WsB27.Text)).ToString();
                    txt_WsB30.Text = "0";//Nd based
                    txt_WsB31.Text = "0";//Sm based
                    txt_WsB32.Text = "0";//Sr based
                    txt_WsB33.Text = (
                        Convert.ToInt32(txt_WsB30.Text) +
                        Convert.ToInt32(txt_WsB31.Text) +
                        Convert.ToInt32(txt_WsB32.Text)).ToString();
                    txt_WsB34.Text = "0";//SST300s (Fe/Cr/Ni) 
                    txt_WsB35.Text = blanks.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.SST400s)).ToString();//SST400s (Fe/Cr)
                    txt_WsB36.Text = (
                        Convert.ToInt32(txt_WsB34.Text) +
                        Convert.ToInt32(txt_WsB35.Text)).ToString();
                    txt_WsB37.Text = "0";//Ag based
                    txt_WsB38.Text = "0";//Al based
                    txt_WsB39.Text = "0";//Au based
                    txt_WsB40.Text = "0";//Cu based
                    txt_WsB41.Text = "0";//Fe based
                    txt_WsB43.Text = "0";//MnCrS
                    txt_WsB42.Text = "0";//Ni based
                    txt_WsB44.Text = "0";//NiP
                    txt_WsB45.Text = "0";//Pt based
                    txt_WsB46.Text = "0";//Sb based
                    txt_WsB47.Text = blanks.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Snbased)).ToString();//Sn based
                    txt_WsB48.Text = "0";//SnPb
                    txt_WsB49.Text = "0";//Zn based
                    txt_WsB50.Text = "0";//AlSi(FeCrCuZnMn)
                    txt_WsB51.Text = blanks.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.NiFe)).ToString();//NiFe
                    txt_WsB52.Text = "0";//ZnPFe
                    txt_WsB53.Text = "0";//CrCoNiP (disc material)
                    txt_WsB54.Text = "0";//NiPCr
                    txt_WsB55.Text = "0";//NiPCrFe
                    txt_WsB56.Text = "0";//CuZn
                    txt_WsB57.Text = (
                        Convert.ToInt32(txt_WsB37.Text) +
                        Convert.ToInt32(txt_WsB38.Text) +
                        Convert.ToInt32(txt_WsB39.Text) +
                        Convert.ToInt32(txt_WsB40.Text) +
                        Convert.ToInt32(txt_WsB41.Text) +
                        Convert.ToInt32(txt_WsB43.Text) +
                        Convert.ToInt32(txt_WsB42.Text) +
                        Convert.ToInt32(txt_WsB44.Text) +
                        Convert.ToInt32(txt_WsB45.Text) +
                        Convert.ToInt32(txt_WsB46.Text) +
                        Convert.ToInt32(txt_WsB47.Text) +
                        Convert.ToInt32(txt_WsB48.Text) +
                        Convert.ToInt32(txt_WsB49.Text) +
                        Convert.ToInt32(txt_WsB50.Text) +
                        Convert.ToInt32(txt_WsB51.Text) +
                        Convert.ToInt32(txt_WsB52.Text) +
                        Convert.ToInt32(txt_WsB53.Text) +
                        Convert.ToInt32(txt_WsB54.Text) +
                        Convert.ToInt32(txt_WsB55.Text) +
                        Convert.ToInt32(txt_WsB56.Text)).ToString();
                    txt_WsB58.Text = "0";//FeO
                    txt_WsB59.Text = "0";//AlFeO
                    txt_WsB60.Text = "0";//AlNiO
                    txt_WsB61.Text = "0";//AlSiO
                    txt_WsB62.Text = "0";//Cl based
                    txt_WsB63.Text = "0";//FeMgSiO
                    txt_WsB64.Text = "0";//MgCaO
                    txt_WsB65.Text = "0";//MgSiO
                    txt_WsB66.Text = "0";//S based
                    txt_WsB67.Text = "0";//F based
                    txt_WsB68.Text = blanks.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Cabased)).ToString();//Ca based
                    txt_WsB69.Text = "0";//Na based
                    txt_WsB70.Text = "0";//K based
                    txt_WsB71.Text = "0";//Anodised Al
                    txt_WsB72.Text = "0";//PZT
                    txt_WsB73.Text = "0";//Pb base
                    txt_WsB74.Text = blanks.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.others)).ToString();//Others
                    txt_WsB75.Text = (
                        Convert.ToInt32(txt_WsB58.Text) +
                        Convert.ToInt32(txt_WsB59.Text) +
                        Convert.ToInt32(txt_WsB60.Text) +
                        Convert.ToInt32(txt_WsB61.Text) +
                        Convert.ToInt32(txt_WsB62.Text) +
                        Convert.ToInt32(txt_WsB63.Text) +
                        Convert.ToInt32(txt_WsB64.Text) +
                        Convert.ToInt32(txt_WsB65.Text) +
                        Convert.ToInt32(txt_WsB66.Text) +
                        Convert.ToInt32(txt_WsB67.Text) +
                        Convert.ToInt32(txt_WsB68.Text) +
                        Convert.ToInt32(txt_WsB69.Text) +
                        Convert.ToInt32(txt_WsB70.Text) +
                        Convert.ToInt32(txt_WsB71.Text) +
                        Convert.ToInt32(txt_WsB72.Text) +
                        Convert.ToInt32(txt_WsB73.Text) +
                        Convert.ToInt32(txt_WsB74.Text)).ToString();
                    txt_WsB76.Text = (
                        Convert.ToInt32(txt_WsB29.Text) +
                        Convert.ToInt32(txt_WsB33.Text) +
                        Convert.ToInt32(txt_WsB36.Text) +
                        Convert.ToInt32(txt_WsB57.Text) +
                        Convert.ToInt32(txt_WsB75.Text)).ToString();
                    #endregion
                    #region "C"
                    txt_WsC13.Text = "0";//AlMgO
                    txt_WsC14.Text = "0";//AlO
                    txt_WsC15.Text = "0";//AlOTiC
                    txt_WsC16.Text = "0";//CrO
                    txt_WsC17.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.FeSiO)).ToString();//FeSiO
                    txt_WsC18.Text = "0";//NbB
                    txt_WsC19.Text = "0";//SiC
                    txt_WsC20.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.SiO)).ToString();//SiO
                    txt_WsC21.Text = "0";//TiB
                    txt_WsC22.Text = "0";//TiC
                    txt_WsC23.Text = "0";//TiO
                    txt_WsC24.Text = "0";//TiSn
                    txt_WsC25.Text = "0";//TiV
                    txt_WsC26.Text = "0";//WC
                    txt_WsC27.Text = "0";//ZrC
                    txt_WsC28.Text = "0";//ZrO
                    txt_WsC29.Text = (
                        Convert.ToInt32(txt_WsC13.Text) +
                        Convert.ToInt32(txt_WsC14.Text) +
                        Convert.ToInt32(txt_WsC15.Text) +
                        Convert.ToInt32(txt_WsC16.Text) +
                        Convert.ToInt32(txt_WsC17.Text) +
                        Convert.ToInt32(txt_WsC18.Text) +
                        Convert.ToInt32(txt_WsC19.Text) +
                        Convert.ToInt32(txt_WsC20.Text) +
                        Convert.ToInt32(txt_WsC21.Text) +
                        Convert.ToInt32(txt_WsC22.Text) +
                        Convert.ToInt32(txt_WsC23.Text) +
                        Convert.ToInt32(txt_WsC24.Text) +
                        Convert.ToInt32(txt_WsC25.Text) +
                        Convert.ToInt32(txt_WsC26.Text) +
                        Convert.ToInt32(txt_WsC27.Text)).ToString();
                    txt_WsC30.Text = "0";//Nd based
                    txt_WsC31.Text = "0";//Sm based
                    txt_WsC32.Text = "0";//Sr based
                    txt_WsC33.Text = (
                        Convert.ToInt32(txt_WsC30.Text) +
                        Convert.ToInt32(txt_WsC31.Text) +
                        Convert.ToInt32(txt_WsC32.Text)).ToString();
                    txt_WsC34.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.SST300s)).ToString();//SST300s (Fe/Cr/Ni) 
                    txt_WsC35.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.SST400s)).ToString();//SST400s (Fe/Cr)
                    txt_WsC36.Text = (
                        Convert.ToInt32(txt_WsC34.Text) +
                        Convert.ToInt32(txt_WsC35.Text)).ToString();
                    txt_WsC37.Text = "0";//Ag based
                    txt_WsC38.Text = "0";//Al based
                    txt_WsC39.Text = "0";//Au based
                    txt_WsC40.Text = "0";//Cu based
                    txt_WsC41.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Febased)).ToString();//Fe based
                    txt_WsC42.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.MnCrS)).ToString();//MnCrS
                    txt_WsC43.Text = "0";//Ni based
                    txt_WsC44.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.NiP)).ToString();//NiP
                    txt_WsC45.Text = "0";//Pt based
                    txt_WsC46.Text = "0";//Sb based
                    txt_WsC47.Text = "0";//Sn based
                    txt_WsC48.Text = "0";//SnPb
                    txt_WsC49.Text = "0";//Zn based
                    txt_WsC50.Text = "0";//AlSi(FeCrCuZnMn)
                    txt_WsC51.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.NiFe)).ToString();//NiFe
                    txt_WsC52.Text = "0";//ZnPFe
                    txt_WsC53.Text = "0";//CrCoNiP (disc material)
                    txt_WsC54.Text = "0";//NiPCr
                    txt_WsC55.Text = "0";//NiPCrFe
                    txt_WsC56.Text = "0";//CuZn
                    txt_WsC57.Text = (
                        Convert.ToInt32(txt_WsC37.Text) +
                        Convert.ToInt32(txt_WsC38.Text) +
                        Convert.ToInt32(txt_WsC39.Text) +
                        Convert.ToInt32(txt_WsC40.Text) +
                        Convert.ToInt32(txt_WsC41.Text) +
                        Convert.ToInt32(txt_WsC43.Text) +
                        Convert.ToInt32(txt_WsC42.Text) +
                        Convert.ToInt32(txt_WsC44.Text) +
                        Convert.ToInt32(txt_WsC45.Text) +
                        Convert.ToInt32(txt_WsC46.Text) +
                        Convert.ToInt32(txt_WsC47.Text) +
                        Convert.ToInt32(txt_WsC48.Text) +
                        Convert.ToInt32(txt_WsC49.Text) +
                        Convert.ToInt32(txt_WsC50.Text) +
                        Convert.ToInt32(txt_WsC51.Text) +
                        Convert.ToInt32(txt_WsC52.Text) +
                        Convert.ToInt32(txt_WsC53.Text) +
                        Convert.ToInt32(txt_WsC54.Text) +
                        Convert.ToInt32(txt_WsC55.Text) +
                        Convert.ToInt32(txt_WsC56.Text)).ToString();
                    txt_WsC58.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.FeO)).ToString();//FeO
                    txt_WsC59.Text = "0";//AlFeO
                    txt_WsC60.Text = "0";//AlNiO
                    txt_WsC61.Text = "0";//AlSiO
                    txt_WsC62.Text = "0";//Cl based
                    txt_WsC63.Text = "0";//FeMgSiO
                    txt_WsC64.Text = "0";//MgCaO
                    txt_WsC65.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.MgSiO)).ToString();//MgSiO
                    txt_WsC66.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Sbased)).ToString();//S based
                    txt_WsC67.Text = "0";//F based
                    txt_WsC68.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.Cabased)).ToString();//Ca based
                    txt_WsC69.Text = "0";//Na based
                    txt_WsC70.Text = "0";//K based
                    txt_WsC71.Text = "0";//Anodised Al
                    txt_WsC72.Text = "0";//PZT
                    txt_WsC73.Text = "0";//Pb base
                    txt_WsC74.Text = samples.FindAll(x => x.Rejected_Manual == "0").Sum(x => Convert.ToInt32(x.others)).ToString();//Others
                    txt_WsC75.Text = (
                        Convert.ToInt32(txt_WsC58.Text) +
                        Convert.ToInt32(txt_WsC59.Text) +
                        Convert.ToInt32(txt_WsC60.Text) +
                        Convert.ToInt32(txt_WsC61.Text) +
                        Convert.ToInt32(txt_WsC62.Text) +
                        Convert.ToInt32(txt_WsC63.Text) +
                        Convert.ToInt32(txt_WsC64.Text) +
                        Convert.ToInt32(txt_WsC65.Text) +
                        Convert.ToInt32(txt_WsC66.Text) +
                        Convert.ToInt32(txt_WsC67.Text) +
                        Convert.ToInt32(txt_WsC68.Text) +
                        Convert.ToInt32(txt_WsC69.Text) +
                        Convert.ToInt32(txt_WsC70.Text) +
                        Convert.ToInt32(txt_WsC71.Text) +
                        Convert.ToInt32(txt_WsC72.Text) +
                        Convert.ToInt32(txt_WsC73.Text) +
                        Convert.ToInt32(txt_WsC74.Text)).ToString();
                    txt_WsC76.Text = (
                        Convert.ToInt32(txt_WsC29.Text) +
                        Convert.ToInt32(txt_WsC33.Text) +
                        Convert.ToInt32(txt_WsC36.Text) +
                        Convert.ToInt32(txt_WsC57.Text) +
                        Convert.ToInt32(txt_WsC75.Text)).ToString();
                    #endregion
                    #endregion

                    #endregion
                    CalculateCas();
                    btnSubmit.Enabled = false;
                }
                else
                {
                    lbMessage.Text = error;
                    lbMessage.Attributes["class"] = "alert alert-error";
                }
            }
            else
            {
                lbMessage.Text = "Please enter value of Surface Area.";
                lbMessage.Attributes["class"] = "alert alert-error";
            }
        }

        protected void btnUsLPC_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.Text)
            {
                case "US-LPC(0.3)":
                    btnUsLPC03.CssClass = "btn green";
                    btnUsLPC06.CssClass = "btn blue";
                    btnWorksheetForHPAFiltration.CssClass = "btn blue";
                    pUS_LPC03.Visible = true;
                    pUS_LPC06.Visible = false;
                    pWorksheetForHPAFiltration.Visible = false;
                    break;
                case "US-LPC(0.6)":
                    btnUsLPC03.CssClass = "btn blue";
                    btnUsLPC06.CssClass = "btn green";
                    btnWorksheetForHPAFiltration.CssClass = "btn blue";
                    pUS_LPC03.Visible = false;
                    pUS_LPC06.Visible = true;
                    pWorksheetForHPAFiltration.Visible = false;
                    break;
                case "Worksheet for HPA - Filtration":
                    btnUsLPC03.CssClass = "btn blue";
                    btnUsLPC06.CssClass = "btn blue";
                    btnWorksheetForHPAFiltration.CssClass = "btn green";
                    pUS_LPC03.Visible = false;
                    pUS_LPC06.Visible = false;
                    pWorksheetForHPAFiltration.Visible = true;
                    break;
            }
        }

        #region "Custom method"

        private String validateDSHFile(IList<HttpPostedFile> _files)
        {
            Boolean isFound_b1 = false;
            Boolean isFound_s1 = false;
            Boolean isFound_hb1 = false;
            Boolean isFound_hs1 = false;
            Boolean isFoundWrongExtension = false;

            String result = String.Empty;

            String[] files = new String[_files.Count];
            if (files.Length == 4)
            {
                for (int i = 0; i < _files.Count; i++)
                {
                    files[i] = _files[i].FileName;
                    if (!Path.GetExtension(_files[i].FileName).Trim().ToLower().Equals(".xls"))
                    {
                        isFoundWrongExtension = true;
                        break;
                    }
                }
                if (!isFoundWrongExtension)
                {

                    //Find B1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B"))
                        {
                            isFound_b1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S"))
                        {
                            isFound_s1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("HPA(B)"))
                        {
                            isFound_hb1 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("HPA(S)"))
                        {
                            isFound_hs1 = true;
                            break;
                        }
                    }
                    result = (!isFound_b1) ? result += "File not found B.xls" :
                                (!isFound_s1) ? result += "File not found S.xls" :
                                (!isFound_hb1) ? result += "File not found HPA(B).xls" :
                                (!isFound_hs1) ? result += "File not found HPA(S)xls" : String.Empty;
                }
                else
                {
                    result = "File extension must be *.xls";
                }
            }
            else
            {
                result = "You must to select 4 files for upload.";
            }
            return result;
        }

        private void CalculateCas()
        {

            #region "US-LPC(0.3)"
            //=AVERAGE(B15,B16,B17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_B15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B17.Text))
            {
                txt_UsLPC03_B18.Text = ((
                    Convert.ToDouble(txt_UsLPC03_B15.Text) +
                    Convert.ToDouble(txt_UsLPC03_B16.Text) +
                    Convert.ToDouble(txt_UsLPC03_B17.Text)) / 3).ToString();

                txt_UsLPC03_B18.Text = String.Format("{0:n3}", Math.Round(Convert.ToDouble(txt_UsLPC03_B18.Text), 3));
            }
            //=AVERAGE(C15,C16,C17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_C15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_C16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_C17.Text))
            {
                txt_UsLPC03_C18.Text = ((
                    Convert.ToDouble(txt_UsLPC03_C15.Text) +
                    Convert.ToDouble(txt_UsLPC03_C16.Text) +
                    Convert.ToDouble(txt_UsLPC03_C17.Text)) / 3).ToString();
                txt_UsLPC03_C18.Text = String.Format("{0:n3}", Math.Round(Convert.ToDouble(txt_UsLPC03_C18.Text), 3));
            }
            //=AVERAGE(D15,D16,D17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_D15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_D16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_D17.Text))
            {
                txt_UsLPC03_D18.Text = ((
                    Convert.ToDouble(txt_UsLPC03_D15.Text) +
                    Convert.ToDouble(txt_UsLPC03_D16.Text) +
                    Convert.ToDouble(txt_UsLPC03_D17.Text)) / 3).ToString();
                txt_UsLPC03_D18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC03_D18.Text), 2));
            }
            //=AVERAGE(E15,E16,E17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_E15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_E16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_E17.Text))
            {
                txt_UsLPC03_E18.Text = ((
                    Convert.ToDouble(txt_UsLPC03_E15.Text) +
                    Convert.ToDouble(txt_UsLPC03_E16.Text) +
                    Convert.ToDouble(txt_UsLPC03_E17.Text)) / 3).ToString();
                txt_UsLPC03_E18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC03_E18.Text), 2));
            }
            //=AVERAGE(F15,F16,F17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_F15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_F16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_F17.Text))
            {
                txt_UsLPC03_F18.Text = ((
                    Convert.ToDouble(txt_UsLPC03_F15.Text) +
                    Convert.ToDouble(txt_UsLPC03_F16.Text) +
                    Convert.ToDouble(txt_UsLPC03_F17.Text)) / 3).ToString();
                txt_UsLPC03_F18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC03_F18.Text), 2));
            }
            //=AVERAGE(G15,G16,G17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_G15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_G16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_G17.Text))
            {
                txt_UsLPC03_G18.Text = ((
                    Convert.ToDouble(txt_UsLPC03_G15.Text) +
                    Convert.ToDouble(txt_UsLPC03_G16.Text) +
                    Convert.ToDouble(txt_UsLPC03_G17.Text)) / 3).ToString();
                txt_UsLPC03_G18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC03_G18.Text), 2));
            }
            //FORM COVER PAGE

            txt_UsLPC03_B20.Text = txtB3.Text;//khz68_03.ws_b3;
            txt_UsLPC03_B21.Text = txtB4.Text;//khz68_03.ws_b4;
            txt_UsLPC03_B22.Text = txtB5.Text;//khz68_03.ws_b5;

            if (!String.IsNullOrEmpty(txt_UsLPC03_C18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B22.Text))
            {
                //=(C18-B18)*$B$20/($B$21*$B$22)
                txt_UsLPC03_B25.Text = (((Convert.ToDouble(txt_UsLPC03_C18.Text) - Convert.ToDouble(txt_UsLPC03_B18.Text)) * Convert.ToDouble(txt_UsLPC03_B20.Text)) /
                                        (Convert.ToDouble(txt_UsLPC03_B21.Text) * Convert.ToDouble(txt_UsLPC03_B22.Text))).ToString();
                txt_UsLPC03_B25.Text = Math.Round(Convert.ToDouble(txt_UsLPC03_B25.Text)).ToString();
            }
            //=(E18-D18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC03_E18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_D18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B22.Text))
            {
                txt_UsLPC03_D25.Text = (((Convert.ToDouble(txt_UsLPC03_E18.Text) - Convert.ToDouble(txt_UsLPC03_D18.Text)) * Convert.ToDouble(txt_UsLPC03_B20.Text)) /
                                        (Convert.ToDouble(txt_UsLPC03_B21.Text) * Convert.ToDouble(txt_UsLPC03_B22.Text))).ToString();
                txt_UsLPC03_D25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC03_D25.Text)));
            }
            //=(G18-F18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC03_G18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_F18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B22.Text))
            {
                txt_UsLPC03_F25.Text = (((Convert.ToDouble(txt_UsLPC03_G18.Text) - Convert.ToDouble(txt_UsLPC03_F18.Text)) * Convert.ToDouble(txt_UsLPC03_B20.Text)) /
                                        (Convert.ToDouble(txt_UsLPC03_B21.Text) * Convert.ToDouble(txt_UsLPC03_B22.Text))).ToString();
                txt_UsLPC03_F25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC03_F25.Text)));
            }
            //=AVERAGE(B25,D25,F25)
            if (!String.IsNullOrEmpty(txt_UsLPC03_B25.Text))
            {
                txt_UsLPC03_B26.Text = Math.Round(Convert.ToDouble(txt_UsLPC03_B25.Text)).ToString();
            }
            #endregion

            #region "US-LPC(0.6)"
            //=AVERAGE(B15,B16,B17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_B15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B17.Text))
            {
                txt_UsLPC06_B18.Text = ((
                    Convert.ToDouble(txt_UsLPC06_B15.Text) +
                    Convert.ToDouble(txt_UsLPC06_B16.Text) +
                    Convert.ToDouble(txt_UsLPC06_B17.Text)) / 3).ToString();

                txt_UsLPC06_B18.Text = String.Format("{0:n3}", Math.Round(Convert.ToDouble(txt_UsLPC06_B18.Text), 3));
            }
            //=AVERAGE(C15,C16,C17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_C15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_C16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_C17.Text))
            {
                txt_UsLPC06_C18.Text = ((
                    Convert.ToDouble(txt_UsLPC06_C15.Text) +
                    Convert.ToDouble(txt_UsLPC06_C16.Text) +
                    Convert.ToDouble(txt_UsLPC06_C17.Text)) / 3).ToString();
                txt_UsLPC06_C18.Text = String.Format("{0:n3}", Math.Round(Convert.ToDouble(txt_UsLPC06_C18.Text), 3));
            }
            //=AVERAGE(D15,D16,D17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_D15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_D16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_D17.Text))
            {
                txt_UsLPC06_D18.Text = ((
                    Convert.ToDouble(txt_UsLPC06_D15.Text) +
                    Convert.ToDouble(txt_UsLPC06_D16.Text) +
                    Convert.ToDouble(txt_UsLPC06_D17.Text)) / 3).ToString();
                txt_UsLPC06_D18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC06_D18.Text), 2));
            }
            //=AVERAGE(E15,E16,E17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_E15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_E16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_E17.Text))
            {
                txt_UsLPC06_E18.Text = ((
                    Convert.ToDouble(txt_UsLPC06_E15.Text) +
                    Convert.ToDouble(txt_UsLPC06_E16.Text) +
                    Convert.ToDouble(txt_UsLPC06_E17.Text)) / 3).ToString();
                txt_UsLPC06_E18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC06_E18.Text), 2));
            }
            //=AVERAGE(F15,F16,F17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_F15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_F16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_F17.Text))
            {
                txt_UsLPC06_F18.Text = ((
                    Convert.ToDouble(txt_UsLPC06_F15.Text) +
                    Convert.ToDouble(txt_UsLPC06_F16.Text) +
                    Convert.ToDouble(txt_UsLPC06_F17.Text)) / 3).ToString();
                txt_UsLPC06_F18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC06_F18.Text), 2));
            }
            //=AVERAGE(G15,G16,G17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_G15.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_G16.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_G17.Text))
            {
                txt_UsLPC06_G18.Text = ((
                    Convert.ToDouble(txt_UsLPC06_G15.Text) +
                    Convert.ToDouble(txt_UsLPC06_G16.Text) +
                    Convert.ToDouble(txt_UsLPC06_G17.Text)) / 3).ToString();
                txt_UsLPC06_G18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC06_G18.Text), 2));
            }
            //FORM COVER PAGE

            txt_UsLPC06_B20.Text = txtB3.Text;// khz68_03.ws_b3;
            txt_UsLPC06_B21.Text = txtB4.Text;//khz68_03.ws_b4;
            txt_UsLPC06_B22.Text = txtB5.Text;// khz68_03.ws_b5;
            if (!String.IsNullOrEmpty(txt_UsLPC06_C18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B22.Text))
            {
                //=(C18-B18)*$B$20/($B$21*$B$22)
                txt_UsLPC06_B25.Text = (((Convert.ToDouble(txt_UsLPC06_C18.Text) - Convert.ToDouble(txt_UsLPC06_B18.Text)) * Convert.ToDouble(txt_UsLPC06_B20.Text)) /
                                        (Convert.ToDouble(txt_UsLPC06_B21.Text) * Convert.ToDouble(txt_UsLPC06_B22.Text))).ToString();
                txt_UsLPC06_B25.Text = Math.Round(Convert.ToDouble(txt_UsLPC06_B25.Text)).ToString();
            }
            //=(E18-D18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC06_E18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_D18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B22.Text))
            {
                txt_UsLPC06_D25.Text = (((Convert.ToDouble(txt_UsLPC06_E18.Text) - Convert.ToDouble(txt_UsLPC06_D18.Text)) * Convert.ToDouble(txt_UsLPC06_B20.Text)) /
                                        (Convert.ToDouble(txt_UsLPC06_B21.Text) * Convert.ToDouble(txt_UsLPC06_B22.Text))).ToString();
                txt_UsLPC06_D25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC06_D25.Text)));
            }
            //=(G18-F18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC06_G18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_F18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B22.Text))
            {
                txt_UsLPC06_F25.Text = (((Convert.ToDouble(txt_UsLPC06_G18.Text) - Convert.ToDouble(txt_UsLPC06_F18.Text)) * Convert.ToDouble(txt_UsLPC06_B20.Text)) /
                                        (Convert.ToDouble(txt_UsLPC06_B21.Text) * Convert.ToDouble(txt_UsLPC06_B22.Text))).ToString();
                txt_UsLPC06_F25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDouble(txt_UsLPC06_F25.Text)));
            }
            //=AVERAGE(B25,D25,F25)
            if (!String.IsNullOrEmpty(txt_UsLPC06_B25.Text))
            {
                txt_UsLPC06_B26.Text = Math.Round(Convert.ToDouble(txt_UsLPC06_B25.Text)).ToString();
            }
            #endregion

            #region "Worksheet for HPA - Filtration"
            //ROUND(MAX(0,(C13-B13))*($B$7/$B$9)*($B$3/$B$6)/($B$4*$B$5),1)

            if (!String.IsNullOrEmpty(txtB3.Text) &&
                !String.IsNullOrEmpty(txtB6.Text) &&
                !String.IsNullOrEmpty(txtB4.Text) &&
                !String.IsNullOrEmpty(txtB5.Text) &&
                !String.IsNullOrEmpty(txt_WsC13.Text) &&
                !String.IsNullOrEmpty(txt_WsC14.Text) &&
                !String.IsNullOrEmpty(txt_WsC15.Text) &&
                !String.IsNullOrEmpty(txt_WsC16.Text) &&
                !String.IsNullOrEmpty(txt_WsC17.Text) &&
                !String.IsNullOrEmpty(txt_WsC18.Text) &&
                !String.IsNullOrEmpty(txt_WsC19.Text) &&
                !String.IsNullOrEmpty(txt_WsC20.Text) &&
                !String.IsNullOrEmpty(txt_WsC21.Text) &&
                !String.IsNullOrEmpty(txt_WsC22.Text) &&
                !String.IsNullOrEmpty(txt_WsC23.Text) &&
                !String.IsNullOrEmpty(txt_WsC24.Text) &&
                !String.IsNullOrEmpty(txt_WsC25.Text) &&
                !String.IsNullOrEmpty(txt_WsC26.Text) &&
                !String.IsNullOrEmpty(txt_WsC27.Text) &&
                !String.IsNullOrEmpty(txt_WsC28.Text) &&
                !String.IsNullOrEmpty(txt_WsC29.Text) &&
                !String.IsNullOrEmpty(txt_WsC30.Text) &&
                !String.IsNullOrEmpty(txt_WsC31.Text) &&
                !String.IsNullOrEmpty(txt_WsC32.Text) &&
                !String.IsNullOrEmpty(txt_WsC33.Text) &&
                !String.IsNullOrEmpty(txt_WsC34.Text) &&
                !String.IsNullOrEmpty(txt_WsC35.Text) &&
                !String.IsNullOrEmpty(txt_WsC36.Text) &&
                !String.IsNullOrEmpty(txt_WsC37.Text) &&
                !String.IsNullOrEmpty(txt_WsC38.Text) &&
                !String.IsNullOrEmpty(txt_WsC39.Text) &&
                !String.IsNullOrEmpty(txt_WsC40.Text) &&
                !String.IsNullOrEmpty(txt_WsC41.Text) &&
                !String.IsNullOrEmpty(txt_WsC42.Text) &&
                !String.IsNullOrEmpty(txt_WsC43.Text) &&
                !String.IsNullOrEmpty(txt_WsC44.Text) &&
                !String.IsNullOrEmpty(txt_WsC45.Text) &&
                !String.IsNullOrEmpty(txt_WsC46.Text) &&
                !String.IsNullOrEmpty(txt_WsC47.Text) &&
                !String.IsNullOrEmpty(txt_WsC48.Text) &&
                !String.IsNullOrEmpty(txt_WsC49.Text) &&
                !String.IsNullOrEmpty(txt_WsC50.Text) &&
                !String.IsNullOrEmpty(txt_WsC51.Text) &&
                !String.IsNullOrEmpty(txt_WsC52.Text) &&
                !String.IsNullOrEmpty(txt_WsC53.Text) &&
                !String.IsNullOrEmpty(txt_WsC54.Text) &&
                !String.IsNullOrEmpty(txt_WsC55.Text) &&
                !String.IsNullOrEmpty(txt_WsC56.Text) &&
                !String.IsNullOrEmpty(txt_WsC57.Text) &&
                !String.IsNullOrEmpty(txt_WsC58.Text) &&
                !String.IsNullOrEmpty(txt_WsC59.Text) &&
                !String.IsNullOrEmpty(txt_WsC60.Text) &&
                !String.IsNullOrEmpty(txt_WsC61.Text) &&
                !String.IsNullOrEmpty(txt_WsC62.Text) &&
                !String.IsNullOrEmpty(txt_WsC63.Text) &&
                !String.IsNullOrEmpty(txt_WsC64.Text) &&
                !String.IsNullOrEmpty(txt_WsC65.Text) &&
                !String.IsNullOrEmpty(txt_WsC66.Text) &&
                !String.IsNullOrEmpty(txt_WsC67.Text) &&
                !String.IsNullOrEmpty(txt_WsC68.Text) &&
                !String.IsNullOrEmpty(txt_WsC69.Text) &&
                !String.IsNullOrEmpty(txt_WsC70.Text) &&
                !String.IsNullOrEmpty(txt_WsC71.Text) &&
                !String.IsNullOrEmpty(txt_WsC72.Text) &&
                !String.IsNullOrEmpty(txt_WsC73.Text) &&
                !String.IsNullOrEmpty(txt_WsC74.Text) &&
                !String.IsNullOrEmpty(txt_WsC75.Text) &&
                !String.IsNullOrEmpty(txt_WsC76.Text) &&
                !String.IsNullOrEmpty(txt_WsB13.Text) &&
                !String.IsNullOrEmpty(txt_WsB14.Text) &&
                !String.IsNullOrEmpty(txt_WsB15.Text) &&
                !String.IsNullOrEmpty(txt_WsB16.Text) &&
                !String.IsNullOrEmpty(txt_WsB17.Text) &&
                !String.IsNullOrEmpty(txt_WsB18.Text) &&
                !String.IsNullOrEmpty(txt_WsB19.Text) &&
                !String.IsNullOrEmpty(txt_WsB20.Text) &&
                !String.IsNullOrEmpty(txt_WsB21.Text) &&
                !String.IsNullOrEmpty(txt_WsB22.Text) &&
                !String.IsNullOrEmpty(txt_WsB23.Text) &&
                !String.IsNullOrEmpty(txt_WsB24.Text) &&
                !String.IsNullOrEmpty(txt_WsB25.Text) &&
                !String.IsNullOrEmpty(txt_WsB26.Text) &&
                !String.IsNullOrEmpty(txt_WsB27.Text) &&
                !String.IsNullOrEmpty(txt_WsB28.Text) &&
                !String.IsNullOrEmpty(txt_WsB29.Text) &&
                !String.IsNullOrEmpty(txt_WsB30.Text) &&
                !String.IsNullOrEmpty(txt_WsB31.Text) &&
                !String.IsNullOrEmpty(txt_WsB32.Text) &&
                !String.IsNullOrEmpty(txt_WsB33.Text) &&
                !String.IsNullOrEmpty(txt_WsB34.Text) &&
                !String.IsNullOrEmpty(txt_WsB35.Text) &&
                !String.IsNullOrEmpty(txt_WsB36.Text) &&
                !String.IsNullOrEmpty(txt_WsB37.Text) &&
                !String.IsNullOrEmpty(txt_WsB38.Text) &&
                !String.IsNullOrEmpty(txt_WsB39.Text) &&
                !String.IsNullOrEmpty(txt_WsB40.Text) &&
                !String.IsNullOrEmpty(txt_WsB41.Text) &&
                !String.IsNullOrEmpty(txt_WsB42.Text) &&
                !String.IsNullOrEmpty(txt_WsB43.Text) &&
                !String.IsNullOrEmpty(txt_WsB44.Text) &&
                !String.IsNullOrEmpty(txt_WsB45.Text) &&
                !String.IsNullOrEmpty(txt_WsB46.Text) &&
                !String.IsNullOrEmpty(txt_WsB47.Text) &&
                !String.IsNullOrEmpty(txt_WsB48.Text) &&
                !String.IsNullOrEmpty(txt_WsB49.Text) &&
                !String.IsNullOrEmpty(txt_WsB50.Text) &&
                !String.IsNullOrEmpty(txt_WsB51.Text) &&
                !String.IsNullOrEmpty(txt_WsB52.Text) &&
                !String.IsNullOrEmpty(txt_WsB53.Text) &&
                !String.IsNullOrEmpty(txt_WsB54.Text) &&
                !String.IsNullOrEmpty(txt_WsB55.Text) &&
                !String.IsNullOrEmpty(txt_WsB56.Text) &&
                !String.IsNullOrEmpty(txt_WsB57.Text) &&
                !String.IsNullOrEmpty(txt_WsB58.Text) &&
                !String.IsNullOrEmpty(txt_WsB59.Text) &&
                !String.IsNullOrEmpty(txt_WsB60.Text) &&
                !String.IsNullOrEmpty(txt_WsB61.Text) &&
                !String.IsNullOrEmpty(txt_WsB62.Text) &&
                !String.IsNullOrEmpty(txt_WsB63.Text) &&
                !String.IsNullOrEmpty(txt_WsB64.Text) &&
                !String.IsNullOrEmpty(txt_WsB65.Text) &&
                !String.IsNullOrEmpty(txt_WsB66.Text) &&
                !String.IsNullOrEmpty(txt_WsB67.Text) &&
                !String.IsNullOrEmpty(txt_WsB68.Text) &&
                !String.IsNullOrEmpty(txt_WsB69.Text) &&
                !String.IsNullOrEmpty(txt_WsB70.Text) &&
                !String.IsNullOrEmpty(txt_WsB71.Text) &&
                !String.IsNullOrEmpty(txt_WsB72.Text) &&
                !String.IsNullOrEmpty(txt_WsB73.Text) &&
                !String.IsNullOrEmpty(txt_WsB74.Text) &&
                !String.IsNullOrEmpty(txt_WsB75.Text) &&
                !String.IsNullOrEmpty(txt_WsB76.Text)
                )
            {
                Double _div = (Convert.ToDouble(txtB7.Text) / Convert.ToDouble(txtB9.Text)) * (Convert.ToDouble(txtB3.Text) / Convert.ToDouble(txtB6.Text)) / (Convert.ToDouble(txtB4.Text) * Convert.ToDouble(txtB5.Text));

                txt_WsD13.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC13.Text) - Convert.ToDouble(txt_WsB13.Text))) * _div), 1).ToString();//AlMgO
                txt_WsD14.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC14.Text) - Convert.ToDouble(txt_WsB14.Text))) * _div), 1).ToString();//AlO
                txt_WsD15.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC15.Text) - Convert.ToDouble(txt_WsB15.Text))) * _div), 1).ToString();//AlOTiC
                txt_WsD16.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC16.Text) - Convert.ToDouble(txt_WsB16.Text))) * _div), 1).ToString();//CrO
                txt_WsD17.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC17.Text) - Convert.ToDouble(txt_WsB17.Text))) * _div), 1).ToString();//FeSiO
                txt_WsD18.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC18.Text) - Convert.ToDouble(txt_WsB18.Text))) * _div), 1).ToString();//NbB
                txt_WsD19.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC19.Text) - Convert.ToDouble(txt_WsB19.Text))) * _div), 1).ToString();//SiC
                txt_WsD20.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC20.Text) - Convert.ToDouble(txt_WsB20.Text))) * _div), 1).ToString();//SiO
                txt_WsD21.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC21.Text) - Convert.ToDouble(txt_WsB21.Text))) * _div), 1).ToString();//TiB
                txt_WsD22.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC22.Text) - Convert.ToDouble(txt_WsB22.Text))) * _div), 1).ToString();//TiC
                txt_WsD23.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC23.Text) - Convert.ToDouble(txt_WsB23.Text))) * _div), 1).ToString();//TiO
                txt_WsD24.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC24.Text) - Convert.ToDouble(txt_WsB24.Text))) * _div), 1).ToString();//TiSn
                txt_WsD25.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC25.Text) - Convert.ToDouble(txt_WsB25.Text))) * _div), 1).ToString();//TiV
                txt_WsD26.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC26.Text) - Convert.ToDouble(txt_WsB26.Text))) * _div), 1).ToString();//WC
                txt_WsD27.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC27.Text) - Convert.ToDouble(txt_WsB27.Text))) * _div), 1).ToString();//ZrC
                txt_WsD28.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC28.Text) - Convert.ToDouble(txt_WsB28.Text))) * _div), 1).ToString();//ZrO
                txt_WsD29.Text = (Convert.ToDouble(txt_WsD13.Text) +
                                  Convert.ToDouble(txt_WsD14.Text) +
                                  Convert.ToDouble(txt_WsD15.Text) +
                                  Convert.ToDouble(txt_WsD16.Text) +
                                  Convert.ToDouble(txt_WsD17.Text) +
                                  Convert.ToDouble(txt_WsD18.Text) +
                                  Convert.ToDouble(txt_WsD19.Text) +
                                  Convert.ToDouble(txt_WsD20.Text) +
                                  Convert.ToDouble(txt_WsD21.Text) +
                                  Convert.ToDouble(txt_WsD22.Text) +
                                  Convert.ToDouble(txt_WsD23.Text) +
                                  Convert.ToDouble(txt_WsD24.Text) +
                                  Convert.ToDouble(txt_WsD25.Text) +
                                  Convert.ToDouble(txt_WsD26.Text) +
                                  Convert.ToDouble(txt_WsD27.Text) +
                                  Convert.ToDouble(txt_WsD28.Text)).ToString();
                txt_WsD30.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC30.Text) - Convert.ToDouble(txt_WsB30.Text))) * _div), 1).ToString();//Nd based
                txt_WsD31.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC31.Text) - Convert.ToDouble(txt_WsB31.Text))) * _div), 1).ToString();//Sm based
                txt_WsD32.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC32.Text) - Convert.ToDouble(txt_WsB32.Text))) * _div), 1).ToString();//Sr based
                txt_WsD33.Text = (Convert.ToDouble(txt_WsD30.Text) +
                                  Convert.ToDouble(txt_WsD31.Text) +
                                  Convert.ToDouble(txt_WsD32.Text)).ToString();
                txt_WsD34.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC34.Text) - Convert.ToDouble(txt_WsB34.Text))) * _div), 1).ToString();//SST300s (Fe/Cr/Ni) 
                txt_WsD35.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC35.Text) - Convert.ToDouble(txt_WsB35.Text))) * _div), 1).ToString();//SST400s (Fe/Cr)
                txt_WsD36.Text = (Convert.ToDouble(txt_WsD34.Text) +
                                  Convert.ToDouble(txt_WsD35.Text)).ToString();
                txt_WsD37.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC37.Text) - Convert.ToDouble(txt_WsB37.Text))) * _div), 1).ToString();//Ag based
                txt_WsD38.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC38.Text) - Convert.ToDouble(txt_WsB38.Text))) * _div), 1).ToString();//Al based
                txt_WsD39.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC39.Text) - Convert.ToDouble(txt_WsB39.Text))) * _div), 1).ToString();//Au based
                txt_WsD40.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC40.Text) - Convert.ToDouble(txt_WsB40.Text))) * _div), 1).ToString();//Cu based
                txt_WsD41.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC41.Text) - Convert.ToDouble(txt_WsB41.Text))) * _div), 1).ToString();//Fe based
                txt_WsD42.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC42.Text) - Convert.ToDouble(txt_WsB42.Text))) * _div), 1).ToString();//MnCrS
                txt_WsD43.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC43.Text) - Convert.ToDouble(txt_WsB43.Text))) * _div), 1).ToString();//Ni based
                txt_WsD44.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC44.Text) - Convert.ToDouble(txt_WsB44.Text))) * _div), 1).ToString();//NiP
                txt_WsD45.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC45.Text) - Convert.ToDouble(txt_WsB45.Text))) * _div), 1).ToString();//Pt based
                txt_WsD46.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC46.Text) - Convert.ToDouble(txt_WsB46.Text))) * _div), 1).ToString();//Sb based
                txt_WsD47.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC47.Text) - Convert.ToDouble(txt_WsB47.Text))) * _div), 1).ToString();//Sn based
                txt_WsD48.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC48.Text) - Convert.ToDouble(txt_WsB48.Text))) * _div), 1).ToString();//SnPb
                txt_WsD49.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC49.Text) - Convert.ToDouble(txt_WsB49.Text))) * _div), 1).ToString();//Zn based
                txt_WsD50.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC50.Text) - Convert.ToDouble(txt_WsB50.Text))) * _div), 1).ToString();//AlSi(FeCrCuZnMn)
                txt_WsD51.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC51.Text) - Convert.ToDouble(txt_WsB51.Text))) * _div), 1).ToString();//NiFe
                txt_WsD52.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC52.Text) - Convert.ToDouble(txt_WsB52.Text))) * _div), 1).ToString();//ZnPFe
                txt_WsD53.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC53.Text) - Convert.ToDouble(txt_WsB53.Text))) * _div), 1).ToString();//CrCoNiP (disc material)
                txt_WsD54.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC54.Text) - Convert.ToDouble(txt_WsB54.Text))) * _div), 1).ToString();//NiPCr
                txt_WsD55.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC55.Text) - Convert.ToDouble(txt_WsB55.Text))) * _div), 1).ToString();//NiPCrFe
                txt_WsD56.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC56.Text) - Convert.ToDouble(txt_WsB56.Text))) * _div), 1).ToString();//CuZn
                txt_WsD57.Text = (Convert.ToDouble(txt_WsD37.Text) +
                                  Convert.ToDouble(txt_WsD38.Text) +
                                  Convert.ToDouble(txt_WsD39.Text) +
                                  Convert.ToDouble(txt_WsD40.Text) +
                                  Convert.ToDouble(txt_WsD41.Text) +
                                  Convert.ToDouble(txt_WsD42.Text) +
                                  Convert.ToDouble(txt_WsD43.Text) +
                                  Convert.ToDouble(txt_WsD44.Text) +
                                  Convert.ToDouble(txt_WsD45.Text) +
                                  Convert.ToDouble(txt_WsD46.Text) +
                                  Convert.ToDouble(txt_WsD47.Text) +
                                  Convert.ToDouble(txt_WsD48.Text) +
                                  Convert.ToDouble(txt_WsD49.Text) +
                                  Convert.ToDouble(txt_WsD50.Text) +
                                  Convert.ToDouble(txt_WsD51.Text) +
                                  Convert.ToDouble(txt_WsD52.Text) +
                                  Convert.ToDouble(txt_WsD53.Text) +
                                  Convert.ToDouble(txt_WsD54.Text) +
                                  Convert.ToDouble(txt_WsD55.Text) +
                                  Convert.ToDouble(txt_WsD56.Text)).ToString();
                txt_WsD58.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC58.Text) - Convert.ToDouble(txt_WsB58.Text))) * _div), 1).ToString();//FeO
                txt_WsD59.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC59.Text) - Convert.ToDouble(txt_WsB59.Text))) * _div), 1).ToString();//AlFeO
                txt_WsD60.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC60.Text) - Convert.ToDouble(txt_WsB60.Text))) * _div), 1).ToString();//AlNiO
                txt_WsD61.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC61.Text) - Convert.ToDouble(txt_WsB61.Text))) * _div), 1).ToString();//AlSiO
                txt_WsD62.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC62.Text) - Convert.ToDouble(txt_WsB62.Text))) * _div), 1).ToString();//Cl based
                txt_WsD63.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC63.Text) - Convert.ToDouble(txt_WsB63.Text))) * _div), 1).ToString();//FeMgSiO
                txt_WsD64.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC64.Text) - Convert.ToDouble(txt_WsB64.Text))) * _div), 1).ToString();//MgCaO
                txt_WsD65.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC65.Text) - Convert.ToDouble(txt_WsB65.Text))) * _div), 1).ToString();//MgSiO
                txt_WsD66.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC66.Text) - Convert.ToDouble(txt_WsB66.Text))) * _div), 1).ToString();//S based
                txt_WsD67.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC67.Text) - Convert.ToDouble(txt_WsB67.Text))) * _div), 1).ToString();//F based
                txt_WsD68.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC68.Text) - Convert.ToDouble(txt_WsB68.Text))) * _div), 1).ToString();//Ca based
                txt_WsD69.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC69.Text) - Convert.ToDouble(txt_WsB69.Text))) * _div), 1).ToString();//Na based
                txt_WsD70.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC70.Text) - Convert.ToDouble(txt_WsB70.Text))) * _div), 1).ToString();//K based
                txt_WsD71.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC71.Text) - Convert.ToDouble(txt_WsB71.Text))) * _div), 1).ToString();//Anodised Al
                txt_WsD72.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC72.Text) - Convert.ToDouble(txt_WsB72.Text))) * _div), 1).ToString();//PZT
                txt_WsD73.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC73.Text) - Convert.ToDouble(txt_WsB73.Text))) * _div), 1).ToString();//Pb base
                txt_WsD74.Text = Math.Round((CustomUtils.GetMax((Convert.ToDouble(txt_WsC74.Text) - Convert.ToDouble(txt_WsB74.Text))) * _div), 1).ToString(); ;//Others
                txt_WsD75.Text = (Convert.ToDouble(txt_WsD58.Text) +
                                  Convert.ToDouble(txt_WsD59.Text) +
                                  Convert.ToDouble(txt_WsD60.Text) +
                                  Convert.ToDouble(txt_WsD61.Text) +
                                  Convert.ToDouble(txt_WsD62.Text) +
                                  Convert.ToDouble(txt_WsD63.Text) +
                                  Convert.ToDouble(txt_WsD64.Text) +
                                  Convert.ToDouble(txt_WsD65.Text) +
                                  Convert.ToDouble(txt_WsD66.Text) +
                                  Convert.ToDouble(txt_WsD67.Text) +
                                  Convert.ToDouble(txt_WsD68.Text) +
                                  Convert.ToDouble(txt_WsD69.Text) +
                                  Convert.ToDouble(txt_WsD70.Text) +
                                  Convert.ToDouble(txt_WsD71.Text) +
                                  Convert.ToDouble(txt_WsD72.Text) +
                                  Convert.ToDouble(txt_WsD73.Text) +
                                  Convert.ToDouble(txt_WsD74.Text)).ToString();

                txt_WsD76.Text = (Convert.ToDouble(txt_WsD29.Text) +
                                    Convert.ToDouble(txt_WsD33.Text) +
                                    Convert.ToDouble(txt_WsD36.Text) +
                                    Convert.ToDouble(txt_WsD57.Text) +
                                    Convert.ToDouble(txt_WsD75.Text)).ToString();

            }
            #endregion
        }

        #endregion

        protected void cbLPCType68_CheckedChanged(object sender, EventArgs e)
        {
            cbLPCType132.Checked = !cbLPCType68.Checked;

        }

        protected void cbLPCType132_CheckedChanged(object sender, EventArgs e)
        {
            cbLPCType68.Checked = !cbLPCType132.Checked;
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            CalculateCas();
            btnSubmit.Enabled = true;
        }

        protected void txtB8_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtB9.Text) && !String.IsNullOrEmpty(txtB7.Text))
            {
                txtB8.Text = (Convert.ToDouble(txtB9.Text) / Convert.ToDouble(txtB7.Text) * 100).ToString();
            }
        }

    }
}
