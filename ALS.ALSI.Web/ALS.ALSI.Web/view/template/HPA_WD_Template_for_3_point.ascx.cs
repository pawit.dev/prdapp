﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.template
{
    public partial class HPA_WD_Template_for_3_point : System.Web.UI.UserControl
    {

        #region "Property"

        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        public job_sample jobSample
        {
            get { return (job_sample)Session["job_sample"]; }
            set { Session["job_sample"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int JobID
        {
            get { return (int)Session[GetType().Name + "JobID"]; }
            set { Session[GetType().Name + "JobID"] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public template_wd_hpa_for3_coverpage HpaFor3
        {
            get { return (template_wd_hpa_for3_coverpage)Session[GetType().Name + "HpaFor3"]; }
            set { Session[GetType().Name + "HpaFor3"] = value; }
        }

        private void initialPage()
        {
            #region "Initial UI Component"
            ddlAssignTo.Items.Clear();
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LOGIN_SELECT_SPEC), Convert.ToInt16(StatusEnum.LOGIN_SELECT_SPEC) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.CHEMIST_TESTING), Convert.ToInt16(StatusEnum.CHEMIST_TESTING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_CHECKING), Convert.ToInt16(StatusEnum.SR_CHEMIST_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_WORD), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_WORD) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_CHECKING), Convert.ToInt16(StatusEnum.LABMANAGER_CHECKING) + ""));
            ddlAssignTo.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.ADMIN_CONVERT_PDF), Convert.ToInt16(StatusEnum.ADMIN_CONVERT_PDF) + ""));


            ddlComponent.Items.Clear();
            ddlComponent.DataSource = new tb_component_hpa_for3().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.WD));
            ddlComponent.DataBind();
            ddlComponent.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));

            ddlSpecification.Items.Clear();
            ddlSpecification.DataSource = new tb_detailspec_hpa_for3().SelectBySpecificationID(Convert.ToInt16(SpecificationEnum.WD));
            ddlSpecification.DataBind();
            ddlSpecification.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));
            #endregion

            #region "job Info"
            job_info job = new job_info().SelectByID(this.JobID);

            lbPoNo.Text = (job.customer_po_ref == null) ? String.Empty : job.customer_po_ref.ToString();
            lbDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            lbCompany.Text = String.Format("{0}<br />{1}", job.m_customer.company_name, job.m_customer.address);
            lbDateSampleReceived.Text = Convert.ToDateTime(job.date_of_receive).ToString("MM/dd/yyyy");

            this.jobSample = new job_sample().SelectByID(this.SampleID);

            if (this.jobSample != null)
            {


                lbRefNo.Text = this.jobSample.job_number.ToString();
                lbDownloadName.Text = this.jobSample.job_number.ToString();
                lbDateTestCompleted.Text = Convert.ToDateTime(this.jobSample.due_date).ToString("MM/dd/yyyy");

                lbSampleDescription.Text = String.Format("Description:{0}<br />Model:{1}<br />Surface Area:{2}<br />Remark:{3}<br />", this.jobSample.description, this.jobSample.model, this.jobSample.surface_area, this.jobSample.remarks);

                RoleEnum userRole = (RoleEnum)Enum.Parse(typeof(RoleEnum), userLogin.role_id.ToString(), true);

                StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);

                lbJobStatus.Text = Constants.GetEnumDescription(status);
                ddlStatus.Items.Clear();
                switch (status)
                {
                    case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                        break;
                    case StatusEnum.LOGIN_SELECT_SPEC:
                        pSpecification.Visible = true;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.LOGIN) ? ((this.jobSample.step1owner == null) ? true : ((this.jobSample.step1owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.CHEMIST_TESTING:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        btnSubmit.Visible = (userRole == RoleEnum.CHEMIST) ? ((this.jobSample.step2owner == null) ? true : ((this.jobSample.step2owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.SR_CHEMIST_CHECKING:
                    case StatusEnum.SR_CHEMIST_APPROVE:
                    case StatusEnum.SR_CHEMIST_DISAPPROVE:

                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_APPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.SR_CHEMIST_DISAPPROVE), Convert.ToInt16(StatusEnum.SR_CHEMIST_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.SR_CHEMIST) ? ((this.jobSample.step3owner == null) ? true : ((this.jobSample.step3owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_WORD:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step4owner == null) ? true : ((this.jobSample.step4owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.LABMANAGER_CHECKING:
                    case StatusEnum.LABMANAGER_APPROVE:
                    case StatusEnum.LABMANAGER_DISAPPROVE:
                        pSpecification.Visible = false;
                        pStatus.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        pUploadfile.Visible = false;
                        pDownload.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_APPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_APPROVE) + ""));
                        ddlStatus.Items.Add(new ListItem(Constants.GetEnumDescription(StatusEnum.LABMANAGER_DISAPPROVE), Convert.ToInt16(StatusEnum.LABMANAGER_DISAPPROVE) + ""));
                        btnSubmit.Visible = (userRole == RoleEnum.LABMANAGER) ? ((this.jobSample.step5owner == null) ? true : ((this.jobSample.step5owner == userLogin.id) ? true : false)) : false;
                        break;
                    case StatusEnum.ADMIN_CONVERT_PDF:
                        pSpecification.Visible = false;
                        pStatus.Visible = false;
                        pUploadfile.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        pDownload.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        btnSubmit.Visible = (userRole == RoleEnum.ADMIN) ? ((this.jobSample.step6owner == null) ? true : ((this.jobSample.step6owner == userLogin.id) ? true : false)) : false;
                        break;
                }

            }
            #endregion

            #region "WorkSheet"
            this.HpaFor3 = new template_wd_hpa_for3_coverpage().SelectBySampleID(this.SampleID);

            if (this.HpaFor3 != null)
            {
                #region "Initial Spec Detail & Component"
                ddlSpecification.SelectedValue = this.HpaFor3.detail_spec_id.ToString();
                ddlComponent.SelectedValue = this.HpaFor3.component_id.ToString();

                tb_detailspec_hpa_for3 detailSpec = new tb_detailspec_hpa_for3().SelectByID(int.Parse(ddlSpecification.SelectedValue));
                if (detailSpec != null)
                {
                    lbDocNo.Text = detailSpec.B;
                    lbComponent.Text = detailSpec.A;
                    lbB34.Text = (detailSpec.D.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.D)));
                    lbC34.Text = (detailSpec.E.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.E)));
                    lbD34.Text = (detailSpec.F.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.F)));
                    lbE34.Text = (detailSpec.G.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.G)));

                }
                tb_component_hpa_for3 comp = new tb_component_hpa_for3().SelectByID(int.Parse(ddlComponent.SelectedValue));
                if (comp != null)
                {
                    lbA23.Text = String.Format("Procedure No:{0}", comp.B);
                    lbC23.Text = comp.D;
                    lbC23.Text = comp.E;
                    lbC23.Text = comp.F;
                }
                #endregion

                #region "COLUMN B"
                //lbB56.Text = this.HpaFor3.ws_b56;
                lbB57.Text = this.HpaFor3.ws_b57;
                lbB58.Text = this.HpaFor3.ws_b58;
                lbB59.Text = this.HpaFor3.ws_b59;
                lbB60.Text = this.HpaFor3.ws_b60;
                lbB61.Text = this.HpaFor3.ws_b61;
                lbB62.Text = this.HpaFor3.ws_b62;
                lbB63.Text = this.HpaFor3.ws_b63;
                lbB64.Text = this.HpaFor3.ws_b64;
                lbB65.Text = this.HpaFor3.ws_b65;
                lbB66.Text = this.HpaFor3.ws_b66;
                lbB67.Text = this.HpaFor3.ws_b67;
                lbB68.Text = this.HpaFor3.ws_b68;
                lbB69.Text = this.HpaFor3.ws_b69;
                lbB70.Text = this.HpaFor3.ws_b70;
                lbB71.Text = this.HpaFor3.ws_b71;
                lbB72.Text = this.HpaFor3.ws_b72;
                lbB73.Text = this.HpaFor3.ws_b73;
                lbB74.Text = this.HpaFor3.ws_b74;
                lbB75.Text = this.HpaFor3.ws_b75;
                //=SUM(B57:B75)
                lbB76.Text = CustomUtils.Sum(new String[] { 
                    lbB57.Text, 
                    lbB58.Text, 
                    lbB59.Text, 
                    lbB60.Text, 
                    lbB61.Text, 
                    lbB62.Text, 
                    lbB63.Text, 
                    lbB64.Text, 
                    lbB65.Text, 
                    lbB66.Text, 
                    lbB67.Text, 
                    lbB68.Text, 
                    lbB69.Text, 
                    lbB70.Text, 
                    lbB71.Text, 
                    lbB72.Text, 
                    lbB73.Text, 
                    lbB74.Text, 
                    lbB75.Text 
                }).ToString();

                lbB78.Text = this.HpaFor3.ws_b78;
                lbB79.Text = this.HpaFor3.ws_b79;
                lbB80.Text = this.HpaFor3.ws_b80;
                lbB81.Text = this.HpaFor3.ws_b81;
                lbB82.Text = this.HpaFor3.ws_b82;
                lbB83.Text = this.HpaFor3.ws_b83;
                lbB84.Text = this.HpaFor3.ws_b84;

                //=SUM(B78:B84)
                lbB85.Text = CustomUtils.Sum(new String[] { 
                    lbB78.Text,
                    lbB79.Text,
                    lbB80.Text,
                    lbB81.Text,
                    lbB82.Text,
                    lbB83.Text,
                    lbB84.Text
                }).ToString();

                //=SUM(B85,B76)
                lbB86.Text = CustomUtils.Sum(new String[] { 
                    lbB85.Text,
                    lbB86.Text
                }).ToString();

                lbB102.Text = this.HpaFor3.ws_b102;
                lbB103.Text = this.HpaFor3.ws_b103;
                lbB104.Text = this.HpaFor3.ws_b104;
                lbB105.Text = this.HpaFor3.ws_b105;
                lbB106.Text = this.HpaFor3.ws_b106;
                lbB107.Text = this.HpaFor3.ws_b107;
                lbB108.Text = this.HpaFor3.ws_b108;
                lbB109.Text = this.HpaFor3.ws_b109;
                lbB110.Text = this.HpaFor3.ws_b110;
                //=SUM(B102:B110)
                lbB111.Text = CustomUtils.Sum(new String[] { 
                    lbB102.Text,
                    lbB103.Text,
                    lbB104.Text,
                    lbB105.Text,
                    lbB106.Text,
                    lbB107.Text,
                    lbB108.Text,
                    lbB109.Text,
                    lbB110.Text
                }).ToString();

                lbB113.Text = this.HpaFor3.ws_b113;
                lbB114.Text = this.HpaFor3.ws_b114;
                lbB115.Text = this.HpaFor3.ws_b115;
                lbB116.Text = this.HpaFor3.ws_b116;
                lbB117.Text = this.HpaFor3.ws_b117;
                lbB118.Text = this.HpaFor3.ws_b118;
                lbB119.Text = this.HpaFor3.ws_b119;
                lbB120.Text = this.HpaFor3.ws_b120;
                lbB121.Text = this.HpaFor3.ws_b121;
                lbB122.Text = this.HpaFor3.ws_b122;
                lbB123.Text = this.HpaFor3.ws_b123;
                lbB124.Text = this.HpaFor3.ws_b124;
                lbB125.Text = this.HpaFor3.ws_b125;
                lbB126.Text = this.HpaFor3.ws_b126;
                lbB127.Text = this.HpaFor3.ws_b127;
                lbB128.Text = this.HpaFor3.ws_b128;
                lbB129.Text = this.HpaFor3.ws_b129;
                lbB130.Text = this.HpaFor3.ws_b130;
                lbB131.Text = this.HpaFor3.ws_b131;
                lbB132.Text = this.HpaFor3.ws_b132;
                lbB133.Text = this.HpaFor3.ws_b133;
                lbB134.Text = this.HpaFor3.ws_b134;
                lbB135.Text = this.HpaFor3.ws_b135;
                lbB136.Text = this.HpaFor3.ws_b136;
                lbB137.Text = this.HpaFor3.ws_b137;
                lbB138.Text = this.HpaFor3.ws_b138;
                lbB139.Text = this.HpaFor3.ws_b139;
                lbB140.Text = this.HpaFor3.ws_b140;
                lbB141.Text = this.HpaFor3.ws_b141;
                lbB142.Text = this.HpaFor3.ws_b142;
                lbB143.Text = this.HpaFor3.ws_b143;
                lbB144.Text = this.HpaFor3.ws_b144;
                lbB145.Text = this.HpaFor3.ws_b145;
                lbB146.Text = this.HpaFor3.ws_b146;
                lbB147.Text = this.HpaFor3.ws_b147;
                lbB148.Text = this.HpaFor3.ws_b148;
                lbB149.Text = this.HpaFor3.ws_b149;
                lbB150.Text = this.HpaFor3.ws_b150;
                lbB151.Text = this.HpaFor3.ws_b151;
                lbB152.Text = this.HpaFor3.ws_b152;
                lbB153.Text = this.HpaFor3.ws_b153;
                lbB154.Text = this.HpaFor3.ws_b154;
                lbB155.Text = this.HpaFor3.ws_b155;
                lbB156.Text = this.HpaFor3.ws_b156;
                lbB157.Text = this.HpaFor3.ws_b157;
                lbB158.Text = this.HpaFor3.ws_b158;
                //=SUM(B113:B128,B130:B157)
                lbB159.Text = CustomUtils.Sum(new String[] { 
                    lbB113.Text,
                    lbB114.Text,
                    lbB115.Text,
                    lbB116.Text,
                    lbB117.Text,
                    lbB118.Text,
                    lbB119.Text,
                    lbB120.Text,
                    lbB121.Text,
                    lbB122.Text,                                                          
                    lbB123.Text,
                    lbB124.Text,
                    lbB125.Text,
                    lbB126.Text,
                    lbB127.Text,
                    lbB128.Text,                                                      
                    lbB130.Text,                                                       
                    lbB131.Text,                                                        
                    lbB132.Text,                                                           
                    lbB133.Text,                                                        
                    lbB134.Text,                                                       
                    lbB135.Text,                                                      
                    lbB136.Text,                                                    
                    lbB137.Text,                                                    
                    lbB138.Text,                                                    
                    lbB139.Text,                                                       
                    lbB140.Text,                                                      
                    lbB141.Text,
                    lbB142.Text,
                    lbB143.Text,
                    lbB144.Text,
                    lbB145.Text,
                    lbB146.Text,
                    lbB147.Text,
                    lbB148.Text,
                    lbB149.Text,
                    lbB150.Text,
                    lbB151.Text,
                    lbB152.Text,
                    lbB153.Text,
                    lbB154.Text,
                    lbB155.Text,
                    lbB156.Text,
                    lbB157.Text,
                    lbB158.Text
                }).ToString();
                //=SUM(B76,B85,B111,B129,B159)
                lbB159.Text = CustomUtils.Sum(new String[] { 
                    lbB76.Text,
                    lbB111.Text,
                    lbB129.Text,
                    lbB159.Text
                }).ToString();

                lbB173.Text = this.HpaFor3.ws_b173;
                lbB174.Text = this.HpaFor3.ws_b174;
                lbB175.Text = this.HpaFor3.ws_b175;
                lbB176.Text = this.HpaFor3.ws_b176;
                lbB177.Text = this.HpaFor3.ws_b177;
                lbB178.Text = this.HpaFor3.ws_b178;
                lbB179.Text = this.HpaFor3.ws_b179;
                lbB180.Text = this.HpaFor3.ws_b180;
                lbB181.Text = this.HpaFor3.ws_b181;
                lbB182.Text = this.HpaFor3.ws_b182;
                lbB183.Text = this.HpaFor3.ws_b183;
                lbB184.Text = this.HpaFor3.ws_b184;
                lbB185.Text = this.HpaFor3.ws_b185;
                lbB186.Text = this.HpaFor3.ws_b186;
                lbB187.Text = this.HpaFor3.ws_b187;
                lbB188.Text = this.HpaFor3.ws_b188;
                lbB189.Text = this.HpaFor3.ws_b189;
                lbB190.Text = this.HpaFor3.ws_b190;
                lbB191.Text = this.HpaFor3.ws_b191;
                //=SUM(B173:B191)
                lbB192.Text = CustomUtils.Sum(new String[] { 
                    lbB173.Text,
                    lbB174.Text,
                    lbB175.Text,
                    lbB176.Text,
                    lbB177.Text,
                    lbB178.Text,
                    lbB179.Text,
                    lbB180.Text,
                    lbB181.Text,
                    lbB182.Text,
                    lbB183.Text,
                    lbB184.Text,
                    lbB185.Text,
                    lbB186.Text,
                    lbB187.Text,
                    lbB188.Text,
                    lbB189.Text,
                    lbB190.Text,
                    lbB191.Text
                }).ToString();


                lbB194.Text = this.HpaFor3.ws_b194;
                lbB195.Text = this.HpaFor3.ws_b195;
                lbB196.Text = this.HpaFor3.ws_b196;
                lbB197.Text = this.HpaFor3.ws_b197;
                lbB198.Text = this.HpaFor3.ws_b198;
                lbB199.Text = this.HpaFor3.ws_b199;
                lbB200.Text = this.HpaFor3.ws_b200;
                //=SUM(B194:B200)
                lbB201.Text = CustomUtils.Sum(new String[] { 
                    lbB194.Text,
                    lbB195.Text,
                    lbB196.Text,
                    lbB197.Text,
                    lbB198.Text,
                    lbB199.Text,
                    lbB200.Text
                }).ToString();
                //=SUM(B201,B192)
                lbB202.Text = CustomUtils.Sum(new String[] { 
                    lbB201.Text,
                    lbB192.Text
                }).ToString();

                lbB218.Text = this.HpaFor3.ws_b218;
                lbB219.Text = this.HpaFor3.ws_b219;
                lbB220.Text = this.HpaFor3.ws_b220;
                lbB221.Text = this.HpaFor3.ws_b221;
                lbB222.Text = this.HpaFor3.ws_b222;
                lbB223.Text = this.HpaFor3.ws_b223;
                lbB224.Text = this.HpaFor3.ws_b224;
                lbB225.Text = this.HpaFor3.ws_b225;
                lbB226.Text = this.HpaFor3.ws_b226;
                //=SUM(B218,B219,B220,B221,B222,B223,B224,B225,B226)
                lbB227.Text = CustomUtils.Sum(new String[] { 
                    lbB218.Text,
                    lbB219.Text,
                    lbB220.Text,
                    lbB221.Text,
                    lbB222.Text,
                    lbB223.Text,
                    lbB224.Text,
                    lbB225.Text,
                    lbB226.Text
                }).ToString();

                lbB229.Text = this.HpaFor3.ws_b229;
                lbB230.Text = this.HpaFor3.ws_b230;
                lbB231.Text = this.HpaFor3.ws_b231;
                lbB232.Text = this.HpaFor3.ws_b232;
                lbB233.Text = this.HpaFor3.ws_b233;
                lbB234.Text = this.HpaFor3.ws_b234;
                lbB235.Text = this.HpaFor3.ws_b235;
                lbB236.Text = this.HpaFor3.ws_b236;
                lbB237.Text = this.HpaFor3.ws_b237;
                lbB238.Text = this.HpaFor3.ws_b238;
                lbB239.Text = this.HpaFor3.ws_b239;
                lbB240.Text = this.HpaFor3.ws_b240;
                lbB241.Text = this.HpaFor3.ws_b241;
                lbB242.Text = this.HpaFor3.ws_b242;
                lbB243.Text = this.HpaFor3.ws_b243;
                lbB244.Text = this.HpaFor3.ws_b244;
                lbB245.Text = this.HpaFor3.ws_b245;
                lbB246.Text = this.HpaFor3.ws_b246;
                lbB247.Text = this.HpaFor3.ws_b247;
                lbB248.Text = this.HpaFor3.ws_b248;
                lbB249.Text = this.HpaFor3.ws_b249;
                lbB250.Text = this.HpaFor3.ws_b250;
                lbB251.Text = this.HpaFor3.ws_b251;
                lbB252.Text = this.HpaFor3.ws_b252;
                lbB253.Text = this.HpaFor3.ws_b253;
                lbB254.Text = this.HpaFor3.ws_b254;
                lbB255.Text = this.HpaFor3.ws_b255;
                lbB256.Text = this.HpaFor3.ws_b256;
                lbB257.Text = this.HpaFor3.ws_b257;
                lbB258.Text = this.HpaFor3.ws_b258;
                lbB259.Text = this.HpaFor3.ws_b259;
                lbB260.Text = this.HpaFor3.ws_b260;
                lbB261.Text = this.HpaFor3.ws_b261;
                lbB262.Text = this.HpaFor3.ws_b262;
                lbB263.Text = this.HpaFor3.ws_b263;
                lbB264.Text = this.HpaFor3.ws_b264;
                lbB265.Text = this.HpaFor3.ws_b265;
                lbB266.Text = this.HpaFor3.ws_b266;
                lbB267.Text = this.HpaFor3.ws_b267;
                lbB268.Text = this.HpaFor3.ws_b268;
                lbB269.Text = this.HpaFor3.ws_b269;
                lbB270.Text = this.HpaFor3.ws_b270;
                lbB271.Text = this.HpaFor3.ws_b271;
                lbB272.Text = this.HpaFor3.ws_b272;
                lbB273.Text = this.HpaFor3.ws_b273;
                lbB274.Text = this.HpaFor3.ws_b274;
                //=SUM(B230:B244,B246:B273)
                lbB275.Text = CustomUtils.Sum(new String[] { 
                    lbB229.Text,
                    lbB230.Text,
                    lbB231.Text,
                    lbB232.Text,
                    lbB233.Text,
                    lbB234.Text,
                    lbB235.Text,
                    lbB236.Text,
                    lbB237.Text,
                    lbB238.Text,
                    lbB239.Text,
                    lbB240.Text,
                    lbB241.Text,
                    lbB242.Text,
                    lbB243.Text,
                    lbB244.Text,
                    lbB246.Text,
                    lbB247.Text,
                    lbB248.Text,
                    lbB249.Text,
                    lbB250.Text,
                    lbB251.Text,
                    lbB252.Text,
                    lbB253.Text,
                    lbB254.Text,
                    lbB255.Text,
                    lbB256.Text,
                    lbB257.Text,
                    lbB258.Text,
                    lbB259.Text,
                    lbB260.Text,
                    lbB261.Text,
                    lbB262.Text,
                    lbB263.Text,
                    lbB264.Text,
                    lbB265.Text,
                    lbB266.Text,
                    lbB267.Text,
                    lbB268.Text,
                    lbB269.Text,
                    lbB270.Text,
                    lbB271.Text,
                    lbB272.Text,
                    lbB273.Text,
                    lbB274.Text
                }).ToString();
                //=SUM(B192,B201,B227,B245,B275)
                lbB276.Text = CustomUtils.Sum(new String[] { 
                    lbB192.Text,
                    lbB201.Text,
                    lbB227.Text,
                    lbB245.Text,
                    lbB275.Text
                }).ToString();
                lbB289.Text = this.HpaFor3.ws_b289;
                lbB290.Text = this.HpaFor3.ws_b290;
                lbB291.Text = this.HpaFor3.ws_b291;
                lbB292.Text = this.HpaFor3.ws_b292;
                lbB293.Text = this.HpaFor3.ws_b293;
                lbB294.Text = this.HpaFor3.ws_b294;
                lbB295.Text = this.HpaFor3.ws_b295;
                lbB296.Text = this.HpaFor3.ws_b296;
                lbB297.Text = this.HpaFor3.ws_b297;
                lbB298.Text = this.HpaFor3.ws_b298;
                lbB299.Text = this.HpaFor3.ws_b299;
                lbB300.Text = this.HpaFor3.ws_b300;
                lbB301.Text = this.HpaFor3.ws_b301;
                lbB302.Text = this.HpaFor3.ws_b302;
                lbB303.Text = this.HpaFor3.ws_b303;
                lbB304.Text = this.HpaFor3.ws_b304;
                lbB305.Text = this.HpaFor3.ws_b305;
                lbB306.Text = this.HpaFor3.ws_b306;
                lbB307.Text = this.HpaFor3.ws_b307;

                //=SUM(B289:B307)
                lbB308.Text = CustomUtils.Sum(new String[] { 
                    lbB289.Text,
                    lbB290.Text,
                    lbB291.Text,
                    lbB292.Text,
                    lbB293.Text,
                    lbB294.Text,
                    lbB295.Text,
                    lbB296.Text,
                    lbB297.Text,
                    lbB298.Text,
                    lbB299.Text,
                    lbB300.Text,
                    lbB301.Text,
                    lbB302.Text,
                    lbB303.Text,
                    lbB304.Text,
                    lbB305.Text,
                    lbB306.Text,
                    lbB307.Text
                }).ToString();

                lbB310.Text = this.HpaFor3.ws_b310;
                lbB311.Text = this.HpaFor3.ws_b311;
                lbB312.Text = this.HpaFor3.ws_b312;
                lbB313.Text = this.HpaFor3.ws_b313;
                lbB314.Text = this.HpaFor3.ws_b314;
                lbB315.Text = this.HpaFor3.ws_b315;
                lbB316.Text = this.HpaFor3.ws_b316;

                //=SUM(B310:B316)
                lbB317.Text = CustomUtils.Sum(new String[] { 
                    lbB310.Text,
                    lbB311.Text,
                    lbB312.Text,
                    lbB313.Text,
                    lbB314.Text,
                    lbB315.Text,
                    lbB316.Text
                }).ToString();
                //=SUM(B317,B308)
                lbB318.Text = CustomUtils.Sum(new String[] { 
                    lbB317.Text,
                    lbB308.Text
                }).ToString();

                lbB334.Text = this.HpaFor3.ws_b334;
                lbB335.Text = this.HpaFor3.ws_b335;
                lbB336.Text = this.HpaFor3.ws_b336;
                lbB337.Text = this.HpaFor3.ws_b337;
                lbB338.Text = this.HpaFor3.ws_b338;
                lbB339.Text = this.HpaFor3.ws_b339;
                lbB340.Text = this.HpaFor3.ws_b340;
                lbB341.Text = this.HpaFor3.ws_b341;
                lbB342.Text = this.HpaFor3.ws_b342;
                //=SUM(B334:B342)
                lbB343.Text = CustomUtils.Sum(new String[] { 
                    lbB334.Text,
                    lbB335.Text,
                    lbB336.Text,
                    lbB337.Text,
                    lbB338.Text,
                    lbB339.Text,
                    lbB340.Text,
                    lbB341.Text,
                    lbB342.Text
                }).ToString();

                lbB345.Text = this.HpaFor3.ws_b345;
                lbB346.Text = this.HpaFor3.ws_b346;
                lbB347.Text = this.HpaFor3.ws_b347;
                lbB348.Text = this.HpaFor3.ws_b348;
                lbB349.Text = this.HpaFor3.ws_b349;
                lbB350.Text = this.HpaFor3.ws_b350;
                lbB351.Text = this.HpaFor3.ws_b351;
                lbB352.Text = this.HpaFor3.ws_b352;
                lbB353.Text = this.HpaFor3.ws_b353;
                lbB354.Text = this.HpaFor3.ws_b354;
                lbB355.Text = this.HpaFor3.ws_b355;
                lbB356.Text = this.HpaFor3.ws_b356;
                lbB357.Text = this.HpaFor3.ws_b357;
                lbB358.Text = this.HpaFor3.ws_b358;
                lbB359.Text = this.HpaFor3.ws_b359;
                lbB360.Text = this.HpaFor3.ws_b360;
                lbB361.Text = this.HpaFor3.ws_b361;
                lbB362.Text = this.HpaFor3.ws_b362;
                lbB363.Text = this.HpaFor3.ws_b363;
                lbB364.Text = this.HpaFor3.ws_b364;
                lbB365.Text = this.HpaFor3.ws_b365;
                lbB366.Text = this.HpaFor3.ws_b366;
                lbB367.Text = this.HpaFor3.ws_b367;
                lbB368.Text = this.HpaFor3.ws_b368;
                lbB369.Text = this.HpaFor3.ws_b369;
                lbB370.Text = this.HpaFor3.ws_b370;
                lbB371.Text = this.HpaFor3.ws_b371;
                lbB372.Text = this.HpaFor3.ws_b372;
                lbB373.Text = this.HpaFor3.ws_b373;
                lbB374.Text = this.HpaFor3.ws_b374;
                lbB375.Text = this.HpaFor3.ws_b375;
                lbB376.Text = this.HpaFor3.ws_b376;
                lbB377.Text = this.HpaFor3.ws_b377;
                lbB378.Text = this.HpaFor3.ws_b378;
                lbB379.Text = this.HpaFor3.ws_b379;
                lbB380.Text = this.HpaFor3.ws_b380;
                lbB381.Text = this.HpaFor3.ws_b381;
                lbB382.Text = this.HpaFor3.ws_b382;
                lbB383.Text = this.HpaFor3.ws_b383;
                lbB384.Text = this.HpaFor3.ws_b384;
                lbB385.Text = this.HpaFor3.ws_b385;
                lbB386.Text = this.HpaFor3.ws_b386;
                lbB387.Text = this.HpaFor3.ws_b387;
                lbB388.Text = this.HpaFor3.ws_b388;
                lbB389.Text = this.HpaFor3.ws_b389;
                lbB390.Text = this.HpaFor3.ws_b390;
                //=SUM(B346:B360,B362:B389)
                lbB391.Text = CustomUtils.Sum(new String[] { 
                    lbB346.Text,
                    lbB347.Text,
                    lbB348.Text,
                    lbB349.Text,
                    lbB350.Text,
                    lbB351.Text,
                    lbB352.Text,
                    lbB353.Text,
                    lbB354.Text,
                    lbB355.Text,
                    lbB356.Text,
                    lbB357.Text,
                    lbB358.Text,
                    lbB359.Text,
                    lbB360.Text,
                    lbB362.Text,
                    lbB363.Text,
                    lbB364.Text,
                    lbB365.Text,
                    lbB366.Text,
                    lbB367.Text,
                    lbB368.Text,
                    lbB369.Text,
                    lbB370.Text,
                    lbB371.Text,
                    lbB372.Text,
                    lbB373.Text,
                    lbB374.Text,
                    lbB375.Text,
                    lbB376.Text,
                    lbB377.Text,
                    lbB378.Text,
                    lbB379.Text,
                    lbB380.Text,
                    lbB381.Text,
                    lbB382.Text,
                    lbB383.Text,
                    lbB384.Text,
                    lbB385.Text,
                    lbB386.Text,
                    lbB387.Text,
                    lbB388.Text,
                    lbB389.Text,
                    lbB390.Text
                }).ToString();
                //=SUM(B308,B317,B343,B361,B391)
                lbB392.Text = CustomUtils.Sum(new String[] { 
                    lbB308.Text,
                    lbB317.Text,
                    lbB343.Text,
                    lbB361.Text,
                    lbB391.Text
                }).ToString();
                #endregion

                #region "RESULTS && PASS / FAIL"
                lbB30.Text = lbC76.Text;
                lbB31.Text = lbC192.Text;
                lbB32.Text = lbC308.Text;

                lbC30.Text = lbC129.Text;
                lbC31.Text = lbC245.Text;
                lbC32.Text = lbC361.Text;

                lbD30.Text = lbC111.Text;
                lbD31.Text = lbC227.Text;
                lbD32.Text = lbC343.Text;

                lbE30.Text = lbC85.Text;
                lbE31.Text = lbC201.Text;
                lbE32.Text = lbC317.Text;

                //Average result
                double[] b33 = { Convert.ToDouble(lbB31.Text), Convert.ToDouble(lbB32.Text), Convert.ToDouble(lbB33.Text) };
                double[] c33 = { Convert.ToDouble(lbC31.Text), Convert.ToDouble(lbC32.Text), Convert.ToDouble(lbC33.Text) };
                double[] d33 = { Convert.ToDouble(lbD31.Text), Convert.ToDouble(lbD32.Text), Convert.ToDouble(lbD33.Text) };
                double[] e33 = { Convert.ToDouble(lbE31.Text), Convert.ToDouble(lbE32.Text), Convert.ToDouble(lbE33.Text) };
                lbB33.Text = CustomUtils.Average(b33).ToString();
                lbC33.Text = CustomUtils.Average(c33).ToString();
                lbD33.Text = CustomUtils.Average(d33).ToString();
                lbE33.Text = CustomUtils.Average(e33).ToString();
                //PASS / FAIL
                if (detailSpec != null)
                {
                    lbB35.Text = (lbB34.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbB33.Text) >= Convert.ToDouble(detailSpec.D)) ? "FAIL" : "PASS");
                    lbC35.Text = (lbC34.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbC33.Text) >= Convert.ToDouble(detailSpec.E)) ? "FAIL" : "PASS");
                    lbD35.Text = (lbD34.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbD33.Text) >= Convert.ToDouble(detailSpec.F)) ? "FAIL" : "PASS");
                    lbE35.Text = (lbE34.Text.Equals("NA") ? "NA" : (Convert.ToDouble(lbE33.Text) >= Convert.ToDouble(detailSpec.G)) ? "FAIL" : "PASS");
                }
                #endregion

                #region "IMAGE"
                if (!String.IsNullOrEmpty(this.HpaFor3.img_path1))
                {
                    lbImgPath1.Text = String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.HpaFor3.img_path1);
                    img1.ImageUrl = lbImgPath1.Text;
                }
                if (!String.IsNullOrEmpty(this.HpaFor3.img_path2))
                {
                    lbImgPath2.Text = String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.HpaFor3.img_path2);
                    img1.ImageUrl = lbImgPath1.Text;
                }
                if (!String.IsNullOrEmpty(this.HpaFor3.img_path3))
                {
                    lbImgPath3.Text = String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.HpaFor3.img_path3);
                    img1.ImageUrl = lbImgPath3.Text;
                }
                #endregion

                this.CommandName = CommandNameEnum.Edit;
                ShowItem(this.HpaFor3.item_visible);
            }
            else
            {
                this.HpaFor3 = new template_wd_hpa_for3_coverpage();
                this.CommandName = CommandNameEnum.Add;
            }
            #endregion

            //init
            pRemark.Visible = false;
            pDisapprove.Visible = false;
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "JobID");
            Session.Remove(GetType().Name + "job_sample");
            Session.Remove(GetType().Name + "SampleID");
            Session.Remove(GetType().Name + "HpaFor3");
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.JobID = (prvPage == null) ? this.JobID : prvPage.JobID;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void ddlSpecification_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_detailspec_hpa_for3 detailSpec = new tb_detailspec_hpa_for3().SelectByID(int.Parse(ddlSpecification.SelectedValue));
            if (detailSpec != null)
            {
                lbDocNo.Text = detailSpec.B;
                lbComponent.Text = detailSpec.A;
                lbB34.Text = (detailSpec.D.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.D)));
                lbC34.Text = (detailSpec.E.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.E)));
                lbD34.Text = (detailSpec.F.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.F)));
                lbE34.Text = (detailSpec.G.Equals("NA") ? "NA" : (detailSpec.Equals("0") ? "0" : String.Format("<", detailSpec.G)));
            }
        }
        protected void ddlComponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_component_hpa_for3 comp = new tb_component_hpa_for3().SelectByID(int.Parse(ddlComponent.SelectedValue));
            if (comp != null)
            {
                lbA23.Text = String.Format("Procedure No:{0}", comp.B);
                lbC23.Text = comp.D;
                lbC23.Text = comp.E;
                lbC23.Text = comp.F;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            Boolean isValid = true;

            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                    this.jobSample.step1owner = userLogin.id;

                    this.HpaFor3.sample_id = this.SampleID;
                    this.HpaFor3.detail_spec_id = Convert.ToInt32(ddlSpecification.SelectedValue);
                    this.HpaFor3.component_id = Convert.ToInt32(ddlComponent.SelectedValue);
                    this.HpaFor3.item_visible = getItemStatus();
                    //insert filed
                    this.HpaFor3.ws_b57 = lbB57.Text;
                    this.HpaFor3.ws_b58 = lbB58.Text;
                    this.HpaFor3.ws_b59 = lbB59.Text;
                    this.HpaFor3.ws_b60 = lbB60.Text;
                    this.HpaFor3.ws_b61 = lbB61.Text;
                    this.HpaFor3.ws_b62 = lbB62.Text;
                    this.HpaFor3.ws_b63 = lbB63.Text;
                    this.HpaFor3.ws_b64 = lbB64.Text;
                    this.HpaFor3.ws_b65 = lbB65.Text;
                    this.HpaFor3.ws_b66 = lbB66.Text;
                    this.HpaFor3.ws_b67 = lbB67.Text;
                    this.HpaFor3.ws_b68 = lbB68.Text;
                    this.HpaFor3.ws_b69 = lbB69.Text;
                    this.HpaFor3.ws_b70 = lbB70.Text;
                    this.HpaFor3.ws_b71 = lbB71.Text;
                    this.HpaFor3.ws_b72 = lbB72.Text;
                    this.HpaFor3.ws_b73 = lbB73.Text;
                    this.HpaFor3.ws_b74 = lbB74.Text;
                    this.HpaFor3.ws_b75 = lbB75.Text;
                    this.HpaFor3.ws_b78 = lbB78.Text;
                    this.HpaFor3.ws_b79 = lbB79.Text;
                    this.HpaFor3.ws_b80 = lbB80.Text;
                    this.HpaFor3.ws_b81 = lbB81.Text;
                    this.HpaFor3.ws_b82 = lbB82.Text;
                    this.HpaFor3.ws_b83 = lbB83.Text;
                    this.HpaFor3.ws_b84 = lbB84.Text;
                    this.HpaFor3.ws_b102 = lbB102.Text;
                    this.HpaFor3.ws_b103 = lbB103.Text;
                    this.HpaFor3.ws_b104 = lbB104.Text;
                    this.HpaFor3.ws_b105 = lbB105.Text;
                    this.HpaFor3.ws_b106 = lbB106.Text;
                    this.HpaFor3.ws_b107 = lbB107.Text;
                    this.HpaFor3.ws_b108 = lbB108.Text;
                    this.HpaFor3.ws_b109 = lbB109.Text;
                    this.HpaFor3.ws_b110 = lbB110.Text;
                    this.HpaFor3.ws_b113 = lbB113.Text;
                    this.HpaFor3.ws_b114 = lbB114.Text;
                    this.HpaFor3.ws_b115 = lbB115.Text;
                    this.HpaFor3.ws_b116 = lbB116.Text;
                    this.HpaFor3.ws_b117 = lbB117.Text;
                    this.HpaFor3.ws_b118 = lbB118.Text;
                    this.HpaFor3.ws_b119 = lbB119.Text;
                    this.HpaFor3.ws_b120 = lbB120.Text;
                    this.HpaFor3.ws_b121 = lbB121.Text;
                    this.HpaFor3.ws_b122 = lbB122.Text;
                    this.HpaFor3.ws_b123 = lbB123.Text;
                    this.HpaFor3.ws_b124 = lbB124.Text;
                    this.HpaFor3.ws_b125 = lbB125.Text;
                    this.HpaFor3.ws_b126 = lbB126.Text;
                    this.HpaFor3.ws_b127 = lbB127.Text;
                    this.HpaFor3.ws_b128 = lbB128.Text;
                    this.HpaFor3.ws_b129 = lbB129.Text;
                    this.HpaFor3.ws_b130 = lbB130.Text;
                    this.HpaFor3.ws_b131 = lbB131.Text;
                    this.HpaFor3.ws_b132 = lbB132.Text;
                    this.HpaFor3.ws_b133 = lbB133.Text;
                    this.HpaFor3.ws_b134 = lbB134.Text;
                    this.HpaFor3.ws_b135 = lbB135.Text;
                    this.HpaFor3.ws_b136 = lbB136.Text;
                    this.HpaFor3.ws_b137 = lbB137.Text;
                    this.HpaFor3.ws_b138 = lbB138.Text;
                    this.HpaFor3.ws_b139 = lbB139.Text;
                    this.HpaFor3.ws_b140 = lbB140.Text;
                    this.HpaFor3.ws_b141 = lbB141.Text;
                    this.HpaFor3.ws_b142 = lbB142.Text;
                    this.HpaFor3.ws_b143 = lbB143.Text;
                    this.HpaFor3.ws_b144 = lbB144.Text;
                    this.HpaFor3.ws_b145 = lbB145.Text;
                    this.HpaFor3.ws_b146 = lbB146.Text;
                    this.HpaFor3.ws_b147 = lbB147.Text;
                    this.HpaFor3.ws_b148 = lbB148.Text;
                    this.HpaFor3.ws_b149 = lbB149.Text;
                    this.HpaFor3.ws_b150 = lbB150.Text;
                    this.HpaFor3.ws_b151 = lbB151.Text;
                    this.HpaFor3.ws_b152 = lbB152.Text;
                    this.HpaFor3.ws_b153 = lbB153.Text;
                    this.HpaFor3.ws_b154 = lbB154.Text;
                    this.HpaFor3.ws_b155 = lbB155.Text;
                    this.HpaFor3.ws_b156 = lbB156.Text;
                    this.HpaFor3.ws_b157 = lbB157.Text;
                    this.HpaFor3.ws_b158 = lbB158.Text;
                    this.HpaFor3.ws_b173 = lbB173.Text;
                    this.HpaFor3.ws_b174 = lbB174.Text;
                    this.HpaFor3.ws_b175 = lbB175.Text;
                    this.HpaFor3.ws_b176 = lbB176.Text;
                    this.HpaFor3.ws_b177 = lbB177.Text;
                    this.HpaFor3.ws_b178 = lbB178.Text;
                    this.HpaFor3.ws_b179 = lbB179.Text;
                    this.HpaFor3.ws_b180 = lbB180.Text;
                    this.HpaFor3.ws_b181 = lbB181.Text;
                    this.HpaFor3.ws_b182 = lbB182.Text;
                    this.HpaFor3.ws_b183 = lbB183.Text;
                    this.HpaFor3.ws_b184 = lbB184.Text;
                    this.HpaFor3.ws_b185 = lbB185.Text;
                    this.HpaFor3.ws_b186 = lbB186.Text;
                    this.HpaFor3.ws_b187 = lbB187.Text;
                    this.HpaFor3.ws_b188 = lbB188.Text;
                    this.HpaFor3.ws_b189 = lbB189.Text;
                    this.HpaFor3.ws_b190 = lbB190.Text;
                    this.HpaFor3.ws_b191 = lbB191.Text;
                    this.HpaFor3.ws_b194 = lbB194.Text;
                    this.HpaFor3.ws_b195 = lbB195.Text;
                    this.HpaFor3.ws_b196 = lbB196.Text;
                    this.HpaFor3.ws_b197 = lbB197.Text;
                    this.HpaFor3.ws_b198 = lbB198.Text;
                    this.HpaFor3.ws_b199 = lbB199.Text;
                    this.HpaFor3.ws_b200 = lbB200.Text;
                    this.HpaFor3.ws_b218 = lbB218.Text;
                    this.HpaFor3.ws_b219 = lbB219.Text;
                    this.HpaFor3.ws_b220 = lbB220.Text;
                    this.HpaFor3.ws_b221 = lbB221.Text;
                    this.HpaFor3.ws_b222 = lbB222.Text;
                    this.HpaFor3.ws_b223 = lbB223.Text;
                    this.HpaFor3.ws_b224 = lbB224.Text;
                    this.HpaFor3.ws_b225 = lbB225.Text;
                    this.HpaFor3.ws_b226 = lbB226.Text;
                    this.HpaFor3.ws_b229 = lbB229.Text;
                    this.HpaFor3.ws_b230 = lbB230.Text;
                    this.HpaFor3.ws_b231 = lbB231.Text;
                    this.HpaFor3.ws_b232 = lbB232.Text;
                    this.HpaFor3.ws_b233 = lbB233.Text;
                    this.HpaFor3.ws_b234 = lbB234.Text;
                    this.HpaFor3.ws_b235 = lbB235.Text;
                    this.HpaFor3.ws_b236 = lbB236.Text;
                    this.HpaFor3.ws_b237 = lbB237.Text;
                    this.HpaFor3.ws_b238 = lbB238.Text;
                    this.HpaFor3.ws_b239 = lbB239.Text;
                    this.HpaFor3.ws_b240 = lbB240.Text;
                    this.HpaFor3.ws_b241 = lbB241.Text;
                    this.HpaFor3.ws_b242 = lbB242.Text;
                    this.HpaFor3.ws_b243 = lbB243.Text;
                    this.HpaFor3.ws_b244 = lbB244.Text;
                    this.HpaFor3.ws_b245 = lbB245.Text;
                    this.HpaFor3.ws_b246 = lbB246.Text;
                    this.HpaFor3.ws_b247 = lbB247.Text;
                    this.HpaFor3.ws_b248 = lbB248.Text;
                    this.HpaFor3.ws_b249 = lbB249.Text;
                    this.HpaFor3.ws_b250 = lbB250.Text;
                    this.HpaFor3.ws_b251 = lbB251.Text;
                    this.HpaFor3.ws_b252 = lbB252.Text;
                    this.HpaFor3.ws_b253 = lbB253.Text;
                    this.HpaFor3.ws_b254 = lbB254.Text;
                    this.HpaFor3.ws_b255 = lbB255.Text;
                    this.HpaFor3.ws_b256 = lbB256.Text;
                    this.HpaFor3.ws_b257 = lbB257.Text;
                    this.HpaFor3.ws_b258 = lbB258.Text;
                    this.HpaFor3.ws_b259 = lbB259.Text;
                    this.HpaFor3.ws_b260 = lbB260.Text;
                    this.HpaFor3.ws_b261 = lbB261.Text;
                    this.HpaFor3.ws_b262 = lbB262.Text;
                    this.HpaFor3.ws_b263 = lbB263.Text;
                    this.HpaFor3.ws_b264 = lbB264.Text;
                    this.HpaFor3.ws_b265 = lbB265.Text;
                    this.HpaFor3.ws_b266 = lbB266.Text;
                    this.HpaFor3.ws_b267 = lbB267.Text;
                    this.HpaFor3.ws_b268 = lbB268.Text;
                    this.HpaFor3.ws_b269 = lbB269.Text;
                    this.HpaFor3.ws_b270 = lbB270.Text;
                    this.HpaFor3.ws_b271 = lbB271.Text;
                    this.HpaFor3.ws_b272 = lbB272.Text;
                    this.HpaFor3.ws_b273 = lbB273.Text;
                    this.HpaFor3.ws_b274 = lbB274.Text;
                    this.HpaFor3.ws_b289 = lbB289.Text;
                    this.HpaFor3.ws_b290 = lbB290.Text;
                    this.HpaFor3.ws_b291 = lbB291.Text;
                    this.HpaFor3.ws_b292 = lbB292.Text;
                    this.HpaFor3.ws_b293 = lbB293.Text;
                    this.HpaFor3.ws_b294 = lbB294.Text;
                    this.HpaFor3.ws_b295 = lbB295.Text;
                    this.HpaFor3.ws_b296 = lbB296.Text;
                    this.HpaFor3.ws_b297 = lbB297.Text;
                    this.HpaFor3.ws_b298 = lbB298.Text;
                    this.HpaFor3.ws_b299 = lbB299.Text;
                    this.HpaFor3.ws_b300 = lbB300.Text;
                    this.HpaFor3.ws_b301 = lbB301.Text;
                    this.HpaFor3.ws_b302 = lbB302.Text;
                    this.HpaFor3.ws_b303 = lbB303.Text;
                    this.HpaFor3.ws_b304 = lbB304.Text;
                    this.HpaFor3.ws_b305 = lbB305.Text;
                    this.HpaFor3.ws_b306 = lbB306.Text;
                    this.HpaFor3.ws_b307 = lbB307.Text;
                    this.HpaFor3.ws_b310 = lbB310.Text;
                    this.HpaFor3.ws_b311 = lbB311.Text;
                    this.HpaFor3.ws_b312 = lbB312.Text;
                    this.HpaFor3.ws_b313 = lbB313.Text;
                    this.HpaFor3.ws_b314 = lbB314.Text;
                    this.HpaFor3.ws_b315 = lbB315.Text;
                    this.HpaFor3.ws_b316 = lbB316.Text;
                    this.HpaFor3.ws_b334 = lbB334.Text;
                    this.HpaFor3.ws_b335 = lbB335.Text;
                    this.HpaFor3.ws_b336 = lbB336.Text;
                    this.HpaFor3.ws_b337 = lbB337.Text;
                    this.HpaFor3.ws_b338 = lbB338.Text;
                    this.HpaFor3.ws_b339 = lbB339.Text;
                    this.HpaFor3.ws_b340 = lbB340.Text;
                    this.HpaFor3.ws_b341 = lbB341.Text;
                    this.HpaFor3.ws_b342 = lbB342.Text;
                    this.HpaFor3.ws_b345 = lbB345.Text;
                    this.HpaFor3.ws_b346 = lbB346.Text;
                    this.HpaFor3.ws_b347 = lbB347.Text;
                    this.HpaFor3.ws_b348 = lbB348.Text;
                    this.HpaFor3.ws_b349 = lbB349.Text;
                    this.HpaFor3.ws_b350 = lbB350.Text;
                    this.HpaFor3.ws_b351 = lbB351.Text;
                    this.HpaFor3.ws_b352 = lbB352.Text;
                    this.HpaFor3.ws_b353 = lbB353.Text;
                    this.HpaFor3.ws_b354 = lbB354.Text;
                    this.HpaFor3.ws_b355 = lbB355.Text;
                    this.HpaFor3.ws_b356 = lbB356.Text;
                    this.HpaFor3.ws_b357 = lbB357.Text;
                    this.HpaFor3.ws_b358 = lbB358.Text;
                    this.HpaFor3.ws_b359 = lbB359.Text;
                    this.HpaFor3.ws_b360 = lbB360.Text;
                    this.HpaFor3.ws_b361 = lbB361.Text;
                    this.HpaFor3.ws_b362 = lbB362.Text;
                    this.HpaFor3.ws_b363 = lbB363.Text;
                    this.HpaFor3.ws_b364 = lbB364.Text;
                    this.HpaFor3.ws_b365 = lbB365.Text;
                    this.HpaFor3.ws_b366 = lbB366.Text;
                    this.HpaFor3.ws_b367 = lbB367.Text;
                    this.HpaFor3.ws_b368 = lbB368.Text;
                    this.HpaFor3.ws_b369 = lbB369.Text;
                    this.HpaFor3.ws_b370 = lbB370.Text;
                    this.HpaFor3.ws_b371 = lbB371.Text;
                    this.HpaFor3.ws_b372 = lbB372.Text;
                    this.HpaFor3.ws_b373 = lbB373.Text;
                    this.HpaFor3.ws_b374 = lbB374.Text;
                    this.HpaFor3.ws_b375 = lbB375.Text;
                    this.HpaFor3.ws_b376 = lbB376.Text;
                    this.HpaFor3.ws_b377 = lbB377.Text;
                    this.HpaFor3.ws_b378 = lbB378.Text;
                    this.HpaFor3.ws_b379 = lbB379.Text;
                    this.HpaFor3.ws_b380 = lbB380.Text;
                    this.HpaFor3.ws_b381 = lbB381.Text;
                    this.HpaFor3.ws_b382 = lbB382.Text;
                    this.HpaFor3.ws_b383 = lbB383.Text;
                    this.HpaFor3.ws_b384 = lbB384.Text;
                    this.HpaFor3.ws_b385 = lbB385.Text;
                    this.HpaFor3.ws_b386 = lbB386.Text;
                    this.HpaFor3.ws_b387 = lbB387.Text;
                    this.HpaFor3.ws_b388 = lbB388.Text;
                    this.HpaFor3.ws_b389 = lbB389.Text;
                    this.HpaFor3.ws_b390 = lbB390.Text;

                    switch (this.CommandName)
                    {
                        case CommandNameEnum.Add:
                            this.HpaFor3.Insert();
                            break;
                        case CommandNameEnum.Edit:
                            this.HpaFor3.Update();
                            break;
                    }

                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                    StatusEnum srChemistApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (srChemistApproveStatus)
                    {
                        case StatusEnum.SR_CHEMIST_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_WORD);
                            break;
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.CHEMIST_TESTING);
                            break;
                    }
                    this.jobSample.step3owner = userLogin.id;
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                    StatusEnum labApproveStatus = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue, true);
                    switch (labApproveStatus)
                    {
                        case StatusEnum.LABMANAGER_APPROVE:
                            this.jobSample.job_status = Convert.ToInt32(StatusEnum.ADMIN_CONVERT_PDF);
                            break;
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                            this.jobSample.job_status = Convert.ToInt32(ddlAssignTo.SelectedValue);
                            break;
                    }
                    this.jobSample.step5owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".doc") || Path.GetExtension(btnUpload.FileName).Equals(".docx")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_word = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.LABMANAGER_CHECKING);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .doc|.docx";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step4owner = userLogin.id;
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (btnUpload.HasFile && (Path.GetExtension(btnUpload.FileName).Equals(".pdf")))
                    {
                        string yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                        String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                        String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        String savefilePathWithoutDrive = String.Format("{0}/{1}", yyyMMdd, this.jobSample.job_number + Path.GetExtension(btnUpload.FileName));
                        if (!Directory.Exists(sourceFileFolder))
                        {
                            Directory.CreateDirectory(sourceFileFolder);
                        }
                        btnUpload.SaveAs(savefilePath);
                        this.jobSample.path_pdf = savefilePathWithoutDrive;
                        this.jobSample.job_status = Convert.ToInt32(StatusEnum.JOB_COMPLETE);
                        lbMessage.Text = string.Empty;
                    }
                    else
                    {
                        lbMessage.Text = "Invalid File. Please upload a File with extension .pdf";
                        lbMessage.Attributes["class"] = "alert alert-error";
                        isValid = false;
                    }
                    this.jobSample.step6owner = userLogin.id;
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    this.jobSample.step2owner = userLogin.id;
                    break;
            }
            //Stamp date
            job_sample_logs tmp = new job_sample_logs
            {
                ID = 0,
                job_sample_id = this.jobSample.ID,
                job_status = Convert.ToInt32(this.jobSample.job_status),
                job_remark = txtRemark.Text,
                get_alerts = "0",
                date = DateTime.Now
            };
            tmp.Insert();

            this.jobSample.Update();
            //Commit
            GeneralManager.Commit();
            //Return to main.
            if (isValid)
            {
                Response.Redirect(this.PreviousPath);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void lbDownload_Click(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
            switch (status)
            {
                case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                    break;
                case StatusEnum.LOGIN_SELECT_SPEC:
                    break;
                case StatusEnum.CHEMIST_TESTING:
                    break;
                case StatusEnum.SR_CHEMIST_CHECKING:
                case StatusEnum.SR_CHEMIST_APPROVE:
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    break;
                case StatusEnum.ADMIN_CONVERT_WORD:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    else
                    {
                        downloadWord();
                    }
                    break;
                case StatusEnum.LABMANAGER_CHECKING:
                case StatusEnum.LABMANAGER_APPROVE:
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    if (!String.IsNullOrEmpty(this.jobSample.path_word))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
                case StatusEnum.ADMIN_CONVERT_PDF:
                    if (!String.IsNullOrEmpty(this.jobSample.path_pdf))
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_pdf));
                    }
                    else
                    {
                        Response.Redirect(String.Format("{0}{1}", Configuration.PATH_DOWNLOAD, this.jobSample.path_word));
                    }
                    break;
            }


        }

        private void downloadWord()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            string strFileName = string.Format("{0}_{1}.doc", lbRefNo.Text.Replace("-", "_"), DateTime.Now.ToString("yyyyMMddhhmmss"));

            HttpContext.Current.Response.ContentType = "application/vnd.ms-word";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

            StringWriter sw = new StringWriter();
            HtmlTextWriter h = new HtmlTextWriter(sw);
            invDiv.RenderControl(h);
            string strHTMLContent = sw.GetStringBuilder().ToString();
            String html = "<html><header><style>body {max-width: 800px;margin:initial;font-family: \'Arial Unicode MS\';font-size: 10px;}table {border-collapse: collapse;}th {background: #666;color: #fff;border: 1px solid #999;padding: 0.5rem;text-align: center;}td { border: 1px solid #999;padding: 0.5rem;text-align: left;}h6 {font-weight:initial;}</style></header><body>" + strHTMLContent + "</body></html>";


            HttpContext.Current.Response.Write(html);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), ddlStatus.SelectedValue.ToString(), true);
            switch (status)
            {
                case StatusEnum.SR_CHEMIST_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = false;
                    break;
                case StatusEnum.LABMANAGER_DISAPPROVE:
                    pRemark.Visible = true;
                    pDisapprove.Visible = true;
                    break;
                default:
                    pRemark.Visible = false;
                    pDisapprove.Visible = false;
                    break;
            }

        }

        private String getItemStatus()
        {
            String result = String.Empty;
            result = ((CheckBox1.Checked) ? "1" : "0") +
                        ((CheckBox2.Checked) ? "1" : "0") +
                        ((CheckBox3.Checked) ? "1" : "0") +
                        ((CheckBox4.Checked) ? "1" : "0") +
                        ((CheckBox5.Checked) ? "1" : "0")+
                        ((CheckBox6.Checked) ? "1" : "0")+
                        ((CheckBox7.Checked) ? "1" : "0");
            return result;
        }

        private void ShowItem(String _itemVisible)
        {
            if (_itemVisible != null)
            {
                char[] item = _itemVisible.ToCharArray();
                if (item.Length == 5)
                {
                    StatusEnum status = (StatusEnum)Enum.Parse(typeof(StatusEnum), this.jobSample.job_status.ToString(), true);
                    switch (status)
                    {
                        case StatusEnum.LOGIN_CONVERT_TEMPLATE:
                            break;
                        case StatusEnum.LOGIN_SELECT_SPEC:
                            CheckBox1.Checked = item[0] == '1' ? true : false;
                            CheckBox2.Checked = item[1] == '1' ? true : false;
                            CheckBox3.Checked = item[2] == '1' ? true : false;
                            CheckBox4.Checked = item[3] == '1' ? true : false;
                            CheckBox5.Checked = item[4] == '1' ? true : false;
                            CheckBox6.Checked = item[5] == '1' ? true : false;
                            CheckBox7.Checked = item[6] == '1' ? true : false;
                            txtCVP_D23.Visible = true;
                            txtCVP_D24.Visible = true;
                            txtCVP_D25.Visible = true;
                            lbD23.Visible = false;
                            lbD24.Visible = false;
                            lbD25.Visible = false;
                            break;
                        case StatusEnum.CHEMIST_TESTING:
                        case StatusEnum.SR_CHEMIST_CHECKING:
                        case StatusEnum.SR_CHEMIST_APPROVE:
                        case StatusEnum.SR_CHEMIST_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_WORD:
                        case StatusEnum.LABMANAGER_CHECKING:
                        case StatusEnum.LABMANAGER_APPROVE:
                        case StatusEnum.LABMANAGER_DISAPPROVE:
                        case StatusEnum.ADMIN_CONVERT_PDF:
                            tb1.Visible = item[0] == '1' ? true : false;
                            tr1.Visible = item[1] == '1' ? true : false;
                            tr2.Visible = item[2] == '1' ? true : false;
                            tr3.Visible = item[3] == '1' ? true : false;
                            tr4.Visible = item[4] == '1' ? true : false;
                            tr5.Visible = item[5] == '1' ? true : false;
                            tr6.Visible = item[6] == '1' ? true : false;
                            CheckBox1.Visible = false;
                            CheckBox2.Visible = false;
                            CheckBox3.Visible = false;
                            CheckBox4.Visible = false;
                            CheckBox5.Visible = false;
                            CheckBox6.Visible = false;
                            CheckBox7.Visible = false;
                            txtCVP_D23.Visible = false;
                            txtCVP_D24.Visible = false;
                            txtCVP_D25.Visible = false;
                            lbD23.Visible = true;
                            lbD24.Visible = true;
                            lbD25.Visible = true;
                            break;
                    }
                }
            }

        }

    }
}
