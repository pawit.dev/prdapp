﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using System;
using System.Web.UI;

namespace ALS.ALSI.Web.view.template
{
    public partial class Seagate_IC_Component_1_4_WorkingSheet : System.Web.UI.UserControl
    {


        #region "Property"

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public template_seagate_ic_coverpage objWork
        {
            get
            {
                template_seagate_ic_coverpage work = new template_seagate_ic_coverpage();
                work.sample_id = this.SampleID;
                work.specification_id = int.Parse(hDdlSpec.Value);

                work.b9 = txtB9.Text;
                work.b10 = txtB10.Text;
                work.b11 = txtB11.Text;

                work.b14 = txtB14_Chem.Text;
                work.b15 = txtB15_Chem.Text;
                work.b16 = txtB16_Chem.Text;
                work.b17 = txtB17_Chem.Text;
                work.b18 = txtB18_Chem.Text;
                work.b19 = txtB19_Chem.Text;
                work.b20 = txtB20_Chem.Text;
                work.b23 = txtB23_Chem.Text;
                work.b24 = txtB24_Chem.Text;
                work.b25 = txtB25_Chem.Text;
                work.b26 = txtB26_Chem.Text;
                work.b27 = txtB27_Chem.Text;
                work.b28 = txtB28_Chem.Text;

                work.c14 = txtC14_Chem.Text;
                work.c15 = txtC15_Chem.Text;
                work.c16 = txtC16_Chem.Text;
                work.c17 = txtC17_Chem.Text;
                work.c18 = txtC18_Chem.Text;
                work.c19 = txtC19_Chem.Text;
                work.c20 = txtC20_Chem.Text;
                work.c23 = txtC23_Chem.Text;
                work.c25 = txtC25_Chem.Text;
                work.c26 = txtC26_Chem.Text;
                work.c27 = txtC27_Chem.Text;
                work.c28 = txtC28_Chem.Text;
                work.c24 = txtC24_Chem.Text;

                work.result_c25 = lbAnI14.Text;
                work.result_c26 = lbAnI15.Text;
                work.result_c27 = lbAnI16.Text;
                work.result_c28 = lbAnI17.Text;
                work.result_c29 = lbAnI18.Text;
                work.result_c30 = lbAnI19.Text;
                work.result_c31 = lbAnI20.Text;
                work.result_c32 = lbAnI21.Text;

                work.result_c34 = lbAnI23.Text;
                work.result_c35 = lbAnI24.Text;
                work.result_c36 = lbAnI25.Text;
                work.result_c37 = lbAnI26.Text;
                work.result_c38 = lbAnI27.Text;
                work.result_c39 = lbAnI28.Text;
                work.result_c40 = lbAnI29.Text;

                work.item_visible = hItemVisible.Value;
                return work;
            }
        }

        private void initialPage()
        {

            template_seagate_ic_coverpage work = new template_seagate_ic_coverpage().SelectBySampleID(this.SampleID);
            if (work != null)
            {
                hDdlSpec.Value = work.specification_id.ToString();
                txtB9.Text = work.b9.ToString();
                txtB10.Text = work.b10.ToString();
                txtB11.Text = work.b11.ToString();

                txtB14_Chem.Text = work.b14.ToString();
                txtB15_Chem.Text = work.b15.ToString();
                txtB16_Chem.Text = work.b16.ToString();
                txtB17_Chem.Text = work.b17.ToString();
                txtB18_Chem.Text = work.b18.ToString();
                txtB19_Chem.Text = work.b19.ToString();
                txtB20_Chem.Text = work.b20.ToString();
                txtB23_Chem.Text = work.b23.ToString();
                txtB24_Chem.Text = work.b24.ToString();
                txtB25_Chem.Text = work.b25.ToString();
                txtB26_Chem.Text = work.b26.ToString();
                txtB27_Chem.Text = work.b27.ToString();
                txtB28_Chem.Text = work.b28.ToString();

                txtC14_Chem.Text = work.c14.ToString();
                txtC15_Chem.Text = work.c15.ToString();
                txtC16_Chem.Text = work.c16.ToString();
                txtC17_Chem.Text = work.c17.ToString();
                txtC18_Chem.Text = work.c18.ToString();
                txtC19_Chem.Text = work.c19.ToString();
                txtC20_Chem.Text = work.c20.ToString();
                txtC23_Chem.Text = work.c23.ToString();
                txtC25_Chem.Text = work.c25.ToString();
                txtC26_Chem.Text = work.c26.ToString();
                txtC27_Chem.Text = work.c27.ToString();
                txtC28_Chem.Text = work.c28.ToString();
                txtC24_Chem.Text = work.c24.ToString();

                hItemVisible.Value = work.item_visible;
                //CAL
                calculateByFormular();
            }

            //Disable Save button
            btnSubmit.Enabled = false;
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            objWork.UpdateBySampleID();
            //Commit
            GeneralManager.Commit();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnCalulate_Click(object sender, EventArgs e)
        {
            btnSubmit.Enabled = true;
            calculateByFormular();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.PreviousPath);
        }

        private void calculateByFormular()
        {

            double C14 = Convert.ToDouble((String.IsNullOrEmpty(txtC14_Chem.Text)) ? "0" : (!CustomUtils.isNumber(txtC14_Chem.Text) ? "0" : txtC14_Chem.Text));



            double C15 = Convert.ToDouble((String.IsNullOrEmpty(txtC15_Chem.Text)) ? "0" : txtC15_Chem.Text);
            double C16 = Convert.ToDouble((String.IsNullOrEmpty(txtC16_Chem.Text)) ? "0" : txtC16_Chem.Text);
            double C17 = Convert.ToDouble((String.IsNullOrEmpty(txtC17_Chem.Text)) ? "0" : txtC17_Chem.Text);
            double C18 = Convert.ToDouble((String.IsNullOrEmpty(txtC18_Chem.Text)) ? "0" : txtC18_Chem.Text);
            double C19 = Convert.ToDouble((String.IsNullOrEmpty(txtC19_Chem.Text)) ? "0" : txtC19_Chem.Text);
            double C20 = Convert.ToDouble((String.IsNullOrEmpty(txtC20_Chem.Text)) ? "0" : txtC20_Chem.Text);

            double B9 = Convert.ToDouble((String.IsNullOrEmpty(txtB9.Text)) ? "0" : txtB9.Text);
            double B10 = Convert.ToDouble((String.IsNullOrEmpty(txtB10.Text)) ? "0" : txtB10.Text);
            double B11 = Convert.ToDouble((String.IsNullOrEmpty(txtB11.Text)) ? "0" : txtB11.Text);
            double B14 = Convert.ToDouble((String.IsNullOrEmpty(txtB14_Chem.Text)) ? "0" : txtB14_Chem.Text);
            double B15 = Convert.ToDouble((String.IsNullOrEmpty(txtB15_Chem.Text)) ? "0" : txtB15_Chem.Text);
            double B16 = Convert.ToDouble((String.IsNullOrEmpty(txtB16_Chem.Text)) ? "0" : txtB16_Chem.Text);
            double B17 = Convert.ToDouble((String.IsNullOrEmpty(txtB17_Chem.Text)) ? "0" : txtB17_Chem.Text);
            double B18 = Convert.ToDouble((String.IsNullOrEmpty(txtB18_Chem.Text)) ? "0" : txtB18_Chem.Text);
            double B19 = Convert.ToDouble((String.IsNullOrEmpty(txtB19_Chem.Text)) ? "0" : txtB19_Chem.Text);
            double B20 = Convert.ToDouble((String.IsNullOrEmpty(txtB20_Chem.Text)) ? "0" : txtB20_Chem.Text);

            double B23 = Convert.ToDouble((String.IsNullOrEmpty(txtB23_Chem.Text)) ? "0" : txtB23_Chem.Text);
            double B24 = Convert.ToDouble((String.IsNullOrEmpty(txtB24_Chem.Text)) ? "0" : txtB24_Chem.Text);
            double B25 = Convert.ToDouble((String.IsNullOrEmpty(txtB25_Chem.Text)) ? "0" : txtB25_Chem.Text);
            double B26 = Convert.ToDouble((String.IsNullOrEmpty(txtB26_Chem.Text)) ? "0" : txtB26_Chem.Text);
            double B27 = Convert.ToDouble((String.IsNullOrEmpty(txtB27_Chem.Text)) ? "0" : txtB27_Chem.Text);
            double B28 = Convert.ToDouble((String.IsNullOrEmpty(txtB28_Chem.Text)) ? "0" : txtB28_Chem.Text);

            double C23 = Convert.ToDouble((String.IsNullOrEmpty(txtC23_Chem.Text)) ? "0" : txtC23_Chem.Text);
            double C24 = Convert.ToDouble((String.IsNullOrEmpty(txtC24_Chem.Text)) ? "0" : txtC24_Chem.Text);
            double C25 = Convert.ToDouble((String.IsNullOrEmpty(txtC25_Chem.Text)) ? "0" : txtC25_Chem.Text);
            double C26 = Convert.ToDouble((String.IsNullOrEmpty(txtC26_Chem.Text)) ? "0" : txtC26_Chem.Text);
            double C27 = Convert.ToDouble((String.IsNullOrEmpty(txtC27_Chem.Text)) ? "0" : txtC27_Chem.Text);
            double C28 = Convert.ToDouble((String.IsNullOrEmpty(txtC28_Chem.Text)) ? "0" : txtC28_Chem.Text);

            #region "ANIONS"

            //=((C14-B14)*D14*$B$9/($B$10*$B$11))
            lbAnE14.Text = String.Format("{0:n4}", ((C14 - B14) * 1 * B9 / (B10 * B11)));
            lbAnE15.Text = String.Format("{0:n4}", ((C15 - B15) * 1 * B9 / (B10 * B11)));
            lbAnE16.Text = String.Format("{0:n4}", ((C16 - B16) * 1 * B9 / (B10 * B11)));
            lbAnE17.Text = String.Format("{0:n4}", ((C17 - B17) * 1 * B9 / (B10 * B11)));
            lbAnE18.Text = String.Format("{0:n4}", ((C18 - B18) * 1 * B9 / (B10 * B11)));
            lbAnE19.Text = String.Format("{0:n4}", ((C19 - B19) * 1 * B9 / (B10 * B11)));
            lbAnE20.Text = String.Format("{0:n4}", ((C20 - B20) * 1 * B9 / (B10 * B11)));

            //=F14*$B$9/$B$10/$B$11*D14
            lbAnG14.Text = String.Format("{0:n4}", (0.5 * B9 / B10 / B11 * 1));
            lbAnG15.Text = String.Format("{0:n4}", (0.5 * B9 / B10 / B11 * 1));
            lbAnG16.Text = String.Format("{0:n4}", (0.5 * B9 / B10 / B11 * 1));
            lbAnG17.Text = String.Format("{0:n4}", (0.5 * B9 / B10 / B11 * 1));
            lbAnG18.Text = String.Format("{0:n4}", (0.5 * B9 / B10 / B11 * 1));
            lbAnG19.Text = String.Format("{0:n4}", (0.5 * B9 / B10 / B11 * 1));
            lbAnG20.Text = String.Format("{0:n4}", (0.5 * B9 / B10 / B11 * 1));

            //=IF(E14<G14,1,0)
            lbAnH14.Text = (Convert.ToDouble(lbAnE14.Text) < Convert.ToDouble(lbAnG14.Text) ? 1 : 0) + "";
            lbAnH15.Text = (Convert.ToDouble(lbAnE15.Text) < Convert.ToDouble(lbAnG15.Text) ? 1 : 0) + "";
            lbAnH16.Text = (Convert.ToDouble(lbAnE16.Text) < Convert.ToDouble(lbAnG16.Text) ? 1 : 0) + "";
            lbAnH17.Text = (Convert.ToDouble(lbAnE17.Text) < Convert.ToDouble(lbAnG17.Text) ? 1 : 0) + "";
            lbAnH18.Text = (Convert.ToDouble(lbAnE18.Text) < Convert.ToDouble(lbAnG18.Text) ? 1 : 0) + "";
            lbAnH19.Text = (Convert.ToDouble(lbAnE19.Text) < Convert.ToDouble(lbAnG19.Text) ? 1 : 0) + "";
            lbAnH20.Text = (Convert.ToDouble(lbAnE20.Text) < Convert.ToDouble(lbAnG20.Text) ? 1 : 0) + "";
            lbAnH21.Text = (Convert.ToDouble(lbAnH14.Text) +
                            Convert.ToDouble(lbAnH15.Text) +
                            Convert.ToDouble(lbAnH16.Text) +
                            Convert.ToDouble(lbAnH17.Text) +
                            Convert.ToDouble(lbAnH18.Text) +
                            Convert.ToDouble(lbAnH19.Text) +
                              Convert.ToDouble(lbAnH20.Text)
                            ).ToString();
            //=IF(H14=1,CONClbAnH20.TextATENATE("<",ROUND(G14,4)),ROUND(E14,6))
            lbAnI14.Text = ((Convert.ToDouble(lbAnH14.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG14.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE14.Text), 6));
            lbAnI15.Text = ((Convert.ToDouble(lbAnH15.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG15.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE15.Text), 6));
            lbAnI16.Text = ((Convert.ToDouble(lbAnH16.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG16.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE16.Text), 6));
            lbAnI17.Text = ((Convert.ToDouble(lbAnH17.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG17.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE17.Text), 6));
            lbAnI18.Text = ((Convert.ToDouble(lbAnH18.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG18.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE18.Text), 6));
            lbAnI19.Text = ((Convert.ToDouble(lbAnH19.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG19.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE19.Text), 6));
            lbAnI20.Text = ((Convert.ToDouble(lbAnH20.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG20.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE20.Text), 6));


            //=IF(H14=1,ROUND(G14,4),ROUND(E14,6))****** RESULT *******
            lbAnJ14.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH14.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG14.Text), 4) : Math.Round(Convert.ToDouble(lbAnE14.Text), 6)));
            lbAnJ15.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH15.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG15.Text), 4) : Math.Round(Convert.ToDouble(lbAnE15.Text), 6)));
            lbAnJ16.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH16.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG16.Text), 4) : Math.Round(Convert.ToDouble(lbAnE16.Text), 6)));
            lbAnJ17.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH17.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG17.Text), 4) : Math.Round(Convert.ToDouble(lbAnE17.Text), 6)));
            lbAnJ18.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH18.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG18.Text), 4) : Math.Round(Convert.ToDouble(lbAnE18.Text), 6)));
            lbAnJ19.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH19.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG19.Text), 4) : Math.Round(Convert.ToDouble(lbAnE19.Text), 6)));
            lbAnJ20.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH20.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG20.Text), 4) : Math.Round(Convert.ToDouble(lbAnE20.Text), 6)));
            lbAnJ21.Text = (Convert.ToDouble(lbAnJ14.Text) +
                            Convert.ToDouble(lbAnJ15.Text) +
                            Convert.ToDouble(lbAnJ16.Text) +
                            Convert.ToDouble(lbAnJ17.Text) +
                            Convert.ToDouble(lbAnJ18.Text) +
                            Convert.ToDouble(lbAnJ19.Text) +
                            Convert.ToDouble(lbAnJ20.Text)).ToString();
            lbAnI21.Text = (Convert.ToInt32(lbAnH21.Text) == 0) ? lbAnJ21.Text : "<" + Math.Round(Convert.ToDecimal(lbAnJ21.Text), 4);
            #endregion

            #region "CATIONS"

            //=((C23-B23)*D23*$B$9/($B$10*$B$11))
            lbAnE23.Text = String.Format("{0:n4}", ((C23 - B23) * 1 * B9 / (B10 * B11)));
            lbAnE24.Text = String.Format("{0:n4}", ((C24 - B24) * 1 * B9 / (B10 * B11)));
            lbAnE25.Text = String.Format("{0:n4}", ((C25 - B25) * 1 * B9 / (B10 * B11)));
            lbAnE26.Text = String.Format("{0:n4}", ((C26 - B26) * 1 * B9 / (B10 * B11)));
            lbAnE27.Text = String.Format("{0:n4}", ((C27 - B27) * 1 * B9 / (B10 * B11)));
            lbAnE28.Text = String.Format("{0:n4}", ((C28 - B28) * 1 * B9 / (B10 * B11)));

            //F23*$B$9/$B$10/$B$11*D23
            lbAnG23.Text = String.Format("{0:n4}", (0.6 * B9 / B10 / B11 * 1));
            lbAnG24.Text = String.Format("{0:n4}", (0.6 * B9 / B10 / B11 * 1));
            lbAnG25.Text = String.Format("{0:n4}", (0.6 * B9 / B10 / B11 * 1));
            lbAnG26.Text = String.Format("{0:n4}", (0.6 * B9 / B10 / B11 * 1));
            lbAnG27.Text = String.Format("{0:n4}", (0.6 * B9 / B10 / B11 * 1));
            lbAnG28.Text = String.Format("{0:n4}", (0.6 * B9 / B10 / B11 * 1));

            //=IF(E14<G14,1,0)
            lbAnH23.Text = (Convert.ToDouble(lbAnE23.Text) < Convert.ToDouble(lbAnG23.Text) ? 1 : 0) + "";
            lbAnH24.Text = (Convert.ToDouble(lbAnE24.Text) < Convert.ToDouble(lbAnG24.Text) ? 1 : 0) + "";
            lbAnH25.Text = (Convert.ToDouble(lbAnE25.Text) < Convert.ToDouble(lbAnG25.Text) ? 1 : 0) + "";
            lbAnH26.Text = (Convert.ToDouble(lbAnE26.Text) < Convert.ToDouble(lbAnG26.Text) ? 1 : 0) + "";
            lbAnH27.Text = (Convert.ToDouble(lbAnE27.Text) < Convert.ToDouble(lbAnG27.Text) ? 1 : 0) + "";
            lbAnH28.Text = (Convert.ToDouble(lbAnE28.Text) < Convert.ToDouble(lbAnG28.Text) ? 1 : 0) + "";
            lbAnH29.Text = (Convert.ToDouble(lbAnH23.Text) +
                            Convert.ToDouble(lbAnH24.Text) +
                            Convert.ToDouble(lbAnH25.Text) +
                            Convert.ToDouble(lbAnH26.Text) +
                            Convert.ToDouble(lbAnH27.Text) +
                            Convert.ToDouble(lbAnH28.Text)).ToString();

            //=IF(H23=1,CONCATENATE("<",ROUND(G23,4)),ROUND(E23,6))
            lbAnI23.Text = ((Convert.ToDouble(lbAnH23.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG23.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE23.Text), 6));
            lbAnI24.Text = ((Convert.ToDouble(lbAnH24.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG24.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE24.Text), 6));
            lbAnI25.Text = ((Convert.ToDouble(lbAnH25.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG25.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE25.Text), 6));
            lbAnI26.Text = ((Convert.ToDouble(lbAnH26.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG26.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE26.Text), 6));
            lbAnI27.Text = ((Convert.ToDouble(lbAnH27.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG27.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE27.Text), 6));
            lbAnI28.Text = ((Convert.ToDouble(lbAnH28.Text) == 1) ? "<" + Math.Round(Convert.ToDouble(lbAnG28.Text), 4) : "" + Math.Round(Convert.ToDouble(lbAnE28.Text), 6));

            //=IF(H23=1,ROUND(G23,4),ROUND(E23,6))****** RESULT *******
            lbAnJ23.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH23.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG23.Text), 4) : Math.Round(Convert.ToDouble(lbAnE23.Text), 6)));
            lbAnJ24.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH24.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG24.Text), 4) : Math.Round(Convert.ToDouble(lbAnE24.Text), 6)));
            lbAnJ25.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH25.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG25.Text), 4) : Math.Round(Convert.ToDouble(lbAnE25.Text), 6)));
            lbAnJ26.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH26.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG26.Text), 4) : Math.Round(Convert.ToDouble(lbAnE26.Text), 6)));
            lbAnJ27.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH27.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG27.Text), 4) : Math.Round(Convert.ToDouble(lbAnE27.Text), 6)));
            lbAnJ28.Text = String.Format("{0:n4}", ((Convert.ToDouble(lbAnH28.Text) == 1) ? Math.Round(Convert.ToDouble(lbAnG28.Text), 4) : Math.Round(Convert.ToDouble(lbAnE28.Text), 6)));
            lbAnJ29.Text = (
                Convert.ToDouble(lbAnJ23.Text) +
                Convert.ToDouble(lbAnJ24.Text) +
                Convert.ToDouble(lbAnJ25.Text) +
                Convert.ToDouble(lbAnJ26.Text) +
                Convert.ToDouble(lbAnJ27.Text) +
                Convert.ToDouble(lbAnJ28.Text)).ToString();
            lbAnI29.Text = (Convert.ToInt32(lbAnH29.Text) == 0) ? lbAnJ29.Text : "<" + Math.Round(Convert.ToDecimal(lbAnJ29.Text), 4);
            #endregion
        }
    }
}