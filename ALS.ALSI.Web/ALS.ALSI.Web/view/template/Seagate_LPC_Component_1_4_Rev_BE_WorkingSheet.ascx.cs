﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using ALS.ALSI.Web.view.request;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.template
{
    public partial class Seagate_LPC_Component_1_4_Rev_BE_WorkingSheet : System.Web.UI.UserControl
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Seagate_DHS_Component_Rev_BE_1_5_WorkingSheet));

        #region "Property"

        public List<template_seagate_lpc_coverpage> Lpcs
        {
            get { return (List<template_seagate_lpc_coverpage>)Session[GetType().Name + "Lpcs"]; }
            set { Session[GetType().Name + "Lpcs"] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            //Session.Remove(GetType().Name + "tbCas");
            //Session.Remove(GetType().Name + "coverpages");
            Session.Remove(GetType().Name + "Lpcs");
            Session.Remove(GetType().Name + Constants.PREVIOUS_PATH);
            Session.Remove(GetType().Name + "SampleID");
        }

        private void initialPage()
        {
            this.Lpcs = template_seagate_lpc_coverpage.FindAllBySampleID(this.SampleID);

            #region "US-LPC(0.3)"
            template_seagate_lpc_coverpage khz68_03 = Lpcs.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString());
            if (khz68_03 != null)
            {
                LPCTypeEnum lpcType = (LPCTypeEnum)Enum.ToObject(typeof(LPCTypeEnum), Convert.ToInt32(khz68_03.lpc_type));
                switch (lpcType)
                {
                    case LPCTypeEnum.KHz_68:
                        cbLPCType68.Checked = true;
                        break;
                    case LPCTypeEnum.KHz_132:
                        cbLPCType132.Checked = true;
                        break;
                }
                txt_UsLPC03_B14.Text = khz68_03.b14;
                txt_UsLPC03_B15.Text = khz68_03.b15;
                txt_UsLPC03_B16.Text = khz68_03.b16;
                txt_UsLPC03_B17.Text = khz68_03.b17;

                txt_UsLPC03_C14.Text = khz68_03.c14;
                txt_UsLPC03_C15.Text = khz68_03.c15;
                txt_UsLPC03_C16.Text = khz68_03.c16;
                txt_UsLPC03_C17.Text = khz68_03.c17;

                txt_UsLPC03_D14.Text = khz68_03.d14;
                txt_UsLPC03_D15.Text = khz68_03.d15;
                txt_UsLPC03_D16.Text = khz68_03.d16;
                txt_UsLPC03_D17.Text = khz68_03.d17;

                txt_UsLPC03_E14.Text = khz68_03.e14;
                txt_UsLPC03_E15.Text = khz68_03.e15;
                txt_UsLPC03_E16.Text = khz68_03.e16;
                txt_UsLPC03_E17.Text = khz68_03.e17;

                txt_UsLPC03_F14.Text = khz68_03.f14;
                txt_UsLPC03_F15.Text = khz68_03.f15;
                txt_UsLPC03_F16.Text = khz68_03.f16;
                txt_UsLPC03_F17.Text = khz68_03.f17;

                txt_UsLPC03_G14.Text = khz68_03.g14;
                txt_UsLPC03_G15.Text = khz68_03.g15;
                txt_UsLPC03_G16.Text = khz68_03.g16;
                txt_UsLPC03_G17.Text = khz68_03.g17;

                txt_UsLPC03_B20.Text = Lpcs[0].cvp_c19;
                txt_UsLPC03_B21.Text = khz68_03.b21;
                txt_UsLPC03_B22.Text = Lpcs[0].cvp_e19;
            }
            #endregion
            #region "US-LPC(0.6)"
            template_seagate_lpc_coverpage khz68_06 = Lpcs.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString());
            if (khz68_06 != null)
            {
                txt_UsLPC06_B14.Text = khz68_06.b14;
                txt_UsLPC06_B15.Text = khz68_06.b15;
                txt_UsLPC06_B16.Text = khz68_06.b16;
                txt_UsLPC06_B17.Text = khz68_06.b17;

                txt_UsLPC06_C14.Text = khz68_06.c14;
                txt_UsLPC06_C15.Text = khz68_06.c15;
                txt_UsLPC06_C16.Text = khz68_06.c16;
                txt_UsLPC06_C17.Text = khz68_06.c17;

                txt_UsLPC06_D14.Text = khz68_06.d14;
                txt_UsLPC06_D15.Text = khz68_06.d15;
                txt_UsLPC06_D16.Text = khz68_06.d16;
                txt_UsLPC06_D17.Text = khz68_06.d17;

                txt_UsLPC06_E14.Text = khz68_06.e14;
                txt_UsLPC06_E15.Text = khz68_06.e15;
                txt_UsLPC06_E16.Text = khz68_06.e16;
                txt_UsLPC06_E17.Text = khz68_06.e17;

                txt_UsLPC06_F14.Text = khz68_06.f14;
                txt_UsLPC06_F15.Text = khz68_06.f15;
                txt_UsLPC06_F16.Text = khz68_06.f16;
                txt_UsLPC06_F17.Text = khz68_06.f17;

                txt_UsLPC06_G14.Text = khz68_06.g14;
                txt_UsLPC06_G15.Text = khz68_06.g15;
                txt_UsLPC06_G16.Text = khz68_06.g16;
                txt_UsLPC06_G17.Text = khz68_06.g17;

                txt_UsLPC06_B20.Text = Lpcs[0].cvp_c19;
                txt_UsLPC06_B21.Text = khz68_06.b21;
                txt_UsLPC06_B22.Text = Lpcs[0].cvp_e19;
            }
            #endregion

            CalculateCas();
            //initial component
            btnUsLPC03.CssClass = "btn green";
            btnUsLPC06.CssClass = "btn blue";
            pUS_LPC03.Visible = true;
            pUS_LPC06.Visible = false;
            btnSubmit.Enabled = false;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            #region "US-LPC(0.3)"
            template_seagate_lpc_coverpage khz68_03 = Lpcs.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_03).ToString());
            if (khz68_03 != null)
            {
                khz68_03.lpc_type = (cbLPCType68.Checked) ? "1" : "2";
                khz68_03.b14 = txt_UsLPC03_B14.Text;
                khz68_03.b15 = txt_UsLPC03_B15.Text;
                khz68_03.b16 = txt_UsLPC03_B16.Text;
                khz68_03.b17 = txt_UsLPC03_B17.Text;

                khz68_03.c14 = txt_UsLPC03_C14.Text;
                khz68_03.c15 = txt_UsLPC03_C15.Text;
                khz68_03.c16 = txt_UsLPC03_C16.Text;
                khz68_03.c17 = txt_UsLPC03_C17.Text;

                khz68_03.d14 = txt_UsLPC03_D14.Text;
                khz68_03.d15 = txt_UsLPC03_D15.Text;
                khz68_03.d16 = txt_UsLPC03_D16.Text;
                khz68_03.d17 = txt_UsLPC03_D17.Text;

                khz68_03.e14 = txt_UsLPC03_E14.Text;
                khz68_03.e15 = txt_UsLPC03_E15.Text;
                khz68_03.e16 = txt_UsLPC03_E16.Text;
                khz68_03.e17 = txt_UsLPC03_E17.Text;

                khz68_03.f14 = txt_UsLPC03_F14.Text;
                khz68_03.f15 = txt_UsLPC03_F15.Text;
                khz68_03.f16 = txt_UsLPC03_F16.Text;
                khz68_03.f17 = txt_UsLPC03_F17.Text;

                khz68_03.g14 = txt_UsLPC03_G14.Text;
                khz68_03.g15 = txt_UsLPC03_G15.Text;
                khz68_03.g16 = txt_UsLPC03_G16.Text;
                khz68_03.g17 = txt_UsLPC03_G17.Text;

                khz68_03.b21 = txt_UsLPC03_B21.Text;
                khz68_03.b25 = txt_UsLPC03_B25.Text;
                khz68_03.d25 = txt_UsLPC03_D25.Text;
                khz68_03.f25 = txt_UsLPC03_F25.Text;


            }
            #endregion
            #region "US-LPC(0.6)"
            template_seagate_lpc_coverpage khz68_06 = Lpcs.Find(x => x.particle_type == Convert.ToInt16(ParticleTypeEnum.PAR_06).ToString());
            if (khz68_06 != null)
            {
                khz68_06.b14 = txt_UsLPC06_B14.Text;
                khz68_06.b15 = txt_UsLPC06_B15.Text;
                khz68_06.b16 = txt_UsLPC06_B16.Text;
                khz68_06.b17 = txt_UsLPC06_B17.Text;

                khz68_06.c14 = txt_UsLPC06_C14.Text;
                khz68_06.c15 = txt_UsLPC06_C15.Text;
                khz68_06.c16 = txt_UsLPC06_C16.Text;
                khz68_06.c17 = txt_UsLPC06_C17.Text;

                khz68_06.d14 = txt_UsLPC06_D14.Text;
                khz68_06.d15 = txt_UsLPC06_D15.Text;
                khz68_06.d16 = txt_UsLPC06_D16.Text;
                khz68_06.d17 = txt_UsLPC06_D17.Text;

                khz68_06.e14 = txt_UsLPC06_E14.Text;
                khz68_06.e15 = txt_UsLPC06_E15.Text;
                khz68_06.e16 = txt_UsLPC06_E16.Text;
                khz68_06.e17 = txt_UsLPC06_E17.Text;

                khz68_06.f14 = txt_UsLPC06_F14.Text;
                khz68_06.f15 = txt_UsLPC06_F15.Text;
                khz68_06.f16 = txt_UsLPC06_F16.Text;
                khz68_06.f17 = txt_UsLPC06_F17.Text;

                khz68_06.g14 = txt_UsLPC06_G14.Text;
                khz68_06.g15 = txt_UsLPC06_G15.Text;
                khz68_06.g16 = txt_UsLPC06_G16.Text;
                khz68_06.g17 = txt_UsLPC06_G17.Text;

                khz68_06.b21 = txt_UsLPC06_B21.Text;
                khz68_06.b25 = txt_UsLPC06_B25.Text;
                khz68_06.d25 = txt_UsLPC06_D25.Text;
                khz68_06.f25 = txt_UsLPC06_F25.Text;
            }
            #endregion

            khz68_03.UpdateList(this.Lpcs);
            //Commit
            GeneralManager.Commit();

            Response.Redirect(this.PreviousPath);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnLoadFile_Click(object sender, EventArgs e)
        {
            String error = validateDSHFile(btnUpload.PostedFiles);

            if (!String.IsNullOrEmpty(txt_UsLPC03_B21.Text) && !String.IsNullOrEmpty(txt_UsLPC06_B21.Text))
            {
                if (String.IsNullOrEmpty(error))
                {
                    #region "LOAD"
                    String yyyMMdd = DateTime.Now.ToString("yyyyMMdd");

                    String[] B1_03 = new String[4];
                    String[] B1_06 = new String[4];
                    String[] B2_03 = new String[4];
                    String[] B2_06 = new String[4];
                    String[] B3_03 = new String[4];
                    String[] B3_06 = new String[4];
                    String[] S1_03 = new String[4];
                    String[] S1_06 = new String[4];
                    String[] S2_03 = new String[4];
                    String[] S2_06 = new String[4];
                    String[] S3_03 = new String[4];
                    String[] S3_06 = new String[4];

                    for (int i = 0; i < btnUpload.PostedFiles.Count; i++)
                    {
                        HttpPostedFile _postedFile = btnUpload.PostedFiles[i];
                        try
                        {
                            if (_postedFile.ContentLength > 0)
                            {
                                String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                                String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, _postedFile.FileName);
                                if (!Directory.Exists(sourceFileFolder))
                                {
                                    Directory.CreateDirectory(sourceFileFolder);
                                }

                                _postedFile.SaveAs(savefilePath);



                                using (FileStream fs = new FileStream(savefilePath, FileMode.Open, FileAccess.Read))
                                {
                                    HSSFWorkbook wd = new HSSFWorkbook(fs);
                                    ISheet sheetSeagateIc = wd.GetSheet("Sheet1");

                                    if (sheetSeagateIc != null)
                                    {
                                        int index_03 = 0;
                                        int index_06 = 0;
                                        for (int row = 22; row <= sheetSeagateIc.LastRowNum; row++)
                                        {
                                            if (sheetSeagateIc.GetRow(row) != null) //null is when the row only contains empty cells 
                                            {
                                                String CS = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(9));
                                                #region "0.300"
                                                if (CS.Equals("0.300"))
                                                {
                                                    String CD = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(18));
                                                    switch (Path.GetFileNameWithoutExtension(savefilePath))
                                                    {
                                                        case "B1": B1_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "B2": B2_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "B3": B3_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "S1": S1_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "S2": S2_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "S3": S3_03[index_03] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                    }
                                                    index_03++;
                                                }
                                                #endregion
                                                #region "0.600"
                                                if (CS.Equals("0.600"))
                                                {
                                                    String CD = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(18));
                                                    switch (Path.GetFileNameWithoutExtension(savefilePath))
                                                    {
                                                        case "B1": B1_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "B2": B2_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "B3": B3_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "S1": S1_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "S2": S2_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                        case "S3": S3_06[index_06] = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(CD), 2));
                                                            break;
                                                    }
                                                    index_06++;
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }

                            }

                        }
                        catch (Exception Ex)
                        {
                            logger.Error(Ex.Message);
                            Console.WriteLine();
                        }
                    }
                    #endregion
                    #region "SET"
                    #region "US-LPC(0.3)"
                    txt_UsLPC03_B14.Text = B1_03[0];
                    txt_UsLPC03_B15.Text = B1_03[1];
                    txt_UsLPC03_B16.Text = B1_03[2];
                    txt_UsLPC03_B17.Text = B1_03[3];

                    txt_UsLPC03_C14.Text = S1_03[0];
                    txt_UsLPC03_C15.Text = S1_03[1];
                    txt_UsLPC03_C16.Text = S1_03[2];
                    txt_UsLPC03_C17.Text = S1_03[3];

                    txt_UsLPC03_D14.Text = B2_03[0];
                    txt_UsLPC03_D15.Text = B2_03[1];
                    txt_UsLPC03_D16.Text = B2_03[2];
                    txt_UsLPC03_D17.Text = B2_03[3];

                    txt_UsLPC03_E14.Text = S2_03[0];
                    txt_UsLPC03_E15.Text = S2_03[1];
                    txt_UsLPC03_E16.Text = S2_03[2];
                    txt_UsLPC03_E17.Text = S2_03[3];

                    txt_UsLPC03_F14.Text = B3_03[0];
                    txt_UsLPC03_F15.Text = B3_03[1];
                    txt_UsLPC03_F16.Text = B3_03[2];
                    txt_UsLPC03_F17.Text = B3_03[3];

                    txt_UsLPC03_G14.Text = S3_03[0];
                    txt_UsLPC03_G15.Text = S3_03[1];
                    txt_UsLPC03_G16.Text = S3_03[2];
                    txt_UsLPC03_G17.Text = S3_03[3];

                    #endregion
                    #region "US-LPC(0.6)"
                    txt_UsLPC06_B14.Text = B1_06[0];
                    txt_UsLPC06_B15.Text = B1_06[1];
                    txt_UsLPC06_B16.Text = B1_06[2];
                    txt_UsLPC06_B17.Text = B1_06[3];

                    txt_UsLPC06_C14.Text = S1_06[0];
                    txt_UsLPC06_C15.Text = S1_06[1];
                    txt_UsLPC06_C16.Text = S1_06[2];
                    txt_UsLPC06_C17.Text = S1_06[3];

                    txt_UsLPC06_D14.Text = B2_06[0];
                    txt_UsLPC06_D15.Text = B2_06[1];
                    txt_UsLPC06_D16.Text = B2_06[2];
                    txt_UsLPC06_D17.Text = B2_06[3];

                    txt_UsLPC06_E14.Text = S2_06[0];
                    txt_UsLPC06_E15.Text = S2_06[1];
                    txt_UsLPC06_E16.Text = S2_06[2];
                    txt_UsLPC06_E17.Text = S2_06[3];

                    txt_UsLPC06_F14.Text = B3_06[0];
                    txt_UsLPC06_F15.Text = B3_06[1];
                    txt_UsLPC06_F16.Text = B3_06[2];
                    txt_UsLPC06_F17.Text = B3_06[3];

                    txt_UsLPC06_G14.Text = S3_06[0];
                    txt_UsLPC06_G15.Text = S3_06[1];
                    txt_UsLPC06_G16.Text = S3_06[2];
                    txt_UsLPC06_G17.Text = S3_06[3];
                    #endregion
                    #endregion
                    CalculateCas();
                    btnSubmit.Enabled = false;
                    Console.WriteLine("-END-");
                }
                else
                {
                    lbMessage.Text = error;
                    lbMessage.Attributes["class"] = "alert alert-error";
                }
            }
            else
            {
                lbMessage.Text = "Please enter value of Surface Area.";
                lbMessage.Attributes["class"] = "alert alert-error";
            }
        }

        protected void btnUsLPC_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.Text)
            {
                case "US-LPC(0.3)":
                    btnUsLPC03.CssClass = "btn green";
                    btnUsLPC06.CssClass = "btn blue";
                    pUS_LPC03.Visible = true;
                    pUS_LPC06.Visible = false;
                    break;
                case "US-LPC(0.6)":
                    btnUsLPC03.CssClass = "btn blue";
                    btnUsLPC06.CssClass = "btn green";
                    pUS_LPC03.Visible = false;
                    pUS_LPC06.Visible = true;
                    break;
            }
        }

        #region "Custom method"

        private String validateDSHFile(IList<HttpPostedFile> _files)
        {
            Boolean isFound_b1 = false;
            Boolean isFound_b2 = false;
            Boolean isFound_b3 = false;

            Boolean isFound_s1 = false;
            Boolean isFound_s2 = false;
            Boolean isFound_s3 = false;

            Boolean isFoundWrongExtension = false;

            String result = String.Empty;

            String[] files = new String[_files.Count];
            if (files.Length == 6)
            {
                for (int i = 0; i < _files.Count; i++)
                {
                    files[i] = _files[i].FileName;
                    if (!Path.GetExtension(_files[i].FileName).Trim().Equals(".xls"))
                    {
                        isFoundWrongExtension = true;
                        break;
                    }
                }
                if (!isFoundWrongExtension)
                {

                    //Find B1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B1"))
                        {
                            isFound_b1 = true;
                            break;
                        }
                    }

                    //Find B2
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B2"))
                        {
                            isFound_b2 = true;
                            break;
                        }
                    }
                    //Find B3
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("B3"))
                        {
                            isFound_b3 = true;
                            break;
                        }
                    }

                    //Find S1
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S1"))
                        {
                            isFound_s1 = true;
                            break;
                        }
                    }

                    //Find S2
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S2"))
                        {
                            isFound_s2 = true;
                            break;
                        }
                    }
                    //Find S3
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).ToUpper().Equals("S3"))
                        {
                            isFound_s3 = true;
                            break;
                        }
                    }

                    result = (!isFound_b1) ? result += "File not found B1.xls" :
                                (!isFound_b2) ? result += "File not found B2.xls" :
                                (!isFound_b3) ? result += "File not found B3.xls" :
                                (!isFound_s1) ? result += "File not found S1.xls" :
                                (!isFound_s2) ? result += "File not found S2.xls" :
                                (!isFound_s3) ? result += "File not found S3.xls" : String.Empty;
                }
                else
                {
                    result = "File extension must be *.txt";
                }
            }
            else
            {
                result = "You must to select 6 files for upload.";
            }
            return result;
        }

        private void CalculateCas()
        {
            #region "US-LPC(0.3)"
            //=AVERAGE(B15,B16,B17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_B15.Text) && !String.IsNullOrEmpty(txt_UsLPC03_B16.Text) && !String.IsNullOrEmpty(txt_UsLPC03_B17.Text))
            {
                txt_UsLPC03_B18.Text = ((
                    Convert.ToDecimal(txt_UsLPC03_B15.Text) +
                    Convert.ToDecimal(txt_UsLPC03_B16.Text) +
                    Convert.ToDecimal(txt_UsLPC03_B17.Text)) / 3).ToString();


                txt_UsLPC03_B18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_B18.Text), 2));
            }
            //=AVERAGE(C15,C16,C17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_C15.Text) && !String.IsNullOrEmpty(txt_UsLPC03_C16.Text) && !String.IsNullOrEmpty(txt_UsLPC03_C17.Text))
            {
                txt_UsLPC03_C18.Text = ((
                    Convert.ToDecimal(txt_UsLPC03_C15.Text) +
                    Convert.ToDecimal(txt_UsLPC03_C16.Text) +
                    Convert.ToDecimal(txt_UsLPC03_C17.Text)) / 3).ToString();
                txt_UsLPC03_C18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_C18.Text), 2));
            }
            //=AVERAGE(D15,D16,D17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_D15.Text) && !String.IsNullOrEmpty(txt_UsLPC03_D16.Text) && !String.IsNullOrEmpty(txt_UsLPC03_D17.Text))
            {
                txt_UsLPC03_D18.Text = ((
                    Convert.ToDecimal(txt_UsLPC03_D15.Text) +
                    Convert.ToDecimal(txt_UsLPC03_D16.Text) +
                    Convert.ToDecimal(txt_UsLPC03_D17.Text)) / 3).ToString();
                txt_UsLPC03_D18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_D18.Text), 2));
            }
            //=AVERAGE(E15,E16,E17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_E15.Text) && !String.IsNullOrEmpty(txt_UsLPC03_E16.Text) && !String.IsNullOrEmpty(txt_UsLPC03_E17.Text))
            {
                txt_UsLPC03_E18.Text = ((
                    Convert.ToDecimal(txt_UsLPC03_E15.Text) +
                    Convert.ToDecimal(txt_UsLPC03_E16.Text) +
                    Convert.ToDecimal(txt_UsLPC03_E17.Text)) / 3).ToString();
                txt_UsLPC03_E18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_E18.Text), 2));
            }
            //=AVERAGE(F15,F16,F17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_F15.Text) && !String.IsNullOrEmpty(txt_UsLPC03_F16.Text) && !String.IsNullOrEmpty(txt_UsLPC03_F17.Text))
            {
                txt_UsLPC03_F18.Text = ((
                    Convert.ToDecimal(txt_UsLPC03_F15.Text) +
                    Convert.ToDecimal(txt_UsLPC03_F16.Text) +
                    Convert.ToDecimal(txt_UsLPC03_F17.Text)) / 3).ToString();
                txt_UsLPC03_F18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_F18.Text), 2));
            }
            //=AVERAGE(G15,G16,G17)
            if (!String.IsNullOrEmpty(txt_UsLPC03_G15.Text) && !String.IsNullOrEmpty(txt_UsLPC03_G16.Text) && !String.IsNullOrEmpty(txt_UsLPC03_G17.Text))
            {
                txt_UsLPC03_G18.Text = ((
                    Convert.ToDecimal(txt_UsLPC03_G15.Text) +
                    Convert.ToDecimal(txt_UsLPC03_G16.Text) +
                    Convert.ToDecimal(txt_UsLPC03_G17.Text)) / 3).ToString();
                txt_UsLPC03_G18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_G18.Text), 2));
            }
            //FORM COVER PAGE
            txt_UsLPC03_B20.Text = this.Lpcs[0].cvp_e19;
            txt_UsLPC03_B22.Text = this.Lpcs[0].cvp_c19;

            //=(C18-B18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC03_C18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B22.Text))
            {

                txt_UsLPC03_B25.Text = (((Convert.ToDecimal(txt_UsLPC03_C18.Text) - Convert.ToDecimal(txt_UsLPC03_B18.Text)) * Convert.ToDecimal(txt_UsLPC03_B20.Text)) /
                                        (Convert.ToDecimal(txt_UsLPC03_B21.Text) * Convert.ToDecimal(txt_UsLPC03_B22.Text))).ToString();
                txt_UsLPC03_B25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_B25.Text)));
            }
            //=(E18-D18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC03_E18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_D18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B22.Text))
            {

                txt_UsLPC03_D25.Text = (((Convert.ToDecimal(txt_UsLPC03_E18.Text) - Convert.ToDecimal(txt_UsLPC03_D18.Text)) * Convert.ToDecimal(txt_UsLPC03_B20.Text)) /
                                        (Convert.ToDecimal(txt_UsLPC03_B21.Text) * Convert.ToDecimal(txt_UsLPC03_B22.Text))).ToString();
                txt_UsLPC03_D25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_D25.Text)));
            }
            //=(G18-F18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC03_G18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_F18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC03_B22.Text))
            {
                txt_UsLPC03_F25.Text = (((Convert.ToDecimal(txt_UsLPC03_G18.Text) - Convert.ToDecimal(txt_UsLPC03_F18.Text)) * Convert.ToDecimal(txt_UsLPC03_B20.Text)) /
                                        (Convert.ToDecimal(txt_UsLPC03_B21.Text) * Convert.ToDecimal(txt_UsLPC03_B22.Text))).ToString();
                txt_UsLPC03_F25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_F25.Text)));
            }
            //=AVERAGE(B25,D25,F25)
            if (!String.IsNullOrEmpty(txt_UsLPC03_B25.Text) &&
            !String.IsNullOrEmpty(txt_UsLPC03_D25.Text) &&
            !String.IsNullOrEmpty(txt_UsLPC03_F25.Text))
            {
                txt_UsLPC03_B26.Text = ((
                    Convert.ToDecimal(txt_UsLPC03_B25.Text) +
                    Convert.ToDecimal(txt_UsLPC03_D25.Text) +
                    Convert.ToDecimal(txt_UsLPC03_F25.Text)) / 3).ToString();
                txt_UsLPC03_B26.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC03_B26.Text)));
            }
            #endregion
            #region "US-LPC(0.6)"
            if (!String.IsNullOrEmpty(txt_UsLPC06_B15.Text) && !String.IsNullOrEmpty(txt_UsLPC06_B16.Text) && !String.IsNullOrEmpty(txt_UsLPC06_B17.Text))
            {
                txt_UsLPC06_B18.Text = ((
                    Convert.ToDecimal(txt_UsLPC06_B15.Text) +
                    Convert.ToDecimal(txt_UsLPC06_B16.Text) +
                    Convert.ToDecimal(txt_UsLPC06_B17.Text)) / 3).ToString();


                txt_UsLPC06_B18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_B18.Text), 2));
            }
            //=AVERAGE(C15,C16,C17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_C15.Text) && !String.IsNullOrEmpty(txt_UsLPC06_C16.Text) && !String.IsNullOrEmpty(txt_UsLPC06_C17.Text))
            {
                txt_UsLPC06_C18.Text = ((
                    Convert.ToDecimal(txt_UsLPC06_C15.Text) +
                    Convert.ToDecimal(txt_UsLPC06_C16.Text) +
                    Convert.ToDecimal(txt_UsLPC06_C17.Text)) / 3).ToString();
                txt_UsLPC06_C18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_C18.Text), 2));
            }
            //=AVERAGE(D15,D16,D17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_D15.Text) && !String.IsNullOrEmpty(txt_UsLPC06_D16.Text) && !String.IsNullOrEmpty(txt_UsLPC06_D17.Text))
            {
                txt_UsLPC06_D18.Text = ((
                    Convert.ToDecimal(txt_UsLPC06_D15.Text) +
                    Convert.ToDecimal(txt_UsLPC06_D16.Text) +
                    Convert.ToDecimal(txt_UsLPC06_D17.Text)) / 3).ToString();
                txt_UsLPC06_D18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_D18.Text), 2));
            }
            //=AVERAGE(E15,E16,E17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_E15.Text) && !String.IsNullOrEmpty(txt_UsLPC06_E16.Text) && !String.IsNullOrEmpty(txt_UsLPC06_E17.Text))
            {
                txt_UsLPC06_E18.Text = ((
                    Convert.ToDecimal(txt_UsLPC06_E15.Text) +
                    Convert.ToDecimal(txt_UsLPC06_E16.Text) +
                    Convert.ToDecimal(txt_UsLPC06_E17.Text)) / 3).ToString();
                txt_UsLPC06_E18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_E18.Text), 2));
            }
            //=AVERAGE(F15,F16,F17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_F15.Text) && !String.IsNullOrEmpty(txt_UsLPC06_F16.Text) && !String.IsNullOrEmpty(txt_UsLPC06_F17.Text))
            {
                txt_UsLPC06_F18.Text = ((
                    Convert.ToDecimal(txt_UsLPC06_F15.Text) +
                    Convert.ToDecimal(txt_UsLPC06_F16.Text) +
                    Convert.ToDecimal(txt_UsLPC06_F17.Text)) / 3).ToString();
                txt_UsLPC06_F18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_F18.Text), 2));
            }
            //=AVERAGE(G15,G16,G17)
            if (!String.IsNullOrEmpty(txt_UsLPC06_G15.Text) && !String.IsNullOrEmpty(txt_UsLPC06_G16.Text) && !String.IsNullOrEmpty(txt_UsLPC06_G17.Text))
            {
                txt_UsLPC06_G18.Text = ((
                    Convert.ToDecimal(txt_UsLPC06_G15.Text) +
                    Convert.ToDecimal(txt_UsLPC06_G16.Text) +
                    Convert.ToDecimal(txt_UsLPC06_G17.Text)) / 3).ToString();
                txt_UsLPC06_G18.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_G18.Text), 2));
            }
            //FORM COVER PAGE
            txt_UsLPC06_B20.Text = this.Lpcs[0].cvp_e19;
            txt_UsLPC06_B22.Text = this.Lpcs[0].cvp_c19;

            //=(C18-B18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC06_C18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B22.Text))
            {

                txt_UsLPC06_B25.Text = (((Convert.ToDecimal(txt_UsLPC06_C18.Text) - Convert.ToDecimal(txt_UsLPC06_B18.Text)) * Convert.ToDecimal(txt_UsLPC06_B20.Text)) /
                                        (Convert.ToDecimal(txt_UsLPC06_B21.Text) * Convert.ToDecimal(txt_UsLPC06_B22.Text))).ToString();
                txt_UsLPC06_B25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_B25.Text)));
            }
            //=(E18-D18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC06_E18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_D18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B22.Text))
            {

                txt_UsLPC06_D25.Text = (((Convert.ToDecimal(txt_UsLPC06_E18.Text) - Convert.ToDecimal(txt_UsLPC06_D18.Text)) * Convert.ToDecimal(txt_UsLPC06_B20.Text)) /
                                        (Convert.ToDecimal(txt_UsLPC06_B21.Text) * Convert.ToDecimal(txt_UsLPC06_B22.Text))).ToString();
                txt_UsLPC06_D25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_D25.Text)));
            }
            //=(G18-F18)*$B$20/($B$21*$B$22)
            if (!String.IsNullOrEmpty(txt_UsLPC06_G18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_F18.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B20.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B21.Text) &&
                !String.IsNullOrEmpty(txt_UsLPC06_B22.Text))
            {
                txt_UsLPC06_F25.Text = (((Convert.ToDecimal(txt_UsLPC06_G18.Text) - Convert.ToDecimal(txt_UsLPC06_F18.Text)) * Convert.ToDecimal(txt_UsLPC06_B20.Text)) /
                                        (Convert.ToDecimal(txt_UsLPC06_B21.Text) * Convert.ToDecimal(txt_UsLPC06_B22.Text))).ToString();
                txt_UsLPC06_F25.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_F25.Text)));
            }
            //=AVERAGE(B25,D25,F25)
            if (!String.IsNullOrEmpty(txt_UsLPC06_B25.Text) &&
            !String.IsNullOrEmpty(txt_UsLPC06_D25.Text) &&
            !String.IsNullOrEmpty(txt_UsLPC06_F25.Text))
            {
                txt_UsLPC06_B26.Text = ((
                    Convert.ToDecimal(txt_UsLPC06_B25.Text) +
                    Convert.ToDecimal(txt_UsLPC06_D25.Text) +
                    Convert.ToDecimal(txt_UsLPC06_F25.Text)) / 3).ToString();
                txt_UsLPC06_B26.Text = String.Format("{0:n2}", Math.Round(Convert.ToDecimal(txt_UsLPC06_B26.Text)));
            }

            #endregion
        }
        #endregion

        protected void cbLPCType68_CheckedChanged(object sender, EventArgs e)
        {
            cbLPCType132.Checked = !cbLPCType68.Checked;

        }

        protected void cbLPCType132_CheckedChanged(object sender, EventArgs e)
        {
            cbLPCType68.Checked = !cbLPCType132.Checked;
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            CalculateCas();
            btnSubmit.Enabled = true;
        }

    }
}