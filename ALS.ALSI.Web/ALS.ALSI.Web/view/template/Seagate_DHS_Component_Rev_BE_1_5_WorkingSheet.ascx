﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Seagate_DHS_Component_Rev_BE_1_5_WorkingSheet.ascx.cs" Inherits="ALS.ALSI.Web.view.template.Seagate_DHS_Component_Rev_BE_1_5_WorkingSheet" %>


<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="alert alert-error hide">
                <button class="close" data-dismiss="alert"></button>
                You have some form errors. Please check below.
            </div>
            <div class="alert alert-success hide">
                <button class="close" data-dismiss="alert"></button>
                Your form validation is successful!
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="tabbable tabbable-custom boxless">
                        <ul class="nav nav-tabs">
                            <asp:Button ID="btnDHS" runat="server" Text="DHS" CssClass="btn blue" OnClick="btnCoverPage_Click" />
                            <asp:Button ID="btnCoverPage" runat="server" Text="Cover Page" CssClass="btn blue" OnClick="btnCoverPage_Click" />
                        </ul>
                        <div class="tab-content">
                            <asp:Panel ID="pDSH" runat="server">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                            <h6>Results:</h6>
                                            <h6>
                                                <asp:Label ID="lbResultDesc" runat="server" Text=""></asp:Label></h6>

                                            <div class="row-fluid">
                                                <div class="span12">

                                                    <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False"
                                                        CssClass="table table-striped table-bordered mini" ShowHeaderWhenEmpty="True" ShowFooter="true" DataKeyNames="ID" OnRowEditing="gvResult_RowEditing" OnRowCancelingEdit="gvResult_RowCancelingEdit" OnRowUpdating="gvResult_RowUpdating" OnRowDataBound="gvResult_RowDataBound" OnRowDeleting="gvResult_RowDeleting" OnRowCommand="gvResult_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Pk#" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litPk" runat="server" Text='<%# Eval("pk")%>' />
                                                                    <asp:HiddenField ID="hdfID" Value='<%# Eval("ID")%>' runat="server" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="RT" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litRT" runat="server" Text='<%# Eval("rt")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Library/ID" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litLibraryID" runat="server" Text='<%# Eval("library_id")%>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="ddlLibrary" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLibrary_SelectedIndexChanged" class="span12 chosen"></asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <%--                                            <FooterTemplate>
                                                                    <asp:Literal ID="litTotalOUtgassing" runat="server" Text='Total Outgassing' />
                                                                </FooterTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />--%>
                                                            </asp:TemplateField>

                                                            <%--EDIT--%>
                                                            <asp:TemplateField HeaderText="Classification" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litClass" runat="server" Text='<%# Eval("classification")%>'></asp:Literal>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="CAS#" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litCAS" runat="server" Text='<%# Eval("cas")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Qual" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litQual" runat="server" Text='<%# Eval("qual")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Area" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litArea" runat="server" Text='<%# Eval("area")%>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtArea" CssClass="m-wrap small" runat="server" Text='<%# Eval("area")%>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <%--                                  <FooterTemplate>
                                                                    <asp:Literal ID="litTotalArea" runat="server" Text='0' />
                                                                </FooterTemplate>--%>
                                                                <ItemStyle HorizontalAlign="Right" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Amount(ng/part)" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litAmount" runat="server" Text='<%# Eval("amount")%>' />
                                                                </ItemTemplate>
                                                                <%--           <FooterTemplate>
                                                                    <asp:Literal ID="litTotalAmout" runat="server" Text='0' />
                                                                </FooterTemplate>--%>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <%--                                                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"></asp:DropDownList>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnEdit" runat="server" ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("ID")%>'><i class="icon-edit"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this record ?');"
                                                                        CommandArgument='<%# Eval("ID")%>'><i class="icon-trash"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnReuse" runat="server" ToolTip="ReUse" CommandName="ReUse" OnClientClick="return confirm('Are you sure you want to reuse this item ?');"
                                                                        CommandArgument='<%# Eval("ID")%>'><i class="icon-refresh"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="btnUpdate" runat="server" ToolTip="Update" ValidationGroup="CreditLineGrid"
                                                                        CommandName="Update"><i class="icon-save"></i></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkCancel" runat="server" ToolTip="Cancel" CausesValidation="false"
                                                                        CommandName="Cancel"><i class="icon-remove"></i></asp:LinkButton>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerTemplate>
                                                            <div class="pagination">
                                                                <ul>
                                                                    <li>
                                                                        <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                                            CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                                            CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                                                    </li>
                                                                    <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                                                    <li>
                                                                        <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                                            CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                                                    </li>
                                                                    <li>
                                                                        <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                                            CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </PagerTemplate>
                                                        <EmptyDataTemplate>
                                                            <div class="data-not-found">
                                                                <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                                            </div>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>

                                                </div>
                                            </div>

                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="control-group">
                                                        <label class="control-label" for="txtSampleSize">
                                                            <asp:Label ID="Label2" runat="server" Text="SampleSize:"></asp:Label>
                                                        </label>
                                                        <asp:TextBox ID="txtSampleSize" name="txtSampleSize" runat="server" CssClass="mini m-wrap" Text="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="control-group">
                                                        <label class="control-label" for="txtHexadecane">
                                                            <asp:Label ID="Label3" runat="server" Text="Hexadecane:"></asp:Label>
                                                        </label>
                                                        <asp:TextBox ID="txtHexadecane" name="txtUnit" runat="server" CssClass="mini m-wrap" Text="0"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <div class="control-group">
                                                        <label class="control-label" for="txtUnit">
                                                            <asp:Label ID="Label1" runat="server" Text="Unit:"></asp:Label>
                                                        </label>
                                                        <asp:DropDownList ID="ddlUnit" runat="server" class="span12 chosen">
                                                            <asp:ListItem Selected="True">100</asp:ListItem>
                                                            <asp:ListItem>1000</asp:ListItem>
                                                        </asp:DropDownList>
                                                        ng
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row-fluid fileupload-buttonbar">
                                                <div class="span6">
                                                    <%--<label class="control-label" for="ddlSpecification">Uplod file:</label>--%>
                                                    <span class="btn green fileinput-button">
                                                        <i class="icon-plus icon-white"></i>
                                                        <span>Choose DSH files...</span>
                                                        <asp:FileUpload ID="btnUpload" runat="server" AllowMultiple="True" />
                                                    </span>

                                                </div>
                                            </div>
                                            <div class="row-fluid">
                                                <ul>
                                                    <li>The maximum file size for uploads is <strong>5 MB</strong> (default file size is unlimited).</li>
                                                    <li>Only text files (<strong>txt</strong>) are allowed.</li>
                                                </ul>
                                                <br />
                                                <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                                                <br />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="pCoverpage" runat="server">
                                <div class="tab-pane" id="tab_2">
                                    <asp:GridView ID="gvCoverPages" runat="server" AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered mini" ShowHeaderWhenEmpty="True" DataKeyNames="ID,component_id" OnRowDataBound="gvCoverPages_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Compound" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="litName" runat="server" Text='<%# Eval("name")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("name")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Maximum Allowable Amount" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Literal ID="litNgPart" runat="server" Text='<%# Eval("ng_part")%>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtNgPart" runat="server" Text='<%# Eval("ng_part")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="result" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Literal ID="litResult" runat="server" Text='<%# Eval("result")%>' />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtResult" runat="server" Text='<%# Eval("result")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerTemplate>
                                            <div class="pagination">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                            CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                            CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                                    </li>
                                                    <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                                    <li>
                                                        <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                            CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                            CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            <div class="data-not-found">
                                                <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                            </div>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnLoadFile" runat="server" Text="Load" CssClass="btn blue" OnClick="btnLoadFile_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="disable btn" OnClick="btnCancel_Click" />
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoadFile" />
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</form>

