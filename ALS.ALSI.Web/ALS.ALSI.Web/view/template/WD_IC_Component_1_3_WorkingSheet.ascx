﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WD_IC_Component_1_3_WorkingSheet.ascx.cs" Inherits="ALS.ALSI.Web.view.template.WD_IC_Component_1_3_WorkingSheet" %>
<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var form1 = $('#Form1');
        var error1 = $('.alert-error', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-inline', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                ctl00$ContentPlaceHolder2$ctl00$txtB9: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB10: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB11: {
                    required: true,
                    number: true
                },

                ctl00$ContentPlaceHolder2$ctl00$txtB14_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB15_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB16_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB17_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB18_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB19_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB20_Chem: {
                    required: true,
                    number: true
                },

                ctl00$ContentPlaceHolder2$ctl00$txtC14_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC15_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC16_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC17_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC18_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC19_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC20_Chem: {
                    required: true,
                    number: true
                },


                ctl00$ContentPlaceHolder2$ctl00$txtB23_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB24_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB25_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB26_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB27_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtB28_Chem: {
                    required: true,
                    number: true
                },

                ctl00$ContentPlaceHolder2$ctl00$txtC23_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC24_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC25_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC26_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC27_Chem: {
                    required: true,
                    number: true
                },
                ctl00$ContentPlaceHolder2$ctl00$txtC28_Chem: {
                    required: true,
                    number: true
                },

            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.help-inline').removeClass('ok'); // display OK icon
                $(element)
                    .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            },


            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="alert alert-error hide">
                <button class="close" data-dismiss="alert"></button>
                You have some form errors. Please check below.
            </div>
            <div class="alert alert-success hide">
                <button class="close" data-dismiss="alert"></button>
                Your form validation is successful!
            </div>

            <div class="row-fluid invoice">
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover">
                            <tbody>
                                <tr>
                                    <td>Total volume (TV) :</td>
                                    <td>
                                        <asp:TextBox ID="txtB11" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Surface area (A) :</td>
                                    <td>
                                        <asp:TextBox ID="txtB12" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>No of parts extracted (N)</td>
                                    <td>
                                        <asp:TextBox ID="txtB13" runat="server"></asp:TextBox></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hDdlSpec" runat="server" Value="0" />
            <asp:HiddenField ID="hItemVisible" runat="server" Value="000000000000000" />
            <br />
            <div class="row-fluid">
                <div class="span12 ">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Anions</th>
                                <th>Conc of water blank,ug/L (B)</th>
                                <th>Conc of sample,ug/L (C)</th>
                                <th>Dilution Factor</th>
                                <th>Raw Result (ug/sq cm)</th>
                                <th>Instrument Detection Limit (ug/L)</th>
                                <th>Method Detection Limit (ug/sq cm)</th>
                                <th>Below Detection? (1=Yes, 0=No)</th>
                                <th>Final Conc of sample, (ug/sq cm)</th>
                                <th>for use in Total (ug/sq cm)</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Fluoride, F</td>
                                <td>
                                    <asp:TextBox ID="txtB17" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC17" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD17" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE17" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnF17" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH17" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI17" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ17" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Chloride, Cl</td>
                                <td>
                                    <asp:TextBox ID="txtB18" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC18" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD18" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE18" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnF18" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH18" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI18" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ18" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Bromide, Br</td>
                                <td>
                                    <asp:TextBox ID="txtB19" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC19" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD19" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE19" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnF19" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH19" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI19" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ19" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Nitrate, NO3</td>
                                <td>
                                    <asp:TextBox ID="txtB20" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC20" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD20" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE20" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnF20" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH20" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI20" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ20" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Sulfate, SO4</td>
                                <td>
                                    <asp:TextBox ID="txtB21" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC21" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD21" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE21" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnF21" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH21" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI21" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ21" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Phosphate, PO4</td>
                                <td>
                                    <asp:TextBox ID="txtB22" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC22" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD22" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE22" runat="server"></asp:Label></td>
                                <td>0.5</td>
                                <td>
                                    <asp:Label ID="lbAnF22" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH22" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI22" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ22" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="lbAnI23" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ23" runat="server"></asp:Label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span12 ">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Cations</th>
                                <th>Conc of water blank,ug/L (B)</th>
                                <th>Conc of sample,ug/L (C)</th>
                                <th>Dilution Factor</th>
                                <th>Raw Result (ug/sq cm)</th>
                                <th>Instrument Detection Limit (ug/L)</th>
                                <th>Method Detection Limit (ug/sq cm)</th>
                                <th>Below Detection? (1=Yes, 0=No)</th>
                                <th>FinalConc of sample, (ug/sq cm)</th>
                                <th>for use in Total (ug/sq cm)</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>Lithium, Li</td>
                                <td>
                                    <asp:TextBox ID="txtB26" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC26" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD26" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE26" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnF26" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH26" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI26" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ26" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Sodium, Na</td>
                                <td>
                                    <asp:TextBox ID="txtB27" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC27" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD27" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE27" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnF27" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH27" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI27" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ27" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Ammonium, NH4</td>


                                <td>
                                    <asp:TextBox ID="txtB28" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC28" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD28" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE28" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnF28" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH28" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI28" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ28" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Potassium, K</td>


                                <td>
                                    <asp:TextBox ID="txtB29" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC29" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD29" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE29" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnF29" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH29" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI29" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ29" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Magnesium, Mg
                                </td>
                                <td>
                                    <asp:TextBox ID="txtB30" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC30" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD30" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE30" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnF30" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH30" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI30" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ30" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Calcium, Ca
                                </td>
                                <td>
                                    <asp:TextBox ID="txtB31" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtC31" runat="server"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtD31" runat="server">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lbAnE31" runat="server"></asp:Label></td>
                                <td>0.6</td>
                                <td>
                                    <asp:Label ID="lbAnF31" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnH31" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnI31" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ31" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="lbAnI32" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbAnJ32" runat="server"></asp:Label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-actions clearfix">
                    <asp:Button ID="btnCalulate" runat="server" Text="Calculate" CssClass="btn blue" OnClick="btnCalulate_Click" />
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="disable btn" OnClick="btnCancel_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <%--         <asp:PostBackTrigger ControlID="btnCalulate" />--%>
        </Triggers>
    </asp:UpdatePanel>

</form>