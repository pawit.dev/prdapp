﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HPA_WD_Template_for_3_point.ascx.cs" Inherits="ALS.ALSI.Web.view.template.HPA_WD_Template_for_3_point" %>

<script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.date-picker').datepicker();
    });
</script>
<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <!-- BEGIN COVERPAGE-TH CONTENT-->
            <div class="row-fluid invoice" id="invDiv" runat="server">
                <div>
                    CUSTOMER PO NO.:
                        <asp:Label ID="lbPoNo" runat="server" Text=""></asp:Label><br />
                    ALS THAILAND REF NO.:
                        <asp:Label ID="lbRefNo" runat="server" Text=""></asp:Label><br />
                    DATE:
                        <asp:Label ID="lbDate" runat="server" Text=""></asp:Label><br />
                    COMPANY:
                        <asp:Label ID="lbCompany" runat="server" Text=""></asp:Label><br />
                    <br />
                    DATE SAMPLE RECEIVED:
                        <asp:Label ID="lbDateSampleReceived" runat="server" Text=""></asp:Label><br />
                    DATE TEST COMPLETED:
                        <asp:Label ID="lbDateTestCompleted" runat="server" Text=""></asp:Label><br />
                    <br />
                    SAMPLE DESCRIPTION: One lot of sample was received with references:<br />
                    <asp:Label ID="lbSampleDescription" runat="server" Text=""></asp:Label>
                </div>

                <div class="row-fluid">
                    <div class="span12 ">
                        <h5>METHOD:</h5>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Particle Analysis by SEM EDX</th>
                                    <th>Taped Area For Drive Parts</th>
                                    <th>No. of Times Taped</th>
                                    <th>Surface Area Analysed (cm2)</th>
                                    <th>Particle Ranges</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbA23" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB23" runat="server" Text="Arm"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC23" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_D23" runat="server" Text="0.0209"></asp:TextBox>
                                        <asp:Label ID="lbD23" runat="server" Text="0.0209"></asp:Label>
                                    </td>
                                    <td>0.5um - 5.0um</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbA24" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB24" runat="server" Text="Pivot"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC24" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_D24" runat="server" Text="0.0209"></asp:TextBox>
                                        <asp:Label ID="lbD24" runat="server" Text="0.0209"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbA25" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB25" runat="server" Text="Swage"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC25" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtCVP_D25" runat="server" Text="0.0209"></asp:TextBox>
                                        <asp:Label ID="lbD25" runat="server" Text="0.0209"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6 ">
                        <h6>Results:</h6>
                        <h6>The Specification is based on WD's specification Doc No 
                            <asp:Label ID="lbDocNo" runat="server" Text=""></asp:Label>
                            for
                            <asp:Label ID="lbComponent" runat="server" Text=""></asp:Label>
                        </h6>
                    </div>
                </div>
                <%-- RESULT--%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="tb1" runat="server">
                            <thead>

                                <tr>
                                    <th></th>
                                    <th>Total Hard Particles/cm2</th>
                                    <th>Total MgSiO Particles/cm2</th>
                                    <th>Total Steel Particles/cm2</th>
                                    <th>Total Magnetic Particles/cm2</th>
                                    <th runat="server" id="th1">
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr id="tr1">
                                    <td>Result on Arm</td>

                                    <td>
                                        <asp:Label ID="lbB30" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC30" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD30" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE30" runat="server" Text=""></asp:Label></td>
                                    <td runat="server">
                                        <asp:CheckBox ID="CheckBox2" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr2">
                                    <td>Result on Pivot</td>

                                    <td>
                                        <asp:Label ID="lbB31" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC31" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD31" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE31" runat="server" Text=""></asp:Label></td>
                                    <td runat="server">
                                        <asp:CheckBox ID="CheckBox3" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr3">
                                    <td>Result on Swage</td>

                                    <td>
                                        <asp:Label ID="lbB32" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC32" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD32" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE32" runat="server" Text=""></asp:Label></td>
                                    <td runat="server">
                                        <asp:CheckBox ID="CheckBox4" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr4">
                                    <td>Average result</td>

                                    <td>
                                        <asp:Label ID="lbB33" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC33" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD33" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE33" runat="server" Text=""></asp:Label></td>
                                    <td id="Td4" runat="server">
                                        <asp:CheckBox ID="CheckBox5" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr5">
                                    <td>Specification</td>

                                    <td>
                                        <asp:Label ID="lbB34" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC34" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD34" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE34" runat="server" Text=""></asp:Label></td>
                                    <td id="Td5" runat="server">
                                        <asp:CheckBox ID="CheckBox6" runat="server" Checked="True" /></td>
                                </tr>
                                <tr id="tr6">
                                    <td>PASS / FAIL</td>

                                    <td>
                                        <asp:Label ID="lbB35" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC35" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbD35" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbE35" runat="server" Text=""></asp:Label></td>
                                    <td id="Td6" runat="server">
                                        <asp:CheckBox ID="CheckBox7" runat="server" Checked="True" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <h6>Total hard particles/cm2 = total no. of hard particles/(no. of times taped x surface area analysed)</h6>
                <br />
                <table>
                    <tr>
                        <td>Stage Mimic of Arm</td>
                        <td>Stage Mimic of Pivot</td>
                        <td>Stage Mimic of Swage</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbImgPath1" runat="server" Text=""></asp:Label><br />
                            <asp:Image ID="img1" runat="server" Width="120" Height="120" /></td>
                        <td>
                            <asp:Label ID="lbImgPath2" runat="server" Text=""></asp:Label><br />
                            <asp:Image ID="img2" runat="server" Width="120" Height="120" /></td>
                        <td>
                            <asp:Label ID="lbImgPath3" runat="server" Text=""></asp:Label><br />
                            <asp:Image ID="img3" runat="server" Width="120" Height="120" /></td>
                    </tr>
                </table>
                <%--<asp:FileUpload ID="FileUpload1" runat="server" />--%>
                <h6>Please refer to attachment for details of results</h6>
                <h6>Note: This report was performed test by ALS Singapore.</h6>
                <br />

                <h5>Analysis Table on Arm</h5>
                <%-- Analysis Table on Arm --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table3" runat="server">
                            <thead>
                                <tr>
                                    <th>Elemental Composition</th>
                                    <th>Count</th>
                                    <th>Particle/cm2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr56">
                                    <td>
                                        <asp:Label ID="lbA56" runat="server" Text="Hard Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB56" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC56" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr57">
                                    <td>
                                        <asp:Label ID="lbA57" runat="server" Text="  Al-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB57" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC57" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr58">
                                    <td>
                                        <asp:Label ID="lbA58" runat="server" Text="  Al-Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB58" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC58" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr59">
                                    <td>
                                        <asp:Label ID="lbA59" runat="server" Text="  Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB59" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC59" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr60">
                                    <td>
                                        <asp:Label ID="lbA60" runat="server" Text="  Si-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB60" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC60" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr61">
                                    <td>
                                        <asp:Label ID="lbA61" runat="server" Text="  Al-Cu-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB61" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC61" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr62">
                                    <td>
                                        <asp:Label ID="lbA62" runat="server" Text="  Al-Mg-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB62" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC62" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr63">
                                    <td>
                                        <asp:Label ID="lbA63" runat="server" Text="  Al-Si-Cu-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB63" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC63" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr64">
                                    <td>
                                        <asp:Label ID="lbA64" runat="server" Text="  Al-Si-Fe-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB64" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC64" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr65">
                                    <td>
                                        <asp:Label ID="lbA65" runat="server" Text="  Al-Si-Mg-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB65" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC65" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr66">
                                    <td>
                                        <asp:Label ID="lbA66" runat="server" Text="  Al-Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB66" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC66" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr67">
                                    <td>
                                        <asp:Label ID="lbA67" runat="server" Text="  Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB67" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC67" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr68">
                                    <td>
                                        <asp:Label ID="lbA68" runat="server" Text="  Ti-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB68" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC68" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr69">
                                    <td>
                                        <asp:Label ID="lbA69" runat="server" Text="  Ti-B"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB69" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC69" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr70">
                                    <td>
                                        <asp:Label ID="lbA70" runat="server" Text="  Ti-N"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB70" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC70" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr71">
                                    <td>
                                        <asp:Label ID="lbA71" runat="server" Text="  W-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB71" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC71" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr72">
                                    <td>
                                        <asp:Label ID="lbA72" runat="server" Text="  W-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB72" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC72" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr73">
                                    <td>
                                        <asp:Label ID="lbA73" runat="server" Text="  Zr-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB73" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC73" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr74">
                                    <td>
                                        <asp:Label ID="lbA74" runat="server" Text="  Zr-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB74" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC74" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr75">
                                    <td>
                                        <asp:Label ID="lbA75" runat="server" Text="  Pb-Zr-Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB75" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC75" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr76">
                                    <td>
                                        <asp:Label ID="lbA76" runat="server" Text="Subtotal - Hard Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB76" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC76" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr77">
                                    <td>
                                        <asp:Label ID="lbA77" runat="server" Text="Magnetic Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB77" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC77" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr78">
                                    <td>
                                        <asp:Label ID="lbA78" runat="server" Text="  Ce-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB78" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC78" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr79">
                                    <td>
                                        <asp:Label ID="lbA79" runat="server" Text="  Fe-Nd"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB79" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC79" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr80">
                                    <td>
                                        <asp:Label ID="lbA80" runat="server" Text="  Fe-Sm"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB80" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC80" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr81">
                                    <td>
                                        <asp:Label ID="lbA81" runat="server" Text="  Fe-Sr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB81" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC81" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr82">
                                    <td>
                                        <asp:Label ID="lbA82" runat="server" Text="  Nd-Pr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB82" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC82" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr83">
                                    <td>
                                        <asp:Label ID="lbA83" runat="server" Text="  Ni-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB83" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC83" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr84">
                                    <td>
                                        <asp:Label ID="lbA84" runat="server" Text="  Sm-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB84" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC84" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr85">
                                    <td>
                                        <asp:Label ID="lbA85" runat="server" Text="Subtotal - Magnetic Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB85" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC85" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr86">
                                    <td>
                                        <asp:Label ID="lbA86" runat="server" Text="Subtotal- Hard Particles including Magnetic Partilces"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB86" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC86" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr101">
                                    <td>
                                        <asp:Label ID="lbA101" runat="server" Text="Steel Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB101" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC101" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr102">
                                    <td>
                                        <asp:Label ID="lbA102" runat="server" Text="  SS 300- Fe-Cr-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB102" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC102" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr103">
                                    <td>
                                        <asp:Label ID="lbA103" runat="server" Text="  SS 300- Fe-Cr-Ni-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB103" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC103" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr104">
                                    <td>
                                        <asp:Label ID="lbA104" runat="server" Text="  SS 300- Fe-Cr-Ni-Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB104" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC104" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr105">
                                    <td>
                                        <asp:Label ID="lbA105" runat="server" Text="  SS 400- Fe-Cr "></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB105" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC105" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr106">
                                    <td>
                                        <asp:Label ID="lbA106" runat="server" Text="  SS 400- Fe-Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB106" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC106" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr107">
                                    <td>
                                        <asp:Label ID="lbA107" runat="server" Text="  Other Steel - Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB107" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC107" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr108">
                                    <td>
                                        <asp:Label ID="lbA108" runat="server" Text="  Other Steel - Fe-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB108" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC108" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr109">
                                    <td>
                                        <asp:Label ID="lbA109" runat="server" Text="  Other Steel - Fe-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB109" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC109" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr110">
                                    <td>
                                        <asp:Label ID="lbA110" runat="server" Text="  Other Steel - Fe-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB110" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC110" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr111">
                                    <td>
                                        <asp:Label ID="lbA111" runat="server" Text="Subtotal - Steel Particle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB111" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC111" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr112">
                                    <td>
                                        <asp:Label ID="lbA112" runat="server" Text="Other Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB112" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC112" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr113">
                                    <td>
                                        <asp:Label ID="lbA113" runat="server" Text="  No Element"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB113" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC113" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr114">
                                    <td>
                                        <asp:Label ID="lbA114" runat="server" Text="  Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB114" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC114" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr115">
                                    <td>
                                        <asp:Label ID="lbA115" runat="server" Text="  Ni-P"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB115" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC115" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr116">
                                    <td>
                                        <asp:Label ID="lbA116" runat="server" Text="  Sn base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB116" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC116" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr117">
                                    <td>
                                        <asp:Label ID="lbA117" runat="server" Text="  Other"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB117" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC117" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr118">
                                    <td>
                                        <asp:Label ID="lbA118" runat="server" Text="  Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB118" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC118" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr119">
                                    <td>
                                        <asp:Label ID="lbA119" runat="server" Text="  Al-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB119" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC119" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr120">
                                    <td>
                                        <asp:Label ID="lbA120" runat="server" Text="  Al-Ti"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB120" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC120" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr121">
                                    <td>
                                        <asp:Label ID="lbA121" runat="server" Text="  Al-Si (1)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB121" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC121" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr122">
                                    <td>
                                        <asp:Label ID="lbA122" runat="server" Text="  Al-Si (2)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB122" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC122" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr123">
                                    <td>
                                        <asp:Label ID="lbA123" runat="server" Text="  Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB123" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC123" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr124">
                                    <td>
                                        <asp:Label ID="lbA124" runat="server" Text="  Al-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB124" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC124" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr125">
                                    <td>
                                        <asp:Label ID="lbA125" runat="server" Text="  Al-Si-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB125" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC125" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr126">
                                    <td>
                                        <asp:Label ID="lbA126" runat="server" Text="  Al-Si-Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB126" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC126" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr127">
                                    <td>
                                        <asp:Label ID="lbA127" runat="server" Text="  Al-Si-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB127" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC127" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr128">
                                    <td>
                                        <asp:Label ID="lbA128" runat="server" Text="  Mg-Si-O-Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB128" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC128" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr129">
                                    <td>
                                        <asp:Label ID="lbA129" runat="server" Text="  Mg-Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB129" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC129" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr130">
                                    <td>
                                        <asp:Label ID="lbA130" runat="server" Text="  Ti"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB130" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC130" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr131">
                                    <td>
                                        <asp:Label ID="lbA131" runat="server" Text="  Nd"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB131" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC131" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr132">
                                    <td>
                                        <asp:Label ID="lbA132" runat="server" Text="  S-Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB132" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC132" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr133">
                                    <td>
                                        <asp:Label ID="lbA133" runat="server" Text="  Zn base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB133" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC133" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr134">
                                    <td>
                                        <asp:Label ID="lbA134" runat="server" Text="  Ca base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB134" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC134" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr135">
                                    <td>
                                        <asp:Label ID="lbA135" runat="server" Text="  Ni base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB135" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC135" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr136">
                                    <td>
                                        <asp:Label ID="lbA136" runat="server" Text="  Cr base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB136" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC136" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr137">
                                    <td>
                                        <asp:Label ID="lbA137" runat="server" Text="  Zr base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB137" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC137" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr138">
                                    <td>
                                        <asp:Label ID="lbA138" runat="server" Text="  Cl base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB138" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC138" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr139">
                                    <td>
                                        <asp:Label ID="lbA139" runat="server" Text="  Na-Cl"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB139" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC139" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr140">
                                    <td>
                                        <asp:Label ID="lbA140" runat="server" Text="  Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB140" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC140" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr141">
                                    <td>
                                        <asp:Label ID="lbA141" runat="server" Text="  Cu-Au"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB141" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC141" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr142">
                                    <td>
                                        <asp:Label ID="lbA142" runat="server" Text="  Ag-S"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB142" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC142" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr143">
                                    <td>
                                        <asp:Label ID="lbA143" runat="server" Text="  Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB143" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC143" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr144">
                                    <td>
                                        <asp:Label ID="lbA144" runat="server" Text="  Ag"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB144" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC144" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr145">
                                    <td>
                                        <asp:Label ID="lbA145" runat="server" Text="  Au"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB145" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC145" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr146">
                                    <td>
                                        <asp:Label ID="lbA146" runat="server" Text="  Cu-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB146" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC146" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr147">
                                    <td>
                                        <asp:Label ID="lbA147" runat="server" Text="  Cu-Zn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB147" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC147" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr148">
                                    <td>
                                        <asp:Label ID="lbA148" runat="server" Text="  Cu-Zn-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB148" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC148" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr149">
                                    <td>
                                        <asp:Label ID="lbA149" runat="server" Text="  Cu-Zn-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB149" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC149" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr150">
                                    <td>
                                        <asp:Label ID="lbA150" runat="server" Text="  Zn-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB150" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC150" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr151">
                                    <td>
                                        <asp:Label ID="lbA151" runat="server" Text="  Pb"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB151" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC151" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr152">
                                    <td>
                                        <asp:Label ID="lbA152" runat="server" Text="  Fe-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB152" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC152" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr153">
                                    <td>
                                        <asp:Label ID="lbA153" runat="server" Text="  Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB153" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC153" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr154">
                                    <td>
                                        <asp:Label ID="lbA154" runat="server" Text="  Al-Si Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB154" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC154" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr155">
                                    <td>
                                        <asp:Label ID="lbA155" runat="server" Text="  F-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB155" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC155" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr156">
                                    <td>
                                        <asp:Label ID="lbA156" runat="server" Text="  Cu-S-Al-O Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB156" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC156" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr157">
                                    <td>
                                        <asp:Label ID="lbA157" runat="server" Text="  Ti-O/Al-Si-Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB157" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC157" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr158">
                                    <td>
                                        <asp:Label ID="lbA158" runat="server" Text="  Cr-Rich (Cr Base + Cr-Mn)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB158" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC158" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr159">
                                    <td>
                                        <asp:Label ID="lbA159" runat="server" Text="Subtotal - Other Particle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB159" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC159" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr160">
                                    <td>
                                        <asp:Label ID="lbA160" runat="server" Text="Grand Total of Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB160" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC160" runat="server" Text=""></asp:Label></td>
                                </tr>

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <h5>Analysis Table on Pivot </h5>
                <%-- Analysis Table on Pivot --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table1" runat="server">
                            <thead>
                                <tr>
                                    <th>Elemental Composition</th>
                                    <th>Count</th>
                                    <th>Particle/cm2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr172">
                                    <td>
                                        <asp:Label ID="lbA172" runat="server" Text="Hard Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB172" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC172" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr173">
                                    <td>
                                        <asp:Label ID="lbA173" runat="server" Text="  Al-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB173" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC173" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr174">
                                    <td>
                                        <asp:Label ID="lbA174" runat="server" Text="  Al-Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB174" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC174" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr175">
                                    <td>
                                        <asp:Label ID="lbA175" runat="server" Text="  Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB175" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC175" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr176">
                                    <td>
                                        <asp:Label ID="lbA176" runat="server" Text="  Si-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB176" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC176" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr177">
                                    <td>
                                        <asp:Label ID="lbA177" runat="server" Text="  Al-Cu-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB177" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC177" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr178">
                                    <td>
                                        <asp:Label ID="lbA178" runat="server" Text="  Al-Mg-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB178" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC178" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr179">
                                    <td>
                                        <asp:Label ID="lbA179" runat="server" Text="  Al-Si-Cu-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB179" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC179" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr180">
                                    <td>
                                        <asp:Label ID="lbA180" runat="server" Text="  Al-Si-Fe-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB180" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC180" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr181">
                                    <td>
                                        <asp:Label ID="lbA181" runat="server" Text="  Al-Si-Mg-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB181" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC181" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr182">
                                    <td>
                                        <asp:Label ID="lbA182" runat="server" Text="  Al-Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB182" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC182" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr183">
                                    <td>
                                        <asp:Label ID="lbA183" runat="server" Text="  Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB183" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC183" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr184">
                                    <td>
                                        <asp:Label ID="lbA184" runat="server" Text="  Ti-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB184" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC184" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr185">
                                    <td>
                                        <asp:Label ID="lbA185" runat="server" Text="  Ti-B"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB185" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC185" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr186">
                                    <td>
                                        <asp:Label ID="lbA186" runat="server" Text="  Ti-N"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB186" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC186" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr187">
                                    <td>
                                        <asp:Label ID="lbA187" runat="server" Text="  W-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB187" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC187" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr188">
                                    <td>
                                        <asp:Label ID="lbA188" runat="server" Text="  W-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB188" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC188" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr189">
                                    <td>
                                        <asp:Label ID="lbA189" runat="server" Text="  Zr-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB189" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC189" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr190">
                                    <td>
                                        <asp:Label ID="lbA190" runat="server" Text="  Zr-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB190" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC190" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr191">
                                    <td>
                                        <asp:Label ID="lbA191" runat="server" Text="  Pb-Zr-Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB191" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC191" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr192">
                                    <td>
                                        <asp:Label ID="lbA192" runat="server" Text="Subtotal - Hard Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB192" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC192" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr193">
                                    <td>
                                        <asp:Label ID="lbA193" runat="server" Text="Magnetic Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB193" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC193" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr194">
                                    <td>
                                        <asp:Label ID="lbA194" runat="server" Text="  Ce-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB194" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC194" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr195">
                                    <td>
                                        <asp:Label ID="lbA195" runat="server" Text="  Fe-Nd"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB195" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC195" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr196">
                                    <td>
                                        <asp:Label ID="lbA196" runat="server" Text="  Fe-Sm"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB196" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC196" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr197">
                                    <td>
                                        <asp:Label ID="lbA197" runat="server" Text="  Fe-Sr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB197" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC197" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr198">
                                    <td>
                                        <asp:Label ID="lbA198" runat="server" Text="  Nd-Pr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB198" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC198" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr199">
                                    <td>
                                        <asp:Label ID="lbA199" runat="server" Text="  Ni-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB199" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC199" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr200">
                                    <td>
                                        <asp:Label ID="lbA200" runat="server" Text="  Sm-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB200" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC200" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr201">
                                    <td>
                                        <asp:Label ID="lbA201" runat="server" Text="Subtotal - Magnetic Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB201" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC201" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr202">
                                    <td>
                                        <asp:Label ID="lbA202" runat="server" Text="Subtotal- Hard Particles including Magnetic Partilces"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB202" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC202" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr217">
                                    <td>
                                        <asp:Label ID="lbA217" runat="server" Text="Steel Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB217" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC217" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr218">
                                    <td>
                                        <asp:Label ID="lbA218" runat="server" Text="  SS 300- Fe-Cr-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB218" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC218" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr219">
                                    <td>
                                        <asp:Label ID="lbA219" runat="server" Text="  SS 300- Fe-Cr-Ni-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB219" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC219" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr220">
                                    <td>
                                        <asp:Label ID="lbA220" runat="server" Text="  SS 300- Fe-Cr-Ni-Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB220" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC220" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr221">
                                    <td>
                                        <asp:Label ID="lbA221" runat="server" Text="  SS 400- Fe-Cr "></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB221" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC221" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr222">
                                    <td>
                                        <asp:Label ID="lbA222" runat="server" Text="  SS 400- Fe-Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB222" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC222" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr223">
                                    <td>
                                        <asp:Label ID="lbA223" runat="server" Text="  Other Steel - Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB223" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC223" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr224">
                                    <td>
                                        <asp:Label ID="lbA224" runat="server" Text="  Other Steel - Fe-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB224" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC224" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr225">
                                    <td>
                                        <asp:Label ID="lbA225" runat="server" Text="  Other Steel - Fe-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB225" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC225" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr226">
                                    <td>
                                        <asp:Label ID="lbA226" runat="server" Text="  Other Steel - Fe-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB226" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC226" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr227">
                                    <td>
                                        <asp:Label ID="lbA227" runat="server" Text="Subtotal - Steel Particle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB227" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC227" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr228">
                                    <td>
                                        <asp:Label ID="lbA228" runat="server" Text="Other Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB228" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC228" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr229">
                                    <td>
                                        <asp:Label ID="lbA229" runat="server" Text="  No Element"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB229" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC229" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr230">
                                    <td>
                                        <asp:Label ID="lbA230" runat="server" Text="  Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB230" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC230" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr231">
                                    <td>
                                        <asp:Label ID="lbA231" runat="server" Text="  Ni-P"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB231" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC231" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr232">
                                    <td>
                                        <asp:Label ID="lbA232" runat="server" Text="  Sn base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB232" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC232" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr233">
                                    <td>
                                        <asp:Label ID="lbA233" runat="server" Text="  Other"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB233" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC233" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr234">
                                    <td>
                                        <asp:Label ID="lbA234" runat="server" Text="  Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB234" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC234" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr235">
                                    <td>
                                        <asp:Label ID="lbA235" runat="server" Text="  Al-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB235" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC235" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr236">
                                    <td>
                                        <asp:Label ID="lbA236" runat="server" Text="  Al-Ti"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB236" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC236" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr237">
                                    <td>
                                        <asp:Label ID="lbA237" runat="server" Text="  Al-Si (1)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB237" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC237" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr238">
                                    <td>
                                        <asp:Label ID="lbA238" runat="server" Text="  Al-Si (2)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB238" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC238" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr239">
                                    <td>
                                        <asp:Label ID="lbA239" runat="server" Text="  Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB239" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC239" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr240">
                                    <td>
                                        <asp:Label ID="lbA240" runat="server" Text="  Al-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB240" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC240" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr241">
                                    <td>
                                        <asp:Label ID="lbA241" runat="server" Text="  Al-Si-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB241" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC241" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr242">
                                    <td>
                                        <asp:Label ID="lbA242" runat="server" Text="  Al-Si-Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB242" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC242" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr243">
                                    <td>
                                        <asp:Label ID="lbA243" runat="server" Text="  Al-Si-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB243" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC243" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr244">
                                    <td>
                                        <asp:Label ID="lbA244" runat="server" Text="  Mg-Si-O-Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB244" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC244" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr245">
                                    <td>
                                        <asp:Label ID="lbA245" runat="server" Text="  Mg-Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB245" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC245" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr246">
                                    <td>
                                        <asp:Label ID="lbA246" runat="server" Text="  Ti"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB246" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC246" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr247">
                                    <td>
                                        <asp:Label ID="lbA247" runat="server" Text="  Nd"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB247" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC247" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr248">
                                    <td>
                                        <asp:Label ID="lbA248" runat="server" Text="  S-Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB248" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC248" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr249">
                                    <td>
                                        <asp:Label ID="lbA249" runat="server" Text="  Zn base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB249" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC249" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr250">
                                    <td>
                                        <asp:Label ID="lbA250" runat="server" Text="  Ca base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB250" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC250" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr251">
                                    <td>
                                        <asp:Label ID="lbA251" runat="server" Text="  Ni base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB251" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC251" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr252">
                                    <td>
                                        <asp:Label ID="lbA252" runat="server" Text="  Cr base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB252" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC252" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr253">
                                    <td>
                                        <asp:Label ID="lbA253" runat="server" Text="  Zr base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB253" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC253" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr254">
                                    <td>
                                        <asp:Label ID="lbA254" runat="server" Text="  Cl base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB254" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC254" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr255">
                                    <td>
                                        <asp:Label ID="lbA255" runat="server" Text="  Na-Cl"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB255" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC255" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr256">
                                    <td>
                                        <asp:Label ID="lbA256" runat="server" Text="  Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB256" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC256" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr257">
                                    <td>
                                        <asp:Label ID="lbA257" runat="server" Text="  Cu-Au"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB257" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC257" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr258">
                                    <td>
                                        <asp:Label ID="lbA258" runat="server" Text="  Ag-S"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB258" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC258" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr259">
                                    <td>
                                        <asp:Label ID="lbA259" runat="server" Text="  Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB259" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC259" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr260">
                                    <td>
                                        <asp:Label ID="lbA260" runat="server" Text="  Ag"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB260" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC260" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr261">
                                    <td>
                                        <asp:Label ID="lbA261" runat="server" Text="  Au"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB261" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC261" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr262">
                                    <td>
                                        <asp:Label ID="lbA262" runat="server" Text="  Cu-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB262" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC262" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr263">
                                    <td>
                                        <asp:Label ID="lbA263" runat="server" Text="  Cu-Zn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB263" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC263" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr264">
                                    <td>
                                        <asp:Label ID="lbA264" runat="server" Text="  Cu-Zn-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB264" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC264" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr265">
                                    <td>
                                        <asp:Label ID="lbA265" runat="server" Text="  Cu-Zn-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB265" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC265" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr266">
                                    <td>
                                        <asp:Label ID="lbA266" runat="server" Text="  Zn-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB266" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC266" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr267">
                                    <td>
                                        <asp:Label ID="lbA267" runat="server" Text="  Pb"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB267" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC267" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr268">
                                    <td>
                                        <asp:Label ID="lbA268" runat="server" Text="  Fe-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB268" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC268" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr269">
                                    <td>
                                        <asp:Label ID="lbA269" runat="server" Text="  Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB269" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC269" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr270">
                                    <td>
                                        <asp:Label ID="lbA270" runat="server" Text="  Al-Si Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB270" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC270" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr271">
                                    <td>
                                        <asp:Label ID="lbA271" runat="server" Text="  F-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB271" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC271" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr272">
                                    <td>
                                        <asp:Label ID="lbA272" runat="server" Text="  Cu-S-Al-O Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB272" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC272" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr273">
                                    <td>
                                        <asp:Label ID="lbA273" runat="server" Text="  Ti-O/Al-Si-Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB273" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC273" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr274">
                                    <td>
                                        <asp:Label ID="lbA274" runat="server" Text="  Cr-Rich (Cr Base + Cr-Mn)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB274" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC274" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr275">
                                    <td>
                                        <asp:Label ID="lbA275" runat="server" Text="Subtotal - Other Particle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB275" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC275" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr276">
                                    <td>
                                        <asp:Label ID="lbA276" runat="server" Text="Grand Total of Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB276" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC276" runat="server" Text=""></asp:Label></td>
                                </tr>

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <h5>Analysis Table on Swage</h5>
                <%-- Analysis Table on Swage --%>
                <div class="row-fluid">
                    <div class="span6 ">
                        <table class="table table-striped table-hover" id="Table2" runat="server">
                            <thead>
                                <tr>
                                    <th>Elemental Composition</th>
                                    <th>Count</th>
                                    <th>Particle/cm2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr runat="server" id="tr289">
                                    <td>
                                        <asp:Label ID="lbA289" runat="server" Text="Hard Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB289" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC289" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr290">
                                    <td>
                                        <asp:Label ID="lbA290" runat="server" Text="  Al-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB290" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC290" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr291">
                                    <td>
                                        <asp:Label ID="lbA291" runat="server" Text="  Al-Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB291" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC291" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr292">
                                    <td>
                                        <asp:Label ID="lbA292" runat="server" Text="  Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB292" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC292" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr293">
                                    <td>
                                        <asp:Label ID="lbA293" runat="server" Text="  Si-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB293" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC293" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr294">
                                    <td>
                                        <asp:Label ID="lbA294" runat="server" Text="  Al-Cu-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB294" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC294" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr295">
                                    <td>
                                        <asp:Label ID="lbA295" runat="server" Text="  Al-Mg-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB295" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC295" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr296">
                                    <td>
                                        <asp:Label ID="lbA296" runat="server" Text="  Al-Si-Cu-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB296" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC296" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr297">
                                    <td>
                                        <asp:Label ID="lbA297" runat="server" Text="  Al-Si-Fe-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB297" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC297" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr298">
                                    <td>
                                        <asp:Label ID="lbA298" runat="server" Text="  Al-Si-Mg-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB298" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC298" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr299">
                                    <td>
                                        <asp:Label ID="lbA299" runat="server" Text="  Al-Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB299" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC299" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr300">
                                    <td>
                                        <asp:Label ID="lbA300" runat="server" Text="  Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB300" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC300" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr301">
                                    <td>
                                        <asp:Label ID="lbA301" runat="server" Text="  Ti-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB301" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC301" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr302">
                                    <td>
                                        <asp:Label ID="lbA302" runat="server" Text="  Ti-B"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB302" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC302" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr303">
                                    <td>
                                        <asp:Label ID="lbA303" runat="server" Text="  Ti-N"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB303" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC303" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr304">
                                    <td>
                                        <asp:Label ID="lbA304" runat="server" Text="  W-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB304" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC304" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr305">
                                    <td>
                                        <asp:Label ID="lbA305" runat="server" Text="  W-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB305" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC305" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr306">
                                    <td>
                                        <asp:Label ID="lbA306" runat="server" Text="  Zr-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB306" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC306" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr307">
                                    <td>
                                        <asp:Label ID="lbA307" runat="server" Text="  Zr-C"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB307" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC307" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr308">
                                    <td>
                                        <asp:Label ID="lbA308" runat="server" Text="  Pb-Zr-Ti-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB308" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC308" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr309">
                                    <td>
                                        <asp:Label ID="lbA309" runat="server" Text="Subtotal - Hard Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB309" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC309" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr310">
                                    <td>
                                        <asp:Label ID="lbA310" runat="server" Text="Magnetic Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB310" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC310" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr311">
                                    <td>
                                        <asp:Label ID="lbA311" runat="server" Text="  Ce-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB311" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC311" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr312">
                                    <td>
                                        <asp:Label ID="lbA312" runat="server" Text="  Fe-Nd"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB312" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC312" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr313">
                                    <td>
                                        <asp:Label ID="lbA313" runat="server" Text="  Fe-Sm"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB313" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC313" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr314">
                                    <td>
                                        <asp:Label ID="lbA314" runat="server" Text="  Fe-Sr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB314" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC314" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr315">
                                    <td>
                                        <asp:Label ID="lbA315" runat="server" Text="  Nd-Pr"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB315" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC315" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr316">
                                    <td>
                                        <asp:Label ID="lbA316" runat="server" Text="  Ni-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB316" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC316" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr317">
                                    <td>
                                        <asp:Label ID="lbA317" runat="server" Text="  Sm-Co"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB317" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC317" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr318">
                                    <td>
                                        <asp:Label ID="lbA318" runat="server" Text="Subtotal - Magnetic Particles Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB318" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC318" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr319">
                                    <td>
                                        <asp:Label ID="lbA319" runat="server" Text="Subtotal- Hard Particles including Magnetic Partilces"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB319" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC319" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr333">
                                    <td>
                                        <asp:Label ID="lbA333" runat="server" Text="Steel Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB333" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC333" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr334">
                                    <td>
                                        <asp:Label ID="lbA334" runat="server" Text="  SS 300- Fe-Cr-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB334" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC334" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr335">
                                    <td>
                                        <asp:Label ID="lbA335" runat="server" Text="  SS 300- Fe-Cr-Ni-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB335" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC335" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr336">
                                    <td>
                                        <asp:Label ID="lbA336" runat="server" Text="  SS 300- Fe-Cr-Ni-Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB336" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC336" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr337">
                                    <td>
                                        <asp:Label ID="lbA337" runat="server" Text="  SS 400- Fe-Cr "></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB337" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC337" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr338">
                                    <td>
                                        <asp:Label ID="lbA338" runat="server" Text="  SS 400- Fe-Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB338" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC338" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr339">
                                    <td>
                                        <asp:Label ID="lbA339" runat="server" Text="  Other Steel - Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB339" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC339" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr340">
                                    <td>
                                        <asp:Label ID="lbA340" runat="server" Text="  Other Steel - Fe-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB340" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC340" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr341">
                                    <td>
                                        <asp:Label ID="lbA341" runat="server" Text="  Other Steel - Fe-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB341" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC341" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr342">
                                    <td>
                                        <asp:Label ID="lbA342" runat="server" Text="  Other Steel - Fe-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB342" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC342" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr343">
                                    <td>
                                        <asp:Label ID="lbA343" runat="server" Text="Subtotal - Steel Particle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB343" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC343" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr344">
                                    <td>
                                        <asp:Label ID="lbA344" runat="server" Text="Other Particle"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB344" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC344" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr345">
                                    <td>
                                        <asp:Label ID="lbA345" runat="server" Text="  No Element"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB345" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC345" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr346">
                                    <td>
                                        <asp:Label ID="lbA346" runat="server" Text="  Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB346" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC346" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr347">
                                    <td>
                                        <asp:Label ID="lbA347" runat="server" Text="  Ni-P"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB347" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC347" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr348">
                                    <td>
                                        <asp:Label ID="lbA348" runat="server" Text="  Sn base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB348" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC348" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr349">
                                    <td>
                                        <asp:Label ID="lbA349" runat="server" Text="  Other"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB349" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC349" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr350">
                                    <td>
                                        <asp:Label ID="lbA350" runat="server" Text="  Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB350" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC350" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr351">
                                    <td>
                                        <asp:Label ID="lbA351" runat="server" Text="  Al-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB351" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC351" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr352">
                                    <td>
                                        <asp:Label ID="lbA352" runat="server" Text="  Al-Ti"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB352" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC352" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr353">
                                    <td>
                                        <asp:Label ID="lbA353" runat="server" Text="  Al-Si (1)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB353" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC353" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr354">
                                    <td>
                                        <asp:Label ID="lbA354" runat="server" Text="  Al-Si (2)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB354" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC354" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr355">
                                    <td>
                                        <asp:Label ID="lbA355" runat="server" Text="  Si"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB355" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC355" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr356">
                                    <td>
                                        <asp:Label ID="lbA356" runat="server" Text="  Al-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB356" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC356" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr357">
                                    <td>
                                        <asp:Label ID="lbA357" runat="server" Text="  Al-Si-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB357" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC357" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr358">
                                    <td>
                                        <asp:Label ID="lbA358" runat="server" Text="  Al-Si-Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB358" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC358" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr359">
                                    <td>
                                        <asp:Label ID="lbA359" runat="server" Text="  Al-Si-Mg"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB359" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC359" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr360">
                                    <td>
                                        <asp:Label ID="lbA360" runat="server" Text="  Mg-Si-O-Al"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB360" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC360" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr361">
                                    <td>
                                        <asp:Label ID="lbA361" runat="server" Text="  Mg-Si-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB361" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC361" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr362">
                                    <td>
                                        <asp:Label ID="lbA362" runat="server" Text="  Ti"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB362" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC362" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr363">
                                    <td>
                                        <asp:Label ID="lbA363" runat="server" Text="  Nd"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB363" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC363" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr364">
                                    <td>
                                        <asp:Label ID="lbA364" runat="server" Text="  S-Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB364" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC364" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr365">
                                    <td>
                                        <asp:Label ID="lbA365" runat="server" Text="  Zn base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB365" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC365" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr366">
                                    <td>
                                        <asp:Label ID="lbA366" runat="server" Text="  Ca base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB366" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC366" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr367">
                                    <td>
                                        <asp:Label ID="lbA367" runat="server" Text="  Ni base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB367" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC367" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr368">
                                    <td>
                                        <asp:Label ID="lbA368" runat="server" Text="  Cr base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB368" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC368" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr369">
                                    <td>
                                        <asp:Label ID="lbA369" runat="server" Text="  Zr base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB369" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC369" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr370">
                                    <td>
                                        <asp:Label ID="lbA370" runat="server" Text="  Cl base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB370" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC370" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr371">
                                    <td>
                                        <asp:Label ID="lbA371" runat="server" Text="  Na-Cl"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB371" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC371" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr372">
                                    <td>
                                        <asp:Label ID="lbA372" runat="server" Text="  Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB372" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC372" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr373">
                                    <td>
                                        <asp:Label ID="lbA373" runat="server" Text="  Cu-Au"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB373" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC373" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr374">
                                    <td>
                                        <asp:Label ID="lbA374" runat="server" Text="  Ag-S"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB374" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC374" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr375">
                                    <td>
                                        <asp:Label ID="lbA375" runat="server" Text="  Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB375" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC375" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr376">
                                    <td>
                                        <asp:Label ID="lbA376" runat="server" Text="  Ag"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB376" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC376" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr377">
                                    <td>
                                        <asp:Label ID="lbA377" runat="server" Text="  Au"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB377" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC377" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr378">
                                    <td>
                                        <asp:Label ID="lbA378" runat="server" Text="  Cu-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB378" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC378" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr379">
                                    <td>
                                        <asp:Label ID="lbA379" runat="server" Text="  Cu-Zn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB379" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC379" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr380">
                                    <td>
                                        <asp:Label ID="lbA380" runat="server" Text="  Cu-Zn-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB380" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC380" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr381">
                                    <td>
                                        <asp:Label ID="lbA381" runat="server" Text="  Cu-Zn-Au-Ni"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB381" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC381" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr382">
                                    <td>
                                        <asp:Label ID="lbA382" runat="server" Text="  Zn-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB382" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC382" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr383">
                                    <td>
                                        <asp:Label ID="lbA383" runat="server" Text="  Pb"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB383" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC383" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr384">
                                    <td>
                                        <asp:Label ID="lbA384" runat="server" Text="  Fe-Cu"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB384" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC384" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr385">
                                    <td>
                                        <asp:Label ID="lbA385" runat="server" Text="  Cr-Mn"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB385" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC385" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr386">
                                    <td>
                                        <asp:Label ID="lbA386" runat="server" Text="  Al-Si Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB386" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC386" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr387">
                                    <td>
                                        <asp:Label ID="lbA387" runat="server" Text="  F-O"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB387" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC387" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr388">
                                    <td>
                                        <asp:Label ID="lbA388" runat="server" Text="  Cu-S-Al-O Base"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB388" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC388" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr389">
                                    <td>
                                        <asp:Label ID="lbA389" runat="server" Text="  Ti-O/Al-Si-Fe"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB389" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC389" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr390">
                                    <td>
                                        <asp:Label ID="lbA390" runat="server" Text="  Cr-Rich (Cr Base + Cr-Mn)"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB390" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC390" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr391">
                                    <td>
                                        <asp:Label ID="lbA391" runat="server" Text="Subtotal - OrtherParticle Only"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB391" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC391" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr runat="server" id="tr392">
                                    <td>
                                        <asp:Label ID="lbA392" runat="server" Text="Grand Total of Particles"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbB392" runat="server" Text=""></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lbC392" runat="server" Text=""></asp:Label></td>
                                </tr>

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

            <div class="well">
                <h3><i class="icon-tasks"></i>Operations (<asp:Label ID="lbJobStatus" runat="server" Text=""></asp:Label>)</h3>
                <asp:Panel ID="pSpecification" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlSpecification">Specification:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlSpecification" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" OnSelectedIndexChanged="ddlSpecification_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>

                    </div>
                    <div class="row-fluid">
                        <div class="span6 ">
                            <label class="control-label" for="ddlComponent">Component:</label>
                            <div class="controls">
                                <asp:DropDownList ID="ddlComponent" runat="server" class="span12 chosen" DataTextField="A" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="ddlComponent_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pStatus" runat="server">
                    <div class="row-fluid">

                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="ddlStatus">
                                    <asp:Label ID="lbStatus" runat="server" Text="Approve Status:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="span6" DataTextField="name" DataValueField="ID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pRemark" runat="server">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="txtRemark">
                                    <asp:Label ID="Label1" runat="server" Text="Remark:"></asp:Label>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtRemark" name="txtRemark" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pDisapprove" runat="server">
                    <div class="row-fluid">
                        <div class="span6 ">
                            <div class="control-group">
                                <asp:Label ID="lbAssignTo" runat="server" Text="Assign To:" class="control-label"></asp:Label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlAssignTo" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="pDownload" runat="server">
                    <ul>
                        <li>Link for download Cover Page
                            <asp:LinkButton ID="lbDownload" runat="server" OnClick="lbDownload_Click">
                                <i class="icon-download-alt"></i>
                                <asp:Label ID="lbDownloadName" runat="server" Text="Label"></asp:Label>
                            </asp:LinkButton>
                        </li>
                    </ul>
                </asp:Panel>
                <asp:Panel ID="pUploadfile" runat="server">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <label class="control-label" for="ddlSpecification">Uplod file:</label>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Add files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" />
                            </span>
                        </div>
                    </div>
                    <ul>
                        <li>The maximum file size for uploads in this demo is <strong>5 MB</strong> (default file size is unlimited).</li>
                        <li>Only word or pdf files (<strong>DOC, DOCX, PDF</strong>) are allowed in this demo (by default there is no file type restriction).</li>
                    </ul>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    <br />
                </asp:Panel>
            </div>
            <div class="form-actions clearfix">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" />
            </div>
            <!-- END PAGE CONTENT-->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="lbDownload" />
        </Triggers>
    </asp:UpdatePanel>
</form>
