﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Seagate_LPC_Component_1_4_Rev_BE_WorkingSheet.ascx.cs" Inherits="ALS.ALSI.Web.view.template.Seagate_LPC_Component_1_4_Rev_BE_WorkingSheet" %>

<form runat="server" id="Form1" method="POST" enctype="multipart/form-data">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="alert alert-error hide">
                <button class="close" data-dismiss="alert"></button>
                You have some form errors. Please check below.
            </div>
            <div class="alert alert-success hide">
                <button class="close" data-dismiss="alert"></button>
                Your form validation is successful!
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="row-fluid fileupload-buttonbar">
                        <div class="span6">
                            <%--<label class="control-label" for="ddlSpecification">Uplod file:</label>--%>
                            <span class="btn green fileinput-button">
                                <i class="icon-plus icon-white"></i>
                                <span>Choose DSH files...</span>
                                <asp:FileUpload ID="btnUpload" runat="server" AllowMultiple="True" />
                            </span>

                        </div>
                    </div>
                    <div class="row-fluid">
                        <ul>
                            <li>The maximum file size for uploads is <strong>5 MB</strong> (default file size is unlimited).</li>
                            <li>Only text files (<strong>txt</strong>) are allowed.</li>
                        </ul>
                        <br />
                        <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                        <br />

                    </div>
                    <br />
                    <br />
                    <asp:CheckBox ID="cbLPCType68" runat="server" Checked="true" AutoPostBack="true" OnCheckedChanged="cbLPCType68_CheckedChanged" />LPC 68 KHz
                    <asp:CheckBox ID="cbLPCType132" runat="server" AutoPostBack="true" OnCheckedChanged="cbLPCType132_CheckedChanged" />LPC 132 KHZ
                                            
                </div>
            </div>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span10">
                    <div class="tabbable tabbable-custom boxless">
                        <ul class="nav nav-tabs">
                            <asp:Button ID="btnUsLPC03" runat="server" Text="US-LPC(0.3)" CssClass="btn blue" OnClick="btnUsLPC_Click" />
                            <asp:Button ID="btnUsLPC06" runat="server" Text="US-LPC(0.6)" CssClass="btn blue" OnClick="btnUsLPC_Click" />
                        </ul>
                        <div class="tab-content">
                            <%--US-LPC(0.3)--%>
                            <asp:Panel ID="pUS_LPC03" runat="server">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="row-fluid">
                                        <div class="span8">
                                            <h6>Results:</h6>
                                            <div class="row-fluid">
                                                <div class="span8">
                                                    <table class="table table-striped table-hover small" id="tb1" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th colspan="6">No. of Particles ≥ 0.3μm (Counts/ml)</th>
                                                            </tr>
                                                            <tr>
                                                                <th>RUN</th>
                                                                <th>Blank 1</th>
                                                                <th>Sample 1</th>
                                                                <th>Blank 2</th>
                                                                <th>Sample 2</th>
                                                                <th>Blank 3</th>
                                                                <th>Sample 3</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr runat="server" id="tr1">
                                                                <td>1</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr2">
                                                                <td>2</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr3">
                                                                <td>3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr4">
                                                                <td>4</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr5">
                                                                <td>Average of last 3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_B18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_C18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_D18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_E18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_F18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC03_G18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr6">
                                                                <td>Extraction Vol. (ml)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B20" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr7">
                                                                <td>Surface Area (cm2)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B21" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr8">
                                                                <td>No. of Parts Used</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B22" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr9">
                                                                <td>Sample</td>
                                                                <td colspan="2">1</td>
                                                                <td colspan="2">2</td>
                                                                <td colspan="2">3</td>
                                                            </tr>
                                                            <tr runat="server" id="tr10">
                                                                <td>No. of Particles ≥ 0.3µm<br />
                                                                    (Counts/cm2)</td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC03_B25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC03_D25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC03_F25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr11">
                                                                <td>Average</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC03_B26" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--US-LPC(0.6)--%>
                            <asp:Panel ID="pUS_LPC06" runat="server">
                                <div class="tab-pane active" id="Div1">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <h6>Results:</h6>
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <table class="table table-striped table-hover small" id="Table1" runat="server">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th colspan="6">No. of Particles ≥ 0.6μm (Counts/ml)</th>
                                                            </tr>
                                                            <tr>
                                                                <th>RUN</th>
                                                                <th>Blank 1</th>
                                                                <th>Sample 1</th>
                                                                <th>Blank 2</th>
                                                                <th>Sample 2</th>
                                                                <th>Blank 3</th>
                                                                <th>Sample 3</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr runat="server" id="tr12">
                                                                <td>1</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G14" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr13">
                                                                <td>2</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G15" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr14">
                                                                <td>3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G16" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr15">
                                                                <td>4</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G17" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr16">
                                                                <td>Average of last 3</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_B18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_C18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_D18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_E18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_F18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_UsLPC06_G18" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr17">
                                                                <td>Extraction Vol. (ml)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B20" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr18">
                                                                <td>Surface Area (cm2)</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B21" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr19">
                                                                <td>No. of Parts Used</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B22" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr20">
                                                                <td>Sample</td>
                                                                <td colspan="2">1</td>
                                                                <td colspan="2">2</td>
                                                                <td colspan="2">3</td>
                                                            </tr>
                                                            <tr runat="server" id="tr21">
                                                                <td>No. of Particles ≥ 0.6µm<br />
                                                                    (Counts/cm2)</td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC06_B25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC06_D25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="txt_UsLPC06_F25" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                            <tr runat="server" id="tr22">
                                                                <td>Average</td>
                                                                <td colspan="6">
                                                                    <asp:TextBox ID="txt_UsLPC06_B26" runat="server" CssClass="m-wrap small"></asp:TextBox></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions clearfix">
                <asp:Button ID="btnLoadFile" runat="server" Text="Load" CssClass="btn blue" OnClick="btnLoadFile_Click" />
                <asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="btn green" OnClick="btnCalculate_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn green" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="disable btn" OnClick="btnCancel_Click" />
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnLoadFile" />
            <asp:PostBackTrigger ControlID="btnSubmit" />
        </Triggers>
    </asp:UpdatePanel>
</form>

