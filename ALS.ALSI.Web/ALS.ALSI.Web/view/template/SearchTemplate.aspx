﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SearchTemplate.aspx.cs" Inherits="ALS.ALSI.Web.view.template.SearchTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form1" method="post" runat="server" class="form-horizontal">
        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            You have some form errors. Please check below.
        </div>
        <div class="alert alert-success hide">
            <button class="close" data-dismiss="alert"></button>
            Your form validation is successful!
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet">
                    <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Search Condition</h4>
                        <div class="actions">
                            <div class="btn-group">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn mini yellow" OnClick="btnSearch_Click1"><i class="icon-search"></i> Search</asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" class="btn mini black" OnClick="btnCancel_Click"><i class="icon-refresh"></i> Cancel</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <%--                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtId">ID:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtId" runat="server" class="m-wrap span6"></asp:TextBox>
                                </div>
                            </div>

                        </div>--%>
                        <div class="span6 ">
                            <div class="control-group">
                                <label class="control-label" for="txtName">NAME:</label>
                                <div class="controls">
                                    <asp:TextBox ID="txtName" runat="server" class="m-wrap span6"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <h4><i class="icon-bar-chart"></i>Search Result</h4>
                        <%--                        <div class="actions">
                            <div class="btn-group">
                                <asp:LinkButton ID="btnAdd" runat="server" class="btn mini blue" OnClick="lbAdd_Click"><i class="icon-pencil"></i> Add</asp:LinkButton>
                            </div>
                        </div>--%>
                    </div>
                    <div class="portlet-body">
                        <asp:Label ID="lbTotalRecords" runat="server" Text=""></asp:Label>
                        <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            CssClass="table table-striped table-hover table-bordered" ShowHeaderWhenEmpty="True" DataKeyNames="id" OnRowCommand="gvResult_RowCommand" OnRowDeleting="gvResult_RowDeleting" OnPageIndexChanging="gvResult_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="ID" DataField="ID" ItemStyle-HorizontalAlign="Left" SortExpression="ID" />
                                <asp:BoundField HeaderText="Name" DataField="name" ItemStyle-HorizontalAlign="Left" SortExpression="name" />
                                <asp:BoundField HeaderText="Source file" DataField="path_source_file" ItemStyle-HorizontalAlign="Left" SortExpression="path_source_file" />
                                <asp:BoundField HeaderText="Url" DataField="path_url" ItemStyle-HorizontalAlign="Left" SortExpression="path_url" />
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" ToolTip="Upload" CommandName="Edit" CommandArgument='<%# Eval("ID")%>'><i class="icon-upload"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <div class="pagination">
                                    <ul>
                                        <li>
                                            <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                        </li>
                                        <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                        <li>
                                            <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                <div class="data-not-found">
                                    <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
