﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Web.view.request;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

namespace ALS.ALSI.Web.view.template
{
    public partial class Seagate_DHS_Component_Rev_BE_1_5_WorkingSheet : System.Web.UI.UserControl
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Seagate_DHS_Component_Rev_BE_1_5_WorkingSheet));

        #region "Property"

        public List<tb_cas> tbCas
        {
            get { return (List<tb_cas>)Session[GetType().Name + "tbCas"]; }
            set { Session[GetType().Name + "tbCas"] = value; }
        }

        public List<template_16_coverpage> coverpages
        {
            get { return (List<template_16_coverpage>)Session[GetType().Name + "coverpages"]; }
            set { Session[GetType().Name + "coverpages"] = value; }
        }

        public List<tb_m_library> libs
        {
            get { return (List<tb_m_library>)Session[GetType().Name + "libs"]; }
            set { Session[GetType().Name + "libs"] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int SampleID
        {
            get { return (int)Session[GetType().Name + "SampleID"]; }
            set { Session[GetType().Name + "SampleID"] = value; }
        }

        public int HUMValue
        {
            get { return (int)Session[GetType().Name + "HUMValue"]; }
            set { Session[GetType().Name + "HUMValue"] = value; }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "tbCas");
            Session.Remove(GetType().Name + "coverpages");
            Session.Remove(GetType().Name + "libs");
            Session.Remove(GetType().Name + Constants.PREVIOUS_PATH);
            Session.Remove(GetType().Name + "SampleID");
        }

        private void initialPage()
        {

            this.coverpages = template_16_coverpage.FindAllBySampleID(this.SampleID);
            this.libs = new tb_m_library().SelectBySpecificationID(1);//Select WD
            this.tbCas = tb_cas.FindAllBySampleID(this.SampleID);

            if (this.coverpages != null && this.coverpages.Count > 0)
            {
                //initial set edit
                foreach (template_16_coverpage _cas in this.coverpages)
                {
                    _cas.RowState = CommandNameEnum.Edit;
                }

                gvCoverPages.DataSource = this.coverpages;
                gvCoverPages.DataBind();
            }

            if (this.tbCas != null && this.tbCas.Count > 0)
            {
                //initial set edit
                foreach (tb_cas _cas in this.tbCas)
                {
                    _cas.RowState = CommandNameEnum.Add;
                }
                HUMValue = Convert.ToInt32(this.tbCas[0].hump);// 
                txtHexadecane.Text = this.tbCas[0].divisor;
                CalculateCas();
                
     
                //initial component
                btnDHS.CssClass = "btn blue";
                btnCoverPage.CssClass = "btn green";

                pDSH.Visible = true;
                btnCoverPage.Visible = true;
                pCoverpage.Visible = false;
                btnSubmit.Enabled = true;
            }
            else
            {
                //initial component
                btnDHS.CssClass = "btn green";
                btnCoverPage.CssClass = "btn blue";

                pDSH.Visible = true;
                btnCoverPage.Visible = false;
                pCoverpage.Visible = false;
                btnSubmit.Enabled = false;
            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchJobRequest prvPage = Page.PreviousPage as SearchJobRequest;
            this.SampleID = (prvPage == null) ? this.SampleID : prvPage.SampleID;
            this.PreviousPath = Constants.LINK_SEARCH_JOB_REQUEST;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            job_sample sample = new job_sample().SelectByID(this.SampleID);
            sample.job_status = Convert.ToInt32(StatusEnum.SR_CHEMIST_CHECKING);
            sample.Update();

            //CAS#
            tb_cas.DeleteBySampleID(this.SampleID);
            foreach (tb_cas _cas in this.tbCas)
            {
                _cas.hump = this.HUMValue.ToString();
                _cas.divisor = txtHexadecane.Text;
            }
            tb_cas.InsertList(this.tbCas.FindAll(x => x.ID < 10000 && x.ID > 0));
            template_16_coverpage.InsertList(this.coverpages);
            //Commit
            GeneralManager.Commit();

            Response.Redirect(this.PreviousPath);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            removeSession();
            Response.Redirect(this.PreviousPath);
        }

        protected void btnLoadFile_Click(object sender, EventArgs e)
        {

            List<DhsFile_a> listA = new List<DhsFile_a>();
            List<tb_cas> listL = new List<tb_cas>();
            String error = validateDSHFile(btnUpload.PostedFiles);

            if (String.IsNullOrEmpty(error))
            {
                #region "LOAD DHS"
                String yyyMMdd = DateTime.Now.ToString("yyyyMMdd");
                for (int i = 0; i < btnUpload.PostedFiles.Count; i++)
                {
                    HttpPostedFile _postedFile = btnUpload.PostedFiles[i];
                    try
                    {
                        if (_postedFile.ContentLength > 0)
                        {
                            String sourceFileFolder = String.Format("{0}{1}{2}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd);
                            String savefilePath = String.Format("{0}{1}{2}/{3}", Configuration.PATH_SOURCE_DRIVE, Configuration.PATH_SOURCE, yyyMMdd, _postedFile.FileName);
                            if (!Directory.Exists(sourceFileFolder))
                            {
                                Directory.CreateDirectory(sourceFileFolder);
                            }

                            _postedFile.SaveAs(savefilePath);

                            using (StreamReader fs = new StreamReader(savefilePath, Encoding.UTF8))
                            {

                                Boolean bPause = false;

                                int index = 1;
                                int pkIndex = 1;

                                String libraryID = String.Empty;

                                String line = String.Empty;

                                tb_cas fileL = new tb_cas();
                                while ((line = fs.ReadLine()) != null)
                                {
                                    #region ".a"
                                    if (Path.GetFileNameWithoutExtension(savefilePath.ToLower()).EndsWith("a"))
                                    {
                                        if (index > 21)
                                        {

                                            if (String.IsNullOrEmpty(line.Trim())) continue;//skip null row
                                            if (line.Trim().StartsWith(Constants.SUM_OF_CORRECTED_AREAS)) break;

                                            DhsFile_a fileA = new DhsFile_a();
                                            fileA.Peak = line.Substring(0, 3).Trim();
                                            fileA.RTmin = line.Substring(4, 11).Trim();
                                            fileA.FirstScan = line.Substring(12, 6).Trim();
                                            fileA.MaxScan = line.Substring(18, 5).Trim();
                                            fileA.LastScan = line.Substring(23, 5).Trim();
                                            fileA.PKTY = line.Substring(28, 5).Trim();
                                            fileA.PeakHeight = line.Substring(33, 8).Trim();
                                            fileA.CorrArea = line.Substring(41, 10).Trim();
                                            fileA.CorrMax = line.Substring(51, 8).Trim();
                                            fileA.OfTotal = line.Substring(59, 8).Trim();
                                            listA.Add(fileA);
                                            Console.WriteLine(line);
                                        }
                                    }
                                    #endregion

                                    #region ".l"
                                    if (Path.GetFileNameWithoutExtension(savefilePath.ToLower()).EndsWith("l"))
                                    {
                                        if (index > 18)
                                        {

                                            if (line.Trim().StartsWith(Constants.WD_M)) break;//END OF FILE
                                            if (String.IsNullOrEmpty(line.Trim()))
                                            {
                                                if (String.IsNullOrEmpty(fileL.ref_)) continue;
                                                bPause = false;
                                                pkIndex++;
                                                listL.Add(fileL);
                                                fileL = new tb_cas();
                                                continue;//Skip null row.
                                            }

                                            String _pk = line.Substring(0, 3).Trim();
                                            if (!String.IsNullOrEmpty(_pk))
                                            {//Skip บันทักแรก
                                                fileL.pk = _pk;
                                                fileL.rt = line.Substring(4, 8).Trim();
                                                fileL.area = line.Substring(12, 6).Trim();
                                                continue;
                                            };
                                            if (!bPause)
                                            {
                                                String _ref = line.Substring(52, 7).Trim();
                                                if (!String.IsNullOrEmpty(_ref))
                                                {
                                                    fileL.library_id = line.Substring(17, 34).Trim();
                                                    fileL.ref_ = line.Substring(52, 7).Trim();
                                                    fileL.cas = line.Substring(59, 12).Trim();
                                                    fileL.qual = line.Substring(70, 3).Trim();
                                                    bPause = true;
                                                }
                                            }
                                            if (bPause) continue;//ถ้าเจอ Record แรกแล้วของ REF แล้วไม่สนบันทัดอื่นจนกว่า จะเปลี่ยน PK


                                        }

                                        logger.Debug(line);
                                    }
                                    #endregion

                                    #region ".h"
                                    if (Path.GetFileNameWithoutExtension(savefilePath.ToLower()).EndsWith("h"))
                                    {
                                        if (line.Trim().StartsWith(Constants.SUM_OF_CORRECTED_AREAS))
                                        {
                                            HUMValue = Convert.ToInt32(line.Split(Constants.CHAR_COLON)[1].Trim());
                                        }
                                    }
                                    #endregion

                                    index++;
                                }

                                Console.WriteLine();

                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        logger.Error(Ex.Message);
                        Console.WriteLine();
                    }
                }
                #endregion
            }

            if (listA.Count != listL.Count)
            {
                error = "Number of pk in (*-a.txt)  is not equal (*-l.txt) ";
            }

            if (String.IsNullOrEmpty(error))
            {
                List<tb_m_library> listLib = new tb_m_library().SelectBySpecificationID(1);
                if (listLib.Count > 0)
                {
                    foreach (tb_cas _cas in listL)
                    {
                        _cas.sample_id = this.SampleID;
                        _cas.ID = Convert.ToInt32(_cas.pk);

                        DhsFile_a _a = listA.Find(x => x.Peak == _cas.pk);
                        tb_m_library lib = listLib.Find(x => x.cas == _cas.cas);
                        if (lib != null)
                        {
                            _cas.library_id = lib.libraryID;
                            _cas.classification = lib.classification;
                        }
                        else
                        {
                            tb_m_library xxxxx = listLib.Find(x => x.cas == "000098-52-2");
                            _cas.library_id = xxxxx.libraryID;
                            _cas.classification = xxxxx.classification;
                            //test

                            logger.Debug("Not found cas#" + _cas.cas);
                        }
                        _cas.area = _a.CorrArea;
                        _cas.amount = String.Empty;
                        _cas.RowState = CommandNameEnum.Add;
                    }
                }


                this.tbCas = listL;
                CalculateCas();

                //initial component
                btnDHS.CssClass = "btn green";
                btnCoverPage.CssClass = "btn blue";

                pDSH.Visible = true;
                btnCoverPage.Visible = true;
                pCoverpage.Visible = false;
                btnSubmit.Enabled = true;

            }
            else
            {
                lbMessage.Text = error;
                lbMessage.Attributes["class"] = "alert alert-error";
            }
        }

        protected void btnCoverPage_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.Text)
            {
                case "Cover Page":
                    btnCoverPage.CssClass = "btn green";
                    btnDHS.CssClass = "btn blue";
                    pCoverpage.Visible = true;
                    pDSH.Visible = false;
                    break;
                case "DHS":
                    btnCoverPage.CssClass = "btn blue";
                    btnDHS.CssClass = "btn green";
                    pCoverpage.Visible = false;
                    pDSH.Visible = true;
                    break;
            }

            //ReFresh Clas# value
            Boolean isComplateData = true;
            foreach (tb_cas _cas in this.tbCas)
            {
                if (_cas.classification == null)
                {
                    isComplateData = false;
                    break;
                }
            }

            if (isComplateData)
            {
                List<template_16_coverpage> listCoverPages = new List<template_16_coverpage>();
                foreach (template_16_coverpage cover in this.coverpages)
                {
                    listCoverPages.Add(cover);
                    List<tb_cas> childs = this.tbCas.FindAll(x => cover.name.StartsWith(x.classification) && !String.IsNullOrEmpty(x.classification)).ToList();
                    if (childs.Count > 0)
                    {
                        listCoverPages[listCoverPages.Count - 1].result = childs[childs.Count - 1].amount;
                    }
                    else
                    {
                        cover.result = Constants.GetEnumDescription(ResultEnum.NOT_DETECTED);
                    }
                    cover.RowState = CommandNameEnum.Edit;
                }

                gvCoverPages.DataSource = listCoverPages;
                gvCoverPages.DataBind();
            }
        }

        protected void ddlLibrary_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow grow = (GridViewRow)((Control)sender).NamingContainer;
            DropDownList ddlLibrary = (DropDownList)grow.FindControl("ddlLibrary");
            Literal litClass = (Literal)grow.FindControl("litClass");
            Literal litCAS = (Literal)grow.FindControl("litCAS");

            if (!String.IsNullOrEmpty(ddlLibrary.SelectedValue))
            {
                tb_m_library lib = this.libs.Find(x => x.id == Convert.ToInt32(ddlLibrary.SelectedValue));
                if (lib != null)
                {
                    litClass.Text = lib.classification;
                    litCAS.Text = lib.cas;
                }
            }

        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow grow = (GridViewRow)((Control)sender).NamingContainer;
            HiddenField hdfID = (HiddenField)grow.FindControl("hdfID");
            DropDownList ddlStatus = (DropDownList)grow.FindControl("ddlStatus");

            tb_cas tmp = this.tbCas.Find(x => x.ID == Convert.ToInt32(hdfID.Value));
            //tmp.rowStatus = (DHSGridViewRowActionEnum)Convert.ToInt32(ddlStatus.SelectedValue);
            CalculateCas();
        }

        #region "COVERPAGES GRID."
        protected void gvCoverPages_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label _lbAnalytes = (e.Row.FindControl("lbAnalytes") as Label);
                if (_lbAnalytes != null)
                {
                    if (_lbAnalytes.Text.StartsWith(Constants.CHAR_DASH))
                    {
                        _lbAnalytes.Attributes.CssStyle.Add("margin-left", "15px");
                    }
                }
            }

        }
        #endregion

        #region "DHS GRID."

        protected void gvResult_RowEditing(object sender, GridViewEditEventArgs e)
        {

            gvResult.EditIndex = e.NewEditIndex;
            CalculateCas();
            DropDownList _ddlLibrary = (DropDownList)gvResult.Rows[e.NewEditIndex].FindControl("ddlLibrary");

            _ddlLibrary.DataSource = this.libs;
            _ddlLibrary.DataValueField = "id";
            _ddlLibrary.DataTextField = "SelectedText";
            _ddlLibrary.DataBind();
        }

        protected void gvResult_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvResult.EditIndex = -1;
            gvResult.DataSource = this.tbCas;
            gvResult.DataBind();
        }

        protected void gvResult_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            int _id = Convert.ToInt32(gvResult.DataKeys[e.RowIndex].Values[0].ToString());
            TextBox _txtArea = (TextBox)gvResult.Rows[e.RowIndex].FindControl("txtArea");
            Literal _litCAS = (Literal)gvResult.Rows[e.RowIndex].FindControl("litCAS");
            Literal _litClass = (Literal)gvResult.Rows[e.RowIndex].FindControl("litClass");

            tb_cas _cas = this.tbCas.Find(x => x.ID == _id);
            if (_cas != null)
            {
                _cas.area = _txtArea.Text;
                _cas.cas = _litCAS.Text;
                _cas.classification = _litClass.Text;
            }

            gvResult.EditIndex = -1;
            CalculateCas();

        }

        protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int _id = Convert.ToInt32(gvResult.DataKeys[e.Row.RowIndex].Values[0].ToString());
                DropDownList _ddlStatus = (e.Row.FindControl("ddlStatus") as DropDownList);
                Literal _litRT = (e.Row.FindControl("litRT") as Literal);
                Literal _litAmount = (e.Row.FindControl("litAmount") as Literal);
                LinkButton _btnEdit = (LinkButton)e.Row.FindControl("btnEdit");
                LinkButton _btnDelete = (LinkButton)e.Row.FindControl("btnDelete");
                LinkButton _btnReuse = (LinkButton)e.Row.FindControl("btnReuse");

                tb_cas _cas = this.tbCas.Find(x => x.ID == _id);
                if (_cas != null)
                {
                    _litAmount.Text = _cas.amount;
                    switch (_cas.RowState)
                    {
                        case CommandNameEnum.Add:
                            if (_btnEdit != null && _btnDelete != null && _btnReuse != null)
                            {
                                _btnEdit.Visible = true;
                                _btnReuse.Visible = false;
                                _btnDelete.Visible = true;
                            }
                            e.Row.ForeColor = System.Drawing.Color.Black;
                            break;
                        case CommandNameEnum.Delete:
                            if (_btnEdit != null && _btnDelete != null && _btnReuse != null)
                            {
                                _btnEdit.Visible = false;
                                _btnReuse.Visible = true;
                                _btnDelete.Visible = false;
                            }
                            e.Row.ForeColor = System.Drawing.Color.Red;
                            break;
                        default:
                            if (_btnEdit != null && _btnDelete != null && _btnReuse != null)
                            {
                                _btnEdit.Visible = true;
                                _btnReuse.Visible = false;
                                _btnDelete.Visible = true;
                            }
                            break;
                    }
                }

                if (String.IsNullOrEmpty(_litRT.Text))
                {
                    e.Row.ForeColor = System.Drawing.Color.Blue;
                    e.Row.Font.Bold = true;
                    if (_btnEdit != null && _btnDelete != null && _btnReuse != null)
                    {
                        _btnEdit.Visible = false;
                        _btnReuse.Visible = false;
                        _btnDelete.Visible = false;
                    }
                }
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {

            }
        }

        protected void gvResult_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int PKID = int.Parse(e.Keys[0].ToString().Split(Constants.CHAR_COMMA)[0]);

            tb_cas _cas = this.tbCas.Find(x => x.ID == PKID);
            if (_cas != null)
            {
                _cas.RowState = CommandNameEnum.Delete;

                gvResult.DataSource = this.tbCas;
                gvResult.DataBind();
            }
        }

        protected void gvResult_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            CommandNameEnum cmd = (CommandNameEnum)Enum.Parse(typeof(CommandNameEnum), e.CommandName, true);
            switch (cmd)
            {
                case CommandNameEnum.ReUse:
                    int PKID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);


                    tb_cas _cas = this.tbCas.Find(x => x.ID == PKID);
                    if (_cas != null)
                    {
                        _cas.RowState = CommandNameEnum.Add;

                        gvResult.DataSource = this.tbCas;
                        gvResult.DataBind();
                    }
                    break;
            }
        }
        #endregion

        #region "Custom method"

        private String validateDSHFile(IList<HttpPostedFile> _files)
        {
            Boolean isFound_h = false;
            Boolean isFound_l = false;
            Boolean isFound_a = false;
            Boolean isFoundWrongExtension = false;

            String result = String.Empty;

            String[] files = new String[_files.Count];
            if (files.Length == 3)
            {
                for (int i = 0; i < _files.Count; i++)
                {
                    files[i] = _files[i].FileName;
                    if (!Path.GetExtension(_files[i].FileName).Trim().Equals(".txt"))
                    {
                        isFoundWrongExtension = true;
                        break;
                    }
                }
                if (!isFoundWrongExtension)
                {
                    //Find **-h.txt
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).EndsWith("h"))
                        {
                            isFound_h = true;
                            break;
                        }
                    }

                    //Find **-l.txt
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).EndsWith("l"))
                        {
                            isFound_l = true;
                            break;
                        }
                    }
                    //Find **-a.txt
                    foreach (String file in files)
                    {
                        if (Path.GetFileNameWithoutExtension(file).EndsWith("a"))
                        {
                            isFound_a = true;
                            break;
                        }
                    }
                    result = (!isFound_h) ? result += "File not found *-h.txt." : (!isFound_l) ? result += "File not found *-l.txt." : (!isFound_a) ? result += "File not found *-a.txt." : String.Empty;
                }
                else
                {
                    result = "File extension must be *.txt";
                }
            }
            else
            {
                result = "You must to select 3 files for upload.";
            }
            return result;
        }

        private void CalculateCas()
        {
            this.tbCas = this.tbCas.OrderBy(x => x.classification).ToList();

            Boolean isBeginOrder = true;
            Decimal totalArea = 0;
            Decimal grandTotal = 0;
            String groupName = String.Empty;
            int index = 0;

            List<tb_cas> listCas = new List<tb_cas>();
            foreach (tb_cas _cas in this.tbCas.FindAll(x => x.ID < 10000 && x.ID > 0 && x.RowState != CommandNameEnum.Delete))
            {
                if (index == 0)
                {
                    groupName = _cas.classification;
                }

                if (groupName != null)
                {
                    if (!groupName.Equals(_cas.classification))
                    {
                        tb_cas tmp = new tb_cas();
                        tmp.ID = (10000 + _cas.ID);
                        tmp.sample_id = this.SampleID;
                        tmp.pk = string.Empty;
                        tmp.rt = string.Empty;
                        tmp.library_id = String.Empty;
                        tmp.classification = groupName;
                        tmp.cas = String.Empty;
                        tmp.qual = String.Empty;
                        tmp.area = totalArea.ToString();
                        tmp.amount = string.Empty;
                        listCas.Add(tmp);

                        groupName = _cas.classification;
                        grandTotal += totalArea;
                        totalArea = 0;
                    }
                }
                else
                {
                    isBeginOrder = false;
                }
                totalArea += Convert.ToDecimal(_cas.area);
                listCas.Add(_cas);
                index++;
            }

            //ADD LAST TOTAL
            tb_cas tmp1 = new tb_cas();
            tmp1.ID = (10000 + 999);
            tmp1.sample_id = this.SampleID;
            tmp1.pk = string.Empty;
            tmp1.rt = string.Empty;
            tmp1.library_id = String.Empty;
            tmp1.classification = groupName;
            tmp1.cas = String.Empty;
            tmp1.qual = String.Empty;
            tmp1.area = totalArea.ToString();
            tmp1.amount = String.Empty;
            listCas.Add(tmp1);
            //Add Empty Row to last
            listCas = listCas.OrderBy(x => x.classification).ToList();
            if (isBeginOrder)
            {
                //ADD HUM
                tmp1 = new tb_cas();
                tmp1.ID = -1;
                tmp1.sample_id = this.SampleID;
                tmp1.pk = string.Empty;
                tmp1.rt = "5.229-19.922";
                tmp1.library_id = "Hydrocarbon Hump(Subtracted)";
                tmp1.classification = "Hydrocarbon Hump";
                tmp1.cas = String.Empty;
                tmp1.qual = String.Empty;
                tmp1.area = HUMValue.ToString();
                tmp1.amount = String.Empty;
                listCas.Add(tmp1);
                //Add HUM Total
                tmp1 = new tb_cas();
                tmp1.ID = -1;//HUMID
                tmp1.sample_id = this.SampleID;
                tmp1.pk = string.Empty;
                tmp1.rt = string.Empty;
                tmp1.library_id = String.Empty;
                tmp1.classification = "Hydrocarbon Hump";
                tmp1.cas = String.Empty;
                tmp1.qual = String.Empty;
                tmp1.area = HUMValue.ToString();
                tmp1.amount = String.Empty;
                listCas.Add(tmp1);

                //Add Grand Total (Total Outgassing)
                tmp1 = new tb_cas();
                tmp1.ID = -2;//HUMID
                tmp1.sample_id = this.SampleID;
                tmp1.pk = string.Empty;
                tmp1.rt = string.Empty;
                tmp1.library_id = "";
                tmp1.classification = "Total Outgassing";
                tmp1.cas = String.Empty;
                tmp1.qual = String.Empty;
                tmp1.area = grandTotal.ToString();
                tmp1.amount = String.Empty;
                listCas.Add(tmp1);
                this.tbCas = listCas;
            }
            //Calculate amount.
            foreach (tb_cas _c in this.tbCas)
            {
                if (!txtHexadecane.Text.Equals("0") && !String.IsNullOrEmpty(txtHexadecane.Text))
                {
                    _c.amount = String.Format("{0:N2}", (Convert.ToDecimal(_c.area) / Convert.ToDecimal(txtHexadecane.Text) / Convert.ToDecimal(txtSampleSize.Text)) * Convert.ToDecimal(ddlUnit.SelectedValue));
                }
                else
                {
                    _c.amount = String.Empty;
                }
            }
            gvResult.DataSource = this.tbCas;
            gvResult.DataBind();
        }
        #endregion

    }
}