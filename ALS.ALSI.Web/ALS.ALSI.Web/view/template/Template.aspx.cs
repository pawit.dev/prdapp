﻿using ALS.ALSI.Biz;
using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;

namespace ALS.ALSI.Web.view.template
{
    public partial class Template : System.Web.UI.Page
    {

        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Template));

        #region "Property"
        public user_login userLogin
        {
            get { return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null); }
        }

        protected String Message
        {
            get { return (String)Session[GetType().Name + "Message"]; }
            set { Session[GetType().Name + "Message"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public string PreviousPath
        {
            get { return (string)ViewState[GetType().Name + Constants.PREVIOUS_PATH]; }
            set { ViewState[GetType().Name + Constants.PREVIOUS_PATH] = value; }
        }

        public int PKID
        {
            get { return (int)Session[GetType().Name + "PKID"]; }
            set { Session[GetType().Name + "PKID"] = value; }
        }

        public m_template obj
        {
            get
            {
                m_template tmp = new m_template();
                tmp.ID = PKID;
                tmp.name = txtName.Text;
                tmp.path_source_file = txtPathSourceFile.Text;
                tmp.path_url = txtPathUrl.Text;
                tmp.status = "A";
                return tmp;
            }
        }

        private void initialPage()
        {
            lbCommandName.Text = CommandName.ToString();

            switch (CommandName)
            {
                case CommandNameEnum.Add:
                    txtName.ReadOnly = true;
                    txtPathSourceFile.ReadOnly = true;
                    txtPathUrl.ReadOnly = true;
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;
                    break;
                case CommandNameEnum.Edit:
                    fillinScreen();

                    txtName.ReadOnly = false;
                    txtPathSourceFile.ReadOnly = true;
                    txtPathUrl.ReadOnly = true;
                    btnSave.Enabled = true;
                    btnCancel.Enabled = true;


                    break;
                case CommandNameEnum.View:
                    fillinScreen();

                    txtName.ReadOnly = false;
                    txtPathSourceFile.ReadOnly = false;
                    txtPathUrl.ReadOnly = false;
                    btnSave.Enabled = false;
                    btnCancel.Enabled = true;

                    break;
            }
        }

        private void fillinScreen()
        {
            m_template tem = new m_template().SelectByID(this.PKID);
            txtName.Text = tem.name;
            txtPathSourceFile.Text = tem.path_source_file;
            txtPathUrl.Text = tem.path_url;
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "PKID");
            Session.Remove(GetType().Name + Constants.PREVIOUS_PATH);
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchTemplate prvPage = Page.PreviousPage as SearchTemplate;
            this.CommandName = (prvPage == null) ? this.CommandName : prvPage.CommandName;
            this.PKID = (prvPage == null) ? this.PKID : prvPage.PKID;
            this.PreviousPath = Constants.LINK_SEARCH_TEMPLATE;

            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            switch (CommandName)
            {
                case CommandNameEnum.Add:
                    obj.Insert();
                    break;
                case CommandNameEnum.Edit:
                    if (FileUpload1.HasFile && (Path.GetExtension(FileUpload1.FileName).Equals(".xls")))
                    {
                        String _pathSourceFile = String.Format(Configuration.PATH_TEMPLATE, FileUpload1.FileName);
                        String _phisicalPath = String.Format(System.AppDomain.CurrentDomain.BaseDirectory + Configuration.PATH_TEMPLATE, String.Empty);
                        String _savefilePath = String.Format(System.AppDomain.CurrentDomain.BaseDirectory + Configuration.PATH_TEMPLATE, FileUpload1.FileName);
                        if (!Directory.Exists(_phisicalPath))
                        {
                            Directory.CreateDirectory(_phisicalPath);
                        }
                        txtPathSourceFile.Text = _pathSourceFile;
                        FileUpload1.SaveAs(_savefilePath);
                        //::PROCESS UPLOAD
                        processUpload(_savefilePath);
                    }
                    obj.Update();
                    break;
            }

            //Commit
            GeneralManager.Commit();
            removeSession();
            Response.Redirect(PreviousPath);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtName.Text = string.Empty;
            removeSession();
            Response.Redirect(PreviousPath);
        }

        private void processUpload(String filePath)
        {
            Boolean bUploadSuccess = false;
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    HSSFWorkbook wd = new HSSFWorkbook(fs);
                    int id = 1;
                    switch (this.PKID)
                    {
                        #region "OTHER"
                        case 1://Canon-DHS.xlt
                            break;
                        case 2://DI Water Template (Asia Micro,Thailand Sample, MClean)_Ver 2.XLT
                            break;
                        case 3://Donaldson-DHS.xlt
                            break;
                        case 4://Donaldson-FTIR & NVR.xlt
                            break;
                        case 5://Donalson-LPC.xlt
                            break;
                        case 6://ELWA (IC Water).xlt
                            break;
                        case 7://NTN-IC, NVR & FTIR (Bearphite).xlt
                            break;
                        case 8://NTN-IC, NVR & FTIR (Housing).xlt
                            break;
                        case 9://Samsung-Pivot.xlt
                            break;
                        #endregion
                        #region "SEAGATE"
                        case 10://Seagate-Corrosion Copper Wire.xlt
                            break;
                        case 11://Seagate-Corrosion.xlt
                            break;
                        case 12://Seagate-DHS (Cleanroom mat Construction).xlt
                            break;
                        case 13://Seagate-DHS Adhesive.xlt
                            break;
                        case 14://Seagate-DHS Bag.xlt
                            break;
                        case 15://Seagate-DHS Component 1.1.xlt
                            break;
                        case 16:
                            #region "Seagate-DHS Component Rev. BE 1.5.xlt"
                            ISheet sheetComponent = wd.GetSheet("Component");
                            ISheet sheetDetailSpec = wd.GetSheet("Detail Spec");

                            List<template_16_component> listCompo16 = new List<template_16_component>();
                            List<template_16_detail_spec> listDetail16 = new List<template_16_detail_spec>();

                            template_16_component tem16 = new template_16_component();

                            template_16_detail_spec tem16_detail = new template_16_detail_spec();

                            //Delete before new insert
                            tem16_detail.EmptyDB();
                            tem16.EmptyDB();



                            for (int row = 4; row <= sheetComponent.LastRowNum; row++)
                            {
                                if (sheetComponent.GetRow(row) != null) //null is when the row only contains empty cells 
                                {
                                    if (sheetComponent.GetRow(row).GetCell(4) != null)
                                    {
                                        if (sheetComponent.GetRow(row).GetCell(4).CellType == CellType.Numeric)
                                        {
                                            tem16.ID = id;
                                            tem16.A = (sheetComponent.GetRow(row).GetCell(0) == null) ? String.Empty : sheetComponent.GetRow(row).GetCell(0).StringCellValue;
                                            tem16.B = (sheetComponent.GetRow(row).GetCell(1) == null) ? String.Empty : sheetComponent.GetRow(row).GetCell(1).StringCellValue;
                                            tem16.C = (sheetComponent.GetRow(row).GetCell(2) == null) ? String.Empty : sheetComponent.GetRow(row).GetCell(2).StringCellValue;
                                            tem16.D = (sheetComponent.GetRow(row).GetCell(3) == null) ? String.Empty : sheetComponent.GetRow(row).GetCell(3).StringCellValue;
                                            #region "Add Detail"
                                            int spectRefId = Convert.ToInt32(sheetComponent.GetRow(row).GetCell(4).NumericCellValue);
                                            for (int row1 = 2; row1 <= sheetDetailSpec.LastRowNum; row1++)
                                            {
                                                template_16_detail_spec spec16 = new template_16_detail_spec();
                                                spec16.component_order = row1 - 1;
                                                spec16.component_id = id;
                                                spec16.name = (sheetDetailSpec.GetRow(row1).GetCell(spectRefId) == null) ? String.Empty : sheetDetailSpec.GetRow(row1).GetCell(spectRefId).StringCellValue;
                                                if (sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1) != null)
                                                {
                                                    if (sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1).CellType == CellType.Numeric)
                                                    {
                                                        spec16.ng_part = ((sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1) == null) ? 0 : sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1).NumericCellValue) + "";
                                                    }
                                                    else
                                                    {
                                                        spec16.ng_part = ((sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1) == null) ? String.Empty : sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1).StringCellValue) + "";
                                                    }
                                                }
                                                else
                                                {
                                                    spec16.ng_part = ((sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1) == null) ? String.Empty : sheetDetailSpec.GetRow(row1).GetCell(spectRefId + 1).StringCellValue) + "";
                                                }

                                                listDetail16.Add(spec16);

                                            }
                                            #endregion
                                            listCompo16.Add(tem16);
                                            id++;
                                        }
                                    }
                                }
                            }

                            tem16.InsertList(listCompo16);
                            tem16_detail.InsertList(listDetail16);

                            #endregion
                            break;
                        case 17://Seagate-DHS Component.xlt
                            break;
                        case 18://Seagate-DHS Indirect Material.xlt
                            break;
                        case 19://Seagate-DHS Tape Label.xlt
                            break;
                        case 20://Seagate-FTIR Component Rev.BE 1.5.xlt
                            break;
                        case 21://Seagate-FTIR Component.xlt
                            break;
                        case 22://Seagate-FTIR Consumable 1.2.xlt
                            break;
                        case 23://Seagate-FTIR Consumable.xlt
                            break;
                        case 24://Seagate-GCMS Component 1.9.xlt
                            break;
                        case 25://Seagate-GCMS Component.xlt
                            break;
                        case 26:
                            #region "Seagate-HPA Filtration Rev.BE 1.4.xlt"
                            ISheet sheetSeagateHpa = wd.GetSheet("Specification");
                            id = 1;
                            if (sheetSeagateHpa != null)
                            {
                                tb_m_hpa_specification icSeagateSpec = new tb_m_hpa_specification();
                                List<tb_m_hpa_specification> lists = new List<tb_m_hpa_specification>();
                                //Delete before new insert
                                icSeagateSpec.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.SEAGATE));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_m_hpa_specification> tmpList = icSeagateSpec.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.SEAGATE));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 4; row <= sheetSeagateHpa.LastRowNum; row++)
                                {
                                    if (sheetSeagateHpa.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {

                                        tb_m_hpa_specification tmp = new tb_m_hpa_specification();
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.SEAGATE);
                                        tmp.ID = id;

                                        String A = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(A)) { continue; }
                                        tmp.A = A;
                                        tmp.B = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(1));
                                        tmp.C = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(2));
                                        tmp.D = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(3));
                                        tmp.E = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(4));
                                        tmp.F = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(5));
                                        tmp.G = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(6));
                                        tmp.H = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(7));
                                        tmp.I = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(8));
                                        tmp.J = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(9));
                                        tmp.K = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(10));
                                        tmp.L = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(11));
                                        tmp.M = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(12));
                                        tmp.N = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(13));
                                        tmp.O = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(14));
                                        tmp.P = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(15));
                                        tmp.Q = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(16));
                                        tmp.R = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(17));
                                        tmp.S = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(18));
                                        tmp.T = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(19));
                                        tmp.U = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(20));
                                        tmp.V = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(21));
                                        tmp.W = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(22));
                                        tmp.X = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(23));
                                        tmp.Y = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(24));
                                        tmp.Z = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(25));
                                        tmp.AA = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(26));
                                        tmp.AB = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(27));
                                        tmp.AC = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(28));
                                        tmp.AD = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(29));
                                        tmp.AE = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(30));
                                        tmp.AF = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(31));
                                        tmp.AG = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(32));
                                        tmp.AH = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(33));
                                        tmp.AI = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(34));
                                        tmp.AJ = CustomUtils.GetCellValue(sheetSeagateHpa.GetRow(row).GetCell(35));


                                        tmp.RowState = cmd;

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                icSeagateSpec.InsertList(lists);
                            }
                            #endregion
                            break;
                        case 27://Seagate-HPA Filtration3.xlt
                            break;
                        case 28://Seagate-HPA Swab.xlt
                            break;
                        case 29:
                            #region "Seagate-IC Component 1.4.xlt"
                            ISheet sheetSeagateIc = wd.GetSheet("Specification");
                            id = 1;
                            if (sheetSeagateIc != null)
                            {
                                tb_m_ic_specification icSeagateSpec = new tb_m_ic_specification();
                                List<tb_m_ic_specification> lists = new List<tb_m_ic_specification>();
                                //Delete before new insert
                                icSeagateSpec.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.SEAGATE));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_m_ic_specification> tmpList = icSeagateSpec.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.SEAGATE));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 3; row <= sheetSeagateIc.LastRowNum; row++)
                                {
                                    if (sheetSeagateIc.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {
                                        tb_m_ic_specification tmp = new tb_m_ic_specification();
                                        tmp.ID = id;

                                        String A = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(A))
                                        {
                                            continue;
                                        }
                                        tmp.A = A;

                                        String B = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(1));
                                        tmp.B = String.IsNullOrEmpty(B) ? "-1" : B;
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.SEAGATE);
                                        tmp.C = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(2));
                                        tmp.D = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(3));//SO4
                                        tmp.E = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(4));//Br

                                        tmp.F = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(5));//F
                                        tmp.G = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(6));//Cl	

                                        tmp.H = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(7));//NO3
                                        tmp.I = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(8));//NO2	
                                        tmp.J = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(9));//PO4
                                        tmp.K = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(10));//Total Anions


                                        tmp.L = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(11));//NH4
                                        tmp.M = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(12));//Li
                                        tmp.N = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(13));//Ca
                                        tmp.O = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(14));//K
                                        tmp.P = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(15));//Na
                                        tmp.Q = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(16));//Mg

                                        tmp.R = CustomUtils.GetCellValue(sheetSeagateIc.GetRow(row).GetCell(17));//Total Cations
                                        tmp.RowState = cmd;

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                icSeagateSpec.InsertList(lists);
                            }
                            #endregion
                            break;
                        case 30://Seagate-IC Component HGA.xlt
                            break;
                        case 31://Seagate-IC Component PCBA.xlt
                            break;
                        case 32://Seagate-IC Component.xlt
                            break;
                        case 33://Seagate-IC Consumable 1.2.xlt
                            break;
                        case 34:
                            #region "Seagate-LPC Component 1.4 Rev.BE.xlt"
                            ISheet sheetSeagateLpc = wd.GetSheet("Specification");
                            id = 1;
                            if (sheetSeagateLpc != null)
                            {
                                tb_m_detail_spec_lpc icSeagateSpec = new tb_m_detail_spec_lpc();
                                List<tb_m_detail_spec_lpc> lists = new List<tb_m_detail_spec_lpc>();
                                //Delete before new insert
                                //icSeagateSpec.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.SEAGATE));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_m_detail_spec_lpc> tmpList = icSeagateSpec.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.SEAGATE));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 3; row <= sheetSeagateLpc.LastRowNum; row++)
                                {
                                    if (sheetSeagateLpc.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {
                                        tb_m_detail_spec_lpc tmp = new tb_m_detail_spec_lpc();
                                        tmp.ID = id;
                                        String B = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(1));
                                        tmp.B = String.IsNullOrEmpty(B) ? "-1" : B;
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.SEAGATE);
                                        tmp.C = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(2));
                                        tmp.D = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(3));//SO4
                                        tmp.E = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(4));//Br

                                        tmp.F = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(5));//F
                                        tmp.G = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(6));//Cl	

                                        tmp.H = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(7));//NO3
                                        tmp.I = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(8));//NO2	
                                        tmp.J = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(9));//PO4
                                        tmp.K = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(10));//Total Anions


                                        tmp.L = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(11));//NH4
                                        tmp.M = CustomUtils.GetCellValue(sheetSeagateLpc.GetRow(row).GetCell(12));//Li
                                        tmp.RowState = cmd;

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                icSeagateSpec.InsertList(lists);
                            }

                            #endregion
                            break;
                        case 35://Seagate-LPC Component.xlt
                            break;
                        case 36://Seagate-LPC Consumable.xlt
                            break;
                        case 37://Segate HPA(MgSiO)-Wiper&Paper.xlt
                            break;
                        #endregion
                        #region "WD"
                        case 38://D-Corrosion 1.0.xlt
                            break;
                        case 39://D-CVR 1.0.xlt
                            break;
                        case 40://D-DHS (Label, Sticker, Tape) 1.0.xlt
                            break;
                        case 41://D-DHS Component 1.0.xlt
                            break;
                        case 42:
                            #region "WD-DHS Component 1.5.xlt"
                            ISheet sheet = wd.GetSheet("Detail Spec");
                            id = 1;
                            if (sheet != null)
                            {
                                template_42_detail_spec ds42 = new template_42_detail_spec();
                                List<template_42_detail_spec> lists = new List<template_42_detail_spec>();
                                //Delete before new insert
                                ds42.EmptyDB();
                                for (int row = 3; row <= sheet.LastRowNum; row++)
                                {
                                    if (sheet.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {
                                        template_42_detail_spec tmp = new template_42_detail_spec();
                                        tmp.ID = id;

                                        String A = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(A))
                                        {
                                            continue;
                                        }
                                        tmp.A = A;

                                        String B = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(1));
                                        tmp.B = String.IsNullOrEmpty(B) ? "-1" : B;

                                        tmp.C = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(2));
                                        tmp.D = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(3));
                                        tmp.E = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(4));
                                        tmp.F = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(5));
                                        tmp.G = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(6));

                                        tmp.H = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(7));
                                        tmp.I = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(8));
                                        tmp.J = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(9));
                                        tmp.K = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(10));
                                        tmp.L = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(11));

                                        tmp.M = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(12));
                                        tmp.N = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(13));
                                        tmp.O = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(14));
                                        tmp.P = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(15));
                                        tmp.Q = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(16));

                                        tmp.R = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(17));
                                        tmp.S = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(18));
                                        tmp.T = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(19));
                                        tmp.U = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(20));
                                        tmp.V = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(21));

                                        tmp.W = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(22));
                                        tmp.X = CustomUtils.GetCellValue(sheet.GetRow(row).GetCell(23));

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                ds42.InsertList(lists);
                            }
                            #endregion
                            break;
                        case 43://WD-DHS Template 1.0.xlt
                            break;
                        case 44://WD-GCMS 1.0.xlt
                            break;
                        case 45://WD-GCMS 1.8.xlt
                            break;
                        case 46://WD-GCMS Tooling 1.0.xlt
                            break;
                        case 47://WD-HPA Filtration 1.2.xlt
                            break;
                        case 48://WD-HPA Tape Test (Edge Damper, FCOF) 1.2.xlt
                            break;
                        case 49://WD-HPA Tape Test (HGA, Suspension) 1.0.xlt
                            break;
                        case 50://WD-HPA Tape Test (HSA, APFA, ACA) 1.1.xlt
                            break;
                        case 51://WD-HPA Tape Test (HSA, APFA, ACA) 1.6.xlt
                            #region "Seagate-HPA Filtration Rev.BE 1.4.xlt"
                            #region "-Component"
                            ISheet wdComponent = wd.GetSheet("Component");
                            id = 1;
                            if (wdComponent != null)
                            {
                                tb_component_hpa_for1 compWdHpa = new tb_component_hpa_for1();
                                List<tb_component_hpa_for1> lists = new List<tb_component_hpa_for1>();
                                //Delete before new insert
                                compWdHpa.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_component_hpa_for1> tmpList = compWdHpa.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 4; row <= wdComponent.LastRowNum; row++)
                                {
                                    if (wdComponent.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {

                                        tb_component_hpa_for1 tmp = new tb_component_hpa_for1();
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.WD);
                                        tmp.ID = id;

                                        String A = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(A)) { continue; }
                                        tmp.A = A;
                                        tmp.B = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(1));
                                        tmp.C = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(2));
                                        tmp.D = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(3));
                                        tmp.E = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(4));
                                        tmp.F = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(5));
                                        tmp.G = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(6));
                                        tmp.H = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(7));
                                        tmp.I = CustomUtils.GetCellValue(wdComponent.GetRow(row).GetCell(8));

                                        tmp.RowState = cmd;

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                compWdHpa.InsertList(lists);
                            }
                            #endregion
                            #region "-Detail Spec"
                            ISheet wdDetailSpec = wd.GetSheet("Detail Spec");
                            id = 1;
                            if (wdDetailSpec != null)
                            {
                                tb_detailspec_hpa_for1 detailSpecWdHpa = new tb_detailspec_hpa_for1();
                                List<tb_detailspec_hpa_for1> lists = new List<tb_detailspec_hpa_for1>();
                                //Delete before new insert
                                detailSpecWdHpa.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_detailspec_hpa_for1> tmpList = detailSpecWdHpa.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 4; row <= wdDetailSpec.LastRowNum; row++)
                                {
                                    if (wdDetailSpec.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {

                                        tb_detailspec_hpa_for1 tmp = new tb_detailspec_hpa_for1();
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.WD);
                                        tmp.ID = id;

                                        String A = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(A)) { continue; }
                                        tmp.A = A;
                                        tmp.B = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(1));
                                        tmp.C = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(2));
                                        tmp.D = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(3));
                                        tmp.E = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(4));
                                        tmp.F = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(5));
                                        tmp.G = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(6));
                                        tmp.H = CustomUtils.GetCellValue(wdDetailSpec.GetRow(row).GetCell(7));

                                        tmp.RowState = cmd;

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                detailSpecWdHpa.InsertList(lists);
                            }
                            #endregion
                            #endregion
                            break;
                        case 52://WD-HPA Tape Test 1.2.xlt
                            break;
                        case 53://WD-IC Component 1.1.xlt
                            break;
                        case 54:
                            #region "WD-IC Component 1.3.xlt"
                            ISheet sheetWDIc = wd.GetSheet("Detail Spec");
                            id = 1;
                            if (sheetWDIc != null)
                            {
                                tb_m_ic_specification icSpec = new tb_m_ic_specification();
                                List<tb_m_ic_specification> lists = new List<tb_m_ic_specification>();
                                //Delete before new insert
                                icSpec.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_m_ic_specification> tmpList = icSpec.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 3; row <= sheetWDIc.LastRowNum; row++)
                                {
                                    if (sheetWDIc.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {
                                        tb_m_ic_specification tmp = new tb_m_ic_specification();
                                        tmp.ID = id;

                                        String A = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(A))
                                        {
                                            continue;
                                        }
                                        tmp.A = A;

                                        String B = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(1));
                                        tmp.B = String.IsNullOrEmpty(B) ? "-1" : B;
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.WD);
                                        tmp.C = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(2));
                                        tmp.D = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(3));
                                        tmp.E = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(4));
                                        tmp.F = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(5));
                                        tmp.G = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(6));

                                        tmp.H = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(7));
                                        tmp.I = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(8));
                                        tmp.J = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(9));
                                        tmp.K = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(10));
                                        tmp.L = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(11));

                                        tmp.M = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(12));
                                        tmp.N = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(13));
                                        tmp.O = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(14));
                                        tmp.P = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(15));
                                        tmp.Q = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(16));

                                        tmp.R = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(17));
                                        tmp.RowState = cmd;
                                        //tmp.S = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(18));
                                        //tmp.T = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(19));
                                        //tmp.U = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(20));
                                        //tmp.V = CustomUtils.GetCellValue(sheetWDIc.GetRow(row).GetCell(21));

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                icSpec.InsertList(lists);
                            }
                            #endregion
                            break;
                        case 55://WD-IC Template 1.0.xlt
                            break;
                        case 56://WD-IR Component 1.5.xlt
                            break;
                        case 57://WD-LPC Component 2.0.xlt
                            break;
                        case 58:
                            #region "WD-LPC Component 2.2.xlt"
                            ISheet sheetWdLpc = wd.GetSheet("Detail Spec");
                            id = 1;
                            if (sheetWdLpc != null)
                            {
                                tb_m_detail_spec_lpc wdLPC = new tb_m_detail_spec_lpc();
                                List<tb_m_detail_spec_lpc> lists = new List<tb_m_detail_spec_lpc>();
                                //Delete before new insert
                                //wdLPC.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_m_detail_spec_lpc> tmpList = wdLPC.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 3; row <= sheetWdLpc.LastRowNum; row++)
                                {
                                    if (sheetWdLpc.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {
                                        tb_m_detail_spec_lpc tmp = new tb_m_detail_spec_lpc();
                                        tmp.ID = id;
                                        String B = CustomUtils.GetCellValue(sheetWdLpc.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(B))
                                        {
                                            continue;
                                        }
                                        tmp.B = B;//Component
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.WD);
                                        tmp.C = CustomUtils.GetCellValue(sheetWdLpc.GetRow(row).GetCell(1));//Spec / Rev
                                        tmp.D = CustomUtils.GetCellValue(sheetWdLpc.GetRow(row).GetCell(2));//Unit

                                        tmp.E = CustomUtils.GetCellValue(sheetWdLpc.GetRow(row).GetCell(3));//≥ 0.2 um(Particles/cm2)
                                        tmp.F = CustomUtils.GetCellValue(sheetWdLpc.GetRow(row).GetCell(4));//≥ 0.3 um(Particles/cm2)
                                        tmp.G = CustomUtils.GetCellValue(sheetWdLpc.GetRow(row).GetCell(5));//≥ 0.5 um(Particles/cm2)
                                        tmp.H = CustomUtils.GetCellValue(sheetWdLpc.GetRow(row).GetCell(7));//≥ 1.0 um(Particles/cm2)

                                        tmp.RowState = cmd;

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                wdLPC.InsertList(lists);
                            }
                            ISheet sheetWdComponentLpc = wd.GetSheet("Component");
                            id = 1;
                            if (sheetWdComponentLpc != null)
                            {
                                tb_component_lpc wdComponentLPC = new tb_component_lpc();
                                List<tb_component_lpc> lists = new List<tb_component_lpc>();
                                //Delete before new insert
                                //wdLPC.DeleteBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                CommandNameEnum cmd = CommandNameEnum.Add;
                                List<tb_component_lpc> tmpList = wdComponentLPC.SelectBySpecificationID(Convert.ToInt32(SpecificationEnum.WD));
                                if (tmpList != null && tmpList.Count > 0)
                                {
                                    cmd = CommandNameEnum.Edit;
                                }

                                for (int row = 3; row <= sheetWdComponentLpc.LastRowNum; row++)
                                {
                                    if (sheetWdComponentLpc.GetRow(row) != null) //null is when the row only contains empty cells 
                                    {
                                        tb_component_lpc tmp = new tb_component_lpc();
                                        tmp.ID = id;

                                        String A = CustomUtils.GetCellValue(sheetWdComponentLpc.GetRow(row).GetCell(0));
                                        if (String.IsNullOrEmpty(A))
                                        {
                                            continue;
                                        }
                                        tmp.A = A;//Component
                                        tmp.specification_id = Convert.ToInt32(SpecificationEnum.WD);
                                        tmp.B = CustomUtils.GetCellValue(sheetWdComponentLpc.GetRow(row).GetCell(1));//Spec / Rev
                                        tmp.C = CustomUtils.GetCellValue(sheetWdComponentLpc.GetRow(row).GetCell(2));//Spec / Rev
                                        tmp.D = CustomUtils.GetCellValue(sheetWdComponentLpc.GetRow(row).GetCell(3));//Unit
                                        tmp.E = CustomUtils.GetCellValue(sheetWdComponentLpc.GetRow(row).GetCell(4));//≥ 0.2 um(Particles/cm2)
                                        tmp.F = CustomUtils.GetCellValue(sheetWdComponentLpc.GetRow(row).GetCell(5));//≥ 0.3 um(Particles/cm2)

                                        tmp.RowState = cmd;

                                        lists.Add(tmp);
                                        id++;
                                    }
                                }

                                wdComponentLPC.InsertList(lists);
                            }
                            #endregion
                            break;
                        case 59://WD-LPC Template 1.0.xlt
                            break;
                        case 60://WD-MESA Template 1.1.xlt
                            break;
                        #endregion
                    }
                }

                bUploadSuccess = true;
                //Commit
                GeneralManager.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (bUploadSuccess)
            {
                Message = "<div class=\"alert alert-uccess\"><button class=\"close\" data-dismiss=\"alert\"></button><span>Upload Success.</span></div>";
            }
            else
            {
                Message = "<div class=\"alert alert-error\"><button class=\"close\" data-dismiss=\"alert\"></button><span>Upload Fail.</span></div>";
            }

        }
    }
}