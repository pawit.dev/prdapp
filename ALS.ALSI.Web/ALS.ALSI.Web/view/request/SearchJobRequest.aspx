﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SearchJobRequest.aspx.cs" Inherits="ALS.ALSI.Web.view.request.SearchJobRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.date-picker').datepicker();
            App.init(); // init the rest of plugins and elements

        });
    </script>
    <style>
        .job_status {
            position: relative;
            border: 0px solid black;
        }
        /*Status*/
        .job_status_black {
            width: 16px;
            height: 16px;
            overflow: hidden;
            cursor: pointer;
            float: left;
            text-indent: -999px;
            margin-right: 6px;
            margin-bottom: 6px;
            border: 1px solid #999;
            background: #333;
        }

        .job_status_blue {
            width: 16px;
            height: 16px;
            overflow: hidden;
            cursor: pointer;
            float: left;
            text-indent: -999px;
            margin-right: 6px;
            margin-bottom: 6px;
            border: 1px solid #999;
            background: #0088cc;
        }

        .job_status_pink {
            width: 16px;
            height: 16px;
            overflow: hidden;
            cursor: pointer;
            float: left;
            text-indent: -999px;
            margin-right: 6px;
            margin-bottom: 6px;
            border: 1px solid #999;
            background: #f2dede;
        }

        .job_status_red {
            width: 16px;
            height: 16px;
            overflow: hidden;
            cursor: pointer;
            float: left;
            text-indent: -999px;
            margin-right: 6px;
            margin-bottom: 6px;
            border: 1px solid #999;
            background: #f00;
        }

        .job_status_green {
            width: 16px;
            height: 16px;
            overflow: hidden;
            cursor: pointer;
            float: left;
            text-indent: -999px;
            margin-right: 6px;
            margin-bottom: 6px;
            border: 1px solid #999;
            background: #408124;
        }

        .job_status_violet {
            width: 16px;
            height: 16px;
            overflow: hidden;
            cursor: pointer;
            float: left;
            text-indent: -999px;
            margin-right: 6px;
            margin-bottom: 6px;
            border: 1px solid #999;
            background: violet;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form1" method="post" runat="server" class="form-horizontal">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:HiddenField ID="hPrefix" Value="1" runat="server" />
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet">
                            <div class="portlet-title">
                                <h4><i class="icon-reorder"></i>Search Condition</h4>
                                <div class="actions">
                                    <div class="btn-group">
                                        <asp:LinkButton ID="btnSearch" runat="server" class="btn mini blue" OnClick="btnSearch_Click"><i class="icon-search"></i> Search</asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" runat="server" class="btn mini black" OnClick="btnCancel_Click"><i class="icon-refresh"></i> Cancel</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label" for="txtJobNumber">Jub Number:</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtJobNumber" runat="server" class="m-wrap span6"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label" for="txtDateOfRecieve">Date Of Recieve:</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtDateOfRecieve" runat="server" class="m-wrap span6 date-picker"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label" for="txtId">Company:</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlCompany" runat="server" class="span6 chosen" DataTextField="company_name" DataValueField="ID" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="span6 ">
                                        <div class="control-group">
                                            <label class="control-label" for="txtName">Contract Person:</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddlContract_person" runat="server" class="span6 chosen" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box grey">
                            <div class="portlet-title">
                                <h4><i class="icon-bar-chart"></i>Search Result</h4>
                                <div class="actions">
                                    <div class="btn-group">
                                        <asp:LinkButton ID="lbAddJob" runat="server" class="btn mini green" OnClick="lbAddJob_Click"><i class="icon-pencil"></i> Add</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <i class="icon-table"></i>
                                <asp:Label ID="lbTotalRecords" runat="server" Text=""></asp:Label>

                                <br />
                                <table>
                                    <tr>
                                        <td>Normal:</td>
                                        <td>
                                            <div class="job_status">
                                                <div class="job_status_black"></div>
                                            </div>
                                        </td>
                                        <td>Urgent:</td>
                                        <td>
                                            <div class="job_status">
                                                <div class="job_status_blue"></div>
                                            </div>
                                        </td>
                                        <td>Express:</td>
                                        <td>
                                            <div class="job_status">
                                                <div class="job_status_pink"></div>
                                            </div>
                                        </td>
                                        <td>Cancel:</td>
                                        <td>
                                            <div class="job_status">
                                                <div class="job_status_red"></div>
                                            </div>
                                        </td>
                                        <td>Complete:</td>
                                        <td>
                                            <div class="job_status">
                                                <div class="job_status_green"></div>
                                            </div>
                                        </td>
                                        <td>Hold:</td>
                                        <td>
                                            <div class="job_status">
                                                <div class="job_status_violet"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Button ID="btnElp" runat="server" Text="ELP" CssClass="btn blue" OnClick="btnElp_Click" />
                                <asp:Button ID="btnEls" runat="server" Text="ELS" CssClass="btn blue" OnClick="btnElp_Click" />
                                <asp:Button ID="btnFa" runat="server" Text="FA" CssClass="btn blue" OnClick="btnElp_Click" />
                                <asp:Button ID="btnElwa" runat="server" Text="ELWA" CssClass="btn blue" OnClick="btnElp_Click" />
                                <asp:Button ID="btnGrp" runat="server" Text="GRP" CssClass="btn blue" OnClick="btnElp_Click" />
                                <asp:Button ID="btnTrb" runat="server" Text="TRB" CssClass="btn blue" OnClick="btnElp_Click" />
                                <asp:GridView ID="gvJob" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    CssClass="table table-striped table-hover table-bordered" ShowHeaderWhenEmpty="True" DataKeyNames="ID,job_status,job_role,status_completion_scheduled,step1owner,step2owner,step3owner,step4owner,step5owner,step6owner" OnRowCommand="gvJob_RowCommand" OnPageIndexChanging="gvJob_PageIndexChanging" OnRowDataBound="gvJob_RowDataBound">
                                    <Columns>
                                        <asp:BoundField HeaderText="Date Received." DataField="create_date" ItemStyle-HorizontalAlign="Center" SortExpression="create_date" DataFormatString="{0:dd-MM-yyyy}" />
                                        <asp:BoundField HeaderText="Due Date." DataField="due_date" ItemStyle-HorizontalAlign="Center" SortExpression="due_date" DataFormatString="{0:dd-MM-yyyy}" />
                                        <%--<asp:BoundField HeaderText="Ref No." DataField="job_number" ItemStyle-HorizontalAlign="Left" SortExpression="job_number" />--%>
                                        <asp:TemplateField HeaderText="Ref No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lbJobNumber" runat="server" Text='<%# Eval("job_number")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Cus Ref No." DataField="customer_ref_no" ItemStyle-HorizontalAlign="Left" SortExpression="customer_ref_no" />
                                        <asp:BoundField HeaderText="S'Ref No." DataField="s_pore_ref_no" ItemStyle-HorizontalAlign="Left" SortExpression="s_pore_ref_no" />
                                        <%--<asp:BoundField HeaderText="Other" DataField="customer_po_ref" ItemStyle-HorizontalAlign="Left" SortExpression="customer_po_ref" />--%>
                                        <asp:TemplateField HeaderText="Other">
                                            <ItemTemplate>
                                                <asp:Label ID="ltJobStatus" runat="server" Text="-"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Comp" DataField="customer" ItemStyle-HorizontalAlign="Left" SortExpression="customer" />
                                        <asp:BoundField HeaderText="Contact" DataField="contract_person" ItemStyle-HorizontalAlign="Left" SortExpression="contract_person" />
                                        <%--<asp:BoundField HeaderText="S/N" DataField="sn" ItemStyle-HorizontalAlign="Left" SortExpression="sn" />--%>
                                        <%--<asp:BoundField HeaderText="Description" DataField="description" ItemStyle-HorizontalAlign="Left" SortExpression="description" />--%>
                                        <asp:BoundField HeaderText="Model" DataField="model" ItemStyle-HorizontalAlign="Left" SortExpression="model" />
                                        <asp:BoundField HeaderText="Surface Area" DataField="surface_area" ItemStyle-HorizontalAlign="Left" SortExpression="surface_area" />
                                        <%--<asp:BoundField HeaderText="Remarks" DataField="remarks" ItemStyle-HorizontalAlign="Left" SortExpression="remarks" />--%>
                                        <asp:BoundField HeaderText="Specification" DataField="specification" ItemStyle-HorizontalAlign="Left" SortExpression="specification" />
                                        <asp:BoundField HeaderText="Type of test" DataField="type_of_test" ItemStyle-HorizontalAlign="Left" SortExpression="type_of_test" />

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>

                                                <asp:LinkButton ID="btnInfo" runat="server" ToolTip="Info" CommandName="View" CommandArgument='<%# Eval("ID")%>'><i class="icon-zoom-in"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnEdit" runat="server" ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("ID")%>'><i class="icon-edit"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnConvertTemplete" runat="server" ToolTip="Convert Template" CommandName="ConvertTemplate" CommandArgument='<%# String.Concat(Eval("ID"),ALS.ALSI.Biz.Constant.Constants.CHAR_COMMA,Eval("SN"))%>'><i class="icon-tasks"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnWorkFlow" runat="server" ToolTip="Work Flow" CommandName="Workflow" CommandArgument='<%# String.Concat(Eval("ID"),ALS.ALSI.Biz.Constant.Constants.CHAR_COMMA,Eval("SN"))%>'><i class="icon-beaker"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnChangeStatus" runat="server" ToolTip="Change Status" CommandName="ChangeStatus" CommandArgument='<%# String.Concat(Eval("ID"),ALS.ALSI.Biz.Constant.Constants.CHAR_COMMA,Eval("SN"))%>'><i class="icon-refresh"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnChangeDueDate" runat="server" ToolTip="Change DueDate" CommandName="ChangeDueDate" CommandArgument='<%# String.Concat(Eval("ID"),ALS.ALSI.Biz.Constant.Constants.CHAR_COMMA,Eval("SN"))%>'><i class="icon-calendar"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnChangePo" runat="server" ToolTip="Change PO" CommandName="ChangePo" CommandArgument='<%# String.Concat(Eval("ID"),ALS.ALSI.Biz.Constant.Constants.CHAR_COMMA,Eval("SN"))%>'><i class="icon-credit-card"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnChangeInvoice" runat="server" ToolTip="Chnage Invoice" CommandName="ChangeInvoice" CommandArgument='<%# String.Concat(Eval("ID"),ALS.ALSI.Biz.Constant.Constants.CHAR_COMMA,Eval("SN"))%>'><i class="icon-tag"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnPrintLabel" runat="server" ToolTip="Print Label" CommandName="Print" CommandArgument='<%# String.Concat(Eval("ID"),ALS.ALSI.Biz.Constant.Constants.CHAR_COMMA,Eval("SN"))%>'><i class="icon-print"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerTemplate>
                                        <div class="pagination">
                                            <ul>
                                                <li>
                                                    <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                        CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                                </li>
                                                <li>
                                                    <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                        CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                                </li>
                                                <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                                <li>
                                                    <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                        CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                                </li>
                                                <li>
                                                    <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                        CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                                </li>
                                            </ul>
                                        </div>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        <div class="data-not-found">
                                            <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                        </div>
                                    </EmptyDataTemplate>
                                </asp:GridView>

                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</asp:Content>
