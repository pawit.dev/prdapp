﻿using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web.view.request
{
    public partial class SearchJobRequest : System.Web.UI.Page
    {

        #region "Property"
        public user_login userLogin
        {
            get
            {
                return ((Session[Constants.SESSION_USER] != null) ? (user_login)Session[Constants.SESSION_USER] : null);
            }
        }

        public IEnumerable searchResult
        {
            get { return (IEnumerable)Session[GetType().Name + "SearchJobRequest"]; }
            set { Session[GetType().Name + "SearchJobRequest"] = value; }
        }

        public CommandNameEnum CommandName
        {
            get { return (CommandNameEnum)ViewState[Constants.COMMAND_NAME]; }
            set { ViewState[Constants.COMMAND_NAME] = value; }
        }

        public int JobID { get; set; }

        public int SampleID { get; set; }

        public job_info obj
        {
            get
            {
                job_info tmp = new job_info();
                //tmp.job_number = txtJobNumber.Text;
                tmp.date_of_receive = String.IsNullOrEmpty(txtDateOfRecieve.Text) ? DateTime.MinValue : Convert.ToDateTime(txtDateOfRecieve.Text);
                tmp.contract_person_id = String.IsNullOrEmpty(ddlContract_person.SelectedValue) ? 0 : int.Parse(ddlContract_person.SelectedValue);
                tmp.customer_id = String.IsNullOrEmpty(ddlCompany.SelectedValue) ? 0 : int.Parse(ddlCompany.SelectedValue);
                tmp.job_prefix = String.IsNullOrEmpty(hPrefix.Value) ? 1 : Convert.ToInt16(hPrefix.Value);
                RoleEnum role = (RoleEnum)Enum.ToObject(typeof(RoleEnum), userLogin.role_id);
                switch (role)
                {
                    case RoleEnum.LOGIN:
                        break;
                    case RoleEnum.ROOT:
                        break;
                    case RoleEnum.CHEMIST:
                        tmp.responsible_test = userLogin.responsible_test.Split(Constants.CHAR_COMMA);
                        break;
                    case RoleEnum.SR_CHEMIST:
                        break;
                    case RoleEnum.LABMANAGER:
                        break;
                    case RoleEnum.ADMIN:
                        break;
                }
                return tmp;
            }
        }

        #endregion

        #region "Method"

        private void initialPage()
        {

            ddlCompany.Items.Clear();
            ddlCompany.DataSource = new m_customer().SelectAll();
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));


            ddlContract_person.Items.Clear();
            ddlContract_person.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));
            ddlContract_person.DataBind();

            bindingData();
            //prefix tab
            btnElp.CssClass = "btn green";
            btnEls.CssClass = "btn blue";
            btnFa.CssClass = "btn blue";
            btnElwa.CssClass = "btn blue";
            btnGrp.CssClass = "btn blue";
            btnTrb.CssClass = "btn blue";
        }

        private void bindingData()
        {
            searchResult = obj.SearchData();
            gvJob.DataSource = searchResult;
            gvJob.DataBind();
            if (gvJob.Rows.Count > 0)
            {
                lbTotalRecords.Text = String.Format(Constants.TOTAL_RECORDS, gvJob.Rows.Count);
            }
            else
            {
                lbTotalRecords.Text = string.Empty;
            }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name);
            Session.Remove(GetType().Name + "SearchJobRequest");
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                initialPage();
            }
        }

        protected void lbAddJob_Click(object sender, EventArgs e)
        {
            this.CommandName = CommandNameEnum.Add;
            Server.Transfer(Constants.LINK_JOB_REQUEST);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            bindingData();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtJobNumber.Text = string.Empty;
            txtDateOfRecieve.Text = string.Empty;
            lbTotalRecords.Text = string.Empty;
            ddlCompany.SelectedIndex = 0;

            ddlContract_person.Items.Clear();
            ddlContract_person.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));
            ddlContract_person.DataBind();

            removeSession();

            bindingData();
        }

        protected void gvJob_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            CommandNameEnum cmd = (CommandNameEnum)Enum.Parse(typeof(CommandNameEnum), e.CommandName, true);
            this.CommandName = cmd;
            switch (cmd)
            {
                case CommandNameEnum.Edit:
                case CommandNameEnum.View:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    Server.Transfer(Constants.LINK_JOB_REQUEST);
                    break;
                case CommandNameEnum.ConvertTemplate:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    this.SampleID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[1]);
                    Server.Transfer(Constants.LINK_JOB_CONVERT_TEMPLATE);
                    break;
                case CommandNameEnum.Workflow:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    this.SampleID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[1]);
                    Server.Transfer(Constants.LINK_JOB_WORK_FLOW);
                    break;
                case CommandNameEnum.ChangeStatus:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    this.SampleID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[1]);
                    Server.Transfer(Constants.LINK_JOB_CHANGE_STATUS);
                    break;
                case CommandNameEnum.ChangeDueDate:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    this.SampleID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[1]);
                    Server.Transfer(Constants.LINK_JOB_CHANGE_DUEDATE);
                    break;
                case CommandNameEnum.ChangePo:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    this.SampleID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[1]);
                    Server.Transfer(Constants.LINK_JOB_CHANGE_PO);
                    break;
                case CommandNameEnum.ChangeInvoice:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    this.SampleID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[1]);
                    Server.Transfer(Constants.LINK_JOB_CHANGE_INVOICE);
                    break;
                case CommandNameEnum.Print:
                    this.JobID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[0]);
                    this.SampleID = int.Parse(e.CommandArgument.ToString().Split(Constants.CHAR_COMMA)[1]);
                    Server.Transfer(Constants.LINK_JOB_PRINT_LABEL);
                    break;
            }

        }

        protected void gvJob_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e.NewPageIndex < 0) return;
            GridView gv = (GridView)sender;
            gv.DataSource = searchResult;
            gv.PageIndex = e.NewPageIndex;
            gv.DataBind();
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlContract_person.Items.Clear();
            ddlContract_person.Items.Insert(0, new ListItem(Constants.PLEASE_SELECT, "0"));
            ddlContract_person.DataBind();


            m_customer cus = new m_customer().SelectByID(Convert.ToInt32(ddlCompany.SelectedValue));
            if (cus != null)
            {

                List<m_customer_contract_person> contractPersonList = new m_customer_contract_person().FindAllByCompanyID(cus.ID);
                if (contractPersonList != null)
                {
                    ddlContract_person.Items.Clear();
                    ddlContract_person.Items.Add(new ListItem(Constants.PLEASE_SELECT, ""));
                    ddlContract_person.DataSource = contractPersonList;
                    ddlContract_person.DataBind();

                    ddlContract_person.Enabled = true;
                }
                else
                {
                    //TODO            

                }
            }
            else
            {
                //TODO
            }


        }

        protected void gvJob_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gv = (GridView)sender;

                int _valueStatus = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][1]);
                int _valueCompletion_scheduled = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][3]);
                int _step1owner = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][4]);
                int _step2owner = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][5]);
                int _step3owner = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][6]);
                int _step4owner = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][7]);
                int _step5owner = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][8]);
                int _step6owner = Convert.ToInt32(gv.DataKeys[e.Row.RowIndex][9]);

                LinkButton btnInfo = (LinkButton)e.Row.FindControl("btnInfo");
                LinkButton btnEdit = (LinkButton)e.Row.FindControl("btnEdit");
                LinkButton btnConvertTemplete = (LinkButton)e.Row.FindControl("btnConvertTemplete");
                LinkButton btnWorkFlow = (LinkButton)e.Row.FindControl("btnWorkFlow");
                LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("btnChangeStatus");
                LinkButton btnChangeDueDate = (LinkButton)e.Row.FindControl("btnChangeDueDate");
                LinkButton btnChangePo = (LinkButton)e.Row.FindControl("btnChangePo");
                LinkButton btnChangeInvoice = (LinkButton)e.Row.FindControl("btnChangeInvoice");
                LinkButton btnPrintLabel = (LinkButton)e.Row.FindControl("btnPrintLabel");

                Label ltJobStatus = (Label)e.Row.FindControl("ltJobStatus");
                Label lbJobNumber = (Label)e.Row.FindControl("lbJobNumber");

                CompletionScheduledEnum status_completion_scheduled = (CompletionScheduledEnum)Enum.ToObject(typeof(CompletionScheduledEnum), _valueCompletion_scheduled);

                StatusEnum job_status = (StatusEnum)Enum.ToObject(typeof(StatusEnum), _valueStatus);
                ltJobStatus.Text = Constants.GetEnumDescription(job_status);
                RoleEnum role = (RoleEnum)Enum.ToObject(typeof(RoleEnum), userLogin.role_id);
                switch (role)
                {
                    case RoleEnum.ROOT:
                        btnInfo.Visible = true;
                        btnEdit.Visible = true;
                        btnConvertTemplete.Visible = true;
                        btnChangeStatus.Visible = true;
                        btnWorkFlow.Visible = true;
                        btnChangeDueDate.Visible = true;
                        btnChangePo.Visible = true;
                        btnChangeInvoice.Visible = true;
                        btnPrintLabel.Visible = true;
                        break;
                    case RoleEnum.LOGIN:
                        btnInfo.Visible = true;
                        btnEdit.Visible = true;
                        btnConvertTemplete.Visible = (job_status == StatusEnum.LOGIN_CONVERT_TEMPLATE) ? true : false;
                        btnChangeStatus.Visible = true;
                        btnWorkFlow.Visible = (job_status == StatusEnum.LOGIN_CONVERT_TEMPLATE || job_status == StatusEnum.CHEMIST_TESTING) ? false : true;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = false;
                        btnPrintLabel.Visible = true;
                        break;
                    case RoleEnum.CHEMIST:
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = false;
                        btnWorkFlow.Visible = (job_status == StatusEnum.LOGIN_CONVERT_TEMPLATE || job_status == StatusEnum.LOGIN_SELECT_SPEC) ? false : true;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = false;
                        btnPrintLabel.Visible = false;
                        break;
                    case RoleEnum.SR_CHEMIST:
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = false;
                        btnWorkFlow.Visible = (job_status == StatusEnum.LOGIN_CONVERT_TEMPLATE || job_status == StatusEnum.LOGIN_SELECT_SPEC || job_status == StatusEnum.CHEMIST_TESTING) ? false : true;
                        btnChangeDueDate.Visible = true;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = false;
                        btnPrintLabel.Visible = false;
                        break;
                    case RoleEnum.LABMANAGER:
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = false;
                        btnWorkFlow.Visible = (job_status == StatusEnum.LOGIN_CONVERT_TEMPLATE || job_status == StatusEnum.LOGIN_SELECT_SPEC || job_status == StatusEnum.CHEMIST_TESTING) ? false : true;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = false;
                        btnPrintLabel.Visible = false;
                        break;
                    case RoleEnum.ADMIN:
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = false;
                        btnWorkFlow.Visible = (job_status == StatusEnum.LOGIN_CONVERT_TEMPLATE || job_status == StatusEnum.LOGIN_SELECT_SPEC || job_status == StatusEnum.CHEMIST_TESTING) ? false : true;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = true;
                        btnChangeInvoice.Visible = false;
                        btnPrintLabel.Visible = false;
                        break;
                    case RoleEnum.ACCOUNT:
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = false;
                        btnWorkFlow.Visible = (job_status == StatusEnum.LOGIN_CONVERT_TEMPLATE || job_status == StatusEnum.LOGIN_SELECT_SPEC || job_status == StatusEnum.CHEMIST_TESTING) ? false : true;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = true;
                        btnPrintLabel.Visible = false;
                        break;
                }

                #region "Job color status"
                var span = new HtmlGenericControl("span");
                span.InnerHtml = lbJobNumber.Text;
                switch (status_completion_scheduled)
                {
                    case CompletionScheduledEnum.NORMAL:
                        span.Attributes["class"] = "label label-normal";
                        break;
                    case CompletionScheduledEnum.URGENT:
                        span.Attributes["class"] = "label label-urgent";
                        break;
                    case CompletionScheduledEnum.EXPRESS:
                        span.Attributes["class"] = "label label-express";
                        break;
                }
                lbJobNumber.Controls.Add(span);
                switch (job_status)
                {
                    case StatusEnum.JOB_CANCEL:
                        e.Row.ForeColor = System.Drawing.Color.Red;
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = false;
                        btnWorkFlow.Visible = false;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = false;
                        break;
                    case StatusEnum.JOB_COMPLETE:
                        e.Row.ForeColor = System.Drawing.Color.Green;
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = false;
                        btnWorkFlow.Visible = false;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = false;
                        break;
                    case StatusEnum.JOB_HOLD:
                        e.Row.ForeColor = System.Drawing.Color.Violet;
                        btnInfo.Visible = false;
                        btnEdit.Visible = false;
                        btnConvertTemplete.Visible = false;
                        btnChangeStatus.Visible = true;
                        btnWorkFlow.Visible = false;
                        btnChangeDueDate.Visible = false;
                        btnChangePo.Visible = false;
                        btnChangeInvoice.Visible = false;
                        break;
                    default:
                        e.Row.ForeColor = System.Drawing.Color.Black;
                        break;
                }
                #endregion

            }
        }

        protected void btnElp_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.Text)
            {
                case "ELP":
                    hPrefix.Value = "1";
                    btnElp.CssClass = "btn green";
                    btnEls.CssClass = "btn blue";
                    btnFa.CssClass = "btn blue";
                    btnElwa.CssClass = "btn blue";
                    btnGrp.CssClass = "btn blue";
                    btnTrb.CssClass = "btn blue";
                    break;
                case "ELS":
                    hPrefix.Value = "2";
                    btnElp.CssClass = "btn blue";
                    btnEls.CssClass = "btn green";
                    btnFa.CssClass = "btn blue";
                    btnElwa.CssClass = "btn blue";
                    btnGrp.CssClass = "btn blue";
                    btnTrb.CssClass = "btn blue";
                    break;
                case "FA":
                    hPrefix.Value = "3";
                    btnElp.CssClass = "btn blue";
                    btnEls.CssClass = "btn blue";
                    btnFa.CssClass = "btn green";
                    btnElwa.CssClass = "btn blue";
                    btnGrp.CssClass = "btn blue";
                    btnTrb.CssClass = "btn blue";
                    break;
                case "ELWA":
                    hPrefix.Value = "4";
                    btnElp.CssClass = "btn blue";
                    btnEls.CssClass = "btn blue";
                    btnFa.CssClass = "btn blue";
                    btnElwa.CssClass = "btn green";
                    btnGrp.CssClass = "btn blue";
                    btnTrb.CssClass = "btn blue";
                    break;
                case "GRP":
                    hPrefix.Value = "5";
                    btnElp.CssClass = "btn blue";
                    btnEls.CssClass = "btn blue";
                    btnFa.CssClass = "btn blue";
                    btnElwa.CssClass = "btn blue";
                    btnGrp.CssClass = "btn green";
                    btnTrb.CssClass = "btn blue";
                    break;
                case "TRB":
                    hPrefix.Value = "6";
                    btnElp.CssClass = "btn blue";
                    btnEls.CssClass = "btn blue";
                    btnFa.CssClass = "btn blue";
                    btnElwa.CssClass = "btn blue";
                    btnGrp.CssClass = "btn blue";
                    btnTrb.CssClass = "btn green";
                    break;
            }
            bindingData();
            Console.WriteLine();
        }
    }
}