﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="PrintSticker.aspx.cs" Inherits="ALS.ALSI.Web.view.request.PrintSticker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //App.init(); // init the rest of plugins and elements
        });

        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              "<html><head><title></title><style type=\"text/css\" media=\"print\">@page { size: landscape; }.printbreak {page-break-before: always;}</style></head><body>" +
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            //document.body.innerHTML = oldPage;
            //window.location.assign("CashFlowCalendar.aspx")
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form1" method="post" runat="server" class="form-horizontal">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <h4><i class="icon-bar-chart"></i>&nbsp;Print Label</h4>
                    </div>
                    <br />
                    <h4>&nbsp;&nbsp;<i class="icon-info-sign"></i> Sample Infomation</h4>
                    <div class="row-fluid">
                        <div class="span12 ">
                            <asp:GridView ID="gvJob" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                CssClass="table table-striped table-hover table-bordered" ShowHeaderWhenEmpty="True" DataKeyNames="ID">
                                <Columns>
                                    <%--<asp:BoundField HeaderText="Date Received." DataField="create_date" ItemStyle-HorizontalAlign="Left" SortExpression="create_date" DataFormatString="{0:dd-MM-yyyy}" />--%>
                                    <asp:BoundField HeaderText="Ref No." DataField="job_number" ItemStyle-HorizontalAlign="Left" SortExpression="job_number" />
                                    <%--<asp:BoundField HeaderText="Comp" DataField="customer" ItemStyle-HorizontalAlign="Left" SortExpression="customer" />--%>
                                    <%--<asp:BoundField HeaderText="Contact" DataField="contract_person" ItemStyle-HorizontalAlign="Left" SortExpression="contract_person" />--%>
                                    <%--<asp:BoundField HeaderText="S/N" DataField="sn" ItemStyle-HorizontalAlign="Left" SortExpression="sn" />--%>
                                    <asp:BoundField HeaderText="Description" DataField="description" ItemStyle-HorizontalAlign="Left" SortExpression="description" />
                                    <asp:BoundField HeaderText="Model" DataField="model" ItemStyle-HorizontalAlign="Left" SortExpression="model" />
                                    <asp:BoundField HeaderText="Surface Area" DataField="surface_area" ItemStyle-HorizontalAlign="Left" SortExpression="surface_area" />
                                    <%--<asp:BoundField HeaderText="Remarks" DataField="remarks" ItemStyle-HorizontalAlign="Left" SortExpression="remarks" />--%>
                                    <asp:BoundField HeaderText="Specification" DataField="specification" ItemStyle-HorizontalAlign="Left" SortExpression="specification" />
                                    <asp:BoundField HeaderText="Type of test" DataField="type_of_test" ItemStyle-HorizontalAlign="Left" SortExpression="type_of_test" />
                                </Columns>
                                <PagerTemplate>
                                    <div class="pagination">
                                        <ul>
                                            <li>
                                                <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                    CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                    CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                            </li>
                                            <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                            <li>
                                                <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                    CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                    CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    <div class="data-not-found">
                                        <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                    </div>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>

                    <div id="divStrickerPreview">
                        <div class="modal-header">
                            <h4 id="H1"><i class="icon-print"></i> Label Preview</h4>
                        </div>
                        <div class="modal-body" id="divStricker">
                            <h4><img src="~/img/logo.jpg" width="30" height="30" runat="server" /> Testing Services (Thailand)</h4>
                            <table>
                                <tr>
                                    <td>Job No:</td>
                                    <td>
                                        <asp:Label ID="lbJobNo" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Client:</td>
                                    <td>
                                        <asp:Label ID="lbClient" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Contract:</td>
                                    <td>
                                        <asp:Label ID="lbContract" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sample:</td>
                                    <td>
                                        <asp:Label ID="lbSample" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Spec:</td>
                                    <td>
                                        <asp:Label ID="lbSpec" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Test:</td>
                                    <td>
                                        <%=type_of_test%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sample </td>
                                    <td>
                                        <%=sample_diposition%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button onclick="javascript:printDiv('divStricker');" class="cancel btn blue"><i class="icon-print"></i>Print</button>
                            <asp:Button ID="btnCancel" runat="server" class="cancel btn" Text="Cancel" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</asp:Content>
