﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SearchCustomer.aspx.cs" Inherits="ALS.ALSI.Web.view.customer.SearchCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%= ResolveClientUrl("~/js/jquery-1.8.3.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            App.init();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form id="Form1" method="post" runat="server">

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet">
                    <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Search Condition</h4>
                        <div class="actions">
                            <div class="btn-group">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn mini blue" OnClick="btnSearch_Click"><i class="icon-search"></i> Search</asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" class="btn mini black" OnClick="btnCancel_Click"><i class="icon-refresh"></i> Cancel</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">

<%--                        <div class="row-fluid">
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="txtJobNumber">Jub Number:</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtJobNumber" runat="server" class="m-wrap span6"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="txtDateOfRecieve">Date Of Recieve:</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtDateOfRecieve" runat="server" class="m-wrap span12 date-picker"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="txtId">Company:</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlCompany" runat="server" class="span12 chosen" DataTextField="company_name" DataValueField="ID" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="txtName">Contract Person:</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddlContract_person" runat="server" class="span12 chosen" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>--%>

                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box grey">
                    <div class="portlet-title">
                        <h4><i class="icon-bar-chart"></i>Search Result</h4>
                        <div class="actions">
                            <div class="btn-group">
                                <asp:LinkButton ID="LinkButton1" runat="server" class="btn mini green" OnClick="lbAdd_Click"><i class="icon-pencil"></i> Add</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <asp:Label ID="lbTotalRecords" runat="server" Text=""></asp:Label>
                        <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            CssClass="table table-striped table-hover table-bordered" ShowHeaderWhenEmpty="True" DataKeyNames="id" OnRowCommand="gvResult_RowCommand" OnRowDeleting="gvResult_RowDeleting" OnPageIndexChanging="gvResult_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Code" DataField="customer_code" ItemStyle-HorizontalAlign="Left" SortExpression="customer_code" />
                                <asp:BoundField HeaderText="Name" DataField="company_name" ItemStyle-HorizontalAlign="Left" SortExpression="company_name" />
                                <%--<asp:BoundField HeaderText="Address" DataField="address" ItemStyle-HorizontalAlign="Left" SortExpression="address" />--%>
                                <asp:BoundField HeaderText="Mobile" DataField="mobile_number" ItemStyle-HorizontalAlign="Left" SortExpression="mobile_number" />
                                <asp:BoundField HeaderText="Email" DataField="email_address" ItemStyle-HorizontalAlign="Left" SortExpression="email_address" />
                                <%--<asp:BoundField HeaderText="Branch" DataField="branch" ItemStyle-HorizontalAlign="Left" SortExpression="branch" />--%>
                                <asp:BoundField HeaderText="Department" DataField="department" ItemStyle-HorizontalAlign="Left" SortExpression="department" />
                                <asp:BoundField HeaderText="Province" DataField="province" ItemStyle-HorizontalAlign="Left" SortExpression="province" />

                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" ToolTip="Edit" CommandName="Edit" CommandArgument='<%# Eval("id")%>'><i class="icon-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="btnDelete" runat="server" ToolTip="Delete" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                            CommandArgument='<%# Eval("id")%>'><i class="icon-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <div class="pagination">
                                    <ul>
                                        <li>
                                            <asp:LinkButton ID="btnFirst" runat="server" CommandName="Page" CommandArgument="First"
                                                CausesValidation="false" ToolTip="First Page"><i class="icon-fast-backward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnPrev" runat="server" CommandName="Page" CommandArgument="Prev"
                                                CausesValidation="false" ToolTip="Previous Page"><i class="icon-backward"></i> Prev</asp:LinkButton>
                                        </li>
                                        <asp:PlaceHolder ID="pHolderNumberPage" runat="server" />
                                        <li>
                                            <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" CommandArgument="Next"
                                                CausesValidation="false" ToolTip="Next Page">Next <i class="icon-forward"></i></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="btnLast" runat="server" CommandName="Page" CommandArgument="Last"
                                                CausesValidation="false" ToolTip="Last Page"><i class="icon-fast-forward"></i></asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                <div class="data-not-found">
                                    <asp:Literal ID="libDataNotFound" runat="server" Text="Data Not found" />
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
