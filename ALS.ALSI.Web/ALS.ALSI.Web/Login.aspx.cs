﻿using ALS.ALSI.Biz.Constant;
using ALS.ALSI.Biz.DataAccess;
using ALS.ALSI.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ALS.ALSI.Web
{
    public partial class Login : System.Web.UI.Page
    {

        protected String Message
        {
            get { return (String)Session[GetType().Name + "Message"]; }
            set { Session[GetType().Name + "Message"] = value; }
        }

        public user_login objUser
        {
            get
            {
                user_login user = new user_login();
                user.username = Request.Params["username"];
                user.password = CustomUtils.EncodeMD5(Request.Params["password"]);

                return user;
            }
        }

        private void removeSession()
        {
            Session.Remove(GetType().Name + "Message");
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbLogin_Click(object sender, EventArgs e)
        {

            user_login user = objUser.Login();
            if (user != null)
            {
                Session.Add(Constants.SESSION_USER, user);
                if (!Convert.ToBoolean(user.is_force_change_password))
                {
                    removeSession();
                    Response.Redirect(Constants.LINK_SEARCH_JOB_REQUEST);
                }
                else {
                    Response.Redirect("ForceChangePassword.aspx");
                }
            }
            else
            {
                Message = "<div class=\"alert alert-error\"><button class=\"close\" data-dismiss=\"alert\"></button><span>Enter any username and passowrd.</span></div>";
            }
        }

    }
}