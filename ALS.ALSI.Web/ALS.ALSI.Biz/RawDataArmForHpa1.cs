﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALS.ALSI.Biz
{
    public class RawDataArmForHpa1
    {
        /*
               * Feature
               * Area	
               * Field	
               * Rank	
               * Al-Ti-O (0.5<=ECD<=2.0 um)	
               * Si-O	Fe-Cr-Ni	
               * Fe	
               * No Element	
               * Other	
               * Cr-O	
               * Rejected (Manual)	
               * Rejected (ED)	
               * Rejected (Morph)	
               * Area (sq. µm)	
               * Aspect Ratio	
               * Beam X (pixels)	
               * Beam Y (pixels)	
               * Breadth (µm)	
               * Direction (degrees)	
               * ECD (µm)	
               * Length (µm)	
               * Perimeter (µm)	
               * Shape	
               * Mean grey	
               * Spectrum Area	
               * Stage X (mm)	
               * Stage Y (mm)	
               * Stage Z (mm)	
               * O (Wt%)	
               * Mg (Wt%)	
               * Al (Wt%)	
               * Si (Wt%)	
               * P (Wt%)	
               * S (Wt%)	
               * K (Wt%)	
               * Ca (Wt%)	
               * Ti (Wt%)	
               * Cr (Wt%)	
               * Fe (Wt%)	
               * Ni (Wt%)	
               * Zn (Wt%)	
               * Se (Wt%)	
               * Cd (Wt%)
              */
        
        public String Feature { get; set; }
        public String Area { get; set; }
        public String Field { get; set; }
        public String Rank { get; set; }
        public String Al_Ti_O { get; set; }
        public String Si_O { get; set; }
        public String Fe_Cr_Ni { get; set; }
        public String Fe { get; set; }
        public String No_Element { get; set; }
        public String Other { get; set; }
        public String Cr_O { get; set; }
        public String Rejected_Manual { get; set; }
        public String Rejected_ED { get; set; }
        public String Rejected_Morph { get; set; }
        public String Area_squm { get; set; }
        public String Aspect_Ratio { get; set; }
        public String Beam_X { get; set; }
        public String Beam_Y { get; set; }
        public String Breadth { get; set; }
        public String Direction { get; set; }
        public String ECD { get; set; }
        public String Length { get; set; }
        public String Perimeter { get; set; }
        public String Shape { get; set; }
        public String Mean_grey { get; set; }
        public String Spectrum_Area { get; set; }
        public String Stage_X { get; set; }
        public String Stage_Y { get; set; }
        public String Stage_Z { get; set; }
        public String O_Wt { get; set; }
        public String Mg_Wt { get; set; }
        public String Al_Wt { get; set; }
        public String Si_Wt { get; set; }
        public String P_Wt { get; set; }
        public String S_Wt { get; set; }
        public String K_Wt { get; set; }
        public String Ca_Wt { get; set; }
        public String Ti_Wt { get; set; }
        public String Cr_Wt { get; set; }
        public String Fe_Wt { get; set; }
        public String Ni_Wt { get; set; }
        public String Zn_Wt { get; set; }
        public String Se_Wt { get; set; }
        public String Cd_Wt { get; set; }
    }
}
