//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALS.ALSI.Biz.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class template_wd_ic_coverpage
    {
        public int ID { get; set; }
        public Nullable<int> sample_id { get; set; }
        public Nullable<int> specification_id { get; set; }
        public string b11 { get; set; }
        public string b12 { get; set; }
        public string b13 { get; set; }
        public string b17 { get; set; }
        public string b18 { get; set; }
        public string b19 { get; set; }
        public string b20 { get; set; }
        public string b21 { get; set; }
        public string b22 { get; set; }
        public string b26 { get; set; }
        public string b27 { get; set; }
        public string b28 { get; set; }
        public string b29 { get; set; }
        public string b30 { get; set; }
        public string b31 { get; set; }
        public string c17 { get; set; }
        public string c18 { get; set; }
        public string c19 { get; set; }
        public string c20 { get; set; }
        public string c21 { get; set; }
        public string c22 { get; set; }
        public string c26 { get; set; }
        public string c27 { get; set; }
        public string c28 { get; set; }
        public string c29 { get; set; }
        public string c30 { get; set; }
        public string c31 { get; set; }
        public string item_visible { get; set; }
        public string d17 { get; set; }
        public string d18 { get; set; }
        public string d19 { get; set; }
        public string d20 { get; set; }
        public string d21 { get; set; }
        public string d22 { get; set; }
        public string d26 { get; set; }
        public string d27 { get; set; }
        public string d28 { get; set; }
        public string d29 { get; set; }
        public string d30 { get; set; }
        public string d31 { get; set; }
        public string f17 { get; set; }
        public string f18 { get; set; }
        public string f19 { get; set; }
        public string f20 { get; set; }
        public string f21 { get; set; }
        public string f22 { get; set; }
        public string f26 { get; set; }
        public string f27 { get; set; }
        public string f28 { get; set; }
        public string f29 { get; set; }
        public string f30 { get; set; }
        public string f31 { get; set; }
        public string i17 { get; set; }
        public string i18 { get; set; }
        public string i19 { get; set; }
        public string i20 { get; set; }
        public string i21 { get; set; }
        public string i22 { get; set; }
        public string i23 { get; set; }
        public string i26 { get; set; }
        public string i27 { get; set; }
        public string i28 { get; set; }
        public string i29 { get; set; }
        public string i30 { get; set; }
        public string i31 { get; set; }
        public string i32 { get; set; }
    
        public virtual job_sample job_sample { get; set; }
        public virtual tb_m_ic_specification tb_m_ic_specification { get; set; }
    }
}
