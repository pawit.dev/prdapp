//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALS.ALSI.Biz.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class template_wd_hpa_for3_coverpage
    {
        public int ID { get; set; }
        public Nullable<int> sample_id { get; set; }
        public Nullable<int> detail_spec_id { get; set; }
        public Nullable<int> component_id { get; set; }
        public string cvp_d23 { get; set; }
        public string cvp_d24 { get; set; }
        public string cvp_d25 { get; set; }
        public string ws_b57 { get; set; }
        public string ws_b58 { get; set; }
        public string ws_b59 { get; set; }
        public string ws_b60 { get; set; }
        public string ws_b61 { get; set; }
        public string ws_b62 { get; set; }
        public string ws_b63 { get; set; }
        public string ws_b64 { get; set; }
        public string ws_b65 { get; set; }
        public string ws_b66 { get; set; }
        public string ws_b67 { get; set; }
        public string ws_b68 { get; set; }
        public string ws_b69 { get; set; }
        public string ws_b70 { get; set; }
        public string ws_b71 { get; set; }
        public string ws_b72 { get; set; }
        public string ws_b73 { get; set; }
        public string ws_b74 { get; set; }
        public string ws_b75 { get; set; }
        public string ws_b78 { get; set; }
        public string ws_b79 { get; set; }
        public string ws_b80 { get; set; }
        public string ws_b81 { get; set; }
        public string ws_b82 { get; set; }
        public string ws_b83 { get; set; }
        public string ws_b84 { get; set; }
        public string ws_b102 { get; set; }
        public string ws_b103 { get; set; }
        public string ws_b104 { get; set; }
        public string ws_b105 { get; set; }
        public string ws_b106 { get; set; }
        public string ws_b107 { get; set; }
        public string ws_b108 { get; set; }
        public string ws_b109 { get; set; }
        public string ws_b110 { get; set; }
        public string ws_b113 { get; set; }
        public string ws_b114 { get; set; }
        public string ws_b115 { get; set; }
        public string ws_b116 { get; set; }
        public string ws_b117 { get; set; }
        public string ws_b118 { get; set; }
        public string ws_b119 { get; set; }
        public string ws_b120 { get; set; }
        public string ws_b121 { get; set; }
        public string ws_b122 { get; set; }
        public string ws_b123 { get; set; }
        public string ws_b124 { get; set; }
        public string ws_b125 { get; set; }
        public string ws_b126 { get; set; }
        public string ws_b127 { get; set; }
        public string ws_b128 { get; set; }
        public string ws_b129 { get; set; }
        public string ws_b130 { get; set; }
        public string ws_b131 { get; set; }
        public string ws_b132 { get; set; }
        public string ws_b133 { get; set; }
        public string ws_b134 { get; set; }
        public string ws_b135 { get; set; }
        public string ws_b136 { get; set; }
        public string ws_b137 { get; set; }
        public string ws_b138 { get; set; }
        public string ws_b139 { get; set; }
        public string ws_b140 { get; set; }
        public string ws_b141 { get; set; }
        public string ws_b142 { get; set; }
        public string ws_b143 { get; set; }
        public string ws_b144 { get; set; }
        public string ws_b145 { get; set; }
        public string ws_b146 { get; set; }
        public string ws_b147 { get; set; }
        public string ws_b148 { get; set; }
        public string ws_b149 { get; set; }
        public string ws_b150 { get; set; }
        public string ws_b151 { get; set; }
        public string ws_b152 { get; set; }
        public string ws_b153 { get; set; }
        public string ws_b154 { get; set; }
        public string ws_b155 { get; set; }
        public string ws_b156 { get; set; }
        public string ws_b157 { get; set; }
        public string ws_b158 { get; set; }
        public string ws_b173 { get; set; }
        public string ws_b174 { get; set; }
        public string ws_b175 { get; set; }
        public string ws_b176 { get; set; }
        public string ws_b177 { get; set; }
        public string ws_b178 { get; set; }
        public string ws_b179 { get; set; }
        public string ws_b180 { get; set; }
        public string ws_b181 { get; set; }
        public string ws_b182 { get; set; }
        public string ws_b183 { get; set; }
        public string ws_b184 { get; set; }
        public string ws_b185 { get; set; }
        public string ws_b186 { get; set; }
        public string ws_b187 { get; set; }
        public string ws_b188 { get; set; }
        public string ws_b189 { get; set; }
        public string ws_b190 { get; set; }
        public string ws_b191 { get; set; }
        public string ws_b194 { get; set; }
        public string ws_b195 { get; set; }
        public string ws_b196 { get; set; }
        public string ws_b197 { get; set; }
        public string ws_b198 { get; set; }
        public string ws_b199 { get; set; }
        public string ws_b200 { get; set; }
        public string ws_b218 { get; set; }
        public string ws_b219 { get; set; }
        public string ws_b220 { get; set; }
        public string ws_b221 { get; set; }
        public string ws_b222 { get; set; }
        public string ws_b223 { get; set; }
        public string ws_b224 { get; set; }
        public string ws_b225 { get; set; }
        public string ws_b226 { get; set; }
        public string ws_b229 { get; set; }
        public string ws_b230 { get; set; }
        public string ws_b231 { get; set; }
        public string ws_b232 { get; set; }
        public string ws_b233 { get; set; }
        public string ws_b234 { get; set; }
        public string ws_b235 { get; set; }
        public string ws_b236 { get; set; }
        public string ws_b237 { get; set; }
        public string ws_b238 { get; set; }
        public string ws_b239 { get; set; }
        public string ws_b240 { get; set; }
        public string ws_b241 { get; set; }
        public string ws_b242 { get; set; }
        public string ws_b243 { get; set; }
        public string ws_b244 { get; set; }
        public string ws_b245 { get; set; }
        public string ws_b246 { get; set; }
        public string ws_b247 { get; set; }
        public string ws_b248 { get; set; }
        public string ws_b249 { get; set; }
        public string ws_b250 { get; set; }
        public string ws_b251 { get; set; }
        public string ws_b252 { get; set; }
        public string ws_b253 { get; set; }
        public string ws_b254 { get; set; }
        public string ws_b255 { get; set; }
        public string ws_b256 { get; set; }
        public string ws_b257 { get; set; }
        public string ws_b258 { get; set; }
        public string ws_b259 { get; set; }
        public string ws_b260 { get; set; }
        public string ws_b261 { get; set; }
        public string ws_b262 { get; set; }
        public string ws_b263 { get; set; }
        public string ws_b264 { get; set; }
        public string ws_b265 { get; set; }
        public string ws_b266 { get; set; }
        public string ws_b267 { get; set; }
        public string ws_b268 { get; set; }
        public string ws_b269 { get; set; }
        public string ws_b270 { get; set; }
        public string ws_b271 { get; set; }
        public string ws_b272 { get; set; }
        public string ws_b273 { get; set; }
        public string ws_b274 { get; set; }
        public string ws_b289 { get; set; }
        public string ws_b290 { get; set; }
        public string ws_b291 { get; set; }
        public string ws_b292 { get; set; }
        public string ws_b293 { get; set; }
        public string ws_b294 { get; set; }
        public string ws_b295 { get; set; }
        public string ws_b296 { get; set; }
        public string ws_b297 { get; set; }
        public string ws_b298 { get; set; }
        public string ws_b299 { get; set; }
        public string ws_b300 { get; set; }
        public string ws_b301 { get; set; }
        public string ws_b302 { get; set; }
        public string ws_b303 { get; set; }
        public string ws_b304 { get; set; }
        public string ws_b305 { get; set; }
        public string ws_b306 { get; set; }
        public string ws_b307 { get; set; }
        public string ws_b310 { get; set; }
        public string ws_b311 { get; set; }
        public string ws_b312 { get; set; }
        public string ws_b313 { get; set; }
        public string ws_b314 { get; set; }
        public string ws_b315 { get; set; }
        public string ws_b316 { get; set; }
        public string ws_b334 { get; set; }
        public string ws_b335 { get; set; }
        public string ws_b336 { get; set; }
        public string ws_b337 { get; set; }
        public string ws_b338 { get; set; }
        public string ws_b339 { get; set; }
        public string ws_b340 { get; set; }
        public string ws_b341 { get; set; }
        public string ws_b342 { get; set; }
        public string ws_b345 { get; set; }
        public string ws_b346 { get; set; }
        public string ws_b347 { get; set; }
        public string ws_b348 { get; set; }
        public string ws_b349 { get; set; }
        public string ws_b350 { get; set; }
        public string ws_b351 { get; set; }
        public string ws_b352 { get; set; }
        public string ws_b353 { get; set; }
        public string ws_b354 { get; set; }
        public string ws_b355 { get; set; }
        public string ws_b356 { get; set; }
        public string ws_b357 { get; set; }
        public string ws_b358 { get; set; }
        public string ws_b359 { get; set; }
        public string ws_b360 { get; set; }
        public string ws_b361 { get; set; }
        public string ws_b362 { get; set; }
        public string ws_b363 { get; set; }
        public string ws_b364 { get; set; }
        public string ws_b365 { get; set; }
        public string ws_b366 { get; set; }
        public string ws_b367 { get; set; }
        public string ws_b368 { get; set; }
        public string ws_b369 { get; set; }
        public string ws_b370 { get; set; }
        public string ws_b371 { get; set; }
        public string ws_b372 { get; set; }
        public string ws_b373 { get; set; }
        public string ws_b374 { get; set; }
        public string ws_b375 { get; set; }
        public string ws_b376 { get; set; }
        public string ws_b377 { get; set; }
        public string ws_b378 { get; set; }
        public string ws_b379 { get; set; }
        public string ws_b380 { get; set; }
        public string ws_b381 { get; set; }
        public string ws_b382 { get; set; }
        public string ws_b383 { get; set; }
        public string ws_b384 { get; set; }
        public string ws_b385 { get; set; }
        public string ws_b386 { get; set; }
        public string ws_b387 { get; set; }
        public string ws_b388 { get; set; }
        public string ws_b389 { get; set; }
        public string ws_b390 { get; set; }
        public string item_visible { get; set; }
        public string img_path1 { get; set; }
        public string img_path2 { get; set; }
        public string img_path3 { get; set; }
    
        public virtual job_sample job_sample { get; set; }
        public virtual tb_component_hpa_for3 tb_component_hpa_for3 { get; set; }
        public virtual tb_detailspec_hpa_for3 tb_detailspec_hpa_for3 { get; set; }
    }
}
