﻿using System;

namespace ALS.ALSI.Biz
{
    public class DhsFile_a
    {
        public String Peak { get; set; }
        public String RTmin { get; set; }
        public String FirstScan { get; set; }
        public String MaxScan { get; set; }
        public String LastScan { get; set; }
        public String PKTY { get; set; }
        public String PeakHeight { get; set; }
        public String CorrArea { get; set; }
        public String CorrMax { get; set; }
        public String OfTotal { get; set; }
    }
}
