package com.toket.android.muicstore.info;

import java.util.ArrayList;

public class StartInfo {
	
	private ArrayList<StartMenuInfo> menu = new ArrayList<StartMenuInfo>();

	public ArrayList<StartMenuInfo> getMenu() {
		return menu;
	}

	public void addMenu(StartMenuInfo menu) {
		this.menu.add(menu);
	}
	
}
