package com.toket.android.muicstore.info;

import java.util.ArrayList;

public class ContactInfo {
	
	private ArrayList<ContactTeacherInfo> teacher = new ArrayList<ContactTeacherInfo>();

	public ArrayList<ContactTeacherInfo> getTeacher() {
		return teacher;
	}

	public void addTeacher(ContactTeacherInfo teacher) {
		this.teacher.add(teacher);
	}
	
}
