package com.toket.android.muicstore.info;

import java.util.ArrayList;

public class ARInfo {

	private ArrayList<ARMarkerInfo> maker = new ArrayList<ARMarkerInfo>();

	public ArrayList<ARMarkerInfo> getMaker() {
		return maker;
	}

	public void addMaker(ARMarkerInfo maker) {
		this.maker.add(maker);
	}
	
}
