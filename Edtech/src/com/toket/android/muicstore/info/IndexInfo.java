package com.toket.android.muicstore.info;

import java.util.ArrayList;

public class IndexInfo {
	
	private ArrayList<IndexChapterInfo> chapter = new ArrayList<IndexChapterInfo>();

	public ArrayList<IndexChapterInfo> getChapter() {
		return chapter;
	}

	public void addChapter(IndexChapterInfo chapter) {
		this.chapter.add(chapter);
	}
	
}
