package com.toket.android.muicstore.parser;

import android.content.Context;

import com.toket.android.muicstore.info.ARInfo;
import com.toket.android.muicstore.info.ARMarkerInfo;

public class ARParser extends AbstractXMLParser{
	
	public ARParser(Context context, String url) {
		super(context, url);
		// TODO Auto-generated constructor stub
	}
	
	public boolean connectInputStreamAsset(String assetsPath) {
		return super.connectInputStreamAsset(assetsPath);
	}
	
	public ARInfo parserInfo(){
		ARInfo info_ = new ARInfo();
		while(content.indexOf("<maker") > -1){
			attribute		= content;
			String id		= getAttributeValue("id");
			String title		= getXMLValue("title");
			String description		= getXMLValue("description");
			String image		= getXMLValue("image");
			String source		= getXMLValue("source");
			
			ARMarkerInfo item = new ARMarkerInfo();
			item.setId(id);
			item.setTitle(title);
			item.setDescription(description);
			item.setImage(image);
			item.setSource(source);
			
			info_.addMaker(item);
		}
		return info_;
	}
	
}
