package com.toket.android.muicstore.parser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;

public abstract class AbstractXMLParser {

	private static final String TYPE_STRING_ENCODE = "UTF-8";
	
	protected String url_;
	protected Context context_;

	protected String content;
	protected String node_content;
	protected String node_content2;
	protected String attribute;
	protected String node_attribute;
	
	public AbstractXMLParser(Context context, String url){
		url_ = url;
		context_ = context;
	}
	
	protected void setURL(String url){
		url_ = url;
	}
	protected String getURL(){
		return url_;
	}

	protected boolean connectInputStream(){
		InputStream input = getInputStream(url_);
		content = convertStreamToString(input);
		if(input == null){
			return false;
		}
		return true;
	}

	protected boolean connectInputStreamAsset(String assetsPath){
		InputStream input;
		try {
			input = context_.getAssets().open(assetsPath);
			content = convertStreamToString(input);
			if(input == null){
				return false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	protected boolean connectInputStreamSDCard(String sdPath){
		FileInputStream input = null;
		try {
			input = new FileInputStream(sdPath);
			content = convertStreamToString(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private static InputStream getInputStream(String url){

		System.out.println("getUrlData ===>>> " + url);
		
		int timeOutMS = 1000*10;
		InputStream is = null;
		
		try {
			URL myURL = new URL(url);
			URLConnection ucon = myURL.openConnection();
			ucon.setConnectTimeout(timeOutMS);
			is = ucon.getInputStream();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return is;
	}
	
	private static String convertStreamToString(InputStream is) {

		  StringBuilder sb = new StringBuilder();

		  try {
		   BufferedReader reader = new BufferedReader(new InputStreamReader(
		     is, TYPE_STRING_ENCODE));
		   String line = null;
		   try {
		    while ((line = reader.readLine()) != null) {
		     sb.append(line + "\n");
		    }
		   } catch (IOException e) {
		    e.printStackTrace();
		   } finally {
		    try {
		     is.close();
		    } catch (IOException e) {
		     e.printStackTrace();
		    }
		   }
		  } catch (Exception e) {

		  }

		  return sb.toString();
	}

	protected String getXMLValue(String aName) {
		String result_ = "";
		try {
			// remove attribute
			String startTAG = "<" + aName;
			String endTAG	= ">";
			int startTAGIndex = content.indexOf(startTAG);
			int endTAGIndex = content.indexOf(endTAG, startTAGIndex + startTAG.length());
			if(startTAGIndex > -1 && endTAGIndex > -1){
				String oldChar = content.substring(startTAGIndex + startTAG.length(), endTAGIndex);
				if(oldChar.length() != 0){
					content = content.replace(oldChar, "");
				}
			}
			
			String startTAG_ = "<" + aName + ">";
			String endTAG_ = "</" + aName + ">";
			int startIndex_ = content.indexOf(startTAG_);
			int endIndex_ = content.indexOf(endTAG_);
			if (startIndex_ > -1 && endIndex_ > -1) {
				result_ = content.substring(startIndex_ + startTAG_.length(),
						endIndex_);
				if(result_.indexOf("<![CDATA[") > -1){
					result_ = result_.replace("<![CDATA[", "");
					result_ = result_.replace("]]>", "");
				}

				content = content.substring(endIndex_ + 1);
			}
		} catch (Exception ex) {
			System.out.println("getXMLValue() error" + "\n" + ex.getMessage());
			// ex.printStackTrace();
		}
		return result_;
	}

	protected String getXMLNodeValue(String aName) {
		String result_ = "";
		try {
			// remove attribute
			String startTAG = "<" + aName;
			String endTAG = ">";
			int startTAGIndex = node_content.indexOf(startTAG);
			int endTAGIndex = node_content.indexOf(endTAG, startTAGIndex + startTAG.length());
			if (startTAGIndex > -1 && endTAGIndex > -1) {
				String oldChar = node_content.substring(startTAGIndex + startTAG.length(), endTAGIndex);
				if(oldChar.length() != 0){
					node_content = node_content.replace(oldChar, "");
				}
			}
						
			String startTAG_ = "<" + aName + ">";
			String endTAG_ = "</" + aName + ">";
			int startIndex_ = node_content.indexOf(startTAG_);
			int endIndex_ = node_content.indexOf(endTAG_);
			if (startIndex_ > -1 && endIndex_ > -1) {
				result_ = node_content.substring(startIndex_ + startTAG_.length(),
						endIndex_);
				if(result_.indexOf("<![CDATA[") > -1){
					result_ = result_.replace("<![CDATA[", "");
					result_ = result_.replace("]]>", "");
				}

				node_content = node_content.substring(endIndex_ + 1);
			}
		} catch (Exception ex) {
			System.out.println("getXMLNodeValue() error" + "\n" + ex.getMessage());
			// ex.printStackTrace();
		}
		return result_;
	}
	
	protected String getXMLNodeValue2(String aName) {
		String result_ = "";
		try {
			// remove attribute
			String startTAG = "<" + aName;
			String endTAG = ">";
			int startTAGIndex = node_content2.indexOf(startTAG);
			int endTAGIndex = node_content2.indexOf(endTAG, startTAGIndex + startTAG.length());
			if (startTAGIndex > -1 && endTAGIndex > -1) {
				String oldChar = node_content2.substring(startTAGIndex + startTAG.length(), endTAGIndex);
				if(oldChar.length() != 0){
					node_content2 = node_content2.replace(oldChar, "");
				}
			}
						
			String startTAG_ = "<" + aName + ">";
			String endTAG_ = "</" + aName + ">";
			int startIndex_ = node_content2.indexOf(startTAG_);
			int endIndex_ = node_content2.indexOf(endTAG_);
			if (startIndex_ > -1 && endIndex_ > -1) {
				result_ = node_content2.substring(startIndex_ + startTAG_.length(),
						endIndex_);
				if(result_.indexOf("<![CDATA[") > -1){
					result_ = result_.replace("<![CDATA[", "");
					result_ = result_.replace("]]>", "");
				}

				node_content2 = node_content2.substring(endIndex_ + 1);
			}
		} catch (Exception ex) {
			System.out.println("getXMLNodeValue2() error" + "\n" + ex.getMessage());
			// ex.printStackTrace();
		}
		return result_;
	}

	protected String getAttributeValue(String aName) {
		String result_ = "";
		try {
			String startTAG_ = aName + "=\"";
			String endTAG_ = "\"";
			int startIndex_ = attribute.indexOf(startTAG_);
			int endIndex_ = attribute.indexOf(endTAG_, startIndex_ + startTAG_.length());
			if (startIndex_ > -1 && endIndex_ > -1) {
				result_ = attribute.substring(startIndex_ + startTAG_.length(),
						endIndex_);
				if(result_.indexOf("<![CDATA[") > -1){
					result_ = result_.replace("<![CDATA[", "");
					result_ = result_.replace("]]>", "");
				}

				attribute = attribute.substring(endIndex_ + 1);
			}
		} catch (Exception ex) {
			System.out.println("getAttributeValue() error" + "\n" + ex.getMessage());
			// ex.printStackTrace();
		}
		return result_;
	}
	
	protected String getNodeAttributeValue(String aName) {
		String result_ = "";
		try {
			String startTAG_ = aName + "=\"";
			String endTAG_ = "\"";
			int startIndex_ = node_attribute.indexOf(startTAG_);
			int endIndex_ = node_attribute.indexOf(endTAG_, startIndex_ + startTAG_.length());
			if (startIndex_ > -1 && endIndex_ > -1) {
				result_ = attribute.substring(startIndex_ + startTAG_.length(),
						endIndex_);
				if(result_.indexOf("<![CDATA[") > -1){
					result_ = result_.replace("<![CDATA[", "");
					result_ = result_.replace("]]>", "");
				}

				node_attribute = node_attribute.substring(endIndex_ + 1);
			}
		} catch (Exception ex) {
			System.out.println("getNodeAttributeValue() error" + "\n" + ex.getMessage());
			// ex.printStackTrace();
		}
		return result_;
	}

}
