package com.toket.android.muicstore.parser;

import android.content.Context;

import com.toket.android.muicstore.info.MediaClipInfo;
import com.toket.android.muicstore.info.MediaInfo;

public class MediaParser extends AbstractXMLParser{
	
	public MediaParser(Context context, String url) {
		super(context, url);
		// TODO Auto-generated constructor stub
	}
	
	public boolean connectInputStreamAsset(String assetsPath) {
		return super.connectInputStreamAsset(assetsPath);
	}
	
	public MediaInfo parserInfo(){
		MediaInfo info_ = new MediaInfo();
		while(content.indexOf("<clip") > -1){
			attribute		= content;
			String id			= getAttributeValue("id");
			String title		= getXMLValue("title");
			String description	= getXMLValue("description");
			String image		= getXMLValue("image");
			String source		= getXMLValue("source");

			MediaClipInfo item = new MediaClipInfo();
			item.setId(id);
			item.setTitle(title);
			item.setDescription(description);
			item.setImage(image);
			item.setSource(source);
			
			info_.addClip(item);
		}
		return info_;
	}
	
}
