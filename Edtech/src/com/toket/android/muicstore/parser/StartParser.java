package com.toket.android.muicstore.parser;

import android.content.Context;

import com.toket.android.muicstore.info.StartInfo;
import com.toket.android.muicstore.info.StartMenuInfo;

public class StartParser extends AbstractXMLParser{
	
	public StartParser(Context context, String url) {
		super(context, url);
		// TODO Auto-generated constructor stub
	}
	
	public boolean connectInputStreamAsset(String assetsPath) {
		return super.connectInputStreamAsset(assetsPath);
	}
	
	public StartInfo parserInfo(){
		StartInfo info_ = new StartInfo();
		while(content.indexOf("<menu") > -1){
			attribute		= content;
			String id		= getAttributeValue("id");
			String name		= getXMLValue("name");
			String src		= getXMLValue("src");

			StartMenuInfo item = new StartMenuInfo();
			item.setId(id);
			item.setName(name);
			item.setSrc(src);
			
			info_.addMenu(item);
		}
		return info_;
	}
	
}
