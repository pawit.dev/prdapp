package com.toket.android.muicstore.adapter;

import pl.polidea.treeview.AbstractTreeViewAdapter;
import pl.polidea.treeview.TreeNodeInfo;
import pl.polidea.treeview.TreeStateManager;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.toket.android.muicstore.R;

/**
 * This is a very simple adapter that provides very basic tree view with a
 * checkboxes and simple item description.
 * 
 */
public class IndexAdapter extends AbstractTreeViewAdapter<String> {
	
	private OnHandleItemClickListener mListener;
    
    public void setOnHandleItemClickListener(OnHandleItemClickListener eventListener) {
    	mListener = eventListener;
    }
    
    public interface OnHandleItemClickListener {

    	public void onItemClick(View view, TreeNodeInfo<String> node_info, String value);
        
    }
	
    public IndexAdapter(final Activity treeViewListDemo,
            final TreeStateManager<String> treeStateManager,
            final int numberOfLevels) {
        super(treeViewListDemo, treeStateManager, numberOfLevels);
        
    }

    @Override
    public View getNewChildView(final TreeNodeInfo<String> treeNodeInfo) {
        final LinearLayout viewLayout = (LinearLayout) getActivity()
                .getLayoutInflater().inflate(R.layout.item_index_list, null);
        return updateView(viewLayout, treeNodeInfo);
    }

    @Override
    public LinearLayout updateView(final View view,
            final TreeNodeInfo<String> treeNodeInfo) {
        final LinearLayout viewLayout = (LinearLayout) view;
        final TextView text_index_list = (TextView) viewLayout.findViewById(R.id.text_index_list);
        text_index_list.setText(treeNodeInfo.getId());
        
        ImageView index_indicator = (ImageView) view.findViewById(R.id.index_indicator);
		if(treeNodeInfo.isExpanded()){
			index_indicator.setImageResource(R.drawable.arrow_r);
		}else{
			index_indicator.setImageResource(R.drawable.arrow);
		}
        return viewLayout;
    }

    @Override
    public void handleItemClick(final View view, final Object id) {
        final String longId = (String) id;
        final TreeNodeInfo<String> info = getManager().getNodeInfo(longId);
        if(mListener != null){
        	mListener.onItemClick(view, info, longId);
        }
        if(info.isWithChildren()){
        	super.handleItemClick(view, longId);
        }
    }

    @Override
    public long getItemId(final int position) {
        return 0;
    }
}