package com.toket.android.muicstore.view;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.info.MediaClipInfo;
import com.toket.android.muicstore.info.MediaInfo;
import com.toket.android.muicstore.parser.MediaParser;
import com.toket.android.muicstore.screen.VideoScreen;
import com.toket.android.utils.ScalableImageView;

public class MediaView extends AbstractView{
	
	private LinearLayout Layout_media_view;
	
	private ListView listview_media;
	private MyListAdapter adp;
	
	public MediaView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setVisibleView(boolean value){
		if(value){
			Layout_media_view.setVisibility(View.VISIBLE);
		}else{
			Layout_media_view.setVisibility(View.GONE);
		}
	}
	
	public boolean isViewVisible(){
		return Layout_media_view.getVisibility() == View.VISIBLE;
	}

	public void loadContent(String path){
		MyJsonParser parser = new MyJsonParser();
		parser.execute(path);
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		Layout_media_view = (LinearLayout) screen.findViewById(R.id.Layout_media_view);
		
		listview_media	= (ListView) screen.findViewById(R.id.listview_media);
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		adp = new MyListAdapter(context, new ArrayList<MediaClipInfo>());
		listview_media.setAdapter(adp);
		listview_media.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				String linkDownload = adp.getAllInfo().get(position).getSource();
				if(linkDownload.length() == 0) return;
				
				String saveFilePath = DATA_STORAGE + screen.getApplication().getPackageName() +
						"/" + linkDownload.substring(linkDownload.lastIndexOf("/")+1, linkDownload.length());
				
				DownloadMediaFile download = new DownloadMediaFile(context, linkDownload, saveFilePath);
				download.execute("");
			}
		});
	}
	
	private class DownloadMediaFile extends AsyncTask<String, Integer, Boolean>{
		
		static final int BUFFER_SIZE = 1024*4;
		Context mContext;
		ProgressDialog pd;
		String downloadPath, saveFilePath;
		AsyncTask<String, Integer, Boolean> async;
		
		public DownloadMediaFile(Context mContext, String downloadPath, String saveFilePath) {
			this.mContext = mContext;
			this.downloadPath = downloadPath;
			this.saveFilePath = saveFilePath;
			this.async = this;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(mContext);
			pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			File folder = new File(saveFilePath);
			if(folder.exists()){
				pd.setMessage("Opening...");
			}else{
				pd.setMessage("Downloading...");
			}
			pd.setCancelable(true);
			pd.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					async.cancel(true);
					System.out.println("## Cancel download");
				}
			});
			pd.show();
		}

		@Override
		protected Boolean doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			File folder = new File(saveFilePath);
			if(folder.exists()){
				return true;
			}
			try {
				long downloaded = 0;
				
				URL url = new URL(downloadPath);
				URLConnection connection = url.openConnection();
				connection.setConnectTimeout(1000*10);
				connection.connect();
				int fileLength = connection.getContentLength();
				
				// download the file
				InputStream input = new BufferedInputStream(connection.getInputStream());
				OutputStream output = new FileOutputStream(saveFilePath, true);
				
				byte data[] = new byte[BUFFER_SIZE];
				int count;
				System.out.println("## FILE SIZE = " + fileLength);
				while ((count = input.read(data)) != -1) {
					downloaded += count;
					// publishing the progress....
					publishProgress((int) (downloaded * 100 / fileLength));
					output.write(data, 0, count);
					System.out.println("## DOWNLOADED = " + downloaded);

					if(async.isCancelled()){
						if(folder.exists()){
							folder.delete();
							System.out.println("## DELETE FILE = " + saveFilePath);
						}
						break;
					}
				}

				output.flush();
				output.close();
				input.close();
				
				if(async.isCancelled()){
					return false;
				}
			} catch (Exception e) {
				// TODO: handle exception
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			if(result){
				VideoScreen.video_path = saveFilePath;
				Intent goToScreen = new Intent(context, VideoScreen.class);
				screen.startActivity(goToScreen);
			}else{
				Toast.makeText(context, "Connection fail.", Toast.LENGTH_SHORT).show();
			}
		}
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			pd.setProgress(values[0]);
		}

	}
	
	private class MyJsonParser extends AsyncTask<String, Void, MediaInfo>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected MediaInfo doInBackground(String... url) {
			// TODO Auto-generated method stub
			MediaParser parser = new MediaParser(context, url[0]);
			if(parser.connectInputStreamAsset(url[0])){
				return parser.parserInfo();
			}
			return null;
		}

		@Override
		protected void onPostExecute(MediaInfo result) {
			// TODO Auto-generated method stub
			if(result != null){
				adp.setAllInfo(result.getClip());
				adp.notifyDataSetChanged();
			}
			
		}
		
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

	private class MyListAdapter extends BaseAdapter {

        private final Context mContext;
        private ArrayList<MediaClipInfo> info_;

        public MyListAdapter(Context context, ArrayList<MediaClipInfo> info) {
            super();
            mContext = context;
            info_ = info;
            
        }
        
        public void setAllInfo(ArrayList<MediaClipInfo> info){
        	if(info_ != null && info != null){
        		info_.clear();
        		info_.addAll(info);
        	}else if(info_ == null && info != null){
        		info_ = info;
        	}
        }
        
        public ArrayList<MediaClipInfo> getAllInfo(){
        	return info_;
        }
        
//        public void removeAllInfo(){
//        	if(info_ != null){
//        		info_.clear();
//        	}
//        	notifyDataSetChanged();
//        }
        
        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View v, ViewGroup container) {
        	
        	MediaClipInfo info = getItem(position);
        	
        	if(v == null){
        		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        		v = inflater.inflate(R.layout.item_list_view, container, false);
        	}
        	
        	try {
				// get input stream
				InputStream ims = screen.getAssets().open(info.getImage());
				// load image as Drawable
				Drawable d = Drawable.createFromStream(ims, null);
				
				ScalableImageView image_home_view = (ScalableImageView) v.findViewById(R.id.image_home_view);
				image_home_view.setImageDrawable(d);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			TextView text_list_title	= (TextView) v.findViewById(R.id.text_list_title);
//			TextView text_list_detail	= (TextView) v.findViewById(R.id.text_list_detail);
			
			text_list_title.setText(info.getDescription());
//			text_list_detail.setText(info.getDescription());
			
			text_list_title.setSelected(true);
			
//			text_list_detail.setVisibility(View.GONE);
			
            return v;
        }

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return info_.size();
		}

		@Override
		public MediaClipInfo getItem(int position) {
			// TODO Auto-generated method stub
			return info_.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
        
    }

}
