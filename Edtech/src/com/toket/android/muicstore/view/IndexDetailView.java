package com.toket.android.muicstore.view;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.screen.MainScreen;

public class IndexDetailView extends AbstractView{
	
	private LinearLayout Layout_index_detail_view;
	
	private WebView webview_index_detail;
	
	public IndexDetailView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setVisibleView(boolean value, String assetHtmlPath, String newTitleName){
		if(value){
			((MainScreen)screen).getTopView().setVisibleBackButton(true);
			((MainScreen)screen).getTopView().setTextTitle(newTitleName);
			Layout_index_detail_view.setVisibility(View.VISIBLE);
			webview_index_detail.loadUrl("file:///android_asset/" + assetHtmlPath);
		}else{
			webview_index_detail.clearView();
			Layout_index_detail_view.setVisibility(View.GONE);
			((MainScreen)screen).getTopView().setTextTitle(
					((MainScreen)screen).getBottomView().getStartInfo().getMenu().get(1).getName());
			((MainScreen)screen).getTopView().setVisibleBackButton(false);
		}
	}
	
	public boolean isViewVisible(){
		return Layout_index_detail_view.getVisibility() == View.VISIBLE;
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		Layout_index_detail_view = (LinearLayout) screen.findViewById(R.id.Layout_index_detail_view);
		
		webview_index_detail	= (WebView) screen.findViewById(R.id.webview_index_detail);
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

}
