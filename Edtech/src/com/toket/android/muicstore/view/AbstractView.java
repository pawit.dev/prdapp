package com.toket.android.muicstore.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.toket.android.utils.ApplicationConfigs;

public abstract class AbstractView implements ApplicationConfigs{
	
	protected ProgressDialog progressBar;
	protected boolean isCancleProgressBar;
	protected int screenWidth, screenHeight;
	protected Context context;
	protected Activity screen;
	
	public AbstractView(Context context){
		this.context = context;
		this.screen = (Activity) context;

		DisplayMetrics dm = new DisplayMetrics();
		screen.getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;

		initLayout();
		initValue();
	}

	abstract protected void initLayout();
	abstract protected void initValue();
	abstract public void cleanUp();

	protected ProgressDialog showProgressDialog(final String header, final String body,
			final boolean cancelable) {
		isCancleProgressBar = false;
		dismissProgressDialog();
		
		screen.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub

				try {		
					progressBar = new ProgressDialog(context);
					progressBar.setMax(2);
					progressBar.setTitle(header);
					progressBar.setMessage(body);
					progressBar.setCancelable(cancelable);
					progressBar.setIndeterminate(true);
					progressBar.show();
					
					progressBar.setOnCancelListener(new OnCancelListener() {
						
						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							isCancleProgressBar = true;
						}
					});
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		return progressBar;
	}

	protected void dismissProgressDialog() {
		screen.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					if (progressBar != null && progressBar.isShowing()){
						progressBar.dismiss();
						progressBar = null;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
	}
	
	protected boolean isShowProgressDialog(){
		if(progressBar != null){
			return progressBar.isShowing();
		}else{
			return false;
		}
	}

	protected void cleanUpDrawable(View view) {
		Drawable d = null;
		if (view != null) {
			// // cleanup imageview
			if (view instanceof ImageView) {
				ImageView v = (ImageView) view;

				if (v != null) {
					d = v.getDrawable();
					if (d != null) {
						((BitmapDrawable) d).getBitmap().recycle();
					}

					d = v.getBackground();
					if (d != null) {
						try {
							((BitmapDrawable) d).getBitmap().recycle();
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("AbstractView.cleanUpDrawable() Exception :: " + e);
						}
					}
				}
			}

			// // cleanup viewgroup(layout)
			else if (view instanceof ViewGroup) {
				ViewGroup v = (ViewGroup) view;

				if (v != null) {
					for (int i = 0; i < v.getChildCount(); i++) {
						View childView = v.getChildAt(i);
						cleanUpDrawable(childView);
					}
				}
			}

			// //
			else {
				d = view.getBackground();
				if (d != null) {
					try {
						((BitmapDrawable) d).getBitmap().recycle();
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("AbstractView.cleanUpDrawable() Exception :: " + e);
					}
				}
			}

		}
	}

}
