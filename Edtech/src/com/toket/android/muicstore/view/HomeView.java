package com.toket.android.muicstore.view;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.info.HomeInfo;
import com.toket.android.muicstore.parser.HomeParser;

public class HomeView extends AbstractView{
	
	private LinearLayout Layout_home_view;
	
	private ImageView image_home_view;
	
	public HomeView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public void setVisibleView(boolean value){
		if(value){
			Layout_home_view.setVisibility(View.VISIBLE);
		}else{
			Layout_home_view.setVisibility(View.GONE);
		}
	}
	
	public boolean isViewVisible(){
		return Layout_home_view.getVisibility() == View.VISIBLE;
	}
	
	public void loadContent(String path){
		MyJsonParser parser = new MyJsonParser();
		parser.execute(path);
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		Layout_home_view = (LinearLayout) screen.findViewById(R.id.Layout_home_view);
		
		image_home_view		= (ImageView) screen.findViewById(R.id.image_home_view);
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		
	}

	private class MyJsonParser extends AsyncTask<String, Void, HomeInfo>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		protected HomeInfo doInBackground(String... url) {
			// TODO Auto-generated method stub
			HomeParser parser = new HomeParser(context, url[0]);
			if(parser.connectInputStreamAsset(url[0])){
				return parser.parserInfo();
			}
			return null;
		}

		@Override
		protected void onPostExecute(HomeInfo result) {
			// TODO Auto-generated method stub
			if(result != null){
				try {
					// get input stream
					InputStream ims = screen.getAssets().open(result.getCover().get(0).getSrc());
					// load image as Drawable
					Drawable d = Drawable.createFromStream(ims, null);
					image_home_view.setImageDrawable(d);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}

	@Override
	public void cleanUp() {
		// TODO Auto-generated method stub
		
	}

}
