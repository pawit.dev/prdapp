package com.toket.android.muicstore.screen;

import android.content.Context;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.activity.MUICStoreActivity;

public class VideoScreen extends MUICStoreActivity{
	
	public static String video_path;
	
	private static Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_screen);
		
		mContext = this;
		
		if(video_path == null){
			showToast("No Uri path.", Toast.LENGTH_SHORT);
			return;
		}
		
		initActivity();
		
	}
	
	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		System.out.println("## PATH = " + video_path);
		MediaController mediacontroller = new MediaController(mContext);
		
		VideoView video = (VideoView) this.findViewById (R.id.video_view);
		video.setMediaController(mediacontroller);
		video.setVideoPath(video_path);
		video.requestFocus();
		video.start();
	}

	@Override
	protected void cleanUp() {
		// TODO Auto-generated method stub
		
	}

}
