package com.toket.android.muicstore.screen;

import java.io.File;

import pl.polidea.treeview.InMemoryTreeStateManager;
import pl.polidea.treeview.TreeStateManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.toket.android.muicstore.R;
import com.toket.android.muicstore.activity.MUICStoreActivity;
import com.toket.android.muicstore.info.StartInfo;
import com.toket.android.muicstore.parser.StartParser;
import com.toket.android.muicstore.view.ARView;
import com.toket.android.muicstore.view.BottomView;
import com.toket.android.muicstore.view.ContactView;
import com.toket.android.muicstore.view.HomeView;
import com.toket.android.muicstore.view.IndexDetailView;
import com.toket.android.muicstore.view.IndexExpandListView;
import com.toket.android.muicstore.view.MediaView;
import com.toket.android.muicstore.view.TopView;
import com.toket.android.utils.ApplicationConfigs;

public class MainScreen extends MUICStoreActivity{
	
	public static Context mContext;
	
    private TreeStateManager<String> manager = null;
    
    private TopView mTopView;
    private HomeView  mHomeView;
    private IndexExpandListView mIndexExpandListView;
    private IndexDetailView mIndexDetailView;
    private MediaView mMediaView;
    private ARView mARView;
    private ContactView mContactView;
    private BottomView mBottomView;
    
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_screen);
		
		if (savedInstanceState == null) {
			manager = new InMemoryTreeStateManager<String>();
		}else{
			manager = (TreeStateManager<String>) savedInstanceState
                    .getSerializable("treeManager");
		}
		
		// Checking application data storage path
		String dataStoragePath = ApplicationConfigs.DATA_STORAGE + getApplication().getPackageName();
		File folder = new File(dataStoragePath);
		if(!folder.exists()){
			folder.mkdirs();
		}
		
		mContext = this;
		initActivity();
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		deleteAPK();
	}
	
	private void deleteAPK(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				String dataStoragePath = ApplicationConfigs.DATA_STORAGE + getApplication().getPackageName();
				File sdPath = new File(dataStoragePath);
				if(sdPath.exists() && sdPath.isDirectory()){
					for(File file : sdPath.listFiles()){
						if(file.getAbsolutePath().indexOf(".apk") != -1){
							file.delete();
						}
					}
				}else{
					sdPath.mkdir();
				}
				
			}
		}).start();
	}
	
	public TreeStateManager<String> getTreeStateManager(){
		return manager;
	}

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        outState.putSerializable("treeManager", manager);
        super.onSaveInstanceState(outState);
    }

	@Override
	protected void initLayout() {
		// TODO Auto-generated method stub
		SetUpLayout setup = new SetUpLayout();
		setup.execute("");
	}
	
	private class SetUpLayout extends AsyncTask<String, Void, String>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			showProgressDialog("", "Loading...", true);
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			mTopView				= new TopView(mContext);
			
			mHomeView				= new HomeView(mContext);
			mIndexExpandListView	= new IndexExpandListView(mContext);
			mIndexDetailView		= new IndexDetailView(mContext);
			mMediaView				= new MediaView(mContext);
			mARView					= new ARView(mContext);
			mContactView			= new ContactView(mContext);
			
			mBottomView				= new BottomView(mContext);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			dismissProgressDialog();
		}
		
	}

	@Override
	protected void initValue() {
		// TODO Auto-generated method stub
		MyJsonParser parser = new MyJsonParser();
		parser.execute(ApplicationConfigs.PATH_START);
	}

	private class MyJsonParser extends AsyncTask<String, Void, StartInfo>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			showProgressDialog("", "Loading...", true);
		}

		@Override
		protected StartInfo doInBackground(String... url) {
			// TODO Auto-generated method stub
			StartParser parser = new StartParser(mContext, url[0]);
			if(parser.connectInputStreamAsset(url[0])){
				return parser.parserInfo();
			}
			return null;
		}

		@Override
		protected void onPostExecute(StartInfo result) {
			// TODO Auto-generated method stub
			if(result != null){
				mBottomView.setStartInfo(result);
				mBottomView.onClickMenu(BottomView.MENU_HOME);
				mHomeView.loadContent(result.getMenu().get(0).getSrc());
				mIndexExpandListView.loadContent(result.getMenu().get(1).getSrc());
				mMediaView.loadContent(result.getMenu().get(2).getSrc());
				mARView.loadContent(result.getMenu().get(3).getSrc());
				mContactView.loadContent(result.getMenu().get(4).getSrc());
			}
			dismissProgressDialog();
		}
		
	}

	@Override
	protected void cleanUp() {
		// TODO Auto-generated method stub
		VideoScreen.video_path = null;
//		deleteAPK();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(mIndexDetailView.isViewVisible()){
			mIndexDetailView.setVisibleView(false, "", mBottomView.getStartInfo().getMenu().get(1).getName());
		}else{
			finish();
		}
	}
	
	//##################################//
	//########### METHOD ###############//
	//##################################//
	
	public TopView getTopView(){
		return mTopView;
	}
	
	public HomeView getHomeView(){
		return mHomeView;
	}
	
	public IndexExpandListView getIndexExpandListView(){
		return mIndexExpandListView;
	}

	public IndexDetailView getIndexDetailView(){
		return mIndexDetailView;
	}
	
	public MediaView getMediaView(){
		return mMediaView;
	}
	
	public ARView getARView(){
		return mARView;
	}
	
	public ContactView getContactView(){
		return mContactView;
	}
	
	public BottomView getBottomView(){
		return mBottomView;
	}
	
}
