package com.toket.android.utils;

public class Shared {
	
	// ############################ //
	//	SharedPreferences
	// ############################ //
	public static final String SHARED_NAME = "com.toket.android.aeon";
	
	public static final String PREF_USERNAME = "username";
	public static final String PREF_PASSWORD = "password";
	public static final String PREF_ISLOGIN = "islogin";
	public static final String PREF_IS_SOUND_ON = "is_sound_on";
	public static final String PREF_FIRST_INSTALL = "first_install";
	public static final String PREF_FIRST_LOGIN = "first_login";
	public static final String PREF_LANGUAGE_TH = "language";
	public static final String PREF_USER_ID = "user_id";
	public static final String PREF_COLLECTION_SIZE = "collection_size";
	public static final String PREF_COLLECTION_ = "collection_";
	public static final String PREF_VOLUME = "app_volume";
	

	public static final boolean DEFAULT_ISLOGIN = false;
	public static final boolean DEFAULT_IS_SOUND_ON = true;
	public static final boolean DEFAULT_FIRST_INSTALL = true;
	public static final boolean DEFAULT_FIRST_LOGIN = true;
	public static final boolean DEFAULT_LANGUAGE_TH = true;
	
	public static final String DEFAULT_USERNAME = "";
	public static final String DEFAULT_PASSWORD = "";
	public static final String DEFAULT_USER_ID = "";

	public static final int DEFAULT_COLLECTION_SIZE = 0;
	public static final int DEFAULT_COLLECTION_ = -1;
	public static final int DEFAULT_VOLUME = 6;
	
}
